<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Custom\Customer\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Msrp\Model\Product\Attribute\Source\Type;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Api\ProductRepositoryInterface;

/**
 * DefaultHelper data helper
 */
class DefaultHelper extends AbstractHelper
{
    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Msrp\Model\Product\Options
     */
    protected $productOptions;

    /**
     * @var \Magento\Msrp\Model\Config
     */
    protected $config;

    /**
     * @var \Magento\Framework\Pricing\PriceCurrencyInterface
     */
    protected $priceCurrency;

    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @param Context $context
     * @param StoreManagerInterface $storeManager
     * @param \Magento\Msrp\Model\Product\Options $productOptions
     * @param \Magento\Msrp\Model\Msrp $msrp
     * @param \Magento\Msrp\Model\Config $config
     * @param \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency
     * @param ProductRepositoryInterface $productRepository
     */
    public function __construct(
        Context $context,
        StoreManagerInterface $storeManager,
        \Magento\Msrp\Model\Product\Options $productOptions,
        \Magento\Msrp\Model\Msrp $msrp,
        \Magento\Msrp\Model\Config $config,
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency,
        ProductRepositoryInterface $productRepository
    ) {
        parent::__construct($context);
        $this->storeManager = $storeManager;
        $this->productOptions = $productOptions;
        $this->msrp = $msrp;
        $this->config = $config;
        $this->priceCurrency = $priceCurrency;
        $this->productRepository = $productRepository;
    }

    /**
     * Check if can apply Minimum Advertise price to product
     * in specific visibility
     *
     * @param int|Product $product
     * @param int|null $visibility Check displaying price in concrete place (by default generally)
     * @return bool
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    

    /**
     * @param int|Product $product
     * @return bool|float
     */
    public function demo()
    {
        echo "inside helper demo";
    }

    public function jsonToHtml($arr){
        foreach ($arr as $key => $value) {
            if(is_array($value) || is_object($value)){
                foreach ($value as $key1 => $value1) {
                    if(is_array($value1) || is_object($value1)){
                        foreach ($value1 as $key2 => $value2) {
                            if(is_array($value2) || is_object($value2)){
                                foreach ($value2 as $key3 => $value3) {
                                    if($value1 != '')
                                    echo "<tr> <td>".$key3."</td> <td> ".$value3."</td> </tr>";        
                                }
                            }else{
                                if($value1 != '')
                                echo "<tr> <td>".$key2."</td> <td> ".$value2."</td> </tr>";    
                            }
                        }
                    }else{
                        if($value1 != '')
                        echo "<tr> <td>".$key1."</td> <td> ".$value1."</td> </tr>";
                    } 
                }
            }else{
                // echo "<tr> <td>".$key."</td> <td> ".$value."</td> </tr>";
            }
        }

        // foreach($arr as $key => $value){
        //     if(is_object($value) || is_array($value)){
        //         echo $key." : ";
        //         foreach ($value as $key1 => $value1) {
        //             if(is_object($value) || is_array($value)){

        //             }
        //         }
        //     }else{
        //         if($value != '' && $key != 'product'){
        //             echo '<tr>';
        //             echo "<td>".$key."</td> <td> ".$value."</td></tr>";
        //         }
        //     }
        // }
    }
}
