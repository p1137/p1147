<?php
namespace Custom\Customer\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        if (version_compare($context->getVersion(), '1.0.1') < 0) {

	        $installer = $setup;
	        $installer->startSetup();
	        $connection = $installer->getConnection();
	        //cart table
	        $connection->addColumn(
	                $installer->getTable('quote_item'),
	                'json',
	                [
	                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
	                    'comment' =>'JSON DATA'
	                ]
	            );
	        // Order address table
	        $connection->addColumn(
	                $installer->getTable('sales_order_item'),
	                'json',
	                [
	                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
	                    'comment' =>'Json Data'
	                ]
	            );

	        //wishlist table
	        $connection->addColumn(
	                $installer->getTable('wishlist_item'),
	                'json',
	                [
	                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
	                    'comment' =>'JSON DATA'
	                ]
	            );

	        //Product table
	        $connection->addColumn(
	                $installer->getTable('catalog_product_entity'),
	                'wishlist_id',
	                [
	                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
	                    'comment' =>'Wishlist Id'
	                ]
	            );



	        $installer->endSetup(); 
    	}
    }
}