<?php
namespace Custom\Customer\Setup;

use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class UpgradeData implements UpgradeDataInterface {

	private $_eavSetupFactory;
	private $_attributeRepository;

	public function __construct(
		\Magento\Eav\Setup\EavSetupFactory $eavSetupFactory,
		\Magento\Eav\Model\AttributeRepository $attributeRepository
	)
	{
		$this->_eavSetupFactory = $eavSetupFactory;
		$this->_attributeRepository = $attributeRepository;
	}

	public function upgrade( ModuleDataSetupInterface $setup, ModuleContextInterface $context )
	{
		$eavSetup = $this->_eavSetupFactory->create(['setup' => $setup]);
		if (version_compare($context->getVersion(), '2.1.6', '<')) {
		

			$eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY, 'product_type', /* Custom Attribute Code */ [
            'group' => 'General', /* Group name in which you want 
              to display your custom attribute */
            'type' => 'int', /* Data type in which formate your value save in database */
            'backend' => '',
            'frontend' => '',
            'label' => 'Customized Product Type', /* lablel of your attribute */
            'input' => 'select',
            'class' => '',
            'source' => 'Custom\Customer\Model\Config\Source\ProductTypeOptions',
            /* Source of your select type custom attribute options */
            'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
            /* Scope of your attribute */
            'visible' => true,
            'required' => true,
            'user_defined' => false,
            'default' => '',
            'searchable' => false,
            'filterable' => false,
            'comparable' => false,
            'visible_on_front' => false,
            'used_in_product_listing' => true,
            'unique' => false
                ]
			);

			$eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY, 'product_type_gender', /* Custom Attribute Code */ [
            'group' => 'General', /* Group name in which you want 
              to display your custom attribute */
            'type' => 'int', /* Data type in which formate your value save in database */
            'backend' => '',
            'frontend' => '',
            'label' => 'Customized Product Gender', /* lablel of your attribute */
            'input' => 'select',
            'class' => '',
            'source' => 'Custom\Customer\Model\Config\Source\ProductTypeGenderOptions',
            /* Source of your select type custom attribute options */
            'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
            /* Scope of your attribute */
            'visible' => true,
            'required' => true,
            'user_defined' => false,
            'default' => '',
            'searchable' => false,
            'filterable' => false,
            'comparable' => false,
            'visible_on_front' => false,
            'used_in_product_listing' => true,
            'unique' => false
                ]

			);

		}
	}
}
