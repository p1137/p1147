<?php

namespace Custom\Customer\Setup;

use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallData implements InstallDataInterface {

    private $eavSetupFactory;

    public function __construct(EavSetupFactory $eavSetupFactory) {
        $this->eavSetupFactory = $eavSetupFactory;
    }

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context) {
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
        $eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY, 'customized_data', [
            'type' => 'text',
            'backend' => '',
            'group' => 'Product Details',
            'frontend' => '',
            'label' => 'Customized Data',
            'class' => '',
            'source' => '',
            'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
            'visible' => false,
            'required' => false,
            'user_defined' => false,
            'default' => '',
            'searchable' => false,
            'filterable' => false,
            'comparable' => false,
            'visible_on_front' => false,
            'used_in_product_listing' => false,
            'unique' => false
                ]
        );

        $eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY, 'product_type');
        $eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY, 'product_type', /* Custom Attribute Code */ [
            'group' => 'General', /* Group name in which you want 
              to display your custom attribute */
            'type' => 'int', /* Data type in which formate your value save in database */
            'backend' => '',
            'frontend' => '',
            'label' => 'Customized Product Type', /* lablel of your attribute */
            'input' => 'select',
            'class' => '',
            'source' => 'Custom\Customer\Model\Config\Source\ProductTypeOptions',
            /* Source of your select type custom attribute options */
            'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
            /* Scope of your attribute */
            'visible' => true,
            'required' => true,
            'user_defined' => false,
            'default' => '',
            'searchable' => false,
            'filterable' => false,
            'comparable' => false,
            'visible_on_front' => false,
            'used_in_product_listing' => true,
            'unique' => false
                ]
        );

        $eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY, 'product_type_gender');
        $eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY, 'product_type_gender', /* Custom Attribute Code */ [
            'group' => 'General', /* Group name in which you want 
              to display your custom attribute */
            'type' => 'int', /* Data type in which formate your value save in database */
            'backend' => '',
            'frontend' => '',
            'label' => 'Customized Product Gender', /* lablel of your attribute */
            'input' => 'select',
            'class' => '',
            'source' => 'Custom\Customer\Model\Config\Source\ProductTypeGenderOptions',
            /* Source of your select type custom attribute options */
            'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
            /* Scope of your attribute */
            'visible' => true,
            'required' => true,
            'user_defined' => false,
            'default' => '',
            'searchable' => false,
            'filterable' => false,
            'comparable' => false,
            'visible_on_front' => false,
            'used_in_product_listing' => true,
            'unique' => false
                ]
        );


        $eavSetup->addAttribute(
                \Magento\Catalog\Model\Category::ENTITY, 'is_product_customizable', [
            'type' => 'int',
            'label' => 'Is Product Customizable',
            'input' => 'boolean',
            'source' => 'Magento\Eav\Model\Entity\Attribute\Source\Boolean',
            'visible' => true,
            'default' => '0',
            'required' => false,
            'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
            'group' => 'Display Settings',
        ]);

        $eavSetup->addAttribute(
                \Magento\Catalog\Model\Category::ENTITY, 'customizable_url', [
            'type' => 'varchar',
            'label' => 'Customizable URL',
            'input' => 'text',
            'source' => '',
            'visible' => true,
            'default' => '0',
            'required' => false,
            'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
            'group' => 'Display Settings',
        ]);
    }

}
