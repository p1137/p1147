<?php

namespace Custom\Customer\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\Filesystem\DirectoryList;

class Createimg extends \Magento\Framework\App\Action\Action {

    protected $resultJsonFactory;
    protected $_filesystem;

    public function __construct(
    \Magento\Framework\Filesystem $filesystem, Context $context, \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory) {

        $this->resultJsonFactory = $resultJsonFactory;
        $this->_filesystem = $filesystem;
        parent::__construct($context);
    }

    public function execute() {
        $font = $this->_request->getParam('font');
        $pos = $this->_request->getParam('pos');
        $name = 'text.png';

        $str = 'convert -size 550X200 xc:transparent -font ' . $this->getMediaPath() . 'font_images/' . $font . ' -fill ' . $this->_request->getParam('color') . ' -pointsize 8.5 -draw "text 10,10' . "'" . $this->_request->getParam('txt') . "'" . '" -trim +repage ' . $this->getMediaPath() . 'txt_img/' . $name;
        exec($str);

        if ($pos == "highPos") {
            $str1 = 'composite -geometry +310+328 ' . $this->getMediaPath() . 'txt_img/' . $name . ' ' . $this->getMediaPath() . 'blanktest2.png ' . $this->getMediaPath() . 'txt_img/' . $name;
        } else if ($pos == "mediumPos") {
            $str1 = 'composite -geometry +310+450 ' . $this->getMediaPath() . 'txt_img/' . $name . ' ' . $this->getMediaPath() . 'blanktest2.png ' . $this->getMediaPath() . 'txt_img/' . $name;
        } elseif ($pos == "lowPos") {
            $str1 = 'composite -geometry +310+520 ' . $this->getMediaPath() . 'txt_img/' . $name . ' ' . $this->getMediaPath() . 'blanktest2.png ' . $this->getMediaPath() . 'txt_img/' . $name;
        } elseif ($pos == "cuffPos") {
            $str1 = 'composite -geometry +388+646 ' . $this->getMediaPath() . 'txt_img/' . $name . ' ' . $this->getMediaPath() . 'blanktest2.png ' . $this->getMediaPath() . 'txt_img/' . $name;
        } elseif ($pos == "collarPos") {
            $str1 = 'composite -geometry +226+195 ' . $this->getMediaPath() . 'txt_img/' . $name . ' ' . $this->getMediaPath() . 'blanktest2.png ' . $this->getMediaPath() . 'txt_img/' . $name;
        }
        if ($pos == "women_highPos") {
            $str1 = 'composite -geometry +294+316 ' . $this->getMediaPath() . 'txt_img/' . $name . ' ' . $this->getMediaPath() . 'blanktest2.png ' . $this->getMediaPath() . 'txt_img/' . $name;
        } else if ($pos == "women_mediumPos") {
            $str1 = 'composite -geometry +294+438 ' . $this->getMediaPath() . 'txt_img/' . $name . ' ' . $this->getMediaPath() . 'blanktest2.png ' . $this->getMediaPath() . 'txt_img/' . $name;
        } elseif ($pos == "women_lowPos") {
            $str1 = 'composite -geometry +294+508 ' . $this->getMediaPath() . 'txt_img/' . $name . ' ' . $this->getMediaPath() . 'blanktest2.png ' . $this->getMediaPath() . 'txt_img/' . $name;
        } elseif ($pos == "women_cuffPos") {
            $str1 = 'composite -geometry +388+646 ' . $this->getMediaPath() . 'txt_img/' . $name . ' ' . $this->getMediaPath() . 'blanktest2.png ' . $this->getMediaPath() . 'txt_img/' . $name;
        } elseif ($pos == "women_collarPos") {
            $str1 = 'composite -geometry +226+195 ' . $this->getMediaPath() . 'txt_img/' . $name . ' ' . $this->getMediaPath() . 'blanktest2.png ' . $this->getMediaPath() . 'txt_img/' . $name;
        }

        exec($str1);


        //return file_get_contents($this->getMediaPath().'txt_img/' . $name);
        echo file_get_contents($this->getMediaPath() . 'txt_img/' . $name);
        // return $this->resultJsonFactory->create()->setData($data);
    }

    public function getMediaPath() {
        return $this->_filesystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath();
    }

}
