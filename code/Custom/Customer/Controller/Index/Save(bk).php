<?php

namespace Custom\Customer\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Controller\ResultFactory;

class Save extends \Magento\Framework\App\Action\Action {

    protected $_resultPageFactory;
    protected $_cart;
    protected $_productRepositoryInterface;
    protected $_url;
    protected $_responseFactory;
    protected $_logger;
    protected $resultJsonFactory;

    public function __construct(
    Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory, \Magento\Checkout\Model\Cart $cart, \Magento\Catalog\Api\ProductRepositoryInterface $productRepositoryInterface, \Magento\Framework\App\ResponseFactory $responseFactory, \Psr\Log\LoggerInterface $logger, \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory) {

        $this->_resultPageFactory = $resultPageFactory;
        $this->_cart = $cart;
        $this->_productRepositoryInterface = $productRepositoryInterface;
        $this->_responseFactory = $responseFactory;
        $this->_url = $context->getUrl();
        $this->_logger = $logger;
        $this->_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->resource = $this->_objectManager->get('Magento\Framework\App\ResourceConnection');
        $this->connection = $this->resource->getConnection();
        $this->resultJsonFactory = $resultJsonFactory;

        parent::__construct($context);
    }

    public function execute() {

     	$productid = $this->_request->getParam('product_id');        
        $size_data = $this->_request->getParam('size_Data_Arr');

        $type = $size_data['type'];

        $qty = $size_data['qty'];

        $_product = $this->_productRepositoryInterface->getById($productid);

        $storeId = $this->_objectManager->get('Magento\Store\Model\StoreManagerInterface')->getStore()->getId();

        /* @var \Magento\Checkout\Model\Cart $cart */
        $cart = $this->_objectManager->get('\Magento\Checkout\Model\Cart');
        $price = $this->getRequest()->getParam('price');


        $params = array(
            'product' => $productid,
            'price'     => $price,
            'qty' => $qty
        );
        $this->_cart->addProduct($_product, $params);

         if ($this->_cart->save()) {
            $this->messageManager->addSuccessMessage('You added product to your shopping cart.');
            $data['status'] = 1;
            $data['message'] = "You added product to your shopping cart.";
        } else {
            $data['status'] = 0;
            $data['message'] = "Error while adding product to your shopping cart.Please try again.";
        }
        return $this->resultJsonFactory->create()->setData($data);
    }

}

