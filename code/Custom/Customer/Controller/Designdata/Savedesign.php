<?php

namespace Custom\Customer\Controller\Designdata;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Controller\ResultFactory;


class Savedesign extends \Magento\Framework\App\Action\Action {

    const TYPEMENSHIRT = "Shirt";
    const TYPEMENSUIT = "Suit";
    const TYPEWOMENSHIRT = "WomenShirt";
    const TYPEWOMENSUIT = "WomenSuit";


    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    private $resultPageFactory;
    protected $_menShirtFabricCollectionFactory;
    protected $_womenShirtFabricCollectionFactory;
    protected $_menSuitFabricCollectionFactory;
    protected $_womenSuitFabricCollectionFactory;

    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
    \Magento\Framework\App\Action\Context $context, 
    \Magento\Framework\App\RequestInterface $request,
    \Wholesaler\Mgmt\Model\Design $design,
    \Wholesaler\Mgmt\Model\DesignFactory $designfactory,
    \Magento\Customer\Model\Session $customerSession,
    \Magento\Framework\View\Result\PageFactory $resultPageFactory,
    \Shirt\Tool\Model\ResourceModel\ShirtShirtfabric\CollectionFactory $menShirtFabricCollectionFactory, 
    \Shirt\Shirtfabricwomen\Model\ResourceModel\Shirtfabricwomen\CollectionFactory $womenShirtFabricCollectionFactory, 
    \Suit\Womenjacket\Model\ResourceModel\Womenjacket\CollectionFactory $womenSuitJacketFabricCollectionFactory, 
    \Suit\Womenpant\Model\ResourceModel\Womenpant\CollectionFactory $womenSuitPantFabricCollectionFactory, 
    \Suit\Womenvest\Model\ResourceModel\Womenvest\CollectionFactory $womenSuitVestFabricCollectionFactory, 
    \Suit\Vest\Model\ResourceModel\Vest\CollectionFactory $menSuitVestFabricCollectionFactory, 
    \Suit\Tool\Model\ResourceModel\Tool\CollectionFactory $menSuitJacketFabricCollectionFactory, 
    \Suit\Pant\Model\ResourceModel\Pant\CollectionFactory $menSuitPantFabricCollectionFactory)
     {
        $this->_request = $request;
        $this->design = $design;
        $this->designfactory = $designfactory;
        $this->_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->_menShirtFabricCollectionFactory = $menShirtFabricCollectionFactory;
        $this->_womenShirtFabricCollectionFactory = $womenShirtFabricCollectionFactory;
        $this->_menSuitJacketFabricCollectionFactory = $menSuitJacketFabricCollectionFactory;
        $this->_womenSuitJacketFabricCollectionFactory = $womenSuitJacketFabricCollectionFactory;
        $this->_menSuitVestFabricCollectionFactory = $menSuitVestFabricCollectionFactory;
        $this->_womenSuitVestFabricCollectionFactory = $womenSuitVestFabricCollectionFactory;
        $this->_menSuitPantFabricCollectionFactory = $menSuitPantFabricCollectionFactory;
        $this->_womenSuitPantFabricCollectionFactory = $womenSuitPantFabricCollectionFactory;
        $this->resultPageFactory = $resultPageFactory;
        $this->customerSession = $customerSession;
        parent::__construct($context);
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute() {

        $data = $this->_request->getPost();
        $wholesalerId = $this->customerSession->getCustomer()->getId();
        $product_type = $data['productType'];
        $styleData = $this->_request->getParam("style_data");

        if($product_type=="Shirt" || $product_type=="WomenShirt")
        { 

          $FinalDisplayStyleData = $this->saveShirt();
          $json_img = $data['prodImg'];
        }else{
          $FinalDisplayStyleData = $this->saveSuit();
          $img = $this->_request->getParam('final_image_data');
          $json_img = $img[1]['dataurl'];
        }


        $image = str_replace('data:image/png;base64,', '', $json_img);
        $name = 'img_' . time();

        $directory = $this->_objectManager->get('\Magento\Framework\Filesystem\DirectoryList');

        $fp = fopen($directory->getRoot() . "/pub/media/design-image/" . $name . ".png", "w+");

        fwrite($fp, base64_decode($image));
        fclose($fp);

        $product_image = "pub/media/design-image/" . $name . ".png";

        $model = $this->designfactory->create();

        $model->setJson(
                json_encode(array('product' => $product_image, 'style' => $styleData, 'display_style' => $FinalDisplayStyleData, 'type' => $product_type))
        );
        $model->setProductType($product_type);
        $model->setWholesalerId($wholesalerId);
        $model->save();

       return $this->resultPageFactory->create();
    }

    protected function saveShirt() {
        $DisplayStyleData = array();
        if ($this->_request->getParam("style_data") != "") {
            $styleData = $this->_request->getParam("style_data");
            /* Remove spaces from key */
            foreach ($styleData as $style => $data) {
                $finalStyleData[trim($style)] = $data;
            }
            /* Format the style for display purpose */
            foreach ($finalStyleData as $style => $data) {
                $style = trim($style);
                switch ($style) {
                    case 'Fit':
                        $DisplayStyleData['fit']['label'] = "Fit";
                        $DisplayStyleData['fit']['value'] = $data;
                        break;
                    case 'Pocket':
                        $DisplayStyleData['pocket']['label'] = "Pocket";
                        $DisplayStyleData['pocket']['value'] = $data;
                        break;
                    case 'Plackets':
                        $DisplayStyleData['placket']['label'] = 'Placket';
                        $DisplayStyleData['placket']['value'] = $data;
                        break;
                    case 'Bottom':
                        $DisplayStyleData['bottom']['label'] = 'Bottom';
                        $DisplayStyleData['bottom']['value'] = $data;
                        break;
                    case 'Sleeves':
                        $DisplayStyleData['sleeve']['label'] = 'Sleeve';
                        $DisplayStyleData['sleeve']['value'] = $data;
                        break;
                    case 'Cuffs':
                        $DisplayStyleData['cuff']['label'] = 'Cuff';
                        $DisplayStyleData['cuff']['value'] = $data;
                        break;
                    case 'CollarStyle':
                        $DisplayStyleData['collar']['label'] = 'Collar';
                        $DisplayStyleData['collar']['value'] = $data;
                        break;
                    case 'Pleats':
                        $DisplayStyleData['pleats']['label'] = 'Pleats';
                        $DisplayStyleData['pleats']['value'] = $data;
                        break;
                    case 'ShirtFabricId':
                        $DisplayStyleData['fabric']['label'] = 'Fabric';
                        $DisplayStyleData['fabric']['value'] = $this->getFabricData($data);
                        break;
                    case 'ChestpleatsContrast':
                        $DisplayStyleData['accent']['cheastpleats']['label'] = 'Accent Cheast Pleats';
                        $DisplayStyleData['accent']['cheastpleats']['value'] = $data;
                        break;
                    case 'Monogram':
                        $DisplayStyleData['accent']['monogram']['label'] = 'Monogram';
                        $monogramData = explode(" ", $data);
                        foreach ($monogramData as $monogram) {
                            $styles = explode(":", $monogram);
                            $DisplayStyleData['accent']['monogram']['value'][$styles[0]] = $styles[1];
                        }
                        break;
                    case 'CollarStyleContrast':
                        $DisplayStyleData['accent']['collar']['label'] = 'Accent Collar';
                        $DisplayStyleData['accent']['collar']['value'] = $data;
                        if (array_key_exists('CollarStylefabricId', $finalStyleData))
                            $DisplayStyleData['accent']['collar']['fabric'] = $this->getFabricData($finalStyleData['CollarStylefabricId']);
                        break;
                    case 'CuffsContrast':
                        $DisplayStyleData['accent']['cuff']['label'] = 'Accent Cuff';
                        $DisplayStyleData['accent']['cuff']['value'] = $data;
                        if (array_key_exists('CuffsfabricId', $finalStyleData))
                            $DisplayStyleData['accent']['cuff']['fabric'] = $this->getFabricData($finalStyleData['CuffsfabricId']);
                        break;
                    case 'ThreadsContrast':
                        $DisplayStyleData['accent']['thread']['label'] = 'Accent Button Thread Color';
                        $DisplayStyleData['accent']['thread']['value'] = $data;
                        break;
                    case 'ElbowPatchContrast':
                        $DisplayStyleData['accent']['elbowpatch']['label'] = 'Accent Elbowpatch';
                        $DisplayStyleData['accent']['elbowpatch']['value'] = $data;
                        if (array_key_exists('ElbowPatchfabricId', $finalStyleData))
                            $DisplayStyleData['accent']['elbowpatch']['fabric'] = $this->getFabricData($finalStyleData['ElbowPatchfabricId']);
                        break;
                    case 'PlacketsContrast':
                        $DisplayStyleData['accent']['placket']['label'] = 'Accent Placket';
                        $DisplayStyleData['accent']['placket']['value'] = $data;
                        if (array_key_exists('PlacketsfabricId', $finalStyleData))
                            $DisplayStyleData['accent']['placket']['fabric'] = $this->getFabricData($finalStyleData['PlacketsfabricId']);
                        break;
                }
            }
        }
        $FinalDisplayStyleData['shirt'] = $DisplayStyleData;

        return $FinalDisplayStyleData;
    }

    protected function saveSuit() {
        if ($this->_request->getParam("style_data") != "") {
            $styleData = $this->_request->getParam("style_data");
            /* Remove spaces from key */
            foreach ($styleData as $style => $data) {
                $finalStyleData[trim($style)] = $data;
            }
            /* Format the style for display purpose */
            foreach ($finalStyleData as $style => $data) {
                $style = trim($style);
                switch ($style) {
                    case 'pantfit':
                        $DisplayStyleData['pant']['fit']['label'] = "Fit";
                        $DisplayStyleData['pant']['fit']['value'] = $data;
                        break;
                    case 'pantpockets':
                        $DisplayStyleData['pant']['pocket']['label'] = "Pocket";
                        $DisplayStyleData['pant']['pocket']['value'] = $data;
                        break;
                    case 'pantbackpockets':
                        $DisplayStyleData['pant']['backpocket']['label'] = "Back Pocket";
                        $DisplayStyleData['pant']['backpocket']['value'] = $data;
                        break;
                    case 'pantpleats':
                        $DisplayStyleData['pant']['pleats']['label'] = 'Pleats';
                        $DisplayStyleData['pant']['pleats']['value'] = $data;
                        break;
                    case 'fastening':
                        $DisplayStyleData['pant']['fastening']['label'] = 'Fastening';
                        $DisplayStyleData['pant']['fastening']['value'] = $data;
                        break;
                    case 'pantcuff':
                        $DisplayStyleData['pant']['cuff']['label'] = 'Cuff';
                        $DisplayStyleData['pant']['cuff']['value'] = $data;
                        break;
                    case 'pantCuffSize':
                        $DisplayStyleData['pant']['cuffsize']['label'] = 'Cuff Size';
                        $DisplayStyleData['pant']['cuffsize']['value'] = $data;
                        break;
                    case 'fasteningButton':
                        $DisplayStyleData['pant']['fasteningbutton']['label'] = 'Fastening Button';
                        $DisplayStyleData['pant']['fasteningbutton']['value'] = $data;
                        break;
                    case 'beltloop':
                        $DisplayStyleData['pant']['beltloop']['label'] = 'Beltloop';
                        $DisplayStyleData['pant']['beltloop']['value'] = $data;
                        break;
                    case 'veststyle':
                        $DisplayStyleData['vest']['style']['label'] = 'Style';
                        $DisplayStyleData['vest']['style']['value'] = $data;
                        break;
                    case 'vestlapel':
                        $DisplayStyleData['vest']['lapel']['label'] = 'Lapel';
                        $DisplayStyleData['vest']['lapel']['value'] = $data;
                        break;
                    case 'vestedge':
                        $DisplayStyleData['vest']['edge']['label'] = 'Edge';
                        $DisplayStyleData['vest']['edge']['value'] = $data;
                        break;
                    case 'vestpockets':
                        $DisplayStyleData['vest']['pocket']['label'] = 'Pocket';
                        $DisplayStyleData['vest']['pocket']['value'] = $data;
                        break;
                    case 'breastpocket':
                        $DisplayStyleData['vest']['breastpocket']['label'] = 'Breast Pocket';
                        $DisplayStyleData['vest']['breastpocket']['value'] = $data;
                        break;
                    // case 'ticketpockets':
                    //     $DisplayStyleData['vest']['ticketpockets']['label'] = 'Ticket Pocket';
                    //     $DisplayStyleData['vest']['ticketpockets']['value'] = $data;
                    //     break;
                    case 'vestbuttons':
                        $DisplayStyleData['vest']['button']['label'] = 'Button';
                        $DisplayStyleData['vest']['button']['value'] = $data;
                        break;
                    case 'Style':
                        $DisplayStyleData['jacket']['style']['label'] = 'Style';
                        $DisplayStyleData['jacket']['style']['value'] = $data;
                        break;
                    case 'Pockets':
                        $DisplayStyleData['jacket']['pocket']['label'] = 'Pocket';
                        $DisplayStyleData['jacket']['pocket']['value'] = $data;
                        break;
                    case 'Fit':
                        $DisplayStyleData['jacket']['fit']['label'] = 'Fit';
                        $DisplayStyleData['jacket']['fit']['value'] = $data;
                        break;
                    case 'Lapel':
                        $DisplayStyleData['jacket']['lapel']['label'] = 'Lapel';
                        $DisplayStyleData['jacket']['lapel']['value'] = $data;
                        break;
                    case 'ButtonsColor':
                        $DisplayStyleData['jacket']['buttoncolor']['label'] = 'Button Color';
                        $DisplayStyleData['jacket']['buttoncolor']['value'] = $data;
                        break;
                    case 'backbeltloop':
                        $DisplayStyleData['jacket']['backbeltloop']['label'] = 'Back Beltloop';
                        $DisplayStyleData['jacket']['backbeltloop']['value'] = $data;
                        break;
                    case 'FitBack':
                        $DisplayStyleData['jacket']['fitback']['label'] = 'Back Fit';
                        $DisplayStyleData['jacket']['fitback']['value'] = $data;
                        break;
                    case 'Vents':
                        $DisplayStyleData['jacket']['vent']['label'] = 'Vent';
                        $DisplayStyleData['jacket']['vent']['value'] = $data;
                        break;
                    case 'sleeve_buttons':
                        $DisplayStyleData['jacket']['sleevebutton']['label'] = 'Sleeve Button';
                        $DisplayStyleData['jacket']['sleevebutton']['value'] = $data;
                        break;
                    case 'sleeveButtonsHoles':
                        $DisplayStyleData['jacket']['sleevebuttonholes']['label'] = 'Sleeve Button Holes';
                        $DisplayStyleData['jacket']['sleevebuttonholes']['value'] = $data;
                        break;
                    case 'vestFabricId':
                        $DisplayStyleData['vest']['fabric']['label'] = 'Fabric';
                        $DisplayStyleData['vest']['fabric']['value'] = $this->getSuitFabricData('vest', $data);
                        break;
                    case 'pantFabricId':
                        $DisplayStyleData['pant']['fabric']['label'] = 'Fabric';
                        $DisplayStyleData['pant']['fabric']['value'] = $this->getSuitFabricData('pant', $data);
                        break;
                    case 'jacketFabricId':
                        $DisplayStyleData['jacket']['fabric']['label'] = 'Fabric';
                        $DisplayStyleData['jacket']['fabric']['value'] = $this->getSuitFabricData('jacket', $data);
                        break;
                    case 'backcollar':
                        $DisplayStyleData['jacket']['accent']['backcollar']['label'] = 'Accent Back Collar';
                        $DisplayStyleData['jacket']['accent']['backcollar']['value'] = $data;
                        break;
                    case 'backcollaraccent':
                        $DisplayStyleData['jacket']['accent']['backcollarfabric']['label'] = 'Back Collar Fabric';
                        $DisplayStyleData['jacket']['accent']['backcollarfabric']['value'] = $data;
                        break;
                    case 'threads':
                        $DisplayStyleData['jacket']['accent']['thread']['label'] = 'Accent Button Thread';
                        $DisplayStyleData['jacket']['accent']['thread']['value'] = $data;
                        break;
                    case 'buttons':
                        $DisplayStyleData['jacket']['accent']['button']['label'] = 'Accent Buttons';
                        $DisplayStyleData['jacket']['accent']['button']['value'] = $data;
                        break;
                    case 'pocketsquare':
                        $DisplayStyleData['jacket']['accent']['pocketsquare']['label'] = 'Accent Pocket Square';
                        $DisplayStyleData['jacket']['accent']['pocketsquare']['value'] = $data;
                        break;
                    case 'Tie':
                        $DisplayStyleData['jacket']['accent']['tie']['label'] = 'Accent Tie';
                        $DisplayStyleData['jacket']['accent']['tie']['value'] = $data;
                        break;
                    case 'ManBow':
                        $DisplayStyleData['jacket']['accent']['bow']['label'] = 'Accent Bow';
                        $DisplayStyleData['jacket']['accent']['bow']['value'] = $data;
                        break;
                    case 'liningStyle':
                        $DisplayStyleData['jacket']['accent']['liningstyle']['label'] = 'Lining Style';
                        $DisplayStyleData['jacket']['accent']['liningstyle']['value'] = $data;
                        break;
                    case 'liningFabric':
                        $DisplayStyleData['jacket']['accent']['liningfabric']['label'] = 'Lining Fabric';
                        $DisplayStyleData['jacket']['accent']['liningfabric']['value'] = $data;
                        break;
                    case 'backelbow':
                        $DisplayStyleData['jacket']['accent']['elbow']['label'] = 'Back Elbow';
                        $DisplayStyleData['jacket']['accent']['elbow']['value'] = $data;
                        break;
                    case 'tuxedoFabric':
                        $DisplayStyleData['jacket']['tuxedofabric']['label'] = 'Tuxedo Fabric';
                        $DisplayStyleData['jacket']['tuxedofabric']['value'] = $data;
                        break;
                    case 'TuxedoStyle':
                        $DisplayStyleData['jacket']['tuxedostyle']['label'] = 'Tuxedo Style';
                        $DisplayStyleData['jacket']['tuxedostyle']['value'] = $data;
                        break;
                    case 'waistband':
                        $DisplayStyleData['pant']['waistband']['label'] = 'Waistband';
                        $DisplayStyleData['pant']['waistband']['value'] = $data;
                        break;
                    case 'Monogram':
                        $DisplayStyleData['jacket']['accent']['monogram']['label'] = 'Monogram';
                        $monogramData = explode(" ", $data);
                        foreach ($monogramData as $monogram) {
                            $styles = explode(":", $monogram);
                            $DisplayStyleData['jacket']['accent']['monogram']['value'][$styles[0]] = $styles[1];
                        }
                        break;
                }
            }
            $FinalDisplayStyleData['jacket']= (isset($DisplayStyleData['jacket']))?$DisplayStyleData['jacket']:'';
            $FinalDisplayStyleData['pant']= (isset($DisplayStyleData['pant']))?$DisplayStyleData['pant']:'';
            $FinalDisplayStyleData['vest']= (isset($DisplayStyleData['vest']))?$DisplayStyleData['vest']:'';
        }
            return $FinalDisplayStyleData;


    }

    protected function getFabricData($fabricId) {
        $mediaPath = $this->_objectManager->get('Magento\Store\Model\StoreManagerInterface')
                ->getStore()
                ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        $StyleData = array();
        $type = $this->_request->getParam('productType');
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        if ($type == self::TYPEMENSHIRT) {
            $imageUrl = "shirt-tool/"; //append base url
            $shirtCollection = $this->_menShirtFabricCollectionFactory->create();
            $shirtCollection->addFieldToFilter('fabric_id', $fabricId);
        } else if ($type == self::TYPEWOMENSHIRT) {
            $imageUrl = "shirt-tool/"; //append base url
            $shirtCollection = $this->_womenShirtFabricCollectionFactory->create();
            $shirtCollection->addFieldToFilter('shirtfabricwomen_id', $fabricId);
        }
        if (isset($shirtCollection)) {
            foreach ($shirtCollection as $shirt) {
                $StyleData['id'] = $fabricId;
                $StyleData['name'] = $shirt->getTitle();
                if($type == self::TYPEMENSHIRT){
                    $StyleData['image'] = $mediaPath . "shirt-tool/" . $shirt->getDisplayFabricThumb(); 
                }
                else{
                    $StyleData['image'] = $mediaPath . $shirt->getDisplayFabricThumb(); 
                }
            }
        }

        return $StyleData;
    }

    protected function getSuitFabricData($suitType, $fabricId) {
        $mediaPath = $this->_objectManager->get('Magento\Store\Model\StoreManagerInterface')
                ->getStore()
                ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        $StyleData = array();
        $type = $this->_request->getParam('productType');
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        if ($type == self::TYPEMENSUIT) {
            $imageUrl = ""; //append base url
            if ($suitType == 'jacket') {
                $shirtCollection = $this->_menSuitJacketFabricCollectionFactory->create();
                $shirtCollection->addFieldToFilter('id', $fabricId);
            } elseif ($suitType == 'vest') {
                $shirtCollection = $this->_menSuitVestFabricCollectionFactory->create();
                $shirtCollection->addFieldToFilter('id', $fabricId);
            } elseif ($suitType == 'pant') {
                $shirtCollection = $this->_menSuitPantFabricCollectionFactory->create();
                $shirtCollection->addFieldToFilter('id', $fabricId);
            }
        } else if ($type == self::TYPEWOMENSUIT) {
            $imageUrl = ""; //append base url
            if ($suitType == 'jacket') {
                $shirtCollection = $this->_womenSuitJacketFabricCollectionFactory->create();
                $shirtCollection->addFieldToFilter('id', $fabricId);
            } elseif ($suitType == 'vest') {
                $shirtCollection = $this->_womenSuitVestFabricCollectionFactory->create();
                $shirtCollection->addFieldToFilter('id', $fabricId);
            } elseif ($suitType == 'pant') {
                $shirtCollection = $this->_womenSuitPantFabricCollectionFactory->create();
                $shirtCollection->addFieldToFilter('id', $fabricId);
            }
        }
        if (isset($shirtCollection)) {
            foreach ($shirtCollection as $shirt) {
                $StyleData['id'] = $fabricId;
                $StyleData['name'] = $shirt->getName();
                $StyleData['image'] = $mediaPath . $shirt->getDisplayFabricThumb();
            }
        }
        return $StyleData;
    }

}
