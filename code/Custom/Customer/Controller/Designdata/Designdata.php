<?php

namespace Custom\Customer\Controller\Designdata;

class Designdata extends \Magento\Framework\App\Action\Action {

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    private $resultPageFactory;
    private $quoteItemFactory;
    private $itemResourceModel;

    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
    \Magento\Framework\App\Action\Context $context, 
    \Magento\Framework\View\Result\PageFactory $resultPageFactory, 
    \Magento\Framework\Controller\Result\JsonFactory $jsonFactory, 
    \Magento\Quote\Model\Quote\ItemFactory $quoteItemFactory, 
    \Magento\Quote\Model\ResourceModel\Quote\Item $itemResourceModel, 
    \Magento\Sales\Api\OrderItemRepositoryInterface $orderItemRepository, 
    \Magento\Quote\Model\QuoteFactory $quoteFactory, 
    \Wholesaler\Mgmt\Model\DesignFactory $designfactory,
    \Wholesaler\Mgmt\Model\StandardMeasurementFactory $standardfactory,
    \Magento\Customer\Model\Session $customerSession,
    \Wholesaler\Mgmt\Model\MeasurementFactory $measurementfactory,
    \Magento\Wishlist\Model\ItemFactory $itemFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->_designfactory = $designfactory;
        $this->jsonFactory = $jsonFactory;
        $this->quoteItemFactory = $quoteItemFactory;
        $this->itemResourceModel = $itemResourceModel;
        $this->orderItemRepository = $orderItemRepository;
        $this->quoteFactory = $quoteFactory;
        $this->_standardfactory = $standardfactory;
        $this->_customerSession = $customerSession;
        $this->measurementfactory = $measurementfactory;
        $this->itemFactory = $itemFactory;

        parent::__construct($context);
    }

    public function isWholesaler() {
        // $customerGroup = $this->_customerSession->getCustomer()->getGroupId();

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerSession = $objectManager->create('Magento\Customer\Model\Session');
        $customerGroup = $customerSession->getCustomer()->getGroupId();


        if ($customerGroup == '2') {

            return "wholesaler";
        } else {
            return "customer";
        }
    }

    public function getBodyMeasurements() {

        try {

            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $customerSession = $objectManager->create('Magento\Customer\Model\Session');
   
            $cust_id = $customerSession->getCustomer()->getId();

            // $cust_id = $this->_customerSession->getCustomer()->getId();
            $sample = $this->measurementfactory->create();
            $measurementArray = array();

            $collection = $sample->getCollection()->addFieldToFilter('customer_id', array('eq' => $cust_id));
            foreach ($collection as $item) {
                $cust_data = $item->getData();

                $measurementArray['measurement_id'] = $cust_data['measurement_id'];
                $measurementArray['customer_id'] = $cust_id;
                $measurementArray['json'] = $cust_data['json'];

            }
            if ($measurementArray != NULL) {
                return $measurementArray;
            } else {
                $measurementArray['measurement_id'] = '';
                $measurementArray['customer_id'] = $cust_id;
                $measurementArray['json'] = '';
                return $measurementArray;
            }
        } catch (\Exception $e) {
            return __('You have not set your body measurements.');
        }
    }

    public function getLastOrder(){

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerSession = $objectManager->create('Magento\Customer\Model\Session');
        $cust_id = $customerSession->getCustomer()->getId();

        $orderDatamodel = $objectManager->get('Magento\Sales\Model\Order')->getCollection()->addFieldToFilter('customer_id',$cust_id)->getLastItem();
        $orderId   =   $orderDatamodel->getId();
        $order = $objectManager->create('\Magento\Sales\Model\Order')->load($orderId);
        $orderItems = $order->getAllItems();
        $orderData = array();

        foreach ($orderItems as $item) { 
            if($item->getJson()!=''){

                $json = json_decode($item->getJson(),true);
                if(array_key_exists('rtw_measurement', $json)){

                    if($json['rtw_measurement']['product_type']=='Shirt'){
                        $shirtLastData = $json;
                        $orderData['Shirt'] = $shirtLastData; 
                    }
                    if($json['rtw_measurement']['product_type']=='WomenShirt'){
                        $womenshirtLastData = $json;
                        $orderData['WomenShirt'] = $womenshirtLastData; 
                    }
                    if($json['rtw_measurement']['product_type']=='Suit'){
                        $suitLastData = $json;
                        $orderData['Suit'] = $suitLastData; 
                    }
                    if($json['rtw_measurement']['product_type']=='WomenSuit'){
                        $womensuitLastData = $json;
                        $orderData['WomenSuit'] = $womensuitLastData; 
                    }

                }else{

                    if($json['type']=='Shirt'){
                        $shirtLastData = $json;
                        $orderData['Shirt'] = $shirtLastData; 
                    }
                    if($json['type']=='WomenShirt'){
                        $womenshirtLastData = $json;
                        $orderData['WomenShirt'] = $womenshirtLastData; 
                    }
                    if($json['type']=='Suit'){
                        $suitLastData = $json;
                        $orderData['Suit'] = $suitLastData; 
                    }
                    if($json['type']=='WomenSuit'){
                        $womensuitLastData = $json;
                        $orderData['WomenSuit'] = $womensuitLastData; 
                    }

                }

            }

        }
        return $orderData;

    }

        /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute() {

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
        $baseUrl = $storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_WEB);

        $data = $this->getRequest()->getPostValue();
        $item_id = $data['id'];

        if ($data['reqType'] == 'cart') {
            $html = "";
            $mhtml = "";
            $quoteItem = $this->quoteItemFactory->create();
            $collection = $quoteItem->getCollection()->addFieldToFilter('item_id', array('eq' => $item_id));
            $arr = array();
            foreach ($collection as $item) {
                $json = json_decode($item->getJson(), true);
                if($json['style']['mesurementIn']=='in'){
                    $json['style']['mesurementIn'] = 'inch';
                }

                $arr['img'] = $baseUrl . $json['product'];
                $temp['styles'] = $json['display_style'];
                $arr['styles'] = $temp;
                $type = $json['size']['type'];
                $p_type = $json['type'];

                
                if($type == 'Standard' && ($p_type == "Shirt" || $p_type == "WomenShirt")){

                    $mhtml .= "<tr style='background-color: #b38d4a;color: white;'><td colspan=2 style='text-align: center;text-transform: uppercase;'><strong> Shirt (".$json['style']['mesurementIn'].")</strong></td></tr>";
                    $measurementVal = $json['size']['sizeData'][0];

                        $mhtml .= "<tr><td>Size</td><td>".$measurementVal['size_name']."</td></tr>";
                        $mhtml .= "<tr><td>Collar</td><td>".$measurementVal['Collar']."</td></tr>";
                        $mhtml .= "<tr><td>Shoulder</td><td>".$measurementVal['shoulder']."</td></tr>";
                        $mhtml .= "<tr><td>Length</td><td>".$measurementVal['length']."</td></tr>";
                        $mhtml .= "<tr><td>Sleeve</td><td>".$measurementVal['sleeve']."</td></tr>";
                        $mhtml .= "<tr><td>Chest</td><td>".$measurementVal['chest']."</td></tr>";
                        $mhtml .= "<tr><td>Stomach</td><td>".$measurementVal['stomach']."</td></tr>";
                        $mhtml .= "<tr><td>Seat</td><td>".$measurementVal['seat']."</td></tr>";
                        $mhtml .= "<tr><td>Bicep Width</td><td>".$measurementVal['Bicep_Width']."</td></tr>";
                        $mhtml .= "<tr><td>Wrist Width</td><td>".$measurementVal['Wrist_width']."</td></tr>";
                }

                else if($type == 'Scan' && ($p_type == "Shirt" || $p_type == "WomenShirt")){

                    $mhtml .= "<tr style='background-color: #b38d4a;color: white;'><td colspan=2 style='text-align: center;text-transform: uppercase;'><strong> Shirt (".$json['style']['mesurementIn'].")</strong></td></tr>";
                    $measurementVal = $json['size']['sizeData'];

                        $mhtml .= "<tr><td>Collar</td><td>".$measurementVal['collar']."</td></tr>";
                        $mhtml .= "<tr><td>Shoulder</td><td>".$measurementVal['shoulder']."</td></tr>";
                        $mhtml .= "<tr><td>Length</td><td>".$measurementVal['length']."</td></tr>";
                        $mhtml .= "<tr><td>Sleeve</td><td>".$measurementVal['sleeve']."</td></tr>";
                        $mhtml .= "<tr><td>Chest</td><td>".$measurementVal['chest']."</td></tr>";
                        $mhtml .= "<tr><td>Stomach</td><td>".$measurementVal['stomach']."</td></tr>";
                        $mhtml .= "<tr><td>Seat</td><td>".$measurementVal['seat']."</td></tr>";
                        $mhtml .= "<tr><td>Bicep Width</td><td>".$measurementVal['BicepWidth']."</td></tr>";
                        $mhtml .= "<tr><td>Wrist Width</td><td>".$measurementVal['Wristwidth']."</td></tr>";
                }
               
                else if($type == 'Standard' && ($p_type == "Suit" || $p_type == "WomenSuit")){

                    if(isset($json['size']['sizeData'][0])){
                        $measurementJacket = $json['size']['sizeData'][0];
                        $mhtml .= "<tr style='background-color: #b38d4a;color: white;'><td colspan=2 style='text-align: center;text-transform: uppercase;'><strong> Jacket (".$json['style']['mesurementIn'].")</strong></td></tr>";

                        foreach ($measurementJacket as $jkey => $jval) {
                           if($jkey=='id' || $jkey=='parent' || $jkey=='product_type' || $jkey=='gender'){

                            }
                            elseif($jkey=='size_name'){
                                $mhtml .= "<tr><td> Size </td><td>".$measurementJacket['size_name']."</td></tr>";
                            }
                            else{
                                $mhtml .= "<tr><td>".$jkey."</td><td>".$jval."</td></tr>";
                            }
                             
                        }
                    }

                    if(isset($json['size']['sizeData'][1])){
                        $measurementPant = $json['size']['sizeData'][1];
                        $mhtml .= "<tr style='background-color: #b38d4a;color: white;'><td colspan=2 style='text-align: center;text-transform: uppercase;'><strong> Pant (".$json['style']['mesurementIn'].")</strong></td></tr>";
                        foreach ($measurementPant as $pkey => $pval) {
                           if($pkey=='id' || $pkey=='parent' || $pkey=='product_type' || $pkey=='gender'){

                            }
                            elseif($pkey=='size_name'){
                                $mhtml .= "<tr><td> Size </td><td>".$measurementPant['size_name']."</td></tr>";
                            }
                            else{
                                $mhtml .= "<tr><td>".$pkey."</td><td>".$pval."</td></tr>";
                            }
                             
                        }
                    }

                    if(isset($json['size']['sizeData'][2])){
                        $measurementVest = $json['size']['sizeData'][2];
                        $mhtml .= "<tr style='background-color: #b38d4a;color: white;'><td colspan=2 style='text-align: center;text-transform: uppercase;'><strong> Vest (".$json['style']['mesurementIn'].")</strong></td></tr>";
                        foreach ($measurementVest as $vkey => $vval) {
                           if($vkey=='id' || $vkey=='parent' || $vkey=='product_type' || $vkey=='gender'){

                            }
                            elseif($vkey=='size_name'){
                                $mhtml .= "<tr><td> Size </td><td>".$measurementVest['size_name']."</td></tr>";
                            }
                            else{
                                $mhtml .= "<tr><td>".$vkey."</td><td>".$vval."</td></tr>";
                            }
                             
                        }
                    }
                }

                else if($type == 'Scan' && ($p_type == "Suit" || $p_type == "WomenSuit")){
                    $mhtml= "";
                    $measurementData = $json['size']['sizeData'];

                    if(isset($measurementData['chest']))
                        $mhtml .= "<tr style='background-color: #b38d4a;color: white;'><td colspan=2 style='text-align: center;text-transform: uppercase;'><strong> Jacket (".$json['style']['mesurementIn'].")</strong></td></tr>";

                    if(isset($measurementData['chest']))
                    $mhtml .= "<tr><td>Chest</td><td>".@$measurementData['chest']."</td></tr>";

                    if(isset($measurementData['stomach']))
                        $mhtml .= "<tr><td>Stomach</td><td>".@$measurementData['stomach']." </td></tr>";

                    if(isset($measurementData['sleeve_length']))
                        $mhtml .= "<tr><td>Sleeve Length</td><td>".@$measurementData['sleeve_length']."</td></tr>";

                    if(isset($measurementData['shoulder']))
                        $mhtml .= "<tr><td>Shoulder</td><td>".@$measurementData['shoulder']."</td></tr>";

                    if(isset($measurementData['frontJacketLength']))
                        $mhtml .= "<tr><td>Front Jacket Length</td><td>".@$measurementData['frontJacketLength']."</td></tr>";

                    if(isset($measurementData['backJacketLength']))
                        $mhtml .= "<tr><td>Back Jacket Length</td><td>".@$measurementData['backJacketLength']."</td></tr>";

                    if(isset($measurementData['bicepWidth']))
                        $mhtml .= "<tr><td>Bicep Width</td><td>".@$measurementData['bicepWidth']."</td></tr>";
 
                    if(isset($measurementData['wristWidth']))
                        $mhtml .= "<tr><td>Wrist Width</td><td>".@$measurementData['wristWidth']."</td></tr>";

                    if(isset($measurementData['seat']))
                        $mhtml .= "<tr><td>Seat</td><td>".@$measurementData['seat']."</td></tr>";

                    if(isset($measurementData['trouser_waist']))
                        $mhtml .= "<tr style='background-color: #b38d4a;color: white;'><td colspan=2 style='text-align: center;text-transform: uppercase;'><strong> Pant (".$json['style']['mesurementIn'].")</strong></td></tr>";

                    if(isset($measurementData['trouser_waist']))
                        $mhtml .= "<tr><td>Trouser Waist</td><td>".@$measurementData['trouser_waist']."</td></tr>";

                    if(isset($measurementData['trouser_seat']))
                        $mhtml .= "<tr><td>Trouser Seat</td><td>".@$measurementData['trouser_seat']."</td></tr>";

                    if(isset($measurementData['outseam']))
                        $mhtml .= "<tr><td>Outseam</td><td>".@$measurementData['outseam']."</td></tr>";

                    if(isset($measurementData['thigh']))
                        $mhtml .= "<tr><td>Thigh</td><td>".@$measurementData['thigh']."</td></tr>";

                    if(isset($measurementData['knee']))
                        $mhtml .= "<tr><td>Knee</td><td>".@$measurementData['knee']."</td></tr>";

                    if(isset($measurementData['ankle']))
                        $mhtml .= "<tr><td>Ankle</td><td>".@$measurementData['ankle']."</td></tr>";

                    if(isset($measurementData['crotch']))
                        $mhtml .= "<tr><td>Crotch</td><td>".@$measurementData['crotch']."</td></tr>";

                    if(isset($measurementData['chest_circumf']))
                        $mhtml .= "<tr style='background-color: #b38d4a;color: white;'><td colspan=2 style='text-align: center;text-transform: uppercase;'><strong> Vest (".$json['style']['mesurementIn'].")</strong></td></tr>";

                    if(isset($measurementData['chest_circumf']))
                        $mhtml .= "<tr><td>Chest</td><td>".@$measurementData['chest_circumf']."</td></tr>";

                    if(isset($measurementData['back_length']))
                        $mhtml .= "<tr><td>Back Length</td><td>".@$measurementData['back_length']."</td></tr>";

                    if(isset($measurementData['waist']))
                        $mhtml .= "<tr><td>Waist</td><td>".@$measurementData['waist']."</td></tr>";
                }

                    $arr['measurement'] = $mhtml;

                /* Displaying Customized Styles */
                $type = $json['type'];
                $tempStyles = $json['display_style'];
                foreach ($tempStyles as $key => $styles) {
                    $html .= "<tr style='background-color: #b38d4a;color: white;'><td colspan=2 style='text-align: center;text-transform: uppercase;'><strong>" . $key . "</strong></td></tr>";
                    if (isset($styles)) {
                        foreach ($styles as $key => $style) {
                            $html .= "<tr>";
                            if ($key == "fabric") {
                                $label = $style['label'];
                                $fabricId = $style['value']['id'];
                                $fabricName = $style['value']['name'];
                                $fabricImage = $style['value']['image'];
                                $html .= "<td>" . $label . "</td>"
                                        . "<td style='text-align:center'>
                                        <img src='" . $fabricImage . "' width='100px'></td>";
                            } elseif ($key == "accent") {
                                if ($type == "Suit" || $type == "WomenSuit") {
                                    foreach ($style as $key => $accent) {
                                        if ($key == "monogram") {
                                            $html .= "<tr>";
                                            $label = $accent['label'];
                                            $value = $accent['value'];
                                            $html .= "<td>" . $label . "</td><td><strong>Text : </strong>" . $value['Text'] . ""
                                                    . "<br><strong>Color : </strong>" . $value['Color'] . ""
                                                    . "<br><strong>Font : </strong>" . $value['Font'] . ""
                                                    . "<br><strong>Position : </strong>" . $value['Position'] . ""
                                                    . "</td>";
                                            $html .= "</tr>";
                                        } else {
                                            $html .= "<tr>";
                                            $label = $accent['label'];
                                            $value = $accent['value'];
                                            $html .= "<td>" . $label . "</td><td>" . $value . "</td>";
                                            $html .= "</tr>";
                                        }
                                    }
                                } elseif ($type == "Shirt" || $type == "WomenShirt") {
                                    foreach ($style as $key => $accent) {
                                        if ($key == "monogram") {
                                            $html .= "<tr>";
                                            $label = $accent['label'];
                                            $value = $accent['value'];
                                            $html .= "<td>" . $label . "</td><td><strong>Text : </strong>" . $value['Text'] . ""
                                                    . "<br><strong>Color : </strong>" . $value['Color'] . ""
                                                    . "<br><strong>Font : </strong>" . $value['Font'] . ""
                                                    . "<br><strong>Position : </strong>" . $value['Position'] . ""
                                                    . "</td>";
                                            $html .= "</tr>";
                                        } else {
                                            $html .= "<tr>";
                                            $label = $accent['label'];
                                            $value = $accent['value'];
                                            if (array_key_exists('fabric', $accent)) {
                                                $fabric = $accent['fabric'];
                                                $fabricId = $accent['fabric']['id'];
                                                $fabricName = $accent['fabric']['name'];
                                                $fabricImage = $accent['fabric']['image'];
                                                $html .= "<td>" . $label . "</td>";
                                                $html .= "<td>" . $value . "<br><br><img src='" . $fabricImage . "' width='100px'></td>";
                                            } else
                                                $html .= "<td>" . $label . "</td><td>" . $value . "</td>";

                                            $html .= "</tr>";
                                        }
                                    }
                                }
                            } else {
                                $label = $style['label'];
                                $value = $style['value'];
                                $html .= "<td>" . $label . "</td><td>" . $value . "</td>";
                            }
                            $html .= "</tr>";
                        }
                    }
                }
                $arr['styles'] = $html;
            }
        } 
        
        else if ($data['reqType'] == 'order') {
            $arr = array();
            $html = "";
            $mhtml = "";
            $collection = $this->orderItemRepository->get($item_id);
            $json = json_decode($collection->getJson(), true);

            if(array_key_exists('rtw_measurement', $json)){

                $p_type = $json['rtw_measurement']['product_type'];
                $s_type = $json['rtw_measurement']['size_type'];

                if($p_type == "Shirt" || $p_type == "WomenShirt"){

                    $mhtml .= "<tr style='background-color: #b38d4a;color: white;'><td colspan=2 style='text-align: center;'><strong style='text-transform: uppercase;'> Shirt </strong> (In ".$s_type .")</td></tr>";
                    $measurementVal = $json['rtw_measurement'];

                        if(array_key_exists('size_name', $measurementVal)){
                            $mhtml .= "<tr><td>Size</td><td>".$measurementVal['size_name']."</td></tr>";
                        }
                        $mhtml .= "<tr><td>Collar</td><td>".$measurementVal['sh_collar']."</td></tr>";
                        $mhtml .= "<tr><td>Shoulder</td><td>".$measurementVal['sh_shoulder']."</td></tr>";
                        $mhtml .= "<tr><td>Length</td><td>".$measurementVal['sh_backlength']."</td></tr>";
                        $mhtml .= "<tr><td>Sleeve</td><td>".$measurementVal['sh_sleeve']."</td></tr>";
                        $mhtml .= "<tr><td>Chest</td><td>".$measurementVal['sh_chest']."</td></tr>";
                        $mhtml .= "<tr><td>Stomach</td><td>".$measurementVal['sh_stomach']."</td></tr>";
                        $mhtml .= "<tr><td>Seat</td><td>".$measurementVal['sh_seat']."</td></tr>";
                        $mhtml .= "<tr><td>Bicep Width</td><td>".$measurementVal['sh_bicepwidth']."</td></tr>";
                        $mhtml .= "<tr><td>Wrist Width</td><td>".$measurementVal['sh_wristwidth']."</td></tr>";
                }

                if($p_type == "Suit" || $p_type == "WomenSuit") {

                    $mhtml= "";
                    $measurementData = $json['rtw_measurement'];

                    if(isset($measurementData['su_chest']))
                        $mhtml .= "<tr style='background-color: #b38d4a;color: white;'><td colspan=2 style='text-align: center;'><strong style='text-transform: uppercase;'> Jacket </strong> (In ".$s_type .")</td></tr>";

                    if(array_key_exists('size_name', $measurementData)){
                        $mhtml .= "<tr><td>Size</td><td>".$measurementData['size_name']."</td></tr>";
                    }

                    if(isset($measurementData['su_chest']))
                        $mhtml .= "<tr><td>Chest</td><td>".$measurementData['su_chest']."</td></tr>";

                    if(isset($measurementData['su_stomach']))
                        $mhtml .= "<tr><td>Stomach</td><td>".$measurementData['su_stomach']."</td></tr>";

                    if(isset($measurementData['su_sleevelength']))
                        $mhtml .= "<tr><td>Sleeve Length</td><td>".$measurementData['su_sleevelength']."</td></tr>";

                    if(isset($measurementData['su_shoulder']))
                        $mhtml .= "<tr><td>Shoulder</td><td>".$measurementData['su_shoulder']."</td></tr>";

                    if(isset($measurementData['su_front_jacket_length']))
                        $mhtml .= "<tr><td>Front Jacket Length</td><td>".$measurementData['su_front_jacket_length']."</td></tr>";

                    if(isset($measurementData['su_back_jacket_length']))
                        $mhtml .= "<tr><td>Back Jacket Length</td><td>".$measurementData['su_back_jacket_length']."</td></tr>";

                    if(isset($measurementData['su_bicep_width']))
                        $mhtml .= "<tr><td>Bicep Width</td><td>".$measurementData['su_bicep_width']."</td></tr>";

                    if(isset($measurementData['su_wrist_width']))
                        $mhtml .= "<tr><td>Wrist Width</td><td>".$measurementData['su_wrist_width']."</td></tr>";

                    if(isset($measurementData['su_seat']))
                        $mhtml .= "<tr><td>Seat</td><td>".$measurementData['su_seat']."</td></tr>";

                    if(isset($measurementData['pa_trouser_waist']))
                        $mhtml .= "<tr style='background-color: #b38d4a;color: white;'><td colspan=2 style='text-align: center;text-transform: uppercase;'><strong> Pant </strong></td></tr>";

                    if(isset($measurementData['pa_trouser_waist']))
                        $mhtml .= "<tr><td>Trouser Waist</td><td>".$measurementData['pa_trouser_waist']."</td></tr>";

                    if(isset($measurementData['pa_seat']))
                        $mhtml .= "<tr><td>Trouser Seat</td><td>".$measurementData['pa_seat']."</td></tr>";

                    if(isset($measurementData['pa_outseam']))
                        $mhtml .= "<tr><td>Outseam</td><td>".$measurementData['pa_outseam']."</td></tr>";

                    if(isset($measurementData['pa_thigh']))
                        $mhtml .= "<tr><td>Thigh</td><td>".$measurementData['pa_thigh']."</td></tr>";

                    if(isset($measurementData['pa_knee']))
                        $mhtml .= "<tr><td>Knee</td><td>".$measurementData['pa_knee']."</td></tr>";

                    if(isset($measurementData['pa_ankle']))
                        $mhtml .= "<tr><td>Ankle</td><td>".$measurementData['pa_ankle']."</td></tr>";

                    if(isset($measurementData['pa_crotch']))
                        $mhtml .= "<tr><td>Crotch</td><td>".$measurementData['pa_crotch']."</td></tr>";

                    if(isset($measurementData['wc_chest']))
                        $mhtml .= "<tr style='background-color: #b38d4a;color: white;'><td colspan=2 style='text-align: center;text-transform: uppercase;'><strong> Vest </strong></td></tr>";

                    if(isset($measurementData['wc_chest']))
                        $mhtml .= "<tr><td>Chest</td><td>".$measurementData['wc_chest']."</td></tr>";

                    if(isset($measurementData['wc_backlength']))
                        $mhtml .= "<tr><td>Back Length</td><td>".$measurementData['wc_backlength']."</td></tr>";

                    if(isset($measurementData['wc_waist']))
                        $mhtml .= "<tr><td>Waist</td><td>".$measurementData['wc_waist']."</td></tr>";

                }

                $arr['measurement'] = $mhtml;


            } else {

                $arr['img'] = $baseUrl . $json['product'];
                $arr['styles'] = $json['style'];

                $type = $json['size']['type'];
                $p_type = $json['type'];

                if($arr['styles']['mesurementIn']=='in'){
                    $arr['styles']['mesurementIn'] = 'inch';
                }
                
                if($type == 'Standard' && ($p_type == "Shirt" || $p_type == "WomenShirt")){

                    $mhtml .= "<tr style='background-color: #b38d4a;color: white;'><td colspan=2 style='text-align: center;text-transform: uppercase;'><strong> Shirt (".$arr['styles']['mesurementIn'].")</strong></td></tr>";
                    $measurementVal = $json['size']['sizeData'][0];

                        $mhtml .= "<tr><td>Size</td><td>".$measurementVal['size_name']."</td></tr>";
                        $mhtml .= "<tr><td>Collar</td><td>".$measurementVal['Collar']."</td></tr>";
                        $mhtml .= "<tr><td>Shoulder</td><td>".$measurementVal['shoulder']."</td></tr>";
                        $mhtml .= "<tr><td>Length</td><td>".$measurementVal['length']."</td></tr>";
                        $mhtml .= "<tr><td>Sleeve</td><td>".$measurementVal['sleeve']."</td></tr>";
                        $mhtml .= "<tr><td>Chest</td><td>".$measurementVal['chest']."</td></tr>";
                        $mhtml .= "<tr><td>Stomach</td><td>".$measurementVal['stomach']."</td></tr>";
                        $mhtml .= "<tr><td>Seat</td><td>".$measurementVal['seat']."</td></tr>";
                        $mhtml .= "<tr><td>Bicep Width</td><td>".$measurementVal['Bicep_Width']."</td></tr>";
                        $mhtml .= "<tr><td>Wrist Width</td><td>".$measurementVal['Wrist_width']."</td></tr>";
                }

                else if($type == 'Scan' && ($p_type == "Shirt" || $p_type == "WomenShirt")){

                    $mhtml .= "<tr style='background-color: #b38d4a;color: white;'><td colspan=2 style='text-align: center;text-transform: uppercase;'><strong> Shirt (".$arr['styles']['mesurementIn'].")</strong></td></tr>";
                    $measurementVal = $json['size']['sizeData'];

                        $mhtml .= "<tr><td>Collar</td><td>".$measurementVal['collar']."</td></tr>";
                        $mhtml .= "<tr><td>Shoulder</td><td>".$measurementVal['shoulder']."</td></tr>";
                        $mhtml .= "<tr><td>Length</td><td>".$measurementVal['length']."</td></tr>";
                        $mhtml .= "<tr><td>Sleeve</td><td>".$measurementVal['sleeve']."</td></tr>";
                        $mhtml .= "<tr><td>Chest</td><td>".$measurementVal['chest']."</td></tr>";
                        $mhtml .= "<tr><td>Stomach</td><td>".$measurementVal['stomach']."</td></tr>";
                        $mhtml .= "<tr><td>Seat</td><td>".$measurementVal['seat']."</td></tr>";
                        $mhtml .= "<tr><td>Bicep Width</td><td>".$measurementVal['BicepWidth']."</td></tr>";
                        $mhtml .= "<tr><td>Wrist Width</td><td>".$measurementVal['Wristwidth']."</td></tr>";

                }
               
                else if($type == 'Standard' && ($p_type == "Suit" || $p_type == "WomenSuit")){

                    if(isset($json['size']['sizeData'][0])){
                        $measurementJacket = $json['size']['sizeData'][0];
                        $mhtml .= "<tr style='background-color: #b38d4a;color: white;'><td colspan=2 style='text-align: center;text-transform: uppercase;'><strong> Jacket (".$arr['styles']['mesurementIn'].")</strong></td></tr>";

                        foreach ($measurementJacket as $jkey => $jval) {
                           if($jkey=='id' || $jkey=='parent' || $jkey=='product_type' || $jkey=='gender'){

                            }
                            elseif($jkey=='size_name'){
                                $mhtml .= "<tr><td> Size </td><td>".$measurementJacket['size_name']."</td></tr>";
                            }
                            else{
                                $mhtml .= "<tr><td>".$jkey."</td><td>".$jval."</td></tr>";
                            }
                             
                        }
                    }


                    if(isset($json['size']['sizeData'][1])){
                        $measurementPant = $json['size']['sizeData'][1];
                        $mhtml .= "<tr style='background-color: #b38d4a;color: white;'><td colspan=2 style='text-align: center;text-transform: uppercase;'><strong> Pant (".$arr['styles']['mesurementIn'].")</strong></td></tr>";
                        foreach ($measurementPant as $pkey => $pval) {
                           if($pkey=='id' || $pkey=='parent' || $pkey=='product_type' || $pkey=='gender'){

                            }
                            elseif($pkey=='size_name'){
                                $mhtml .= "<tr><td> Size </td><td>".$measurementPant['size_name']."</td></tr>";
                            }
                            else{
                                $mhtml .= "<tr><td>".$pkey."</td><td>".$pval."</td></tr>";
                            }
                             
                        }
                    }

                    if(isset($json['size']['sizeData'][2])){
                        $measurementVest = $json['size']['sizeData'][2];
                        $mhtml .= "<tr style='background-color: #b38d4a;color: white;'><td colspan=2 style='text-align: center;text-transform: uppercase;'><strong> Vest (".$arr['styles']['mesurementIn'].")</strong></td></tr>";
                        foreach ($measurementVest as $vkey => $vval) {
                           if($vkey=='id' || $vkey=='parent' || $vkey=='product_type' || $vkey=='gender'){

                            }
                            elseif($vkey=='size_name'){
                                $mhtml .= "<tr><td> Size </td><td>".$measurementVest['size_name']."</td></tr>";
                            }
                            else{
                                $mhtml .= "<tr><td>".$vkey."</td><td>".$vval."</td></tr>";
                            }
                             
                        }
                    }


                }

                else if($type == 'Scan' && ($p_type == "Suit" || $p_type == "WomenSuit")){
                    $mhtml= "";
                    $measurementData = $json['size']['sizeData'];

                    if(isset($measurementData['chest']))
                        $mhtml .= "<tr style='background-color: #b38d4a;color: white;'><td colspan=2 style='text-align: center;text-transform: uppercase;'><strong> Jacket (".$arr['styles']['mesurementIn'].")</strong></td></tr>";

                    if(isset($measurementData['chest']))
                        $mhtml .= "<tr><td>Chest</td><td>".$measurementData['chest']."</td></tr>";

                    if(isset($measurementData['stomach']))
                        $mhtml .= "<tr><td>Stomach</td><td>".$measurementData['stomach']."</td></tr>";

                    if(isset($measurementData['sleeve_length']))
                        $mhtml .= "<tr><td>Sleeve Length</td><td>".$measurementData['sleeve_length']."</td></tr>";

                    if(isset($measurementData['shoulder']))
                        $mhtml .= "<tr><td>Shoulder</td><td>".$measurementData['shoulder']."</td></tr>";

                    if(isset($measurementData['frontJacketLength']))
                        $mhtml .= "<tr><td>Front Jacket Length</td><td>".$measurementData['frontJacketLength']."</td></tr>";

                    if(isset($measurementData['backJacketLength']))
                        $mhtml .= "<tr><td>Back Jacket Length</td><td>".$measurementData['backJacketLength']."</td></tr>";

                    if(isset($measurementData['bicepWidth']))
                        $mhtml .= "<tr><td>Bicep Width</td><td>".$measurementData['bicepWidth']."</td></tr>";

                    if(isset($measurementData['wristWidth']))
                        $mhtml .= "<tr><td>Wrist Width</td><td>".$measurementData['wristWidth']."</td></tr>";

                    if(isset($measurementData['seat']))
                        $mhtml .= "<tr><td>Seat</td><td>".$measurementData['seat']."</td></tr>";

                    if(isset($measurementData['trouser_waist']))
                        $mhtml .= "<tr style='background-color: #b38d4a;color: white;'><td colspan=2 style='text-align: center;text-transform: uppercase;'><strong> Pant (".$arr['styles']['mesurementIn'].")</strong></td></tr>";

                    if(isset($measurementData['trouser_waist']))
                        $mhtml .= "<tr><td>Trouser Waist</td><td>".$measurementData['trouser_waist']."</td></tr>";

                    if(isset($measurementData['trouser_seat']))
                        $mhtml .= "<tr><td>Trouser Seat</td><td>".$measurementData['trouser_seat']."</td></tr>";

                    if(isset($measurementData['outseam']))
                        $mhtml .= "<tr><td>Outseam</td><td>".$measurementData['outseam']."</td></tr>";

                    if(isset($measurementData['knee']))
                        $mhtml .= "<tr><td>Knee</td><td>".$measurementData['knee']."</td></tr>";

                    if(isset($measurementData['ankle']))
                        $mhtml .= "<tr><td>Ankle</td><td>".$measurementData['ankle']."</td></tr>";

                    if(isset($measurementData['crotch']))
                        $mhtml .= "<tr><td>Crotch</td><td>".$measurementData['crotch']."</td></tr>";

                    if(isset($measurementData['chest_circumf']))
                        $mhtml .= "<tr style='background-color: #b38d4a;color: white;'><td colspan=2 style='text-align: center;text-transform: uppercase;'><strong> Vest (".$arr['styles']['mesurementIn'].")</strong></td></tr>";

                    if(isset($measurementData['chest_circumf']))
                        $mhtml .= "<tr><td>Chest</td><td>".$measurementData['chest_circumf']."</td></tr>";

                    if(isset($measurementData['back_length']))
                        $mhtml .= "<tr><td>Back Length</td><td>".$measurementData['back_length']."</td></tr>";

                    if(isset($measurementData['waist']))
                        $mhtml .= "<tr><td>Waist</td><td>".$measurementData['waist']."</td></tr>";

                }

                    $arr['measurement'] = $mhtml;

                /* Displaying Customized Styles */
                $type = $json['type'];
                $tempStyles = $json['display_style'];


                foreach ($tempStyles as $key => $styles) {
                    $html .= "<tr style='background-color: #b38d4a;color: white;'><td colspan=2 style='text-align: center;text-transform: uppercase;'><strong>" . $key . "</strong></td></tr>";
                    if (isset($styles)) {
                        foreach ($styles as $key => $style) {
                            $html .= "<tr>";
                            if ($key == "fabric") {
                                $label = $style['label'];
                                $fabricId = $style['value']['id'];
                                $fabricName = $style['value']['name'];
                                $fabricImage = $style['value']['image'];
                                $html .= "<td>" . $label . "</td>"
                                        . "<td style='text-align:center'>
                                            <br><img src='" . $fabricImage . "' width='100px'></td>";
                            } elseif ($key == "accent") {
                                if ($type == "Suit" || $type == "WomenSuit") {
                                    foreach ($style as $key => $accent) {

                                        if ($key == "monogram") {
                                            $html .= "<tr>";
                                            $label = $accent['label'];
                                            $value = $accent['value'];
                                            $html .= "<td>" . $label . "</td><td><strong>Text : </strong>" . $value['Text'] . ""
                                                    . "<br><strong>Color : </strong>" . $value['Color'] . ""
                                                    . "<br><strong>Font : </strong>" . $value['Font'] . ""
                                                    . "<br><strong>Position : </strong>" . $value['Position'] . ""
                                                    . "</td>";
                                            $html .= "</tr>";
                                        } else {

                                            $html .= "<tr>";
                                            $label = $accent['label'];
                                            $value = $accent['value'];
                                            $html .= "<td>" . $label . "</td><td>" . $value . "</td>";
                                            $html .= "</tr>";
                                        }
                                    }
                                } elseif ($type == "Shirt" || $type == "WomenShirt") {
                                    foreach ($style as $key => $accent) {
                                        if ($key == "monogram") {
                                            $html .= "<tr>";
                                            $label = $accent['label'];
                                            $value = $accent['value'];
                                            $html .= "<td>" . $label . "</td><td><strong>Text : </strong>" . $value['Text'] . ""
                                                    . "<br><strong>Color : </strong>" . $value['Color'] . ""
                                                    . "<br><strong>Font : </strong>" . $value['Font'] . ""
                                                    . "<br><strong>Position : </strong>" . $value['Position'] . ""
                                                    . "</td>";
                                            $html .= "</tr>";
                                        } else {
                                            $html .= "<tr>";
                                            $label = $accent['label'];
                                            $value = $accent['value'];
                                            if (array_key_exists('fabric', $accent)) {
                                                $fabric = $accent['fabric'];
                                                $fabricId = $accent['fabric']['id'];
                                                $fabricName = $accent['fabric']['name'];
                                                $fabricImage = $accent['fabric']['image'];
                                                $html .= "<td>" . $label . "</td>";
                                                $html .= "<td>" . $value . "<br><img src='" . $fabricImage . "' width='100px'></td>";
                                            } else
                                                $html .= "<td>" . $label . "</td><td>" . $value . "</td>";

                                            $html .= "</tr>";
                                        }
                                    }
                                }
                            } else {
                                $label = $style['label'];
                                $value = $style['value'];
                                $html .= "<td>" . $label . "</td><td>" . $value . "</td>";
                            }
                            $html .= "</tr>";
                        }
                    }
                }
                $arr['styles'] = $html;

            }


        }

        else if ($data['reqType'] == 'cart_measurement') {
            $arr = array();
            $html = "";
            $mhtml = "";

            $quoteItem = $this->quoteItemFactory->create();
            $collection = $quoteItem->getCollection()->addFieldToFilter('item_id', array('eq' => $item_id));

                        

            $json = json_decode($collection->getFirstItem()->getJson(), true);

            if(array_key_exists('rtw_measurement', $json)){

                $p_type = $json['rtw_measurement']['product_type'];
                $s_type = $json['rtw_measurement']['size_type'];

                if($p_type == "Shirt" || $p_type == "WomenShirt"){

                    $mhtml .= "<tr style='background-color: #b38d4a;color: white;'><td colspan=2 style='text-align: center;'><strong style='text-transform: uppercase;'> Shirt </strong> (In ".$s_type .")</td></tr>";
                    $measurementVal = $json['rtw_measurement'];

                        if(array_key_exists('size_name', $measurementVal)){
                            $mhtml .= "<tr><td>Size</td><td>".$measurementVal['size_name']."</td></tr>";
                        }
                        $mhtml .= "<tr><td>Collar</td><td>".$measurementVal['sh_collar']."</td></tr>";
                        $mhtml .= "<tr><td>Shoulder</td><td>".$measurementVal['sh_shoulder']."</td></tr>";
                        $mhtml .= "<tr><td>Length</td><td>".$measurementVal['sh_backlength']."</td></tr>";
                        $mhtml .= "<tr><td>Sleeve</td><td>".$measurementVal['sh_sleeve']."</td></tr>";
                        $mhtml .= "<tr><td>Chest</td><td>".$measurementVal['sh_chest']."</td></tr>";
                        $mhtml .= "<tr><td>Stomach</td><td>".$measurementVal['sh_stomach']."</td></tr>";
                        $mhtml .= "<tr><td>Seat</td><td>".$measurementVal['sh_seat']."</td></tr>";
                        $mhtml .= "<tr><td>Bicep Width</td><td>".$measurementVal['sh_bicepwidth']."</td></tr>";
                        $mhtml .= "<tr><td>Wrist Width</td><td>".$measurementVal['sh_wristwidth']."</td></tr>";
                }

                if($p_type == "Suit" || $p_type == "WomenSuit") {

                    $mhtml= "";
                    $measurementData = $json['rtw_measurement'];

                    if(isset($measurementData['su_chest']))
                        $mhtml .= "<tr style='background-color: #b38d4a;color: white;'><td colspan=2 style='text-align: center;'><strong style='text-transform: uppercase;'> Jacket </strong> (In ".$s_type .")</td></tr>";

                    if(array_key_exists('size_name', $measurementData)){
                        $mhtml .= "<tr><td>Size</td><td>".$measurementData['size_name']."</td></tr>";
                    }

                    if(isset($measurementData['su_chest']))
                        $mhtml .= "<tr><td>Chest</td><td>".$measurementData['su_chest']."</td></tr>";

                    if(isset($measurementData['su_stomach']))
                        $mhtml .= "<tr><td>Stomach</td><td>".$measurementData['su_stomach']."</td></tr>";

                    if(isset($measurementData['su_sleevelength']))
                        $mhtml .= "<tr><td>Sleeve Length</td><td>".$measurementData['su_sleevelength']."</td></tr>";

                    if(isset($measurementData['su_shoulder']))
                        $mhtml .= "<tr><td>Shoulder</td><td>".$measurementData['su_shoulder']."</td></tr>";

                    if(isset($measurementData['su_front_jacket_length']))
                        $mhtml .= "<tr><td>Front Jacket Length</td><td>".$measurementData['su_front_jacket_length']."</td></tr>";

                    if(isset($measurementData['su_back_jacket_length']))
                        $mhtml .= "<tr><td>Back Jacket Length</td><td>".$measurementData['su_back_jacket_length']."</td></tr>";

                    if(isset($measurementData['su_bicep_width']))
                        $mhtml .= "<tr><td>Bicep Width</td><td>".$measurementData['su_bicep_width']."</td></tr>";

                    if(isset($measurementData['su_wrist_width']))
                        $mhtml .= "<tr><td>Wrist Width</td><td>".$measurementData['su_wrist_width']."</td></tr>";

                    if(isset($measurementData['su_seat']))
                        $mhtml .= "<tr><td>Seat</td><td>".$measurementData['su_seat']."</td></tr>";

                    if(isset($measurementData['pa_trouser_waist']))
                        $mhtml .= "<tr style='background-color: #b38d4a;color: white;'><td colspan=2 style='text-align: center;text-transform: uppercase;'><strong> Pant </strong></td></tr>";

                    if(isset($measurementData['pa_trouser_waist']))
                        $mhtml .= "<tr><td>Trouser Waist</td><td>".$measurementData['pa_trouser_waist']."</td></tr>";

                    if(isset($measurementData['pa_seat']))
                        $mhtml .= "<tr><td>Trouser Seat</td><td>".$measurementData['pa_seat']."</td></tr>";

                    if(isset($measurementData['pa_outseam']))
                        $mhtml .= "<tr><td>Outseam</td><td>".$measurementData['pa_outseam']."</td></tr>";

                    if(isset($measurementData['pa_thigh']))
                        $mhtml .= "<tr><td>Thigh</td><td>".$measurementData['pa_thigh']."</td></tr>";

                    if(isset($measurementData['pa_knee']))
                        $mhtml .= "<tr><td>Knee</td><td>".$measurementData['pa_knee']."</td></tr>";

                    if(isset($measurementData['pa_ankle']))
                        $mhtml .= "<tr><td>Ankle</td><td>".$measurementData['pa_ankle']."</td></tr>";

                    if(isset($measurementData['pa_crotch']))
                        $mhtml .= "<tr><td>Crotch</td><td>".$measurementData['pa_crotch']."</td></tr>";

                    if(isset($measurementData['wc_chest']))
                        $mhtml .= "<tr style='background-color: #b38d4a;color: white;'><td colspan=2 style='text-align: center;text-transform: uppercase;'><strong> Vest </strong></td></tr>";

                    if(isset($measurementData['wc_chest']))
                        $mhtml .= "<tr><td>Chest</td><td>".$measurementData['wc_chest']."</td></tr>";

                    if(isset($measurementData['wc_backlength']))
                        $mhtml .= "<tr><td>Back Length</td><td>".$measurementData['wc_backlength']."</td></tr>";

                    if(isset($measurementData['wc_waist']))
                        $mhtml .= "<tr><td>Waist</td><td>".$measurementData['wc_waist']."</td></tr>";

                }

                $arr['measurement'] = $mhtml;


            } else {

                $arr['img'] = $baseUrl . $json['product'];
                $arr['styles'] = $json['style'];

                $type = $json['size']['type'];
                $p_type = $json['type'];

                if($arr['styles']['mesurementIn']=='in'){
                    $arr['styles']['mesurementIn'] = 'inch';
                }
                
                if($type == 'Standard' && ($p_type == "Shirt" || $p_type == "WomenShirt")){

                    $mhtml .= "<tr style='background-color: #b38d4a;color: white;'><td colspan=2 style='text-align: center;text-transform: uppercase;'><strong> Shirt (".$arr['styles']['mesurementIn'].")</strong></td></tr>";
                    $measurementVal = $json['size']['sizeData'][0];

                        $mhtml .= "<tr><td>Size</td><td>".$measurementVal['size_name']."</td></tr>";
                        $mhtml .= "<tr><td>Collar</td><td>".$measurementVal['Collar']."</td></tr>";
                        $mhtml .= "<tr><td>Shoulder</td><td>".$measurementVal['shoulder']."</td></tr>";
                        $mhtml .= "<tr><td>Length</td><td>".$measurementVal['length']."</td></tr>";
                        $mhtml .= "<tr><td>Sleeve</td><td>".$measurementVal['sleeve']."</td></tr>";
                        $mhtml .= "<tr><td>Chest</td><td>".$measurementVal['chest']."</td></tr>";
                        $mhtml .= "<tr><td>Stomach</td><td>".$measurementVal['stomach']."</td></tr>";
                        $mhtml .= "<tr><td>Seat</td><td>".$measurementVal['seat']."</td></tr>";
                        $mhtml .= "<tr><td>Bicep Width</td><td>".$measurementVal['Bicep_Width']."</td></tr>";
                        $mhtml .= "<tr><td>Wrist Width</td><td>".$measurementVal['Wrist_width']."</td></tr>";
                }

                else if($type == 'Scan' && ($p_type == "Shirt" || $p_type == "WomenShirt")){

                    $mhtml .= "<tr style='background-color: #b38d4a;color: white;'><td colspan=2 style='text-align: center;text-transform: uppercase;'><strong> Shirt (".$arr['styles']['mesurementIn'].")</strong></td></tr>";
                    $measurementVal = $json['size']['sizeData'];

                        $mhtml .= "<tr><td>Collar</td><td>".$measurementVal['collar']."</td></tr>";
                        $mhtml .= "<tr><td>Shoulder</td><td>".$measurementVal['shoulder']."</td></tr>";
                        $mhtml .= "<tr><td>Length</td><td>".$measurementVal['length']."</td></tr>";
                        $mhtml .= "<tr><td>Sleeve</td><td>".$measurementVal['sleeve']."</td></tr>";
                        $mhtml .= "<tr><td>Chest</td><td>".$measurementVal['chest']."</td></tr>";
                        $mhtml .= "<tr><td>Stomach</td><td>".$measurementVal['stomach']."</td></tr>";
                        $mhtml .= "<tr><td>Seat</td><td>".$measurementVal['seat']."</td></tr>";
                        $mhtml .= "<tr><td>Bicep Width</td><td>".$measurementVal['BicepWidth']."</td></tr>";
                        $mhtml .= "<tr><td>Wrist Width</td><td>".$measurementVal['Wristwidth']."</td></tr>";

                }
               
                else if($type == 'Standard' && ($p_type == "Suit" || $p_type == "WomenSuit")){

                    if(isset($json['size']['sizeData'][0])){
                        $measurementJacket = $json['size']['sizeData'][0];
                        $mhtml .= "<tr style='background-color: #b38d4a;color: white;'><td colspan=2 style='text-align: center;text-transform: uppercase;'><strong> Jacket (".$arr['styles']['mesurementIn'].")</strong></td></tr>";

                        foreach ($measurementJacket as $jkey => $jval) {
                           if($jkey=='id' || $jkey=='parent' || $jkey=='product_type' || $jkey=='gender'){

                            }
                            elseif($jkey=='size_name'){
                                $mhtml .= "<tr><td> Size </td><td>".$measurementJacket['size_name']."</td></tr>";
                            }
                            else{
                                $mhtml .= "<tr><td>".$jkey."</td><td>".$jval."</td></tr>";
                            }
                             
                        }
                    }


                    if(isset($json['size']['sizeData'][1])){
                        $measurementPant = $json['size']['sizeData'][1];
                        $mhtml .= "<tr style='background-color: #b38d4a;color: white;'><td colspan=2 style='text-align: center;text-transform: uppercase;'><strong> Pant (".$arr['styles']['mesurementIn'].")</strong></td></tr>";
                        foreach ($measurementPant as $pkey => $pval) {
                           if($pkey=='id' || $pkey=='parent' || $pkey=='product_type' || $pkey=='gender'){

                            }
                            elseif($pkey=='size_name'){
                                $mhtml .= "<tr><td> Size </td><td>".$measurementPant['size_name']."</td></tr>";
                            }
                            else{
                                $mhtml .= "<tr><td>".$pkey."</td><td>".$pval."</td></tr>";
                            }
                             
                        }
                    }

                    if(isset($json['size']['sizeData'][2])){
                        $measurementVest = $json['size']['sizeData'][2];
                        $mhtml .= "<tr style='background-color: #b38d4a;color: white;'><td colspan=2 style='text-align: center;text-transform: uppercase;'><strong> Vest (".$arr['styles']['mesurementIn'].")</strong></td></tr>";
                        foreach ($measurementVest as $vkey => $vval) {
                           if($vkey=='id' || $vkey=='parent' || $vkey=='product_type' || $vkey=='gender'){

                            }
                            elseif($vkey=='size_name'){
                                $mhtml .= "<tr><td> Size </td><td>".$measurementVest['size_name']."</td></tr>";
                            }
                            else{
                                $mhtml .= "<tr><td>".$vkey."</td><td>".$vval."</td></tr>";
                            }
                             
                        }
                    }


                }

                else if($type == 'Scan' && ($p_type == "Suit" || $p_type == "WomenSuit")){
                    $mhtml= "";
                    $measurementData = $json['size']['sizeData'];

                    if(isset($measurementData['chest']))
                        $mhtml .= "<tr style='background-color: #b38d4a;color: white;'><td colspan=2 style='text-align: center;text-transform: uppercase;'><strong> Jacket (".$arr['styles']['mesurementIn'].")</strong></td></tr>";

                    if(isset($measurementData['chest']))
                        $mhtml .= "<tr><td>Chest</td><td>".$measurementData['chest']."</td></tr>";

                    if(isset($measurementData['stomach']))
                        $mhtml .= "<tr><td>Stomach</td><td>".$measurementData['stomach']."</td></tr>";

                    if(isset($measurementData['sleeve_length']))
                        $mhtml .= "<tr><td>Sleeve Length</td><td>".$measurementData['sleeve_length']."</td></tr>";

                    if(isset($measurementData['shoulder']))
                        $mhtml .= "<tr><td>Shoulder</td><td>".$measurementData['shoulder']."</td></tr>";

                    if(isset($measurementData['frontJacketLength']))
                        $mhtml .= "<tr><td>Front Jacket Length</td><td>".$measurementData['frontJacketLength']."</td></tr>";

                    if(isset($measurementData['backJacketLength']))
                        $mhtml .= "<tr><td>Back Jacket Length</td><td>".$measurementData['backJacketLength']."</td></tr>";

                    if(isset($measurementData['bicepWidth']))
                        $mhtml .= "<tr><td>Bicep Width</td><td>".$measurementData['bicepWidth']."</td></tr>";

                    if(isset($measurementData['wristWidth']))
                        $mhtml .= "<tr><td>Wrist Width</td><td>".$measurementData['wristWidth']."</td></tr>";

                    if(isset($measurementData['seat']))
                        $mhtml .= "<tr><td>Seat</td><td>".$measurementData['seat']."</td></tr>";

                    if(isset($measurementData['trouser_waist']))
                        $mhtml .= "<tr style='background-color: #b38d4a;color: white;'><td colspan=2 style='text-align: center;text-transform: uppercase;'><strong> Pant (".$arr['styles']['mesurementIn'].")</strong></td></tr>";

                    if(isset($measurementData['trouser_waist']))
                        $mhtml .= "<tr><td>Trouser Waist</td><td>".$measurementData['trouser_waist']."</td></tr>";

                    if(isset($measurementData['trouser_seat']))
                        $mhtml .= "<tr><td>Trouser Seat</td><td>".$measurementData['trouser_seat']."</td></tr>";

                    if(isset($measurementData['outseam']))
                        $mhtml .= "<tr><td>Outseam</td><td>".$measurementData['outseam']."</td></tr>";

                    if(isset($measurementData['knee']))
                        $mhtml .= "<tr><td>Knee</td><td>".$measurementData['knee']."</td></tr>";

                    if(isset($measurementData['ankle']))
                        $mhtml .= "<tr><td>Ankle</td><td>".$measurementData['ankle']."</td></tr>";

                    if(isset($measurementData['crotch']))
                        $mhtml .= "<tr><td>Crotch</td><td>".$measurementData['crotch']."</td></tr>";

                    if(isset($measurementData['chest_circumf']))
                        $mhtml .= "<tr style='background-color: #b38d4a;color: white;'><td colspan=2 style='text-align: center;text-transform: uppercase;'><strong> Vest (".$arr['styles']['mesurementIn'].")</strong></td></tr>";

                    if(isset($measurementData['chest_circumf']))
                        $mhtml .= "<tr><td>Chest</td><td>".$measurementData['chest_circumf']."</td></tr>";

                    if(isset($measurementData['back_length']))
                        $mhtml .= "<tr><td>Back Length</td><td>".$measurementData['back_length']."</td></tr>";

                    if(isset($measurementData['waist']))
                        $mhtml .= "<tr><td>Waist</td><td>".$measurementData['waist']."</td></tr>";

                }

                    $arr['measurement'] = $mhtml;

                /* Displaying Customized Styles */
                $type = $json['type'];
                $tempStyles = $json['display_style'];


                foreach ($tempStyles as $key => $styles) {
                    $html .= "<tr style='background-color: #b38d4a;color: white;'><td colspan=2 style='text-align: center;text-transform: uppercase;'><strong>" . $key . "</strong></td></tr>";
                    if (isset($styles)) {
                        foreach ($styles as $key => $style) {
                            $html .= "<tr>";
                            if ($key == "fabric") {
                                $label = $style['label'];
                                $fabricId = $style['value']['id'];
                                $fabricName = $style['value']['name'];
                                $fabricImage = $style['value']['image'];
                                $html .= "<td>" . $label . "</td>"
                                        . "<td style='text-align:center'>
                                            <br><img src='" . $fabricImage . "' width='100px'></td>";
                            } elseif ($key == "accent") {
                                if ($type == "Suit" || $type == "WomenSuit") {
                                    foreach ($style as $key => $accent) {

                                        if ($key == "monogram") {
                                            $html .= "<tr>";
                                            $label = $accent['label'];
                                            $value = $accent['value'];
                                            $html .= "<td>" . $label . "</td><td><strong>Text : </strong>" . $value['Text'] . ""
                                                    . "<br><strong>Color : </strong>" . $value['Color'] . ""
                                                    . "<br><strong>Font : </strong>" . $value['Font'] . ""
                                                    . "<br><strong>Position : </strong>" . $value['Position'] . ""
                                                    . "</td>";
                                            $html .= "</tr>";
                                        } else {

                                            $html .= "<tr>";
                                            $label = $accent['label'];
                                            $value = $accent['value'];
                                            $html .= "<td>" . $label . "</td><td>" . $value . "</td>";
                                            $html .= "</tr>";
                                        }
                                    }
                                } elseif ($type == "Shirt" || $type == "WomenShirt") {
                                    foreach ($style as $key => $accent) {
                                        if ($key == "monogram") {
                                            $html .= "<tr>";
                                            $label = $accent['label'];
                                            $value = $accent['value'];
                                            $html .= "<td>" . $label . "</td><td><strong>Text : </strong>" . $value['Text'] . ""
                                                    . "<br><strong>Color : </strong>" . $value['Color'] . ""
                                                    . "<br><strong>Font : </strong>" . $value['Font'] . ""
                                                    . "<br><strong>Position : </strong>" . $value['Position'] . ""
                                                    . "</td>";
                                            $html .= "</tr>";
                                        } else {
                                            $html .= "<tr>";
                                            $label = $accent['label'];
                                            $value = $accent['value'];
                                            if (array_key_exists('fabric', $accent)) {
                                                $fabric = $accent['fabric'];
                                                $fabricId = $accent['fabric']['id'];
                                                $fabricName = $accent['fabric']['name'];
                                                $fabricImage = $accent['fabric']['image'];
                                                $html .= "<td>" . $label . "</td>";
                                                $html .= "<td>" . $value . "<br><img src='" . $fabricImage . "' width='100px'></td>";
                                            } else
                                                $html .= "<td>" . $label . "</td><td>" . $value . "</td>";

                                            $html .= "</tr>";
                                        }
                                    }
                                }
                            } else {
                                $label = $style['label'];
                                $value = $style['value'];
                                $html .= "<td>" . $label . "</td><td>" . $value . "</td>";
                            }
                            $html .= "</tr>";
                        }
                    }
                }
                $arr['styles'] = $html;

            }


        } 

        else if ($data['reqType'] == 'wishlist') {
            $arr = array();
            $html = "";
            $mhtml = "";
            $wishlistitem = $this->itemFactory->create()->load($item_id);
            $json = json_decode($wishlistitem->getJson(),true);
            $arr['img'] = $baseUrl . $json['product'];
            $arr['styles'] = $json['style'];

            if($json['style']['mesurementIn']=='in'){
                $json['style']['mesurementIn'] = 'inch';
            }


            $type = $json['size']['type'];
            $p_type = $json['type'];

                
                if($type == 'Standard' && ($p_type == "Shirt" || $p_type == "WomenShirt")){

                    $mhtml .= "<tr style='background-color: #b38d4a;color: white;'><td colspan=2 style='text-align: center;text-transform: uppercase;'><strong> Shirt (".$json['style']['mesurementIn'].")</strong></td></tr>";
                    $measurementVal = $json['size']['sizeData'][0];

                        $mhtml .= "<tr><td>Size</td><td>".$measurementVal['size_name']."</td></tr>";
                        $mhtml .= "<tr><td>Collar</td><td>".$measurementVal['Collar']."</td></tr>";
                        $mhtml .= "<tr><td>Shoulder</td><td>".$measurementVal['shoulder']."</td></tr>";
                        $mhtml .= "<tr><td>Length</td><td>".$measurementVal['length']."</td></tr>";
                        $mhtml .= "<tr><td>Sleeve</td><td>".$measurementVal['sleeve']."</td></tr>";
                        $mhtml .= "<tr><td>Chest</td><td>".$measurementVal['chest']."</td></tr>";
                        $mhtml .= "<tr><td>Stomach</td><td>".$measurementVal['stomach']."</td></tr>";
                        $mhtml .= "<tr><td>Seat</td><td>".$measurementVal['seat']."</td></tr>";
                        $mhtml .= "<tr><td>Bicep Width</td><td>".$measurementVal['Bicep_Width']."</td></tr>";
                        $mhtml .= "<tr><td>Wrist Width</td><td>".$measurementVal['Wrist_width']."</td></tr>";
                }

                else if($type == 'Scan' && ($p_type == "Shirt" || $p_type == "WomenShirt")){

                    $mhtml .= "<tr style='background-color: #b38d4a;color: white;'><td colspan=2 style='text-align: center;text-transform: uppercase;'><strong> Shirt (".$json['style']['mesurementIn'].")</strong></td></tr>";
                    $measurementVal = $json['size']['sizeData'];

                        $mhtml .= "<tr><td>Collar</td><td>".$measurementVal['collar']."</td></tr>";
                        $mhtml .= "<tr><td>Shoulder</td><td>".$measurementVal['shoulder']."</td></tr>";
                        $mhtml .= "<tr><td>Length</td><td>".$measurementVal['length']."</td></tr>";
                        $mhtml .= "<tr><td>Sleeve</td><td>".$measurementVal['sleeve']."</td></tr>";
                        $mhtml .= "<tr><td>Chest</td><td>".$measurementVal['chest']."</td></tr>";
                        $mhtml .= "<tr><td>Stomach</td><td>".$measurementVal['stomach']."</td></tr>";
                        $mhtml .= "<tr><td>Seat</td><td>".$measurementVal['seat']."</td></tr>";
                        $mhtml .= "<tr><td>Bicep Width</td><td>".$measurementVal['BicepWidth']."</td></tr>";
                        $mhtml .= "<tr><td>Wrist Width</td><td>".$measurementVal['Wristwidth']."</td></tr>";

                }
               
                else if($type == 'Standard' && ($p_type == "Suit" || $p_type == "WomenSuit")){

                    if(isset($json['size']['sizeData'][0])){
                        $measurementJacket = $json['size']['sizeData'][0];
                        $mhtml .= "<tr style='background-color: #b38d4a;color: white;'><td colspan=2 style='text-align: center;text-transform: uppercase;'><strong> Jacket (".$json['style']['mesurementIn'].")</strong></td></tr>";

                        foreach ($measurementJacket as $jkey => $jval) {
                           if($jkey=='id' || $jkey=='parent' || $jkey=='product_type' || $jkey=='gender'){

                            }
                            elseif($jkey=='size_name'){
                                $mhtml .= "<tr><td> Size </td><td>".$measurementJacket['size_name']."</td></tr>";
                            }
                            else{
                                $mhtml .= "<tr><td>".$jkey."</td><td>".$jval."</td></tr>";
                            }
                             
                        }
                    }

                    if(isset($json['size']['sizeData'][1])){
                        $measurementPant = $json['size']['sizeData'][1];
                        $mhtml .= "<tr style='background-color: #b38d4a;color: white;'><td colspan=2 style='text-align: center;text-transform: uppercase;'><strong> Pant (".$json['style']['mesurementIn'].")</strong></td></tr>";
                        foreach ($measurementPant as $pkey => $pval) {
                           if($pkey=='id' || $pkey=='parent' || $pkey=='product_type' || $pkey=='gender'){

                            }
                            elseif($pkey=='size_name'){
                                $mhtml .= "<tr><td> Size </td><td>".$measurementPant['size_name']."</td></tr>";
                            }
                            else{
                                $mhtml .= "<tr><td>".$pkey."</td><td>".$pval."</td></tr>";
                            }
                             
                        }
                    }

                    if(isset($json['size']['sizeData'][2])){
                        $measurementVest = $json['size']['sizeData'][2];
                        $mhtml .= "<tr style='background-color: #b38d4a;color: white;'><td colspan=2 style='text-align: center;text-transform: uppercase;'><strong> Vest (".$json['style']['mesurementIn'].")</strong></td></tr>";
                        foreach ($measurementVest as $vkey => $vval) {
                           if($vkey=='id' || $vkey=='parent' || $vkey=='product_type' || $vkey=='gender'){

                            }
                            elseif($vkey=='size_name'){
                                $mhtml .= "<tr><td> Size </td><td>".$measurementVest['size_name']."</td></tr>";
                            }
                            else{
                                $mhtml .= "<tr><td>".$vkey."</td><td>".$vval."</td></tr>";
                            }
                             
                        }

                    }

                }

                else if($type == 'Scan' && ($p_type == "Suit" || $p_type == "WomenSuit")){
                    $mhtml= "";
                    $measurementData = $json['size']['sizeData'];

                    if(isset($measurementData['chest']))
                        $mhtml .= "<tr style='background-color: #b38d4a;color: white;'><td colspan=2 style='text-align: center;text-transform: uppercase;'><strong> Jacket (".$json['style']['mesurementIn'].")</strong></td></tr>";

                    if(isset($measurementData['chest']))
                        $mhtml .= "<tr><td>Chest</td><td>".$measurementData['chest']."</td></tr>";

                    if(isset($measurementData['stomach']))
                        $mhtml .= "<tr><td>Stomach</td><td>".$measurementData['stomach']."</td></tr>";

                    if(isset($measurementData['sleeve_length']))
                        $mhtml .= "<tr><td>Sleeve Length</td><td>".$measurementData['sleeve_length']."</td></tr>";

                    if(isset($measurementData['shoulder']))
                        $mhtml .= "<tr><td>Shoulder</td><td>".$measurementData['shoulder']."</td></tr>";

                    if(isset($measurementData['frontJacketLength']))
                        $mhtml .= "<tr><td>Front Jacket Length</td><td>".$measurementData['frontJacketLength']."</td></tr>";

                    if(isset($measurementData['backJacketLength']))
                        $mhtml .= "<tr><td>Back Jacket Length</td><td>".$measurementData['backJacketLength']."</td></tr>";

                    if(isset($measurementData['bicepWidth']))
                        $mhtml .= "<tr><td>Bicep Width</td><td>".$measurementData['bicepWidth']."</td></tr>";

                    if(isset($measurementData['wristWidth']))
                        $mhtml .= "<tr><td>Wrist Width</td><td>".$measurementData['wristWidth']."</td></tr>";

                    if(isset($measurementData['seat']))
                        $mhtml .= "<tr><td>Seat</td><td>".$measurementData['seat']."</td></tr>";

                    if(isset($measurementData['trouser_waist']))
                        $mhtml .= "<tr style='background-color: #b38d4a;color: white;'><td colspan=2 style='text-align: center;text-transform: uppercase;'><strong> Pant (".$json['style']['mesurementIn'].")</strong></td></tr>";

                    if(isset($measurementData['trouser_waist']))
                        $mhtml .= "<tr><td>Trouser Waist</td><td>".$measurementData['trouser_waist']."</td></tr>";

                    if(isset($measurementData['trouser_seat']))
                        $mhtml .= "<tr><td>Trouser Seat</td><td>".$measurementData['trouser_seat']."</td></tr>";

                    if(isset($measurementData['outseam']))
                        $mhtml .= "<tr><td>Outseam</td><td>".$measurementData['outseam']."</td></tr>";

                    if(isset($measurementData['thigh']))
                        $mhtml .= "<tr><td>Thigh</td><td>".$measurementData['thigh']."</td></tr>";

                    if(isset($measurementData['knee']))
                        $mhtml .= "<tr><td>Knee</td><td>".$measurementData['knee']."</td></tr>";

                    if(isset($measurementData['ankle']))
                        $mhtml .= "<tr><td>Ankle</td><td>".$measurementData['ankle']."</td></tr>";

                    if(isset($measurementData['crotch']))
                        $mhtml .= "<tr><td>Crotch</td><td>".$measurementData['crotch']."</td></tr>";

                    if(isset($measurementData['chest_circumf']))
                        $mhtml .= "<tr style='background-color: #b38d4a;color: white;'><td colspan=2 style='text-align: center;text-transform: uppercase;'><strong> Vest (".$json['style']['mesurementIn'].")</strong></td></tr>";

                    if(isset($measurementData['chest_circumf']))
                        $mhtml .= "<tr><td>Chest</td><td>".$measurementData['chest_circumf']."</td></tr>";

                    if(isset($measurementData['back_length']))
                        $mhtml .= "<tr><td>Back Length</td><td>".$measurementData['back_length']."</td></tr>";

                    if(isset($measurementData['waist']))
                        $mhtml .= "<tr><td>Waist</td><td>".$measurementData['waist']."</td></tr>";

                }

                    $arr['measurement'] = $mhtml;

                /* Displaying Customized Styles */
                $type = $json['type'];
                $tempStyles = $json['display_style'];


                foreach ($tempStyles as $key => $styles) {
                    $html .= "<tr style='background-color: #b38d4a;color: white;'><td colspan=2 style='text-align: center;text-transform: uppercase;'><strong>" . $key . "</strong></td></tr>";
                    if (isset($styles)) {
                        foreach ($styles as $key => $style) {
                            $html .= "<tr>";
                            if ($key == "fabric") {
                                $label = $style['label'];
                                $fabricId = $style['value']['id'];
                                $fabricName = $style['value']['name'];
                                $fabricImage = $style['value']['image'];
                                $html .= "<td>" . $label . "</td>"
                                        . "<td style='text-align:center'>
                                            <br><img src='" . $fabricImage . "' width='100px'></td>";
                            } elseif ($key == "accent") {
                                if ($type == "Suit" || $type == "WomenSuit") {
                                    foreach ($style as $key=>$accent) {

                                        if ($key == "monogram") {
                                            $html .= "<tr>";
                                            $label = $accent['label'];
                                            $value = $accent['value'];
                                            $html .= "<td>" . $label . "</td><td><strong>Text : </strong>" . $value['Text'] . ""
                                                    . "<br><strong>Color : </strong>" . $value['Color'] . ""
                                                    . "<br><strong>Font : </strong>" . $value['Font'] . ""
                                                    . "<br><strong>Position : </strong>" . $value['Position'] . ""
                                                    . "</td>";
                                            $html .= "</tr>";
                                        } else {

                                            $html .= "<tr>";
                                            $label = $accent['label'];
                                            $value = $accent['value'];
                                            $html .= "<td>" . $label . "</td><td>" . $value . "</td>";
                                            $html .= "</tr>";
                                        }
                                    }
                                } elseif ($type == "Shirt" || $type == "WomenShirt") {
                                    foreach ($style as $key => $accent) {
                                        if ($key == "monogram") {
                                            $html .= "<tr>";
                                            $label = $accent['label'];
                                            $value = $accent['value'];
                                            $html .= "<td>" . $label . "</td><td><strong>Text : </strong>" . $value['Text'] . ""
                                                    . "<br><strong>Color : </strong>" . $value['Color'] . ""
                                                    . "<br><strong>Font : </strong>" . $value['Font'] . ""
                                                    . "<br><strong>Position : </strong>" . $value['Position'] . ""
                                                    . "</td>";
                                            $html .= "</tr>";
                                        } else {
                                            $html .= "<tr>";
                                            $label = $accent['label'];
                                            $value = $accent['value'];
                                            if (array_key_exists('fabric', $accent)) {
                                                $fabric = $accent['fabric'];
                                                $fabricId = $accent['fabric']['id'];
                                                $fabricName = $accent['fabric']['name'];
                                                $fabricImage = $accent['fabric']['image'];
                                                $html .= "<td>" . $label . "</td>";
                                                $html .= "<td>" . $value . "<br><img src='" . $fabricImage . "' width='100px'></td>";
                                            } else
                                                $html .= "<td>" . $label . "</td><td>" . $value . "</td>";

                                            $html .= "</tr>";
                                        }
                                    }
                                }
                            } else {
                                $label = $style['label'];
                                $value = $style['value'];
                                $html .= "<td>" . $label . "</td><td>" . $value . "</td>";
                            }
                            $html .= "</tr>";
                        }
                    }
                }
                $arr['styles'] = $html;

        } 

        else if ($data['reqType'] == 'wishlist_rtw') {
            $arr = array();
            $html = "";
            $mhtml = "";
            $wishlistitem = $this->itemFactory->create()->load($item_id);
            $json = json_decode($wishlistitem->getJson(),true);
            $measurementData = $json['rtw_measurement'];

            if(array_key_exists("su_chest", $measurementData) || $measurementData['product_type']=='Suit' || $measurementData['product_type']=='WomenSuit'){
                $mhtml .= "<tr><th colspan='2' style='text-align:center;'>Measurement Details (".$measurementData['size_type'].")</th></tr>";
                $mhtml .= "<tr style='background-color: #b38d4a;color: white;'><td colspan=2 style='text-align: center;text-transform: uppercase;'><strong> Jacket </strong></td></tr>";

                if(array_key_exists("size_name", $measurementData)){
                    $mhtml .= "<tr><td>Size</td><td>".$measurementData['size_name']."</td></tr>";                    
                }

                $mhtml .= "<tr><td>Chest</td><td>".$measurementData['su_chest']."</td></tr>";
                $mhtml .= "<tr><td>Stomach</td><td>".$measurementData['su_stomach']."</td></tr>";
                $mhtml .= "<tr><td>Sleeve Length</td><td>".$measurementData['su_sleevelength']."</td></tr>";
                $mhtml .= "<tr><td>Shoulder</td><td>".$measurementData['su_shoulder']."</td></tr>";
                $mhtml .= "<tr><td>Front Jacket Length</td><td>".$measurementData['su_front_jacket_length']."</td></tr>";
                $mhtml .= "<tr><td>Back Jacket Length</td><td>".$measurementData['su_back_jacket_length']."</td></tr>";
                $mhtml .= "<tr><td>Bicep Width</td><td>".$measurementData['su_bicep_width']."</td></tr>";
                $mhtml .= "<tr><td>Wrist Width</td><td>".$measurementData['su_wrist_width']."</td></tr>";
                $mhtml .= "<tr><td>Seat</td><td>".$measurementData['su_seat']."</td></tr>";

                $mhtml .= "<tr style='background-color: #b38d4a;color: white;'><td colspan=2 style='text-align: center;text-transform: uppercase;'><strong> Pant </strong></td></tr>";

                $mhtml .= "<tr><td>Trouser Waist</td><td>".$measurementData['pa_trouser_waist']."</td></tr>";
                $mhtml .= "<tr><td>Trouser Seat</td><td>".$measurementData['pa_seat']."</td></tr>";
                $mhtml .= "<tr><td>Outseam</td><td>".$measurementData['pa_outseam']."</td></tr>";
                $mhtml .= "<tr><td>Thigh</td><td>".$measurementData['pa_thigh']."</td></tr>";
                $mhtml .= "<tr><td>Knee</td><td>".$measurementData['pa_knee']."</td></tr>";
                $mhtml .= "<tr><td>Ankle</td><td>".$measurementData['pa_ankle']."</td></tr>";
                $mhtml .= "<tr><td>Crotch</td><td>".$measurementData['pa_crotch']."</td></tr>";

                $mhtml .= "<tr style='background-color: #b38d4a;color: white;'><td colspan=2 style='text-align: center;text-transform: uppercase;'><strong> Vest </strong></td></tr>";
                $mhtml .= "<tr><td>Chest</td><td>".$measurementData['wc_chest']."</td></tr>";
                $mhtml .= "<tr><td>Back Length</td><td>".$measurementData['wc_backlength']."</td></tr>";
                $mhtml .= "<tr><td>Waist</td><td>".$measurementData['wc_waist']."</td></tr>";
            }
            else if(array_key_exists("sh_collar", $measurementData) || $measurementData['product_type']=='Shirt' || $measurementData['product_type']=='WomenShirt'){
                $mhtml .= "<tr><th colspan='2' style='text-align:center;'>Measurement Details (".$measurementData['size_type'].")</th></tr>";

                $mhtml .= "<tr style='background-color: #b38d4a;color: white;'><td colspan=2 style='text-align: center;text-transform: uppercase;'><strong> Shirt </strong></td></tr>";

                if(array_key_exists("size_name", $measurementData)){
                    $mhtml .= "<tr><td>Size</td><td>".$measurementData['size_name']."</td></tr>";                    
                }

                $mhtml .= "<tr><td>Collar</td><td>".$measurementData['sh_collar']."</td></tr>";
                $mhtml .= "<tr><td>Shoulder</td><td>".$measurementData['sh_shoulder']."</td></tr>";
                $mhtml .= "<tr><td>Length</td><td>".$measurementData['sh_backlength']."</td></tr>";
                $mhtml .= "<tr><td>Sleeve</td><td>".$measurementData['sh_chest']."</td></tr>";
                $mhtml .= "<tr><td>Chest</td><td>".$measurementData['sh_sleeve']."</td></tr>";
                $mhtml .= "<tr><td>Stomach</td><td>".$measurementData['sh_stomach']."</td></tr>";
                $mhtml .= "<tr><td>Seat</td><td>".$measurementData['sh_seat']."</td></tr>";
                $mhtml .= "<tr><td>Bicep Width</td><td>".$measurementData['sh_bicepwidth']."</td></tr>";
                $mhtml .= "<tr><td>Wrist Width</td><td>".$measurementData['sh_wristwidth']."</td></tr>";
            }


                    $arr['measurement'] = $mhtml;

        } 

        else if ($data['reqType'] == 'std_cart') {
            $arr = array();
            $category = $data['cName'];
            $gender_name = $data['gName'];
            if($category!= null){
                $lastorderData = $this->getLastOrder();

            }

            if(!empty($lastorderData)){
              if(($category=='Shirt'||$category=='shirt') && ($gender_name=='Men'||$gender_name=='men') && isset($lastorderData['Shirt']) && $lastorderData['Shirt']!=''){
                  $final_last_order_data = $lastorderData['Shirt'];
                } 

              else if(($category=='Shirt'||$category=='shirt') && ($gender_name=='Women'||$gender_name=='women') && isset($lastorderData['WomenShirt']) && $lastorderData['WomenShirt']!=''){
                  $final_last_order_data = $lastorderData['WomenShirt'];
                } 

              else if(($category=='Suit'||$category=='suit') && ($gender_name=='Men'||$gender_name=='men') && isset($lastorderData['Suit']) && $lastorderData['Suit']!=''){
                  $final_last_order_data = $lastorderData['Suit'];
                } 

              else if(($category=='Suit'||$category=='suit') && ($gender_name=='Women'||$gender_name=='women') && isset($lastorderData['WomenSuit']) && $lastorderData['WomenSuit']!=''){
                  $final_last_order_data = $lastorderData['WomenSuit'];
                } 
              else{
                  $final_last_order_data = '';
              }

            }

            $quoteItem = $this->quoteItemFactory->create();
            $collection = $quoteItem->getCollection()->addFieldToFilter('item_id', array('eq' => $item_id));
            $rtw_measurement = array();
            foreach ($collection as $item) {
                $rtw_measurement = json_decode($item->getJson(), true);
            }


            $standardmodel = $this->_standardfactory->create();
            $collection = $standardmodel->getCollection()->addFieldToFilter('product_type', array('eq' => $category));
            $data = $collection->getData();
            $standard_data = array();
            foreach ($data as $key => $value) {
            $standard_data[$key] = json_decode($value['json'],true);
            }

            // if($this->isWholesaler() == "customer"){
            //      $Measure_data = $this->getBodyMeasurements();
            //      $measurementArray = json_decode($Measure_data['json'],true);
            // }

                 $Measure_data = $this->getBodyMeasurements();
                 $measurementArray = json_decode($Measure_data['json'],true);

            if($measurementArray['convert_type']=="inch" && array_key_exists('convert_type', $measurementArray)){
              $convert_inch = 'checked="checked"';
            }else{
              $convert_cm = 'checked="checked"';
            }

            $radioRow_html='<label class="radio-inline">
                          <input type="radio" name="unitScanOptions" id="radioScanCm" value="cm" ng-model="unitScanName" value="cm" onchange="changeScanUnit(\'cm\');" '.@$convert_cm.'>
                          Cm </label>
                      <label class="radio-inline">
                          <input type="radio" name="unitScanOptions" id="radioScanInch" value="inch" ng-model="unitScanName" value="inch" onchange="changeScanUnit(\'inch\');" '.@$convert_inch.'>
                          Inch </label>';


            if($category=="Shirt" || $category=="shirt")
            {

            $scanForm_html = '<div class="form-group">
                  <label for="inputPassword3" class="col-xs-2 control-label"> <i class="fa fa-arrow-circle-right text-success"> </i> &nbsp; </label>
                  <div class="col-xs-10">  <label class="control-label"> Enter Shirt Measurements</label> </div>
              </div>
              <hr>';

             $scanForm_html.='<div class="col-xs-12 col-sm-6" id="jacketScanSizes">
                  <div class="form-group">

                      <label for="sh_collar" class="col-xs-5 control-label">Collar</label>
                      <div class="col-xs-6">
                          <input type="text" id="sh_collar" parent="Shirt" name="sh_collar" class="form-control scan_input scan_number"  value="'.@$measurementArray['sh_collar'].'" placeholder="Collar">
                          <span class="sh_collar"></span>
                      </div>
                      <div class="col-xs-7"> </div>
                  </div>

                  <div class="form-group">
                      <label for="sh_shoulder" class="col-xs-5 control-label">Shoulder</label>

                      <div class="col-xs-6">
                          <input type="text" id="sh_shoulder" parent="Shirt" name="sh_shoulder" class="form-control scan_input scan_number" value="'.@$measurementArray['sh_shoulder'].'" placeholder="Shoulder">
                          <span class="sh_shoulder"></span>
                      </div>
                      <div class="col-xs-7"> </div>
                  </div>
                  <div class="form-group">
                      <label for="sh_backlength" class="col-xs-5 control-label">Back length</label>
                      <div class="col-xs-6">
                          <input type="text" id="sh_backlength" name="sh_backlength" parent="Shirt" class="form-control scan_input scan_number" value="'.@$measurementArray['sh_backlength'].'" placeholder="Back length">
                          <span class="sh_backlength"></span>
                      </div>
                      <div class="col-xs-7"> </div>
                  </div>
                  <div class="form-group">
                      <label for="sh_chest" class="col-xs-5 control-label">Chest</label>
                      <div class="col-xs-6">
                          <input type="text" id="sh_chest" parent="Shirt" name="sh_chest" class="form-control scan_input scan_number" value="'.@$measurementArray['sh_chest'].'" placeholder="Chest">
                          <span class="sh_chest"></span>
                      </div>
                      <div class="col-xs-7"> </div>
                  </div>
                  <div class="form-group">
                      <label for="sh_sleeve" class="col-xs-5 control-label">Sleeve</label>
                      <div class="col-xs-6">
                          <input type="text" id="sh_sleeve" name="sh_sleeve" parent="Shirt" class="form-control scan_input scan_number"  placeholder="Sleeve" value="'. 
                          @$measurementArray['sh_sleeve'].'">
                          <span class="sh_sleeve"></span>
                      </div>
                      <div class="col-xs-7"> </div>
                  </div>
              </div>
              <div class="col-xs-12 col-sm-6" id="jacketScanSizes">
                  <div class="form-group">
                      <label for="sh_stomach" class="col-xs-5 control-label">Stomach</label>
                      <div class="col-xs-6">
                          <input type="text" id="sh_stomach" name="sh_stomach" parent="Shirt" class="form-control scan_input scan_number"  placeholder="Stomach" value="'.
                           @$measurementArray['sh_stomach'].'">
                          <span class="sh_stomach"></span>
                      </div>
                      <div class="col-xs-7"> </div>
                  </div>
                  <div class="form-group">
                      <label for="sh_seat" class="col-xs-5 control-label">Seat</label>
                      <div class="col-xs-6">
                          <input type="text" id="sh_seat" name="sh_seat" parent="Shirt" class="form-control scan_input scan_number"  placeholder="Seat" value="'.@$measurementArray['sh_seat'].'">
                          <span class="sh_seat"></span>
                      </div>
                      <div class="col-xs-7"> </div>
                  </div>
                  <div class="form-group">
                      <label for="sh_bicepwidth" class="col-xs-5 control-label">Bicep Width</label>
                      <div class="col-xs-6">
                          <input type="text" id="sh_bicepwidth" name="sh_bicepwidth" parent="Shirt" class="form-control scan_input scan_number"  placeholder="Bicep Width" value="'.
                          @$measurementArray['sh_bicepwidth'].'">
                          <span class="sh_bicepwidth"></span>
                      </div>
                      <div class="col-xs-7"> </div>
                  </div>
                  <div class="form-group">
                      <label for="sh_wristwidth" class="col-xs-5 control-label">Wrist Width</label>
                      <div class="col-xs-6">
                          <input type="text" id="sh_wristwidth" name="sh_wristwidth" parent="Shirt" class="form-control scan_input scan_number"  placeholder="Wrist Width" value="'. 
                          @$measurementArray['sh_wristwidth'].'">
                          <span class="sh_wristwidth"></span>
                      </div>
                      <div class="col-xs-7"> </div>
                  </div>
              </div>'; 


            $html = '<table class="table table-bordered size">
                            <thead>
                                <tr>
                                    <th style="font-weight:bold">Shirt</th>
                                </tr>
                                <tr>
                                    <th>SIZE</th>'; 

                                foreach ($standard_data as $key => $value) { 
                                    
                            $html.= '<th>' .$value['size_name'].'</th>';
                                    }
                            $html.= '</tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Collar</td>';

                                    foreach ($standard_data as $key => $value){ 

                            $html.= '<td class="std_number">'.$value['sh_collar'].'</td>';
                                    }
                            $html.= '</tr>

                                <tr>
                                    <td>Shoulder</td>';

                                    foreach ($standard_data as $key => $value){ 

                            $html.= '<td class="std_number">'.$value['sh_shoulder'].'</td>';
                                    }
                            $html.= '</tr>
                                <tr>
                                    <td>Back Length</td>';

                                    foreach ($standard_data as $key => $value){ 

                            $html.= '<td class="std_number">'.$value['sh_backlength'].'</td>';
                                    }
                            $html.= '</tr>
                                <tr>
                                    <td>Chest</td>';

                                    foreach ($standard_data as $key => $value){ 

                            $html.= '<td class="std_number">'.$value['sh_chest'].'</td>';
                                    }
                            $html.= '</tr>
                                <tr>
                                    <td>Sleeve</td>';

                                    foreach ($standard_data as $key => $value){ 

                            $html.= '<td class="std_number">'.$value['sh_sleeve'].'</td>';
                                    }
                            $html.= '</tr>
                                <tr>
                                    <td>Stomach</td>';

                                    foreach ($standard_data as $key => $value){ 

                            $html.= '<td class="std_number">'.$value['sh_stomach'].'</td>';
                                    }
                            $html.= '</tr>
                                <tr>
                                    <td>Seat</td>';

                                    foreach ($standard_data as $key => $value){ 

                            $html.= '<td class="std_number">'.$value['sh_seat'].'</td>';
                                    }
                            $html.= '</tr>
                                <tr>
                                    <td>Bicep Width</td>';

                                    foreach ($standard_data as $key => $value){ 

                            $html.= '<td class="std_number">'.$value['sh_bicepwidth'].'</td>';
                                    }
                            $html.= '</tr>
                                <tr>
                                    <td>Wrist Width</td>';

                                    foreach ($standard_data as $key => $value){ 

                            $html.= '<td class="std_number">'.$value['sh_wristwidth'].'</td>';
                                    }
                            $html.= '</tr>

                            </tbody>
                    </table>';

                    $size_html='';
                    foreach ($standard_data as $key => $value) {
                        
                    $size_html.= '<option class="size_option" value="'.$value['size_name'].'">'.$value['size_name'].'</option>';
                     }                                    

                    $saveStd_html='';
                    $saveStd_html.= '<hr>
                            <script>
                              var pType = "'.$category.'"
                              var gName = "'.@$gender_name.'"
                            </script>
                            <a href="javascript:void(0);" class="btn btn-success pull-right" onclick="saveStdSizeData(pType,gName);">Save</a>';

                    $saveScan_html='';
                    $saveScan_html.= '<hr>
                            <script>
                              var pType = "'.$category.'"
                              var gName = "'.@$gender_name.'"
                            </script>
                            <a href="javascript:void(0);" class="btn btn-success pull-right" onclick="saveScanSizeData(pType,gName);">Save</a>';

            }
            else if($category=="Suit" || $category=="suit")
            {
                $scanForm_html ='<div class="form-group">
                  <label for="inputPassword3" class="col-xs-2 control-label"> <i class="fa fa-arrow-circle-right text-success"> </i> &nbsp; </label>
                  <div class="col-xs-10">  <label class="control-label"> Enter Suit Measurements</label> </div>
              </div>
              <hr>

              <div class="col-xs-12 col-sm-6" id="jacketScanSizes">
                  <div class="form-group">

                      <label for="su_chest" class="col-xs-5 control-label">Chest</label>
                      <div class="col-xs-6">
                          <input type="text" id="su_chest" parent="Shirt" name="su_chest" class="form-control scan_input scan_number"  value="'. @$measurementArray['su_chest'].'" placeholder="Chest">
                          <span class="su_chest"></span>
                      </div>
                      <div class="col-xs-7"> </div>
                  </div>

                  <div class="form-group">
                      <label for="su_stomach" class="col-xs-5 control-label">Stomach</label>

                      <div class="col-xs-6">
                          <input type="text" id="su_stomach" parent="Shirt" name="su_stomach" class="form-control scan_input scan_number" value="'. @$measurementArray['su_stomach'].'" placeholder="Stomach">
                          <span class="su_stomach"></span>
                      </div>
                      <div class="col-xs-7"> </div>
                  </div>
                  <div class="form-group">
                      <label for="su_sleevelength" class="col-xs-5 control-label">Sleeve length</label>
                      <div class="col-xs-6">
                          <input type="text" id="su_sleevelength" name="su_sleevelength" parent="Shirt" class="form-control scan_input scan_number" value="'. @$measurementArray['su_sleevelength'].'" placeholder="Sleeve length">
                          <span class="su_sleevelength"></span>
                      </div>
                      <div class="col-xs-7"> </div>
                  </div>
                  <div class="form-group">
                      <label for="su_shoulder" class="col-xs-5 control-label">Shoulder</label>
                      <div class="col-xs-6">
                          <input type="text" id="su_shoulder" parent="Shirt" name="su_shoulder" class="form-control scan_input scan_number" value="'. @$measurementArray['su_shoulder'].'" placeholder="Shoulder">
                          <span class="su_shoulder"></span>
                      </div>
                      <div class="col-xs-7"> </div>
                  </div>
                  <div class="form-group">
                      <label for="su_front_jacket_length" class="col-xs-5 control-label">Front Jacket Length</label>
                      <div class="col-xs-6">
                          <input type="text" id="su_front_jacket_length" name="su_front_jacket_length" parent="Shirt" class="form-control scan_input scan_number"  placeholder="Front Jacket Length" value="'. @$measurementArray['su_front_jacket_length'].'">
                          <span class="su_front_jacket_length"></span>
                      </div>
                      <div class="col-xs-7"> </div>
                  </div>
              </div>    
              <div class="col-xs-12 col-sm-6" id="jacketScanSizes">
                  <div class="form-group">
                      <label for="su_back_jacket_length" class="col-xs-5 control-label">Back jacket length</label>
                      <div class="col-xs-6">
                          <input type="text" id="su_back_jacket_length" name="su_back_jacket_length" parent="Shirt" class="form-control scan_input scan_number"  placeholder="Back jacket length" value="'. @$measurementArray['su_back_jacket_length'].'">
                          <span class="su_back_jacket_length"></span>
                      </div>
                      <div class="col-xs-7"> </div>
                  </div>
                  <div class="form-group">
                      <label for="su_bicep_width" class="col-xs-5 control-label">Bicep Width</label>
                      <div class="col-xs-6">
                          <input type="text" id="su_bicep_width" name="su_bicep_width" parent="Shirt" class="form-control scan_input scan_number"  placeholder="Bicep Width" value="'. @$measurementArray['su_bicep_width'].'">
                          <span class="su_bicep_width"></span>
                      </div>
                      <div class="col-xs-7"> </div>
                  </div>
                  <div class="form-group">
                      <label for="su_wrist_width" class="col-xs-5 control-label">Wrist Width</label>
                      <div class="col-xs-6">
                          <input type="text" id="su_wrist_width" name="su_wrist_width" parent="Shirt" class="form-control scan_input scan_number"  placeholder="Wrist Width" value="'. @$measurementArray['su_wrist_width'].'">
                          <span class="su_wrist_width"></span>
                      </div>
                      <div class="col-xs-7"> </div>
                  </div>
                  <div class="form-group">
                      <label for="su_seat" class="col-xs-5 control-label">Seat</label>
                      <div class="col-xs-6">
                          <input type="text" id="su_seat" name="su_seat" parent="Shirt" class="form-control scan_input scan_number"  placeholder="Seat" value="'. @$measurementArray['su_seat'].'">
                          <span class="su_seat"></span>
                      </div>
                      <div class="col-xs-7"> </div>
                  </div>
              </div>

              <div class="col-sm-12 form-group">
                  <label for="inputPassword3" class="col-xs-2 control-label"> <i class="fa fa-arrow-circle-right text-success"> </i> &nbsp; </label>
                  <div class="col-xs-10">  <label class="control-label"> Enter Pant Measurements</label> </div>
              <hr>
              </div>

              <div class="col-xs-12 col-sm-6" id="jacketScanSizes">
                  <div class="form-group">

                      <label for="pa_trouser_waist" class="col-xs-5 control-label">Trouser waist</label>
                      <div class="col-xs-6">
                          <input type="text" id="pa_trouser_waist" parent="Shirt" name="pa_trouser_waist" class="form-control scan_input scan_number"  value="'.@$measurementArray['pa_trouser_waist'].'" placeholder="Trouser waist">
                          <span class="pa_trouser_waist"></span>
                      </div>
                      <div class="col-xs-7"> </div>
                  </div>

                  <div class="form-group">
                      <label for="pa_seat" class="col-xs-5 control-label">Seat</label>

                      <div class="col-xs-6">
                          <input type="text" id="pa_seat" parent="Shirt" name="pa_seat" class="form-control scan_input scan_number" value="'.@$measurementArray['pa_seat'].'" placeholder="Seat">
                          <span class="pa_seat"></span>
                      </div>
                      <div class="col-xs-7"> </div>
                  </div>
                  <div class="form-group">
                      <label for="pa_outseam" class="col-xs-5 control-label">Outseam</label>
                      <div class="col-xs-6">
                          <input type="text" id="pa_outseam" name="pa_outseam" parent="Shirt" class="form-control scan_input scan_number" value="'.@$measurementArray['pa_outseam'].'" placeholder="Outseam">
                          <span class="pa_outseam"></span>
                      </div>
                      <div class="col-xs-7"> </div>
                  </div>
                  <div class="form-group">
                      <label for="pa_thigh" class="col-xs-5 control-label">Thigh</label>
                      <div class="col-xs-6">
                          <input type="text" id="pa_thigh" parent="Shirt" name="pa_thigh" class="form-control scan_input scan_number" value="'.@$measurementArray['pa_thigh'].'" placeholder="Thigh">
                          <span class="pa_thigh"></span>
                      </div>
                      <div class="col-xs-7"> </div>
                  </div>
              </div>    
              <div class="col-xs-12 col-sm-6" id="jacketScanSizes">
                  <div class="form-group">
                      <label for="pa_knee" class="col-xs-5 control-label">Knee</label>
                      <div class="col-xs-6">
                          <input type="text" id="pa_knee" name="pa_knee" parent="Shirt" class="form-control scan_input scan_number"  placeholder="Knee" value="'.@$measurementArray['pa_knee'].'">
                          <span class="pa_knee"></span>
                      </div>
                      <div class="col-xs-7"> </div>
                  </div>
                  <div class="form-group">
                      <label for="pa_ankle" class="col-xs-5 control-label">Ankle</label>
                      <div class="col-xs-6">
                          <input type="text" id="pa_ankle" name="pa_ankle" parent="Shirt" class="form-control scan_input scan_number"  placeholder="Ankle" value="'. @$measurementArray['pa_ankle'].'">
                          <span class="pa_ankle"></span>
                      </div>
                      <div class="col-xs-7"> </div>
                  </div>
                  <div class="form-group">
                      <label for="pa_crotch" class="col-xs-5 control-label">Crotch</label>
                      <div class="col-xs-6">
                          <input type="text" id="pa_crotch" name="pa_crotch" parent="Shirt" class="form-control scan_input scan_number"  placeholder="Crotch" value="'. @$measurementArray['pa_crotch'].'">
                          <span class="pa_crotch"></span>
                      </div>
                      <div class="col-xs-7"> </div>
                  </div>
              </div>

              <div class="col-sm-12 form-group">
                  <label for="inputPassword3" class="col-xs-2 control-label"> <i class="fa fa-arrow-circle-right text-success"> </i> &nbsp; </label>
                  <div class="col-xs-10">  <label class="control-label"> Enter Waistcoat Measurements</label> </div>
              <hr>
              </div>

              <div class="col-xs-12 col-sm-6" id="jacketScanSizes">
                  <div class="form-group">

                      <label for="wc_chest" class="col-xs-5 control-label">Chest</label>
                      <div class="col-xs-6">
                          <input type="text" id="wc_chest" parent="Shirt" name="wc_chest" class="form-control scan_input scan_number"  value="'.@$measurementArray['wc_chest'].'" placeholder="Chest">
                          <span class="wc_chest"></span>
                      </div>
                      <div class="col-xs-7"> </div>
                  </div>

                  <div class="form-group">
                      <label for="wc_backlength" class="col-xs-5 control-label">Back length</label>

                      <div class="col-xs-6">
                          <input type="text" id="wc_backlength" parent="Shirt" name="wc_backlength" class="form-control scan_input scan_number" value="'. @$measurementArray['wc_backlength'].'" placeholder="Back length">
                          <span class="wc_backlength"></span>
                      </div>
                      <div class="col-xs-7"> </div>
                  </div>
                  <div class="form-group">
                      <label for="wc_waist" class="col-xs-5 control-label">Waist</label>
                      <div class="col-xs-6">
                          <input type="text" id="wc_waist" name="wc_waist" parent="Shirt" class="form-control scan_input scan_number" value="'.@$measurementArray['wc_waist'].'" placeholder="Waist">
                          <span class="wc_waist"></span>
                      </div>
                      <div class="col-xs-7"> </div>
                  </div>
              </div>';   

                $html = '<table class="table table-bordered size">
                            <thead>
                                <tr>
                                    <th style="font-weight:bold">Suit</th>
                                </tr>
                                <tr>
                                    <th>SIZE</th>'; 

                                foreach ($standard_data as $key => $value) { 
                                    
                            $html.= '<th>' .$value['size_name'].'</th>';
                                    }
                            $html.= '</tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Chest</td>';

                                    foreach ($standard_data as $key => $value){ 

                            $html.= '<td class="std_number">'.$value['su_chest'].'</td>';
                                    }
                            $html.= '</tr>

                                <tr>
                                    <td>Stomach</td>';

                                    foreach ($standard_data as $key => $value){ 

                            $html.= '<td class="std_number">'.$value['su_stomach'].'</td>';
                                    }
                            $html.= '</tr>
                                <tr>
                                    <td>Sleeve Length</td>';

                                    foreach ($standard_data as $key => $value){ 

                            $html.= '<td class="std_number">'.$value['su_sleevelength'].'</td>';
                                    }
                            $html.= '</tr>
                                <tr>
                                    <td>Shoulder</td>';

                                    foreach ($standard_data as $key => $value){ 

                            $html.= '<td class="std_number">'.$value['su_shoulder'].'</td>';
                                    }
                            $html.= '</tr>
                                <tr>
                                    <td>Front Jacket Length</td>';

                                    foreach ($standard_data as $key => $value){ 

                            $html.= '<td class="std_number">'.$value['su_front_jacket_length'].'</td>';
                                    }
                            $html.= '</tr>
                                <tr>
                                    <td>Back Jacket Length</td>';

                                    foreach ($standard_data as $key => $value){ 

                            $html.= '<td class="std_number">'.$value['su_back_jacket_length'].'</td>';
                                    }
                            $html.= '</tr>
                                <tr>
                                    <td>Bicep Width</td>';

                                    foreach ($standard_data as $key => $value){ 

                            $html.= '<td class="std_number">'.$value['su_bicep_width'].'</td>';
                                    }
                            $html.= '</tr>
                                <tr>
                                    <td>Wrist Width</td>';

                                    foreach ($standard_data as $key => $value){ 

                            $html.= '<td class="std_number">'.$value['su_wrist_width'].'</td>';
                                    }
                            $html.= '</tr>
                                <tr>
                                    <td>Seat</td>';

                                    foreach ($standard_data as $key => $value){ 

                            $html.= '<td class="std_number">'.$value['su_seat'].'</td>';
                                    }
                            $html.= '</tr>

                            </tbody>
                    </table>';

                $html.= '<table class="table table-bordered size">
                            <thead>
                                <tr>
                                    <th style="font-weight:bold">Pant</th>
                                </tr>
                                <tr>
                                    <th>SIZE</th>'; 

                                foreach ($standard_data as $key => $value) { 
                                    
                            $html.= '<th>' .$value['size_name'].'</th>';
                                    }
                            $html.= '</tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Trouser Waist</td>';

                                    foreach ($standard_data as $key => $value){ 

                            $html.= '<td class="std_number">'.$value['pa_trouser_waist'].'</td>';
                                    }
                            $html.= '</tr>

                                <tr>
                                    <td>Seat</td>';

                                    foreach ($standard_data as $key => $value){ 

                            $html.= '<td class="std_number">'.$value['pa_seat'].'</td>';
                                    }
                            $html.= '</tr>
                                <tr>
                                    <td>Outseam</td>';

                                    foreach ($standard_data as $key => $value){ 

                            $html.= '<td class="std_number">'.$value['pa_outseam'].'</td>';
                                    }
                            $html.= '</tr>
                                <tr>
                                    <td>Thigh</td>';

                                    foreach ($standard_data as $key => $value){ 

                            $html.= '<td class="std_number">'.$value['pa_thigh'].'</td>';
                                    }
                            $html.= '</tr>
                                <tr>
                                    <td>Knee</td>';

                                    foreach ($standard_data as $key => $value){ 

                            $html.= '<td class="std_number">'.$value['pa_knee'].'</td>';
                                    }
                            $html.= '</tr>
                                <tr>
                                    <td>Ankle</td>';

                                    foreach ($standard_data as $key => $value){ 

                            $html.= '<td class="std_number">'.$value['pa_ankle'].'</td>';
                                    }
                            $html.= '</tr>
                                <tr>
                                    <td>Crotch</td>';

                                    foreach ($standard_data as $key => $value){ 

                            $html.= '<td class="std_number">'.$value['pa_crotch'].'</td>';
                                    }
                            $html.= '</tr>

                            </tbody>
                    </table>';

                $html.= '<table class="table table-bordered size">
                            <thead>
                                <tr>
                                    <th style="font-weight:bold">Waistcoat</th>
                                </tr>
                                <tr>
                                    <th>SIZE</th>'; 

                                foreach ($standard_data as $key => $value) { 
                                    
                            $html.= '<th>' .$value['size_name'].'</th>';
                                    }
                            $html.= '</tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Chest</td>';

                                    foreach ($standard_data as $key => $value){ 

                            $html.= '<td class="std_number">'.$value['wc_chest'].'</td>';
                                    }
                            $html.= '</tr>

                                <tr>
                                    <td>Back Length</td>';

                                    foreach ($standard_data as $key => $value){ 

                            $html.= '<td class="std_number">'.$value['wc_backlength'].'</td>';
                                    }
                            $html.= '</tr>
                                <tr>
                                    <td>Waist</td>';

                                    foreach ($standard_data as $key => $value){ 

                            $html.= '<td class="std_number">'.$value['wc_waist'].'</td>';
                                    }
                       $html.= '</tr>

                            </tbody>
                    </table>';
                    

                $size_html='';
                    foreach ($standard_data as $key => $value) {
                        
                    $size_html.= '<option class="size_option" value="'.$value['size_name'].'">'.$value['size_name'].'</option>';
                     }                                    

                    $saveStd_html='';
                    $saveStd_html.= '<hr>
                            <script>
                              var pType = "'.$category.'"
                              var gName = "'.@$gender_name.'"
                            </script>
                            <a href="javascript:void(0);" class="btn btn-success pull-right" onclick="saveStdSizeData(pType,gName);">Save</a>';

                    
                    $saveScan_html='';
                    $saveScan_html.= '<hr>
                            <script>
                              var pType = "'.$category.'"
                              var gName = "'.@$gender_name.'"
                            </script>
                            <a href="javascript:void(0);" class="btn btn-success pull-right" onclick="saveScanSizeData(pType,gName);">Save</a>';


            }

            $arr['rtw_measurement'] =$rtw_measurement;
            $arr['final_last_order_data'] = (isset($final_last_order_data))?$final_last_order_data:'';
            $arr['measurement_html'] = $html;
            $arr['body_measurements'] = $measurementArray;
            $arr['measurement_data'] = $standard_data;
            $arr['size_html'] = $size_html;
            $arr['saveStd_html'] = $saveStd_html;
            $arr['saveScan_html'] = $saveScan_html;
            $arr['scanForm_html'] = $scanForm_html;
            $arr['radioRow_html'] = $radioRow_html;


        }
        else if ($data['reqType'] == 'design') {

            $html = "";
            $dsignmodel = $this->_designfactory->create();
            $collection = $dsignmodel->getCollection()->addFieldToFilter('design_id', array('eq' => $item_id));
            $arr = array();

            foreach ($collection as $item) {
                $json = json_decode($item->getJson(), true);
                $arr['img'] = $baseUrl . $json['product'];
                $temp['styles'] = $json['display_style'];
                $arr['styles'] = $temp;

                /* Displaying Customized Styles */
                $type = $json['type'];
                $tempStyles = $json['display_style'];
                foreach ($tempStyles as $key => $styles) {
                    if (is_array($styles) || is_object($styles)) {
                    $html .= "<tr style='background-color: #b38d4a;color: white;'><td colspan=2 style='text-align: center;text-transform: uppercase;'><strong>" . $key . "</strong></td></tr>";
                        foreach ($styles as $key => $style) {
                            $html .= "<tr>";
                            if ($key == "fabric") {
                                $label = $style['label'];
                                $fabricId = $style['value']['id'];
                                $fabricName = $style['value']['name'];
                                $fabricImage = $style['value']['image'];
                                $html .= "<td>" . $label . "</td>"
                                        . "<td style='text-align:center'>
                                        <img src='" . $fabricImage . "' width='100px'></td>";
                            } 
                            elseif ($key == "accent") {
                                if ($type == "Suit" || $type == "WomenSuit") {
                                    foreach ($style as $key => $accent) {

                                        if ($key == "monogram") {
                                            $html .= "<tr>";
                                            $label = $accent['label'];
                                            $value = $accent['value'];
                                            $html .= "<td>" . $label . "</td><td><strong>Text : </strong>" . $value['Text'] . ""
                                                    . "<br><strong>Color : </strong>" . $value['Color'] . ""
                                                    . "<br><strong>Font : </strong>" . $value['Font'] . ""
                                                    . "<br><strong>Position : </strong>" . $value['Position'] . ""
                                                    . "</td>";
                                            $html .= "</tr>";
                                        } else {

                                            $html .= "<tr>";
                                            $label = $accent['label'];
                                            $value = $accent['value'];
                                            $html .= "<td>" . $label . "</td><td>" . $value . "</td>";
                                            $html .= "</tr>";
                                        }
                                    }
                                } elseif ($type == "Shirt" || $type == "WomenShirt") {
                                    foreach ($style as $key => $accent) {
                                        if ($key == "monogram") {
                                            $html .= "<tr>";
                                            $label = $accent['label'];
                                            $value = $accent['value'];
                                            $html .= "<td>" . $label . "</td><td><strong>Text : </strong>" . $value['Text'] . ""
                                                    . "<br><strong>Color : </strong>" . $value['Color'] . ""
                                                    . "<br><strong>Font : </strong>" . $value['Font'] . ""
                                                    . "<br><strong>Position : </strong>" . $value['Position'] . ""
                                                    . "</td>";
                                            $html .= "</tr>";
                                        } else {
                                            $html .= "<tr>";
                                            $label = $accent['label'];
                                            $value = $accent['value'];
                                            if (array_key_exists('fabric', $accent)) {
                                                $fabric = $accent['fabric'];
                                                $fabricId = $accent['fabric']['id'];
                                                $fabricName = $accent['fabric']['name'];
                                                $fabricImage = $accent['fabric']['image'];
                                                $html .= "<td>" . $label . "</td>";
                                                $html .= "<td>" . $value . "<br><br><img src='" . $fabricImage . "' width='100px'></td>";
                                            } else
                                                $html .= "<td>" . $label . "</td><td>" . $value . "</td>";

                                            $html .= "</tr>";
                                        }
                                    }
                                }
                            } 
                            else {
                                $label = $style['label'];
                                $value = $style['value'];
                                $html .= "<td>" . $label . "</td><td>" . $value . "</td>";
                            }
                            $html .= "</tr>";
                        }
                    }
                }
                $arr['styles'] = $html;
            }



        }

        else if($data['reqType'] == 'rtw_cart') {
            $quoteItem = $this->quoteItemFactory->create();
            $collection = $quoteItem->getCollection()->addFieldToFilter('item_id', array('eq' => $item_id));
            $json = $data['jsonData'];
            $arr['rtw_measurement'] = $json;
            foreach ($collection as $item) {
                $item->setJson(json_encode($arr));
                $item->save();
            }
        } 

        $result = $this->jsonFactory->create();
        return $result->setData($arr);
    }

}
