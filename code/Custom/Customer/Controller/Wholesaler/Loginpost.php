<?php

namespace Custom\Customer\Controller\Wholesaler;

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\RequestInterface;
use Magento\Customer\Model\Account\Redirect as AccountRedirect;
use Magento\Customer\Model\Session;
use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Model\Url as CustomerUrl;
use Magento\Framework\Exception\EmailNotConfirmedException;
use Magento\Framework\Exception\AuthenticationException;
use Magento\Framework\Data\Form\FormKey\Validator;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\State\UserLockedException;
use Magento\Framework\App\Config\ScopeConfigInterface;

class Loginpost extends \Magento\Framework\App\Action\Action {

    const WHOLESALERID = 2;

    public function __construct(
    Context $context, Session $customerSession, AccountManagementInterface $customerAccountManagement, CustomerUrl $customerHelperData, Validator $formKeyValidator, AccountRedirect $accountRedirect) {
        $this->session = $customerSession;
        $this->customerAccountManagement = $customerAccountManagement;
        $this->customerUrl = $customerHelperData;
        $this->formKeyValidator = $formKeyValidator;
        $this->accountRedirect = $accountRedirect;
        parent::__construct($context);
    }

    public function execute() {
//        if ($this->session->isLoggedIn() || !$this->formKeyValidator->validate($this->getRequest())) {
//            /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
//            $resultRedirect = $this->resultRedirectFactory->create();
//            $resultRedirect->setPath('*/*/');
//            return $resultRedirect;
//        }


        if ($this->getRequest()->isPost()) {
            $login = $this->getRequest()->getPost('login');
            if (!empty($login['username']) && !empty($login['password'])) {
                try {
                    $customer = $this->customerAccountManagement->authenticate($login['username'], $login['password']);
                    if (self::WHOLESALERID != $customer->getGroupId()) {
                        $message = __(
                                'Please sign in as a Customer.'
                        );
                        $this->messageManager->addError($message);
                        $resultRedirect = $this->resultRedirectFactory->create();
                        $resultRedirect->setPath('customer/account/login/');
                        return $resultRedirect;
                    }

                    $this->session->setCustomerDataAsLoggedIn($customer);
                    $this->session->regenerateId();
                    $redirectUrl = $this->accountRedirect->getRedirectCookie();
//                    if (!$this->getScopeConfig()->getValue('customer/startup/redirect_dashboard') && $redirectUrl) {
//                        $this->accountRedirect->clearRedirectCookie();
//                        $resultRedirect = $this->resultRedirectFactory->create();
//                        // URL is checked to be internal in $this->_redirect->success()
//                        $resultRedirect->setUrl($this->_redirect->success($redirectUrl));
////                        return $resultRedirect;
//                        $resultRedirect = $this->resultRedirectFactory->create();
//                        $resultRedirect->setPath('*/*/customer/account/');
//                        return $resultRedirect;
//                    }
                } catch (EmailNotConfirmedException $e) {
                    $value = $this->customerUrl->getEmailConfirmationUrl($login['username']);
                    $message = __(
                            'This account is not confirmed. <a href="%1">Click here</a> to resend confirmation email.', $value
                    );
                } catch (UserLockedException $e) {
                    $message = __(
                            'You did not sign in correctly or your account is temporarily disabled.'
                    );

                        $this->messageManager->addError($message);
                        $resultRedirect = $this->resultRedirectFactory->create();
                        $resultRedirect->setPath('c_customer/wholesaler/login/');
                        return $resultRedirect;


                } catch (AuthenticationException $e) {
                    $message = __('You did not sign in correctly or your account is temporarily disabled.');

                        $this->messageManager->addError($message);
                        $resultRedirect = $this->resultRedirectFactory->create();
                        $resultRedirect->setPath('c_customer/wholesaler/login/');
                        return $resultRedirect;


                } catch (LocalizedException $e) {
                    $message = $e->getMessage();

                        $this->messageManager->addError($message);
                        $resultRedirect = $this->resultRedirectFactory->create();
                        $resultRedirect->setPath('c_customer/wholesaler/login/');
                        return $resultRedirect;


                } catch (\Exception $e) {
                    // PA DSS violation: throwing or logging an exception here can disclose customer password
                    $this->messageManager->addError(
                            __('An unspecified error occurred. Please contact us for assistance.')
                    );
                } finally {
                    if (isset($message)) {
                        $this->messageManager->addError($message);
                        $this->session->setUsername($login['username']);
                    }
                }
            } else {
                $this->messageManager->addError(__('A login and a password are required.'));
            }
        }
        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath('customer/account/');
        return $resultRedirect;
    }

}
