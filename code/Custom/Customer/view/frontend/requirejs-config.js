/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

var config = {
   map: {
        "*": {
            theCustomNameValidationMethod: "Custom_Customer/js/theCustomNameValidationRule"
        }
    },
   deps: ["Custom_Customer/js/custom_cc"]
};
