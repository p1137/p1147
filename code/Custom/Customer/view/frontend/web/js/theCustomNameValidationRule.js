define([
    'jquery',
    'jquery/ui',
    'jquery/validate',
    'mage/translate'
], function($){
    'use strict';
 
    return function() {
        $.validator.addMethod(
            "filter_name",
            function(value, element) {
                if (/^[A-Za-z\s]{1,}[\.]{0,1}[A-Za-z\s]{0,}$/.test(value)){
                    return true;
                }else{
                    return false;
                }
            },
            $.mage.__("Invalid name field")
        );
    }
});