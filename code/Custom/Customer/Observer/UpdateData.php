<?php

namespace Custom\Customer\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Checkout\Model\Cart as CustomerCart;

class UpdateData implements ObserverInterface {

    const TYPEMENSHIRT = "Shirt";
    const TYPEMENSUIT = "Suit";
    const TYPEWOMENSHIRT = "WomenShirt";
    const TYPEWOMENSUIT = "WomenSuit";

    protected $_request;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;

    /**
     * @var \Magento\Checkout\Model\Cart
     */
    protected $cart;

    public function __construct(
    \Magento\Framework\App\RequestInterface $request, \Magento\Checkout\Model\Session $checkoutSession, CustomerCart $cart
    ) {
        $this->_request = $request;
        $this->_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->checkoutSession = $checkoutSession;
        $this->cart = $cart;
    }

    public function execute(\Magento\Framework\Event\Observer $observer) {
        $item = $observer->getEvent()->getData('quote_item');
        $check_measurement = json_decode($this->_request->getParam('measurement_hidden'));

        $measurement_array = (array) $check_measurement;
        if (!empty($measurement_array)) {
            $item->setJson(
                    json_encode(array('rtw_measurement' => $measurement_array))
            );
        }

    }



}
