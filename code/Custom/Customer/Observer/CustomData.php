<?php

namespace Custom\Customer\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Checkout\Model\Cart as CustomerCart;
use Psr\Log\LoggerInterface as Logger;

class CustomData implements ObserverInterface {

    const TYPEMENSHIRT = "Shirt";
    const TYPEMENSUIT = "Suit";
    const TYPEWOMENSHIRT = "WomenShirt";
    const TYPEWOMENSUIT = "WomenSuit";

    protected $_request;
    protected $_menShirtFabricCollectionFactory;
    protected $_womenShirtFabricCollectionFactory;
    protected $_menSuitFabricCollectionFactory;
    protected $_womenSuitFabricCollectionFactory;
    protected $_logger;
    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;

    /**
     * @var \Magento\Checkout\Model\Cart
     */
    protected $cart;

    public function __construct(
    \Magento\Framework\App\RequestInterface $request, \Shirt\Tool\Model\ResourceModel\ShirtShirtfabric\CollectionFactory $menShirtFabricCollectionFactory, \Shirt\Shirtfabricwomen\Model\ResourceModel\Shirtfabricwomen\CollectionFactory $womenShirtFabricCollectionFactory, \Suit\Womenjacket\Model\ResourceModel\Womenjacket\CollectionFactory $womenSuitJacketFabricCollectionFactory, \Suit\Womenpant\Model\ResourceModel\Womenpant\CollectionFactory $womenSuitPantFabricCollectionFactory, \Suit\Womenvest\Model\ResourceModel\Womenvest\CollectionFactory $womenSuitVestFabricCollectionFactory, \Suit\Vest\Model\ResourceModel\Vest\CollectionFactory $menSuitVestFabricCollectionFactory, \Suit\Tool\Model\ResourceModel\Tool\CollectionFactory $menSuitJacketFabricCollectionFactory, \Suit\Pant\Model\ResourceModel\Pant\CollectionFactory $menSuitPantFabricCollectionFactory, \Magento\Checkout\Model\Session $checkoutSession, 
        \Magento\Wishlist\Model\ItemFactory $itemFactory,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Wishlist\Model\Wishlist $wishlist,
        CustomerCart $cart,
        Logger $logger
    ) {
        $this->_request = $request;
        $this->_menShirtFabricCollectionFactory = $menShirtFabricCollectionFactory;
        $this->_womenShirtFabricCollectionFactory = $womenShirtFabricCollectionFactory;
        $this->_menSuitJacketFabricCollectionFactory = $menSuitJacketFabricCollectionFactory;
        $this->_womenSuitJacketFabricCollectionFactory = $womenSuitJacketFabricCollectionFactory;
        $this->_menSuitVestFabricCollectionFactory = $menSuitVestFabricCollectionFactory;
        $this->_womenSuitVestFabricCollectionFactory = $womenSuitVestFabricCollectionFactory;
        $this->_menSuitPantFabricCollectionFactory = $menSuitPantFabricCollectionFactory;
        $this->_womenSuitPantFabricCollectionFactory = $womenSuitPantFabricCollectionFactory;
        $this->_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->checkoutSession = $checkoutSession;
        $this->itemFactory = $itemFactory;
        $this->_customerSession = $customerSession;
        $this->cart = $cart;
        $this->wishlist = $wishlist;
        $this->_logger = $logger;
    }

    public function execute(\Magento\Framework\Event\Observer $observer) {

        $wishlist_item_id = $this->_request->getParam('item');
        $wishlist_id = $this->_request->getParam('wishlist_id');
        $qty_array = $this->_request->getParam('qty');
        $reorder_json = $observer->getEvent()->getData('extra');

        $type = $this->_request->getParam('productType');


        if($type){
            if ($type == self::TYPEMENSHIRT || $type == self::TYPEWOMENSHIRT)
                $this->saveShirt($observer);

            else if ($type == self::TYPEMENSUIT || $type == self::TYPEWOMENSUIT)
                $this->saveSuit($observer);
            
        }elseif ($wishlist_item_id || ($wishlist_id && $qty_array)) {
            $product = $this->_request->getParam('product');
            if($product){
                $this->saveReadytowear($observer);
            }else{
                $this->saveWishlistToCart($observer);
            }
        }elseif($reorder_json!=NULL){
            $this->saveReorderToCart($observer,$reorder_json);

        }else {
            $this->saveReadytowear($observer);
        }    
    }

    protected function saveReorderToCart($observer,$reorder_json) {
        if ($reorder_json != "") {
            $json = json_decode($reorder_json['json'],true);
            $price = $reorder_json['price'];
            $item = $observer->getEvent()->getData('quote_item');
            $item = ( $item->getParentItem() ? $item->getParentItem() : $item );
            $item->setCustomPrice($price);
            $item->setOriginalCustomPrice($price);
            $item->setJson(json_encode($json));
            $item->getProduct()->setIsSuperMode(true);
        }
    }

    protected function saveShirt($observer) {
        $DisplayStyleData = array();
        if ($this->_request->getParam("style_data") != "") {
            $styleData = $this->_request->getParam("style_data");
            /* Remove spaces from key */
            foreach ($styleData as $style => $data) {
                $finalStyleData[trim($style)] = $data;
            }
            /* Format the style for display purpose */
            foreach ($finalStyleData as $style => $data) {
                $style = trim($style);
                switch ($style) {
                    case 'Fit':
                        $DisplayStyleData['fit']['label'] = "Fit";
                        $DisplayStyleData['fit']['value'] = $data;
                        break;
                    case 'Pocket':
                        $DisplayStyleData['pocket']['label'] = "Pocket";
                        $DisplayStyleData['pocket']['value'] = $data;
                        break;
                    case 'Plackets':
                        $DisplayStyleData['placket']['label'] = 'Placket';
                        $DisplayStyleData['placket']['value'] = $data;
                        break;
                    case 'Bottom':
                        $DisplayStyleData['bottom']['label'] = 'Bottom';
                        $DisplayStyleData['bottom']['value'] = $data;
                        break;
                    case 'Sleeves':
                        $DisplayStyleData['sleeve']['label'] = 'Sleeve';
                        $DisplayStyleData['sleeve']['value'] = $data;
                        break;
                    case 'Cuffs':
                        $DisplayStyleData['cuff']['label'] = 'Cuff';
                        $DisplayStyleData['cuff']['value'] = $data;
                        break;
                    case 'CollarStyle':
                        $DisplayStyleData['collar']['label'] = 'Collar';
                        $DisplayStyleData['collar']['value'] = $data;
                        break;
                    case 'Pleats':
                        $DisplayStyleData['pleats']['label'] = 'Pleats';
                        $DisplayStyleData['pleats']['value'] = $data;
                        break;
                    case 'ShirtFabricId':
                        $DisplayStyleData['fabric']['label'] = 'Fabric';
                        $DisplayStyleData['fabric']['value'] = $this->getFabricData($data);
                        break;
                    case 'ChestpleatsContrast':
                        $DisplayStyleData['accent']['cheastpleats']['label'] = 'Accent Cheast Pleats';
                        $DisplayStyleData['accent']['cheastpleats']['value'] = $data;
                        break;
                    case 'Monogram':
                        $DisplayStyleData['accent']['monogram']['label'] = 'Monogram';
                        $monogramData = explode(" ", $data);
                        foreach ($monogramData as $monogram) {
                            $styles = explode(":", $monogram);
                            $DisplayStyleData['accent']['monogram']['value'][$styles[0]] = $styles[1];
                        }
                        break;
                    case 'CollarStyleContrast':
                        $DisplayStyleData['accent']['collar']['label'] = 'Accent Collar';
                        $DisplayStyleData['accent']['collar']['value'] = $data;
                        if (array_key_exists('CollarStylefabricId', $finalStyleData))
                            $DisplayStyleData['accent']['collar']['fabric'] = $this->getFabricData($finalStyleData['CollarStylefabricId']);
                        break;
                    case 'CuffsContrast':
                        $DisplayStyleData['accent']['cuff']['label'] = 'Accent Cuff';
                        $DisplayStyleData['accent']['cuff']['value'] = $data;
                        if (array_key_exists('CuffsfabricId', $finalStyleData))
                            $DisplayStyleData['accent']['cuff']['fabric'] = $this->getFabricData($finalStyleData['CuffsfabricId']);
                        break;
                    case 'ThreadsContrast':
                        $DisplayStyleData['accent']['thread']['label'] = 'Accent Button Thread Color';
                        $DisplayStyleData['accent']['thread']['value'] = $data;
                        break;
                    case 'ElbowPatchContrast':
                        $DisplayStyleData['accent']['elbowpatch']['label'] = 'Accent Elbowpatch';
                        $DisplayStyleData['accent']['elbowpatch']['value'] = $data;
                        if (array_key_exists('ElbowPatchfabricId', $finalStyleData))
                            $DisplayStyleData['accent']['elbowpatch']['fabric'] = $this->getFabricData($finalStyleData['ElbowPatchfabricId']);
                        break;
                    case 'PlacketsContrast':
                        $DisplayStyleData['accent']['placket']['label'] = 'Accent Placket';
                        $DisplayStyleData['accent']['placket']['value'] = $data;
                        if (array_key_exists('PlacketsfabricId', $finalStyleData))
                            $DisplayStyleData['accent']['placket']['fabric'] = $this->getFabricData($finalStyleData['PlacketsfabricId']);
                        break;
                }
            }

            // Delete previous design before saving edited changes
            // $item_id = $this->_request->getParam('itemId');
            // if($item_id){
            //     $allItems = $this->checkoutSession->getQuote()->getAllVisibleItems();
            //     foreach ($allItems as $item) {
            //         $itemId = $item->getItemId();
            //         if($itemId == $item_id){
            //             // echo "hii";die;
            //         // $this->cart->removeItem($itemId)->save();
            //         }
            //     }
            // }

            $item = $observer->getEvent()->getData('quote_item');
            $item = ( $item->getParentItem() ? $item->getParentItem() : $item );
            $price = $this->_request->getParam('price');
            $item->setCustomPrice($price);
            $item->setOriginalCustomPrice($price);
            $type = $this->_request->getParam('productType');
            $json_img = $this->_request->getParam('prodImg');
            $image = str_replace('data:image/png;base64,', '', $json_img);
            $name = 'img_' . time();

            $directory = $this->_objectManager->get('\Magento\Framework\Filesystem\DirectoryList');

            $fp = fopen($directory->getRoot() . "/pub/media/shirt-tool/tool_data/img/customize-image/" . $name . ".png", "w+");

            fwrite($fp, base64_decode($image));
            fclose($fp);

            $product_image = "pub/media/shirt-tool/tool_data/img/customize-image/" . $name . ".png";
            $FinalDisplayStyleData['shirt'] = $DisplayStyleData;
            $item->setJson(
                    json_encode(array('product' => $product_image, 'style' => $styleData, 'size' => $this->_request->getParam('size_Data_Arr'), 'type' => $type, 'display_style' => $FinalDisplayStyleData))
            );
            $item->getProduct()->setIsSuperMode(true);
        }
    }

    protected function saveSuit($observer) {
        if ($this->_request->getParam("style_data") != "") {
            $styleData = $this->_request->getParam("style_data");
            /* Remove spaces from key */
            foreach ($styleData as $style => $data) {
                $finalStyleData[trim($style)] = $data;
            }
            /* Format the style for display purpose */
            foreach ($finalStyleData as $style => $data) {
                $style = trim($style);
                switch ($style) {
                    case 'pantfit':
                        $DisplayStyleData['pant']['fit']['label'] = "Fit";
                        $DisplayStyleData['pant']['fit']['value'] = $data;
                        break;
                    case 'pantpockets':
                        $DisplayStyleData['pant']['pocket']['label'] = "Pocket";
                        $DisplayStyleData['pant']['pocket']['value'] = $data;
                        break;
                    case 'pantbackpockets':
                        $DisplayStyleData['pant']['backpocket']['label'] = "Back Pocket";
                        $DisplayStyleData['pant']['backpocket']['value'] = $data;
                        break;
                    case 'pantpleats':
                        $DisplayStyleData['pant']['pleats']['label'] = 'Pleats';
                        $DisplayStyleData['pant']['pleats']['value'] = $data;
                        break;
                    case 'fastening':
                        $DisplayStyleData['pant']['fastening']['label'] = 'Fastening';
                        $DisplayStyleData['pant']['fastening']['value'] = $data;
                        break;
                    case 'pantcuff':
                        $DisplayStyleData['pant']['cuff']['label'] = 'Cuff';
                        $DisplayStyleData['pant']['cuff']['value'] = $data;
                        break;
                    case 'pantCuffSize':
                        $DisplayStyleData['pant']['cuffsize']['label'] = 'Cuff Size';
                        $DisplayStyleData['pant']['cuffsize']['value'] = $data;
                        break;
                    case 'fasteningButton':
                        $DisplayStyleData['pant']['fasteningbutton']['label'] = 'Fastening Button';
                        $DisplayStyleData['pant']['fasteningbutton']['value'] = $data;
                        break;
                    case 'beltloop':
                        $DisplayStyleData['pant']['beltloop']['label'] = 'Beltloop';
                        $DisplayStyleData['pant']['beltloop']['value'] = $data;
                        break;
                    case 'veststyle':
                        $DisplayStyleData['vest']['style']['label'] = 'Style';
                        $DisplayStyleData['vest']['style']['value'] = $data;
                        break;
                    case 'vestlapel':
                        $DisplayStyleData['vest']['lapel']['label'] = 'Lapel';
                        $DisplayStyleData['vest']['lapel']['value'] = $data;
                        break;
                    case 'vestedge':
                        $DisplayStyleData['vest']['edge']['label'] = 'Edge';
                        $DisplayStyleData['vest']['edge']['value'] = $data;
                        break;
                    case 'vestpockets':
                        $DisplayStyleData['vest']['pocket']['label'] = 'Pocket';
                        $DisplayStyleData['vest']['pocket']['value'] = $data;
                        break;
                    case 'breastpocket':
                        $DisplayStyleData['vest']['breastpocket']['label'] = 'Breast Pocket';
                        $DisplayStyleData['vest']['breastpocket']['value'] = $data;
                        break;
                    // case 'ticketpockets':
                    //     $DisplayStyleData['vest']['ticketpockets']['label'] = 'Ticket Pocket';
                    //     $DisplayStyleData['vest']['ticketpockets']['value'] = $data;
                    //     break;
                    case 'vestbuttons':
                        $DisplayStyleData['vest']['button']['label'] = 'Button';
                        $DisplayStyleData['vest']['button']['value'] = $data;
                        break;
                    case 'Style':
                        $DisplayStyleData['jacket']['style']['label'] = 'Style';
                        $DisplayStyleData['jacket']['style']['value'] = $data;
                        break;
                    case 'Pockets':
                        $DisplayStyleData['jacket']['pocket']['label'] = 'Pocket';
                        $DisplayStyleData['jacket']['pocket']['value'] = $data;
                        break;
                    case 'Fit':
                        $DisplayStyleData['jacket']['fit']['label'] = 'Fit';
                        $DisplayStyleData['jacket']['fit']['value'] = $data;
                        break;
                    case 'Lapel':
                        $DisplayStyleData['jacket']['lapel']['label'] = 'Lapel';
                        $DisplayStyleData['jacket']['lapel']['value'] = $data;
                        break;
                    case 'ButtonsColor':
                        $DisplayStyleData['jacket']['buttoncolor']['label'] = 'Button Color';
                        $DisplayStyleData['jacket']['buttoncolor']['value'] = $data;
                        break;
                    case 'backbeltloop':
                        $DisplayStyleData['jacket']['backbeltloop']['label'] = 'Back Beltloop';
                        $DisplayStyleData['jacket']['backbeltloop']['value'] = $data;
                        break;
                    case 'FitBack':
                        $DisplayStyleData['jacket']['fitback']['label'] = 'Back Fit';
                        $DisplayStyleData['jacket']['fitback']['value'] = $data;
                        break;
                    case 'Vents':
                        $DisplayStyleData['jacket']['vent']['label'] = 'Vent';
                        $DisplayStyleData['jacket']['vent']['value'] = $data;
                        break;
                    case 'sleeve_buttons':
                        $DisplayStyleData['jacket']['sleevebutton']['label'] = 'Sleeve Button';
                        $DisplayStyleData['jacket']['sleevebutton']['value'] = $data;
                        break;
                    case 'sleeveButtonsHoles':
                        $DisplayStyleData['jacket']['sleevebuttonholes']['label'] = 'Sleeve Button Holes';
                        $DisplayStyleData['jacket']['sleevebuttonholes']['value'] = $data;
                        break;
                    case 'vestFabricId':
                        $DisplayStyleData['vest']['fabric']['label'] = 'Fabric';
                        $DisplayStyleData['vest']['fabric']['value'] = $this->getSuitFabricData('vest', $data);
                        break;
                    case 'pantFabricId':
                        $DisplayStyleData['pant']['fabric']['label'] = 'Fabric';
                        $DisplayStyleData['pant']['fabric']['value'] = $this->getSuitFabricData('pant', $data);
                        break;
                    case 'jacketFabricId':
                        $DisplayStyleData['jacket']['fabric']['label'] = 'Fabric';
                        $DisplayStyleData['jacket']['fabric']['value'] = $this->getSuitFabricData('jacket', $data);
                        break;
                    case 'backcollar':
                        $DisplayStyleData['jacket']['accent']['backcollar']['label'] = 'Accent Back Collar';
                        $DisplayStyleData['jacket']['accent']['backcollar']['value'] = $data;
                        break;
                    case 'backcollaraccent':
                        $DisplayStyleData['jacket']['accent']['backcollarfabric']['label'] = 'Back Collar Fabric';
                        $DisplayStyleData['jacket']['accent']['backcollarfabric']['value'] = $data;
                        break;
                    case 'threads':
                        $DisplayStyleData['jacket']['accent']['thread']['label'] = 'Accent Button Thread';
                        $DisplayStyleData['jacket']['accent']['thread']['value'] = $data;
                        break;
                    case 'buttons':
                        $DisplayStyleData['jacket']['accent']['button']['label'] = 'Accent Buttons';
                        $DisplayStyleData['jacket']['accent']['button']['value'] = $data;
                        break;
                    case 'pocketsquare':
                        $DisplayStyleData['jacket']['accent']['pocketsquare']['label'] = 'Accent Pocket Square';
                        $DisplayStyleData['jacket']['accent']['pocketsquare']['value'] = $data;
                        break;
                    case 'Tie':
                        $DisplayStyleData['jacket']['accent']['tie']['label'] = 'Accent Tie';
                        $DisplayStyleData['jacket']['accent']['tie']['value'] = $data;
                        break;
                    case 'ManBow':
                        $DisplayStyleData['jacket']['accent']['bow']['label'] = 'Accent Bow';
                        $DisplayStyleData['jacket']['accent']['bow']['value'] = $data;
                        break;
                    case 'liningStyle':
                        $DisplayStyleData['jacket']['accent']['liningstyle']['label'] = 'Lining Style';
                        $DisplayStyleData['jacket']['accent']['liningstyle']['value'] = $data;
                        break;
                    case 'liningFabric':
                        $DisplayStyleData['jacket']['accent']['liningfabric']['label'] = 'Lining Fabric';
                        $DisplayStyleData['jacket']['accent']['liningfabric']['value'] = $data;
                        break;
                    case 'backelbow':
                        $DisplayStyleData['jacket']['accent']['elbow']['label'] = 'Back Elbow';
                        $DisplayStyleData['jacket']['accent']['elbow']['value'] = $data;
                        break;
                    case 'tuxedoFabric':
                        $DisplayStyleData['jacket']['tuxedofabric']['label'] = 'Tuxedo Fabric';
                        $DisplayStyleData['jacket']['tuxedofabric']['value'] = $data;
                        break;
                    case 'TuxedoStyle':
                        $DisplayStyleData['jacket']['tuxedostyle']['label'] = 'Tuxedo Style';
                        $DisplayStyleData['jacket']['tuxedostyle']['value'] = $data;
                        break;
                    case 'waistband':
                        $DisplayStyleData['pant']['waistband']['label'] = 'Waistband';
                        $DisplayStyleData['pant']['waistband']['value'] = $data;
                        break;
                    case 'Monogram':
                        $DisplayStyleData['jacket']['accent']['monogram']['label'] = 'Monogram';
                        $monogramData = explode(" ", $data);
                        foreach ($monogramData as $monogram) {
                            $styles = explode(":", $monogram);
                            $DisplayStyleData['jacket']['accent']['monogram']['value'][$styles[0]] = $styles[1];
                        }
                        break;
                }
            }

            if(isset($DisplayStyleData['jacket'])){
                $FinalDisplayStyleData['jacket']= $DisplayStyleData['jacket'];
            }
            if(isset($DisplayStyleData['pant'])){
                $FinalDisplayStyleData['pant']= $DisplayStyleData['pant'];
            }
            if(isset($DisplayStyleData['vest'])){
                $FinalDisplayStyleData['vest']= $DisplayStyleData['vest'];
            }

            $item = $observer->getEvent()->getData('quote_item');
            $item = ( $item->getParentItem() ? $item->getParentItem() : $item );
            $price = $this->_request->getParam('price');
            $item->setCustomPrice($price);
            $item->setOriginalCustomPrice($price);
            $type = $this->_request->getParam('productType');

            $img = $this->_request->getParam('final_image_data');
            $json_img = $img[1]['dataurl'];

            $image = str_replace('data:image/png;base64,', '', $json_img);
            $name = 'img_' . time();
            $directory = $this->_objectManager->get('\Magento\Framework\Filesystem\DirectoryList');
            $fp = fopen($directory->getRoot() . "/pub/media/suit-tool/tool_data/img/customize-image/" . $name . ".png", "w+");

            fwrite($fp, base64_decode($image));
            fclose($fp);

            $product_image = "pub/media/suit-tool/tool_data/img/customize-image/" . $name . ".png";

            $item->setJson(
                    json_encode(array('product' => $product_image, 'style' => $styleData, 'size' => $this->_request->getParam('size_Data_Arr'), 'type' => $type, 'display_style' => $FinalDisplayStyleData))
            );
            $item->getProduct()->setIsSuperMode(true);
        }
    }

    protected function saveReadytowear($observer) {
        $item = $observer->getEvent()->getData('quote_item');
        $check_measurement = json_decode($this->_request->getParam('measurement_hidden'));
        $measurement_array = (array) $check_measurement;
        if (!empty($measurement_array)) {
            $item->setJson(
                    json_encode(array('rtw_measurement' => $measurement_array))
            );
        }
        $item->getProduct()->setIsSuperMode(true);
    }

    protected function saveWishlistToCart($observer) {
        $item = $observer->getEvent()->getData('quote_item');
        $quote_p_id = $item->getProduct()->getId();
        $quote_wishlist_id = $item->getProduct()->getWishlistId();

        $wishlist_item_id = $this->_request->getParam('item');
        $wishlist_id = $this->_request->getParam('wishlist_id');
        $qty_array = $this->_request->getParam('qty');

        if($wishlist_id && $qty_array){
            foreach ($qty_array as $qkey => $qvalue) {
                $allwishlistitem = $this->itemFactory->create()->load($qkey);
                $wishlist_p_id = $allwishlistitem->getProductId();
                if(($quote_p_id == $wishlist_p_id) && ($quote_wishlist_id == $qkey)){
                    $json = (array)json_decode($allwishlistitem->getJson());
                    if (!empty($json)) {
                        $item->setJson(json_encode($json));
                    }

                }
            }
        }else{
            $wishlistitem = $this->itemFactory->create()->load($wishlist_item_id);
            $json = json_decode($wishlistitem->getJson());
            if (!empty($json)) {
                $item->setJson(
                        json_encode($json)
                );
            }

        }

        $item->getProduct()->setIsSuperMode(true);
    }

    protected function getFabricData($fabricId) {
        $mediaPath = $this->_objectManager->get('Magento\Store\Model\StoreManagerInterface')
                ->getStore()
                ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        $StyleData = array();
        $type = $this->_request->getParam('productType');
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        if ($type == self::TYPEMENSHIRT) {
            $imageUrl = "shirt-tool/"; //append base url
            $shirtCollection = $this->_menShirtFabricCollectionFactory->create();
            $shirtCollection->addFieldToFilter('fabric_id', $fabricId);
        } else if ($type == self::TYPEWOMENSHIRT) {
            $imageUrl = "shirt-tool/"; //append base url
            $shirtCollection = $this->_womenShirtFabricCollectionFactory->create();
            $shirtCollection->addFieldToFilter('shirtfabricwomen_id', $fabricId);
        }
        if (isset($shirtCollection)) {
            foreach ($shirtCollection as $shirt) {
                $StyleData['id'] = $fabricId;
                $StyleData['name'] = $shirt->getTitle();
                if($type == self::TYPEMENSHIRT){
                    $StyleData['image'] = $mediaPath . "shirt-tool/" . $shirt->getDisplayFabricThumb(); 
                }
                else{
                    $StyleData['image'] = $mediaPath . $shirt->getDisplayFabricThumb(); 
                }
            }
        }
        return $StyleData;
    }

    protected function getSuitFabricData($suitType, $fabricId) {
        $mediaPath = $this->_objectManager->get('Magento\Store\Model\StoreManagerInterface')
                ->getStore()
                ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        $StyleData = array();
        $type = $this->_request->getParam('productType');
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        if ($type == self::TYPEMENSUIT) {
            $imageUrl = ""; //append base url
            if ($suitType == 'jacket') {
                $shirtCollection = $this->_menSuitJacketFabricCollectionFactory->create();
                $shirtCollection->addFieldToFilter('id', $fabricId);
            } elseif ($suitType == 'vest') {
                $shirtCollection = $this->_menSuitVestFabricCollectionFactory->create();
                $shirtCollection->addFieldToFilter('id', $fabricId);
            } elseif ($suitType == 'pant') {
                $shirtCollection = $this->_menSuitPantFabricCollectionFactory->create();
                $shirtCollection->addFieldToFilter('id', $fabricId);
            }
        } else if ($type == self::TYPEWOMENSUIT) {
            $imageUrl = ""; //append base url
            if ($suitType == 'jacket') {
                $shirtCollection = $this->_womenSuitJacketFabricCollectionFactory->create();
                $shirtCollection->addFieldToFilter('id', $fabricId);
            } elseif ($suitType == 'vest') {
                $shirtCollection = $this->_womenSuitVestFabricCollectionFactory->create();
                $shirtCollection->addFieldToFilter('id', $fabricId);
            } elseif ($suitType == 'pant') {
                $shirtCollection = $this->_womenSuitPantFabricCollectionFactory->create();
                $shirtCollection->addFieldToFilter('id', $fabricId);
            }
        }
        if (isset($shirtCollection)) {
            foreach ($shirtCollection as $shirt) {
                $StyleData['id'] = $fabricId;
                $StyleData['name'] = $shirt->getName();
                $StyleData['image'] = $mediaPath . $shirt->getDisplayFabricThumb();
            }
        }
        return $StyleData;
    }

}
