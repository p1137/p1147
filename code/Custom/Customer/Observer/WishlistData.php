<?php

namespace Custom\Customer\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Checkout\Model\Cart as CustomerCart;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Stdlib\Cookie\CookieMetadataFactory;
use Magento\Framework\Stdlib\CookieManagerInterface;
use Magento\Framework\Session\SessionManagerInterface;



class WishlistData implements ObserverInterface {

    const MEN_SHIRT_PRODUCT_ID = 'toolConfiguration/general/men_shirt_tool_product';
    const MEN_SUIT_PRODUCT_ID = 'toolConfiguration/general/men_suit_tool_product';
    const WOMEN_SHIRT_PRODUCT_ID = 'toolConfiguration/general/women_shirt_tool_product';
    const WOMEN_SUIT_PRODUCT_ID = 'toolConfiguration/general/women_suit_tool_product';

    protected $_request;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;

    /**
     * @var \Magento\Checkout\Model\Cart
     */
    protected $cart;

    public function __construct(
    \Magento\Framework\App\RequestInterface $request, 
    \Magento\Checkout\Model\Session $checkoutSession, 
    \Magento\Quote\Model\Quote\ItemFactory $quoteItemFactory,
    \Magento\Quote\Model\ResourceModel\Quote\Item $itemResourceModel,
    \Magento\Wishlist\Model\ItemFactory $itemFactory,
    ScopeConfigInterface $scopeConfigInterface,
    CookieManagerInterface $cookieManager,
    CookieMetadataFactory $cookieMetadataFactory,
    SessionManagerInterface $sessionManager,
    \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, 
    \Magento\Wishlist\Model\WishlistFactory $wishlistRepository,
    \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
    \Magento\Quote\Model\QuoteFactory $quoteFactory,
    CustomerCart $cart
    ) {
        $this->_request = $request;
        $this->_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->checkoutSession = $checkoutSession;
        $this->quoteItemFactory = $quoteItemFactory;
        $this->itemResourceModel = $itemResourceModel;
        $this->itemFactory = $itemFactory;
        $this->scopeConfigInterface = $scopeConfigInterface;
        $this->cookieManager = $cookieManager;
        $this->cookieMetadataFactory = $cookieMetadataFactory;
        $this->sessionManager = $sessionManager;
        $this->scopeConfig = $scopeConfig;
        $this->_wishlistRepository= $wishlistRepository;
        $this->_productRepository = $productRepository;
        $this->quoteFactory = $quoteFactory;
        $this->cart = $cart;
    }

    public function execute(\Magento\Framework\Event\Observer $observer) {

        $items = $observer->getEvent()->getData('items');
        $item_id = $this->_request->getParam('item');
        $wishlist_item_id = $this->_request->getParam('id');
        $product_id = $this->_request->getParam('product');

        if($item_id){
            $quoteItem = $this->quoteItemFactory->create();
            $collection = $quoteItem->getCollection()->addFieldToFilter('item_id', array('eq' => $item_id));

            foreach ($collection as $item) {
                $json = json_decode($item->getJson(), true);
                $p_id = $item->getProductId();
                $quote_id = $item->getQuoteId();
            }
            foreach ($items as $item) {
                if(!empty($json)){
                 $item->setJson(json_encode($json));
                 $item->save();
                }else{
                    
                }

            }

        }else if($product_id){
            $check_measurement = json_decode($this->cookieManager->getCookie('my_json'));
            $json = (array) $check_measurement;

            foreach ($items as $item) {
                if(!empty($json)){
                 $item->setJson(json_encode(array('rtw_measurement' => $json)));
                 $item->save();
                 $this->cookieManager->deleteCookie(
                            'my_json',
                            $this->cookieMetadataFactory
                                ->createCookieMetadata()
                                ->setPath($this->sessionManager->getCookiePath())
                                ->setDomain($this->sessionManager->getCookieDomain())
                        );

                }else{
                    
                }

            }

        }

    }

    public function getProductId($scopeProduct) {
        return $this->scopeConfig->getValue($scopeProduct, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }



}
