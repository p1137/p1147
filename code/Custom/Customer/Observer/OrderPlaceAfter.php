<?php 
namespace Custom\Customer\Observer;

use Magento\Framework\Event\ObserverInterface;

class OrderPlaceAfter implements ObserverInterface
{

    protected $_request;

    public function __construct(
    \Magento\Framework\App\RequestInterface $request,
    \Wholesaler\Mgmt\Model\Design $design,
    \Wholesaler\Mgmt\Model\DesignFactory $designfactory
    ) {
        $this->_request = $request;
        $this->design = $design;
        $this->designfactory = $designfactory;
        $this->_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    }

    /**
     * @param Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {   
        $customerSession = $this->_objectManager->get('\Magento\Customer\Model\Session');
        $cust_id = $customerSession->getWholesalerCustomer();
        $event = $observer->getEvent();
        $order = $event->getOrder();
        $wholesaler_id = $order->getCustomerId();


        foreach($order->getAllItems() as $item)
        {
            $json = json_decode($item->getJson(),true);
            if(!is_null($json) && !is_null($cust_id) && $cust_id!=0)
            {
                $order->setWholesalersCustomerId($cust_id);

                // $model = $this->designfactory->create();

                // $model->setJson(json_encode($json));
                // $model->setCustomerId($cust_id);
                // $model->setWholesalerId($wholesaler_id);
                // $model->save();

                $customerSession->unsWholesalerCustomer();

            }
        }
    }

}