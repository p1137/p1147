<?php

namespace Custom\Customer\Block;

class Design extends \Magento\Framework\View\Element\Template
{

  public function __construct(\Magento\Catalog\Block\Product\Context $context, 
  	\Magento\Checkout\Model\CartFactory $cartFactory,
  	array $data = []) {

      $this->_cartFactory = $cartFactory;
      parent::__construct($context, $data);

  }


  protected function _prepareLayout()
  {
      return parent::_prepareLayout();
  }


    public function getUpdatedQty($p_id) {

    	// $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$cart = $this->_cartFactory->create(); 

		// get quote items array
		$items = $cart->getQuote()->getAllItems();

		foreach($items as $item) {
		   $cartItemId = $item->getProductId();
		   if($cartItemId == $p_id){
		    $qty = $item->getQty();
		   }
		}

		return $qty;

    }

}
