<?php

namespace Custom\Customer\Block\Wishlist;

class Index extends \Magento\Framework\View\Element\Template
{

  public function __construct(\Magento\Catalog\Block\Product\Context $context, 
  	\Magento\Checkout\Model\Cart $cart,
    \Magento\Wishlist\Model\ItemFactory $itemFactory,
  	array $data = []) {

      $this->cart = $cart;
      $this->itemFactory = $itemFactory;
      parent::__construct($context, $data);

  }

  protected function _prepareLayout()
  {
      return parent::_prepareLayout();
  }


}
