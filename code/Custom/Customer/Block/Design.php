<?php

namespace Custom\Customer\Block;

class Design extends \Magento\Framework\View\Element\Template
{

  public function __construct(\Magento\Catalog\Block\Product\Context $context, 
  	\Magento\Checkout\Model\Cart $cart,
    \Magento\Wishlist\Model\ItemFactory $itemFactory,
  	array $data = []) {

      $this->cart = $cart;
      $this->itemFactory = $itemFactory;
      parent::__construct($context, $data);

  }


  protected function _prepareLayout()
  {
      return parent::_prepareLayout();
  }


    public function getUpdatedQty($configure_id,$p_id) {

		$quoteItem = $this->cart->getQuote()->getItemById($configure_id);
		$pro_id = $quoteItem->getProduct()->getId();
		$qty = $quoteItem->getBuyRequest()->getQty();

		return $qty;

    }

    public function getUpdatedMeasurement($configure_id) {

    $quoteItem = $this->cart->getQuote()->getItemById($configure_id);
    $wishlistitem = $this->itemFactory->create()->load($configure_id);


    if($quoteItem){
        $json = json_decode($quoteItem->getData('json'),true);
        $json_data = $json['rtw_measurement'];
     }elseif ($wishlistitem) {
        $json = json_decode($wishlistitem->getData('json'),true);
        $json_data = $json['rtw_measurement'];
     }else{
      $json_data = NULL;
     }
         return $json_data;

    }    

}
