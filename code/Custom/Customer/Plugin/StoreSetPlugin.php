<?php

namespace Custom\Customer\Plugin;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;

class StoreSetPlugin extends \Magento\Customer\Controller\Account\EditPost
{

    protected $_request;
    protected $_storeManager;
    protected $_transportBuilder;
    protected $scopeConfig;

    public function __construct(
    \Magento\Framework\App\RequestInterface $request,
    \Magento\Store\Model\StoreManagerInterface $storeManager,
    \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
    \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface,
    \Magento\UrlRewrite\Model\UrlRewriteFactory $urlRewriteFactory,
    \Magento\UrlRewrite\Model\UrlRewrite $urlRewrite,
    ScopeConfigInterface $scopeConfig
    ) {
        $this->_request = $request;
        $this->_storeManager = $storeManager;
        $this->_transportBuilder = $transportBuilder;
        $this->scopeConfig = $scopeConfig;
        $this->_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->_urlRewriteFactory = $urlRewriteFactory;
        $this->_urlRewrite = $urlRewrite;
        $this->_customerRepositoryInterface = $customerRepositoryInterface;
    }

    public function aroundExecute($subject, callable $proceed){

        $originalRequestData = $subject->getRequest()->getPostValue();

        $new_store_name = (isset($originalRequestData['wholesaler_store_name']))?$originalRequestData['wholesaler_store_name']:'';

        if($new_store_name != ''){
            $new_store_name = preg_replace('/\s+/', '-', $new_store_name);
        }

        $customerId = $subject->session->getCustomerId();

        $oldCustomer = $this->_customerRepositoryInterface->getById($customerId);
        $GroupId = $oldCustomer->getGroupId();
        $result = $proceed();

        if($GroupId=='2'){

            if($oldCustomer->getCustomAttribute('wholesaler_store_name')==NULL && $new_store_name!=''){

                $old_url_model = $this->_urlRewrite->getCollection()->addFieldTofilter('request_path',$new_store_name);
                if($old_url_model->getData()){
                    return $result;
                }else{

                    $urlRewriteModel = $this->_urlRewriteFactory->create();
                    /* set current store id */
                    $urlRewriteModel->setStoreId(1);
                    /* this url is not created by system so set as 0 */
                    $urlRewriteModel->setIsSystem(0);
                    /* unique identifier - set random unique value to id path */
                    $urlRewriteModel->setIdPath(rand(1, 100000));
                    /* set actual url path to target path field */
                    $urlRewriteModel->setTargetPath("cms/page/view/page_id/2");
                    /* set requested path which you want to create */
                    $urlRewriteModel->setRequestPath($new_store_name);
                    /* set current store id */
                    $urlRewriteModel->save();
                    return $result;


                }

            }else if($oldCustomer->getCustomAttribute('wholesaler_store_name')==NULL && $new_store_name==''){
                return $result;
            }else{

                $old_store_name = $oldCustomer->getCustomAttribute('wholesaler_store_name')->getValue();

                if($old_store_name != ''){
                    $old_store_name = preg_replace('/\s+/', '-', $old_store_name);
                }

                if($old_store_name != $new_store_name && $new_store_name!=''){

                    $old_url_model = $this->_urlRewrite->getCollection()->addFieldTofilter('request_path',$old_store_name);

                    foreach ($old_url_model as $key => $value) {
                        $url_id = $value->getId();
                        if($url_id){
                            $old_url_model = $this->_urlRewrite->load($url_id);
                            $old_url_model->delete();
                        }
                    }


                    $urlRewriteModel = $this->_urlRewriteFactory->create();
                    /* set current store id */
                    $urlRewriteModel->setStoreId(1);
                    /* this url is not created by system so set as 0 */
                    $urlRewriteModel->setIsSystem(0);
                    /* unique identifier - set random unique value to id path */
                    $urlRewriteModel->setIdPath(rand(1, 100000));
                    /* set actual url path to target path field */
                    $urlRewriteModel->setTargetPath("cms/page/view/page_id/2");
                    /* set requested path which you want to create */
                    $urlRewriteModel->setRequestPath($new_store_name);
                    /* set current store id */
                    $urlRewriteModel->save();
                    return $result;

                }
            }

        }
            
        return $result;
        
    }


}
