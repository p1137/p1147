<?php

namespace Custom\Customer\Plugin\Minicart;


use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product;
use Magento\Quote\Model\Quote\Item;

class Image
{

    protected $productRepo;

    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepo = $productRepository;
    }

    public function aroundGetItemData($subject, \Closure $proceed, Item $item)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
        $result = $proceed($item);


        $data_array = json_decode($item->getData('json'));

        $product = $this->productRepo->getById($item->getProduct()->getId());
        if (is_null($data_array) || array_key_exists("rtw_measurement",$data_array)){
        	 $result['product_image']['src'] = $storeManager->getStore()->getBaseUrl()."pub/media/catalog/product/".$product->getImage($product->getProductForThumbnail());
        }
        else{
            // print_r(json_decode($item->getData('json'))->product);
            $result['product_image']['src'] = $storeManager->getStore()->getBaseUrl().json_decode($item->getData('json'))->product ;
        	 
        
        }
        /** @var Product $product */
              
          return $result;
    }
}

            