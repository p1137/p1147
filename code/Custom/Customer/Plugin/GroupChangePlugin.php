<?php

namespace Custom\Customer\Plugin;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;

class GroupChangePlugin extends \Magento\Customer\Controller\Adminhtml\Index\Save
{

    protected $_request;
    protected $_storeManager;
    protected $_transportBuilder;
    protected $scopeConfig;

    public function __construct(
    \Magento\Framework\App\RequestInterface $request,
    \Magento\Store\Model\StoreManagerInterface $storeManager,
    \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
    ScopeConfigInterface $scopeConfig
    ) {
        $this->_request = $request;
        $this->_storeManager = $storeManager;
        $this->_transportBuilder = $transportBuilder;
        $this->scopeConfig = $scopeConfig;
        $this->_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    }

    public function aroundExecute($subject, callable $proceed){

        $originalRequestData = $subject->getRequest()->getPostValue();
        $customerId = isset($originalRequestData['customer']['entity_id'])
            ? $originalRequestData['customer']['entity_id']
            : null;

        if($customerId){
            $currentCustomer = $subject->_customerRepository->getById($customerId);
            $oldGroupId = $currentCustomer->getGroupId();
            $result = $proceed();
            $newCustomer = $subject->_customerRepository->getById($customerId);
            $newGroupId = $newCustomer->getGroupId();

            if($oldGroupId != $newGroupId){

                $baseUrl = $this->_storeManager->getStore()->getBaseUrl();

                $customerFname = $newCustomer->getFirstname();
                $customerLname = $newCustomer->getLastname();
                $customerName = $customerFname.' '.$customerLname;
                $custmail = $newCustomer->getEmail();
                $custStoreId = $newCustomer->getStoreId();


              if($newGroupId == 2){
                $msg = "Sign in as a Wholesaler";
                $desc = "Your customer group has been changed to Wholesaler. You can manage your account by signing in as a wholesaler. You can also add your customers and manage their orders.";
                $loginUrl = $baseUrl. 'c_customer/wholesaler/login/';

              }else if($newGroupId == 1){
                $msg = "Sign in as a Customer";
                $desc = "Your customer group has been changed to Customer. You can manage your account by signing in as a Customer. You can shop and also manage your orders.";
                $loginUrl = $baseUrl. 'customer/account/login/';

              }else{

              }  

              $this->sendGroupChangeMail($customerName,$custmail,$loginUrl,$custStoreId ,$msg,$desc);
              return $result;

            }else{

                return $result;

            }  

        }else{

            $post = $subject->getRequest()->getPostValue();
            $group_id = $post['customer']['group_id'];

            if($group_id == 2){

                $msg = "Sign in as a Wholesaler";
                $desc = "Your customer group has been changed to Wholesaler. You can manage your account by signing in as a wholesaler. You can also add your customers and manage their orders.";

                $customerFname = $post['customer']['firstname'];
                $customerLname = $post['customer']['lastname'];
                $customerName = $customerFname.' '.$customerLname;
                $custmail = $post['customer']['email'];
                $custStoreId = $post['customer']['sendemail_store_id'];

                $baseUrl = $this->_storeManager->getStore()->getBaseUrl();
                $loginUrl = $baseUrl. 'c_customer/wholesaler/login/';

                $this->sendGroupChangeMail($customerName,$custmail,$loginUrl,$custStoreId ,$msg,$desc);

                 $result = $proceed();
                 return $result;

            }else{
              $result = $proceed();
              return $result;
            }


        }
        
    }

    public function sendGroupChangeMail($customerName,$custmail,$loginUrl,$custStoreId,$msg,$desc)
    {

        $msg = $msg;
        $desc = $desc;
        $customerName  = $customerName;
        $customerEmail = $custmail;
        $loginUrl = $loginUrl;
        $storeId = $custStoreId;

        $sEmail = $this->scopeConfig->getValue('trans_email/ident_support/email',ScopeInterface::SCOPE_STORE);
        $sName  = $this->scopeConfig->getValue('trans_email/ident_support/name',ScopeInterface::SCOPE_STORE);

        $storename = $this->_storeManager->getStore()->getName();


        $transport = $this->_transportBuilder->setTemplateIdentifier('customer_groupchange_template')
            ->setTemplateOptions(['area' => 'frontend' , 'store' => $storeId])
            ->setTemplateVars(
                [
                    'store' => $this->_storeManager->getStore(),
                    'message'   => $msg,
                    'desc'=> $desc,
                    'custname'  => $customerName,
                    'storename' => $storename,
                    'loginUrl' => $loginUrl
                ]
            )
            ->setFrom([

                    'name' => $sName,
                    'email' => $sEmail
            ])
            ->addTo($customerEmail,$customerName)
            ->getTransport();
            try {
                $transport->sendMessage();
            } catch(\Exception $e) {
                
            }

    }





}
