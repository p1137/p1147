<?php

namespace Custom\Customer\Plugin\Order;

use Closure;

class OrderToQuote {

    /**
     * afterAddOrderItem
     *
     * @param \Magento\Checkout\Model\Cart $subject
     * @param \Closure $proceed
     * 
     */
    public function aroundAddOrderItem(\Magento\Checkout\Model\Cart $subject, \Closure $proceed, $orderItem, $qtyFlag = null
    ) {
        /** @var $orderItem Item */
        $cart = $proceed($orderItem, $qtyFlag);
        // if ($orderItem->getJson() != "") {
            
        //     $items = $cart->getItems();

        //     foreach ($items as $item) {
        //         if (($item->getProductId() == $orderItem->getProductId()) && $item->getPrice()==NULL) {
        //             $item->setJson($orderItem->getJson());
        //             $item->setCustomPrice($orderItem->getPrice());
        //             $item->setOriginalCustomPrice($orderItem->getPrice());
        //             $item->setPrice($orderItem->getPrice());
        //             $item->setBasePrice($orderItem->getPrice());
        //             $item->setRowTotal($orderItem->getPrice());
        //             $item->setBaseRowTotal($orderItem->getPrice());
        //             break;
        //         }
        //     }
        //     $cart->save();
        // }
        return $cart;
    }

}
