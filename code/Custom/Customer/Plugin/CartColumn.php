<?php
namespace Custom\Customer\Plugin;
class CartColumn
{
    
    public function afterGetItemRenderer(\Magento\Checkout\Block\Cart\AbstractCart $subject, $result)
    {
        $result->setTemplate('Custom_Customer::default.phtml');
        return $result;
    }
    
}