<?php

namespace Custom\Customer\Plugin;

class ProductHidePlugin 
{
    const MEN_SHIRT_PRODUCT_ID = 'toolConfiguration/general/men_shirt_tool_product';
    const MEN_SUIT_PRODUCT_ID = 'toolConfiguration/general/men_suit_tool_product';
    const WOMEN_SHIRT_PRODUCT_ID = 'toolConfiguration/general/women_shirt_tool_product';
    const WOMEN_SUIT_PRODUCT_ID = 'toolConfiguration/general/women_suit_tool_product';

    protected $scopeConfig;

    public function __construct(
      \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
        $this->scopeConfig = $scopeConfig;
    }

  public function aroundGetData(\Magento\Catalog\Ui\DataProvider\Product\ProductDataProvider $subject, \Closure $proceed){

    $menShirtPid = $this->getProductId(self::MEN_SHIRT_PRODUCT_ID);
    $womenShirtPid = $this->getProductId(self::WOMEN_SHIRT_PRODUCT_ID);
    $menSiutPid = $this->getProductId(self::MEN_SUIT_PRODUCT_ID);
    $womenSuitPid = $this->getProductId(self::WOMEN_SUIT_PRODUCT_ID);
    $pArray = array($menShirtPid,$womenShirtPid,$menSiutPid,$womenSuitPid);

    if (!$subject->getCollection()->isLoaded()) {
            $subject->getCollection()->addFieldToFilter('entity_id', array('nin' => $pArray));
            $subject->getCollection()->load();
        }

        return $proceed();
    }

    public function getProductId($scopeProduct) {
        return $this->scopeConfig->getValue($scopeProduct, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }


}
