<?php
namespace Custom\Customer\Plugin;

use Magento\Framework\App\Response\Http as responseHttp;
use Magento\Framework\UrlInterface;

class Contact {  
  public function __construct(
        responseHttp $response,
        UrlInterface $url
    )
    {
        $this->response = $response;
        $this->_url = $url;
    }
 
    public function afterExecute(\Magento\Contact\Controller\Index\Post $subject , $result)    {
	$url = $this->_url->getUrl('contact-us');
         $this->response->setRedirect($url);	 
         //return $result;
    }
}
