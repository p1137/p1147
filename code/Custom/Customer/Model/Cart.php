<?php

/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Custom\Customer\Model;

/**
 * Shopping cart model
 *
 * @api
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @deprecated 100.1.0 Use \Magento\Quote\Model\Quote instead
 * @see \Magento\Quote\Api\Data\CartInterface
 * @since 100.0.2
 */
class Cart extends \Magento\Checkout\Model\Cart {

    public function addOrderItem($orderItem, $qtyFlag = null) {
        if ($orderItem->getParentItem() === null) {
            $storeId = $this->_storeManager->getStore()->getId();
            try {
                $product = $this->productRepository->getById($orderItem->getProductId(), false, $storeId, true);
            } catch (NoSuchEntityException $e) {
                return $this;
            }
            $info = $orderItem->getProductOptionByCode('info_buyRequest');
            $info = new \Magento\Framework\DataObject($info);
            if ($qtyFlag === null) {
                $info->setQty($orderItem->getQtyOrdered());
            } else {
                $info->setQty(1);
            }

            if($orderItem->getJson() != ""){
                $extra = array('json' => $orderItem->getJson(),'price'=>$orderItem->getPrice());
                $info->setExtra($extra);
            }
            $this->addProduct($product, $info);
        }
        return $this;
    }

    public function addProduct($productInfo, $requestInfo = null) {

        $product = $this->_getProduct($productInfo);
        $request = $this->_getProductRequest($requestInfo);
        $productId = $product->getId();

        if ($productId) {
            $stockItem = $this->stockRegistry->getStockItem($productId, $product->getStore()->getWebsiteId());
            $minimumQty = $stockItem->getMinSaleQty();
            //If product quantity is not specified in request and there is set minimal qty for it
            if ($minimumQty && $minimumQty > 0 && !$request->getQty()
            ) {
                $request->setQty($minimumQty);
            }

            try {
                $this->_eventManager->dispatch(
                        'checkout_cart_product_add_before', ['info' => $requestInfo, 'product' => $product]
                );
                $result = $this->getQuote()->addProduct($product, $request);
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->_checkoutSession->setUseNotice(false);
                $result = $e->getMessage();
            }
            /**
             * String we can get if prepare process has error
             */
            if (is_string($result)) {
                if ($product->hasOptionsValidationFail()) {
                    $redirectUrl = $product->getUrlModel()->getUrl(
                            $product, ['_query' => ['startcustomization' => 1]]
                    );
                } else {
                    $redirectUrl = $product->getProductUrl();
                }
                $this->_checkoutSession->setRedirectUrl($redirectUrl);
                if ($this->_checkoutSession->getUseNotice() === null) {
                    $this->_checkoutSession->setUseNotice(true);
                }
                throw new \Magento\Framework\Exception\LocalizedException(__($result));
            }
        } else {
            throw new \Magento\Framework\Exception\LocalizedException(__('The product does not exist.'));
        }
        $extra = $request->getExtra();
        $this->_eventManager->dispatch(
                'checkout_cart_product_add_after', ['quote_item' => $result, 'product' => $product, 'extra' => $extra]
        );
        $this->_checkoutSession->setLastAddedProductId($productId);
        return $this;
    }

}
