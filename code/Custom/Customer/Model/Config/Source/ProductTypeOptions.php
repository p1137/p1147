<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Custom\Customer\Model\Config\Source;

use Magento\Eav\Model\ResourceModel\Entity\Attribute\OptionFactory;
use Magento\Framework\DB\Ddl\Table;

class ProductTypeOptions extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource {

    protected $_options;

    /**
     * @return array
     */
    public function getAllOptions() {
        /* your Attribute options list */
        $this->_options = [
                ['label' => 'Select Options', 'value' => ''],
                ['label' => 'Shirt', 'value' => '1'],
                ['label' => 'Suit', 'value' => '2'],
                ['label' => 'Accessories', 'value' => '3'],
        ];
        return $this->_options;
    }

}
