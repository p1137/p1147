<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Custom\Customer\Model\Config;

class SuitToolConfig implements \Magento\Framework\Option\ArrayInterface {

    protected $_productCollectionFactory;
    protected $_options;

    public function __construct(
    \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory
    ) {
        $this->_productCollectionFactory = $productCollectionFactory;
    }

    /**
     * @return array
     */
    public function toOptionArray() {
        $_productCollection = $this->_productCollectionFactory->create();
        $_productCollection->addAttributeToSelect('*');
        $this->_options = [['label' => '', 'value' => '']];
        foreach ($_productCollection as $_product) {
            $this->_options[] = [
                'label' => __('%1 Product ', $_product->getName()),
                'value' => $_product->getId()
            ];
        }
        return $this->_options;
    }

}
