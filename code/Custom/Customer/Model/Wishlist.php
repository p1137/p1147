<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Custom\Customer\Model;
/**
 * Wishlist model
 *
 * @method int getShared()
 * @method \Magento\Wishlist\Model\Wishlist setShared(int $value)
 * @method string getSharingCode()
 * @method \Magento\Wishlist\Model\Wishlist setSharingCode(string $value)
 * @method string getUpdatedAt()
 * @method \Magento\Wishlist\Model\Wishlist setUpdatedAt(string $value)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.TooManyFields)
 *
 * @api
 * @since 100.0.2
 */
class Wishlist extends \Magento\Wishlist\Model\Wishlist
{

    const MEN_SHIRT_PRODUCT_ID = 'toolConfiguration/general/men_shirt_tool_product';
    const MEN_SUIT_PRODUCT_ID = 'toolConfiguration/general/men_suit_tool_product';
    const WOMEN_SHIRT_PRODUCT_ID = 'toolConfiguration/general/women_shirt_tool_product';
    const WOMEN_SUIT_PRODUCT_ID = 'toolConfiguration/general/women_suit_tool_product';


    public function getProductId($scopeProduct) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $scopeConfig = $objectManager->create('Magento\Framework\App\Config\ScopeConfigInterface');

        return $scopeConfig->getValue($scopeProduct, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     * Add catalog product object data to wishlist
     *
     * @param   \Magento\Catalog\Model\Product $product
     * @param   int $qty
     * @param   bool $forciblySetQty
     *
     * @return  Item
     */
    protected function _addCatalogProduct(\Magento\Catalog\Model\Product $product, $qty = 1, $forciblySetQty = false)
    {
        $p_id = $product->getId();
        $item = null;
        foreach ($this->getItemCollection() as $_item) {
            if ($_item->representProduct($product)) {
                $item = $_item;
                break;
            }
        }

        if ($item === null) {
            $storeId = $product->hasWishlistStoreId() ? $product->getWishlistStoreId() : $this->getStore()->getId();
            $item = $this->_wishlistItemFactory->create();
            $item->setProductId($product->getId());
            $item->setWishlistId($this->getId());
            $item->setAddedAt((new \DateTime())->format(\Magento\Framework\Stdlib\DateTime::DATETIME_PHP_FORMAT));
            $item->setStoreId($storeId);
            $item->setOptions($product->getCustomOptions());
            $item->setProduct($product);
            $item->setQty($qty);
            $item->save();
            if ($item->getId()) {
                $this->getItemCollection()->addItem($item);
            }
        } else {
            if($p_id == $this->getProductId(self::MEN_SHIRT_PRODUCT_ID) || $p_id == $this->getProductId(self::MEN_SUIT_PRODUCT_ID) || $p_id == $this->getProductId(self::WOMEN_SHIRT_PRODUCT_ID) || $p_id == $this->getProductId(self::WOMEN_SUIT_PRODUCT_ID)){
                    $storeId = $product->hasWishlistStoreId() ? $product->getWishlistStoreId() : $this->getStore()->getId();
                    $item = $this->_wishlistItemFactory->create();
                    $item->setProductId($product->getId());
                    $item->setWishlistId($this->getId());
                    $item->setAddedAt((new \DateTime())->format(\Magento\Framework\Stdlib\DateTime::DATETIME_PHP_FORMAT));
                    $item->setStoreId($storeId);
                    $item->setOptions($product->getCustomOptions());
                    $item->setProduct($product);
                    $item->setQty($qty);
                    $item->save();
                    if ($item->getId()) {
                        $this->getItemCollection()->addItem($item);
                    }


            }else{
                $qty = $forciblySetQty ? $qty : $item->getQty() + $qty;
                $item->setQty($qty)->save();
            }

        }

        $this->addItem($item);

        return $item;
    }

}
