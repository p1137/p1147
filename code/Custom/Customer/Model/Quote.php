<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Custom\Customer\Model;


class Quote extends \Magento\Quote\Model\Quote
{

    const MEN_SHIRT_PRODUCT_ID = 'toolConfiguration/general/men_shirt_tool_product';
    const MEN_SUIT_PRODUCT_ID = 'toolConfiguration/general/men_suit_tool_product';
    const WOMEN_SHIRT_PRODUCT_ID = 'toolConfiguration/general/women_shirt_tool_product';
    const WOMEN_SUIT_PRODUCT_ID = 'toolConfiguration/general/women_suit_tool_product';


    public function getProductId($scopeProduct) {
        return $this->_scopeConfig->getValue($scopeProduct, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

 
public function addProduct(
        \Magento\Catalog\Model\Product $product,
        $request = null,
        $processMode = \Magento\Catalog\Model\Product\Type\AbstractType::PROCESS_MODE_FULL
    ) {
        if ($request === null) {
            $request = 1;
        }
        if (is_numeric($request)) {
            $request = $this->objectFactory->create(['qty' => $request]);
        }
        if (!$request instanceof \Magento\Framework\DataObject) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('We found an invalid request for adding product to quote.')
            );
        }

        if (!$product->isSalable()) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('Product that you are trying to add is not available.')
            );
        }

        $cartCandidates = $product->getTypeInstance()->prepareForCartAdvanced($request, $product, $processMode);

        /**
         * Error message
         */
        if (is_string($cartCandidates) || $cartCandidates instanceof \Magento\Framework\Phrase) {
            return strval($cartCandidates);
        }

        /**
         * If prepare process return one object
         */
        if (!is_array($cartCandidates)) {
            $cartCandidates = [$cartCandidates];
        }

        $parentItem = null;
        $errors = [];
        $item = null;
        $items = [];

        

        foreach ($cartCandidates as $candidate) {
            // Child items can be sticked together only within their parent
            $stickWithinParent = $candidate->getParentProductId() ? $parentItem : null;
            $candidate->setStickWithinParent($stickWithinParent);
            $item = $this->getItemByProduct($candidate);            
            $p_id = $candidate->getId();

            if (!$item) {
                $item = $this->itemProcessor->init($candidate, $request);
                $item->setQuote($this);
                $item->setOptions($candidate->getCustomOptions());
                $item->setProduct($candidate);
                // Add only item that is not in quote already
                $this->addItem($item);
            }else if(($candidate->getId()==$item->getProductId() && isset($_POST['action']))
                || ($p_id == $this->getProductId(self::MEN_SHIRT_PRODUCT_ID) || $p_id == $this->getProductId(self::MEN_SUIT_PRODUCT_ID) || $p_id == $this->getProductId(self::WOMEN_SHIRT_PRODUCT_ID) || $p_id == $this->getProductId(self::WOMEN_SUIT_PRODUCT_ID)) ){
                $item = $this->itemProcessor->init($candidate, $request);
                $item->setQuote($this);
                $item->setOptions($candidate->getCustomOptions());
                $item->setProduct($candidate);
                // Add only item that is not in quote already
                $this->addItem($item);
            } else if ($this->allowNewEntryToCart($item->getProductId())) {
                $item = $this->itemProcessor->init($candidate, $request);
                $item->setQuote($this);
                $item->setOptions($candidate->getCustomOptions());
                $item->setProduct($candidate);
                // Add only item that is not in quote already
                $this->addItem($item);
            }
            $items[] = $item;

            /**
             * As parent item we should always use the item of first added product
             */
            if (!$parentItem) {
                $parentItem = $item;
            }
            if ($parentItem && $candidate->getParentProductId() && !$item->getParentItem()) {
                $item->setParentItem($parentItem);                
            }

            $this->itemProcessor->prepare($item, $request, $candidate);

            // collect errors instead of throwing first one
            if ($item->getHasError()) {
                foreach ($item->getMessage(false) as $message) {
                    if (!in_array($message, $errors)) {
                        // filter duplicate messages
                        $errors[] = $message;
                    }
                }
            }
        }
        if (!empty($errors)) {
            throw new \Magento\Framework\Exception\LocalizedException(__(implode("\n", $errors)));
        }

        $this->_eventManager->dispatch('sales_quote_product_add_after', ['items' => $items]);
        return $parentItem;
    }

    public function allowNewEntryToCart($product_id) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $product = $objectManager->get('Magento\Catalog\Model\Product')->load($product_id);
        $product_type = $product->getproduct_type();
        
        // 1 = shirt, 2 = suit, 3 = accessories 
        return ($product_type == 1 || $product_type == 2 ) ? true : false;
    }

}
