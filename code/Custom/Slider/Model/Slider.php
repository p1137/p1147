<?php

/**
 * @author Dhirajkumar Deore
 * Copyright © 2018 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Custom\Slider\Model;

class Slider extends \Magento\Framework\Model\AbstractModel {

    protected function _construct() {
        $this->_init('Custom\Slider\Model\ResourceModel\Slider');
    }

    public function getAvailableStatuses() {


        $availableOptions = ['1' => 'Enable',
            '0' => 'Disable'];

        return $availableOptions;
    }

}
