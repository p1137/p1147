<?php

namespace Custom\Slider\Block;

use Magento\Framework\ObjectManagerInterface;

class Quicklinks extends \Magento\Framework\View\Element\Template {

    public function __construct(ObjectManagerInterface $objectManager, \Magento\Catalog\Block\Product\Context $context, array $data = []) {
        $this->objectManager = $objectManager;
        parent::__construct($context, $data);
    }

    public function getbaseUrl() {

        $baseUrl = $this->objectManager->get('Magento\Store\Model\StoreManagerInterface')
                ->getStore()
                ->getBaseUrl('');

        return $baseUrl;
    }

    public function getMediaUrl() {

        $baseUrl = $this->objectManager->get('Magento\Store\Model\StoreManagerInterface')
                ->getStore()
                ->getBaseUrl('');

        return $baseUrl;
    }

}
