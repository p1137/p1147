<?php

namespace Custom\Slider\Block;

use Swissup\Testimonials\Block\TestimonialsList;

class Homepage extends \Magento\Framework\View\Element\Template {

    const STATUS_ENABLED = 1;
    const READYTOWEARCATID = 8;

    protected $_sliderFactory;
    protected $_storeManagerInterface;
    protected $_testimonialsCollectionFactory;
    protected $_productCollectionFactory;
    protected $_categoryFactory;
    protected $_priceData;
    protected $_imageBuilder;
    protected $_listBlock;
    protected $_reviewFactory;

    public function __construct(
    \Magento\Review\Model\ReviewFactory $reviewFactory, \Magento\Checkout\Helper\Cart $cartHelper, \Magento\Catalog\Api\ProductRepositoryInterface $productRepository, \Magento\Catalog\Block\Product\ListProduct $listBlock, \Magento\Catalog\Helper\Image $productImageHelper, \Magento\Framework\Pricing\Helper\Data $PriceData, \Magento\Store\Model\StoreManagerInterface $storeManagerInterface, \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory, \Magento\Catalog\Model\CategoryFactory $categoryFactory, TestimonialsList $testimonialsCollectionFactory, \Custom\Slider\Model\SliderFactory $sliderFactory, \Magento\Catalog\Block\Product\Context $context, array $data = []) {
        $this->_listBlock = $listBlock;
        $this->_cartHelper = $cartHelper;
        $this->_productRepository = $productRepository;
        $this->_productImageHelper = $productImageHelper;
        $this->_priceData = $PriceData;
        $this->_storeManagerInterface = $storeManagerInterface;
        $this->_sliderFactory = $sliderFactory;
        $this->_testimonialsCollectionFactory = $testimonialsCollectionFactory;
        $this->_categoryFactory = $categoryFactory;
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->_reviewFactory = $reviewFactory;

        parent::__construct($context, $data);
    }

    public function getProductPrice($price) {
        return $this->_priceData->currency($price, true, false);
    }

    public function getProductImage($product, $imageId, $width, $height = null) {
        return $this->_productImageHelper
                        ->init($product, $imageId)
                        ->constrainOnly(TRUE)
                        ->keepAspectRatio(TRUE)
                        ->keepTransparency(TRUE)
                        ->keepFrame(FALSE);
//                        ->resize($width, $height);
    }

    public function getAddToCartUrlForProduct($product) {
        return $this->_cartHelper->getAddUrl($product);
    }

    public function getRatingSummary($product) {
        $this->_reviewFactory->create()->getEntitySummary($product, $this->_storeManagerInterface->getStore()->getId());
        $ratingSummary = $product->getRatingSummary()->getRatingSummary();
        return $ratingSummary;
    }

    public function getSlider() {
        return $this->_sliderFactory->create()->getCollection()->addFieldToFilter("status", self::STATUS_ENABLED);
    }

    public function getMediaPath() {
        return $this->_storeManagerInterface->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
    }

    public function getTestimonials() {
        return $this->_testimonialsCollectionFactory->getTestimonials();
    }

    public function getProfileImageUrl($testimonial) {
        return $this->_testimonialsCollectionFactory->getProfileImageUrl($testimonial);
    }

    public function getReadyToWearCollection() {
        $category = $this->_categoryFactory->create()->load(self::READYTOWEARCATID);
        $collection = $this->_productCollectionFactory->create();
        $collection->addAttributeToSelect('*');
        $collection->addCategoryFilter($category);
        $collection->addAttributeToFilter('visibility', \Magento\Catalog\Model\Product\Visibility::VISIBILITY_BOTH);
        $collection->addAttributeToFilter('status', \Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED);
        return $collection;
    }

}
