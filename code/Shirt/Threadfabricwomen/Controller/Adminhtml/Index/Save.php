<?php

/**
 * @author Dhirajkumar Deore
 * Copyright © 2018 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Shirt\Threadfabricwomen\Controller\Adminhtml\Index;

use Magento\Backend\App\Action\Context;
use Magento\Backend\App\Action;
use Shirt\Threadfabricwomen\Model\Threadfabricwomen;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\TestFramework\Inspection\Exception;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\RequestInterface;

//use Magento\Framework\Stdlib\DateTime\DateTime;
//use Magento\Ui\Component\MassAction\Filter;
//use FME\News\Model\ResourceModel\Test\CollectionFactory;

class Save extends \Magento\Backend\App\Action {

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;
    protected $scopeConfig;
    protected $_escaper;
    protected $inlineTranslation;
    protected $_dateFactory;
    protected $objectManager;
    protected $connection;
    protected $directory;
    protected $rootPath1;
    protected $rootPath;
    protected $mediaURL;

    /**
     * @param Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param DataPersistorInterface $dataPersistor
     */
    public function __construct(
    Context $context, DataPersistorInterface $dataPersistor, \Magento\Framework\Escaper $escaper, \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Magento\Framework\Stdlib\DateTime\DateTimeFactory $dateFactory
    ) {

        $this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->connection = $this->objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $this->directory = $this->objectManager->get('\Magento\Framework\Filesystem\DirectoryList');
        $this->rootPath1 = $this->directory->getRoot();
        $this->mediaURL = $this->objectManager->get('Magento\Store\Model\StoreManagerInterface')
                ->getStore()
                ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        $this->rootPath = $this->rootPath1 . "/pub/media/shirt-tool/";
        $this->dataPersistor = $dataPersistor;
        $this->scopeConfig = $scopeConfig;
        $this->_escaper = $escaper;
        $this->_dateFactory = $dateFactory;
        $this->inlineTranslation = $inlineTranslation;
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute() {

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();


        if ($data) {

            $id = $this->getRequest()->getParam('id');

            if (isset($data['status']) && $data['status'] === 'true') {
                $data['status'] = Block::STATUS_ENABLED;
            }
            if (empty($data['id'])) {
                $data['id'] = null;
            }


            /** @var \Magento\Cms\Model\Block $model */
            $model = $this->_objectManager->create('Shirt\Threadfabricwomen\Model\Threadfabricwomen')->load($id);
            if (!$model->getId() && $id) {
                $this->messageManager->addError(__('This shirt_threadfabricwomen no longer exists.'));
                return $resultRedirect->setPath('*/*/');
            }


            if (isset($data['image'][0]['name']) && isset($data['image'][0]['tmp_name'])) {
                $data['image'] = 'shirt-tool/shirt-threadfabricwomen/' . $data['image'][0]['name'];
            } elseif (isset($data['image'][0]['name']) && !isset($data['image'][0]['tmp_name'])) {
                $data['image'] = $data['image'][0]['name'];
            } else {
                $data['image'] = null;
            }
            $fabric_thumb_name = $data['image'];

            $model->setData($data);

            $this->inlineTranslation->suspend();
            try {
                $model->save();
                $this->messageManager->addSuccess(__('Threadfabricwomen Saved successfully'));
                $this->dataPersistor->clear('shirt_threadfabricwomen');

                if ($fabric_thumb_name != '') {

                    $fabric_id = $model->getId();                  

                    $homepath = $this->rootPath;


//================  Shirt Image Generator START ================================                    
                    $fabricThumbImgpath = $this->mediaURL . $fabric_thumb_name;
                    $fabricName = "fabric_main.png";
                    $fabric_0 = $homepath . 'fab_shirt_images/' . $fabricName;

                    //for zoom view
                    $fabric_zoom = $homepath . "fab_shirt_images/fabric_zoom.png";
                    $shirtFabImgPath = $homepath . 'fab_shirt_images/';


                    exec('convert -size 500x1320 tile:' . $fabricThumbImgpath . ' ' . $fabric_0);                    

                    //zoom view fabirc
                    exec('convert -size 1200x900 tile:' . $fabricThumbImgpath . ' ' . $fabric_zoom);
                    //created for collar
                    exec('convert ' . $fabric_zoom . ' -background "rgba(0,0,0,0.5)" -distort SRT 90 ' . $shirtFabImgPath . 'fabric_main_back_zoom.png');

                    /* END create fabric image */



                    $placket_front_thread = $homepath . 'glow_mask_shirt/Front_View/Plackets/Placket_Front_thread.png';
                    $placket_front_buttonhole = $homepath . 'glow_mask_shirt/Front_View/Plackets/Placket_Front_buttonhole.png';


                    $placket_side_thread = $homepath . 'glow_mask_shirt/Side_View/Plackets/Placket_side_thread.png';
                    $placket_side_buttonhole = $homepath . 'glow_mask_shirt/Side_View/Plackets/Placket_side_buttonhole.png';

                    $placket_fold_thread = $homepath . 'glow_mask_shirt/Folded_View/Plackets/Folded_Placket_thread.png';
                    $placket_fold_buttonhole = $homepath . 'glow_mask_shirt/Folded_View/Plackets/Folded_Placket_buttonhole.png';

                    $placket_zoom_thread = $homepath . 'glow_mask_shirt/Zoom_View/Collar_Styles/Buttonthread/Torso_ButtonThreads.png';
                    $placket_zoom_buttonhole = $homepath . 'glow_mask_shirt/Zoom_View/Collar_Styles/Buttonholes/Torso_Buttonhole.png';

                    $final_zoom_thread = $homepath . 'glow_mask_shirt/Zoom_View/Collar_Styles/Buttonthread/1_final_buttonthread_view_5.png';
                    $final_zoom_buttonhole = $homepath . 'glow_mask_shirt/Zoom_View/Collar_Styles/Buttonholes/1_final_buttonhole_view_5.png';

                    $base_final_zoom_thread = $homepath . 'glow_mask_shirt/Zoom_View/Collar_Styles/Buttonthread/base_buttonthreads.png';
                    $base_final_zoom_buttonhole = $homepath . 'glow_mask_shirt/Zoom_View/Collar_Styles/Buttonholes/base_buttonhole.png';

                    //final thread , hole view 1
                    exec('composite -compose CopyOpacity ' . $final_zoom_thread . ' ' . $fabric_zoom . ' ' . $homepath . 'women_shirt_images/plackets/' . $fabric_id . '_final_buttonthread_view_5.png');
                    exec('composite -compose CopyOpacity ' . $final_zoom_buttonhole . ' ' . $fabric_zoom . ' ' . $homepath . 'women_shirt_images/plackets/' . $fabric_id . '_final_buttonhole_view_5.png');

                    exec('composite -compose CopyOpacity ' . $base_final_zoom_thread . ' ' . $fabric_zoom . ' ' . $homepath . 'women_shirt_images/plackets/' . $fabric_id . '_base_final_buttonthread_view_5.png');
                    exec('composite -compose CopyOpacity ' . $base_final_zoom_buttonhole . ' ' . $fabric_zoom . ' ' . $homepath . 'women_shirt_images/plackets/' . $fabric_id . '_base_final_buttonhole_view_5.png');

                    //placket thread , hole view 1
                    exec('composite -compose CopyOpacity ' . $placket_front_thread . ' ' . $fabric_0 . ' ' . $homepath . 'women_shirt_images/plackets/' . $fabric_id . '_thread_view_1.png');
                    exec('composite -compose CopyOpacity ' . $placket_front_buttonhole . ' ' . $fabric_0 . ' ' . $homepath . 'women_shirt_images/plackets/' . $fabric_id . '_buttonhole_view_1.png');

                    //placket thread , hole view 2
                    exec('composite -compose CopyOpacity ' . $placket_side_thread . ' ' . $fabric_0 . ' ' . $homepath . 'women_shirt_images/plackets/' . $fabric_id . '_thread_view_2.png');
                    exec('composite -compose CopyOpacity ' . $placket_side_buttonhole . ' ' . $fabric_0 . ' ' . $homepath . 'women_shirt_images/plackets/' . $fabric_id . '_buttonhole_view_2.png');


                    //placket thread , hole view 4
                    exec('composite -compose CopyOpacity ' . $placket_fold_thread . ' ' . $fabric_0 . ' ' . $homepath . 'women_shirt_images/plackets/' . $fabric_id . '_thread_view_4.png');
                    exec('composite -compose CopyOpacity ' . $placket_fold_buttonhole . ' ' . $fabric_0 . ' ' . $homepath . 'women_shirt_images/plackets/' . $fabric_id . '_buttonhole_view_4.png');

                    //placket thread , hole view 5
                    exec('composite -compose CopyOpacity ' . $placket_zoom_thread . ' ' . $fabric_zoom . ' ' . $homepath . 'women_shirt_images/plackets/' . $fabric_id . '_thread_view_5.png');
                    exec('composite -compose CopyOpacity ' . $placket_zoom_buttonhole . ' ' . $fabric_zoom . ' ' . $homepath . 'women_shirt_images/plackets/' . $fabric_id . '_buttonhole_view_5.png');

                    //CUFF

                    $Cuff_Back_Double_thread = $homepath . 'glow_mask_shirt/Back_View/Cuffs/Cuff_Back_Double_thread.png';
                    $Cuff_Back_Double_thread_buttonhole = $homepath . 'glow_mask_shirt/Back_View/Cuffs/Cuff_Back_Double_buttonhole.png';
                    $Cuff_Back_Single_thread = $homepath . 'glow_mask_shirt/Back_View/Cuffs/Cuff_Back_Single_thread.png';
                    $Cuff_Back_Single_thread_buttonhole = $homepath . 'glow_mask_shirt/Back_View/Cuffs/Cuff_Back_Single_buttonhole.png';

                    exec('composite -compose CopyOpacity ' . $Cuff_Back_Double_thread . ' ' . $fabric_0 . ' ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_double_thread_view_3.png');
                    exec('composite -compose CopyOpacity ' . $Cuff_Back_Double_thread_buttonhole . ' ' . $fabric_0 . ' ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_double_buttonhole_view_3.png');

                    exec('composite -compose CopyOpacity ' . $Cuff_Back_Single_thread . ' ' . $fabric_0 . ' ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_single_thread_view_3.png');
                    exec('composite -compose CopyOpacity ' . $Cuff_Back_Single_thread_buttonhole . ' ' . $fabric_0 . ' ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_single_buttonhole_view_3.png');

                    $Cuff_Fold_Single_thread = $homepath . 'glow_mask_shirt/Folded_View/Cuffs/Folded_CuffSingle_buttonhole.png';
                    $Cuff_Fold_Single_thread_buttonhole = $homepath . 'glow_mask_shirt/Folded_View/Cuffs/Folded_CuffSingle_thread.png';

                    exec('composite -compose CopyOpacity ' . $Cuff_Fold_Single_thread . ' ' . $fabric_0 . ' ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_single_thread_view_4.png');
                    exec('composite -compose CopyOpacity ' . $Cuff_Fold_Single_thread_buttonhole . ' ' . $fabric_0 . ' ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_single_buttonhole_view_4.png');

                    $Cuff_Fold_Double_thread = $homepath . 'glow_mask_shirt/Folded_View/Cuffs/Folded_CuffDouble_thread.png';
                    $Cuff_Fold_Double_thread_buttonhole = $homepath . 'glow_mask_shirt/Folded_View/Cuffs/Folded_CuffDouble_buttonhole.png';

                    exec('composite -compose CopyOpacity ' . $Cuff_Fold_Double_thread . ' ' . $fabric_0 . ' ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_double_thread_view_4.png');
                    exec('composite -compose CopyOpacity ' . $Cuff_Fold_Double_thread_buttonhole . ' ' . $fabric_0 . ' ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_double_buttonhole_view_4.png');


                    $Collar_Side_thread = $homepath . 'glow_mask_shirt/Side_View/Collar_Styles/Side_Collar_Single_button_thread.png';
                    $Collar_Front_thread = $homepath . 'glow_mask_shirt/Front_View/Collar_Styles/Front_single_button_thread.png';
                    exec('composite -compose CopyOpacity ' . $Collar_Side_thread . ' ' . $fabric_0 . ' ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_single_thread_view_2.png');
                    exec('composite -compose CopyOpacity ' . $Collar_Front_thread . ' ' . $fabric_0 . ' ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_single_thread_view_1.png');

                    $SleevePlacekt_thread_right = $homepath . 'glow_mask_shirt/Back_View/Sleeves/SleevePlacekt_thread_right.png';
                    $SleevePlacekt_thread_left = $homepath . 'glow_mask_shirt/Back_View/Sleeves/SleevePlacekt_thread_left.png';
                    exec('composite -compose CopyOpacity ' . $SleevePlacekt_thread_right . ' ' . $fabric_0 . ' ' . $homepath . 'women_shirt_images/sleeveplacket/' . $fabric_id . '_placket_thread_right_view_3.png');
                    exec('composite -compose CopyOpacity ' . $SleevePlacekt_thread_left . ' ' . $fabric_0 . ' ' . $homepath . 'women_shirt_images/sleeveplacket/' . $fabric_id . '_placket_thread_left_view_3.png');                    
//================  Shirt Image Generator END ==================================
                }

                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['id' => $model->getId()]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the Threadfabricwomen.'));
            }
            $this->dataPersistor->set('shirt_threadfabricwomen', $data);
            return $resultRedirect->setPath('*/*/edit', ['id' => $this->getRequest()->getParam('id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }

}
