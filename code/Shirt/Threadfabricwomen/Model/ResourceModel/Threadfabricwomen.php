<?php

/**
 * @author Dhirajkumar Deore
 * Copyright © 2018 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Shirt\Threadfabricwomen\Model\ResourceModel;

class Threadfabricwomen extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb {

    protected function _construct() {
        $this->_init('shirt_threadfabricwomen', 'id');
    }

}
