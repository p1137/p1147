<?php

/**
 * @author Dhirajkumar Deore
 * Copyright © 2018 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Shirt\Threadfabricwomen\Model;

class Threadfabricwomen extends \Magento\Framework\Model\AbstractModel {

    protected function _construct() {
        $this->_init('Shirt\Threadfabricwomen\Model\ResourceModel\Threadfabricwomen');
    }

    public function getAvailableStatuses() {


        $availableOptions = ['1' => 'Enable',
            '0' => 'Disable'];

        return $availableOptions;
    }

}
