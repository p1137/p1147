<?php

namespace Shirt\Shirtfabricwomen\Block;

class Shirt extends \Magento\Framework\View\Element\Template {

    protected $_scopeConfig;
    protected $customerSession;

    public function __construct(
    \Magento\Framework\View\Element\Template\Context $context, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Magento\Customer\Model\Session $customerSession, \Wholesaler\Mgmt\Model\MeasurementFactory $measurementfactory, array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_scopeConfig = $scopeConfig;
        $this->_customerSession = $customerSession;
        $this->measurementfactory = $measurementfactory;
    }

    public function getProductId() {
        return $this->_scopeConfig->getValue('toolConfiguration/general/women_shirt_tool_product', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

}
