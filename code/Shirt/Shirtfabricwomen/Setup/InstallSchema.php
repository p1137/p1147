<?php

namespace Shirt\Shirtfabricwomen\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface {

    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context) {
        $installer = $setup;
        $installer->startSetup();
        /**
         * Create table 'shirt_shirtfabricwomen'
         */
        $table = $installer->getConnection()->newTable($installer->getTable('shirt_shirtfabricwomen'))
                ->addColumn(
                        'shirtfabricwomen_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true], 'Shirtfabricwomen ID'
                )
                ->addColumn(
                        'title', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, ['nullable' => true, 'default' => null], 'Title'
                )
                ->addColumn(
                        'display_fabric_thumb', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'Display Fabric Thumb Image'
                )
                ->addColumn(
                        'fabric_thumb', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'Fabric Image for generation'
                )
                ->addColumn(
                        'fabric_large_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'Fabric Large Image'
                )
                ->addColumn(
                        'fabric_type', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, null, ['nullable' => false, 'unsigned' => true, 'default' => '0'], 'Fabric Type'
                )
                ->addColumn(
                        'price', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['nullable' => true, 'unsigned' => true, 'default' => '0'], 'Price'
                )
                ->addColumn(
                        'btn_color_id', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, 6, [], 'Button Color Id'
                )
                ->addColumn(
                        'fabric_material_id', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, 6, [], 'Fabric Material Id'
                )
                ->addColumn(
                        'fabric_pattern_id', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, 6, [], 'Fabric Pattern Id'
                )
                ->addColumn(
                        'fabric_season_id', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, 6, [], 'Fabric Season Id'
                )
                ->addColumn(
                        'fabric_color_id', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, 6, [], 'Fabric Color Id'
                )
                ->addColumn(
                        'fabric_category_id', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, 6, [], 'Fabric Category Id'
                )
                ->addColumn(
                        'status', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, null, ['nullable' => false, 'unsigned' => true, 'default' => '1'], 'Status'
                )
                ->addColumn(
                        'created_time', \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP, null, ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT], 'Creation Time'
                )
                ->addColumn(
                'update_time', \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP, null, ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE], 'Update Time'
        );
        $installer->getConnection()->createTable($table);

        $installer->endSetup();
    }

}
