<?php

namespace Shirt\Shirtfabricwomen\Controller\Adminhtml\Index;

use Magento\Backend\App\Action\Context;
use Magento\Backend\App\Action;
use Shirt\Shirtfabricwomen\Model\Shirtfabricwomen;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\TestFramework\Inspection\Exception;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\RequestInterface;

//use Magento\Framework\Stdlib\DateTime\DateTime;
//use Magento\Ui\Component\MassAction\Filter;
//use Shirt\News\Model\ResourceModel\Test\CollectionFactory;

class Check extends \Magento\Backend\App\Action {

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;
    protected $scopeConfig;
    protected $_escaper;
    protected $inlineTranslation;
    protected $_dateFactory;
    protected $_dir;
    protected $objectManager;
    protected $connection;
    protected $directory;
    protected $rootPath1;
    protected $rootPath;
    protected $_womenShirtFit;
    protected $_womenShirtPlackets;
    protected $_womenShirtCuff;
    protected $_womenShirtStyle;
    protected $_womenShirtAccentElbowpatches;
    protected $_womenShirtPocket;
    protected $_womenShirtSleeves;

//protected $_modelNewsFactory;
//  protected $collectionFactory;
//  protected $filter;
    /**
     * @param Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param DataPersistorInterface $dataPersistor
     */
    public function __construct(
    \Shirt\Shirtfabricwomen\Model\WomenShirtSleevesFactory $womenShirtSleeves, 
    \Shirt\Shirtfabricwomen\Model\WomenShirtPocketFactory $womenShirtPocket, 
    \Shirt\Shirtfabricwomen\Model\WomenShirtAccentElbowpatchesFactory $womenShirtAccentElbowpatches, 
    \Shirt\Shirtfabricwomen\Model\WomenShirtStyleFactory $womenShirtStyle, 
    \Shirt\Shirtfabricwomen\Model\WomenShirtCuffFactory $womenShirtCuff, 
    \Shirt\Shirtfabricwomen\Model\WomenShirtFitFactory $womenShirtFit, 
    \Shirt\Shirtfabricwomen\Model\WomenShirtPlacketsFactory $womenShirtPlackets, 
    \Magento\Framework\Filesystem\DirectoryList $dir, 
    Context $context, 
    DataPersistorInterface $dataPersistor, 
    \Magento\Framework\Escaper $escaper, 
    \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation, 
    \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, 
    \Magento\Framework\Stdlib\DateTime\DateTimeFactory $dateFactory
    ) {
        $this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->connection = $this->objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $this->directory = $this->objectManager->get('\Magento\Framework\Filesystem\DirectoryList');
        $this->rootPath1 = $this->directory->getRoot();
        $this->rootPath = $this->directory->getPath('media');
        $this->dataPersistor = $dataPersistor;
        $this->scopeConfig = $scopeConfig;
        $this->_escaper = $escaper;
        $this->_dir = $dir;
        $this->_dateFactory = $dateFactory;
        $this->inlineTranslation = $inlineTranslation;
        $this->_womenShirtFit = $womenShirtFit;
        $this->_womenShirtPlackets = $womenShirtPlackets;
        $this->_womenShirtCuff = $womenShirtCuff;
        $this->_womenShirtStyle = $womenShirtStyle;
        $this->_womenShirtAccentElbowpatches = $womenShirtAccentElbowpatches;
        $this->_womenShirtPocket = $womenShirtPocket;
        $this->_womenShirtSleeves = $womenShirtSleeves;
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute() {

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();

        if ($data) {
            $id = $this->getRequest()->getParam('shirtfabricwomen_id');

            if (isset($data['status']) && $data['status'] === 'true') {
                $data['status'] = Block::STATUS_ENABLED;
            }
            if (empty($data['shirtfabricwomen_id'])) {
                $data['shirtfabricwomen_id'] = null;
            }


            /** @var \Magento\Cms\Model\Block $model */
            $model = $this->_objectManager->create('Shirt\Shirtfabricwomen\Model\Shirtfabricwomen')->load($id);
            if (!$model->getId() && $id) {
                $this->messageManager->addError(__('This Shirt Style no longer exists.'));
                return $resultRedirect->setPath('*/*/');
            }
            $mediaPath = $this->_dir->getPath('media');

            if (isset($data['display_fabric_thumb'][0]['name']) && isset($data['display_fabric_thumb'][0]['tmp_name'])) {
                $data['display_fabric_thumb'] = 'shirt-tool/fab_women_shirt_images/display_thumb/' . $data['display_fabric_thumb'][0]['name'];
            } elseif (isset($data['display_fabric_thumb'][0]['name']) && !isset($data['display_fabric_thumb'][0]['tmp_name'])) {
                $data['display_fabric_thumb'] = $data['display_fabric_thumb'][0]['name'];
            } else {
                $data['display_fabric_thumb'] = null;
            }


            if (isset($data['fabric_thumb'][0]['name']) && isset($data['fabric_thumb'][0]['tmp_name'])) {
                $data['fabric_thumb'] = 'shirt-tool/fab_women_shirt_images/thumbs/' . $data['fabric_thumb'][0]['name'];
            } elseif (isset($data['fabric_thumb'][0]['name']) && !isset($data['fabric_thumb'][0]['tmp_name'])) {
                $data['fabric_thumb'] = $data['fabric_thumb'][0]['name'];
            } else {
                $data['fabric_thumb'] = null;
            }

            $fabric_thumb_name = $data['fabric_thumb'];

            if (isset($data['fabric_large_image'][0]['name']) && isset($data['fabric_large_image'][0]['tmp_name'])) {
                $data['fabric_large_image'] = 'shirt-tool/fab_women_shirt_images/' . $data['fabric_large_image'][0]['name'];
            } elseif (isset($data['fabric_large_image'][0]['name']) && !isset($data['fabric_large_image'][0]['tmp_name'])) {
                $data['fabric_large_image'] = $data['fabric_large_image'][0]['name'];
            } else {
                $data['fabric_large_image'] = null;
            }

            if (!isset($data['price']))
                $data['price'] = null;
            $model->setData($data);

            $this->inlineTranslation->suspend();

                $model->save();
                if ($fabric_thumb_name != '') {
                    $fabric_id = $model->getId();
                    $homepath = $this->rootPath . "/shirt-tool/";

                    $fabricThumbImgpath = $this->rootPath . "/" . $fabric_thumb_name;
                    $fabricName = $fabric_id . "_fabric_main.png";
                    $fabric_0 = $homepath . 'fab_women_shirt_images/' . $fabricName;

                    $fabricName_90 = $fabric_id . "_fabric_main_90.png";
                    $fabric_90 = $homepath . 'fab_women_shirt_images/' . $fabricName_90;

                    $fabric_zoom = $homepath . "fab_women_shirt_images/" . $fabric_id . "_fabric_zoom.png";
                    $shirtFabImgPath = $homepath . 'fab_women_shirt_images/';

                    exec('convert ' . $fabricThumbImgpath . ' -rotate 90 ' . $fabric_90);
                    exec('convert -size 1080x1320 tile:' . $fabric_90 . ' ' . $shirtFabImgPath . $fabric_id . '_fabric_main_back.png');
                    exec('convert -size 1080x1320 tile:' . $fabricThumbImgpath . ' ' . $fabric_0);

                    $fabricName_wave_left = $fabric_id . "_fabric_wave_left.png";
                    $fabricName_wave_right = $fabric_id . "_fabric_wave_right.png";
                    $fabric_cuff_side = $fabric_id . "_fabric_cuff_side.png";

                    $fabric_side_view = $homepath . 'fab_women_shirt_images/' . $fabric_id . "_fabric_side_view.png";
                    $wave_left = $homepath . 'fab_women_shirt_images/' . $fabricName_wave_left;
                    $wave_right = $homepath . 'fab_women_shirt_images/' . $fabricName_wave_right;

                    exec('convert ' . $fabric_0 . ' -background "rgba(0,0,0,0.5)" -distort SRT -5 ' . $wave_left);
                    exec('convert ' . $fabric_0 . ' -background "rgba(0,0,0,0.5)" -distort SRT 5 ' . $wave_right);
                    exec('convert ' . $fabric_0 . ' -background "rgba(0,0,0,0.5)" -distort SRT -3 ' . $fabric_side_view);

                    exec('convert -size 300x300 tile:' . $fabricThumbImgpath . ' ' . $homepath . 'fab_women_shirt_images/fabric_' . $fabric_id . '_show.png');

                    exec('convert ' . $fabric_0 . ' -background "rgba(0,0,0,0.5)" -distort SRT 80 ' . $shirtFabImgPath . $fabric_id . '_fabric_cuff_side.png');

                    exec('convert -size 1200x900 tile:' . $fabricThumbImgpath . ' ' . $fabric_zoom);

                    exec('convert ' . $fabric_zoom . ' -background "rgba(0,0,0,0.5)" -distort SRT 90 ' . $shirtFabImgPath . $fabric_id . '_fabric_main_back_zoom.png');

                    $front_button_thread = $homepath . 'women_glow_mask_shirt/Front_View/button_thread/Front_single_button_thread.png';
                    $side_collar_button_thread = $homepath . 'women_glow_mask_shirt/Side_View/button_thread/Side_Collar_Single_button_thread.png';

                    exec('composite -compose Dst_In -gravity center ' . $front_button_thread . ' ' . $shirtFabImgPath . $fabric_id . '_fabric_main_back.png -alpha Set ' . $homepath . 'women_shirt_images/buttons/front_button_thread.png');
                    exec('convert ' . $homepath . 'women_shirt_images/buttons/front_button_thread.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/buttons/front_button_thread.png');

                    exec('composite -compose Dst_In -gravity center ' . $side_collar_button_thread . ' ' . $shirtFabImgPath . $fabric_id . '_fabric_main_back.png -alpha Set ' . $homepath . 'women_shirt_images/buttons/side_collar_button_thread.png');
                    exec('convert ' . $homepath . 'women_shirt_images/buttons/side_collar_button_thread.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/buttons/side_collar_button_thread.png');



                    $womenShirtStyle = $this->_womenShirtStyle->create()->getCollection();
                    $rows = $womenShirtStyle->getData();
                    foreach ($rows as $style) {
                        $style_id = $style['shirtstyle_id'];

                        //mask
                        $cmd = 'composite -compose Dst_In -gravity center ' . $homepath . 'women_glow_mask_shirt/Front_View/Collar_Casual/Torso_Bust/Casual-Bust_Front_Torso_mask.png ' . $fabric_0 . ' -alpha Set ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_casual_common_torso_view_1.png';

                        exec($cmd);

                        $cmd = 'convert ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_casual_common_torso_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_casual_common_torso_view_1.png';

                        exec($cmd);

                        //glow image
                        $cmd = 'convert ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_casual_common_torso_view_1.png  ' . $homepath . 'women_glow_mask_shirt/Front_View/Collar_Casual/Torso_Bust/Casual-Bust_Front_Torso_shad.png -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_casual_common_torso_view_1.png';

                        exec($cmd);

                        //highlighted
                        $cmd = 'composite ' . $homepath . 'women_glow_mask_shirt/Front_View/Collar_Casual/Torso_Bust/Casual-Bust_Front_Torso_hi.png -compose Overlay  ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_casual_common_torso_view_1.png ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_casual_common_torso_view_1.png';

                        exec($cmd);


                        //step-2: common placket
                        //mask
                        $cmd = 'composite -compose Dst_In -gravity center ' . $homepath . 'women_glow_mask_shirt/Front_View/Collar_Casual/Placket/Casual-Bust_Front_Placket_mask.png ' . $fabric_0 . ' -alpha Set ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_common_placket_view_1.png';

                        exec($cmd);

                        $cmd = 'convert ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_common_placket_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_common_placket_view_1.png';

                        exec($cmd);

                        //glow image
                        $cmd = 'convert ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_common_placket_view_1.png  ' . $homepath . 'women_glow_mask_shirt/Front_View/Collar_Casual/Placket/Casual-Bust_Front_Placket_shad.png -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_common_placket_view_1.png';

                        exec($cmd);

                        //highlighted
                        $cmd = 'composite ' . $homepath . 'women_glow_mask_shirt/Front_View/Collar_Casual/Placket/Casual-Bust_Front_Placket_hi.png -compose Overlay  ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_common_placket_view_1.png ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_common_placket_view_1.png';

                        exec($cmd);
                    }
                
                }

        }
               
    }

}
