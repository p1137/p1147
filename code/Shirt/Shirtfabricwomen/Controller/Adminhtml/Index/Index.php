<?php
/**
 *
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Shirt\Shirtfabricwomen\Controller\Adminhtml\Index;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Index extends \Magento\Backend\App\Action
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }
    /**
     * Check the permission to run it
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Shirt_Shirtfabricwomen::shirtfabricwomen');
    }

    /**
     * Index action
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Shirt_Shirtfabricwomen::shirtfabricwomen');
        $resultPage->addBreadcrumb(__('Shirtfabricwomen'), __('Shirtfabricwomen'));
        $resultPage->addBreadcrumb(__('Manage Shirtfabricwomen'), __('Manage Shirtfabricwomen'));
        $resultPage->getConfig()->getTitle()->prepend(__('Manage women shirt fabrics'));

        return $resultPage;
    }
}
