<?php

/**
 *
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Shirt\Shirtfabricwomen\Controller\Adminhtml\Index;

use Magento\Backend\App\Action\Context;
use Magento\Backend\App\Action;
use Shirt\Shirtfabricwomen\Model\Shirtfabricwomen;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\TestFramework\Inspection\Exception;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\RequestInterface;

//use Magento\Framework\Stdlib\DateTime\DateTime;
//use Magento\Ui\Component\MassAction\Filter;
//use Shirt\News\Model\ResourceModel\Test\CollectionFactory;

class Save extends \Magento\Backend\App\Action {

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;
    protected $scopeConfig;
    protected $_escaper;
    protected $inlineTranslation;
    protected $_dateFactory;
    protected $_dir;
    protected $objectManager;
    protected $connection;
    protected $directory;
    protected $rootPath1;
    protected $rootPath;
    protected $_womenShirtFit;
    protected $_womenShirtPlackets;
    protected $_womenShirtCuff;
    protected $_womenShirtStyle;
    protected $_womenShirtAccentElbowpatches;
    protected $_womenShirtPocket;
    protected $_womenShirtSleeves;

//protected $_modelNewsFactory;
//  protected $collectionFactory;
//  protected $filter;
    /**
     * @param Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param DataPersistorInterface $dataPersistor
     */
    public function __construct(
    \Shirt\Shirtfabricwomen\Model\WomenShirtSleevesFactory $womenShirtSleeves, \Shirt\Shirtfabricwomen\Model\WomenShirtPocketFactory $womenShirtPocket, \Shirt\Shirtfabricwomen\Model\WomenShirtAccentElbowpatchesFactory $womenShirtAccentElbowpatches, \Shirt\Shirtfabricwomen\Model\WomenShirtStyleFactory $womenShirtStyle, \Shirt\Shirtfabricwomen\Model\WomenShirtCuffFactory $womenShirtCuff, \Shirt\Shirtfabricwomen\Model\WomenShirtFitFactory $womenShirtFit, \Shirt\Shirtfabricwomen\Model\WomenShirtPlacketsFactory $womenShirtPlackets, \Magento\Framework\Filesystem\DirectoryList $dir, Context $context, DataPersistorInterface $dataPersistor, \Magento\Framework\Escaper $escaper, \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Magento\Framework\Stdlib\DateTime\DateTimeFactory $dateFactory
    ) {
        $this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->connection = $this->objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $this->directory = $this->objectManager->get('\Magento\Framework\Filesystem\DirectoryList');
        $this->rootPath1 = $this->directory->getRoot();
        $this->rootPath = $this->directory->getPath('media');
        $this->dataPersistor = $dataPersistor;
        $this->scopeConfig = $scopeConfig;
        $this->_escaper = $escaper;
        $this->_dir = $dir;
        $this->_dateFactory = $dateFactory;
        $this->inlineTranslation = $inlineTranslation;
        $this->_womenShirtFit = $womenShirtFit;
        $this->_womenShirtPlackets = $womenShirtPlackets;
        $this->_womenShirtCuff = $womenShirtCuff;
        $this->_womenShirtStyle = $womenShirtStyle;
        $this->_womenShirtAccentElbowpatches = $womenShirtAccentElbowpatches;
        $this->_womenShirtPocket = $womenShirtPocket;
        $this->_womenShirtSleeves = $womenShirtSleeves;
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute() {

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();
        if ($data) {
            $id = $this->getRequest()->getParam('shirtfabricwomen_id');

            if (isset($data['status']) && $data['status'] === 'true') {
                $data['status'] = Block::STATUS_ENABLED;
            }
            if (empty($data['shirtfabricwomen_id'])) {
                $data['shirtfabricwomen_id'] = null;
            }


            /** @var \Magento\Cms\Model\Block $model */
            $model = $this->_objectManager->create('Shirt\Shirtfabricwomen\Model\Shirtfabricwomen')->load($id);
            if (!$model->getId() && $id) {
                $this->messageManager->addError(__('This Shirt Style no longer exists.'));
                return $resultRedirect->setPath('*/*/');
            }
            $mediaPath = $this->_dir->getPath('media');

            if (isset($data['display_fabric_thumb'][0]['name']) && isset($data['display_fabric_thumb'][0]['tmp_name'])) {
                $data['display_fabric_thumb'] = 'fab_shirt_images/display_thumb/' . $data['display_fabric_thumb'][0]['name'];
            } elseif (isset($data['display_fabric_thumb'][0]['name']) && !isset($data['display_fabric_thumb'][0]['tmp_name'])) {
                $data['display_fabric_thumb'] = $data['display_fabric_thumb'][0]['name'];
            } else {
                $data['display_fabric_thumb'] = null;
            }


            if (isset($data['fabric_thumb'][0]['name']) && isset($data['fabric_thumb'][0]['tmp_name'])) {
                $data['fabric_thumb'] = 'fab_shirt_images/thumbs/' . $data['fabric_thumb'][0]['name'];
            } elseif (isset($data['fabric_thumb'][0]['name']) && !isset($data['fabric_thumb'][0]['tmp_name'])) {
                $data['fabric_thumb'] = $data['fabric_thumb'][0]['name'];
            } else {
                $data['fabric_thumb'] = null;
            }

            $fabric_thumb_name = $data['fabric_thumb'];

            if (isset($data['fabric_large_image'][0]['name']) && isset($data['fabric_large_image'][0]['tmp_name'])) {
                $data['fabric_large_image'] = 'fab_shirt_images/' . $data['fabric_large_image'][0]['name'];
            } elseif (isset($data['fabric_large_image'][0]['name']) && !isset($data['fabric_large_image'][0]['tmp_name'])) {
                $data['fabric_large_image'] = $data['fabric_large_image'][0]['name'];
            } else {
                $data['fabric_large_image'] = null;
            }

            $data['status'] = 0;
            if (!isset($data['price']))
                $data['price'] = null;
            $model->setData($data);

            $this->inlineTranslation->suspend();
            try {
                $model->save();
                $this->messageManager->addSuccess(__('Saved successfully'));
                $this->dataPersistor->clear('shirt_shirtfabricwomen');

//write womenshirtfabric image generation code here..
                if ($fabric_thumb_name != '') {

                    /* color image creation start */
                    $fabric_id = $model->getId();
                    $homepath = $this->rootPath . "/shirt-tool/";
//================  Shirt Image Generator START ================================

                    $fabricThumbImgpath = $homepath . '' . $fabric_thumb_name;
                    $fabricName = $fabric_id . "_fabric_main.png";
                    $fabric_0 = $homepath . 'fab_women_shirt_images/' . $fabricName;

//back

                    $fabricName_90 = $fabric_id . "_fabric_main_90.png";
                    $fabric_90 = $homepath . 'fab_women_shirt_images/' . $fabricName_90;

//for zoom view
                    $fabric_zoom = $homepath . "fab_women_shirt_images/" . $fabric_id . "_fabric_zoom.png";
                    $shirtFabImgPath = $homepath . 'fab_women_shirt_images/';

                    exec('convert ' . $fabricThumbImgpath . ' -rotate 90 ' . $fabric_90);
                    exec('convert -size 1080x1320 tile:' . $fabric_90 . ' ' . $shirtFabImgPath . $fabric_id . '_fabric_main_back.png');
                    exec('convert -size 1080x1320 tile:' . $fabricThumbImgpath . ' ' . $fabric_0);
//created for sleeve
                    $fabricName_wave_left = $fabric_id . "_fabric_wave_left.png";
                    $fabricName_wave_right = $fabric_id . "_fabric_wave_right.png";
                    $fabric_cuff_side = $fabric_id . "_fabric_cuff_side.png";


                    $fabric_side_view = $homepath . 'fab_women_shirt_images/' . $fabric_id . "_fabric_side_view.png";
                    $wave_left = $homepath . 'fab_women_shirt_images/' . $fabricName_wave_left;
                    $wave_right = $homepath . 'fab_women_shirt_images/' . $fabricName_wave_right;

                    exec('convert ' . $fabric_0 . ' -background "rgba(0,0,0,0.5)" -distort SRT -5 ' . $wave_left);
                    exec('convert ' . $fabric_0 . ' -background "rgba(0,0,0,0.5)" -distort SRT 5 ' . $wave_right);
                    exec('convert ' . $fabric_0 . ' -background "rgba(0,0,0,0.5)" -distort SRT -3 ' . $fabric_side_view);

                    exec('convert -size 300x300 tile:' . $fabricThumbImgpath . ' ' . $homepath . 'fab_women_shirt_images/fabric_' . $fabric_id . '_show.png');
//created for collar

                    exec('convert ' . $fabric_0 . ' -background "rgba(0,0,0,0.5)" -distort SRT 80 ' . $shirtFabImgPath . $fabric_id . '_fabric_cuff_side.png');

//zoom view fabirc
                    exec('convert -size 1200x900 tile:' . $fabricThumbImgpath . ' ' . $fabric_zoom);
//created for collar
                    exec('convert ' . $fabric_zoom . ' -background "rgba(0,0,0,0.5)" -distort SRT 90 ' . $shirtFabImgPath . $fabric_id . '_fabric_main_back_zoom.png');

                    /* END create fabric image */

                    /* Create plain fabric image */

                    $vestfabricThumbImgpath = $homepath . '' . $fabric_vest_thumb_name;
                    $vestfabricName = $fabric_id . "_fabric_vestmain.png";
                    $vestfabric_0 = $homepath . 'fab_women_shirt_images/' . $vestfabricName;

                    exec('convert -size 1080x1320 tile:' . $vestfabricThumbImgpath . ' ' . $vestfabric_0);


// button thread..
                    $front_button_thread = $homepath . 'women_glow_mask_shirt/Front_View/button_thread/Front_single_button_thread.png';
                    $side_collar_button_thread = $homepath . 'women_glow_mask_shirt/Side_View/button_thread/Side_Collar_Single_button_thread.png';

                    exec('composite -compose Dst_In -gravity center ' . $front_button_thread . ' ' . $shirtFabImgPath . $fabric_id . '_fabric_main_back.png -alpha Set ' . $homepath . 'women_shirt_images/buttons/front_button_thread.png');
                    exec('convert ' . $homepath . 'women_shirt_images/buttons/front_button_thread.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/buttons/front_button_thread.png');

                    exec('composite -compose Dst_In -gravity center ' . $side_collar_button_thread . ' ' . $shirtFabImgPath . $fabric_id . '_fabric_main_back.png -alpha Set ' . $homepath . 'women_shirt_images/buttons/side_collar_button_thread.png');
                    exec('convert ' . $homepath . 'women_shirt_images/buttons/side_collar_button_thread.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/buttons/side_collar_button_thread.png');
// button thread end.
// shirt fit start here..

                    $modelShirtFit = $this->_womenShirtFit->create()->getCollection();
                    $rows_fit = $modelShirtFit->getData();

                    foreach ($rows_fit as $fit) {

                        $shirtfit_id = $fit['shirtfit_id'];
                        $style_type = '';
                        $cuff_size = '';
                        $cuff_type = '';

                        $fit_glow = $homepath . '' . $fit['glow_front_image'];
                        $fit_mask = $homepath . '' . $fit['mask_front_image'];
                        $fit_highlighted = $homepath . '' . $fit['highlighted_front_image'];

                        if ($fit['glow_front_image'] != '' && $fit['mask_front_image'] != '' && $fit['highlighted_front_image'] != '') {
//mask changed
                            $cmd = ' composite -compose Dst_In  -gravity center ' . $fit_mask . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'women_shirt_images/fit/' . $fabric_id . '_fitstyle_' . $style_type . '_size_' . $cuff_size . '_fittype_' . $cuff_type . '_' . $shirtfit_id . '_view_1.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);
//crop
                            $cmd = ' convert ' . $homepath . 'women_shirt_images/fit/' . $fabric_id . '_fitstyle_' . $style_type . '_size_' . $cuff_size . '_fittype_' . $cuff_type . '_' . $shirtfit_id . '_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/fit/' . $fabric_id . '_fitstyle_' . $style_type . '_size_' . $cuff_size . '_fittype_' . $cuff_type . '_' . $shirtfit_id . '_view_1.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);
//glow
                            $cmd = ' convert ' . $homepath . 'women_shirt_images/fit/' . $fabric_id . '_fitstyle_' . $style_type . '_size_' . $cuff_size . '_fittype_' . $cuff_type . '_' . $shirtfit_id . '_view_1.png ' . $fit_glow . ' -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/fit/' . $fabric_id . '_fitstyle_' . $style_type . '_size_' . $cuff_size . '_fittype_' . $cuff_type . '_' . $shirtfit_id . '_view_1.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);
//highlighted
                            $cmd = ' composite ' . $fit_highlighted . ' -compose Overlay  ' . $homepath . 'women_shirt_images/fit/' . $fabric_id . '_fitstyle_' . $style_type . '_size_' . $cuff_size . '_fittype_' . $cuff_type . '_' . $shirtfit_id . '_view_1.png ' . $homepath . 'women_shirt_images/fit/' . $fabric_id . '_fitstyle_' . $style_type . '_size_' . $cuff_size . '_fittype_' . $cuff_type . '_' . $shirtfit_id . '_view_1.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);
                        }

                        $glow_fit_image = $homepath . '' . $fit['glow_side_image'];
                        $mask_fit_image = $homepath . '' . $fit['mask_side_image'];
                        $highlighted_fit_image = $homepath . '' . $fit['highlighted_side_image'];

                        if ($fit['glow_side_image'] != '' && $fit['mask_side_image'] != '' && $fit['highlighted_side_image'] != '') {

                            $cmd = ' composite -compose Dst_In  -gravity center ' . $mask_fit_image . ' ' . $fabric_side_view . ' -alpha Set ' . $homepath . 'women_shirt_images/fit/' . $fabric_id . '_fitstyle_' . $style_type . '_size_' . $cuff_size . '_fittype_' . $cuff_type . '_' . $shirtfit_id . '_view_2.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);
//crop
                            $cmd = ' convert ' . $homepath . 'women_shirt_images/fit/' . $fabric_id . '_fitstyle_' . $style_type . '_size_' . $cuff_size . '_fittype_' . $cuff_type . '_' . $shirtfit_id . '_view_2.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/fit/' . $fabric_id . '_fitstyle_' . $style_type . '_size_' . $cuff_size . '_fittype_' . $cuff_type . '_' . $shirtfit_id . '_view_2.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);
//glow
                            $cmd = ' convert ' . $homepath . 'women_shirt_images/fit/' . $fabric_id . '_fitstyle_' . $style_type . '_size_' . $cuff_size . '_fittype_' . $cuff_type . '_' . $shirtfit_id . '_view_2.png ' . $glow_fit_image . ' -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/fit/' . $fabric_id . '_fitstyle_' . $style_type . '_size_' . $cuff_size . '_fittype_' . $cuff_type . '_' . $shirtfit_id . '_view_2.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);
//highlighted
                            $cmd = ' composite ' . $highlighted_fit_image . ' -compose Overlay  ' . $homepath . 'women_shirt_images/fit/' . $fabric_id . '_fitstyle_' . $style_type . '_size_' . $cuff_size . '_fittype_' . $cuff_type . '_' . $shirtfit_id . '_view_2.png ' . $homepath . 'women_shirt_images/fit/' . $fabric_id . '_fitstyle_' . $style_type . '_size_' . $cuff_size . '_fittype_' . $cuff_type . '_' . $shirtfit_id . '_view_2.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);
                        }

                        $glow_back_image = $homepath . '' . $fit['glow_back_image'];
                        $mask_back_image = $homepath . '' . $fit['mask_back_image'];
                        $highlighted_back_image = $homepath . '' . $fit['highlighted_back_image'];

                        if ($fit['glow_back_image'] != '' && $fit['mask_back_image'] != '' && $fit['highlighted_back_image'] != '') {
                            $cmd = ' composite -compose Dst_In  -gravity center ' . $mask_back_image . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'women_shirt_images/fit/' . $fabric_id . '_fitstyle_' . $style_type . '_size_' . $cuff_size . '_fittype_' . $cuff_type . '_' . $shirtfit_id . '_view_3.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);
//crop
                            $cmd = ' convert ' . $homepath . 'women_shirt_images/fit/' . $fabric_id . '_fitstyle_' . $style_type . '_size_' . $cuff_size . '_fittype_' . $cuff_type . '_' . $shirtfit_id . '_view_3.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/fit/' . $fabric_id . '_fitstyle_' . $style_type . '_size_' . $cuff_size . '_fittype_' . $cuff_type . '_' . $shirtfit_id . '_view_3.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);
//glow
                            $cmd = ' convert ' . $homepath . 'women_shirt_images/fit/' . $fabric_id . '_fitstyle_' . $style_type . '_size_' . $cuff_size . '_fittype_' . $cuff_type . '_' . $shirtfit_id . '_view_3.png ' . $glow_back_image . ' -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/fit/' . $fabric_id . '_fitstyle_' . $style_type . '_size_' . $cuff_size . '_fittype_' . $cuff_type . '_' . $shirtfit_id . '_view_3.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);
//highlighted
                            $cmd = ' composite ' . $highlighted_back_image . ' -compose Overlay  ' . $homepath . 'women_shirt_images/fit/' . $fabric_id . '_fitstyle_' . $style_type . '_size_' . $cuff_size . '_fittype_' . $cuff_type . '_' . $shirtfit_id . '_view_3.png ' . $homepath . 'women_shirt_images/fit/' . $fabric_id . '_fitstyle_' . $style_type . '_size_' . $cuff_size . '_fittype_' . $cuff_type . '_' . $shirtfit_id . '_view_3.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);
                        }
//
                        $glow_fold_image = $homepath . '' . $fit['glow_fold_image'];
                        $mask_fold_image = $homepath . '' . $fit['mask_fold_image'];
                        $highlighted_fold_image = $homepath . '' . $fit['highlighted_fold_image'];

                        if ($fit['glow_fold_image'] != '' && $fit['mask_fold_image'] != '' && $fit['highlighted_fold_image'] != '') {
                            $cmd = ' composite -compose Dst_In  -gravity center ' . $mask_fold_image . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'women_shirt_images/fit/' . $fabric_id . '_fitstyle_' . $style_type . '_size_' . $cuff_size . '_fittype_' . $cuff_type . '_' . $shirtfit_id . '_view_4.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

                            $cmd = ' convert ' . $homepath . 'women_shirt_images/fit/' . $fabric_id . '_fitstyle_' . $style_type . '_size_' . $cuff_size . '_fittype_' . $cuff_type . '_' . $shirtfit_id . '_view_4.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/fit/' . $fabric_id . '_fitstyle_' . $style_type . '_size_' . $cuff_size . '_fittype_' . $cuff_type . '_' . $shirtfit_id . '_view_4.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);
//glow
                            $cmd = ' convert ' . $homepath . 'women_shirt_images/fit/' . $fabric_id . '_fitstyle_' . $style_type . '_size_' . $cuff_size . '_fittype_' . $cuff_type . '_' . $shirtfit_id . '_view_4.png ' . $glow_fold_image . ' -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/fit/' . $fabric_id . '_fitstyle_' . $style_type . '_size_' . $cuff_size . '_fittype_' . $cuff_type . '_' . $shirtfit_id . '_view_4.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);
//highlighted
                            $cmd = ' composite ' . $highlighted_fold_image . ' -compose Overlay  ' . $homepath . 'women_shirt_images/fit/' . $fabric_id . '_fitstyle_' . $style_type . '_size_' . $cuff_size . '_fittype_' . $cuff_type . '_' . $shirtfit_id . '_view_4.png ' . $homepath . 'women_shirt_images/fit/' . $fabric_id . '_fitstyle_' . $style_type . '_size_' . $cuff_size . '_fittype_' . $cuff_type . '_' . $shirtfit_id . '_view_4.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);
                        }
//
// 25-10-2017 Zoom View Code START
                        $glow_zoom_image = $homepath . '' . $fit['glow_zoom_image'];
                        $mask_zoom_image = $homepath . '' . $fit['mask_zoom_image'];
                        $highlighted_zoom_image = $homepath . '' . $fit['highlighted_zoom_image'];
                        if ($fit['glow_zoom_image'] != '' && $fit['mask_zoom_image'] != '' && $fit['highlighted_zoom_image'] != '') {
                            $cmd = ' composite -compose CopyOpacity ' . $mask_zoom_image . ' ' . $fabric_zoom . ' ' . $homepath . 'women_shirt_images/fit/' . $fabric_id . '_fitstyle_' . $style_type . '_size_' . $cuff_size . '_fittype_' . $cuff_type . '_' . $shirtfit_id . '_view_5.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);
//glow
                            $cmd = ' convert ' . $homepath . 'women_shirt_images/fit/' . $fabric_id . '_fitstyle_' . $style_type . '_size_' . $cuff_size . '_fittype_' . $cuff_type . '_' . $shirtfit_id . '_view_5.png ' . $glow_zoom_image . ' -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/fit/' . $fabric_id . '_fitstyle_' . $style_type . '_size_' . $cuff_size . '_fittype_' . $cuff_type . '_' . $shirtfit_id . '_view_5.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);
//highlighted
                            $cmd = ' composite ' . $highlighted_zoom_image . ' -compose Overlay  ' . $homepath . 'women_shirt_images/fit/' . $fabric_id . '_fitstyle_' . $style_type . '_size_' . $cuff_size . '_fittype_' . $cuff_type . '_' . $shirtfit_id . '_view_5.png ' . $homepath . 'women_shirt_images/fit/' . $fabric_id . '_fitstyle_' . $style_type . '_size_' . $cuff_size . '_fittype_' . $cuff_type . '_' . $shirtfit_id . '_view_5.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);
                        }
//25-10-2017 Zoom View Code END
                    }

// shirt fit end.
//shirt plackets

                    $shirtplackets_id = "common";

                    $behind_placket_front_glow = $homepath . 'women_glow_mask_shirt/Front_View/Plackets/Behind_Placket_shad.png';
                    $behind_placket_front_mask = $homepath . 'women_glow_mask_shirt/Front_View/Plackets/Behind_Placket_mask.png';
                    $behind_placket_front_highlighted = $homepath . 'women_glow_mask_shirt/Front_View/Plackets/Behind_Placket_hi.png';

                    $cmd = ' composite -compose Dst_In -gravity center ' . $behind_placket_front_mask . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'women_shirt_images/plackets/' . $fabric_id . '_inner_placket' . $shirtplackets_id . '_view_1.png';
                    $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($query);

                    $cmd = ' convert ' . $homepath . 'women_shirt_images/plackets/' . $fabric_id . '_inner_placket' . $shirtplackets_id . '_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/plackets/' . $fabric_id . '_inner_placket' . $shirtplackets_id . '_view_1.png';
                    $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($query);

                    $cmd = ' convert ' . $homepath . 'women_shirt_images/plackets/' . $fabric_id . '_inner_placket' . $shirtplackets_id . '_view_1.png ' . $behind_placket_front_glow . ' -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/plackets/' . $fabric_id . '_inner_placket' . $shirtplackets_id . '_view_1.png';
                    $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($query);
//highlight
                    $cmd = ' composite ' . $behind_placket_front_highlighted . ' -compose Overlay  ' . $homepath . 'women_shirt_images/plackets/' . $fabric_id . '_inner_placket' . $shirtplackets_id . '_view_1.png ' . $homepath . 'women_shirt_images/plackets/' . $fabric_id . '_inner_placket' . $shirtplackets_id . '_view_1.png';
                    $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($query);

                    $behind_placket_side_glow = $homepath . 'women_glow_mask_shirt/Side_View/Plackets/Behind_Placket_shad.png';
                    $behind_placket_side_mask = $homepath . 'women_glow_mask_shirt/Side_View/Plackets/Behind_Placket_mask.png';
                    $behind_placket_side_highlighted = $homepath . 'women_glow_mask_shirt/Side_View/Plackets/Behind_Placket_hi.png';

                    $cmd = ' composite -compose Dst_In -gravity center ' . $behind_placket_side_mask . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'women_shirt_images/plackets/' . $fabric_id . '_inner_placket' . $shirtplackets_id . '_view_2.png';
                    $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($query);

                    $cmd = ' convert ' . $homepath . 'women_shirt_images/plackets/' . $fabric_id . '_inner_placket' . $shirtplackets_id . '_view_2.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/plackets/' . $fabric_id . '_inner_placket' . $shirtplackets_id . '_view_2.png';
                    $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($query);

                    $cmd = ' convert ' . $homepath . 'women_shirt_images/plackets/' . $fabric_id . '_inner_placket' . $shirtplackets_id . '_view_2.png ' . $behind_placket_side_glow . ' -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/plackets/' . $fabric_id . '_inner_placket' . $shirtplackets_id . '_view_2.png';
                    $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($query);
//highlight
                    $cmd = ' composite ' . $behind_placket_side_highlighted . ' -compose Overlay  ' . $homepath . 'women_shirt_images/plackets/' . $fabric_id . '_inner_placket' . $shirtplackets_id . '_view_2.png ' . $homepath . 'women_shirt_images/plackets/' . $fabric_id . '_inner_placket' . $shirtplackets_id . '_view_2.png';
                    $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($query);

                    $womenShirtPlackets = $this->_womenShirtPlackets->create()->getCollection();
                    $rows_plackets = $womenShirtPlackets->getData();
                    foreach ($rows_plackets as $plackets) {

                        $shirtplackets_id = $plackets['shirtplackets_id'];

                        $plackets_glow = $homepath . '' . $plackets['glow_front_image'];
                        $plackets_mask = $homepath . '' . $plackets['mask_front_image'];
                        $plackets_highlighted = $homepath . '' . $plackets['highlighted_front_image'];

// casual view side placket.
                        $casual_mask_side_image = $homepath . "women_glow_mask_shirt/Side_View/Collar_Casual/casual_side_placket/Shirt_Placket_Side_mask.png";
                        $casual_shad_side_image = $homepath . "women_glow_mask_shirt/Side_View/Collar_Casual/casual_side_placket/Shirt_Placket_Side_shad.png";
                        $casual_hi_side_view = $homepath . "women_glow_mask_shirt/Side_View/Collar_Casual/casual_side_placket/Shirt_Placket_Side_hi.png";

                        $cmd = 'composite -compose Dst_In -gravity center ' . $casual_mask_side_image . ' ' . $fabric_side_view . ' -alpha Set ' . $homepath . 'women_shirt_images/plackets/' . $fabric_id . '_casual_plackets_' . $shirtplackets_id . '_view_2.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

                        $cmd = 'convert ' . $homepath . 'women_shirt_images/plackets/' . $fabric_id . '_casual_plackets_' . $shirtplackets_id . '_view_2.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/plackets/' . $fabric_id . '_casual_plackets_' . $shirtplackets_id . '_view_2.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

                        $cmd = 'convert ' . $homepath . 'women_shirt_images/plackets/' . $fabric_id . '_casual_plackets_' . $shirtplackets_id . '_view_2.png ' . $casual_shad_side_image . ' -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/plackets/' . $fabric_id . '_casual_plackets_' . $shirtplackets_id . '_view_2.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//highlight
                        $cmd = 'composite ' . $casual_hi_side_view . ' -compose Overlay  ' . $homepath . 'women_shirt_images/plackets/' . $fabric_id . '_casual_plackets_' . $shirtplackets_id . '_view_2.png ' . $homepath . 'women_shirt_images/plackets/' . $fabric_id . '_casual_plackets_' . $shirtplackets_id . '_view_2.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

// new plackets images.
//mask image
                        $cmd = ' composite -compose CopyOpacity ' . $homepath . 'women_glow_mask_shirt/Zoom_View/plackets/Placket_mask.png ' . $shirtFabImgPath . $fabric_id . '_fabric_zoom.png ' . $homepath . 'women_shirt_images/plackets/' . $fabric_id . '_plackets_' . $shirtplackets_id . '_view_5.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//glow image
                        $cmd = ' convert ' . $homepath . 'women_shirt_images/plackets/' . $fabric_id . '_plackets_' . $shirtplackets_id . '_view_5.png ' . $homepath . 'women_glow_mask_shirt/Zoom_View/plackets/Placket_shad.png -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/plackets/' . $fabric_id . '_plackets_' . $shirtplackets_id . '_view_5.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//highlighted
                        $cmd = ' composite ' . $homepath . 'women_glow_mask_shirt/Zoom_View/plackets/Placket_hi.png -compose Overlay  ' . $homepath . 'women_shirt_images/plackets/' . $fabric_id . '_plackets_' . $shirtplackets_id . '_view_5.png ' . $homepath . 'women_shirt_images/plackets/' . $fabric_id . '_plackets_' . $shirtplackets_id . '_view_5.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//end

                        if ($plackets_glow != '' && $plackets_mask != '') {
                            $cmd = ' composite -compose Dst_In -gravity center ' . $plackets_mask . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'women_shirt_images/plackets/' . $fabric_id . '_plackets_' . $shirtplackets_id . '_view_1.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

                            $cmd = ' convert ' . $homepath . 'women_shirt_images/plackets/' . $fabric_id . '_plackets_' . $shirtplackets_id . '_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/plackets/' . $fabric_id . '_plackets_' . $shirtplackets_id . '_view_1.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

                            $cmd = ' convert ' . $homepath . 'women_shirt_images/plackets/' . $fabric_id . '_plackets_' . $shirtplackets_id . '_view_1.png ' . $plackets_glow . ' -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/plackets/' . $fabric_id . '_plackets_' . $shirtplackets_id . '_view_1.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);
//highlight
                            $cmd = ' composite ' . $plackets_highlighted . ' -compose Overlay  ' . $homepath . 'women_shirt_images/plackets/' . $fabric_id . '_plackets_' . $shirtplackets_id . '_view_1.png ' . $homepath . 'women_shirt_images/plackets/' . $fabric_id . '_plackets_' . $shirtplackets_id . '_view_1.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);
                        }

                        $glow_side_image = $homepath . '' . $plackets['glow_side_image'];
                        $mask_side_image = $homepath . '' . $plackets['mask_side_image'];
                        $highlighted_side_image = $homepath . '' . $plackets['highlighted_side_image'];

                        if ($glow_side_image != '' && $mask_side_image != '') {
                            $cmd = ' composite -compose Dst_In -gravity center ' . $mask_side_image . ' ' . $fabric_side_view . ' -alpha Set ' . $homepath . 'women_shirt_images/plackets/' . $fabric_id . '_plackets_' . $shirtplackets_id . '_view_2.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

                            $cmd = ' convert ' . $homepath . 'women_shirt_images/plackets/' . $fabric_id . '_plackets_' . $shirtplackets_id . '_view_2.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/plackets/' . $fabric_id . '_plackets_' . $shirtplackets_id . '_view_2.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

                            $cmd = ' convert ' . $homepath . 'women_shirt_images/plackets/' . $fabric_id . '_plackets_' . $shirtplackets_id . '_view_2.png ' . $glow_side_image . ' -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/plackets/' . $fabric_id . '_plackets_' . $shirtplackets_id . '_view_2.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);
//highlight
                            $cmd = ' composite ' . $highlighted_side_image . ' -compose Overlay  ' . $homepath . 'women_shirt_images/plackets/' . $fabric_id . '_plackets_' . $shirtplackets_id . '_view_2.png ' . $homepath . 'women_shirt_images/plackets/' . $fabric_id . '_plackets_' . $shirtplackets_id . '_view_2.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);
                        }

                        $glow_fold_image = $homepath . '' . $plackets['glow_fold_image'];
                        $mask_fold_image = $homepath . '' . $plackets['mask_fold_image'];
                        $highlighted_fold_image = $homepath . '' . $plackets['highlighted_fold_image'];

                        if ($glow_fold_image != '' && $mask_fold_image != '') {
                            $cmd = ' composite -compose Dst_In -gravity center ' . $mask_fold_image . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'women_shirt_images/plackets/' . $fabric_id . '_plackets_' . $shirtplackets_id . '_view_4.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

                            $cmd = ' convert ' . $homepath . 'women_shirt_images/plackets/' . $fabric_id . '_plackets_' . $shirtplackets_id . '_view_4.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/plackets/' . $fabric_id . '_plackets_' . $shirtplackets_id . '_view_4.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

                            $cmd = ' convert ' . $homepath . 'women_shirt_images/plackets/' . $fabric_id . '_plackets_' . $shirtplackets_id . '_view_4.png ' . $glow_fold_image . ' -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/plackets/' . $fabric_id . '_plackets_' . $shirtplackets_id . '_view_4.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);
//highlight
                            $cmd = ' composite ' . $highlighted_fold_image . ' -compose Overlay  ' . $homepath . 'women_shirt_images/plackets/' . $fabric_id . '_plackets_' . $shirtplackets_id . '_view_4.png ' . $homepath . 'women_shirt_images/plackets/' . $fabric_id . '_plackets_' . $shirtplackets_id . '_view_4.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);
                        }
                    }




                    /* Create cuff images */

                    $womenShirtCuff = $this->_womenShirtCuff->create()->getCollection();
                    $rows_cuff = $womenShirtCuff->getData();
                    $cuff_id = 1;
                    $style_type = 1;
                    $cuff_size = '';
                    $cuff_type = '';
//Cuff Contrast START

                    $contrast_cuff_left_glow = $homepath . 'women_glow_mask_shirt/Back_View/Cuffs/FoldedOuterCuff_left_shad.png';
                    $contrast_cuff_left_mask = $homepath . 'women_glow_mask_shirt/Back_View/Cuffs/FoldedOuterCuff_left_mask.png';
                    $contrast_cuff_left_highlighted = $homepath . 'women_glow_mask_shirt/Back_View/Cuffs/FoldedOuterCuff_left_hi.png';
                    $contrast_cuff_right_glow = $homepath . 'women_glow_mask_shirt/Back_View/Cuffs/FoldedOuterCuff_right_shad.png';
                    $contrast_cuff_right_mask = $homepath . 'women_glow_mask_shirt/Back_View/Cuffs/FoldedOuterCuff_right_mask.png';
                    $contrast_cuff_right_highlighted = $homepath . 'women_glow_mask_shirt/Back_View/Cuffs/FoldedOuterCuff_right_hi.png';

                    $cmd = ' composite -compose Dst_In -gravity center ' . $contrast_cuff_left_mask . ' ' . $shirtFabImgPath . $fabric_id . '_fabric_main_back.png -alpha Set ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_contrast_outer_cuff_' . $cuff_type . '_' . $cuff_id . '_left_view_4.png';
                    $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($query);

                    $cmd = ' convert ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_contrast_outer_cuff_' . $cuff_type . '_' . $cuff_id . '_left_view_4.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_contrast_outer_cuff_' . $cuff_type . '_' . $cuff_id . '_left_view_4.png';
                    $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($query);

                    $cmd = ' convert ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_contrast_outer_cuff_' . $cuff_type . '_' . $cuff_id . '_left_view_4.png ' . $contrast_cuff_left_glow . ' -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_contrast_outer_cuff_' . $cuff_type . '_' . $cuff_id . '_left_view_4.png';
                    $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($query);
//highlight
                    $cmd = ' composite ' . $contrast_cuff_left_highlighted . ' -compose Overlay  ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cufftype_' . $cuff_id . '_left_view_4.png ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cufftype_' . $cuff_id . '_left_view_4.png';
                    $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($query);

                    $cmd = ' composite -compose Dst_In -gravity center ' . $contrast_cuff_right_mask . ' ' . $shirtFabImgPath . $fabric_id . '_fabric_main_back.png -alpha Set ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_contrast_outer_cuff_' . $cuff_type . '_' . $cuff_id . '_right_view_4.png';
                    $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($query);

                    $cmd = ' convert ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_contrast_outer_cuff_' . $cuff_type . '_' . $cuff_id . '_right_view_4.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_contrast_outer_cuff_' . $cuff_type . '_' . $cuff_id . '_right_view_4.png';
                    $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($query);

                    $cmd = ' convert ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_contrast_outer_cuff_' . $cuff_type . '_' . $cuff_id . '_right_view_4.png ' . $contrast_cuff_right_glow . ' -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_contrast_outer_cuff_' . $cuff_type . '_' . $cuff_id . '_right_view_4.png';
                    $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($query);
//highlight
                    $cmd = ' composite ' . $contrast_cuff_right_highlighted . ' -compose Overlay  ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_contrast_outer_cuff_' . $cuff_type . '_' . $cuff_id . '_right_view_4.png ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_contrast_outer_cuff_' . $cuff_type . '_' . $cuff_id . '_right_view_4.png';
                    $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($query);
//combine
                    $cmd = ' convert ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_contrast_outer_cuff_' . $cuff_type . '_' . $cuff_id . '_right_view_4.png ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_contrast_outer_cuff_' . $cuff_type . '_' . $cuff_id . '_left_view_4.png -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_contrast_outer_cuff_' . $cuff_type . '_' . $cuff_id . '_final_back_cuff.png';
                    $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($query);

                    $cmd = "rm " . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_contrast_outer_cuff_' . $cuff_type . '_' . $cuff_id . '_right_view_4.png';
                    $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($query);

                    $cmd = "rm " . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_contrast_outer_cuff_' . $cuff_type . '_' . $cuff_id . '_left_view_4.png';
                    $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($query);


//Cuff Contrast END
//open cuff images
                    $opencuff_left_glow = $homepath . 'women_glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_shad.png';
                    $opencuff_left_mask = $homepath . 'women_glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png';
                    $opencuff_left_highlighted = $homepath . 'women_glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_hi.png';
                    $opencuff_right_glow = $homepath . 'women_glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_right_shad.png';
                    $opencuff_right_mask = $homepath . 'women_glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_right_mask.png';
                    $opencuff_right_highlighted = $homepath . 'women_glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_right_hi.png';

                    $cmd = ' composite -compose Dst_In -gravity center ' . $opencuff_left_mask . ' ' . $shirtFabImgPath . $fabric_id . '_fabric_main_back.png -alpha Set ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_opencuff_' . $cuff_type . '_' . $cuff_id . '_left_view_1.png';
                    $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($query);

                    $cmd = ' convert ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_opencuff_' . $cuff_type . '_' . $cuff_id . '_left_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_opencuff_' . $cuff_type . '_' . $cuff_id . '_left_view_1.png';
                    $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($query);

                    $cmd = ' convert ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_opencuff_' . $cuff_type . '_' . $cuff_id . '_left_view_1.png ' . $opencuff_left_glow . ' -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_opencuff_' . $cuff_type . '_' . $cuff_id . '_left_view_1.png';
                    $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($query);
//highlight
                    $cmd = ' composite ' . $opencuff_left_highlighted . ' -compose Overlay  ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cufftype_' . $cuff_id . '_left_view_1.png ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cufftype_' . $cuff_id . '_left_view_1.png';
                    $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($query);

                    $cmd = ' composite -compose Dst_In -gravity center ' . $opencuff_right_mask . ' ' . $shirtFabImgPath . $fabric_id . '_fabric_main_back.png -alpha Set ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_opencuff_' . $cuff_type . '_' . $cuff_id . '_right_view_1.png';
                    $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($query);

                    $cmd = ' convert ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_opencuff_' . $cuff_type . '_' . $cuff_id . '_right_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_opencuff_' . $cuff_type . '_' . $cuff_id . '_right_view_1.png';
                    $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($query);

                    $cmd = ' convert ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_opencuff_' . $cuff_type . '_' . $cuff_id . '_right_view_1.png ' . $opencuff_right_glow . ' -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_opencuff_' . $cuff_type . '_' . $cuff_id . '_right_view_1.png';
                    $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($query);
//highlight
                    $cmd = ' composite ' . $opencuff_right_highlighted . ' -compose Overlay  ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_opencuff_' . $cuff_type . '_' . $cuff_id . '_right_view_1.png ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_opencuff_' . $cuff_type . '_' . $cuff_id . '_right_view_1.png';
                    $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($query);
//combine
                    $cmd = ' convert ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_opencuff_' . $cuff_type . '_' . $cuff_id . '_right_view_1.png ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_opencuff_' . $cuff_type . '_' . $cuff_id . '_left_view_1.png -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_opencuff_' . $cuff_type . '_' . $cuff_id . '_final_front_cuff.png';
                    $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($query);

                    $cmd = "rm " . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_opencuff_' . $cuff_type . '_' . $cuff_id . '_right_view_1.png';
                    $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($query);

                    $cmd = "rm " . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_opencuff_' . $cuff_type . '_' . $cuff_id . '_left_view_1.png';
                    $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($query);


                    $opencuff_side_left_glow = $homepath . 'women_glow_mask_shirt/Open_Cuffs/Side/CasualCuff_Side_left_shad.png';
                    $opencuff_side_left_mask = $homepath . 'women_glow_mask_shirt/Open_Cuffs/Side/CasualCuff_Side_left_mask.png';
                    $opencuff_side_left_highlighted = $homepath . 'women_glow_mask_shirt/Open_Cuffs/Side/CasualCuff_Side_left_hi.png';
                    $opencuff_side_right_glow = $homepath . 'women_glow_mask_shirt/Open_Cuffs/Side/CasualCuff_Side_right_shad.png';
                    $opencuff_side_right_mask = $homepath . 'women_glow_mask_shirt/Open_Cuffs/Side/CasualCuff_Side_right_mask.png';
                    $opencuff_side_right_highlighted = $homepath . 'women_glow_mask_shirt/Open_Cuffs/Side/CasualCuff_Side_right_hi.png';

                    $cmd = ' composite -compose Dst_In -gravity center ' . $opencuff_side_left_mask . ' ' . $shirtFabImgPath . $fabric_id . '_fabric_main_back.png -alpha Set ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_opencuff_' . $cuff_type . '_' . $cuff_id . '_left_view_2.png';
                    $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($query);

                    $cmd = ' convert ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_opencuff_' . $cuff_type . '_' . $cuff_id . '_left_view_2.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_opencuff_' . $cuff_type . '_' . $cuff_id . '_left_view_2.png';
                    $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($query);

                    $cmd = ' convert ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_opencuff_' . $cuff_type . '_' . $cuff_id . '_left_view_2.png ' . $opencuff_side_left_glow . ' -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_opencuff_' . $cuff_type . '_' . $cuff_id . '_left_view_2.png';
                    $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($query);
//highlight
                    $cmd = ' composite ' . $opencuff_side_left_highlighted . ' -compose Overlay  ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cufftype_' . $cuff_id . '_left_view_2.png ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cufftype_' . $cuff_id . '_left_view_2.png';
                    $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($query);

                    $cmd = ' composite -compose Dst_In -gravity center ' . $opencuff_side_right_mask . ' ' . $shirtFabImgPath . $fabric_id . '_fabric_main_back.png -alpha Set ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_opencuff_' . $cuff_type . '_' . $cuff_id . '_right_view_2.png';
                    $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($query);

                    $cmd = ' convert ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_opencuff_' . $cuff_type . '_' . $cuff_id . '_right_view_2.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_opencuff_' . $cuff_type . '_' . $cuff_id . '_right_view_2.png';
                    $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($query);

                    $cmd = ' convert ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_opencuff_' . $cuff_type . '_' . $cuff_id . '_right_view_2.png ' . $opencuff_side_right_glow . ' -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_opencuff_' . $cuff_type . '_' . $cuff_id . '_right_view_2.png';
                    $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($query);
//highlight
                    $cmd = ' composite ' . $opencuff_side_right_highlighted . ' -compose Overlay  ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_opencuff_' . $cuff_type . '_' . $cuff_id . '_right_view_2.png ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_opencuff_' . $cuff_type . '_' . $cuff_id . '_right_view_2.png';
                    $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($query);
//combine
                    $cmd = ' convert ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_opencuff_' . $cuff_type . '_' . $cuff_id . '_right_view_2.png ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_opencuff_' . $cuff_type . '_' . $cuff_id . '_left_view_2.png -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_opencuff_' . $cuff_type . '_' . $cuff_id . '_final_side_cuff.png';
                    $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($query);

                    $cmd = "rm " . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_opencuff_' . $cuff_type . '_' . $cuff_id . '_right_view_2.png';
                    $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($query);

                    $cmd = "rm " . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_opencuff_' . $cuff_type . '_' . $cuff_id . '_left_view_2.png';
                    $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($query);

                    $opencuff_back_left_glow = $homepath . 'women_glow_mask_shirt/Open_Cuffs/Back/CasualCuffs_Back_Left_shad.png';
                    $opencuff_back_left_mask = $homepath . 'women_glow_mask_shirt/Open_Cuffs/Back/CasualCuffs_Back_Left_mask.png';
                    $opencuff_back_left_highlighted = $homepath . 'women_glow_mask_shirt/Open_Cuffs/Back/CasualCuffs_Back_Left_hi.png';
                    $opencuff_back_right_glow = $homepath . 'women_glow_mask_shirt/Open_Cuffs/Back/CasualCuffs_Back_Right_shad.png';
                    $opencuff_back_right_mask = $homepath . 'women_glow_mask_shirt/Open_Cuffs/Back/CasualCuffs_Back_Right_mask.png';
                    $opencuff_back_right_highlighted = $homepath . 'women_glow_mask_shirt/Open_Cuffs/Back/CasualCuffs_Back_Right_hi.png';

                    $cmd = ' composite -compose Dst_In -gravity center ' . $opencuff_back_left_mask . ' ' . $shirtFabImgPath . $fabric_id . '_fabric_main_back.png -alpha Set ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_opencuff_' . $cuff_type . '_' . $cuff_id . '_left_view_3.png';
                    $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($query);

                    $cmd = ' convert ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_opencuff_' . $cuff_type . '_' . $cuff_id . '_left_view_3.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_opencuff_' . $cuff_type . '_' . $cuff_id . '_left_view_3.png';
                    $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($query);

                    $cmd = ' convert ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_opencuff_' . $cuff_type . '_' . $cuff_id . '_left_view_3.png ' . $opencuff_back_left_glow . ' -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_opencuff_' . $cuff_type . '_' . $cuff_id . '_left_view_3.png';
                    $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($query);
//highlight
                    $cmd = ' composite ' . $opencuff_back_left_highlighted . ' -compose Overlay  ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_opencuff_' . $cuff_type . '_' . $cuff_id . '_left_view_3.png ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_opencuff_' . $cuff_type . '_' . $cuff_id . '_left_view_3.png';
                    $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($query);

                    $cmd = ' composite -compose Dst_In -gravity center ' . $opencuff_back_right_mask . ' ' . $shirtFabImgPath . $fabric_id . '_fabric_main_back.png -alpha Set ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_opencuff_' . $cuff_type . '_' . $cuff_id . '_right_view_3.png';
                    $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($query);

                    $cmd = ' convert ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_opencuff_' . $cuff_type . '_' . $cuff_id . '_right_view_3.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_opencuff_' . $cuff_type . '_' . $cuff_id . '_right_view_3.png';
                    $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($query);

                    $cmd = ' convert ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_opencuff_' . $cuff_type . '_' . $cuff_id . '_right_view_3.png ' . $opencuff_back_right_glow . ' -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_opencuff_' . $cuff_type . '_' . $cuff_id . '_right_view_3.png';
                    $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($query);
//highlight
                    $cmd = ' composite ' . $opencuff_back_right_highlighted . ' -compose Overlay  ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_opencuff_' . $cuff_type . '_' . $cuff_id . '_right_view_3.png ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_opencuff_' . $cuff_type . '_' . $cuff_id . '_right_view_3.png';
                    $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($query);
//combine
                    $cmd = ' convert ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_opencuff_' . $cuff_type . '_' . $cuff_id . '_right_view_3.png ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_opencuff_' . $cuff_type . '_' . $cuff_id . '_left_view_3.png -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_opencuff_' . $cuff_type . '_' . $cuff_id . '_final_back_cuff.png';
                    $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($query);

                    $cmd = "rm " . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_opencuff_' . $cuff_type . '_' . $cuff_id . '_right_view_3.png';
                    $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($query);

                    $cmd = "rm " . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_opencuff_' . $cuff_type . '_' . $cuff_id . '_left_view_3.png';
                    $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($query);
//


                    foreach ($rows_cuff as $cuffs) {
                        $cuff_id = $cuffs['shirtcuff_id'];
                        $style_type = $cuffs['type'];
                        $cuff_size = '';
                        $cuff_type = '';

                        $cuff_left_glow = $homepath . '' . $cuffs['glow_front_left_image'];
                        $cuff_left_mask = $homepath . '' . $cuffs['mask_front_left_image'];
                        $cuff_left_highlighted = $homepath . '' . $cuffs['highlighted_front_left_image'];
                        $cuff_right_glow = $homepath . '' . $cuffs['glow_front_right_image'];
                        $cuff_right_mask = $homepath . '' . $cuffs['mask_front_right_image'];
                        $cuff_right_highlighted = $homepath . '' . $cuffs['highlighted_front_right_image'];
//view 1
                        if ($cuffs['glow_front_left_image'] != '' && $cuffs['mask_front_left_image'] != '') {

                            $cmd = ' composite -compose Dst_In -gravity center ' . $cuff_left_mask . ' ' . $shirtFabImgPath . $fabric_id . '_fabric_main_back.png -alpha Set ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_cufftype_' . $cuff_type . '_' . $cuff_id . '_left_view_1.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

                            $cmd = ' convert ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_cufftype_' . $cuff_type . '_' . $cuff_id . '_left_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_cufftype_' . $cuff_type . '_' . $cuff_id . '_left_view_1.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

                            $cmd = ' convert ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_cufftype_' . $cuff_type . '_' . $cuff_id . '_left_view_1.png ' . $cuff_left_glow . ' -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_cufftype_' . $cuff_type . '_' . $cuff_id . '_left_view_1.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);
//highlight
                            $cmd = ' composite ' . $cuff_left_highlighted . ' -compose Overlay  ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_cufftype_' . $cuff_type . '_' . $cuff_id . '_left_view_1.png ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_cufftype_' . $cuff_type . '_' . $cuff_id . '_left_view_1.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

                            $cmd = ' composite -compose Dst_In -gravity center ' . $cuff_right_mask . ' ' . $shirtFabImgPath . $fabric_id . '_fabric_main_back.png -alpha Set ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_cufftype_' . $cuff_type . '_' . $cuff_id . '_right_view_1.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

                            $cmd = ' convert ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_cufftype_' . $cuff_type . '_' . $cuff_id . '_right_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_cufftype_' . $cuff_type . '_' . $cuff_id . '_right_view_1.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

                            $cmd = ' convert ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_cufftype_' . $cuff_type . '_' . $cuff_id . '_right_view_1.png ' . $cuff_right_glow . ' -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_cufftype_' . $cuff_type . '_' . $cuff_id . '_right_view_1.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

//highlight
                            $cmd = ' composite ' . $cuff_right_highlighted . ' -compose Overlay  ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_cufftype_' . $cuff_type . '_' . $cuff_id . '_right_view_1.png ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_cufftype_' . $cuff_type . '_' . $cuff_id . '_right_view_1.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);
//combine
                            $cmd = ' convert ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_cufftype_' . $cuff_type . '_' . $cuff_id . '_left_view_1.png ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_cufftype_' . $cuff_type . '_' . $cuff_id . '_right_view_1.png -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_cufftype_' . $cuff_type . '_' . $cuff_id . '_view_1.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

                            $cmd = "rm " . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_cufftype_' . $cuff_type . '_' . $cuff_id . '_right_view_1.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

                            $cmd = "rm " . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_cufftype_' . $cuff_type . '_' . $cuff_id . '_left_view_1.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

//opencuff
                        }

//view 2
                        $cuff_side_left_glow = $homepath . '' . $cuffs['glow_side_left_image'];
                        $cuff_side_left_mask = $homepath . '' . $cuffs['mask_side_left_image'];
                        $cuff_side_left_highlighted = $homepath . '' . $cuffs['highlighted_side_left_image'];
                        $cuff_side_right_glow = $homepath . '' . $cuffs['glow_side_right_image'];
                        $cuff_side_right_mask = $homepath . '' . $cuffs['mask_side_right_image'];
                        $cuff_side_right_highlighted = $homepath . '' . $cuffs['highlighted_side_right_image'];

                        if ($cuffs['glow_side_left_image'] != '' && $cuffs['mask_side_left_image'] != '') {

                            $cmd = ' composite -compose Dst_In -gravity center ' . $cuff_side_left_mask . ' ' . $shirtFabImgPath . $fabric_id . '_fabric_main_back.png -alpha Set ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_cufftype_' . $cuff_type . '_' . $cuff_id . '_left_view_2.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

                            $cmd = ' convert ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_cufftype_' . $cuff_type . '_' . $cuff_id . '_left_view_2.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_cufftype_' . $cuff_type . '_' . $cuff_id . '_left_view_2.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

                            $cmd = ' convert ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_cufftype_' . $cuff_type . '_' . $cuff_id . '_left_view_2.png ' . $cuff_side_left_glow . ' -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_cufftype_' . $cuff_type . '_' . $cuff_id . '_left_view_2.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);
//highlight
                            $cmd = ' composite ' . $cuff_side_left_highlighted . ' -compose Overlay  ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cufftype_' . $cuff_id . '_left_view_2.png ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cufftype_' . $cuff_id . '_left_view_2.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

                            $cmd = ' composite -compose Dst_In -gravity center ' . $cuff_side_right_mask . ' ' . $shirtFabImgPath . $fabric_id . '_fabric_main_back.png -alpha Set ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_cufftype_' . $cuff_type . '_' . $cuff_id . '_right_view_2.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

                            $cmd = ' convert ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_cufftype_' . $cuff_type . '_' . $cuff_id . '_right_view_2.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_cufftype_' . $cuff_type . '_' . $cuff_id . '_right_view_2.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

                            $cmd = ' convert ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_cufftype_' . $cuff_type . '_' . $cuff_id . '_right_view_2.png ' . $cuff_side_right_glow . ' -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_cufftype_' . $cuff_type . '_' . $cuff_id . '_right_view_2.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

//highlight
                            $cmd = ' composite ' . $cuff_side_right_highlighted . ' -compose Overlay  ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_cufftype_' . $cuff_type . '_' . $cuff_id . '_right_view_2.png ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_cufftype_' . $cuff_type . '_' . $cuff_id . '_right_view_2.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

//combine
                            $cmd = ' convert ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_cufftype_' . $cuff_type . '_' . $cuff_id . '_right_view_2.png ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_cufftype_' . $cuff_type . '_' . $cuff_id . '_left_view_2.png -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_cufftype_' . $cuff_type . '_' . $cuff_id . '_view_2.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

                            $cmd = "rm " . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_cufftype_' . $cuff_type . '_' . $cuff_id . '_right_view_2.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

                            $cmd = "rm " . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_cufftype_' . $cuff_type . '_' . $cuff_id . '_left_view_2.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);
//opencuff
                        }


//view 3
                        $cuff_back_left_glow = $homepath . '' . $cuffs['glow_back_left_image'];
                        $cuff_back_left_mask = $homepath . '' . $cuffs['mask_back_left_image'];
                        $cuff_back_left_highlighted = $homepath . '' . $cuffs['highlighted_back_left_image'];
                        $cuff_back_right_glow = $homepath . '' . $cuffs['glow_back_right_image'];
                        $cuff_back_right_mask = $homepath . '' . $cuffs['mask_back_right_image'];
                        $cuff_back_right_highlighted = $homepath . '' . $cuffs['highlighted_back_right_image'];

                        if ($cuffs['glow_back_left_image'] != '' && $cuffs['mask_back_left_image'] != '') {

                            $cmd = ' composite -compose Dst_In -gravity center ' . $cuff_back_left_mask . ' ' . $shirtFabImgPath . $fabric_id . '_fabric_main_back.png -alpha Set ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_cufftype_' . $cuff_type . '_' . $cuff_id . '_left_view_3.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

                            $cmd = ' convert ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_cufftype_' . $cuff_type . '_' . $cuff_id . '_left_view_3.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_cufftype_' . $cuff_type . '_' . $cuff_id . '_left_view_3.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

                            $cmd = ' convert ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_cufftype_' . $cuff_type . '_' . $cuff_id . '_left_view_3.png ' . $cuff_back_left_glow . ' -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_cufftype_' . $cuff_type . '_' . $cuff_id . '_left_view_3.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

//highlight
                            $cmd = ' composite ' . $cuff_back_left_highlighted . ' -compose Overlay  ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_cufftype_' . $cuff_type . '_' . $cuff_id . '_left_view_3.png ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_cufftype_' . $cuff_type . '_' . $cuff_id . '_left_view_3.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

                            $cmd = ' composite -compose Dst_In -gravity center ' . $cuff_back_right_mask . ' ' . $shirtFabImgPath . $fabric_id . '_fabric_main_back.png -alpha Set ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_cufftype_' . $cuff_type . '_' . $cuff_id . '_right_view_3.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

                            $cmd = ' convert ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_cufftype_' . $cuff_type . '_' . $cuff_id . '_right_view_3.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_cufftype_' . $cuff_type . '_' . $cuff_id . '_right_view_3.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

                            $cmd = ' convert ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_cufftype_' . $cuff_type . '_' . $cuff_id . '_right_view_3.png ' . $cuff_back_right_glow . ' -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_cufftype_' . $cuff_type . '_' . $cuff_id . '_right_view_3.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);
//highlight
                            $cmd = ' composite ' . $cuff_back_right_highlighted . ' -compose Overlay  ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_cufftype_' . $cuff_type . '_' . $cuff_id . '_right_view_3.png ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_cufftype_' . $cuff_type . '_' . $cuff_id . '_right_view_3.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);
//combine

                            $cmd = ' convert ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_cufftype_' . $cuff_type . '_' . $cuff_id . '_right_view_3.png ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_cufftype_' . $cuff_type . '_' . $cuff_id . '_left_view_3.png -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_cufftype_' . $cuff_type . '_' . $cuff_id . '_view_3.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

                            $cmd = "rm " . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_cufftype_' . $cuff_type . '_' . $cuff_id . '_right_view_3.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

                            $cmd = "rm " . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_cufftype_' . $cuff_type . '_' . $cuff_id . '_left_view_3.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

//opencuff
                        }



//view 4
                        $cuff_fold_main_glow = $homepath . '' . $cuffs['glow_fold_main_image'];
                        $cuff_fold_main_mask = $homepath . '' . $cuffs['mask_fold_main_image'];
                        $cuff_fold_main_highlighted = $homepath . '' . $cuffs['highlighted_fold_main_image'];

                        $inner_fold_inner_glow = $homepath . '' . $cuffs['glow_fold_inner_image'];
                        $inner_fold_inner_mask = $homepath . '' . $cuffs['mask_fold_inner_image'];
                        $inner_fold_inner_highlighted = $homepath . '' . $cuffs['highlighted_fold_inner_image'];


                        if ($cuffs['mask_fold_main_image'] != '' && $cuffs['mask_fold_inner_image'] != '') {

                            $cmd = ' composite -compose Dst_In -gravity center ' . $cuff_fold_main_mask . ' ' . $shirtFabImgPath . $fabric_id . '_fabric_main.png -alpha Set ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_cufftype_' . $cuff_type . '_' . $cuff_id . '_view_4.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

                            $cmd = ' convert ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_cufftype_' . $cuff_type . '_' . $cuff_id . '_view_4.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_cufftype_' . $cuff_type . '_' . $cuff_id . '_view_4.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

                            $cmd = ' convert ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_cufftype_' . $cuff_type . '_' . $cuff_id . '_view_4.png ' . $cuff_fold_main_glow . ' -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_cufftype_' . $cuff_type . '_' . $cuff_id . '_view_4.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);
// issue pending.
//highlight
                            $cmd = ' composite ' . $cuff_fold_main_highlighted . ' -compose Overlay  ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_cufftype_' . $cuff_type . '_' . $cuff_id . '_view_4.png ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_cufftype_' . $cuff_type . '_' . $cuff_id . '_view_4.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);
//inner
                            $cmd = ' composite -compose Dst_In -gravity center ' . $inner_fold_inner_mask . ' ' . $shirtFabImgPath . $fabric_id . '_fabric_main.png -alpha Set ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_inner_' . $cuff_type . '_' . $cuff_id . '_inner_view_4.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

                            $cmd = ' convert ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_inner_' . $cuff_type . '_' . $cuff_id . '_inner_view_4.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_inner_' . $cuff_type . '_' . $cuff_id . '_inner_view_4.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

                            $cmd = ' convert ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_inner_' . $cuff_type . '_' . $cuff_id . '_inner_view_4.png ' . $inner_fold_inner_glow . ' -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_inner_' . $cuff_type . '_' . $cuff_id . '_inner_view_4.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);
//highlight
                            $cmd = ' composite ' . $inner_fold_inner_highlighted . ' -compose Overlay  ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_inner_' . $cuff_type . '_' . $cuff_id . '_inner_view_4.png ' . $homepath . 'women_shirt_images/cuffs/' . $fabric_id . '_cuffstyle_' . $style_type . '_size_' . $cuff_size . '_inner_' . $cuff_type . '_' . $cuff_id . '_inner_view_4.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);
                        }
                    }


                    /* Create style collor images */

                    $womenShirtStyle = $this->_womenShirtStyle->create()->getCollection();
                    $rows = $womenShirtStyle->getData();
                    foreach ($rows as $style) {
                        $style_id = $style['shirtstyle_id'];

                        /* view 1 start */

//
// //step-1: back-collar
// //step 3: Front
// //mask  image

                        $cmd = ' composite -compose Dst_In -gravity center ' . $homepath . 'women_glow_mask_shirt/Front_View/Collar_Styles/BackCollar_mask.png ' . $shirtFabImgPath . $fabric_id . '_fabric_main_back.png -alpha Set ' . $homepath . 'women_shirt_images/collarstyle/back_collar/' . $fabric_id . '_style_' . $style_id . '_front_view_1.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/back_collar/' . $fabric_id . '_style_' . $style_id . '_front_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/collarstyle/back_collar/' . $fabric_id . '_style_' . $style_id . '_front_view_1.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

//glow image
                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/back_collar/' . $fabric_id . '_style_' . $style_id . '_front_view_1.png  ' . $homepath . 'women_glow_mask_shirt/Front_View/Collar_Styles/BackCollar_shad.png -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/collarstyle/back_collar/' . $fabric_id . '_style_' . $style_id . '_front_view_1.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//highlighted
                        $cmd = ' composite ' . $homepath . 'women_glow_mask_shirt/Front_View/Collar_Styles/BackCollar_hi.png -compose Overlay  ' . $homepath . 'women_shirt_images/collarstyle/back_collar/' . $fabric_id . '_style_' . $style_id . '_front_view_1.png ' . $homepath . 'women_shirt_images/collarstyle/back_collar/' . $fabric_id . '_style_' . $style_id . '_front_view_1.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
// back-part
                        $cmd = ' composite -compose Dst_In -gravity center ' . $homepath . 'women_glow_mask_shirt/Front_View/Collar_Styles/BackPart_mask.png ' . $shirtFabImgPath . $fabric_id . '_fabric_main.png -alpha Set ' . $homepath . 'women_shirt_images/collarstyle/back_collar/' . $fabric_id . '_style_' . $style_id . '_front_backpart_view_1.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/back_collar/' . $fabric_id . '_style_' . $style_id . '_front_backpart_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/collarstyle/back_collar/' . $fabric_id . '_style_' . $style_id . '_front_backpart_view_1.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

//glow image
                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/back_collar/' . $fabric_id . '_style_' . $style_id . '_front_backpart_view_1.png  ' . $homepath . 'women_glow_mask_shirt/Front_View/Collar_Styles/BackPart_shad.png -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/collarstyle/back_collar/' . $fabric_id . '_style_' . $style_id . '_front_backpart_view_1.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//highlighted
                        $cmd = ' composite ' . $homepath . 'women_glow_mask_shirt/Front_View/Collar_Styles/BackPart_hi.png -compose Overlay  ' . $homepath . 'women_shirt_images/collarstyle/back_collar/' . $fabric_id . '_style_' . $style_id . '_front_backpart_view_1.png ' . $homepath . 'women_shirt_images/collarstyle/back_collar/' . $fabric_id . '_style_' . $style_id . '_front_backpart_view_1.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
// //step-1: side view
//roate fabric image
                        $cmd = ' convert ' . $fabric_0 . ' -background "rgba(0,0,0,0.5)" -distort SRT 45 ' . $shirtFabImgPath . $fabric_id . '_fabric_main_left.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//mask
                        $cmd = ' composite -compose Dst_In -gravity center ' . $homepath . 'women_glow_mask_shirt/Side_View/Collar_Styles/BackCollar_mask.png ' . $shirtFabImgPath . $fabric_id . '_fabric_main_left.png -alpha Set ' . $homepath . 'women_shirt_images/collarstyle/back_collar/' . $fabric_id . '_style_' . $style_id . '_left_view_2.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/back_collar/' . $fabric_id . '_style_' . $style_id . '_left_view_2.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/collarstyle/back_collar/' . $fabric_id . '_style_' . $style_id . '_left_view_2.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//glow image
                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/back_collar/' . $fabric_id . '_style_' . $style_id . '_left_view_2.png  ' . $homepath . 'women_glow_mask_shirt/Side_View/Collar_Styles/BackCollar_shad.png ' . ' -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/collarstyle/back_collar/' . $fabric_id . '_style_' . $style_id . '_left_view_2.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//highlighted
                        $cmd = ' composite ' . $homepath . 'women_glow_mask_shirt/Side_View/Collar_Styles/BackCollar_hi.png ' . ' -compose Overlay  ' . $homepath . 'women_shirt_images/collarstyle/back_collar/' . $fabric_id . '_style_' . $style_id . '_left_view_2.png ' . $homepath . 'women_shirt_images/collarstyle/back_collar/' . $fabric_id . '_style_' . $style_id . '_left_view_2.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

                        $cmd = ' convert ' . $fabric_0 . ' -background "rgba(0,0,0,0.5)" -distort SRT 45 ' . $shirtFabImgPath . $fabric_id . '_fabric_main_left.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//mask
                        $cmd = ' composite -compose Dst_In -gravity center ' . $homepath . 'women_glow_mask_shirt/Side_View/Collar_Styles/BackPart_mask.png ' . $shirtFabImgPath . $fabric_id . '_fabric_main_left.png -alpha Set ' . $homepath . 'women_shirt_images/collarstyle/back_collar/' . $fabric_id . '_style_' . $style_id . '_left_sidepart_view_2.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/back_collar/' . $fabric_id . '_style_' . $style_id . '_left_sidepart_view_2.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/collarstyle/back_collar/' . $fabric_id . '_style_' . $style_id . '_left_sidepart_view_2.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//glow image
                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/back_collar/' . $fabric_id . '_style_' . $style_id . '_left_sidepart_view_2.png  ' . $homepath . 'women_glow_mask_shirt/Side_View/Collar_Styles/BackPart_shad.png ' . ' -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/collarstyle/back_collar/' . $fabric_id . '_style_' . $style_id . '_left_sidepart_view_2.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//highlighted
                        $cmd = ' composite ' . $homepath . 'women_glow_mask_shirt/Side_View/Collar_Styles/BackPart_hi.png ' . ' -compose Overlay  ' . $homepath . 'women_shirt_images/collarstyle/back_collar/' . $fabric_id . '_style_' . $style_id . '_left_sidepart_view_2.png ' . $homepath . 'women_shirt_images/collarstyle/back_collar/' . $fabric_id . '_style_' . $style_id . '_left_sidepart_view_2.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

// collar casual back part..

                        $cmd = ' composite -compose Dst_In -gravity center ' . $homepath . 'women_glow_mask_shirt/Front_View/Collar_Casual/back_part/BackCollar_mask.png ' . $shirtFabImgPath . $fabric_id . '_fabric_main_back.png -alpha Set ' . $homepath . 'women_shirt_images/collarstyle/back_collar/' . $fabric_id . '_style_' . $style_id . '_casual_front_view_1.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/back_collar/' . $fabric_id . '_style_' . $style_id . '_casual_front_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/collarstyle/back_collar/' . $fabric_id . '_style_' . $style_id . '_casual_front_view_1.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//glow image
                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/back_collar/' . $fabric_id . '_style_' . $style_id . '_casual_front_view_1.png  ' . $homepath . 'women_glow_mask_shirt/Front_View/Collar_Casual/back_part/BackCollar_shad.png -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/collarstyle/back_collar/' . $fabric_id . '_style_' . $style_id . '_casual_front_view_1.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//highlighted
                        $cmd = ' composite ' . $homepath . 'women_glow_mask_shirt/Front_View/Collar_Casual/back_part/BackCollar_hi.png -compose Overlay  ' . $homepath . 'women_shirt_images/collarstyle/back_collar/' . $fabric_id . '_style_' . $style_id . '_casual_front_view_1.png ' . $homepath . 'women_shirt_images/collarstyle/back_collar/' . $fabric_id . '_style_' . $style_id . '_casual_front_view_1.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);


                        $cmd = ' composite -compose Dst_In -gravity center ' . $homepath . 'women_glow_mask_shirt/Front_View/Collar_Casual/back_part/BackPart_mask.png ' . $shirtFabImgPath . $fabric_id . '_fabric_main.png -alpha Set ' . $homepath . 'women_shirt_images/collarstyle/back_collar/' . $fabric_id . '_style_' . $style_id . '_casual_left_sidepart_view_1.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/back_collar/' . $fabric_id . '_style_' . $style_id . '_casual_left_sidepart_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/collarstyle/back_collar/' . $fabric_id . '_style_' . $style_id . '_casual_left_sidepart_view_1.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//glow image
                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/back_collar/' . $fabric_id . '_style_' . $style_id . '_casual_left_sidepart_view_1.png  ' . $homepath . 'women_glow_mask_shirt/Front_View/Collar_Casual/back_part/BackPart_shad.png ' . ' -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/collarstyle/back_collar/' . $fabric_id . '_style_' . $style_id . '_casual_left_sidepart_view_1.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//highlighted
                        $cmd = ' composite ' . $homepath . 'women_glow_mask_shirt/Front_View/Collar_Casual/back_part/BackPart_hi.png ' . ' -compose Overlay  ' . $homepath . 'women_shirt_images/collarstyle/back_collar/' . $fabric_id . '_style_' . $style_id . '_casual_left_sidepart_view_1.png ' . $homepath . 'women_shirt_images/collarstyle/back_collar/' . $fabric_id . '_style_' . $style_id . '_casual_left_sidepart_view_1.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);


                        $view1_glow_left_image = $homepath . '' . $style['view1_glow_left_image'];
                        $view1_mask_left_image = $homepath . '' . $style['view1_mask_left_image'];
                        $view1_highlighted_left_image = $homepath . '' . $style['view1_highlighted_left_image'];

                        $view1_glow_right_image = $homepath . '' . $style['view1_glow_right_image'];
                        $view1_mask_right_image = $homepath . '' . $style['view1_mask_right_image'];
                        $view1_highlighted_right_image = $homepath . '' . $style['view1_highlighted_right_image'];

                        $view2_glow_left_image = $homepath . '' . $style['view2_glow_left_image'];
                        $view2_mask_left_image = $homepath . '' . $style['view2_mask_left_image'];
                        $view2_highlighted_left_image = $homepath . '' . $style['view2_highlighted_left_image'];

                        $view2_glow_right_image = $homepath . '' . $style['view2_glow_right_image'];
                        $view2_mask_right_image = $homepath . '' . $style['view2_mask_right_image'];
                        $view2_highlighted_right_image = $homepath . '' . $style['view2_highlighted_right_image'];

                        $view4_glow_left_image = $homepath . '' . $style['view4_glow_left_image'];
                        $view4_mask_left_image = $homepath . '' . $style['view4_mask_left_image'];
                        $view4_highlighted_left_image = $homepath . '' . $style['view4_highlighted_left_image'];

                        $view4_glow_right_image = $homepath . '' . $style['view4_glow_right_image'];
                        $view4_mask_right_image = $homepath . '' . $style['view4_mask_right_image'];
                        $view4_highlighted_right_image = $homepath . '' . $style['view4_highlighted_right_image'];

                        $view4_glow_inner_image = $homepath . '' . $style['view4_glow_inner_image'];
                        $view4_mask_inner_image = $homepath . '' . $style['view4_mask_inner_image'];
                        $view4_highlighted_inner_image = $homepath . '' . $style['view4_highlighted_inner_image'];

                        $view5_glow_left_image = $homepath . '' . $style['view5_glow_left_image'];
                        $view5_mask_left_image = $homepath . '' . $style['view5_mask_left_image'];
                        $view5_highlighted_left_image = $homepath . '' . $style['view5_highlighted_left_image'];

                        $view5_glow_right_image = $homepath . '' . $style['view5_glow_right_image'];
                        $view5_mask_right_image = $homepath . '' . $style['view5_mask_right_image'];
                        $view5_highlighted_right_image = $homepath . '' . $style['view5_highlighted_right_image'];

                        $view5_glow_inner_image = $homepath . '' . $style['view5_glow_inner_image'];
                        $view5_mask_inner_image = $homepath . '' . $style['view5_mask_inner_image'];
                        $view5_highlighted_inner_image = $homepath . '' . $style['view5_highlighted_inner_image'];

//step-1: left
//roate fabric image
                        $cmd = ' convert ' . $fabric_0 . ' -background "rgba(0,0,0,0.5)" -distort SRT 45 ' . $shirtFabImgPath . $fabric_id . '_fabric_main_left.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

//mask
                        $cmd = ' composite -compose Dst_In -gravity center ' . $view1_mask_left_image . ' ' . $shirtFabImgPath . $fabric_id . '_fabric_main_left.png -alpha Set ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_left_view_1.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_left_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_left_view_1.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

//glow image
                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_left_view_1.png  ' . $view1_glow_left_image . ' -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_left_view_1.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//highlighted
                        $cmd = ' composite ' . $view1_highlighted_left_image . ' -compose Overlay  ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_left_view_1.png ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_left_view_1.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

//step-2: right
//roate fabric image
                        $cmd = ' convert ' . $fabric_0 . ' -background "rgba(0,0,0,0.5)" -distort SRT -45 ' . $shirtFabImgPath . $fabric_id . '_fabric_main_right.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//mask
                        $cmd = ' composite -compose Dst_In -gravity center ' . $view1_mask_right_image . ' ' . $shirtFabImgPath . $fabric_id . '_fabric_main_right.png -alpha Set ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_right_view_1.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_right_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_right_view_1.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//glow image
                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_right_view_1.png  ' . $view1_glow_right_image . ' -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_right_view_1.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//highlighted
                        $cmd = ' composite ' . $view1_highlighted_right_image . ' -compose Overlay  ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_right_view_1.png ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_right_view_1.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//Combine
                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_right_view_1.png ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_left_view_1.png -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_view_1.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

                        $cmd = "rm " . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_right_view_1.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

                        $cmd = "rm " . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_left_view_1.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);




                        $cmd = ' composite -compose Dst_In -gravity center ' . $homepath . 'women_glow_mask_shirt/Front_View/Collar_Styles/BackCollar_mask.png ' . $shirtFabImgPath . $fabric_id . '_fabric_main_back.png -alpha Set ' . $homepath . 'women_shirt_images/collarstyle/back_collar/' . $fabric_id . '_style_' . $style_id . '_front_view_1.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/back_collar/' . $fabric_id . '_style_' . $style_id . '_front_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/collarstyle/back_collar/' . $fabric_id . '_style_' . $style_id . '_front_view_1.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//glow image
                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/back_collar/' . $fabric_id . '_style_' . $style_id . '_front_view_1.png  ' . $homepath . 'women_glow_mask_shirt/Front_View/Collar_Styles/BackCollar_shad.png -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/collarstyle/back_collar/' . $fabric_id . '_style_' . $style_id . '_front_view_1.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//highlighted
                        $cmd = ' composite ' . $homepath . 'women_glow_mask_shirt/Front_View/Collar_Styles/BackCollar_hi.png -compose Overlay  ' . $homepath . 'women_shirt_images/collarstyle/back_collar/' . $fabric_id . '_style_' . $style_id . '_front_view_1.png ' . $homepath . 'women_shirt_images/collarstyle/back_collar/' . $fabric_id . '_style_' . $style_id . '_front_view_1.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);



//step 3: Front
//mask  image
                        $cmd = ' composite -compose Dst_In -gravity center ' . $homepath . 'women_glow_mask_shirt/Front_View/Collar_Styles/Commonpart_Front_mask.png ' . $shirtFabImgPath . $fabric_id . '_fabric_main_back.png -alpha Set ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_front_view_1.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_front_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_front_view_1.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//glow image
                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_front_view_1.png  ' . $homepath . 'women_glow_mask_shirt/Front_View/Collar_Styles/Commonpart_Front_shad.png -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_front_view_1.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//highlighted
                        $cmd = ' composite ' . $homepath . 'women_glow_mask_shirt/Front_View/Collar_Styles/Commonpart_Front_hi.png -compose Overlay  ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_front_view_1.png ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_front_view_1.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

//step 5: back
//fabric image
//mask image
                        $cmd = ' composite -compose Dst_In -gravity center ' . $homepath . 'women_glow_mask_shirt/Front_View/Collar_Styles/Shirt_Commoncollar_Back_Mask.png  ' . $shirtFabImgPath . $fabric_id . '_fabric_main_back_wave.png ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_back_view_1.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_back_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_back_view_1.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//glow image
                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_back_view_1.png ' . $homepath . 'women_glow_mask_shirt/Front_View/Collar_Styles/Shirt_Commoncollar_Back_Shad.png -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_back_view_1.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//highlighted
                        $cmd = ' composite ' . $homepath . 'women_glow_mask_shirt/Front_View/Collar_Styles/Shirt_Commoncollar_Back_Hi.png -compose Overlay  ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_back_view_1.png ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_back_view_1.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

//step 4: compose all images
                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_front_view_1.png  ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_right_view_1.png -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_view_1.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_view_1.png  ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_left_view_1.png -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_view_1.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
                        /* view 1 end */

                        /* view 2 start */
//step-1: left
//roate fabric image
                        $cmd = ' convert ' . $fabric_0 . ' -background "rgba(0,0,0,0.5)" -distort SRT 45 ' . $shirtFabImgPath . $fabric_id . '_fabric_main_left.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//mask
                        $cmd = ' composite -compose Dst_In -gravity center ' . $view2_mask_left_image . ' ' . $shirtFabImgPath . $fabric_id . '_fabric_main_left.png -alpha Set ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_left_view_2.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_left_view_2.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_left_view_2.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//glow image
                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_left_view_2.png  ' . $view2_glow_left_image . ' -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_left_view_2.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//highlighted
                        $cmd = ' composite ' . $view2_highlighted_left_image . ' -compose Overlay  ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_left_view_2.png ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_left_view_2.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//step-2: right
//roate fabric image
                        $cmd = ' convert ' . $fabric_0 . ' -background "rgba(0,0,0,0.5)" -distort SRT -45 ' . $shirtFabImgPath . $fabric_id . '_fabric_main_right.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//mask
                        $cmd = ' composite -compose Dst_In -gravity center ' . $view2_mask_right_image . ' ' . $shirtFabImgPath . $fabric_id . '_fabric_main_right.png -alpha Set ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_right_view_2.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_right_view_2.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_right_view_2.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//glow image
                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_right_view_2.png  ' . $view2_glow_right_image . ' -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_right_view_2.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//highlighted
                        $cmd = ' composite ' . $view2_highlighted_right_image . ' -compose Overlay  ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_right_view_2.png ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_right_view_2.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//Combine
                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_right_view_2.png ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_left_view_2.png -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_view_2.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

                        $cmd = "rm " . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_right_view_2.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

                        $cmd = "rm " . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_left_view_2.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//step 3: Front
//mask  image
                        $cmd = ' composite -compose Dst_In -gravity center ' . $homepath . 'women_glow_mask_shirt/Side_View/Collar_Styles/Commonpart_Side_mask.png ' . $shirtFabImgPath . $fabric_id . '_fabric_main_back.png -alpha Set ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_front_view_2.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_front_view_2.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_front_view_2.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//glow image
                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_front_view_2.png  ' . $homepath . 'women_glow_mask_shirt/Side_View/Collar_Styles/Commonpart_Side_shad.png -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_front_view_2.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//highlighted
                        $cmd = ' composite ' . $homepath . 'women_glow_mask_shirt/Side_View/Collar_Styles/Commonpart_Side_hi.png -compose Overlay  ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_front_view_2.png ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_front_view_2.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//step 5: back
//fabric image
                        $cmd = ' convert ' . $shirtFabImgPath . $fabric_id . '_fabric_main_back.png -wave 10x300 -gravity South -chop 0x10 ' . $shirtFabImgPath . $fabric_id . '_fabric_main_back_wave.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//mask image
                        $cmd = ' composite -compose Dst_In -gravity center ' . $homepath . 'women_glow_mask_shirt/Back_View/Collar_Styles/Shirt_Commoncollar_Back_Mask.png  ' . $shirtFabImgPath . $fabric_id . '_fabric_main_back_wave.png -alpha Set ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_back_view_2.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_back_view_2.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_back_view_2.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//glow image
                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_back_view_2.png ' . $homepath . 'women_glow_mask_shirt/Back_View/Collar_Styles/Shirt_Commoncollar_Back_Shad.png -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_back_view_2.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//highlighted
                        $cmd = ' composite ' . $homepath . 'women_glow_mask_shirt/Back_View/Collar_Styles/Shirt_Commoncollar_Back_Hi.png -compose Overlay  ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_back_view_2.png ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_back_view_2.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//step 4: compose all images
                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_front_view_2.png  ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_right_view_2.png -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_view_2.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_view_2.png  ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_left_view_2.png -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_view_2.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);


                        /* view 2 end */

                        /* view 3 start */
//step 5: back
//fabric image
//mask image
                        $cmd = ' composite -compose Dst_In -gravity center ' . $homepath . 'women_glow_mask_shirt/Back_View/Collar_Styles/Shirt_Commoncollar_Back_Mask.png  ' . $shirtFabImgPath . $fabric_id . '_fabric_main_back.png -alpha Set ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_view_3.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_view_3.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_view_3.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//glow image
                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_view_3.png ' . $homepath . 'women_glow_mask_shirt/Back_View/Collar_Styles/Shirt_Commoncollar_Back_Shad.png -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_view_3.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//highlighted
                        $cmd = ' composite ' . $homepath . 'women_glow_mask_shirt/Back_View/Collar_Styles/Shirt_Commoncollar_Back_Hi.png -compose Overlay  ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_view_3.png ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_view_3.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

                        /* view 3 end */

                        /* view 4 start */

//step-1: left
//roate fabric image
                        $cmd = ' convert ' . $fabric_0 . ' -background "rgba(0,0,0,0.5)" -distort SRT 45 ' . $shirtFabImgPath . $fabric_id . '_fabric_main_left.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//mask
                        $cmd = ' composite -compose Dst_In -gravity center ' . $view4_mask_left_image . ' ' . $shirtFabImgPath . $fabric_id . '_fabric_main_left.png -alpha Set ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_left_view_4.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_left_view_4.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_left_view_4.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//glow image
                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_left_view_4.png  ' . $view4_glow_left_image . ' -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_left_view_4.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//highlighted
                        $cmd = ' composite ' . $view4_highlighted_left_image . ' -compose Overlay  ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_left_view_4.png ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_left_view_4.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//step-2: right
//roate fabric image
                        $cmd = ' convert ' . $fabric_0 . ' -background "rgba(0,0,0,0.5)" -distort SRT -45 ' . $shirtFabImgPath . $fabric_id . '_fabric_main_right.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//mask
                        $cmd = ' composite -compose Dst_In -gravity center ' . $view4_mask_right_image . ' ' . $shirtFabImgPath . $fabric_id . '_fabric_main_right.png -alpha Set ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_right_view_4.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_right_view_4.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_right_view_4.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//glow image
                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_right_view_4.png  ' . $view4_glow_right_image . ' -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_right_view_4.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//highlighted
                        $cmd = ' composite ' . $view4_highlighted_right_image . ' -compose Overlay  ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_right_view_4.png ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_right_view_4.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//Combine
                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_right_view_4.png ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_left_view_4.png -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_view_4.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

                        $cmd = "rm " . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_left_view_4.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

                        $cmd = "rm " . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_right_view_4.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

//step 3: common back
//mask  image
                        $cmd = ' composite -compose Dst_In -gravity center ' . $homepath . 'women_glow_mask_shirt/Folded_View/Collar_Styles/Commonbackpart_Folded_mask.png ' . $shirtFabImgPath . $fabric_id . '_fabric_main_back.png -alpha Set ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_back_view_4.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_back_view_4.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_back_view_4.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//glow image
                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_back_view_4.png  ' . $homepath . 'women_glow_mask_shirt/Folded_View/Collar_Styles/Commonbackpart_Folded_shad.png -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_back_view_4.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//highlighted
                        $cmd = ' composite ' . $homepath . 'women_glow_mask_shirt/Folded_View/Collar_Styles/Commonbackpart_Folded_hi.png -compose Overlay  ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_back_view_4.png ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_back_view_4.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//inner mask
                        $cmd = ' composite -compose Dst_In -gravity center ' . $homepath . 'women_glow_mask_shirt/Collars/Folded/inner-fabric_mask.png ' . $shirtFabImgPath . 'fabric_' . $fabric_id . '_main.png -alpha Set ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_inner_view_4.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_inner_view_4.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_inner_view_4.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//inner glow image
                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_inner_view_4.png  ' . $homepath . 'women_glow_mask_shirt/Collars/Folded/inner-fabric_shad.png -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_inner_view_4.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//highlighted
                        $cmd = ' composite ' . $homepath . 'women_glow_mask_shirt/Collars/Folded/inner-fabric_hi.png -compose Overlay  ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_inner_view_4.png ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_inner_view_4.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//step 4: compose all images
                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_back_view_4.png  ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_right_view_4.png -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_view_4.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_view_4.png  ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_left_view_4.png -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_view_4.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
                        /* view 4 end */
//close collar end
//open/casual collar start

                        $view1_casual_glow_left_image = $homepath . '' . $style['view1_casual_glow_left_image'];
                        $view1_casual_mask_left_image = $homepath . '' . $style['view1_casual_mask_left_image'];
                        $view1_casual_highlighted_left_image = $homepath . '' . $style['view1_casual_highlighted_left_image'];

                        $view1_casual_glow_right_image = $homepath . '' . $style['view1_casual_glow_right_image'];
                        $view1_casual_mask_right_image = $homepath . '' . $style['view1_casual_mask_right_image'];
                        $view1_casual_highlighted_right_image = $homepath . '' . $style['view1_casual_highlighted_right_image'];

                        $view2_casual_glow_left_image = $homepath . '' . $style['view2_casual_glow_left_image'];
                        $view2_casual_mask_left_image = $homepath . '' . $style['view2_casual_mask_left_image'];
                        $view2_casual_highlighted_left_image = $homepath . '' . $style['view2_casual_highlighted_left_image'];

                        $view2_casual_glow_right_image = $homepath . '' . $style['view2_casual_glow_right_image'];
                        $view2_casual_mask_right_image = $homepath . '' . $style['view2_casual_mask_right_image'];
                        $view2_casual_highlighted_right_image = $homepath . '' . $style['view2_casual_highlighted_right_image'];


                        $commonbase_front_glow_left_image = $homepath . 'women_glow_mask_shirt/Front_View/Collar_Casual/CommonBase_Left_shad.png';
                        $commonbase_front_mask_left_image = $homepath . 'women_glow_mask_shirt/Front_View/Collar_Casual/CommonBase_Left_mask.png';
                        $commonbase_front_hi_left_image = $homepath . 'women_glow_mask_shirt/Front_View/Collar_Casual/CommonBase_Left_hi.png';
                        $commonbase_front_glow_right_image = $homepath . 'women_glow_mask_shirt/Front_View/Collar_Casual/CommonBase_Right_shad.png';
                        $commonbase_front_mask_right_image = $homepath . 'women_glow_mask_shirt/Front_View/Collar_Casual/CommonBase_Right_mask.png';
                        $commonbase_front_hi_right_image = $homepath . 'women_glow_mask_shirt/Front_View/Collar_Casual/CommonBase_Right_hi.png';

                        $commonbase_side_glow_left_image = $homepath . 'women_glow_mask_shirt/Side_View/Collar_Casual/CommonBase_Left_shad.png';
                        $commonbase_side_mask_left_image = $homepath . 'women_glow_mask_shirt/Side_View/Collar_Casual/CommonBase_Left_mask.png';
                        $commonbase_side_hi_left_image = $homepath . 'women_glow_mask_shirt/Side_View/Collar_Casual/CommonBase_Left_hi.png';
                        $commonbase_side_glow_right_image = $homepath . 'women_glow_mask_shirt/Side_View/Collar_Casual/CommonBase_Right_shad.png';
                        $commonbase_side_mask_right_image = $homepath . 'women_glow_mask_shirt/Side_View/Collar_Casual/CommonBase_Right_mask.png';
                        $commonbase_side_hi_right_image = $homepath . 'women_glow_mask_shirt/Side_View/Collar_Casual/CommonBase_Right_hi.png';


                        $inner_contrast_front_glow_left_image = $homepath . 'women_glow_mask_shirt/Front_View/Collar_Casual/Inner_Collar_Contrast_Left_shad.png';
                        $inner_contrast_front_mask_left_image = $homepath . 'women_glow_mask_shirt/Front_View/Collar_Casual/Inner_Collar_Contrast_Left_mask.png';
                        $inner_contrast_front_hi_left_image = $homepath . 'women_glow_mask_shirt/Front_View/Collar_Casual/Inner_Collar_Contrast_Left_hi.png';
                        $inner_contrast_front_glow_right_image = $homepath . 'women_glow_mask_shirt/Front_View/Collar_Casual/Inner_Collar_Contrast_Right_shad.png';
                        $inner_contrast_front_mask_right_image = $homepath . 'women_glow_mask_shirt/Front_View/Collar_Casual/Inner_Collar_Contrast_Right_mask.png';
                        $inner_contrast_front_hi_right_image = $homepath . 'women_glow_mask_shirt/Front_View/Collar_Casual/Inner_Collar_Contrast_Right_hi.png';
                        $cmd = ' composite -compose Dst_In -gravity center ' . $inner_contrast_front_mask_left_image . ' ' . $shirtFabImgPath . $fabric_id . '_fabric_main_left.png -alpha Set ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_inner_left_view_1.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_inner_left_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_inner_left_view_1.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//glow image
                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_inner_left_view_1.png  ' . $inner_contrast_front_glow_left_image . ' -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_inner_left_view_1.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//highlighted
                        $cmd = ' composite ' . $inner_contrast_front_hi_left_image . ' -compose Overlay  ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_inner_left_view_1.png ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_inner_left_view_1.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

                        $cmd = ' composite -compose Dst_In -gravity center ' . $inner_contrast_front_mask_right_image . ' ' . $shirtFabImgPath . $fabric_id . '_fabric_main_right.png -alpha Set ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_inner_right_view_1.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_inner_right_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_inner_right_view_1.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//glow image
                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_inner_right_view_1.png  ' . $inner_contrast_front_glow_right_image . ' -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_inner_right_view_1.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//highlighted
                        $cmd = ' composite ' . $inner_contrast_front_hi_right_image . ' -compose Overlay  ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_inner_right_view_1.png ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_inner_right_view_1.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//combine
                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_inner_left_view_1.png ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_inner_right_view_1.png -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_inner_view_1.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

                        $cmd = "rm " . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_inner_left_view_1.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

                        $cmd = "rm " . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_inner_right_view_1.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
// collar pending images
                        $inner_contrast_side_glow_image = $homepath . 'women_glow_mask_shirt/Side_View/Collar_Casual/Inner_Collar_Contrast_Left_shad.png';
                        $inner_contrast_side_mask_image = $homepath . 'women_glow_mask_shirt/Side_View/Collar_Casual/Inner_Collar_Contrast_Left_mask.png';
                        $inner_contrast_side_hi_image = $homepath . 'women_glow_mask_shirt/Side_View/Collar_Casual/Inner_Collar_Contrast_Left_hi.png';

                        $cmd = ' composite -compose Dst_In -gravity center ' . $inner_contrast_side_mask_image . ' ' . $shirtFabImgPath . $fabric_id . '_fabric_main_left.png -alpha Set ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_inner_view_2.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_inner_view_2.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_inner_view_2.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

//glow image
                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_inner_view_2.png  ' . $inner_contrast_side_glow_image . ' -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_inner_view_2.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//highlighted
                        $cmd = ' composite ' . $inner_contrast_side_hi_image . ' -compose Overlay  ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_inner_view_2.png ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_inner_view_2.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);




                        /* view 1 start */

//step-1: left
//mask
                        $cmd = ' composite -compose Dst_In -gravity center ' . $view1_casual_mask_left_image . ' ' . $shirtFabImgPath . $fabric_id . '_fabric_main_left.png -alpha Set ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_left_view_1.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_left_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_left_view_1.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//glow image
                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_left_view_1.png  ' . $view1_casual_glow_left_image . ' -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_left_view_1.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//highlighted
                        $cmd = ' composite ' . $view1_casual_highlighted_left_image . ' -compose Overlay  ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_left_view_1.png ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_left_view_1.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//step-2: right
//mask

                        $cmd = ' composite -compose Dst_In -gravity center ' . $view1_casual_mask_right_image . ' ' . $shirtFabImgPath . $fabric_id . '_fabric_main_right.png -alpha Set ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_right_view_1.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_right_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_right_view_1.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//glow image
                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_right_view_1.png  ' . $view1_casual_glow_right_image . ' -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_right_view_1.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//highlighted
                        $cmd = ' composite ' . $view1_casual_highlighted_right_image . ' -compose Overlay  ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_right_view_1.png ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_right_view_1.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//step-1: common left
//mask
                        $cmd = ' composite -compose Dst_In -gravity center ' . $commonbase_front_mask_left_image . ' ' . $shirtFabImgPath . $fabric_id . '_fabric_main_left.png -alpha Set ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_common_left_view_1.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_common_left_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_common_left_view_1.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//glow image
                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_common_left_view_1.png  ' . $commonbase_front_glow_left_image . ' -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_common_left_view_1.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//highlighted
                        $cmd = ' composite ' . $commonbase_front_hi_left_image . ' -compose Overlay  ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_common_left_view_1.png ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_common_left_view_1.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//step-2: common right
//mask
                        $cmd = ' composite -compose Dst_In -gravity center ' . $commonbase_front_mask_right_image . ' ' . $shirtFabImgPath . $fabric_id . '_fabric_main_right.png -alpha Set ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_common_right_view_1.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_common_right_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_common_right_view_1.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//glow image
                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_common_right_view_1.png  ' . $commonbase_front_glow_right_image . ' -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_common_right_view_1.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//highlighted
                        $cmd = ' composite ' . $commonbase_front_hi_right_image . ' -compose Overlay  ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_common_right_view_1.png ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_common_right_view_1.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//step-2: common torso
//step-2: compose all images
                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_left_view_1.png ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_right_view_1.png -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_collar_view_1.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_common_right_view_1.png ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_collar_view_1.png  -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_collar_view_1.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_common_left_view_1.png ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_collar_view_1.png -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_collar_view_1.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

                        $cmd = "rm " . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_common_left_view_1.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

                        $cmd = "rm " . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_common_right_view_1.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

                        $cmd = "rm " . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_left_view_1.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
                        /* view 1 end */

                        /* view 2 start */


//step-1: left
//mask
                        $cmd = ' composite -compose Dst_In -gravity center ' . $view2_casual_mask_left_image . ' ' . $shirtFabImgPath . $fabric_id . '_fabric_main_back.png -alpha Set ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_left_view_2.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_left_view_2.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_left_view_2.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//glow image
                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_left_view_2.png  ' . $view2_casual_glow_left_image . ' -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_left_view_2.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//highlighted
                        $cmd = ' composite ' . $view2_casual_highlighted_left_image . ' -compose Overlay  ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_left_view_2.png ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_left_view_2.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

//step-2: right
//mask
                        $cmd = ' composite -compose Dst_In -gravity center ' . $view2_casual_mask_right_image . ' ' . $shirtFabImgPath . $fabric_id . '_fabric_main_back.png -alpha Set ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_right_view_2.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_right_view_2.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_right_view_2.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//glow image
                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_right_view_2.png  ' . $view2_casual_glow_right_image . ' -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_right_view_2.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//highlighted
                        $cmd = ' composite ' . $view2_casual_highlighted_right_image . ' -compose Overlay  ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_right_view_2.png ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_right_view_2.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//step-1: common left
//mask
                        $cmd = ' composite -compose Dst_In -gravity center ' . $commonbase_side_mask_left_image . ' ' . $shirtFabImgPath . $fabric_id . '_fabric_main_left.png -alpha Set ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_common_left_view_2.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_common_left_view_2.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_common_left_view_2.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//glow image
                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_common_left_view_2.png  ' . $commonbase_side_glow_left_image . ' -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_common_left_view_2.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//highlighted
                        $cmd = ' composite ' . $commonbase_side_hi_left_image . ' -compose Overlay  ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_common_left_view_2.png ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_common_left_view_2.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//step-2: common right
//mask
                        $cmd = ' composite -compose Dst_In -gravity center ' . $commonbase_side_mask_right_image . ' ' . $shirtFabImgPath . $fabric_id . '_fabric_main_right.png -alpha Set ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_common_right_view_2.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_common_right_view_2.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_common_right_view_2.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//glow image
                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_common_right_view_2.png  ' . $commonbase_side_glow_right_image . ' -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_common_right_view_2.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//highlighted
                        $cmd = ' composite ' . $commonbase_side_hi_right_image . ' -compose Overlay  ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_common_right_view_2.png ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_common_right_view_2.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//step-2: common torso
//mask
                        $cmd = ' composite -compose Dst_In -gravity center ' . $homepath . 'women_glow_mask_shirt/Side_View/Collar_Casual/Torso_Bust/CasualBust_mask.png ' . $fabric_side_view . ' -alpha Set ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_casual_common_torso_view_2.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_casual_common_torso_view_2.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_casual_common_torso_view_2.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//glow image
                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_casual_common_torso_view_2.png  ' . $homepath . 'women_glow_mask_shirt/Side_View/Collar_Casual/Torso_Bust/CasualBust_shad.png -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_casual_common_torso_view_2.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//highlighted
                        $cmd = ' composite ' . $homepath . 'women_glow_mask_shirt/Side_View/Collar_Casual/Torso_Bust/CasualBust_hi.png -compose Overlay  ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_casual_common_torso_view_2.png ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_casual_common_torso_view_2.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

// //step-2: common placket
//view 5 start
//roate fabric image
                        $cmd = ' convert ' . $fabric_zoom . ' -background "rgba(0,0,0,0.5)" -distort SRT -45 ' . $shirtFabImgPath . $fabric_id . '_fabric_main_left_zoom.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

//mask
                        $cmd = ' composite -compose CopyOpacity ' . $view5_mask_left_image . ' ' . $shirtFabImgPath . $fabric_id . '_fabric_main_left_zoom.png ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_left_view_5.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//glow image
                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_left_view_5.png  ' . $view5_glow_left_image . ' -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_left_view_5.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//highlighted
                        $cmd = ' composite ' . $view5_highlighted_left_image . ' -compose Overlay  ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_left_view_5.png ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_left_view_5.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

//step-2: right
//roate fabric image
                        $cmd = ' convert ' . $fabric_zoom . ' -background "rgba(0,0,0,0.5)" -distort SRT 45 ' . $shirtFabImgPath . $fabric_id . '_fabric_main_right_zoom.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//mask
                        $cmd = ' composite -compose CopyOpacity ' . $view5_mask_right_image . ' ' . $shirtFabImgPath . $fabric_id . '_fabric_main_right_zoom.png ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_right_view_5.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//glow image
                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_right_view_5.png  ' . $view5_glow_right_image . ' -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_right_view_5.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//highlighted
                        $cmd = ' composite ' . $view5_highlighted_right_image . ' -compose Overlay  ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_right_view_5.png ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_right_view_5.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//mask image
                        $cmd = ' composite -compose CopyOpacity ' . $homepath . 'women_glow_mask_shirt/Zoom_View/Collar_Styles/CollarBase_Back_Mask.png  ' . $shirtFabImgPath . $fabric_id . '_fabric_main_back_zoom.png ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_back_view_5.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//glow image
                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_back_view_5.png ' . $homepath . 'women_glow_mask_shirt/Zoom_View/Collar_Styles/CollarBase_Back_Shad.png -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_back_view_5.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//highlighted
                        $cmd = ' composite ' . $homepath . 'women_glow_mask_shirt/Zoom_View/Collar_Styles/CollarBase_Back_Hi.png -compose Overlay  ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_back_view_5.png ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_back_view_5.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

                        $cmd = ' convert ' . $fabric_zoom . ' -background "rgba(0,0,0,0.5)" -distort SRT 80 ' . $shirtFabImgPath . $fabric_id . '_fabric_main_left_basecollar.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

                        $cmd = ' convert ' . $fabric_zoom . ' -background "rgba(0,0,0,0.5)" -distort SRT -80 ' . $shirtFabImgPath . $fabric_id . '_fabric_main_right_basecollar.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//left
                        $cmd = ' composite -compose CopyOpacity ' . $homepath . 'women_glow_mask_shirt/Zoom_View/Collar_Styles/CollarBase_Left_Mask.png  ' . $shirtFabImgPath . $fabric_id . '_fabric_main_left_basecollar.png ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_commoninnerLeft_view_5.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//glow image
                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_commoninnerLeft_view_5.png ' . $homepath . 'women_glow_mask_shirt/Zoom_View/Collar_Styles/CollarBase_Left_Shad.png -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_commoninnerLeft_view_5.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//highlighted
                        $cmd = ' composite ' . $homepath . 'women_glow_mask_shirt/Zoom_View/Collar_Styles/CollarBase_Left_Hi.png -compose Overlay  ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_commoninnerLeft_view_5.png ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_commoninnerLeft_view_5.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//right
                        $cmd = ' composite -compose CopyOpacity ' . $homepath . 'women_glow_mask_shirt/Zoom_View/Collar_Styles/CollarBase_Right_Mask.png  ' . $shirtFabImgPath . $fabric_id . '_fabric_main_right_basecollar.png ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_commoninnerRight_view_5.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//glow image
                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_commoninnerRight_view_5.png ' . $homepath . 'women_glow_mask_shirt/Zoom_View/Collar_Styles/CollarBase_Right_Shad.png -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_commoninnerRight_view_5.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//highlighted
                        $cmd = ' composite ' . $homepath . 'women_glow_mask_shirt/Zoom_View/Collar_Styles/CollarBase_Right_Hi.png -compose Overlay  ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_commoninnerRight_view_5.png ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_commoninnerRight_view_5.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//innner combine
                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_commoninnerLeft_view_5.png ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_commoninnerRight_view_5.png -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_commoninner_view_5.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

                        $cmd = "rm " . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_commoninnerRight_view_5.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

                        $cmd = "rm " . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_commoninnerLeft_view_5.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

//inner collar end
//step 4: compose all images
                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_left_view_5.png  ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_right_view_5.png -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_view_5.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_commoninner_view_5.png ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_view_5.png -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_view_5.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

                        $cmd = "rm " . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_right_view_5.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

                        $cmd = "rm " . $homepath . 'women_shirt_images/collarstyle/' . $fabric_id . '_style_' . $style_id . '_left_view_5.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
                        /* view 5 end */

//step-2: compose all images

                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_left_view_2.png ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_right_view_2.png -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_collar_view_2.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

                        $cmd = "rm " . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_left_view_2.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

                        $cmd = "rm " . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_right_view_2.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_common_right_view_2.png ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_collar_view_2.png -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_collar_view_2.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_common_left_view_2.png ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_collar_view_2.png  -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_collar_view_2.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

                        $cmd = "rm " . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_common_left_view_2.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

                        $cmd = "rm " . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_common_right_view_2.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

                        $cmd = ' convert ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_casual_common_torso_view_2.png ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_' . $style_id . '_casual_common_placket_view_2.png -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/collarstyle/casual/' . $fabric_id . '_style_casual_common_torso_view_2.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
                    }


                    /* Elbow patches images */
                    $womenShirtAccentElbowpatches = $this->_womenShirtAccentElbowpatches->create()->getCollection();
                    $elbowpatches_collection = $womenShirtAccentElbowpatches->getData();

                    foreach ($elbowpatches_collection as $elbowpatches) {
                        $elbowpatches_id = $elbowpatches['elbowpatches_id'];

                        $elbowpatchesLeft_glow = $homepath . '' . $elbowpatches['glow_back_image'];
                        $elbowpatchesLeft_mask = $homepath . '' . $elbowpatches['mask_back_image'];
                        $elbowpatchesLeft_highlighted = $homepath . '' . $elbowpatches['highlighted_back_image'];

                        if ($elbowpatches['glow_back_image'] != '' && $elbowpatches['mask_back_image'] != '') {
//left
                            $cmd = ' composite -compose Dst_In -gravity center ' . $elbowpatchesLeft_mask . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'women_shirt_images/elbowpatches/' . $fabric_id . '_elbowPatches_view_3.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

                            $cmd = ' convert ' . $homepath . 'women_shirt_images/elbowpatches/' . $fabric_id . '_elbowPatches_view_3.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/elbowpatches/' . $fabric_id . '_elbowPatches_view_3.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

                            $cmd = ' convert ' . $homepath . 'women_shirt_images/elbowpatches/' . $fabric_id . '_elbowPatches_view_3.png ' . $elbowpatchesLeft_glow . ' -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/elbowpatches/' . $fabric_id . '_elbowPatches_view_3.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);
//highlighted
                            $cmd = ' composite ' . $elbowpatchesLeft_highlighted . ' -compose Overlay  ' . $homepath . 'women_shirt_images/elbowpatches/' . $fabric_id . '_elbowPatches_view_3.png ' . $homepath . 'women_shirt_images/elbowpatches/' . $fabric_id . '_elbowPatches_view_3.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);
                        }
                    }



//shirt pockets
                    $womenShirtPocket = $this->_womenShirtPocket->create()->getCollection();
                    $rows_pockets = $womenShirtPocket->getData();

                    foreach ($rows_pockets as $pocket) {

//                        $cmd = ' convert ' . $homepath . 'women_shirt_images/pocket/blank.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/pocket/blank.png';

                        $pocket_id = $pocket['shirtpocket_id'];
                        $pocket_left_glow = $homepath . '' . $pocket['glow_front_left_image'];
                        $pocket_left_mask = $homepath . '' . $pocket['mask_front_left_image'];
                        $pocket_left_highlighted = $homepath . '' . $pocket['highlighted_front_left_image'];
                        $pocket_right_glow = $homepath . '' . $pocket['glow_front_right_image'];
                        $pocket_right_mask = $homepath . '' . $pocket['mask_front_right_image'];
                        $pocket_right_highlighted = $homepath . '' . $pocket['highlighted_front_right_image'];

                        if (($pocket['glow_front_left_image'] != '' && $pocket['mask_front_left_image'] != '') || ($pocket['glow_front_right_image'] != '' && $pocket['mask_front_right_image'] != '')) {
                            $cmd = ' composite -compose Dst_In -gravity center ' . $pocket_left_mask . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'women_shirt_images/pocket/' . $fabric_id . '_pockets_' . $pocket_id . '_left_view_1.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

                            $cmd = ' convert ' . $homepath . 'women_shirt_images/pocket/' . $fabric_id . '_pockets_' . $pocket_id . '_left_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/pocket/' . $fabric_id . '_pockets_' . $pocket_id . '_left_view_1.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

                            $cmd = ' convert ' . $homepath . 'women_shirt_images/pocket/' . $fabric_id . '_pockets_' . $pocket_id . '_left_view_1.png ' . $pocket_left_glow . ' -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/pocket/' . $fabric_id . '_pockets_' . $pocket_id . '_left_view_1.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);
//highlight
                            $cmd = ' composite ' . $pocket_left_highlighted . ' -compose Overlay  ' . $homepath . 'women_shirt_images/pocket/' . $fabric_id . '_pockets_' . $pocket_id . '_left_view_1.png ' . $homepath . 'women_shirt_images/pocket/' . $fabric_id . '_pockets_' . $pocket_id . '_left_view_1.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

                            $cmd = ' composite -compose Dst_In -gravity center ' . $pocket_right_mask . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'women_shirt_images/pocket/' . $fabric_id . '_pockets_' . $pocket_id . '_right_view_1.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

                            $cmd = ' convert ' . $homepath . 'women_shirt_images/pocket/' . $fabric_id . '_pockets_' . $pocket_id . '_right_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/pocket/' . $fabric_id . '_pockets_' . $pocket_id . '_right_view_1.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

                            $cmd = ' convert ' . $homepath . 'women_shirt_images/pocket/' . $fabric_id . '_pockets_' . $pocket_id . '_right_view_1.png ' . $pocket_right_glow . ' -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/pocket/' . $fabric_id . '_pockets_' . $pocket_id . '_right_view_1.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);
//highlight
                            $cmd = ' composite ' . $pocket_right_highlighted . ' -compose Overlay  ' . $homepath . 'women_shirt_images/pocket/' . $fabric_id . '_pockets_' . $pocket_id . '_right_view_1.png ' . $homepath . 'women_shirt_images/pocket/' . $fabric_id . '_pockets_' . $pocket_id . '_right_view_1.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

                            if ($pocket['mask_front_right_image'] != '' && $pocket['mask_front_left_image'] != '') {
//combine
                                $cmd = ' convert ' . $homepath . 'women_shirt_images/pocket/' . $fabric_id . '_pockets_' . $pocket_id . '_right_view_1.png ' . $homepath . 'women_shirt_images/pocket/' . $fabric_id . '_pockets_' . $pocket_id . '_left_view_1.png -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/pocket/' . $fabric_id . '_pockets_' . $pocket_id . '_view_1.png';
                                $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                                $this->connection->query($query);
                            } else if ($pocket['mask_front_right_image'] == '' && $pocket['mask_front_left_image'] != '') {
//combine
                                $cmd = ' convert ' . $homepath . 'women_shirt_images/pocket/static/blank.png ' . $homepath . 'women_shirt_images/pocket/' . $fabric_id . '_pockets_' . $pocket_id . '_left_view_1.png -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/pocket/' . $fabric_id . '_pockets_' . $pocket_id . '_view_1.png';
                                $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                                $this->connection->query($query);
                            } else if ($pocket['mask_front_right_image'] != '' && $pocket['mask_front_left_image'] == '') {
//combine
                                $cmd = ' convert ' . $homepath . 'women_shirt_images/pocket/' . $fabric_id . '_pockets_' . $pocket_id . '_right_view_1.png ' . $homepath . 'women_shirt_images/pocket/static/blank.png -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/pocket/' . $fabric_id . '_pockets_' . $pocket_id . '_view_1.png';
                                $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                                $this->connection->query($query);
                            }
                        }



                        $pocket_left_glow = $homepath . '' . $pocket['glow_side_left_image'];
                        $pocket_left_mask = $homepath . '' . $pocket['mask_side_left_image'];
                        $pocket_left_highlighted = $homepath . '' . $pocket['highlighted_side_left_image'];
                        $pocket_right_glow = $homepath . '' . $pocket['glow_side_right_image'];
                        $pocket_right_mask = $homepath . '' . $pocket['mask_side_right_image'];
                        $pocket_right_highlighted = $homepath . '' . $pocket['highlighted_side_right_image'];

                        if (($pocket['glow_side_left_image'] != '' && $pocket['mask_side_left_image'] != '') || ($pocket['glow_side_right_image'] != '' && $pocket['mask_side_right_image'] != '')) {
                            $cmd = ' composite -compose Dst_In -gravity center ' . $pocket_left_mask . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'women_shirt_images/pocket/' . $fabric_id . '_pockets_' . $pocket_id . '_left_view_2.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

                            $cmd = ' convert ' . $homepath . 'women_shirt_images/pocket/' . $fabric_id . '_pockets_' . $pocket_id . '_left_view_2.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/pocket/' . $fabric_id . '_pockets_' . $pocket_id . '_left_view_2.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

                            $cmd = ' convert ' . $homepath . 'women_shirt_images/pocket/' . $fabric_id . '_pockets_' . $pocket_id . '_left_view_2.png ' . $pocket_left_glow . ' -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/pocket/' . $fabric_id . '_pockets_' . $pocket_id . '_left_view_2.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);
//highlight
                            $cmd = ' composite ' . $pocket_left_highlighted . ' -compose Overlay  ' . $homepath . 'women_shirt_images/pocket/' . $fabric_id . '_pockets_' . $pocket_id . '_left_view_2.png ' . $homepath . 'women_shirt_images/pocket/' . $fabric_id . '_pockets_' . $pocket_id . '_left_view_2.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

                            $cmd = ' composite -compose Dst_In -gravity center ' . $pocket_right_mask . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'women_shirt_images/pocket/' . $fabric_id . '_pockets_' . $pocket_id . '_right_view_2.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

                            $cmd = ' convert ' . $homepath . 'women_shirt_images/pocket/' . $fabric_id . '_pockets_' . $pocket_id . '_right_view_2.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/pocket/' . $fabric_id . '_pockets_' . $pocket_id . '_right_view_2.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

                            $cmd = ' convert ' . $homepath . 'women_shirt_images/pocket/' . $fabric_id . '_pockets_' . $pocket_id . '_right_view_2.png ' . $pocket_right_glow . ' -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/pocket/' . $fabric_id . '_pockets_' . $pocket_id . '_right_view_2.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);
//highlight
                            $cmd = ' composite ' . $pocket_right_highlighted . ' -compose Overlay  ' . $homepath . 'women_shirt_images/pocket/' . $fabric_id . '_pockets_' . $pocket_id . '_right_view_2.png ' . $homepath . 'women_shirt_images/pocket/' . $fabric_id . '_pockets_' . $pocket_id . '_right_view_2.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);
//combine
                            if ($pocket['mask_side_right_image'] != '' && $pocket['mask_side_left_image'] != '') {
                                $cmd = ' convert ' . $homepath . 'women_shirt_images/pocket/' . $fabric_id . '_pockets_' . $pocket_id . '_right_view_2.png ' . $homepath . 'women_shirt_images/pocket/' . $fabric_id . '_pockets_' . $pocket_id . '_left_view_2.png -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/pocket/' . $fabric_id . '_pockets_' . $pocket_id . '_view_2.png';
                                $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                                $this->connection->query($query);
                            } else if ($pocket['mask_side_right_image'] == '' && $pocket['mask_side_left_image'] != '') {
                                $cmd = ' convert ' . $homepath . 'women_shirt_images/pocket/static/blank.png ' . $homepath . 'women_shirt_images/pocket/' . $fabric_id . '_pockets_' . $pocket_id . '_left_view_2.png -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/pocket/' . $fabric_id . '_pockets_' . $pocket_id . '_view_2.png';
                                $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                                $this->connection->query($query);
                            } else if ($pocket['mask_side_right_image'] != '' && $pocket['mask_side_left_image'] == '') {
                                $cmd = ' convert ' . $homepath . 'women_shirt_images/pocket/' . $fabric_id . '_pockets_' . $pocket_id . '_right_view_2.png ' . $homepath . 'women_shirt_images/pocket/static/blank.png -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/pocket/' . $fabric_id . '_pockets_' . $pocket_id . '_view_2.png';
                                $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                                $this->connection->query($query);
                            }
                        }





                        $pocket_left_glow = $homepath . '' . $pocket['glow_fold_left_image'];
                        $pocket_left_mask = $homepath . '' . $pocket['mask_fold_left_image'];
                        $pocket_left_highlighted = $homepath . '' . $pocket['highlighted_fold_left_image'];
                        $pocket_right_glow = $homepath . '' . $pocket['glow_fold_right_image'];
                        $pocket_right_mask = $homepath . '' . $pocket['mask_fold_right_image'];
                        $pocket_right_highlighted = $homepath . '' . $pocket['highlighted_fold_right_image'];

                        if (($pocket['glow_fold_left_image'] != '' && $pocket['mask_fold_left_image'] != '') || ($pocket['glow_fold_right_image'] != '' && $pocket['mask_fold_right_image'] != '')) {
                            $cmd = ' composite -compose Dst_In -gravity center ' . $pocket_left_mask . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'women_shirt_images/pocket/' . $fabric_id . '_pockets_' . $pocket_id . '_left_view_4.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

                            $cmd = ' convert ' . $homepath . 'women_shirt_images/pocket/' . $fabric_id . '_pockets_' . $pocket_id . '_left_view_4.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/pocket/' . $fabric_id . '_pockets_' . $pocket_id . '_left_view_4.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

                            $cmd = ' convert ' . $homepath . 'women_shirt_images/pocket/' . $fabric_id . '_pockets_' . $pocket_id . '_left_view_4.png ' . $pocket_left_glow . ' -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/pocket/' . $fabric_id . '_pockets_' . $pocket_id . '_left_view_4.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);
//highlight
                            $cmd = ' composite ' . $pocket_left_highlighted . ' -compose Overlay  ' . $homepath . 'women_shirt_images/pocket/' . $fabric_id . '_pockets_' . $pocket_id . '_left_view_4.png ' . $homepath . 'women_shirt_images/pocket/' . $fabric_id . '_pockets_' . $pocket_id . '_left_view_4.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

                            $cmd = ' composite -compose Dst_In -gravity center ' . $pocket_right_mask . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'women_shirt_images/pocket/' . $fabric_id . '_pockets_' . $pocket_id . '_right_view_4.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

                            $cmd = ' convert ' . $homepath . 'women_shirt_images/pocket/' . $fabric_id . '_pockets_' . $pocket_id . '_right_view_4.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/pocket/' . $fabric_id . '_pockets_' . $pocket_id . '_right_view_4.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

                            $cmd = ' convert ' . $homepath . 'women_shirt_images/pocket/' . $fabric_id . '_pockets_' . $pocket_id . '_right_view_4.png ' . $pocket_right_glow . ' -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/pocket/' . $fabric_id . '_pockets_' . $pocket_id . '_right_view_4.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);
//highlight
                            $cmd = ' composite ' . $pocket_right_highlighted . ' -compose Overlay  ' . $homepath . 'women_shirt_images/pocket/' . $fabric_id . '_pockets_' . $pocket_id . '_right_view_4.png ' . $homepath . 'women_shirt_images/pocket/' . $fabric_id . '_pockets_' . $pocket_id . '_right_view_4.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);
//combine

                            if ($pocket['mask_fold_right_image'] != '' && $pocket['mask_fold_left_image'] != '') {
                                $cmd = ' convert ' . $homepath . 'women_shirt_images/pocket/' . $fabric_id . '_pockets_' . $pocket_id . '_right_view_4.png ' . $homepath . 'women_shirt_images/pocket/' . $fabric_id . '_pockets_' . $pocket_id . '_left_view_4.png -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/pocket/' . $fabric_id . '_pockets_' . $pocket_id . '_view_4.png';
                                $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                                $this->connection->query($query);
                            } else if ($pocket['mask_fold_right_image'] == '' && $pocket['mask_fold_left_image'] != '') {
                                $cmd = ' convert ' . $homepath . 'women_shirt_images/pocket/static/blank.png ' . $homepath . 'women_shirt_images/pocket/' . $fabric_id . '_pockets_' . $pocket_id . '_left_view_4.png -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/pocket/' . $fabric_id . '_pockets_' . $pocket_id . '_view_4.png';
                                $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                                $this->connection->query($query);
                            } else if ($pocket['mask_fold_right_image'] != '' && $pocket['mask_fold_left_image'] == '') {
                                $cmd = ' convert ' . $homepath . 'women_shirt_images/pocket/' . $fabric_id . '_pockets_' . $pocket_id . '_right_view_4.png ' . $homepath . 'women_shirt_images/pocket/static/blank.png -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/pocket/' . $fabric_id . '_pockets_' . $pocket_id . '_view_4.png';
                                $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                                $this->connection->query($query);
                            }
                        }
                    }





// shirt sleeves start here..

                    $womenShirtSleeves = $this->_womenShirtSleeves->create()->getCollection();
                    $rows_sleeves = $womenShirtSleeves->getData();
                    foreach ($rows_sleeves as $sleeves) {

                        $sleeve_id = $sleeves['shirtsleeves_id'];
                        $style_type = $sleeves['type'];
                        $cuff_size = '';
                        $cuff_type = '';

                        $sleeve_placket_left_glow = $homepath . 'women_glow_mask_shirt/Back_View/Sleeves/SleevePlacket_left_shad.png';
                        $sleeve_placket_left_mask = $homepath . 'women_glow_mask_shirt/Back_View/Sleeves/SleevePlacket_left_mask.png';
                        $sleeve_placket_left_highlighted = $homepath . 'women_glow_mask_shirt/Back_View/Sleeves/SleevePlacket_left_hi.png';
                        $sleeve_placket_right_glow = $homepath . 'women_glow_mask_shirt/Back_View/Sleeves/SleevePlacket_right_shad.png';
                        $sleeve_placket_right_mask = $homepath . 'women_glow_mask_shirt/Back_View/Sleeves/SleevePlacket_right_mask.png';
                        $sleeve_placket_right_highlighted = $homepath . 'women_glow_mask_shirt/Back_View/Sleeves/SleevePlacket_right_hi.png';

                        $cmd = ' composite -compose Dst_In -gravity center ' . $sleeve_placket_left_mask . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'women_shirt_images/sleeveplacket/' . $fabric_id . '_sleeve_' . $sleeve_id . '_left_view_3.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

                        $cmd = ' convert ' . $homepath . 'women_shirt_images/sleeveplacket/' . $fabric_id . '_sleeve_' . $sleeve_id . '_left_view_3.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/sleeveplacket/' . $fabric_id . '_sleeve_' . $sleeve_id . '_left_view_3.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

                        $cmd = ' convert ' . $homepath . 'women_shirt_images/sleeveplacket/' . $fabric_id . '_sleeve_' . $sleeve_id . '_left_view_3.png ' . $sleeve_placket_left_glow . ' -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/sleeveplacket/' . $fabric_id . '_sleeve_' . $sleeve_id . '_left_view_3.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//highlight
                        $cmd = ' composite ' . $sleeve_placket_left_highlighted . ' -compose Overlay  ' . $homepath . 'women_shirt_images/sleeveplacket/' . $fabric_id . '_sleeve_' . $sleeve_id . '_left_view_3.png ' . $homepath . 'women_shirt_images/sleeveplacket/' . $fabric_id . '_sleeve_' . $sleeve_id . '_left_view_3.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

                        $cmd = ' composite -compose Dst_In -gravity center ' . $sleeve_placket_right_mask . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'women_shirt_images/sleeveplacket/' . $fabric_id . '_sleeve_' . $sleeve_id . '_right_view_3.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

                        $cmd = ' convert ' . $homepath . 'women_shirt_images/sleeveplacket/' . $fabric_id . '_sleeve_' . $sleeve_id . '_right_view_3.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/sleeveplacket/' . $fabric_id . '_sleeve_' . $sleeve_id . '_right_view_3.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);

                        $cmd = ' convert ' . $homepath . 'women_shirt_images/sleeveplacket/' . $fabric_id . '_sleeve_' . $sleeve_id . '_right_view_3.png ' . $sleeve_placket_right_glow . ' -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/sleeveplacket/' . $fabric_id . '_sleeve_' . $sleeve_id . '_right_view_3.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//highlight
                        $cmd = ' composite ' . $sleeve_placket_right_highlighted . ' -compose Overlay  ' . $homepath . 'women_shirt_images/sleeveplacket/' . $fabric_id . '_sleeve_' . $sleeve_id . '_right_view_3.png ' . $homepath . 'women_shirt_images/sleeveplacket/' . $fabric_id . '_sleeve_' . $sleeve_id . '_right_view_3.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);
//combine two images
                        $cmd = ' convert ' . $homepath . 'women_shirt_images/sleeveplacket/' . $fabric_id . '_sleeve_' . $sleeve_id . '_right_view_3.png ' . $homepath . 'women_shirt_images/sleeveplacket/' . $fabric_id . '_sleeve_' . $sleeve_placket_id . '_left_view_3.png -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/sleeveplacket/' . $fabric_id . '_sleeve_' . $sleeve_placket_id . '_view_3.png';
                        $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($query);



                        $sleeve_left_glow = $homepath . '' . $sleeves['glow_front_left_image'];
                        $sleeve_left_mask = $homepath . '' . $sleeves['mask_front_left_image'];
                        $sleeve_left_highlighted = $homepath . '' . $sleeves['highlighted_front_left_image'];
                        $sleeve_right_glow = $homepath . '' . $sleeves['glow_front_right_image'];
                        $sleeve_right_mask = $homepath . '' . $sleeves['mask_front_right_image'];
                        $sleeve_right_highlighted = $homepath . '' . $sleeves['highlighted_front_right_image'];


                        if ($sleeves['glow_front_left_image'] != '' && $sleeves['mask_front_left_image'] != '' && $sleeves['highlighted_front_left_image'] != '') {

                            $cmd = ' composite -compose Dst_In -gravity center ' . $sleeve_left_mask . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'women_shirt_images/sleeves/' . $fabric_id . '_sleevesstyle_' . $style_type . '_size_' . $cuff_size . '_sleevestype_' . $cuff_type . '_' . $sleeve_id . '_left_view_1.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

                            $cmd = ' convert ' . $homepath . 'women_shirt_images/sleeves/' . $fabric_id . '_sleevesstyle_' . $style_type . '_size_' . $cuff_size . '_sleevestype_' . $cuff_type . '_' . $sleeve_id . '_left_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/sleeves/' . $fabric_id . '_sleevesstyle_' . $style_type . '_size_' . $cuff_size . '_sleevestype_' . $cuff_type . '_' . $sleeve_id . '_left_view_1.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

                            $cmd = ' convert ' . $homepath . 'women_shirt_images/sleeves/' . $fabric_id . '_sleevesstyle_' . $style_type . '_size_' . $cuff_size . '_sleevestype_' . $cuff_type . '_' . $sleeve_id . '_left_view_1.png ' . $sleeve_left_glow . ' -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/sleeves/' . $fabric_id . '_sleevesstyle_' . $style_type . '_size_' . $cuff_size . '_sleevestype_' . $cuff_type . '_' . $sleeve_id . '_left_view_1.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

//highlight
                            $cmd = ' composite ' . $sleeve_left_highlighted . ' -compose Overlay  ' . $homepath . 'women_shirt_images/sleeves/' . $fabric_id . '_sleevesstyle_' . $style_type . '_size_' . $cuff_size . '_sleevestype_' . $cuff_type . '_' . $sleeve_id . '_left_view_1.png ' . $homepath . 'women_shirt_images/sleeves/' . $fabric_id . '_sleevesstyle_' . $style_type . '_size_' . $cuff_size . '_sleevestype_' . $cuff_type . '_' . $sleeve_id . '_left_view_1.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

                            $cmd = ' composite -compose Dst_In -gravity center ' . $sleeve_right_mask . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'women_shirt_images/sleeves/' . $fabric_id . '_sleevesstyle_' . $style_type . '_size_' . $cuff_size . '_sleevestype_' . $cuff_type . '_' . $sleeve_id . '_right_view_1.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

                            $cmd = ' convert ' . $homepath . 'women_shirt_images/sleeves/' . $fabric_id . '_sleevesstyle_' . $style_type . '_size_' . $cuff_size . '_sleevestype_' . $cuff_type . '_' . $sleeve_id . '_right_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/sleeves/' . $fabric_id . '_sleevesstyle_' . $style_type . '_size_' . $cuff_size . '_sleevestype_' . $cuff_type . '_' . $sleeve_id . '_right_view_1.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

                            $cmd = ' convert ' . $homepath . 'women_shirt_images/sleeves/' . $fabric_id . '_sleevesstyle_' . $style_type . '_size_' . $cuff_size . '_sleevestype_' . $cuff_type . '_' . $sleeve_id . '_right_view_1.png ' . $sleeve_right_glow . ' -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/sleeves/' . $fabric_id . '_sleevesstyle_' . $style_type . '_size_' . $cuff_size . '_sleevestype_' . $cuff_type . '_' . $sleeve_id . '_right_view_1.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);
//highlight
                            $cmd = ' composite ' . $sleeve_right_highlighted . ' -compose Overlay  ' . $homepath . 'women_shirt_images/sleeves/' . $fabric_id . '_sleevesstyle_' . $style_type . '_size_' . $cuff_size . '_sleevestype_' . $cuff_type . '_' . $sleeve_id . '_right_view_1.png ' . $homepath . 'women_shirt_images/sleeves/' . $fabric_id . '_sleevesstyle_' . $style_type . '_size_' . $cuff_size . '_sleevestype_' . $cuff_type . '_' . $sleeve_id . '_right_view_1.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);
//combine two images
                            $cmd = ' convert ' . $homepath . 'women_shirt_images/sleeves/' . $fabric_id . '_sleevesstyle_' . $style_type . '_size_' . $cuff_size . '_sleevestype_' . $cuff_type . '_' . $sleeve_id . '_right_view_1.png ' . $homepath . 'women_shirt_images/sleeves/' . $fabric_id . '_sleevesstyle_' . $style_type . '_size_' . $cuff_size . '_sleevestype_' . $cuff_type . '_' . $sleeve_id . '_left_view_1.png -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/sleeves/' .
                                    $fabric_id . '_sleevesstyle_' . $style_type . '_size_' . $cuff_size . '_sleevestype_' . $cuff_type . '_' . $sleeve_id . '_view_1.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

                            $cmd = "rm " . $homepath . 'women_shirt_images/sleeves/' . $fabric_id . '_sleevesstyle_' . $style_type . '_size_' . $cuff_size . '_sleevestype_' . $cuff_type . '_' . $sleeve_id . '_right_view_1.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

                            $cmd = "rm " . $homepath . 'women_shirt_images/sleeves/' . $fabric_id . '_sleevesstyle_' . $style_type . '_size_' . $cuff_size . '_sleevestype_' . $cuff_type . '_' . $sleeve_id . '_left_view_1.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);
                        }


                        $glow_left_sleeves_image = $homepath . '' . $sleeves['glow_side_left_image'];
                        $mask_left_sleeves_image = $homepath . '' . $sleeves['mask_side_left_image'];
                        $highlighted_left_sleeves_image = $homepath . '' . $sleeves['highlighted_side_left_image'];

                        $glow_right_sleeves_image = $homepath . '' . $sleeves['glow_side_right_image'];
                        $mask_right_sleeves_image = $homepath . '' . $sleeves['mask_side_right_image'];
                        $highlighted_right_sleeves_image = $homepath . '' . $sleeves['highlighted_side_right_image'];

                        if ($sleeves['glow_side_left_image'] != '' && $sleeves['mask_side_left_image'] != '' && $sleeves['highlighted_side_left_image'] != '') {

                            $cmd = ' composite -compose Dst_In -gravity center ' . $mask_left_sleeves_image . ' ' . $wave_left . ' -alpha Set ' . $homepath . 'women_shirt_images/sleeves/' . $fabric_id . '_sleevesstyle_' . $style_type . '_size_' . $cuff_size . '_sleevestype_' . $cuff_type . '_' . $sleeve_id . '_left_view_2.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

                            $cmd = ' convert ' . $homepath . 'women_shirt_images/sleeves/' . $fabric_id . '_sleevesstyle_' . $style_type . '_size_' . $cuff_size . '_sleevestype_' . $cuff_type . '_' . $sleeve_id . '_left_view_2.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/sleeves/' . $fabric_id . '_sleevesstyle_' . $style_type . '_size_' . $cuff_size . '_sleevestype_' . $cuff_type . '_' . $sleeve_id . '_left_view_2.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

                            $cmd = ' convert ' . $homepath . 'women_shirt_images/sleeves/' . $fabric_id . '_sleevesstyle_' . $style_type . '_size_' . $cuff_size . '_sleevestype_' . $cuff_type . '_' . $sleeve_id . '_left_view_2.png ' . $glow_left_sleeves_image . ' -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/sleeves/' . $fabric_id . '_sleevesstyle_' . $style_type . '_size_' . $cuff_size . '_sleevestype_' . $cuff_type . '_' . $sleeve_id . '_left_view_2.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);
//highlight
                            $cmd = ' composite ' . $highlighted_left_sleeves_image . ' -compose Overlay  ' . $homepath . 'women_shirt_images/sleeves/' . $fabric_id . '_sleevesstyle_' . $style_type . '_size_' . $cuff_size . '_sleevestype_' . $cuff_type . '_' . $sleeve_id . '_left_view_2.png ' . $homepath . 'women_shirt_images/sleeves/' . $fabric_id . '_sleevesstyle_' . $style_type . '_size_' . $cuff_size . '_sleevestype_' . $cuff_type . '_' . $sleeve_id . '_left_view_2.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

                            $cmd = ' composite -compose Dst_In -gravity center ' . $mask_right_sleeves_image . ' ' . $wave_right . ' -alpha Set ' . $homepath . 'women_shirt_images/sleeves/' . $fabric_id . '_sleevesstyle_' . $style_type . '_size_' . $cuff_size . '_sleevestype_' . $cuff_type . '_' . $sleeve_id . '_right_view_2.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

                            $cmd = ' convert ' . $homepath . 'women_shirt_images/sleeves/' . $fabric_id . '_sleevesstyle_' . $style_type . '_size_' . $cuff_size . '_sleevestype_' . $cuff_type . '_' . $sleeve_id . '_right_view_2.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/sleeves/' . $fabric_id . '_sleevesstyle_' . $style_type . '_size_' . $cuff_size . '_sleevestype_' . $cuff_type . '_' . $sleeve_id . '_right_view_2.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

                            $cmd = ' convert ' . $homepath . 'women_shirt_images/sleeves/' . $fabric_id . '_sleevesstyle_' . $style_type . '_size_' . $cuff_size . '_sleevestype_' . $cuff_type . '_' . $sleeve_id . '_right_view_2.png ' . $glow_right_sleeves_image . ' -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/sleeves/' . $fabric_id . '_sleevesstyle_' . $style_type . '_size_' . $cuff_size . '_sleevestype_' . $cuff_type . '_' . $sleeve_id . '_right_view_2.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);
//highlight
                            $cmd = ' composite ' . $highlighted_right_sleeves_image . ' -compose Overlay  ' . $homepath . 'women_shirt_images/sleeves/' . $fabric_id . '_sleevesstyle_' . $style_type . '_size_' . $cuff_size . '_sleevestype_' . $cuff_type . '_' . $sleeve_id . '_right_view_2.png ' . $homepath . 'women_shirt_images/sleeves/' . $fabric_id . '_sleevesstyle_' . $style_type . '_size_' . $cuff_size . '_sleevestype_' . $cuff_type . '_' . $sleeve_id . '_right_view_2.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);
//combine two images
                            $cmd = ' convert ' . $homepath . 'women_shirt_images/sleeves/' . $fabric_id . '_sleevesstyle_' . $style_type . '_size_' . $cuff_size . '_sleevestype_' . $cuff_type . '_' . $sleeve_id . '_right_view_2.png ' . $homepath . 'women_shirt_images/sleeves/' . $fabric_id . '_sleevesstyle_' . $style_type . '_size_' . $cuff_size . '_sleevestype_' . $cuff_type . '_' . $sleeve_id . '_left_view_2.png -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/sleeves/' . $fabric_id . '_sleevesstyle_' . $style_type . '_size_' . $cuff_size . '_sleevestype_' . $cuff_type . '_' . $sleeve_id . '_view_2.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

                            $cmd = "rm " . $homepath . 'women_shirt_images/sleeves/' . $fabric_id . '_sleevesstyle_' . $style_type . '_size_' . $cuff_size . '_sleevestype_' . $cuff_type . '_' . $sleeve_id . '_right_view_2.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

                            $cmd = "rm " . $homepath . 'women_shirt_images/sleeves/' . $fabric_id . '_sleevesstyle_' . $style_type . '_size_' . $cuff_size . '_sleevestype_' . $cuff_type . '_' . $sleeve_id . '_left_view_2.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);
                        }

                        $glow_left_back_image = $homepath . '' . $sleeves['glow_back_left_image'];
                        $mask_left_back_image = $homepath . '' . $sleeves['mask_back_left_image'];
                        $highlighted_left_back_image = $homepath . '' . $sleeves['highlighted_back_left_image'];
                        $glow_right_back_image = $homepath . '' . $sleeves['glow_back_right_image'];
                        $mask_right_back_image = $homepath . '' . $sleeves['mask_back_right_image'];
                        $highlighted_right_back_image = $homepath . '' . $sleeves['highlighted_back_right_image'];

                        if ($sleeves['glow_back_left_image'] != '' && $sleeves['mask_back_left_image'] != '' && $sleeves['highlighted_back_left_image'] != '') {

                            $cmd = ' composite -compose Dst_In -gravity center ' . $mask_left_back_image . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'women_shirt_images/sleeves/' . $fabric_id . '_sleevesstyle_' . $style_type . '_size_' . $cuff_size . '_sleevestype_' . $cuff_type . '_' . $sleeve_id . '_left_view_3.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

                            $cmd = ' convert ' . $homepath . 'women_shirt_images/sleeves/' . $fabric_id . '_sleevesstyle_' . $style_type . '_size_' . $cuff_size . '_sleevestype_' . $cuff_type . '_' . $sleeve_id . '_left_view_3.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/sleeves/' . $fabric_id . '_sleevesstyle_' . $style_type . '_size_' . $cuff_size . '_sleevestype_' . $cuff_type . '_' . $sleeve_id . '_left_view_3.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

                            $cmd = ' convert ' . $homepath . 'women_shirt_images/sleeves/' . $fabric_id . '_sleevesstyle_' . $style_type . '_size_' . $cuff_size . '_sleevestype_' . $cuff_type . '_' . $sleeve_id . '_left_view_3.png ' . $glow_left_back_image . ' -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/sleeves/' . $fabric_id . '_sleevesstyle_' . $style_type . '_size_' . $cuff_size . '_sleevestype_' . $cuff_type . '_' . $sleeve_id . '_left_view_3.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);
//highlight
                            $cmd = ' composite ' . $highlighted_left_back_image . ' -compose Overlay  ' . $homepath . 'women_shirt_images/sleeves/' . $fabric_id . '_sleevesstyle_' . $style_type . '_size_' . $cuff_size . '_sleevestype_' . $cuff_type . '_' . $sleeve_id . '_left_view_3.png ' . $homepath . 'women_shirt_images/sleeves/' . $fabric_id . '_sleevesstyle_' . $style_type . '_size_' . $cuff_size . '_sleevestype_' . $cuff_type . '_' . $sleeve_id . '_left_view_3.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

                            $cmd = ' composite -compose Dst_In -gravity center ' . $mask_right_back_image . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'women_shirt_images/sleeves/' . $fabric_id . '_sleevesstyle_' . $style_type . '_size_' . $cuff_size . '_sleevestype_' . $cuff_type . '_' . $sleeve_id . '_right_view_3.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

                            $cmd = ' convert ' . $homepath . 'women_shirt_images/sleeves/' . $fabric_id . '_sleevesstyle_' . $style_type . '_size_' . $cuff_size . '_sleevestype_' . $cuff_type . '_' . $sleeve_id . '_right_view_3.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/sleeves/' . $fabric_id . '_sleevesstyle_' . $style_type . '_size_' . $cuff_size . '_sleevestype_' . $cuff_type . '_' . $sleeve_id . '_right_view_3.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

                            $cmd = ' convert ' . $homepath . 'women_shirt_images/sleeves/' . $fabric_id . '_sleevesstyle_' . $style_type . '_size_' . $cuff_size . '_sleevestype_' . $cuff_type . '_' . $sleeve_id . '_right_view_3.png ' . $glow_right_back_image . ' -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/sleeves/' . $fabric_id . '_sleevesstyle_' . $style_type . '_size_' . $cuff_size . '_sleevestype_' . $cuff_type . '_' . $sleeve_id . '_right_view_3.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);
//highlight
                            $cmd = ' composite ' . $highlighted_right_back_image . ' -compose Overlay  ' . $homepath . 'women_shirt_images/sleeves/' . $fabric_id . '_sleevesstyle_' . $style_type . '_size_' . $cuff_size . '_sleevestype_' . $cuff_type . '_' . $sleeve_id . '_right_view_3.png ' . $homepath . 'women_shirt_images/sleeves/' . $fabric_id . '_sleevesstyle_' . $style_type . '_size_' . $cuff_size . '_sleevestype_' . $cuff_type . '_' . $sleeve_id . '_right_view_3.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);
//combine two images
                            $cmd = ' convert ' . $homepath . 'women_shirt_images/sleeves/' . $fabric_id . '_sleevesstyle_' . $style_type . '_size_' . $cuff_size . '_sleevestype_' . $cuff_type . '_' . $sleeve_id . '_left_view_3.png ' . $homepath . 'women_shirt_images/sleeves/' . $fabric_id . '_sleevesstyle_' . $style_type . '_size_' . $cuff_size . '_sleevestype_' . $cuff_type . '_' . $sleeve_id . '_right_view_3.png -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/sleeves/' . $fabric_id . '_sleevesstyle_' . $style_type . '_size_' . $cuff_size . '_sleevestype_' . $cuff_type . '_' . $sleeve_id . '_view_3.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

                            $cmd = "rm " . $homepath . 'women_shirt_images/sleeves/' . $fabric_id . '_sleevesstyle_' . $style_type . '_size_' . $cuff_size . '_sleevestype_' . $cuff_type . '_' . $sleeve_id . '_right_view_3.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

                            $cmd = "rm " . $homepath . 'women_shirt_images/sleeves/' . $fabric_id . '_sleevesstyle_' . $style_type . '_size_' . $cuff_size . '_sleevestype_' . $cuff_type . '_' . $sleeve_id . '_left_view_3.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);
                        }


                        $glow_fold_image = $homepath . '' . $sleeves['glow_fold_image'];
                        $mask_fold_image = $homepath . '' . $sleeves['mask_fold_image'];
                        $highlighted_fold_image = $homepath . '' . $sleeves['highlighted_fold_image'];


                        if ($sleeves['glow_fold_image'] != '' && $sleeves['mask_fold_image'] != '' && $sleeves['highlighted_fold_image'] != '') {

                            $cmd = ' composite -compose Dst_In -gravity center ' . $mask_fold_image . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'women_shirt_images/sleeves/' . $fabric_id . '_sleevesstyle_' . $style_type . '_size_' . $cuff_size . '_sleevestype_' . $cuff_type . '_' . $sleeve_id . '_view_4.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);
// echo 'composite -compose Dst_In -gravity center ' . $mask_fold_image . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'women_shirt_images/sleeves/' . $fabric_id . '_sleevesstyle_' . $style_type . '_size_' . $cuff_size . '_sleevestype_' . $cuff_type . '_' . $sleeve_id . '_view_4.png';
                            $cmd = ' convert ' . $homepath . 'women_shirt_images/sleeves/' . $fabric_id . '_sleevesstyle_' . $style_type . '_size_' . $cuff_size . '_sleevestype_' . $cuff_type . '_' . $sleeve_id . '_view_4.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_shirt_images/sleeves/' . $fabric_id . '_sleevesstyle_' . $style_type . '_size_' . $cuff_size . '_sleevestype_' . $cuff_type . '_' . $sleeve_id . '_view_4.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);

                            $cmd = ' convert ' . $homepath . 'women_shirt_images/sleeves/' . $fabric_id . '_sleevesstyle_' . $style_type . '_size_' . $cuff_size . '_sleevestype_' . $cuff_type . '_' . $sleeve_id . '_view_4.png ' . $glow_fold_image . ' -geometry +0+0 -composite ' . $homepath . 'women_shirt_images/sleeves/' . $fabric_id . '_sleevesstyle_' . $style_type . '_size_' . $cuff_size . '_sleevestype_' . $cuff_type . '_' . $sleeve_id . '_view_4.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);
//highlight
                            $cmd = ' composite ' . $highlighted_right_back_image . ' -compose Overlay  ' . $homepath . 'women_shirt_images/sleeves/' . $fabric_id . '_sleevesstyle_' . $style_type . '_size_' . $cuff_size . '_sleevestype_' . $cuff_type . '_' . $sleeve_id . '_view_4.png ' . $homepath . 'women_shirt_images/sleeves/' . $fabric_id . '_sleevesstyle_' . $style_type . '_size_' . $cuff_size . '_sleevestype_' . $cuff_type . '_' . $sleeve_id . '_view_4.png';
                            $query = "INSERT INTO `generation_cmd`(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($query);
                        }
                    }
// shirt sleeves end..
                    $updateQuery = "UPDATE `shirt_shirtfabricwomen` SET `status`=1 WHERE `shirtfabricwomen_id`=" . $fabric_id;
                    $writeQuery = "INSERT INTO`generation_cmd`(`command`) VALUES ('{$updateQuery}')";
                    $this->connection->query($writeQuery);
                }

//================  Shirt Image Generator END ==================================

                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['shirtfabricwomen_id' => $model->getId()]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the Data.'));
            }

            $this->dataPersistor->set('shirt_shirtfabricwomen', $data);
            return $resultRedirect->setPath('*/*/edit', ['shirtfabricwomen_id' => $this->getRequest()->getParam('shirtfabricwomen_id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }

}
