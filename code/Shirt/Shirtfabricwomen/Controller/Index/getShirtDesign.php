<?php

namespace Shirt\Shirtfabricwomen\Controller\Index;

class getShirtDesign extends \Magento\Framework\App\Action\Action {

    public function __construct(
    \Magento\Framework\App\Action\Context $context, \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory, \Shirt\Shirtfabricwomen\Model\ShirtCuffFactory $shirtCuff, \Shirt\Shirtfabricwomen\Model\ShirtStyleFactory $shirtStyle, \Shirt\Shirtfabricwomen\Model\ShirtSleevesFactory $shirtSleeves, \Shirt\Shirtfabricwomen\Model\ShirtFitFactory $shirtFit, \Shirt\Shirtfabricwomen\Model\ShirtPocketFactory $shirtPocket, \Shirt\Shirtfabricwomen\Model\ShirtPleatsFactory $shirtPleats, \Shirt\Shirtfabricwomen\Model\ShirtPlacketsFactory $shirtPlackets, \Shirt\Shirtfabricwomen\Model\ShirtBottomFactory $shirtBottom
    ) {
        $this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->resource = $this->objectManager->get('Magento\Framework\App\ResourceConnection');
        $this->connection = $this->resource->getConnection();
        $this->resultJsonFactory = $resultJsonFactory;
        $this->_shirtStyle = $shirtStyle;
        $this->_shirtCuff = $shirtCuff;
        $this->_shirtSleeves = $shirtSleeves;
        $this->_shirtFit = $shirtFit;
        $this->_shirtPocket = $shirtPocket;
        $this->_shirtPleats = $shirtPleats;
        $this->_shirtPlackets = $shirtPlackets;
        $this->_shirtBottom = $shirtBottom;


        return parent::__construct($context);
    }

    public function execute() {
        return $this->getShirtButtonFabricAction();
    }

    public function getShirtButtonFabricAction() {

        $storeManager = $this->objectManager->get('\Magento\Store\Model\StoreManagerInterface');
        $basePath = $storeManager->getStore()->getBaseUrl();  // get base url...
        $mediaURL = $storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);  // get media url...
        // shirtstyle
        $modelCollerstyle = $this->_shirtStyle->create()->getCollection();
        $modelCollerstyle->addFieldToFilter('status', 1);
        $modelCollerstyle->setOrder('shirtstyle_id', 'ASC');
        $collarstyle_collection = $modelCollerstyle->getData();

        $collarstylesArr = array();
        foreach ($collarstyle_collection as $collarstyle) {

            $id = $collarstyle['shirtstyle_id'];
            $title = $collarstyle['title'];
            $class = $collarstyle['class'];
            $price = $collarstyle['price'];
            $thumb = $collarstyle['thumb'];
            $status = $collarstyle['status'];

            if ($status) {
                $collarstylesArr[] = array(
                    'id' => $id,
                    'class' => $class,
                    'parent' => 'Collar Style',
                    'designType' => 'shirt',
                    'name' => $title,
                    'price' => $price,
                    'img' => $mediaURL . 'shirt-shirtfabricwomen/' . $thumb
                );
            }
        }

        $collarstyle_data = array('id' => 1, 'name' => 'Collar Style', 'designType' => 'shirt', 'class' => 'icon-Collar_Main_Icon', 'img' => $mediaURL . 'shirt-shirtfabricwomen/shirt_images/thumbnail/00_Style_Main_Icon.png', "style" => $collarstylesArr);

        // shirtcuff
        $modelShirtCuff = $this->_shirtCuff->create()->getCollection();
        $modelShirtCuff->addFieldToFilter('status', 1);
        $modelShirtCuff->setOrder('shirtcuff_id', 'ASC');
        $cuff_collection = $modelShirtCuff->getData();
        $cuffsArr = array();
        foreach ($cuff_collection as $cuff) {

            $id = $cuff['shirtcuff_id'];
            $title = $cuff['title'];
            $class = $cuff['class'];
            $price = $cuff['price'];
            $thumb = null;
            $status = $cuff['status'];

            if ($status) {
                $cuffsArr[] = array(
                    'id' => $id,
                    'class' => $class,
                    'parent' => 'Cuffs',
                    'designType' => 'shirt',
                    'name' => $title,
                    'price' => $price,
                    'img' => $mediaURL . 'shirt-shirtfabricwomen/' . $thumb
                );
            }
        }

        $cuff_data = array('id' => 2, 'name' => 'Cuffs', 'designType' => 'shirt', 'class' => 'icon-Cuff_Main_Icon', 'img' => $mediaURL . 'shirt-shirtfabricwomen/shirt_images/thumbnail/00_Cuffs_Main_Icon.png', "style" => $cuffsArr);

        // shirtsleeves
        $modelShirtSleeves = $this->_shirtSleeves->create()->getCollection();
        $modelShirtSleeves->addFieldToFilter('status', 1);
        $modelShirtSleeves->setOrder('shirtsleeves_id', 'ASC');
        $sleeves_collection = $modelShirtSleeves->getData();
        $sleevesArr = array();
        foreach ($sleeves_collection as $sleeves) {

            $id = $sleeves['shirtsleeves_id'];
            $title = $sleeves['title'];
            $class = $sleeves['class'];
            $price = $sleeves['price'];
            $thumb = null;
            $status = $sleeves['status'];

            if ($status) {
                $sleevesArr[] = array(
                    'id' => $id,
                    'class' => $class,
                    'parent' => 'Sleeves',
                    'designType' => 'shirt',
                    'name' => $title,
                    'price' => $price,
                    'img' => $mediaURL . 'shirt-shirtfabricwomen/' . $thumb
                );
            }
        }
        $sleeves_data = array('id' => 3, 'name' => 'Sleeves', 'designType' => 'shirt', 'class' => 'icon-Full_Sleeve', 'img' => $mediaURL . 'shirt-shirtfabricwomen/shirt_images/thumbnail/00Fit_Main_Icon.png', "style" => $sleevesArr);

        // shirtfit
        $modelFit = $this->_shirtFit->create()->getCollection();
        $modelFit->addFieldToFilter('status', 1);
        $modelFit->setOrder('shirtfit_id', 'ASC');
        $fit_collection = $modelFit->getData();
        $fitsArr = array();
        foreach ($fit_collection as $fit) {

            $id = $fit['shirtfit_id'];
            $title = $fit['title'];
            $class = $fit['class'];
            $price = $fit['price'];
            $thumb = null;
            $status = $fit['status'];

            if ($status) {
                $fitsArr[] = array(
                    'id' => $id,
                    'class' => $class,
                    'parent' => 'Fit',
                    'designType' => 'shirt',
                    'name' => $title,
                    'price' => $price,
                    'img' => $mediaURL . 'shirt-shirtfabricwomen/' . $thumb
                );
            }
        }



        $fit_data = array('id' => 4, 'name' => 'Fit', 'designType' => 'shirt', 'class' => 'icon-Slim_Fit', 'img' => $mediaURL . 'shirt-shirtfabricwomen/shirt_images/thumbnail/00Fit_Main_Icon.png', "style" => $fitsArr);

        // pocket  
        $modelPocket = $this->_shirtPocket->create()->getCollection();
        $modelPocket->addFieldToFilter('status', 1);
        $modelPocket->setOrder('shirtpocket_id', 'ASC');
        $pocket_collection = $modelPocket->getData();
        $pocketsArr = array();
        foreach ($pocket_collection as $pocket) {

            $id = $pocket['shirtpocket_id'];
            $title = $pocket['title'];
            $class = $pocket['class'];
            $price = $pocket['price'];
            $thumb = $pocket['thumb'];
            $status = $pocket['status'];

            if ($status) {
                $pocketsArr[] = array(
                    'id' => $id,
                    'class' => $class,
                    'parent' => 'Pocket',
                    'designType' => 'shirt',
                    'name' => $title,
                    'price' => $price,
                    'img' => $mediaURL . 'shirt-shirtfabricwomen/' . $thumb
                );
            }
        }

        $pocket_data = array('id' => 5, 'name' => 'Pocket', 'designType' => 'shirt', 'class' => 'icon-WithoutFlap_Pocket_Right', 'img' => $mediaURL . 'shirt-shirtfabricwomen/shirt_images/thumbnail/00_Pocket_Main_Icon.png', "style" => $pocketsArr);

        // shirtPlackets
        $modelPlackets = $this->_shirtPlackets->create()->getCollection();
        $modelPlackets->addFieldToFilter('status', 1);
        $modelPlackets->setOrder('shirtplackets_id', 'ASC');
        $plackets_collection = $modelPlackets->getData();
        $placketsArr = array();
        foreach ($plackets_collection as $plackets) {

            $id = $plackets['shirtplackets_id'];
            $title = $plackets['title'];
            $class = $plackets['class'];
            $price = $plackets['price'];
            $thumb = $plackets['thumb'];
            $status = $plackets['status'];

            if ($status) {
                $placketsArr[] = array(
                    'id' => $id,
                    'class' => $class,
                    'parent' => 'Plackets',
                    'designType' => 'shirt',
                    'name' => $title,
                    'price' => $price,
                    'img' => $mediaURL . 'shirt-shirtfabricwomen/' . $thumb
                );
            }
        }



        $plackets_data = array('id' => 6, 'name' => 'Plackets', 'designType' => 'shirt', 'class' => 'icon-Standard_Placket', 'img' => $mediaURL . 'shirt-shirtfabricwomen/shirt_images/thumbnail/00Plackets_Main_Icon.png', "style" => $placketsArr);


        // shirtpleats
        $modelPleates = $this->_shirtPleats->create()->getCollection();
        $modelPleates->addFieldToFilter('status', 1);
        $modelPleates->setOrder('shirtpleats_id', 'ASC');
        $pleats_collection = $modelPleates->getData();
        $pleatsArr = array();
        foreach ($pleats_collection as $pleats) {

            $id = $pleats['shirtpleats_id'];
            $title = $pleats['title'];
            $class = $pleats['class'];
            $price = $pleats['price'];
            $image = $pleats['image'];
            $status = $pleats['status'];

            if ($status) {
                $pleatsArr[] = array(
                    'id' => $id,
                    'class' => $class,
                    'parent' => 'Pleats',
                    'designType' => 'shirt',
                    'name' => $title,
                    'price' => $price,
                    'img' => $mediaURL . 'shirt-shirtfabricwomen/' . $image
                );
            }
        }

        $pleats_data = array('id' => 7, 'name' => 'Pleats', 'designType' => 'shirt', 'class' => 'icon-Box_Pleats', 'img' => $mediaURL . 'shirt-shirtfabricwomen/shirt_images/thumbnail/00Pleats_Main_Icon.png', "style" => $pleatsArr);

        // shirtbottom
        $modelBottom = $this->_shirtBottom->create()->getCollection();
        $modelBottom->addFieldToFilter('status', 1);
        $modelBottom->setOrder('shirtbottom_id', 'ASC');
        $bottom_collection = $modelBottom->getData();


        $bottomsArr = array();
        foreach ($bottom_collection as $bottom) {

            $id = $bottom['shirtbottom_id'];
            $title = $bottom['title'];
            $class = $bottom['class'];
            $price = $bottom['price'];
            $view1_image = $bottom['view1_image'];
            $view2_image = $bottom['view2_image'];
            $view3_image = $bottom['view3_image'];
            $status = $bottom['status'];

            if ($status) {
                $bottomsArr[] = array(
                    'id' => $id,
                    'class' => $class,
                    'parent' => 'Bottom',
                    'designType' => 'shirt',
                    'name' => $title,
                    'price' => $price,
                    'view1_image' => $mediaURL . 'shirt-shirtfabricwomen/' . $view1_image,
                    'view2_image' => $mediaURL . 'shirt-shirtfabricwomen/' . $view2_image,
                    'view3_image' => $mediaURL . 'shirt-shirtfabricwomen/' . $view3_image,
                );
            }
        }

        $bottom_data = array('id' => 8, 'name' => 'Bottom', 'designType' => 'shirt', 'class' => 'icon-Tailed', 'img' => $mediaURL . 'shirt-shirtfabricwomen/shirt_images/thumbnail/00_Bottom_Main_Icon.png', "style" => $bottomsArr);
        $shirtDesignInfo = array($collarstyle_data, $cuff_data, $sleeves_data, $fit_data, $pocket_data, $plackets_data, $pleats_data, $bottom_data);
        return $this->resultJsonFactory->create()->setData($shirtDesignInfo);   // X2
    }

}
