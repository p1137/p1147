<?php

namespace Shirt\Shirtfabricwomen\Controller\Index;

class getShirtButtonFabric extends \Magento\Framework\App\Action\Action {

    public function __construct(
    \Magento\Framework\App\Action\Context $context, \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory, \Shirt\Threadfabricwomen\Model\ThreadfabricwomenFactory $shirtButtonFabric
    ) {
        $this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->resource = $this->objectManager->get('Magento\Framework\App\ResourceConnection');
        $this->connection = $this->resource->getConnection();
        $this->resultJsonFactory = $resultJsonFactory;
        $this->_shirtButtonFabric = $shirtButtonFabric;  //shirt_button_fabric

        return parent::__construct($context);
    }

    public function execute() {
        return $this->getShirtButtonFabricAction();
    }

    public function getShirtButtonFabricAction() {
        $storeManager = $this->objectManager->get('\Magento\Store\Model\StoreManagerInterface');
        $basePath = $storeManager->getStore()->getBaseUrl();  // get base url...
        $mediaURL = $storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);  // get media url...

        $model = $this->_shirtButtonFabric->create()->getCollection();
        $rows = $model->getData();
        $fabrics = array();
        foreach ($rows as $fabric) {
            $id = $fabric['id'];
            $name = $fabric['title'];
            $price = $fabric['price'];
            $thumb = $fabric['image'];
            $real_img = $fabric['image'];
            $shirt_type_name = "Cotton";
            $fabrics[] = array('id' => $id, 'price' => $price, 'name' => $name, 'real_img' => $mediaURL  . $real_img, 'img' => $mediaURL . $thumb);
        }
        return $this->resultJsonFactory->create()->setData($fabrics);
    }

}
