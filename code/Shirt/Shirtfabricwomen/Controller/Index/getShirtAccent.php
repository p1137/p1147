<?php

namespace Shirt\Shirtfabricwomen\Controller\Index;

class getShirtAccent extends \Magento\Framework\App\Action\Action {

    public function __construct(
    \Magento\Framework\App\Action\Context $context, \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory, \Shirt\Shirtfabricwomen\Model\ShirtAccentCollarstyleFactory $shirtAccentCollarstyle, //shirt_accent_collarstyle
            \Shirt\Shirtfabricwomen\Model\ShirtAccentCuffsFactory $shirtAccentCuffs, // shirt_accent_cuffs
            \Shirt\Shirtfabricwomen\Model\ShirtShirtthreadfabricFactory $shirtShirtthreadfabric, //shirt_shirtthreadfabric
            \Shirt\Shirtfabricwomen\Model\ShirtAccentThreadsFactory $shirtAccentThreads, //shirt_accent_threads
            \Shirt\Shirtfabricwomen\Model\ShirtAccentElbowpatchesFactory $shirtAccentElbowpatches, \Shirt\Shirtfabricwomen\Model\ShirtAccentChestpleatsFactory $shirt_accent_chestpleats, \Shirt\Shirtfabricwomen\Model\ShirtAccentPlacketFactory $shirtAccentPlacket,
 \Shirt\Threadfabricwomen\Model\ThreadfabricwomenFactory $shirtButtonFabric
    ) {
        $this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->resource = $this->objectManager->get('Magento\Framework\App\ResourceConnection');
        $this->connection = $this->resource->getConnection();
        $this->resultJsonFactory = $resultJsonFactory;
        $this->_shirtAccentCollarstyle = $shirtAccentCollarstyle; //shirt_accent_collarstyle
        $this->_shirtAccentCuffs = $shirtAccentCuffs;
        $this->_shirtShirtthreadfabric = $shirtShirtthreadfabric;
        $this->_shirtAccentThreads = $shirtAccentThreads;  //shirt_accent_threads
        $this->_shirtAccentElbowpatches = $shirtAccentElbowpatches;
        $this->_shirt_accent_chestpleats = $shirt_accent_chestpleats;
        $this->_shirtAccentPlacket = $shirtAccentPlacket;
        $this->_shirtButtonFabric = $shirtButtonFabric;  //shirt_button_fabric

        return parent::__construct($context);
    }

    public function execute() {
        return $this->getShirtAccentAction();
    }

    public function getShirtAccentAction() {

        $storeManager = $this->objectManager->get('\Magento\Store\Model\StoreManagerInterface');
        $basePath = $storeManager->getStore()->getBaseUrl();  // get base url...
        $mediaURL = $storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);  // get media url...

        $monogram_data = array('id' => 1, 'name' => 'Add Monogram', 'class' => 'icon-Add_Monogram', 'price' => '20', 'designType' => 'shirt', 'img' => $mediaURL. 'shirt-shirtfabricwomen/shirt_images/thumbnail/00_Style_Main_Icon.png');

        // shirt_accent_collarstyle
        $modelShirtAccentCollarstyle = $this->_shirtAccentCollarstyle->create()->getCollection();
        $modelShirtAccentCollarstyle->addFieldToFilter('status', 1);
        $modelShirtAccentCollarstyle->setOrder('collarstyle_id', 'ASC');
        $collarstyle_collection = $modelShirtAccentCollarstyle->getData();
        $collarstyleArr = array();
        foreach ($collarstyle_collection as $collarstyle) {

            $id = $collarstyle['collarstyle_id'];
            $title = $collarstyle['title'];
            $class = $collarstyle['class'];
            $price = $collarstyle['price'];
            $status = $collarstyle['status'];

            if ($status) {
                $collarstyleArr[] = array(
                    'id' => $id,
                    'class' => $class,
                    'parent' => 'CollarStyle',
                    'designType' => 'shirt',
                    'name' => $title,
                    'price' => $price,
                    'img' => ''
                );
            }
        }


        $collarstyle_data = array('id' => 2, 'name' => 'Collar Style', 'designType' => 'shirt', 'class' => 'icon-Collar_Contrast_Main_Icon', 'img' => $basePath . 'images/Collar/all.svg', "style" => $collarstyleArr);

        // shirt_accent_cuffs
        $modelShirtAccentCuffs = $this->_shirtAccentCuffs->create()->getCollection();
        $modelShirtAccentCuffs->addFieldToFilter('status', 1);
        $modelShirtAccentCuffs->setOrder('cuff_id', 'ASC');
        $cuff_collection = $modelShirtAccentCuffs->getData();
        $cuffsArr = array();
        foreach ($cuff_collection as $cuff) {

            $id = $cuff['cuff_id'];
            $title = $cuff['title'];
            $class = $cuff['class'];
            $price = $cuff['price'];
            $status = $cuff['status'];

            if ($status) {
                $cuffsArr[] = array(
                    'id' => $id,
                    'class' => $class,
                    'parent' => 'Cuffs',
                    'designType' => 'shirt',
                    'name' => $title,
                    'price' => $price,
                    'img' => ''
                );
            }
        }

        $cuff_data = array('id' => 3, 'name' => 'Cuffs', 'designType' => 'shirt', 'class' => 'icon-Cuff_Contrast_Main_Icon', 'price' => '10', 'img' => $basePath . 'images/Cuffs/all.png', "style" => $cuffsArr);

        // threads 
        $modelShirtShirtthreadfabric = $this->_shirtShirtthreadfabric->create()->getCollection();
        $modelShirtShirtthreadfabric->addFieldToFilter('status', 1);
        $modelShirtShirtthreadfabric->setOrder('fabric_id', 'ASC');
        $threads_collection = $modelShirtShirtthreadfabric->getData();
        $threadsArr = array();
        unset($class);

        foreach ($threads_collection as $threads) {

            $id = $threads['fabric_id'];
            $title = $threads['title'];
            if (isset($threads['class'])) {
                $class = $threads['class'];
            } else {
                $class = "";
            }

            $price = $threads['price'];
            $thumb = $threads['display_fabric_thumb'];
            $status = $threads['status'];

            if ($status) {
                $threadsColorArr[] = array(
                    'id' => $id,
                    'class' => $class,
                    'parent' => 'Threads',
                    'designType' => 'shirt',
                    'name' => $title,
                    'price' => $price,
                    'img' => $mediaURL. 'shirt-shirtfabricwomen/' . $thumb
                );
            }
        }

        // shirt_accent_threads
        $modelThreads = $this->_shirtAccentThreads->create()->getCollection();
        $modelThreads->addFieldToFilter('status', 1);
        $modelThreads->setOrder('threads_id', 'ASC');

        $threads_collection = $modelThreads->getData();
        $threadsArr = array();
        foreach ($threads_collection as $threads) {

            $id = $threads['threads_id'];
            $title = $threads['title'];
            $class = $threads['class'];
            $price = $threads['price'];
            $status = $threads['status'];

            if ($status) {
                $threadsArr[] = array(
                    'id' => $id,
                    'class' => $class,
                    'parent' => 'Threads',
                    'designType' => 'shirt',
                    'name' => $title,
                    'price' => $price,
                    'img' => $mediaURL. 'shirt-shirtfabricwomen/' . $thumb,
                );
            }
        }


        $threads_data = array('id' => 4, 'name' => 'Threads', 'designType' => 'shirt', 'class' => 'icon-Thread_Main_Icon', 'price' => '10', "style" => $threadsArr);

        // elbowpatches
        //shirt_accent_elbowpatches

        $modelElbowpatches = $this->_shirtAccentElbowpatches->create()->getCollection();
        $modelElbowpatches->addFieldTOFilter('status', 1);
        $modelElbowpatches->setOrder('elbowpatches_id', 'ASC');
        $elbowpatches_collection = $modelElbowpatches->getData();
        $elbowpatchesArr = array();
        foreach ($elbowpatches_collection as $elbowpatches) {

            $id = $elbowpatches['elbowpatches_id'];
            $title = $elbowpatches['title'];
            $class = $elbowpatches['class'];
            $price = $elbowpatches['price'];
            $thumb = null;
            $status = $elbowpatches['status'];

            if ($status) {
                $elbowpatchesArr[] = array(
                    'id' => $id,
                    'class' => $class,
                    'parent' => 'ElbowPatch',
                    'designType' => 'shirt',
                    'name' => $title,
                    'price' => $price,
                    'img' => $mediaURL. 'shirt-shirtfabricwomen/' . $thumb
                );
            }
        }

        $elbowpatches_data = array('id' => 5, 'name' => 'Elbow Patch', 'designType' => 'shirt', 'class' => 'icon-With_Elbow_Patch', 'price' => '10', 'img' => $mediaURL. 'shirt-shirtfabricwomen/jacket_img/thumbnail/00_Elbowpatch_Main_Icon.png', "style" => $elbowpatchesArr);

        // chest pleats
        $modelchestpleats = $this->_shirt_accent_chestpleats->create()->getCollection();
        $modelchestpleats->addFieldTOFilter('status', 1);
        $modelchestpleats->setOrder('chestpleats_id', 'ASC');
        $chestpleats_collection = $modelchestpleats->getData();
        $chestpleatsArr = array();
        foreach ($chestpleats_collection as $chestpleats) {

            $id = $chestpleats['chestpleats_id'];
            $title = $chestpleats['title'];
            $class = $chestpleats['class'];
            $price = $chestpleats['price'];
            $status = $chestpleats['status'];

            if ($status) {
                $chestpleatsArr[] = array(
                    'id' => $id,
                    'class' => $class,
                    'parent' => 'Chest pleats',
                    'designType' => 'shirt',
                    'designRel' => 'lining',
                    'name' => $title,
                    'price' => $price,
                    'img' => ''
                );
            }
        }

        $chestpleats_data = array('id' => 6, 'name' => 'Chest Pleats', 'designType' => 'shirt', 'class' => 'icon-ChestPleats_Yes', 'price' => '10', 'img' => $mediaURL. 'shirt-shirtfabricwomen/jacket_img/thumbnail/00Fit_Main_Icon.png', "style" => $chestpleatsArr);


        // front placket
        // shirt_accent_placket
        $modelPlacket = $this->_shirtAccentPlacket->create()->getCollection();
        $modelPlacket->addFieldToFilter('status', 1);
        $modelPlacket->setOrder('placket_id', 'ASC');
        $placket_collection = $modelPlacket->getData();
        $placketsArr = array();
        foreach ($placket_collection as $placket) {

            $id = $placket['placket_id'];
            $title = $placket['title'];
            $class = $placket['class'];
            $price = $placket['price'];
            $status = $placket['status'];

            if ($status) {
                $placketsArr[] = array(
                    'id' => $id,
                    'class' => $class,
                    'parent' => 'Front Placket',
                    'designType' => 'shirt',
                    'name' => $title,
                    'price' => $price
                );
            }
        }

	$model = $this->_shirtButtonFabric->create()->getCollection();
        $rows = $model->getData();
        $fabrics = array();
        foreach ($rows as $fabric) {
            $id = $fabric['id'];
            $name = $fabric['title'];
            $price = $fabric['price'];
            $thumb = $fabric['image'];
            $real_img = $fabric['image'];
            $shirt_type_name = "Cotton";
            $fabrics[] = array('id' => $id, 'price' => $price, 'name' => $name, 'real_img' => $mediaURL  . $real_img, 'img' => $mediaURL . $thumb);
        }

        $placket_data = array('id' => 7, 'name' => 'Front Placket', 'designType' => 'shirt', 'class' => 'icon-Placket_Contrast_By_Default', 'price' => '10', 'img' => $basePath . 'images/Plackets/all.png', "style" => $placketsArr);
        $button_threads_data = array('id' => 8, 'name' => 'Button Threads', 'designType' => 'shirt', 'class' => 'icon-Thread_Main_Icon', 'price' => '10', "style" => $fabrics);


        $shirtAccentInfo = array($monogram_data, $collarstyle_data, $cuff_data, $threads_data, $elbowpatches_data, $chestpleats_data, $placket_data, $button_threads_data);
        return $this->resultJsonFactory->create()->setData($shirtAccentInfo);
    }

}
