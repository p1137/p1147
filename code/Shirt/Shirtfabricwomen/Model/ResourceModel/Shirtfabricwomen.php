<?php

namespace Shirt\Shirtfabricwomen\Model\ResourceModel;

class Shirtfabricwomen extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb {

    protected function _construct() {
        $this->_init('shirt_shirtfabricwomen', 'shirtfabricwomen_id');
    }

}
