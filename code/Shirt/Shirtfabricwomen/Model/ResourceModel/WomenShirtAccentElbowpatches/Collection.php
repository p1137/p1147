<?php

/**
 * @author Akshay Shelke    
 * Copyright © 2018 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Shirt\Shirtfabricwomen\Model\ResourceModel\WomenShirtAccentElbowpatches;

use \Shirt\Shirtfabricwomen\Model\ResourceModel\AbstractCollection;

class Collection extends AbstractCollection {

    protected function _construct() {
        $this->_init('Shirt\Shirtfabricwomen\Model\WomenShirtAccentElbowpatches', 'Shirt\Shirtfabricwomen\Model\ResourceModel\WomenShirtAccentElbowpatches');
        $this->_map['fields']['elbowpatches_id'] = 'main_table.elbowpatches_id';
    }

}
