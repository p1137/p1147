<?php

/**
 * @author Akshay Shelke    
 * Copyright © 2018 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Shirt\Shirtfabricwomen\Model\ResourceModel\ShirtAccentChestpleats;

use \Shirt\Shirtfabricwomen\Model\ResourceModel\AbstractCollection;

class Collection extends AbstractCollection {

    protected function _construct() {
        $this->_init('Shirt\Shirtfabricwomen\Model\ShirtAccentChestpleats', 'Shirt\Shirtfabricwomen\Model\ResourceModel\ShirtAccentChestpleats');
        $this->_map['fields']['chestpleats_id'] = 'main_table.chestpleats_id';
    }

}
