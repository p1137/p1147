<?php

/**
 * @author Akshay Shelke    
 * Copyright © 2018 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Shirt\Shirtfabricwomen\Model\ResourceModel\ShirtButtonFabric;

use \Shirt\Shirtfabricwomen\Model\ResourceModel\AbstractCollection;

class Collection extends AbstractCollection {

    protected function _construct() {
        $this->_init('Shirt\Shirtfabricwomen\Model\ShirtButtonFabric', 'Shirt\Shirtfabricwomen\Model\ResourceModel\ShirtButtonFabric');
        $this->_map['fields']['button_fabric_id'] = 'main_table.button_fabric_id';
    }

}
