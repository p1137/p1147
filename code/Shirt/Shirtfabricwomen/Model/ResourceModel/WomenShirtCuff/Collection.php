<?php

/**
 * @author Akshay Shelke    
 * Copyright © 2018 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Shirt\Shirtfabricwomen\Model\ResourceModel\WomenShirtCuff;

use \Shirt\Shirtfabricwomen\Model\ResourceModel\AbstractCollection;

class Collection extends AbstractCollection {

    protected function _construct() {
        $this->_init('Shirt\Shirtfabricwomen\Model\WomenShirtCuff', 'Shirt\Shirtfabricwomen\Model\ResourceModel\WomenShirtCuff');
        $this->_map['fields']['shirtcuff_id'] = 'main_table.shirtcuff_id';
    }

}
