<?php

/**
 * @author Akshay Shelke    
 * Copyright © 2018 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Shirt\Shirtfabricwomen\Model\ResourceModel\ShirtBottom;

use \Shirt\Shirtfabricwomen\Model\ResourceModel\AbstractCollection;

class Collection extends AbstractCollection {

    protected function _construct() {
        $this->_init('Shirt\Shirtfabricwomen\Model\ShirtBottom', 'Shirt\Shirtfabricwomen\Model\ResourceModel\ShirtBottom');
        $this->_map['fields']['shirtbottom_id'] = 'main_table.shirtbottom_id';
    }

}
