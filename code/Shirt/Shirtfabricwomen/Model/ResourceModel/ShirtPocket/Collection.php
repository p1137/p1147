<?php

/**
 * @author Akshay Shelke    
 * Copyright © 2018 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Shirt\Shirtfabricwomen\Model\ResourceModel\ShirtPocket;

use \Shirt\Shirtfabricwomen\Model\ResourceModel\AbstractCollection;

class Collection extends AbstractCollection {

    protected function _construct() {
        $this->_init('Shirt\Shirtfabricwomen\Model\ShirtPocket', 'Shirt\Shirtfabricwomen\Model\ResourceModel\ShirtPocket');
        $this->_map['fields']['shirtpocket_id'] = 'main_table.shirtpocket_id';
    }

}
