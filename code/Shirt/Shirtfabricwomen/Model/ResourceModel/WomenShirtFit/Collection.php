<?php

/**
 * @author Akshay Shelke    
 * Copyright © 2018 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Shirt\Shirtfabricwomen\Model\ResourceModel\WomenShirtFit;

use \Shirt\Shirtfabricwomen\Model\ResourceModel\AbstractCollection;

class Collection extends AbstractCollection {

    protected function _construct() {
        $this->_init('Shirt\Shirtfabricwomen\Model\WomenShirtFit', 'Shirt\Shirtfabricwomen\Model\ResourceModel\WomenShirtFit');
        $this->_map['fields']['shirtfit_id'] = 'main_table.shirtfit_id';
    }

}
