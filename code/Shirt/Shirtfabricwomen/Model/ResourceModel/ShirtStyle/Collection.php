<?php

/**
 * @author Akshay Shelke    
 * Copyright © 2018 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Shirt\Shirtfabricwomen\Model\ResourceModel\ShirtStyle;

use \Shirt\Shirtfabricwomen\Model\ResourceModel\AbstractCollection;

class Collection extends AbstractCollection {

    protected function _construct() {
        $this->_init('Shirt\Shirtfabricwomen\Model\ShirtStyle', 'Shirt\Shirtfabricwomen\Model\ResourceModel\ShirtStyle');
        $this->_map['fields']['shirtstyle_id'] = 'main_table.shirtstyle_id';
    }

}
