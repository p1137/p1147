<?php

/**
 * @author Akshay Shelke    
 * Copyright © 2018 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Shirt\Shirtfabricwomen\Model\ResourceModel\ShirtShirtthreadfabric;

use \Shirt\Shirtfabricwomen\Model\ResourceModel\AbstractCollection;

class Collection extends AbstractCollection {

    protected function _construct() {
        $this->_init('Shirt\Shirtfabricwomen\Model\ShirtShirtthreadfabric', 'Shirt\Shirtfabricwomen\Model\ResourceModel\ShirtShirtthreadfabric');
        $this->_map['fields']['fabric_id'] = 'main_table.fabric_id';
    }

}
