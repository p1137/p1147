<?php

/**
 * @author Akshay Shelke    
 * Copyright © 2018 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Shirt\Shirtfabricwomen\Model\ResourceModel\WomenShirtPlackets;

use \Shirt\Shirtfabricwomen\Model\ResourceModel\AbstractCollection;

class Collection extends AbstractCollection {

    protected function _construct() {
        $this->_init('Shirt\Shirtfabricwomen\Model\WomenShirtPlackets', 'Shirt\Shirtfabricwomen\Model\ResourceModel\WomenShirtPlackets');
        $this->_map['fields']['shirtplackets_id'] = 'main_table.shirtplackets_id';
    }

}
