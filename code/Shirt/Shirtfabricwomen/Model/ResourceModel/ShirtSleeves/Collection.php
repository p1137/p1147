<?php

/**
 * @author Akshay Shelke    
 * Copyright © 2018 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Shirt\Shirtfabricwomen\Model\ResourceModel\ShirtSleeves;

use \Shirt\Shirtfabricwomen\Model\ResourceModel\AbstractCollection;

class Collection extends AbstractCollection {

    protected function _construct() {
        $this->_init('Shirt\Shirtfabricwomen\Model\ShirtSleeves', 'Shirt\Shirtfabricwomen\Model\ResourceModel\ShirtSleeves');
        $this->_map['fields']['shirtsleeves_id'] = 'main_table.shirtsleeves_id';
    }

}
