<?php

/**
 * @author Akshay Shelke    
 * Copyright © 2018 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Shirt\Shirtfabricwomen\Model\ResourceModel\ShirtAccentCollarstyle;

use \Shirt\Shirtfabricwomen\Model\ResourceModel\AbstractCollection;

class Collection extends AbstractCollection {

    protected function _construct() {
        $this->_init('Shirt\Shirtfabricwomen\Model\ShirtAccentCollarstyle', 'Shirt\Shirtfabricwomen\Model\ResourceModel\ShirtAccentCollarstyle');
        $this->_map['fields']['collarstyle_id'] = 'main_table.collarstyle_id';
    }

}
