<?php

/**
 * @author Akshay Shelke    
 * Copyright © 2018 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Shirt\Shirtfabricwomen\Model\ResourceModel\ShirtPleats;

use \Shirt\Shirtfabricwomen\Model\ResourceModel\AbstractCollection;

class Collection extends AbstractCollection {

    protected function _construct() {
        $this->_init('Shirt\Shirtfabricwomen\Model\ShirtPleats', 'Shirt\Shirtfabricwomen\Model\ResourceModel\ShirtPleats');
        $this->_map['fields']['shirtpleats_id'] = 'main_table.shirtpleats_id';
    }

}
