<?php

/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Shirt\Shirtfabricwomen\Model\ResourceModel\Shirtfabricwomen;

use \Shirt\Shirtfabricwomen\Model\ResourceModel\AbstractCollection;

class Collection extends AbstractCollection {

    /**
     * @var string
     */
    protected $_idFieldName = 'shirtfabricwomen_id';

    /**
     * Load data for preview flag
     *
     * @var bool
     */
    protected $_previewFlag;

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct() {
        $this->_init('Shirt\Shirtfabricwomen\Model\Shirtfabricwomen', 'Shirt\Shirtfabricwomen\Model\ResourceModel\Shirtfabricwomen');
        $this->_map['fields']['shirtfabricwomen_id'] = 'main_table.shirtfabricwomen_id';
    }

}
