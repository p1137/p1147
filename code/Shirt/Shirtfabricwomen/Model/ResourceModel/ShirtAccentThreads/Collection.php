<?php

/**
 * @author Akshay Shelke    
 * Copyright © 2018 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Shirt\Shirtfabricwomen\Model\ResourceModel\ShirtAccentThreads;

use \Shirt\Shirtfabricwomen\Model\ResourceModel\AbstractCollection;

class Collection extends AbstractCollection {

    protected function _construct() {
        $this->_init('Shirt\Shirtfabricwomen\Model\ShirtAccentThreads', 'Shirt\Shirtfabricwomen\Model\ResourceModel\ShirtAccentThreads');
        $this->_map['fields']['threads_id'] = 'main_table.threads_id';
    }

}
