<?php

/**
 * @author Akshay Shelke    
 * Copyright © 2018 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Shirt\Shirtfabricwomen\Model\ResourceModel\ShirtAccentPlacket;

use \Shirt\Shirtfabricwomen\Model\ResourceModel\AbstractCollection;

class Collection extends AbstractCollection {

    protected function _construct() {
        $this->_init('Shirt\Shirtfabricwomen\Model\ShirtAccentPlacket', 'Shirt\Shirtfabricwomen\Model\ResourceModel\ShirtAccentPlacket');
        $this->_map['fields']['placket_id'] = 'main_table.placket_id';
    }

}
