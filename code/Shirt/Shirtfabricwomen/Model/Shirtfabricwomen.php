<?php

namespace Shirt\Shirtfabricwomen\Model;

class Shirtfabricwomen extends \Magento\Framework\Model\AbstractModel {

    protected function _construct() {
        $this->_init('Shirt\Shirtfabricwomen\Model\ResourceModel\Shirtfabricwomen');
    }

    public function getAvailableStatuses() {


        $availableOptions = ['1' => 'Enable',
            '0' => 'Disable'];

        return $availableOptions;
    }

}
