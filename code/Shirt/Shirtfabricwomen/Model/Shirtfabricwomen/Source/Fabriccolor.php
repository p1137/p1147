<?php

/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Shirt\Shirtfabricwomen\Model\Shirtfabricwomen\Source;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class Status
 */
class Fabriccolor implements OptionSourceInterface {

    /**
     * Get options
     *
     * @return array
     */
    protected function getConnection() {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        return $resource->getConnection();
    }

    public function toOptionArray() {
        $tableName = "fabric_color";
        $query = "Select * FROM " . $tableName . " WHERE status='1'";
        $collection = $this->getConnection()->fetchAll($query);
        $options[] = [
            'label' => "Please Select",
            'value' => "",
        ];
        foreach ($collection as $data) {
            $options[] = [
                'label' => $data['title'],
                'value' => $data['color_id'],
            ];
        }
        return $options;
    }

}
