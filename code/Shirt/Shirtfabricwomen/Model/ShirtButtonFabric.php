<?php

/**
 * @author Akshay Shelke 
 * Copyright © 2018 Magento. All rights reserved. 
 * See COPYING.txt for license details. 
 */

namespace Shirt\Shirtfabricwomen\Model;

class ShirtButtonFabric extends \Magento\Framework\Model\AbstractModel {

    protected function _construct() {
        $this->_init('Shirt\Shirtfabricwomen\Model\ResourceModel\ShirtButtonFabric');
    }

}
