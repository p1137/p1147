<?php

/**
 * @author Dhirajkumar Deore
 * Copyright © 2018 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Shirt\Threadfabricmen\Controller\Adminhtml\Index;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Index extends \Magento\Backend\App\Action {

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
    Context $context, PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * Check the permission to run it
     *
     * @return bool
     */
    protected function _isAllowed() {
        return $this->_authorization->isAllowed('Shirt_Threadfabricmen::shirt_threadfabricmen');
    }

    /**
     * Index action
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute() {

        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Shirt_Threadfabricmen::shirt_threadfabricmen');
        $resultPage->addBreadcrumb(__('Threadfabricmen'), __('Threadfabricmen'));
        $resultPage->addBreadcrumb(__('Shirt Threadfabricmen'), __('Manage Shirt Threadfabricmen Fabrics'));
        $resultPage->getConfig()->getTitle()->prepend(__('Manage Shirt Threadfabricmen Fabrics'));

        return $resultPage;
    }

}
