<?php
/**
* @author Dhirajkumar Deore
* Copyright © 2018 Magento. All rights reserved.
* See COPYING.txt for license details.
*/
namespace Shirt\Threadfabricmen\Model\ResourceModel\Threadfabricmen;

use \Shirt\Threadfabricmen\Model\ResourceModel\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'id';

    /**
     * Load data for preview flag
     *
     * @var bool
     */
    protected $_previewFlag;

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Shirt\Threadfabricmen\Model\Threadfabricmen', 'Shirt\Threadfabricmen\Model\ResourceModel\Threadfabricmen');
        $this->_map['fields']['id'] = 'main_table.id';
    }
}
