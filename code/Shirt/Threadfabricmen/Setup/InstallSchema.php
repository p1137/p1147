<?php

/*
 * Shirt_Threadfabricmen

 * @category   Shirt
 * @package    Shirt_Threadfabricmen
 * @copyright  Copyright (c) 2018 Scott Parsons
 * @license    https://github.com/ScottParsons/module-threadfabricmenuicomponent/blob/master/LICENSE.md
 * @version    1.0.0
 */

namespace Shirt\Threadfabricmen\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class InstallSchema implements InstallSchemaInterface {

    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context) {
        $installer = $setup;
        $installer->startSetup();
        $tableName = $installer->getTable('shirt_threadfabricmen');

        if (!$installer->tableExists('shirt_threadfabricmen')) {
            $table = $installer->getConnection()
                    ->newTable($tableName)
                    ->addColumn(
                            'id', Table::TYPE_INTEGER, null, [
                        'identity' => true,
                        'unsigned' => true,
                        'nullable' => false,
                        'auto_increment' => true,
                        'primary' => true
                            ], 'ID'
                    )
                    ->addColumn(
                            'title', Table::TYPE_TEXT, 255, ['nullable' => false, 'default' => ''], 'Title'
                    )
                    ->addColumn(
                            'price', Table::TYPE_INTEGER, 11, ['nullable' => false, 'default' => 0], 'Price'
                    )                    
                    ->addColumn(
                            'image', Table::TYPE_TEXT, 255, ['nullable' => false, 'default' => ''], 'Image'
                    )
                    ->addColumn(
                            'status', Table::TYPE_SMALLINT, null, ['unsigned' => true, 'nullable' => false, 'default' => '0'], 'Status'
                    )
                    ->addColumn(
                            'created_at', Table::TYPE_TIMESTAMP, null, ['nullable' => false, 'default' => Table::TIMESTAMP_INIT], 'Created At'
                    )
                    ->addColumn(
                    'updated_at', Table::TYPE_TIMESTAMP, null, ['nullable' => false, 'default' => Table::TIMESTAMP_INIT], 'Updated At'
            );
            $installer->getConnection()->createTable($table);
        }
        $installer->endSetup();
    }

}
