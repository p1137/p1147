<?php

namespace Shirt\Tool\Controller\Index;

class getShirtButtonFabric extends \Magento\Framework\App\Action\Action{
	

	public function __construct(
		\Magento\Framework\App\Action\Context $context,
		\Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory	,
        \Shirt\Threadfabricmen\Model\ThreadfabricmenFactory $shirtButtonFabric


		)
	{
		$this->objectManager      = \Magento\Framework\App\ObjectManager::getInstance();
		$this->resource           = $this->objectManager->get('Magento\Framework\App\ResourceConnection');
		$this->connection         = $this->resource->getConnection();
		$this->resultJsonFactory  = $resultJsonFactory;
        $this->_shirtButtonFabric = $shirtButtonFabric;  //shirt_button_fabric

		return parent::__construct($context);
	}

	public function execute()
	{	
        return $this->getShirtButtonFabricAction();
	}

	
	public function getShirtButtonFabricAction() {
        // $basePath = $this->getBasePath();

        // $basePath = "http://192.168.2.104/Magentop/";

        $storeManager = $this->objectManager->get('\Magento\Store\Model\StoreManagerInterface');
        $basePath = $storeManager->getStore()->getBaseUrl();  // get base url...
	$mediaURL = $storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);

        $model = $this->_shirtButtonFabric->create()->getCollection();
        $rows = $model->getData();

        // $sql = " SELECT * FROM shirt_button_fabric ";
        // $rows = $this->connection->fetchAll($sql);
        $fabrics = array();
        foreach ($rows as $fabric) {
            $id = $fabric['id'];
            $name = $fabric['title'];
	    $price = $fabric['price'];
            $thumb = $fabric['image'];
            $real_img = $fabric['image'];
            $shirt_type_name = "Cotton";
            $fabrics[] = array('id' => $id, 'price' => $price, 'name' => $name, 'real_img' => $mediaURL 
                        . $real_img, 'img' => $mediaURL. $thumb);
        }

        // echo json_encode($fabrics, true);
        // $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($fabrics)); X1
        return $this->resultJsonFactory->create()->setData($fabrics);   // X2
    }
}
