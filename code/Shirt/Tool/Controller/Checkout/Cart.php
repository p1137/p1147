<?php
namespace Shirt\Tool\Controller\Checkout;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\RequestInterface;
  
class Cart extends \Magento\Framework\App\Action\Action
{
    protected $_resultPageFactory;
    protected $_cart;
    protected $_productRepositoryInterface;
    protected $_url;
    protected $_responseFactory;
    protected $_logger;
 
    public function __construct(
        Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Checkout\Model\Cart $cart,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepositoryInterface,
        \Magento\Framework\App\ResponseFactory $responseFactory,
        \Psr\Log\LoggerInterface $logger
        )
    {
        $this->_resultPageFactory = $resultPageFactory;
        $this->_cart = $cart;
        $this->_productRepositoryInterface = $productRepositoryInterface;
        $this->_responseFactory = $responseFactory;
        $this->_url = $context->getUrl();
        $this->_logger = $logger;
        $this->_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->resource = $this->_objectManager->get('Magento\Framework\App\ResourceConnection');
        $this->connection = $this->resource->getConnection();

        parent::__construct($context);
    }

    
    public function execute()
    {
        // echo "inside overrided cart"; die;
        // $post = $this->getArray($this->getRequest()->getParam('style_data'));
        if( $this->getRequest()->getParam('style_data')!= '' ){
            
            $productid = 14;
        
            $_product = $this->_productRepositoryInterface->getById($productid);

            $postStyle = $this->getRequest()->getParam('style_data');
            $postSize = $this->getRequest()->getParam('size_Data_Arr');

            $post = json_encode(array('style' =>$postStyle, 'size' =>$postSize));

            $storeId = $this->_objectManager->get('Magento\Store\Model\StoreManagerInterface')->getStore()->getId();

            /* @var \Magento\Checkout\Model\Cart $cart */
            $cart = $this->_objectManager->get('\Magento\Checkout\Model\Cart');

            $price = $this->getRequest()->getParam('price');
            
            $params = array (
                'product'   => $_product->getId(),
                'price'     => $price,
                'qty'       => $this->getRequest()->getParam('qty'),
                'json'      => array('style' =>$postStyle, 'size' =>$postSize)
            );

            $this->_cart->addProduct($_product,$params);
            $data = "fail";
            if($this->_cart->save()){
                $this->messageManager->addSuccessMessage('You added product to your shopping cart.');
                $data = "done";    
            }

            echo $data;
        }
    }
}