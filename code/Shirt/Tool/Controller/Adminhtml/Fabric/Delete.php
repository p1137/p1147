<?php
/**
* @author Dhirajkumar Deore    
* Copyright © 2018 Magento. All rights reserved.
* See COPYING.txt for license details.
*/
namespace Shirt\Tool\Controller\Adminhtml\Fabric;

class Delete extends \Magento\Backend\App\Action
{
    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Shirt_Tool::shirt_tool');
    }

    /**
     * Delete action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        // check if we know what should be deleted
        $id = $this->getRequest()->getParam('fabric_id');
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($id) {
            $title = "";
            try {
                // init model and delete
                $model = $this->_objectManager->create('Shirt\Tool\Model\ShirtShirtfabric');
                $model->load($id);
                $title = $model->getTitle();

                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                $directory = $objectManager->get('\Magento\Framework\Filesystem\DirectoryList');
                $path = $directory->getPath('media').'shirt-tool/';
                $rootPath1  =  $path.$model->getdisplay_fabric_thumb();
                $rootPath2  =  $path.$model->getfabric_thumb();
                $rootPath3  =  $path.$model->getfabric_large_image();
                
                //  checking if file exists in folder if exist then delete.
                if(is_file($rootPath1)){
                  unlink($rootPath1);
                }

                if(is_file($rootPath2)){
                  unlink($rootPath2);
                }

                if(is_file($rootPath3)){
                  unlink($rootPath3);
                }


                $model->delete();
                // display success message
                $this->messageManager->addSuccess(__('The Image Fabric has been deleted.'));
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
                // go back to edit form
                return $resultRedirect->setPath('*/*/edit', ['fabric_id' => $id]);
            }
        }
        
        // display error message
        $this->messageManager->addError(__('We can\'t find a suit tool to delete.'));
        // go to grid
        return $resultRedirect->setPath('*/*/');
    }
}
