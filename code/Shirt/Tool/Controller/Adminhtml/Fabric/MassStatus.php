<?php
/**
* @author Dhirajkumar Deore    
* Copyright © 2018 Magento. All rights reserved.
* See COPYING.txt for license details.
*/
namespace Shirt\Tool\Controller\Adminhtml\Fabric;

use Magento\Framework\Controller\ResultFactory;
use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;
use Shirt\Tool\Model\ResourceModel\ShirtShirtfabric\CollectionFactory;
use Shirt\Tool\Model\ShirtShirtfabricFactory;


/**
 * Class MassDisable
 */
class MassStatus extends \Magento\Backend\App\Action
{
    /**
     * @var Filter
     */
    protected $filter;

    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @param Context $context
     * @param Filter $filter
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(Context $context, Filter $filter, CollectionFactory $collectionFactory,
                                ShirtShirtfabricFactory $shirtShirtfabricFactory)
    {
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        $this->_shirtShirtfabricFactory = $shirtShirtfabricFactory;
        parent::__construct($context);
    }
    
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Shirt_Tool::shirt_tool');
    }
    
    /**
     * Execute action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     * @throws \Magento\Framework\Exception\LocalizedException|\Exception
     */
    public function execute()
    {
        $status = $this->getRequest()->getParam('status');

        try{
            $collection = $this->filter->getCollection($this->collectionFactory->create());

            foreach ($collection as $item) {
                $item->setStatus($status);
                $item->save();
            }
            $this->messageManager->addSuccess(__('A total of %1 record(s) have been changed.', $collection->getSize()));
        }catch(\Exception $e){
            $this->messageManager->addError($e->getMessage());
        }

        // $post = $this->getRequest()->getParams();
        // try{
        //     $collection = $this->_shirtShirtfabricFactory->create()->getCollection();
        //     if(isset($post['selected']))
        //         $collection->addFieldToFilter('fabric_id', $post['selected']);

        //     foreach($collection as $item){
        //         $item->setStatus($status);
        //         $item->save();
        //     }
        //     $this->messageManager->addSuccess(__('A total of %1 record(s) have been changed.', $collection->getSize()));
        // } catch (\Exception $e) {
        //     $this->messageManager->addError($e->getMessage());
        // }

        
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('*/*/');
    }
}
