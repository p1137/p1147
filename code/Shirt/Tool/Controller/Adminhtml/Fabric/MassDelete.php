<?php
namespace Shirt\Tool\Controller\Adminhtml\Fabric;
use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;
use Shirt\Tool\Model\ResourceModel\ShirtShirtfabric\CollectionFactory;
use Magento\Framework\Controller\ResultFactory;
use Shirt\Tool\Model\ShirtShirtfabricFactory;
/**
 * Class MassDisable
 */
class MassDelete  extends \Magento\Backend\App\Action
{
    /**
     * @var Filter
     */
    // protected $filter= 'fabric_id';
    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;
    /**
     * @param Context $context
     * @param Filter $filter
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(Context $context, 
                    Filter $filter, 
                    CollectionFactory $collectionFactory,
                    ShirtShirtfabricFactory $shirtAdminFactory
        )
    {
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        $this->_shirtAdminFactory = $shirtAdminFactory;
        
        parent::__construct($context);
    }
    /**
     * Execute action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     * @throws \Magento\Framework\Exception\LocalizedException|\Exception
     */
    public function execute()
    {      
        $collection = $this->filter->getCollection($this->collectionFactory->create());
        
        $collectionSize = $collection->getSize();
        
        foreach ($collection as $item) {
                
                
                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                $directory = $objectManager->get('\Magento\Framework\Filesystem\DirectoryList');

                $rootPath1  =  $directory->getPath('media').'shirt-tool/'.$item->getdisplay_fabric_thumb();
                $rootPath2  =  $directory->getPath('media').'shirt-tool/'.$item->getfabric_thumb();
                $rootPath3  =  $directory->getPath('media').'shirt-tool/'.$item->getfabric_large_image();
                
                //  checking if file exists in folder if exist then delete.
                if(is_file($rootPath1)){
                  unlink($rootPath1);
                }

                if(is_file($rootPath2)){
                  unlink($rootPath2);
                }

                if(is_file($rootPath3)){
                  unlink($rootPath3);
                }

            $item->delete();
        }
        
        $this->messageManager->addSuccess(__('A total of %1 record(s) have been deleted.', $collectionSize));

        // $post = $this->getRequest()->getParams();
         
        // try{
        //     $collection = $this->_shirtAdminFactory->create()->getCollection();
           
        //     if(isset($post['selected']))
        //         $collection->addFieldToFilter('fabric_id', $post['selected']);

        //     foreach($collection as $item){
        //         $item->delete();
        //     }
        //     $this->messageManager->addSuccess(__('A total of %1 record(s) have been deleted.', $collection->getSize()));
        // } catch (\Exception $e) {
        //     $this->messageManager->addError($e->getMessage());
        // }
        
      

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('*/*/');
    }
}