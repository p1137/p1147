<?php

/**
 * @author Dhirajkumar Deore    
 * Copyright © 2018 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Shirt\Tool\Block;

use Magento\Framework\View\Element\Template;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\ObjectManagerInterface;

class Tool extends Template {

    protected $scopeConfig;
    protected $collectionFactory;
    protected $objectManager;
    protected $_logo;

    public function __construct(
    \Magento\Theme\Block\Html\Header\Logo $logo, \Magento\Framework\View\Element\Template\Context $context, \Shirt\Tool\Model\ResourceModel\Tool\CollectionFactory $collectionFactory, ObjectManagerInterface $objectManager
    ) {
        $this->_logo = $logo;
        $this->scopeConfig = $context->getScopeConfig();
        $this->collectionFactory = $collectionFactory;
        $this->objectManager = $objectManager;

        parent::__construct($context);
    }

    public function getFrontTool() {


        $collection = $this->collectionFactory->create()->addFieldToFilter('status', 1);

        /*
         * cehck for arguments,provided in block call
         */
        if ($ids_list = $this->getToolBlockArguments()) {
            $collection->addFilter('shirt_tool_id', ['in' => $ids_list], 'public');
        }

        return $collection;
    }

    public function getToolBlockArguments() {

        $list = $this->getToolList();

        $listArray = [];

        if ($list != '') {
            $listArray = explode(',', $list);
        }

        return $listArray;
    }

    public function getMediaDirectoryUrl() {

        $media_dir = $this->objectManager->get('Magento\Store\Model\StoreManagerInterface')
                ->getStore()
                ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);

        return $media_dir;
    }

    /**
     * Get logo image URL
     *
     * @return string
     */
    public function getLogoSrc() {
        return $this->_logo->getLogoSrc();
    }

    /**
     * Get logo text
     *
     * @return string
     */
    public function getLogoAlt() {
        return $this->_logo->getLogoAlt();
    }

    /**
     * Get logo width
     *
     * @return int
     */
    public function getLogoWidth() {
        return $this->_logo->getLogoWidth();
    }

    /**
     * Get logo height
     *
     * @return int
     */
    public function getLogoHeight() {
        return $this->_logo->getLogoHeight();
    }

}
