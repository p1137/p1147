<?php
namespace Shirt\Tool\Block;

class Shirt extends \Magento\Framework\View\Element\Template {

    protected $_scopeConfig;
    protected $customerSession;

    public function __construct(
    \Magento\Framework\View\Element\Template\Context $context, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Magento\Customer\Model\Session $customerSession, \Wholesaler\Mgmt\Model\MeasurementFactory $measurementfactory, array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_scopeConfig = $scopeConfig;
        $this->_customerSession = $customerSession;
        $this->measurementfactory = $measurementfactory;
    }

    public function getProductId() {
        $myvalue = $this->_scopeConfig->getValue('toolConfiguration/general/men_shirt_tool_product', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        return $myvalue;
    }

    public function isWholesaler() {
//        $customerGroup = $this->_customerSession->getCustomer()->getGroupId();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerSession = $objectManager->create('Magento\Customer\Model\Session');
        $customerGroup = $customerSession->getCustomer()->getGroupId();

        if ($customerGroup == '2') {

            return "wholesaler";
        } else {
            return "customer";
        }
    }

    public function getBodyMeasurements() {

        try {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $customerSession = $objectManager->create('Magento\Customer\Model\Session');
   
            $cust_id = $customerSession->getCustomer()->getId();
            $sample = $this->measurementfactory->create();
            $measurementArray = array();

            $collection = $sample->getCollection()->addFieldToFilter('customer_id', array('eq' => $cust_id));
            foreach ($collection as $item) {
                $cust_data = $item->getData();
                $measurementArray['measurement_id'] = $cust_data['measurement_id'];
                $measurementArray['height'] = $cust_data['height'];
                $measurementArray['frontlength'] = $cust_data['frontlength'];
                $measurementArray['backlength'] = $cust_data['backlength'];
                $measurementArray['shoulder_width'] = $cust_data['shoulder_width'];
                $measurementArray['bust'] = $cust_data['bust'];
                $measurementArray['body_weight'] = $cust_data['body_weight'];
                $measurementArray['mid_waist'] = $cust_data['mid_waist'];
                $measurementArray['sleeve_length'] = $cust_data['sleeve_length'];
                $measurementArray['sleeve'] = $cust_data['sleeve'];
                $measurementArray['hip'] = $cust_data['hip'];
                $measurementArray['check'] = 'yes';
            }
            if ($measurementArray != NULL) {
                return $measurementArray;
            } else {
                $measurementArray['measurement_id'] = '';
                $measurementArray['height'] = '';
                $measurementArray['frontlength'] = '';
                $measurementArray['backlength'] = '';
                $measurementArray['shoulder_width'] = '';
                $measurementArray['bust'] = '';
                $measurementArray['body_weight'] = '';
                $measurementArray['mid_waist'] = '';
                $measurementArray['sleeve_length'] = '';
                $measurementArray['sleeve'] = '';
                $measurementArray['hip'] = '';
                $measurementArray['check'] = 'no';
                return $measurementArray;
            }
        } catch (\Exception $e) {
            return __('You have not set your body measurements.');
        }
    }

}

