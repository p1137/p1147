<?php

namespace Shirt\Tool\Block;

class Shirt extends \Magento\Framework\View\Element\Template {

    protected $_scopeConfig;
    protected $customerSession;

    public function __construct(
    \Magento\Framework\View\Element\Template\Context $context, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Magento\Customer\Model\Session $customerSession, \Wholesaler\Mgmt\Model\MeasurementFactory $measurementfactory, array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_scopeConfig = $scopeConfig;
        $this->_customerSession = $customerSession;
        $this->measurementfactory = $measurementfactory;
    }

    public function getProductId() {
        $myvalue = $this->_scopeConfig->getValue('toolConfiguration/general/men_shirt_tool_product', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        return $myvalue;
    }

    public function isWholesaler() {

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerSession = $objectManager->create('Magento\Customer\Model\Session');
        $customerGroup = $customerSession->getCustomer()->getGroupId();

        if ($customerGroup == '2') {

            return 1;
        } else {
            return 0;
        }

    }

    public function getLastOrder(){

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerSession = $objectManager->create('Magento\Customer\Model\Session');
        $cust_id = $customerSession->getCustomer()->getId();

        $orderDatamodel = $objectManager->get('Magento\Sales\Model\Order')->getCollection()->addFieldToFilter('customer_id',$cust_id)->getLastItem();
        $orderId   =   $orderDatamodel->getId();
        $order = $objectManager->create('\Magento\Sales\Model\Order')->load($orderId);
        $orderItems = $order->getAllItems();
        $orderData = array();

        foreach ($orderItems as $item) { 
            if($item->getJson()!=''){
                $json = json_decode($item->getJson(),true);
                if(array_key_exists('rtw_measurement', $json)){

                    if($json['rtw_measurement']['product_type']=='Shirt'){
                        $shirtLastData = $json;
                        $orderData['Shirt'] = $shirtLastData; 
                    }
                    if($json['rtw_measurement']['product_type']=='WomenShirt'){
                        $womenshirtLastData = $json;
                        $orderData['WomenShirt'] = $womenshirtLastData; 
                    }
                    if($json['rtw_measurement']['product_type']=='Suit'){
                        $suitLastData = $json;
                        $orderData['Suit'] = $suitLastData; 
                    }
                    if($json['rtw_measurement']['product_type']=='WomenSuit'){
                        $womensuitLastData = $json;
                        $orderData['WomenSuit'] = $womensuitLastData; 
                    }

                }else{

                    if($json['type']=='Shirt'){
                        $shirtLastData = $json;
                        $orderData['Shirt'] = $shirtLastData; 
                    }
                    if($json['type']=='WomenShirt'){
                        $womenshirtLastData = $json;
                        $orderData['WomenShirt'] = $womenshirtLastData; 
                    }
                    if($json['type']=='Suit'){
                        $suitLastData = $json;
                        $orderData['Suit'] = $suitLastData; 
                    }
                    if($json['type']=='WomenSuit'){
                        $womensuitLastData = $json;
                        $orderData['WomenSuit'] = $womensuitLastData; 
                    }
                }
            }

        }
        // if($orderData==''){
        //     $orderData = '';
        // }
        return $orderData;

    }

    public function getBodyMeasurements() {

        try {
            // $cust_id = $this->_customerSession->getCustomer()->getId();

            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $customerSession = $objectManager->create('Magento\Customer\Model\Session');
            $cust_id = $customerSession->getCustomer()->getId();

            $sample = $this->measurementfactory->create();
            $measurementArray = array();

            $collection = $sample->getCollection()->addFieldToFilter('customer_id', array('eq' => $cust_id));
            foreach ($collection as $item) {
                $cust_data = $item->getData();

                $measurementArray['measurement_id'] = $cust_data['measurement_id'];
                $measurementArray['customer_id'] = $cust_id;
                $measurementArray['json'] = $cust_data['json'];

            }
            if ($measurementArray != NULL) {
                return $measurementArray;
            } else {
                $measurementArray['measurement_id'] = '';
                $measurementArray['customer_id'] = $cust_id;
                $measurementArray['json'] = '';
                return $measurementArray;
            }
        } catch (\Exception $e) {
            return __('You have not set your body measurements.');
        }
    }

    public function getBodyMeasurementsById($cust_id) {

        try {
            // $cust_id = $this->_customerSession->getCustomer()->getId();
            $sample = $this->measurementfactory->create();
            $measurementArray = array();

            $collection = $sample->getCollection()->addFieldToFilter('customer_id', array('eq' => $cust_id));
            foreach ($collection as $item) {
                $cust_data = $item->getData();

                $measurementArray['measurement_id'] = $cust_data['measurement_id'];
                $measurementArray['customer_id'] = $cust_id;
                $measurementArray['json'] = $cust_data['json'];

            }
            if ($measurementArray != NULL) {
                return $measurementArray;
            } else {
                $measurementArray['measurement_id'] = '';
                $measurementArray['customer_id'] = $cust_id;
                $measurementArray['json'] = '';
                return $measurementArray;
            }
        } catch (\Exception $e) {
            return __('You have not set your body measurements.');
        }
    }


}
