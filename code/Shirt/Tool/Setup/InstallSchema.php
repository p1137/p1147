<?php

/**
 * @author Dhirajkumar Deore    
 * Copyright © 2018 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Shirt\Tool\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface {

    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context) {


        $installer = $setup;
        $installer->startSetup();
        /////////////////////////////////////////////////////////////////////////////
        /**
         * Create table 'shirt_tool'
         */
        $table = $installer->getConnection()->newTable($installer->getTable('shirt_tool'))
                ->addColumn(
                        'shirt_tool_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, 11, ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true], 'Suit_ ID'
                )
                ->addColumn(
                        'title', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'Title'
                )
                ->addColumn(
                        'class', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'Class'
                )
                ->addColumn(
                        'suit_upperrightmask', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'Upper Right Mask Image'
                )
                ->addColumn(
                        'main_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'Main Image'
                )
                ->addColumn(
                        'objimage', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'Object File'
                )->addColumn(
                        'mlt_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'MLT File'
                )
                ->addColumn(
                        'status', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, 6, [], 'Status'
                )
                ->addColumn(
                        'created_time', \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP, null, ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT], 'Creation Time'
                )
                ->addColumn(
                'update_time', \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP, null, ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE], 'Update Time'
        );
        $installer->getConnection()->createTable($table);

        unset($table);


        //table admin_shirt_fabric
        $table = $installer->getConnection()->newTable($installer->getTable('admin_shirt_fabric'))
                ->addColumn(
                        'fabric_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, 11, ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true], 'Fabric ID'
                )
                ->addColumn(
                        'display_fabric_thumb', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'Display Fabric Thumb Image'
                )
                ->addColumn(
                        'fabric_thumb', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'Fabric Thumb Image'
                )
                ->addColumn(
                        'fabric_large_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'Fabric Large Image'
                )
                ->addColumn(
                        'btn_color_id', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, 6, [], 'Button Color Id'
                )
                ->addColumn(
                        'fabric_material_id', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, 6, [], 'Fabric Material Id'
                )
                ->addColumn(
                        'fabric_pattern_id', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, 6, [], 'Fabric Pattern Id'
                )
                ->addColumn(
                        'fabric_season_id', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, 6, [], 'Fabric Season Id'
                )
                ->addColumn(
                        'fabric_color_id', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, 6, [], 'Fabric Color Id'
                )
                ->addColumn(
                        'fabric_category_id', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, 6, [], 'Fabric Category Id'
                )
                ->addColumn(
                        'title', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 100, [], 'Title'
                )
                ->addColumn(
                        'price', \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL, '10,2', [], 'Price'
                )->addColumn(
                        'type', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 100, [], 'Type'
                )
                ->addColumn(
                        'status', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, 6, [], 'Status'
                )
                ->addColumn(
                        'created_date', \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP, null, ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT], 'Created Date'
                )
                ->addColumn(
                'modified_date', \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP, null, ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE], 'Modified Date'
        );
        $installer->getConnection()->createTable($table);

        unset($table);


        // table fabric_category
        $table = $installer->getConnection()->newTable($installer->getTable('fabric_category'))
                        ->addColumn(
                                'category_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, 11, ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true], 'Categ ID'
                        )->addColumn(
                        'title', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 50, [], 'Title'
                )->addColumn(
                        'class', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 50, [], 'Class'
                )->addColumn(
                'status', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, 6, [], 'Status'
        );
        $installer->getConnection()->createTable($table);

        unset($table);
        // table fabric_color
        $table = $installer->getConnection()->newTable($installer->getTable('fabric_color'))
                        ->addColumn(
                                'color_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true], 'Color ID'
                        )->addColumn(
                        'title', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 50, [], 'Title'
                )->addColumn(
                'status', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, 6, [], 'Status'
        );
        $installer->getConnection()->createTable($table);

        unset($table);
        // table fabric_material
        $table = $installer->getConnection()->newTable($installer->getTable('fabric_material'))
                        ->addColumn(
                                'material_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true], 'Material ID'
                        )->addColumn(
                        'title', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 50, [], 'Title'
                )->addColumn(
                'status', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, 6, [], 'Status'
        );
        $installer->getConnection()->createTable($table);

        unset($table);
        // table fabric_material
        $table = $installer->getConnection()->newTable($installer->getTable('fabric_pattern'))
                        ->addColumn(
                                'pattern_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true], 'Pattern ID'
                        )->addColumn(
                        'title', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 50, [], 'Title'
                )->addColumn(
                'status', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, 6, [], 'Status'
        );
        $installer->getConnection()->createTable($table);

        unset($table);
        // table fabric_season
        $table = $installer->getConnection()->newTable($installer->getTable('fabric_season'))
                        ->addColumn(
                                'season_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true], 'Season ID'
                        )->addColumn(
                        'title', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 50, [], 'Title'
                )->addColumn(
                'status', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, 6, [], 'Status'
        );
        $installer->getConnection()->createTable($table);

        unset($table);
        // table shirtbottom
        $table = $installer->getConnection()->newTable($installer->getTable('shirtbottom'))
                        ->addColumn(
                                'shirtbottom_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true], 'Shirt Bottom ID'
                        )->addColumn(
                        'title', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 50, [], 'Title'
                )->addColumn(
                        'class', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 50, [], 'Class'
                )->addColumn(
                        'price', \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL, '10,2', [], 'Price'
                )->addColumn(
                        'gender', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, 1, [], 'Gender'
                )->addColumn(
                        'view1_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 50, [], 'View Image 1'
                )->addColumn(
                        'view2_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 50, [], 'View Image 2'
                )->addColumn(
                        'view3_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 50, [], 'View Image 3'
                )->addColumn(
                'status', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, 6, [], 'Status'
        );
        $installer->getConnection()->createTable($table);

        unset($table);
        // table shirtbottom
        $table = $installer->getConnection()->newTable($installer->getTable('shirtcategory'))
                        ->addColumn(
                                'shirtcategory_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true], 'Shirt Category ID'
                        )->addColumn(
                        'title', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 50, [], 'Title'
                )->addColumn(
                        'class', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 50, [], 'Class'
                )->addColumn(
                'status', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, 6, [], 'Status'
        );
        $installer->getConnection()->createTable($table);

        unset($table);
        // table shirtbottom
        $table = $installer->getConnection()->newTable($installer->getTable('shirtcolor'))
                        ->addColumn(
                                'shirtcolor_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true], 'Shirt Category ID'
                        )->addColumn(
                        'title', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 50, [], 'Title'
                )->addColumn(
                'status', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, 6, [], 'Status'
        );
        $installer->getConnection()->createTable($table);

        // for table shirtcuff
        unset($table);
        $table = $installer->getConnection()->newTable($installer->getTable('shirtcuff'))
                ->addColumn(
                        'shirtcuff_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true], 'Shirt Bottom ID'
                )
                ->addColumn(
                        'title', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 50, [], 'Title'
                )
                ->addColumn(
                        'class', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 100, [], 'Class'
                )
                ->addColumn(
                        'price', \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL, '10,2', [], 'Price'
                )
                ->addColumn(
                        'type', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, 6, [], 'Type'
                )
                ->addColumn(
                        'gender', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, 6, [], 'Gender'
                )
                ->addColumn(
                        'glow_front_left_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'Glow Front Left Image'
                )
                ->addColumn(
                        'mask_front_left_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'Mask Front Left Image'
                )
                ->addColumn(
                        'highlighted_front_left_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'Highlighted Front Left Image'
                )
                ->addColumn(
                        'glow_front_right_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'Glow Front Right Image'
                )
                ->addColumn(
                        'mask_front_right_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'Mask Front Right Image'
                )
                ->addColumn(
                        'highlighted_front_right_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'highlighted_front_right_image'
                )
                ->addColumn(
                        'glow_front_opencuff_left_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'glow_front_opencuff_left_image'
                )
                ->addColumn(
                        'mask_front_opencuff_left_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'mask_front_opencuff_left_image'
                )
                ->addColumn(
                        'highlighted_front_opencuff_left_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'highlighted_front_opencuff_left_image'
                )
                ->addColumn(
                        'glow_front_opencuff_right_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'glow_front_opencuff_right_image'
                )
                ->addColumn(
                        'mask_front_opencuff_right_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'mask_front_opencuff_right_image'
                )
                ->addColumn(
                        'highlighted_front_opencuff_right_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'highlighted_front_opencuff_right_image'
                )
                ->addColumn(
                        'glow_side_left_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'glow_side_left_image'
                )
                ->addColumn(
                        'mask_side_left_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'mask_side_left_image'
                )
                ->addColumn(
                        'highlighted_side_left_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'highlighted_side_left_image'
                )
                ->addColumn(
                        'glow_side_right_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'glow_side_right_image'
                )
                ->addColumn(
                        'mask_side_right_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'mask_side_right_image'
                )
                ->addColumn(
                        'highlighted_side_right_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'highlighted_side_right_image'
                )
                ->addColumn(
                        'glow_side_opencuff_left_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'glow_side_opencuff_left_image'
                )
                ->addColumn(
                        'mask_side_opencuff_left_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'mask_side_opencuff_left_image'
                )
                ->addColumn(
                        'highlighted_side_opencuff_left_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'highlighted_side_opencuff_left_image'
                )
                ->addColumn(
                        'glow_side_opencuff_right_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'glow_side_opencuff_right_image'
                )
                ->addColumn(
                        'mask_side_opencuff_right_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'mask_side_opencuff_right_image'
                )
                ->addColumn(
                        'highlighted_side_opencuff_right_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'highlighted_side_opencuff_right_image'
                )
                ->addColumn(
                        'glow_back_left_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'glow_back_left_image'
                )
                ->addColumn(
                        'mask_back_left_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'mask_back_left_image'
                )
                ->addColumn(
                        'highlighted_back_left_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'highlighted_back_left_image'
                )
                ->addColumn(
                        'glow_back_right_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'glow_back_right_image'
                )
                ->addColumn(
                        'mask_back_right_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'mask_back_right_image'
                )
                ->addColumn(
                        'highlighted_back_right_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'highlighted_back_right_image'
                )
                ->addColumn(
                        'glow_back_opencuff_left_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'glow_back_opencuff_left_image'
                )
                ->addColumn(
                        'mask_back_opencuff_left_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'mask_back_opencuff_left_image'
                )
                ->addColumn(
                        'highlighted_back_opencuff_left_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'highlighted_back_opencuff_left_image'
                )
                ->addColumn(
                        'glow_back_opencuff_right_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'glow_back_opencuff_right_image'
                )
                ->addColumn(
                        'mask_back_opencuff_right_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'mask_back_opencuff_right_image'
                )
                ->addColumn(
                        'highlighted_back_opencuff_right_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'highlighted_back_opencuff_right_image'
                )
                ->addColumn(
                        'glow_fold_main_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'glow_fold_main_image'
                )
                ->addColumn(
                        'mask_fold_main_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'mask_fold_main_image'
                )
                ->addColumn(
                        'highlighted_fold_main_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'highlighted_fold_main_image'
                )
                ->addColumn(
                        'glow_fold_inner_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'glow_fold_inner_image'
                )
                ->addColumn(
                        'mask_fold_inner_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'mask_fold_inner_image'
                )
                ->addColumn(
                        'highlighted_fold_inner_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'highlighted_fold_inner_image'
                )
                ->addColumn(
                        'back_button_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'back_button_image'
                )
                ->addColumn(
                        'fold_button_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'fold_button_image'
                )
                ->addColumn(
                        'back_thread_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'back_thread_image'
                )
                ->addColumn(
                        'fold_thread_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'fold_thread_image'
                )
                ->addColumn(
                'status', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, 6, [], 'Status'
        );
        $installer->getConnection()->createTable($table);

        //for table shirtfit
        unset($table);
        $table = $installer->getConnection()->newTable($installer->getTable('shirtfit'))
                ->addColumn(
                        'shirtfit_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true], 'Shirt Fit ID'
                )
                ->addColumn(
                        'title', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 100, [], 'Title'
                )
                ->addColumn(
                        'class', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 100, [], 'Class'
                )
                ->addColumn(
                        'price', \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL, '10,2', [], 'Price'
                )
                ->addColumn(
                        'gender', \Magento\Framework\DB\Ddl\Table::TYPE_BOOLEAN, 2, [], 'Gender'
                )
                ->addColumn(
                        'glow_front_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'glow_front_image'
                )
                ->addColumn(
                        'mask_front_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'mask_front_image'
                )
                ->addColumn(
                        'highlighted_front_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'highlighted_front_image'
                )
                ->addColumn(
                        'glow_side_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'glow_side_image'
                )
                ->addColumn(
                        'mask_side_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'mask_side_image'
                )
                ->addColumn(
                        'highlighted_side_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'highlighted_side_image'
                )
                ->addColumn(
                        'glow_back_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'glow_back_image'
                )
                ->addColumn(
                        'mask_back_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'mask_back_image'
                )
                ->addColumn(
                        'highlighted_back_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'highlighted_back_image'
                )
                ->addColumn(
                        'glow_fold_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'glow_fold_image'
                )
                ->addColumn(
                        'mask_fold_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'mask_fold_image'
                )
                ->addColumn(
                        'highlighted_fold_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'highlighted_fold_image'
                )
                ->addColumn(
                        'glow_zoom_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'glow_zoom_image'
                )
                ->addColumn(
                        'mask_zoom_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'mask_zoom_image'
                )
                ->addColumn(
                        'highlighted_zoom_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'highlighted_zoom_image'
                )
                ->addColumn(
                'status', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, 6, [], 'Status'
        );
        $installer->getConnection()->createTable($table);

        //for table shirtmaterial
        unset($table);
        $table = $installer->getConnection()->newTable($installer->getTable('shirtmaterial'))
                ->addColumn(
                        'shirtmaterial_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true], 'Shirt Material ID'
                )
                ->addColumn(
                        'title', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 100, [], 'Title'
                )
                ->addColumn(
                'status', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, 6, [], 'Status'
        );
        $installer->getConnection()->createTable($table);

        unset($table);
        // for table shirtpattern
        $table = $installer->getConnection()->newTable($installer->getTable('shirtpattern'))
                ->addColumn(
                        'shirtpattern_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true], 'Shirt Pattern ID'
                )
                ->addColumn(
                        'title', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 100, [], 'Title'
                )
                ->addColumn(
                'status', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, 6, [], 'Status'
        );
        $installer->getConnection()->createTable($table);

        unset($table);
        // table shirtplackets
        $table = $installer->getConnection()->newTable($installer->getTable('shirtplackets'))
                ->addColumn(
                        'shirtplackets_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true], 'Shirt Fit ID'
                )
                ->addColumn(
                        'title', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 100, [], 'Title'
                )
                ->addColumn(
                        'class', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 100, [], 'Class'
                )
                ->addColumn(
                        'price', \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL, '10,2', [], 'Price'
                )
                ->addColumn(
                        'thumb', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 100, [], 'Price'
                )
                ->addColumn(
                        'glow_front_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'glow_front_image'
                )
                ->addColumn(
                        'mask_front_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'mask_front_image'
                )
                ->addColumn(
                        'highlighted_front_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'highlighted_front_image'
                )
                ->addColumn(
                        'glow_side_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'glow_side_image'
                )
                ->addColumn(
                        'mask_side_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'mask_side_image'
                )
                ->addColumn(
                        'highlighted_side_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'highlighted_side_image'
                )
                ->addColumn(
                        'glow_fold_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'glow_fold_image'
                )
                ->addColumn(
                        'mask_fold_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'mask_fold_image'
                )
                ->addColumn(
                        'highlighted_fold_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'highlighted_fold_image'
                )
                ->addColumn(
                'status', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, 6, [], 'Status'
        );
        $installer->getConnection()->createTable($table);

        unset($table);
        //table for shirtpleats
        $table = $installer->getConnection()->newTable($installer->getTable('shirtpleats'))
                ->addColumn(
                        'shirtpleats_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true], 'Shirt Fit ID'
                )
                ->addColumn(
                        'title', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 100, [], 'Title'
                )
                ->addColumn(
                        'class', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 100, [], 'Class'
                )
                ->addColumn(
                        'price', \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL, '10,2', [], 'Price'
                )
                ->addColumn(
                        'image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'Image'
                )
                ->addColumn(
                'status', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, 6, [], 'Status'
        );
        $installer->getConnection()->createTable($table);

        unset($table);
        // table for shirtpocket
        $table = $installer->getConnection()->newTable($installer->getTable('shirtpocket'))
                ->addColumn(
                        'shirtpocket_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true], 'Shirt Pocket ID'
                )
                ->addColumn(
                        'title', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 100, [], 'Title'
                )
                ->addColumn(
                        'class', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 100, [], 'Class'
                )
                ->addColumn(
                        'price', \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL, '10,2', [], 'Price'
                )
                ->addColumn(
                        'type', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, 6, [], 'Price'
                )
                ->addColumn(
                        'thumb', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 100, [], 'thumb'
                )
                ->addColumn(
                        'glow_front_left_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'glow_front_left_image'
                )
                ->addColumn(
                        'mask_front_left_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'mask_front_left_image'
                )
                ->addColumn(
                        'highlighted_front_left_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'highlighted_front_left_image'
                )
                ->addColumn(
                        'glow_front_right_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'glow_front_right_image'
                )
                ->addColumn(
                        'mask_front_right_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'mask_front_right_image'
                )
                ->addColumn(
                        'highlighted_front_right_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'highlighted_front_right_image'
                )
                ->addColumn(
                        'glow_side_left_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'glow_side_left_image'
                )
                ->addColumn(
                        'mask_side_left_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'mask_side_left_image'
                )
                ->addColumn(
                        'highlighted_side_left_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'highlighted_side_left_image'
                )
                ->addColumn(
                        'glow_side_right_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'glow_side_right_image'
                )
                ->addColumn(
                        'mask_side_right_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'mask_side_right_image'
                )
                ->addColumn(
                        'highlighted_side_right_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'highlighted_side_right_image'
                )
                ->addColumn(
                        'glow_fold_left_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'glow_fold_left_image'
                )
                ->addColumn(
                        'mask_fold_left_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'mask_fold_left_image'
                )
                ->addColumn(
                        'highlighted_fold_left_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'highlighted_fold_left_image'
                )
                ->addColumn(
                        'glow_fold_right_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'glow_fold_right_image'
                )
                ->addColumn(
                        'mask_fold_right_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'mask_fold_right_image'
                )
                ->addColumn(
                        'highlighted_fold_right_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'highlighted_fold_right_image'
                )
                ->addColumn(
                'status', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, 6, [], 'status'
        );
        $installer->getConnection()->createTable($table);

        unset($table);
        //table for shirtseason
        $table = $installer->getConnection()->newTable($installer->getTable('shirtseason'))
                ->addColumn(
                        'shirtseason_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true], 'Shirt Fit ID'
                )
                ->addColumn(
                        'title', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 100, [], 'Title'
                )
                ->addColumn(
                'status', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, 6, [], 'Status'
        );
        $installer->getConnection()->createTable($table);

        unset($table);
        // shirtsleeves
        $table = $installer->getConnection()->newTable($installer->getTable('shirtsleeves'))
                ->addColumn(
                        'shirtsleeves_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true], 'Shirt Fit ID'
                )
                ->addColumn(
                        'title', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 1100, [], 'Title'
                )
                ->addColumn(
                        'class', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'class'
                )
                ->addColumn(
                        'price', \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL, '10,2', [], 'price'
                )
                ->addColumn(
                        'type', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, 200, [], 'type'
                )
                ->addColumn(
                        'gender', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, 200, [], 'gender'
                )
                ->addColumn(
                        'glow_front_left_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'glow_front_left_image'
                )
                ->addColumn(
                        'mask_front_left_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'mask_front_left_image'
                )
                ->addColumn(
                        'highlighted_front_left_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'highlighted_front_left_image'
                )
                ->addColumn(
                        'glow_front_right_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'glow_front_right_image'
                )
                ->addColumn(
                        'mask_front_right_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'mask_front_right_image'
                )
                ->addColumn(
                        'highlighted_front_right_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'highlighted_front_right_image'
                )
                ->addColumn(
                        'glow_side_left_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'glow_side_left_image'
                )
                ->addColumn(
                        'mask_side_left_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'mask_side_left_image'
                )
                ->addColumn(
                        'highlighted_side_left_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'highlighted_side_left_image'
                )
                ->addColumn(
                        'glow_side_right_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'glow_side_right_image'
                )
                ->addColumn(
                        'mask_side_right_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'mask_side_right_image'
                )
                ->addColumn(
                        'highlighted_side_right_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'highlighted_side_right_image'
                )
                ->addColumn(
                        'glow_back_left_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'glow_back_left_image'
                )
                ->addColumn(
                        'mask_back_left_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'mask_back_left_image'
                )
                ->addColumn(
                        'highlighted_back_left_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'highlighted_back_left_image'
                )
                ->addColumn(
                        'glow_back_right_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'glow_back_right_image'
                )
                ->addColumn(
                        'mask_back_right_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'mask_back_right_image'
                )
                ->addColumn(
                        'highlighted_back_right_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'highlighted_back_right_image'
                )
                ->addColumn(
                        'glow_fold_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'glow_fold_image'
                )
                ->addColumn(
                        'mask_fold_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'mask_fold_image'
                )
                ->addColumn(
                        'highlighted_fold_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'highlighted_fold_image'
                )
                ->addColumn(
                'status', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, 6, [], 'Status'
        );
        $installer->getConnection()->createTable($table);

        unset($table);
        // shirtstyle
        $table = $installer->getConnection()->newTable($installer->getTable('shirtstyle'))
                ->addColumn(
                        'shirtstyle_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true], 'Shirt Style ID'
                )
                ->addColumn(
                        'title', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 1100, [], 'Title'
                )
                ->addColumn(
                        'class', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'class'
                )
                ->addColumn(
                        'price', \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL, '10,2', [], 'price'
                )
                ->addColumn(
                        'type', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, 6, [], 'type'
                )
                ->addColumn(
                        'thumb', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'thumb'
                )
                ->addColumn(
                        'view1_glow_left_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'view1_glow_left_image'
                )
                ->addColumn(
                        'view1_mask_left_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'view1_mask_left_image'
                )
                ->addColumn(
                        'view1_highlighted_left_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'view1_highlighted_left_image'
                )
                ->addColumn(
                        'view1_glow_right_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'view1_glow_right_image'
                )
                ->addColumn(
                        'view1_mask_right_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'view1_mask_right_image'
                )
                ->addColumn(
                        'view1_highlighted_right_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'view1_highlighted_right_image'
                )
                ->addColumn(
                        'view2_glow_left_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'view2_glow_left_image'
                )
                ->addColumn(
                        'view2_mask_left_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'view2_mask_left_image'
                )
                ->addColumn(
                        'view2_highlighted_left_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'view2_highlighted_left_image'
                )
                ->addColumn(
                        'view2_glow_right_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'view2_glow_right_image'
                )
                ->addColumn(
                        'view2_mask_right_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'view2_mask_right_image'
                )
                ->addColumn(
                        'view2_highlighted_right_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'view2_highlighted_right_image'
                )
                ->addColumn(
                        'view3_glow_back_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'view3_glow_back_image'
                )
                ->addColumn(
                        'view3_mask_back_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], ' view3_mask_back_image'
                )
                ->addColumn(
                        'view3_highlighted_back_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'view3_highlighted_back_image'
                )
                ->addColumn(
                        'view4_glow_left_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'view4_glow_left_image'
                )
                ->addColumn(
                        'view4_mask_left_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'view4_mask_left_image'
                )
                ->addColumn(
                        'view4_highlighted_left_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'view4_highlighted_left_image'
                )
                ->addColumn(
                        'view4_glow_right_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'view4_glow_right_image'
                )
                ->addColumn(
                        'view4_mask_right_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'view4_mask_right_image'
                )
                ->addColumn(
                        'view4_highlighted_right_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'view4_highlighted_right_image'
                )
                ->addColumn(
                        'view5_glow_left_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'view5_glow_left_image'
                )
                ->addColumn(
                        'view5_mask_left_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'view5_mask_left_image'
                )
                ->addColumn(
                        'view5_highlighted_left_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'view5_highlighted_left_image'
                )
                ->addColumn(
                        'view5_glow_right_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'view5_glow_right_image'
                )
                ->addColumn(
                        'view5_mask_right_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'view5_mask_right_image'
                )
                ->addColumn(
                        'view5_highlighted_right_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'view5_highlighted_right_image'
                )
                ->addColumn(
                        'view1_casual_glow_left_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'view1_casual_glow_left_image'
                )
                ->addColumn(
                        'view1_casual_mask_left_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'view1_casual_mask_left_image'
                )
                ->addColumn(
                        'view1_casual_highlighted_left_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'view1_casual_highlighted_left_image'
                )
                ->addColumn(
                        'view1_casual_glow_right_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'view1_casual_glow_right_image'
                )
                ->addColumn(
                        'view1_casual_mask_right_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'view1_casual_mask_right_image'
                )
                ->addColumn(
                        'view1_casual_highlighted_right_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'view1_casual_highlighted_right_image'
                )
                ->addColumn(
                        'view2_casual_glow_left_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'view2_casual_glow_left_image'
                )
                ->addColumn(
                        'view2_casual_mask_left_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'view2_casual_mask_left_image'
                )
                ->addColumn(
                        'view2_casual_highlighted_left_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'view2_casual_highlighted_left_image'
                )
                ->addColumn(
                        'view2_casual_glow_right_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'view2_casual_glow_right_image'
                )
                ->addColumn(
                        'view2_casual_mask_right_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'view2_casual_mask_right_image'
                )
                ->addColumn(
                        'view2_casual_highlighted_right_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'view2_casual_highlighted_right_image'
                )
                ->addColumn(
                        'view4_glow_inner_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'view4_glow_inner_image'
                )
                ->addColumn(
                        'view4_mask_inner_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'view4_mask_inner_image'
                )
                ->addColumn(
                        'view4_highlighted_inner_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'view4_highlighted_inner_image'
                )
                ->addColumn(
                        'view5_glow_inner_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'view5_glow_inner_image'
                )
                ->addColumn(
                        'view5_mask_inner_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'view5_mask_inner_image'
                )
                ->addColumn(
                        'view5_highlighted_inner_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'view5_highlighted_inner_image'
                )
                ->addColumn(
                        'status', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, 6, [], 'Status'
                )
                ->addColumn(
                        'created_time', \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP, 200, ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT], 'created_time'
                )
                ->addColumn(
                'update_time', \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP, 200, ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT], 'update_time'
        );
        $installer->getConnection()->createTable($table);

        unset($table);
        // shirts_shirtfabric
        $table = $installer->getConnection()->newTable($installer->getTable('shirts_shirtfabric'))
                ->addColumn(
                        'shirtfabric_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true], 'ShirtFabric ID'
                )
                ->addColumn(
                        'name', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'Name'
                )
                ->addColumn(
                        'price', \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL, '10,2', [], 'Price'
                )
                ->addColumn(
                        'thumbnail_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'Thumbnail Image'
                )
                ->addColumn(
                        'fabric_thumbnail', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'Fabric Thumbnail'
                )
                ->addColumn(
                        'fabric_large_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'Fabric Large Image'
                )
                ->addColumn(
                'status', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, 255, [], 'Status'
        );
        $installer->getConnection()->createTable($table);

        unset($table);
        // shirt_accent_chestpleats
        $table = $installer->getConnection()->newTable($installer->getTable('shirt_accent_chestpleats'))
                ->addColumn(
                        'chestpleats_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true], 'Shirt Fit ID'
                )
                ->addColumn(
                        'title', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 100, [], 'Title'
                )
                ->addColumn(
                        'price', \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL, '10,2', [], 'Price'
                )
                ->addColumn(
                        'class', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 100, [], 'Class'
                )
                ->addColumn(
                        'view1_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'view1_image'
                )
                ->addColumn(
                        'view2_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'view2_image'
                )
                ->addColumn(
                        'view4_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'view4_image'
                )
                ->addColumn(
                        'status', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, 6, [], 'Status'
                )
                ->addColumn(
                        'created_date', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, ['nullable' => false,
                    'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT
                        ], 'created_date'
                )
                ->addColumn(
                'modified_date', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, ['nullable' => false,
            'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT
                ], 'modified_date'
        );
        $installer->getConnection()->createTable($table);

        unset($table);
        //shirt_accent_collarstyle
        $table = $installer->getConnection()->newTable($installer->getTable('shirt_accent_collarstyle'))
                ->addColumn(
                        'collarstyle_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true], 'Collar Style ID'
                )
                ->addColumn(
                        'title', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 100, [], 'Title'
                )
                ->addColumn(
                        'price', \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL, '10,2', [], 'Price'
                )
                ->addColumn(
                        'class', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 100, [], 'Class'
                )
                ->addColumn(
                        'status', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, 6, [], 'Status'
                )
                ->addColumn(
                        'created_date', \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP, null, ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT], 'Creation Time'
                )
                ->addColumn(
                'modified_date', \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP, null, ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE], 'Update Time'
        );
        $installer->getConnection()->createTable($table);

        unset($table);
        //shirt_accent_cuffs
        $table = $installer->getConnection()->newTable($installer->getTable('shirt_accent_cuffs'))
                ->addColumn(
                        'cuff_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true], 'Shirt Cuff ID'
                )
                ->addColumn(
                        'title', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 100, [], 'Title'
                )
                ->addColumn(
                        'price', \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL, '10,2', [], 'Price'
                )
                ->addColumn(
                        'class', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 100, [], 'Class'
                )
                ->addColumn(
                        'status', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, 6, [], 'Status'
                )
                ->addColumn(
                        'created_date', \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP, null, ['nullable' => false,
                    'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT
                        ], 'created_date'
                )
                ->addColumn(
                'modified_date', \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP, null, ['nullable' => false,
            'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT
                ], 'modified_date'
        );
        $installer->getConnection()->createTable($table);

        unset($table);
        // shirt_accent_elbowpatches
        $table = $installer->getConnection()->newTable($installer->getTable('shirt_accent_elbowpatches'))
                ->addColumn(
                        'elbowpatches_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true], 'Shirt Elbow Patches ID'
                )
                ->addColumn(
                        'title', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 100, [], 'Title'
                )
                ->addColumn(
                        'price', \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL, '10,2', [], 'Price'
                )
                ->addColumn(
                        'class', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 100, [], 'Class'
                )
                ->addColumn(
                        'gender', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, 6, [], 'gender'
                )
                ->addColumn(
                        'glow_back_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 100, [], 'glow_back_image'
                )
                ->addColumn(
                        'mask_back_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 100, [], 'mask_back_image'
                )
                ->addColumn(
                        'highlighted_back_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 100, [], 'highlighted_back_image'
                )
                ->addColumn(
                'status', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, 6, [], 'Status'
        );
        $installer->getConnection()->createTable($table);

        unset($table);
        // shirt_accent_placket
        $table = $installer->getConnection()->newTable($installer->getTable('shirt_accent_placket'))
                ->addColumn(
                        'placket_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true], 'Shirt Placket ID'
                )
                ->addColumn(
                        'title', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 100, [], 'Title'
                )
                ->addColumn(
                        'price', \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL, '10,2', [], 'Price'
                )
                ->addColumn(
                        'class', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 100, [], 'Class'
                )
                ->addColumn(
                'status', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, 6, [], 'Status'
        );
        $installer->getConnection()->createTable($table);

        unset($table);
        // shirt_accent_threadcolor
        $table = $installer->getConnection()->newTable($installer->getTable('shirt_accent_threadcolor'))
                ->addColumn(
                        'threadcolor_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true], 'Shirt Thread Color ID'
                )
                ->addColumn(
                        'title', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 100, [], 'Title'
                )
                ->addColumn(
                        'class', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 100, [], 'Class'
                )
                ->addColumn(
                        'price', \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL, '10,2', [], 'Price'
                )
                ->addColumn(
                        'image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 100, [], 'image'
                )
                ->addColumn(
                'status', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, 6, [], 'Status'
        );
        $installer->getConnection()->createTable($table);

        unset($table);
        // shirt_accent_threads
        $table = $installer->getConnection()->newTable($installer->getTable('shirt_accent_threads'))
                ->addColumn(
                        'threads_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true], 'Shirt Threads ID'
                )
                ->addColumn(
                        'title', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 100, [], 'Title'
                )
                ->addColumn(
                        'price', \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL, '10,2', [], 'Price'
                )
                ->addColumn(
                        'class', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 100, [], 'Class'
                )
                ->addColumn(
                        'mask_front_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 100, [], 'mask_front_image'
                )
                ->addColumn(
                        'mask_side_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 100, [], 'mask_side_image'
                )
                ->addColumn(
                        'mask_back_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 100, [], 'mask_back_image'
                )
                ->addColumn(
                        'mask_fold_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 100, [], 'mask_fold_image'
                )
                ->addColumn(
                        'mask_zoom_view', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 100, [], 'mask_zoom_view'
                )
                ->addColumn(
                        'mask_front_buttonhole_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 100, [], 'mask_front_buttonhole_image'
                )
                ->addColumn(
                        'mask_side_buttonhole_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 100, [], 'mask_side_buttonhole_image'
                )
                ->addColumn(
                        'mask_back_buttonhole_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 100, [], 'mask_back_buttonhole_image'
                )
                ->addColumn(
                        'mask_fold_buttonhole_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 100, [], 'mask_fold_buttonhole_image'
                )
                ->addColumn(
                        'mask_zoomview_buttonhole_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 100, [], 'mask_zoomview_buttonhole_image'
                )
                ->addColumn(
                        'status', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, 6, [], 'Status'
                )
                ->addColumn(
                        'created_date', \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP, null, [
                    'nullable' => false,
                    'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT
                        ], 'created_date'
                )
                ->addColumn(
                'modified_date', \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP, null, ['nullable' => false,
            'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT
                ], 'Modified Date'
        );
        $installer->getConnection()->createTable($table);

        unset($table);
        // shirt_button_fabric
        $table = $installer->getConnection()->newTable($installer->getTable('shirt_button_fabric'))
                ->addColumn(
                        'button_fabric_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true], 'Shirt Kutton Fabric ID'
                )
                ->addColumn(
                        'title', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 100, [], 'Title'
                )
                ->addColumn(
                        'class', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 100, [], 'Class'
                )
                ->addColumn(
                        'price', \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL, '10,2', [], 'Price'
                )
                ->addColumn(
                        'btn_count', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 100, [], 'btn_count'
                )
                ->addColumn(
                        'fabric_thumb', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 100, [], 'fabric_thumb'
                )
                ->addColumn(
                'fabric_real_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 100, [], 'fabric_real_image'
        );
        $installer->getConnection()->createTable($table);

        unset($table);
        // shirt_fabric
        $table = $installer->getConnection()->newTable($installer->getTable('shirt_fabric'))
                ->addColumn(
                        'fabric_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, 11, ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true], 'Shirt Fit ID'
                )
                ->addColumn(
                        'fabric_name', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 100, [], 'fabric_name'
                )
                ->addColumn(
                        'fabric_thumb', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'fabric_thumb'
                )
                ->addColumn(
                        'fabric_real_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'sample'
                )
                ->addColumn(
                        'price', \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL, '10,2', [], 'price'
                )
                ->addColumn(
                        'status', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, 6, [], 'status'
                )
                ->addColumn(
                        'shirt_material_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, 11, [], 'Column'
                )
                ->addColumn(
                        'shirt_pattern_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, 11, [], 'shirt_season_id'
                )
                ->addColumn(
                        'shirt_season_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, 11, [], 'shirt_color_id'
                )
                ->addColumn(
                        'shirt_color_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, 11, [], 'shirt_category_id'
                )
                ->addColumn(
                        'shirt_category_id', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 100, [], 'display_fabric_thumb'
                )
                ->addColumn(
                'display_fabric_thumb', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 100, [], 'display_fabric_thumb'
        );
        $installer->getConnection()->createTable($table);

        unset($table);
        // shirt_shirtfabric
        $table = $installer->getConnection()->newTable($installer->getTable('shirt_shirtfabric'))
                ->addColumn(
                        'fabric_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, 11, ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true], 'Shirt Fit ID'
                        // auto increment false
                )
                ->addColumn(
                        'display_fabric_thumb', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'display_fabric_thumb'
                )
                ->addColumn(
                        'fabric_thumb', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'fabric_thumb'
                )
                ->addColumn(
                        'fabric_large_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'sample'
                )
                ->addColumn(
                        'btn_color_id', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, 6, [], 'btn_color_id'
                )
                ->addColumn(
                        'shoes_color_id', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, 6, [], 'shoes_color_id'
                )
                ->addColumn(
                        'fabric_material_id', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, 100, [], 'fabric_material_id'
                )
                ->addColumn(
                        'fabric_pattern_id', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, 100, [], 'fabric_pattern_id'
                )
                ->addColumn(
                        'fabric_season_id', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, 6, [], 'fabric_season_id'
                )
                ->addColumn(
                        'fabric_color_id', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, 6, [], 'fabric_color_id'
                )
                ->addColumn(
                        'fabric_category_id', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, 6, [], 'fabric_category_id'
                )
                ->addColumn(
                        'title', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 100, [], 'title'
                )
                ->addColumn(
                        'price', \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL, '10,2', [], 'Price'
                )
                ->addColumn(
                        'type', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 100, [], 'Type'
                )
                ->addColumn(
                        'status', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 100, [], 'status'
                )
                ->addColumn(
                        'created_date', \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP, null, [
                    'nullable' => false,
                    'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT
                        ], 'created_date'
                )
                ->addColumn(
                'modified_date', \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP, null, ['nullable' => false,
            'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT
                ], 'Modified Date'
        );
        $installer->getConnection()->createTable($table);

        unset($table);
        // shirt_shirtthreadfabric
        $table = $installer->getConnection()->newTable($installer->getTable('shirt_shirtthreadfabric'))
                ->addColumn(
                        'fabric_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, 11, ['auto_increment' => true, 'identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true], 'Shirt Fabric ID'
                )
                ->addColumn(
                        'display_fabric_thumb', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'display_fabric_thumb'
                )
                ->addColumn(
                        'fabric_thumb', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'fabric_thumb'
                )
                ->addColumn(
                        'fabric_large_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'sample'
                )
                ->addColumn(
                        'title', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 100, [], 'title'
                )
                ->addColumn(
                        'price', \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL, '10,2', [], 'Price'
                )
                ->addColumn(
                        'type', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, 6, [], 'type'
                )
                ->addColumn(
                        'status', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, 6, [], 'status'
                )
                ->addColumn(
                        'created_date', \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP, null, [
                    'nullable' => false,
                    'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT
                        ], 'created_date'
                )
                ->addColumn(
                'modified_date', \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP, null, ['nullable' => false,
            'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT
                ], 'Modified Date'
        );
        $installer->getConnection()->createTable($table);

        unset($table);
        // shirt_suitfabric
        $table = $installer->getConnection()->newTable($installer->getTable('shirt_suitfabric'))
                ->addColumn(
                        'fabric_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, 11, ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true], 'Shirt Fabric ID'
                )
                ->addColumn(
                        'display_fabric_thumb', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'display_fabric_thumb'
                )
                ->addColumn(
                        'fabric_thumb', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'fabric_thumb'
                )
                ->addColumn(
                        'fabric_large_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'fabric_large_image'
                )
                ->addColumn(
                        'title', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 100, [], 'Title'
                )
                ->addColumn(
                        'price', \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL, '10,2', [], 'Price'
                )
                ->addColumn(
                        'type', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, 6, [], 'Type'
                )
                ->addColumn(
                        'btn_color_id', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, 6, [], 'btn_color_id'
                )
                ->addColumn(
                        'tie_color', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, 6, [], 'fabric_season_id'
                )
                ->addColumn(
                        'shirt_type', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, 6, [], 'fabric_color_id'
                )
                ->addColumn(
                        'shoes_color', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, 6, [], 'shoes_color'
                )
                ->addColumn(
                        'is_fabric', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'is_fabric'
                )
                ->addColumn(
                        'suit_material_id', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, 6, [], 'suit_material_id'
                )
                ->addColumn(
                        'suit_pattern_id', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, 6, [], 'suit_pattern_id'
                )
                ->addColumn(
                        'suit_season_id', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, 6, [], 'suit_season_id'
                )
                ->addColumn(
                        'suit_color_id', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, 6, [], 'suit_color_id'
                )
                ->addColumn(
                        'suit_category_id', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, 6, [], 'suit_category_id'
                )
                ->addColumn(
                        'status', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, 6, [], 'status'
                )
                ->addColumn(
                        'created_date', \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP, null, [
                    'nullable' => false,
                    'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT
                        ], 'created_date'
                )
                ->addColumn(
                'modified_date', \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP, null, ['nullable' => false,
            'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT
                ], 'Modified Date'
        );
        $installer->getConnection()->createTable($table);

        unset($table);

        $table = $installer->getConnection()->newTable($installer->getTable('generation_cmd'))
                ->addColumn(
                        'id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, 11, ['auto_increment' => true, 'identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true], 'ID'
                )
                ->addColumn(
                        'command', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, '', [], 'command'
                )
                ->addColumn(
                'flag', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, 2, ['default' => 0], 'flag'
                )
        ;
        $installer->getConnection()->createTable($table);

        $installer->endSetup();
    }

}
