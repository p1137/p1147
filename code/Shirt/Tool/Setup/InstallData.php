<?php

namespace Shirt\Tool\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Eav\Setup\EavSetupFactory;

class InstallData implements InstallDataInterface {

    protected $_postFactory;
    private $eavSetupFactory;

    public function __construct(
    \Magento\Framework\Setup\ModuleDataSetupInterface $setup, EavSetupFactory $eavSetupFactory, \Shirt\Tool\Model\FabriccataFactory $fabriccataFactory, \Shirt\Tool\Model\FabricColorFactory $fabricColorFactory, \Shirt\Tool\Model\FabricMaterialFactory $fabricMaterial, \Shirt\Tool\Model\FabricPatternFactory $fabricPattern, \Shirt\Tool\Model\FabricSeasonFactory $fabricSeason, \Shirt\Tool\Model\ShirtBottomFactory $shirtBottom, \Shirt\Tool\Model\ShirtCategoryFactory $shirtCategory, \Shirt\Tool\Model\ShirtColorFactory $shirtColor, \Shirt\Tool\Model\ShirtCuffFactory $shirtCuff, \Shirt\Tool\Model\ShirtFitFactory $shirtFit, \Shirt\Tool\Model\ShirtMaterialFactory $shirtMaterial, \Shirt\Tool\Model\ShirtPatternFactory $shirtPattern, \Shirt\Tool\Model\ShirtPlacketsFactory $shirtPlackets, \Shirt\Tool\Model\ShirtPleatsFactory $shirtPleats, \Shirt\Tool\Model\ShirtPocketFactory $shirtPocket, \Shirt\Tool\Model\ShirtSeasonFactory $shirtSeason, \Shirt\Tool\Model\ShirtSleevesFactory $shirtSleeves, \Shirt\Tool\Model\ShirtStyleFactory $shirtStyle,
//        \Shirt\Tool\Model\ShirtShirtfabricFactory $shirtShirtfabric,
            \Shirt\Tool\Model\ShirtShirtthreadfabricFactory $shirtShirtthreadfabric, \Shirt\Tool\Model\ShirtSuitfabricFactory $shirtSuitfabric, \Shirt\Tool\Model\ShirtAccentCollarstyleFactory $shirtAccentCollarstyle, \Shirt\Tool\Model\ShirtAccentChestpleatsFactory $shirt_accent_chestpleats, \Shirt\Tool\Model\ShirtAccentCuffsFactory $shirtAccentCuffs, \Shirt\Tool\Model\ShirtAccentElbowpatchesFactory $shirtAccentElbowpatches, \Shirt\Tool\Model\ShirtAccentThreadcolorFactory $shirtAccentThreadcolor, \Shirt\Tool\Model\ShirtAccentPlacketFactory $shirtAccentPlacket, \Shirt\Tool\Model\ShirtAccentThreadsFactory $shirtAccentThreads, \Shirt\Tool\Model\ShirtButtonFabricFactory $shirtButtonFabric, \Shirt\Tool\Model\ShirtFabricFactory $ShirtFabric
    ) {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->installer = $setup;
        $this->_fabriccataFactory = $fabriccataFactory;
        $this->_fabricColorFactory = $fabricColorFactory;
        $this->_fabricMaterial = $fabricMaterial;
        $this->_fabricPattern = $fabricPattern;
        $this->_fabricSeason = $fabricSeason;
        $this->_shirtBottom = $shirtBottom;
        $this->_shirtCategory = $shirtCategory;
        $this->_shirtColor = $shirtColor;
        $this->_shirtCuff = $shirtCuff;
        $this->_shirtFit = $shirtFit;
        $this->_shirtMaterial = $shirtMaterial;
        $this->_shirtPattern = $shirtPattern;
        $this->_shirtPlackets = $shirtPlackets;
        $this->_shirtPleats = $shirtPleats;
        $this->_shirtPocket = $shirtPocket;
        $this->_shirtSeason = $shirtSeason;
        $this->_shirtSleeves = $shirtSleeves;
        $this->_shirtStyle = $shirtStyle;
        $this->_shirtShirtthreadfabric = $shirtShirtthreadfabric;
        $this->_shirtSuitfabric = $shirtSuitfabric;
        $this->_shirtAccentCollarstyle = $shirtAccentCollarstyle;
        $this->_shirt_accent_chestpleats = $shirt_accent_chestpleats;
        $this->_shirtAccentCuffs = $shirtAccentCuffs;
        $this->_shirtAccentElbowpatches = $shirtAccentElbowpatches;
        $this->_shirtAccentThreadcolor = $shirtAccentThreadcolor;
        $this->_shirtAccentPlacket = $shirtAccentPlacket;
        $this->_shirtAccentThreads = $shirtAccentThreads;
        $this->_shirtButtonFabric = $shirtButtonFabric;
        $this->_ShirtFabric = $ShirtFabric;
    }

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context) {
        // dont add primary key. Its auto incremented.
        // table fabric_catageory

        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
        $eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY, 'customize', [
            'group' => 'Product Details',
            'type' => 'int',
            'backend' => 'Magento\Eav\Model\Entity\Attribute\Backend\ArrayBackend',
            'frontend' => '',
            'label' => 'Customizable',
            'input' => 'select',
            'class' => '',
            'source' => 'Shirt\Tool\Model\Config\Source\Options',
            'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
            'visible' => true,
            'required' => true,
            'user_defined' => false,
            'default' => 0,
            'searchable' => false,
            'filterable' => false,
            'comparable' => false,
            'visible_on_front' => false,
            'used_in_product_listing' => true,
            'unique' => false,
            'apply_to' => ''
                ]
        );


        $Fabriccatadata = [
                [
                'title' => 'All',
                'class' => 'icon-icon-18',
                'status' => 1
            ],
                [
                'title' => 'Faves',
                'class' => 'icon-icon-20',
                'status' => 1
            ],
                [
                'title' => 'Winter',
                'class' => 'icon-icon-22',
                'status' => 0
            ],
                [
                'title' => 'Limited Edition',
                'class' => '',
                'status' => 0
            ],
                [
                'title' => 'Premium',
                'class' => 'icon-icon-21',
                'status' => 1
            ],
                [
                'title' => 'Promo',
                'class' => '',
                'status' => 0
            ],
                [
                'title' => 'New',
                'class' => 'icon-icon-19',
                'status' => 1
            ]
        ];

        foreach ($Fabriccatadata as $data) {
            $this->_fabriccataFactory->create()->addData($data)->save();
        }

        // table fabric_color
        $FabricColorFactory = [
                [
                'title' => 'Grey',
                'status' => 1
            ],
                [
                'title' => 'Black',
                'status' => 1
            ],
                [
                'title' => 'Blue',
                'status' => 1
            ],
                [
                'title' => 'Brown',
                'status' => 1
            ],
                [
                'title' => 'White Shade',
                'status' => 1
            ],
                [
                'title' => 'pink',
                'status' => 1
            ]
        ];

        foreach ($FabricColorFactory as $data) {
            $this->_fabricColorFactory->create()->addData($data)->save();
        }

        // table fabric_material
        $fabricMaterialData = [
                [
                'title' => 'Wool',
                'status' => 1
            ],
                [
                'title' => 'Corduroy',
                'status' => 1
            ],
                [
                'title' => 'linen',
                'status' => 1
            ],
                [
                'title' => 'Polyester',
                'status' => 1
            ],
                [
                'title' => 'Velvet',
                'status' => 1
            ],
                [
                'title' => 'Pure Cotton',
                'status' => 1
            ]
        ];

        foreach ($fabricMaterialData as $data) {
            $this->_fabricMaterial->create()->addData($data)->save();
        }



        // table fabric_pattern
        $FabricPatternData = [
                [
                'title' => 'Squared',
                'status' => 1
            ],
                [
                'title' => 'Plain',
                'status' => 1
            ],
                [
                'title' => 'Striped',
                'status' => 1
            ],
                [
                'title' => 'Chevron pattern',
                'status' => 1
            ],
                [
                'title' => 'Houndstooth',
                'status' => 1
            ],
                [
                'title' => 'Paisley',
                'status' => 1
            ],
                [
                'title' => 'Herringbone',
                'status' => 1
            ],
                [
                'title' => 'Other',
                'status' => 1
            ]
        ];

        foreach ($FabricPatternData as $data) {
            $this->_fabricPattern->create()->addData($data)->save();
        }

        // table fabric_season
        $FabricSeasonData = [
                [
                'title' => 'Summer',
                'status' => 1
            ],
                [
                'title' => 'Winter',
                'status' => 1
            ],
                [
                'title' => 'Year Round',
                'status' => 1
            ]
        ];

        foreach ($FabricSeasonData as $data) {
            $this->_fabricSeason->create()->addData($data)->save();
        }


        // table shirtbottom
        $ShirtBottomData = [
                [
                'title' => 'Tail',
                'class' => 'icon-Tailed',
                'price' => 5.00,
                'gender' => 0,
                'view1_image' => 'shirt_images/bottom/Shirt_Bottom_1_1.png',
                'view2_image' => 'shirt_images/bottom/Shirt_Bottom_2_1.png',
                'view3_image' => 'shirt_images/bottom/Shirt_Bottom_3_1.png',
                'status' => 1
            ],
                [
                'title' => 'Square',
                'class' => 'icon-Squared',
                'price' => 5.00,
                'gender' => 0,
                'view1_image' => 'shirt_images/bottom/Shirt_Bottom_1_2.png',
                'view2_image' => 'shirt_images/bottom/Shirt_Bottom_2_2.png',
                'view3_image' => 'shirt_images/bottom/Shirt_Bottom_3_2.png',
                'status' => 1
            ]
        ];

        foreach ($ShirtBottomData as $data) {
            $this->_shirtBottom->create()->addData($data)->save();
        }


        //shirtcategory

        $ShirtCategoryData = [
                [
                'title' => 'All',
                'class' => 'icon-All_Main_Icon',
                'status' => 1
            ],
                [
                'title' => 'Faves',
                'class' => 'icon-Faves_Main_Icon',
                'status' => 1
            ],
                [
                'title' => 'Winter',
                'class' => 'icon-Winter_Main_Icon',
                'status' => 1
            ],
                [
                'title' => 'Basic',
                'class' => 'icon-Basic',
                'status' => 1
            ],
                [
                'title' => 'Premium',
                'class' => 'icon-Premium_Main_Icon',
                'status' => 1
            ],
                [
                'title' => 'Limited Edition',
                'class' => 'icon-Limited-Edition',
                'status' => 1
            ],
                [
                'title' => 'New',
                'class' => 'icon-New_Main_Icon',
                'status' => 1
            ]
        ];

        foreach ($ShirtCategoryData as $data) {
            $this->_shirtCategory->create()->addData($data)->save();
        }

        //shirtcolor
        $ShirtColorData = [
                [
                'title' => 'Grey',
                'status' => 1
            ],
                [
                'title' => 'Black',
                'status' => 1
            ],
                [
                'title' => 'Blue',
                'status' => 1
            ],
                [
                'title' => 'Brown',
                'status' => 1
            ],
                [
                'title' => 'White Shade',
                'status' => 1
            ],
                [
                'title' => 'pink',
                'status' => 1
            ]
        ];

        foreach ($ShirtColorData as $data) {
            $this->_shirtColor->create()->addData($data)->save();
        }



        // shirtcuff
        $ShirtCuffData = [
                [
                'title' => 'Double rounded french',
                'class' => 'icon-Cuff_double_rounded_french1',
                'price' => 5.00,
                'type' => 1,
                'gender' => 1,
                'glow_front_left_image' => 'glow_mask_shirt/Front_View/Cuffs/French_left_shad.png',
                'mask_front_left_image' => 'glow_mask_shirt/Front_View/Cuffs/French_left_mask.png',
                'highlighted_front_left_image' => 'glow_mask_shirt/Front_View/Cuffs/French_left_hi.png',
                'glow_front_right_image' => 'glow_mask_shirt/Front_View/Cuffs/French_right_shad.png',
                'mask_front_right_image' => 'glow_mask_shirt/Front_View/Cuffs/French_right_mask.png',
                'highlighted_front_right_image' => 'glow_mask_shirt/Front_View/Cuffs/French_right_hi.png',
                'glow_front_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Front/CasualCuff_Front_left_mask.png',
                'mask_front_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png',
                'highlighted_front_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png',
                'glow_front_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_glow.png',
                'mask_front_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png',
                'highlighted_front_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png',
                'glow_side_left_image' => 'glow_mask_shirt/Side_View/Cuffs/Normal_left_shad.png',
                'mask_side_left_image' => 'glow_mask_shirt/Side_View/Cuffs/Normal_left_mask.png',
                'highlighted_side_left_image' => 'glow_mask_shirt/Side_View/Cuffs/Normal_left_hi.png',
                'glow_side_right_image' => 'glow_mask_shirt/Side_View/Cuffs/Normal_right_shad.png',
                'mask_side_right_image' => 'glow_mask_shirt/Side_View/Cuffs/Normal_right_mask.png',
                'highlighted_side_right_image' => 'glow_mask_shirt/Side_View/Cuffs/Normal_right_hi.png',
                'glow_side_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_glow.png',
                'mask_side_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png',
                'highlighted_side_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png',
                'glow_side_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_glow.png',
                'mask_side_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png',
                'highlighted_side_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png',
                'glow_back_left_image' => 'glow_mask_shirt/Back_View/Cuffs/DoubleFrenchRounded_left_shad.png',
                'mask_back_left_image' => 'glow_mask_shirt/Back_View/Cuffs/DoubleFrenchRounded_left_mask.png',
                'highlighted_back_left_image' => 'glow_mask_shirt/Back_View/Cuffs/DoubleFrenchRounded_left_hi.png',
                'glow_back_right_image' => 'glow_mask_shirt/Back_View/Cuffs/DoubleFrenchRounded_right_shad.png',
                'mask_back_right_image' => 'glow_mask_shirt/Back_View/Cuffs/DoubleFrenchRounded_right_mask.png',
                'highlighted_back_right_image' => 'glow_mask_shirt/Back_View/Cuffs/DoubleFrenchRounded_right_hi.png',
                'glow_back_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_glow.png',
                'mask_back_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png',
                'highlighted_back_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png',
                'glow_back_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_glow.png',
                'mask_back_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png',
                'highlighted_back_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png',
                'glow_fold_main_image' => 'glow_mask_shirt/Folded_View/Cuffs/DoubleFrenchRounded_Main_shad.png',
                'mask_fold_main_image' => 'glow_mask_shirt/Folded_View/Cuffs/DoubleFrenchRounded_Main_mask.png',
                'highlighted_fold_main_image' => 'glow_mask_shirt/Folded_View/Cuffs/DoubleFrenchRounded_Main_hi.png',
                'glow_fold_inner_image' => 'glow_mask_shirt/Folded_View/Cuffs/DoubleFrenchRounded_Inner_shad.png',
                'mask_fold_inner_image' => 'glow_mask_shirt/Folded_View/Cuffs/DoubleFrenchRounded_Inner_mask.png',
                'highlighted_fold_inner_image' => 'glow_mask_shirt/Folded_View/Cuffs/DoubleFrenchRounded_Inner_hi.png',
                'back_button_image' => null,
                'fold_button_image' => null,
                'back_thread_image' => null,
                'fold_thread_image' => null,
                'status' => 1
            ],
                [
                'title' => 'Double Squared french',
                'class' => 'icon-Cuff_double_squared_french2',
                'price' => 5.00,
                'type' => 1,
                'gender' => 1,
                'glow_front_left_image' => 'glow_mask_shirt/Front_View/Cuffs/French_left_shad.png',
                'mask_front_left_image' => 'glow_mask_shirt/Front_View/Cuffs/French_left_mask.png',
                'highlighted_front_left_image' => 'glow_mask_shirt/Front_View/Cuffs/French_left_hi.png',
                'glow_front_right_image' => 'glow_mask_shirt/Front_View/Cuffs/French_right_shad.png',
                'mask_front_right_image' => 'glow_mask_shirt/Front_View/Cuffs/French_right_mask.png',
                'highlighted_front_right_image' => 'glow_mask_shirt/Front_View/Cuffs/French_right_hi.png',
                'glow_front_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_glow.png',
                'mask_front_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png',
                'highlighted_front_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png',
                'glow_front_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_glow.png',
                'mask_front_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png',
                'highlighted_front_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png',
                'glow_side_left_image' => 'glow_mask_shirt/Side_View/Cuffs/Normal_left_shad.png',
                'mask_side_left_image' => 'glow_mask_shirt/Side_View/Cuffs/Normal_left_mask.png',
                'highlighted_side_left_image' => 'glow_mask_shirt/Side_View/Cuffs/Normal_left_hi.png',
                'glow_side_right_image' => 'glow_mask_shirt/Side_View/Cuffs/Normal_right_shad.png',
                'mask_side_right_image' => 'glow_mask_shirt/Side_View/Cuffs/Normal_right_mask.png',
                'highlighted_side_right_image' => 'glow_mask_shirt/Side_View/Cuffs/Normal_right_hi.png',
                'glow_side_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_glow.png',
                'mask_side_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png',
                'highlighted_side_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png',
                'glow_side_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_glow.png',
                'mask_side_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png',
                'highlighted_side_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png',
                'glow_back_left_image' => 'glow_mask_shirt/Back_View/Cuffs/DoubleFrenchStraight_left_shad.png',
                'mask_back_left_image' => 'glow_mask_shirt/Back_View/Cuffs/DoubleFrenchStraight_left_mask.png',
                'highlighted_back_left_image' => 'glow_mask_shirt/Back_View/Cuffs/DoubleFrenchStraight_left_hi.png',
                'glow_back_right_image' => 'glow_mask_shirt/Back_View/Cuffs/DoubleFrenchStraight_right_shad.png',
                'mask_back_right_image' => 'glow_mask_shirt/Back_View/Cuffs/DoubleFrenchStraight_right_mask.png',
                'highlighted_back_right_image' => 'glow_mask_shirt/Back_View/Cuffs/DoubleFrenchStraight_right_hi.png',
                'glow_back_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_glow.png',
                'mask_back_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png',
                'highlighted_back_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png',
                'glow_back_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png',
                'mask_back_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png',
                'highlighted_back_opencuff_right_image' => 'glow_mask_shirt/Folded_View/Cuffs/DoubleFrenchStraight_left_shad.png',
                'glow_fold_main_image' => 'glow_mask_shirt/Folded_View/Cuffs/DoubleFrenchStraight_Main_shad.png',
                'mask_fold_main_image' => 'glow_mask_shirt/Folded_View/Cuffs/DoubleFrenchStraight_Main_mask.png',
                'highlighted_fold_main_image' => 'glow_mask_shirt/Folded_View/Cuffs/DoubleFrenchStraight_Main_hi.png',
                'glow_fold_inner_image' => 'glow_mask_shirt/Folded_View/Cuffs/DoubleFrenchStraight_Inner_shad.png',
                'mask_fold_inner_image' => 'glow_mask_shirt/Folded_View/Cuffs/DoubleFrenchStraight_Inner_mask.png',
                'highlighted_fold_inner_image' => 'glow_mask_shirt/Folded_View/Cuffs/DoubleFrenchStraight_Inner_hi.png',
                'back_button_image' => null,
                'fold_button_image' => null,
                'back_thread_image' => null,
                'fold_thread_image' => null,
                'status' => 1
            ],
                [
                'title' => 'One-button-cut',
                'class' => 'icon-Cuff_one_button_cut3',
                'price' => 5.00,
                'type' => 1,
                'gender' => 1,
                'glow_front_left_image' => 'glow_mask_shirt/Front_View/Cuffs/normal_left_shad.png',
                'mask_front_left_image' => 'glow_mask_shirt/Front_View/Cuffs/normal_left_mask.png',
                'highlighted_front_left_image' => 'glow_mask_shirt/Front_View/Cuffs/normal_left_hi.png',
                'glow_front_right_image' => 'glow_mask_shirt/Front_View/Cuffs/normal_right_shad.png',
                'mask_front_right_image' => 'glow_mask_shirt/Front_View/Cuffs/normal_right_mask.png',
                'highlighted_front_right_image' => 'glow_mask_shirt/Front_View/Cuffs/normal_right_hi.png',
                'glow_front_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_glow.png',
                'mask_front_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png',
                'highlighted_front_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png',
                'glow_front_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_glow.png',
                'mask_front_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png',
                'highlighted_front_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png',
                'glow_side_left_image' => 'glow_mask_shirt/Side_View/Cuffs/Normal_left_shad.png',
                'mask_side_left_image' => 'glow_mask_shirt/Side_View/Cuffs/Normal_left_mask.png',
                'highlighted_side_left_image' => 'glow_mask_shirt/Side_View/Cuffs/Normal_left_hi.png',
                'glow_side_right_image' => 'glow_mask_shirt/Side_View/Cuffs/Normal_right_shad.png',
                'mask_side_right_image' => 'glow_mask_shirt/Side_View/Cuffs/Normal_right_mask.png',
                'highlighted_side_right_image' => 'glow_mask_shirt/Side_View/Cuffs/Normal_right_hi.png',
                'glow_side_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_glow.png',
                'mask_side_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png',
                'highlighted_side_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png',
                'glow_side_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_glow.png',
                'mask_side_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png',
                'highlighted_side_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png',
                'glow_back_left_image' => 'glow_mask_shirt/Back_View/Cuffs/NormalCut_left_shad.png',
                'mask_back_left_image' => 'glow_mask_shirt/Back_View/Cuffs/NormalCut_left_mask.png',
                'highlighted_back_left_image' => 'glow_mask_shirt/Back_View/Cuffs/NormalCut_left_hi.png',
                'glow_back_right_image' => 'glow_mask_shirt/Back_View/Cuffs/NormalCut_right_shad.png',
                'mask_back_right_image' => 'glow_mask_shirt/Back_View/Cuffs/NormalCut_right_mask.png',
                'highlighted_back_right_image' => 'glow_mask_shirt/Back_View/Cuffs/NormalCut_right_hi.png',
                'glow_back_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_glow.png',
                'mask_back_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png',
                'highlighted_back_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png',
                'glow_back_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_glow.png',
                'mask_back_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png',
                'highlighted_back_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png',
                'glow_fold_main_image' => 'glow_mask_shirt/Folded_View/Cuffs/NormalCut_Main_shad.png',
                'mask_fold_main_image' => 'glow_mask_shirt/Folded_View/Cuffs/NormalCut_Main_mask.png',
                'highlighted_fold_main_image' => 'glow_mask_shirt/Folded_View/Cuffs/NormalCut_Main_hi.png',
                'glow_fold_inner_image' => 'glow_mask_shirt/Folded_View/Cuffs/NormalCut_Inner_shad.png',
                'mask_fold_inner_image' => 'glow_mask_shirt/Folded_View/Cuffs/NormalCut_Inner_mask.png',
                'highlighted_fold_inner_image' => 'glow_mask_shirt/Folded_View/Cuffs/NormalCut_Inner_hi.png',
                'back_button_image' => null,
                'fold_button_image' => null,
                'back_thread_image' => null,
                'fold_thread_image' => null,
                'status' => 1
            ],
                [
                'title' => 'Rounded 1 button',
                'class' => 'icon-Cuff_one_button_rounded4',
                'price' => 5.00,
                'type' => 1,
                'gender' => 1,
                'glow_front_left_image' => 'glow_mask_shirt/Front_View/Cuffs/normal_left_shad.png',
                'mask_front_left_image' => 'glow_mask_shirt/Front_View/Cuffs/normal_left_mask.png',
                'highlighted_front_left_image' => 'glow_mask_shirt/Front_View/Cuffs/normal_left_hi.png',
                'glow_front_right_image' => 'glow_mask_shirt/Front_View/Cuffs/normal_right_shad.png',
                'mask_front_right_image' => 'glow_mask_shirt/Front_View/Cuffs/normal_right_mask.png',
                'highlighted_front_right_image' => 'glow_mask_shirt/Front_View/Cuffs/normal_right_hi.png',
                'glow_front_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_glow.png',
                'mask_front_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png',
                'highlighted_front_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png',
                'glow_front_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_glow.png',
                'mask_front_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png',
                'highlighted_front_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png',
                'glow_side_left_image' => 'glow_mask_shirt/Side_View/Cuffs/Normal_left_shad.png',
                'mask_side_left_image' => 'glow_mask_shirt/Side_View/Cuffs/Normal_left_mask.png',
                'highlighted_side_left_image' => 'glow_mask_shirt/Side_View/Cuffs/Normal_left_hi.png',
                'glow_side_right_image' => 'glow_mask_shirt/Side_View/Cuffs/Normal_right_shad.png',
                'mask_side_right_image' => 'glow_mask_shirt/Side_View/Cuffs/Normal_right_mask.png',
                'highlighted_side_right_image' => 'glow_mask_shirt/Side_View/Cuffs/Normal_right_hi.png',
                'glow_side_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_glow.png',
                'mask_side_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png',
                'highlighted_side_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png',
                'glow_side_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_glow.png',
                'mask_side_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png',
                'highlighted_side_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png',
                'glow_back_left_image' => 'glow_mask_shirt/Back_View/Cuffs/NormalRounded_left_shad.png',
                'mask_back_left_image' => 'glow_mask_shirt/Back_View/Cuffs/NormalRounded_left_mask.png',
                'highlighted_back_left_image' => 'glow_mask_shirt/Back_View/Cuffs/NormalRounded_left_hi.png',
                'glow_back_right_image' => 'glow_mask_shirt/Back_View/Cuffs/NormalRounded_right_shad.png',
                'mask_back_right_image' => 'glow_mask_shirt/Back_View/Cuffs/NormalRounded_right_mask.png',
                'highlighted_back_right_image' => 'glow_mask_shirt/Back_View/Cuffs/NormalRounded_right_hi.png',
                'glow_back_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_glow.png',
                'mask_back_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png',
                'highlighted_back_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png',
                'glow_back_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_glow.png',
                'mask_back_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png',
                'highlighted_back_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png',
                'glow_fold_main_image' => 'glow_mask_shirt/Folded_View/Cuffs/NormalRounded_Main_shad.png',
                'mask_fold_main_image' => 'glow_mask_shirt/Folded_View/Cuffs/NormalRounded_Main_mask.png',
                'highlighted_fold_main_image' => 'glow_mask_shirt/Folded_View/Cuffs/NormalRounded_Main_hi.png',
                'glow_fold_inner_image' => 'glow_mask_shirt/Folded_View/Cuffs/NormalRounded_Inner_shad.png',
                'mask_fold_inner_image' => 'glow_mask_shirt/Folded_View/Cuffs/NormalRounded_Inner_mask.png',
                'highlighted_fold_inner_image' => 'glow_mask_shirt/Folded_View/Cuffs/NormalRounded_Inner_hi.png',
                'back_button_image' => null,
                'fold_button_image' => null,
                'back_thread_image' => null,
                'fold_thread_image' => null,
                'status' => 1
            ],
                [
                'title' => 'Rounded 2 buttons',
                'class' => 'icon-Cuff_two_button_rounded5',
                'price' => 5.00,
                'type' => 1,
                'gender' => 1,
                'glow_front_left_image' => 'glow_mask_shirt/Front_View/Cuffs/normal_left_shad.png',
                'mask_front_left_image' => 'glow_mask_shirt/Front_View/Cuffs/normal_left_mask.png',
                'highlighted_front_left_image' => 'glow_mask_shirt/Front_View/Cuffs/normal_left_hi.png',
                'glow_front_right_image' => 'glow_mask_shirt/Front_View/Cuffs/normal_right_shad.png',
                'mask_front_right_image' => 'glow_mask_shirt/Front_View/Cuffs/normal_right_mask.png',
                'highlighted_front_right_image' => 'glow_mask_shirt/Front_View/Cuffs/normal_right_hi.png',
                'glow_front_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_glow.png',
                'mask_front_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png',
                'highlighted_front_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png',
                'glow_front_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_glow.png',
                'mask_front_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png',
                'highlighted_front_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png',
                'glow_side_left_image' => 'glow_mask_shirt/Side_View/Cuffs/Normal_left_shad.png',
                'mask_side_left_image' => 'glow_mask_shirt/Side_View/Cuffs/Normal_left_mask.png',
                'highlighted_side_left_image' => 'glow_mask_shirt/Side_View/Cuffs/Normal_left_hi.png',
                'glow_side_right_image' => 'glow_mask_shirt/Side_View/Cuffs/Normal_right_shad.png',
                'mask_side_right_image' => 'glow_mask_shirt/Side_View/Cuffs/Normal_right_mask.png',
                'highlighted_side_right_image' => 'glow_mask_shirt/Side_View/Cuffs/Normal_right_hi.png',
                'glow_side_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_glow.png',
                'mask_side_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png',
                'highlighted_side_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png',
                'glow_side_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_glow.png',
                'mask_side_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png',
                'highlighted_side_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png',
                'glow_back_left_image' => 'glow_mask_shirt/Back_View/Cuffs/NormalRounded_left_shad.png',
                'mask_back_left_image' => 'glow_mask_shirt/Back_View/Cuffs/NormalRounded_left_mask.png',
                'highlighted_back_left_image' => 'glow_mask_shirt/Back_View/Cuffs/NormalRounded_left_hi.png',
                'glow_back_right_image' => 'glow_mask_shirt/Back_View/Cuffs/NormalRounded_right_shad.png',
                'mask_back_right_image' => 'glow_mask_shirt/Back_View/Cuffs/NormalRounded_right_mask.png',
                'highlighted_back_right_image' => 'glow_mask_shirt/Back_View/Cuffs/NormalRounded_right_hi.png',
                'glow_back_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_glow.png',
                'mask_back_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png',
                'highlighted_back_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png',
                'glow_back_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_glow.png',
                'mask_back_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png',
                'highlighted_back_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png',
                'glow_fold_main_image' => 'glow_mask_shirt/Folded_View/Cuffs/NormalRounded_Main_shad.png',
                'mask_fold_main_image' => 'glow_mask_shirt/Folded_View/Cuffs/NormalRounded_Main_mask.png',
                'highlighted_fold_main_image' => 'glow_mask_shirt/Folded_View/Cuffs/NormalRounded_Main_hi.png',
                'glow_fold_inner_image' => 'glow_mask_shirt/Folded_View/Cuffs/NormalRounded_Inner_shad.png',
                'mask_fold_inner_image' => 'glow_mask_shirt/Folded_View/Cuffs/NormalRounded_Inner_mask.png',
                'highlighted_fold_inner_image' => 'glow_mask_shirt/Folded_View/Cuffs/NormalRounded_Inner_hi.png',
                'back_button_image' => null,
                'fold_button_image' => null,
                'back_thread_image' => null,
                'fold_thread_image' => null,
                'status' => 1
            ],
                [
                'title' => 'Rounded french',
                'class' => 'icon-Cuff_rounded_french6',
                'price' => 5.00,
                'type' => 1,
                'gender' => 1,
                'glow_front_left_image' => 'glow_mask_shirt/Front_View/Cuffs/French_left_shad.png',
                'mask_front_left_image' => 'glow_mask_shirt/Front_View/Cuffs/French_left_mask.png',
                'highlighted_front_left_image' => 'glow_mask_shirt/Front_View/Cuffs/French_left_hi.png',
                'glow_front_right_image' => 'glow_mask_shirt/Front_View/Cuffs/French_right_shad.png',
                'mask_front_right_image' => 'glow_mask_shirt/Front_View/Cuffs/French_right_mask.png',
                'highlighted_front_right_image' => 'glow_mask_shirt/Front_View/Cuffs/French_right_hi.png',
                'glow_front_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_glow.png',
                'mask_front_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png',
                'highlighted_front_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png',
                'glow_front_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_glow.png',
                'mask_front_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png',
                'highlighted_front_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png',
                'glow_side_left_image' => 'glow_mask_shirt/Side_View/Cuffs/Normal_left_shad.png',
                'mask_side_left_image' => 'glow_mask_shirt/Side_View/Cuffs/Normal_left_mask.png',
                'highlighted_side_left_image' => 'glow_mask_shirt/Side_View/Cuffs/Normal_left_hi.png',
                'glow_side_right_image' => 'glow_mask_shirt/Side_View/Cuffs/Normal_right_shad.png',
                'mask_side_right_image' => 'glow_mask_shirt/Side_View/Cuffs/Normal_right_mask.png',
                'highlighted_side_right_image' => 'glow_mask_shirt/Side_View/Cuffs/Normal_right_hi.png',
                'glow_side_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_glow.png',
                'mask_side_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png',
                'highlighted_side_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png',
                'glow_side_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_glow.png',
                'mask_side_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png',
                'highlighted_side_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png',
                'glow_back_left_image' => 'glow_mask_shirt/Back_View/Cuffs/FrenchRound_left_shad.png',
                'mask_back_left_image' => 'glow_mask_shirt/Back_View/Cuffs/FrenchRound_left_mask.png',
                'highlighted_back_left_image' => 'glow_mask_shirt/Back_View/Cuffs/FrenchRound_left_hi.png',
                'glow_back_right_image' => 'glow_mask_shirt/Back_View/Cuffs/FrenchRound_right_shad.png',
                'mask_back_right_image' => 'glow_mask_shirt/Back_View/Cuffs/FrenchRound_right_mask.png',
                'highlighted_back_right_image' => 'glow_mask_shirt/Back_View/Cuffs/FrenchRound_right_hi.png',
                'glow_back_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_glow.png',
                'mask_back_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png',
                'highlighted_back_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png',
                'glow_back_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_glow.png',
                'mask_back_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png',
                'highlighted_back_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png',
                'glow_fold_main_image' => 'glow_mask_shirt/Folded_View/Cuffs/FrenchRounded_Main_shad.png',
                'mask_fold_main_image' => 'glow_mask_shirt/Folded_View/Cuffs/FrenchRounded_Main_mask.png',
                'highlighted_fold_main_image' => 'glow_mask_shirt/Folded_View/Cuffs/FrenchRounded_Main_hi.png',
                'glow_fold_inner_image' => 'glow_mask_shirt/Folded_View/Cuffs/FrenchRounded_Inner_shad.png',
                'mask_fold_inner_image' => 'glow_mask_shirt/Folded_View/Cuffs/FrenchRounded_Inner_mask.png',
                'highlighted_fold_inner_image' => 'glow_mask_shirt/Folded_View/Cuffs/FrenchRounded_Inner_hi.png',
                'back_button_image' => null,
                'fold_button_image' => null,
                'back_thread_image' => null,
                'fold_thread_image' => null,
                'status' => 1
            ],
                [
                'title' => 'Single cuff 1 button',
                'class' => 'icon-Cuff_one_button_straight7',
                'price' => 5.00,
                'type' => 1,
                'gender' => 1,
                'glow_front_left_image' => 'glow_mask_shirt/Front_View/Cuffs/normal_left_shad.png',
                'mask_front_left_image' => 'glow_mask_shirt/Front_View/Cuffs/normal_left_mask.png',
                'highlighted_front_left_image' => 'glow_mask_shirt/Front_View/Cuffs/normal_left_hi.png',
                'glow_front_right_image' => 'glow_mask_shirt/Front_View/Cuffs/normal_right_shad.png',
                'mask_front_right_image' => 'glow_mask_shirt/Front_View/Cuffs/normal_right_mask.png',
                'highlighted_front_right_image' => 'glow_mask_shirt/Front_View/Cuffs/normal_right_hi.png',
                'glow_front_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_glow.png',
                'mask_front_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png',
                'highlighted_front_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png',
                'glow_front_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_glow.png',
                'mask_front_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png',
                'highlighted_front_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png',
                'glow_side_left_image' => 'glow_mask_shirt/Side_View/Cuffs/Normal_left_shad.png',
                'mask_side_left_image' => 'glow_mask_shirt/Side_View/Cuffs/Normal_left_mask.png',
                'highlighted_side_left_image' => 'glow_mask_shirt/Side_View/Cuffs/Normal_left_hi.png',
                'glow_side_right_image' => 'glow_mask_shirt/Side_View/Cuffs/Normal_right_shad.png',
                'mask_side_right_image' => 'glow_mask_shirt/Side_View/Cuffs/Normal_right_mask.png',
                'highlighted_side_right_image' => 'glow_mask_shirt/Side_View/Cuffs/Normal_right_hi.png',
                'glow_side_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_glow.png',
                'mask_side_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png',
                'highlighted_side_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png',
                'glow_side_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_glow.png',
                'mask_side_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png',
                'highlighted_side_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png',
                'glow_back_left_image' => 'glow_mask_shirt/Back_View/Cuffs/NormalStraight_left_shad.png',
                'mask_back_left_image' => 'glow_mask_shirt/Back_View/Cuffs/NormalStraight_left_mask.png',
                'highlighted_back_left_image' => 'glow_mask_shirt/Back_View/Cuffs/NormalStraight_left_hi.png',
                'glow_back_right_image' => 'glow_mask_shirt/Back_View/Cuffs/NormalStraight_right_shad.png',
                'mask_back_right_image' => 'glow_mask_shirt/Back_View/Cuffs/NormalStraight_right_mask.png',
                'highlighted_back_right_image' => 'glow_mask_shirt/Back_View/Cuffs/NormalStraight_right_hi.png',
                'glow_back_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_glow.png',
                'mask_back_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png',
                'highlighted_back_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png',
                'glow_back_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_glow.png',
                'mask_back_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png',
                'highlighted_back_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png',
                'glow_fold_main_image' => 'glow_mask_shirt/Folded_View/Cuffs/NormalStraight_Main_shad.png',
                'mask_fold_main_image' => 'glow_mask_shirt/Folded_View/Cuffs/NormalStraight_Main_mask.png',
                'highlighted_fold_main_image' => 'glow_mask_shirt/Folded_View/Cuffs/NormalStraight_Main_hi.png',
                'glow_fold_inner_image' => 'glow_mask_shirt/Folded_View/Cuffs/NormalStraight_Inner_shad.png',
                'mask_fold_inner_image' => 'glow_mask_shirt/Folded_View/Cuffs/NormalStraight_Inner_mask.png',
                'highlighted_fold_inner_image' => 'glow_mask_shirt/Folded_View/Cuffs/NormalStraight_Inner_hi.png',
                'back_button_image' => null,
                'fold_button_image' => null,
                'back_thread_image' => null,
                'fold_thread_image' => null,
                'status' => 1
            ],
                [
                'title' => 'Single cuff 2 buttons',
                'class' => 'icon-Cuff_two_button_straight8',
                'price' => 5.00,
                'type' => 1,
                'gender' => 1,
                'glow_front_left_image' => 'glow_mask_shirt/Front_View/Cuffs/normal_left_shad.png',
                'mask_front_left_image' => 'glow_mask_shirt/Front_View/Cuffs/normal_left_mask.png',
                'highlighted_front_left_image' => 'glow_mask_shirt/Front_View/Cuffs/normal_left_hi.png',
                'glow_front_right_image' => 'glow_mask_shirt/Front_View/Cuffs/normal_right_shad.png',
                'mask_front_right_image' => 'glow_mask_shirt/Front_View/Cuffs/normal_right_mask.png',
                'highlighted_front_right_image' => 'glow_mask_shirt/Front_View/Cuffs/normal_right_hi.png',
                'glow_front_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_glow.png',
                'mask_front_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png',
                'highlighted_front_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png',
                'glow_front_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_glow.png',
                'mask_front_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png',
                'highlighted_front_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png',
                'glow_side_left_image' => 'glow_mask_shirt/Side_View/Cuffs/Normal_left_shad.png',
                'mask_side_left_image' => 'glow_mask_shirt/Side_View/Cuffs/Normal_left_mask.png',
                'highlighted_side_left_image' => 'glow_mask_shirt/Side_View/Cuffs/Normal_left_hi.png',
                'glow_side_right_image' => 'glow_mask_shirt/Side_View/Cuffs/Normal_right_shad.png',
                'mask_side_right_image' => 'glow_mask_shirt/Side_View/Cuffs/Normal_right_mask.png',
                'highlighted_side_right_image' => 'glow_mask_shirt/Side_View/Cuffs/Normal_right_hi.png',
                'glow_side_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_glow.png',
                'mask_side_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png',
                'highlighted_side_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png',
                'glow_side_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_glow.png',
                'mask_side_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png',
                'highlighted_side_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png',
                'glow_back_left_image' => 'glow_mask_shirt/Back_View/Cuffs/NormalStraight_left_shad.png',
                'mask_back_left_image' => 'glow_mask_shirt/Back_View/Cuffs/NormalStraight_left_mask.png',
                'highlighted_back_left_image' => 'glow_mask_shirt/Back_View/Cuffs/NormalStraight_left_hi.png',
                'glow_back_right_image' => 'glow_mask_shirt/Back_View/Cuffs/NormalStraight_right_shad.png',
                'mask_back_right_image' => 'glow_mask_shirt/Back_View/Cuffs/NormalStraight_right_mask.png',
                'highlighted_back_right_image' => 'glow_mask_shirt/Back_View/Cuffs/NormalStraight_right_hi.png',
                'glow_back_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_glow.png',
                'mask_back_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png',
                'highlighted_back_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png',
                'glow_back_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_glow.png',
                'mask_back_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png',
                'highlighted_back_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png',
                'glow_fold_main_image' => 'glow_mask_shirt/Folded_View/Cuffs/NormalStraight_Main_shad.png',
                'mask_fold_main_image' => 'glow_mask_shirt/Folded_View/Cuffs/NormalStraight_Main_mask.png',
                'highlighted_fold_main_image' => 'glow_mask_shirt/Folded_View/Cuffs/NormalStraight_Main_hi.png',
                'glow_fold_inner_image' => 'glow_mask_shirt/Folded_View/Cuffs/NormalStraight_Inner_shad.png',
                'mask_fold_inner_image' => 'glow_mask_shirt/Folded_View/Cuffs/NormalStraight_Inner_mask.png',
                'highlighted_fold_inner_image' => 'glow_mask_shirt/Folded_View/Cuffs/NormalStraight_Inner_hi.png',
                'back_button_image' => null,
                'fold_button_image' => null,
                'back_thread_image' => null,
                'fold_thread_image' => null,
                'status' => 1
            ],
                [
                'title' => 'Squared French',
                'class' => 'icon-Cuff_squared_french9',
                'price' => 5.00,
                'type' => 1,
                'gender' => 1,
                'glow_front_left_image' => 'glow_mask_shirt/Front_View/Cuffs/French_left_shad.png',
                'mask_front_left_image' => 'glow_mask_shirt/Front_View/Cuffs/French_left_mask.png',
                'highlighted_front_left_image' => 'glow_mask_shirt/Front_View/Cuffs/French_left_hi.png',
                'glow_front_right_image' => 'glow_mask_shirt/Front_View/Cuffs/French_right_shad.png',
                'mask_front_right_image' => 'glow_mask_shirt/Front_View/Cuffs/French_right_mask.png',
                'highlighted_front_right_image' => 'glow_mask_shirt/Front_View/Cuffs/French_right_hi.png',
                'glow_front_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_glow.png',
                'mask_front_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png',
                'highlighted_front_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png',
                'glow_front_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_glow.png',
                'mask_front_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png',
                'highlighted_front_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png',
                'glow_side_left_image' => 'glow_mask_shirt/Side_View/Cuffs/Normal_left_shad.png',
                'mask_side_left_image' => 'glow_mask_shirt/Side_View/Cuffs/Normal_left_mask.png',
                'highlighted_side_left_image' => 'glow_mask_shirt/Side_View/Cuffs/Normal_left_hi.png',
                'glow_side_right_image' => 'glow_mask_shirt/Side_View/Cuffs/Normal_right_shad.png',
                'mask_side_right_image' => 'glow_mask_shirt/Side_View/Cuffs/Normal_right_mask.png',
                'highlighted_side_right_image' => 'glow_mask_shirt/Side_View/Cuffs/Normal_right_hi.png',
                'glow_side_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_glow.png',
                'mask_side_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png',
                'highlighted_side_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png',
                'glow_side_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_glow.png',
                'mask_side_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png',
                'highlighted_side_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png',
                'glow_back_left_image' => 'glow_mask_shirt/Back_View/Cuffs/FrenchStraight_left_shad.png',
                'mask_back_left_image' => 'glow_mask_shirt/Back_View/Cuffs/FrenchStraight_left_mask.png',
                'highlighted_back_left_image' => 'glow_mask_shirt/Back_View/Cuffs/FrenchStraight_left_hi.png',
                'glow_back_right_image' => 'glow_mask_shirt/Back_View/Cuffs/FrenchStraight_right_shad.png',
                'mask_back_right_image' => 'glow_mask_shirt/Back_View/Cuffs/FrenchStraight_right_mask.png',
                'highlighted_back_right_image' => 'glow_mask_shirt/Back_View/Cuffs/FrenchStraight_right_hi.png',
                'glow_back_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_glow.png',
                'mask_back_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png',
                'highlighted_back_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png',
                'glow_back_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_glow.png',
                'mask_back_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png',
                'highlighted_back_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png',
                'glow_fold_main_image' => 'glow_mask_shirt/Folded_View/Cuffs/FrenchStraight_Main_shad.png',
                'mask_fold_main_image' => 'glow_mask_shirt/Folded_View/Cuffs/FrenchStraight_Main_mask.png',
                'highlighted_fold_main_image' => 'glow_mask_shirt/Folded_View/Cuffs/FrenchStraight_Main_hi.png',
                'glow_fold_inner_image' => 'glow_mask_shirt/Folded_View/Cuffs/FrenchStraight_Inner_shad.png',
                'mask_fold_inner_image' => 'glow_mask_shirt/Folded_View/Cuffs/FrenchStraight_Inner_mask.png',
                'highlighted_fold_inner_image' => 'glow_mask_shirt/Folded_View/Cuffs/FrenchStraight_Inner_hi.png',
                'back_button_image' => null,
                'fold_button_image' => null,
                'back_thread_image' => null,
                'fold_thread_image' => null,
                'status' => 1
            ],
                [
                'title' => 'Two-button-cut',
                'class' => 'icon-Cuff_two_button_cut10',
                'price' => 5.00,
                'type' => 1,
                'gender' => 1,
                'glow_front_left_image' => 'glow_mask_shirt/Front_View/Cuffs/normal_left_shad.png',
                'mask_front_left_image' => 'glow_mask_shirt/Front_View/Cuffs/normal_left_mask.png',
                'highlighted_front_left_image' => 'glow_mask_shirt/Front_View/Cuffs/normal_left_hi.png',
                'glow_front_right_image' => 'glow_mask_shirt/Front_View/Cuffs/normal_right_shad.png',
                'mask_front_right_image' => 'glow_mask_shirt/Front_View/Cuffs/normal_right_mask.png',
                'highlighted_front_right_image' => 'glow_mask_shirt/Front_View/Cuffs/normal_right_hi.png',
                'glow_front_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_glow.png',
                'mask_front_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png',
                'highlighted_front_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png',
                'glow_front_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_glow.png',
                'mask_front_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png',
                'highlighted_front_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png',
                'glow_side_left_image' => 'glow_mask_shirt/Side_View/Cuffs/Normal_left_shad.png',
                'mask_side_left_image' => 'glow_mask_shirt/Side_View/Cuffs/Normal_left_mask.png',
                'highlighted_side_left_image' => 'glow_mask_shirt/Side_View/Cuffs/Normal_left_hi.png',
                'glow_side_right_image' => 'glow_mask_shirt/Side_View/Cuffs/Normal_right_shad.png',
                'mask_side_right_image' => 'glow_mask_shirt/Side_View/Cuffs/Normal_right_mask.png',
                'highlighted_side_right_image' => 'glow_mask_shirt/Side_View/Cuffs/Normal_right_hi.png',
                'glow_side_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_glow.png',
                'mask_side_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png',
                'highlighted_side_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png',
                'glow_side_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_glow.png',
                'mask_side_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png',
                'highlighted_side_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png',
                'glow_back_left_image' => 'glow_mask_shirt/Back_View/Cuffs/NormalCut_left_shad.png',
                'mask_back_left_image' => 'glow_mask_shirt/Back_View/Cuffs/NormalCut_left_mask.png',
                'highlighted_back_left_image' => 'glow_mask_shirt/Back_View/Cuffs/NormalCut_left_hi.png',
                'glow_back_right_image' => 'glow_mask_shirt/Back_View/Cuffs/NormalCut_right_shad.png',
                'mask_back_right_image' => 'glow_mask_shirt/Back_View/Cuffs/NormalCut_right_mask.png',
                'highlighted_back_right_image' => 'glow_mask_shirt/Back_View/Cuffs/NormalCut_right_hi.png',
                'glow_back_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_glow.png',
                'mask_back_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png',
                'highlighted_back_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png',
                'glow_back_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_glow.png',
                'mask_back_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png',
                'highlighted_back_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png',
                'glow_fold_main_image' => 'glow_mask_shirt/Folded_View/Cuffs/NormalCut_Main_shad.png',
                'mask_fold_main_image' => 'glow_mask_shirt/Folded_View/Cuffs/NormalCut_Main_mask.png',
                'highlighted_fold_main_image' => 'glow_mask_shirt/Folded_View/Cuffs/NormalCut_Main_hi.png',
                'glow_fold_inner_image' => 'glow_mask_shirt/Folded_View/Cuffs/NormalCut_Inner_shad.png',
                'mask_fold_inner_image' => 'glow_mask_shirt/Folded_View/Cuffs/NormalCut_Inner_mask.png',
                'highlighted_fold_inner_image' => 'glow_mask_shirt/Folded_View/Cuffs/NormalCut_Inner_hi.png',
                'back_button_image' => null,
                'fold_button_image' => null,
                'back_thread_image' => null,
                'fold_thread_image' => null,
                'status' => 1
            ],
                [
                'title' => 'Narrow',
                'class' => 'icon-Cuff_Narrow_11',
                'price' => 5.00,
                'type' => 1,
                'gender' => 1,
                'glow_front_left_image' => 'glow_mask_shirt/Front_View/Cuffs/Narrown_left_shad.png',
                'mask_front_left_image' => 'glow_mask_shirt/Front_View/Cuffs/Narrown_left_mask.png',
                'highlighted_front_left_image' => 'glow_mask_shirt/Front_View/Cuffs/Narrown_left_hi.png',
                'glow_front_right_image' => 'glow_mask_shirt/Front_View/Cuffs/Narrown_right_shad.png',
                'mask_front_right_image' => 'glow_mask_shirt/Front_View/Cuffs/Narrown_right_mask.png',
                'highlighted_front_right_image' => 'glow_mask_shirt/Front_View/Cuffs/Narrown_right_hi.png',
                'glow_front_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_glow.png',
                'mask_front_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png',
                'highlighted_front_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png',
                'glow_front_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_glow.png',
                'mask_front_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png',
                'highlighted_front_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png',
                'glow_side_left_image' => 'glow_mask_shirt/Side_View/Cuffs/Narrow_left_shad.png',
                'mask_side_left_image' => 'glow_mask_shirt/Side_View/Cuffs/Narrow_left_mask.png',
                'highlighted_side_left_image' => 'glow_mask_shirt/Side_View/Cuffs/Narrow_left_hi.png',
                'glow_side_right_image' => 'glow_mask_shirt/Side_View/Cuffs/Narrow_right_shad.png',
                'mask_side_right_image' => 'glow_mask_shirt/Side_View/Cuffs/Narrow_right_mask.png',
                'highlighted_side_right_image' => 'glow_mask_shirt/Side_View/Cuffs/Narrow_right_hi.png',
                'glow_side_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_glow.png',
                'mask_side_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png',
                'highlighted_side_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png',
                'glow_side_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_glow.png',
                'mask_side_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png',
                'highlighted_side_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png',
                'glow_back_left_image' => 'glow_mask_shirt/Back_View/Cuffs/NarrowStraight_left_shad.png',
                'mask_back_left_image' => 'glow_mask_shirt/Back_View/Cuffs/NarrowStraight_left_mask.png',
                'highlighted_back_left_image' => 'glow_mask_shirt/Back_View/Cuffs/NarrowStraight_left_hi.png',
                'glow_back_right_image' => 'glow_mask_shirt/Back_View/Cuffs/NarrowStraight_right_shad.png',
                'mask_back_right_image' => 'glow_mask_shirt/Back_View/Cuffs/NarrowStraight_right_mask.png',
                'highlighted_back_right_image' => 'glow_mask_shirt/Back_View/Cuffs/NarrowStraight_right_hi.png',
                'glow_back_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_glow.png',
                'mask_back_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png',
                'highlighted_back_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png',
                'glow_back_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_glow.png',
                'mask_back_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png',
                'highlighted_back_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png',
                'glow_fold_main_image' => 'glow_mask_shirt/Folded_View/Cuffs/NarrownStraight_Main_shad.png',
                'mask_fold_main_image' => 'glow_mask_shirt/Folded_View/Cuffs/NarrownStraight_Main_mask.png',
                'highlighted_fold_main_image' => 'glow_mask_shirt/Folded_View/Cuffs/NarrownStraight_Main_hi.png',
                'glow_fold_inner_image' => 'glow_mask_shirt/Folded_View/Cuffs/NarrownStraight_Inner_shad.png',
                'mask_fold_inner_image' => 'glow_mask_shirt/Folded_View/Cuffs/NarrownStraight_Inner_mask.png',
                'highlighted_fold_inner_image' => 'glow_mask_shirt/Folded_View/Cuffs/NarrownStraight_Inner_hi.png',
                'back_button_image' => null,
                'fold_button_image' => null,
                'back_thread_image' => null,
                'fold_thread_image' => null,
                'status' => 1
            ],
                [
                'title' => 'French Cut',
                'class' => 'icon-Cuff_French_Cut',
                'price' => 5.00,
                'type' => 1,
                'gender' => 1,
                'glow_front_left_image' => 'glow_mask_shirt/Front_View/Cuffs/French_left_shad.png',
                'mask_front_left_image' => 'glow_mask_shirt/Front_View/Cuffs/French_left_mask.png',
                'highlighted_front_left_image' => 'glow_mask_shirt/Front_View/Cuffs/French_left_hi.png',
                'glow_front_right_image' => 'glow_mask_shirt/Front_View/Cuffs/French_right_shad.png',
                'mask_front_right_image' => 'glow_mask_shirt/Front_View/Cuffs/French_right_mask.png',
                'highlighted_front_right_image' => 'glow_mask_shirt/Front_View/Cuffs/French_right_hi.png',
                'glow_front_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_glow.png',
                'mask_front_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png',
                'highlighted_front_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png',
                'glow_front_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_glow.png',
                'mask_front_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png',
                'highlighted_front_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png',
                'glow_side_left_image' => 'glow_mask_shirt/Side_View/Cuffs/FrenchCut_left_shad.png',
                'mask_side_left_image' => 'glow_mask_shirt/Side_View/Cuffs/FrenchCut_left_mask.png',
                'highlighted_side_left_image' => 'glow_mask_shirt/Side_View/Cuffs/FrenchCut_left_hi.png',
                'glow_side_right_image' => 'glow_mask_shirt/Side_View/Cuffs/FrenchCut_right_shad.png',
                'mask_side_right_image' => 'glow_mask_shirt/Side_View/Cuffs/FrenchCut_right_mask.png',
                'highlighted_side_right_image' => 'glow_mask_shirt/Side_View/Cuffs/FrenchCut_right_hi.png',
                'glow_side_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_glow.png',
                'mask_side_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png',
                'highlighted_side_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png',
                'glow_side_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_glow.png',
                'mask_side_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png',
                'highlighted_side_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png',
                'glow_back_left_image' => 'glow_mask_shirt/Back_View/Cuffs/FrenchCut_left_shad.png',
                'mask_back_left_image' => 'glow_mask_shirt/Back_View/Cuffs/FrenchCut_left_mask.png',
                'highlighted_back_left_image' => 'glow_mask_shirt/Back_View/Cuffs/FrenchCut_left_hi.png',
                'glow_back_right_image' => 'glow_mask_shirt/Back_View/Cuffs/FrenchCut_right_shad.png',
                'mask_back_right_image' => 'glow_mask_shirt/Back_View/Cuffs/FrenchCut_right_mask.png',
                'highlighted_back_right_image' => 'glow_mask_shirt/Back_View/Cuffs/FrenchCut_right_hi.png',
                'glow_back_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_glow.png',
                'mask_back_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png',
                'highlighted_back_opencuff_left_image' => 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png',
                'glow_back_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_glow.png',
                'mask_back_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png',
                'highlighted_back_opencuff_right_image' => 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png',
                'glow_fold_main_image' => 'glow_mask_shirt/Folded_View/Cuffs/FrenchCut_Main_shad.png',
                'mask_fold_main_image' => 'glow_mask_shirt/Folded_View/Cuffs/FrenchCut_Main_mask.png',
                'highlighted_fold_main_image' => 'glow_mask_shirt/Folded_View/Cuffs/FrenchCut_Main_hi.png',
                'glow_fold_inner_image' => 'glow_mask_shirt/Folded_View/Cuffs/FrenchCut_Inner_shad.png',
                'mask_fold_inner_image' => 'glow_mask_shirt/Folded_View/Cuffs/FrenchCut_Inner_mask.png',
                'highlighted_fold_inner_image' => 'glow_mask_shirt/Folded_View/Cuffs/FrenchCut_Inner_hi.png',
                'back_button_image' => null,
                'fold_button_image' => null,
                'back_thread_image' => null,
                'fold_thread_image' => null,
                'status' => 1,
            ]
        ];

        foreach ($ShirtCuffData as $data) {
            $this->_shirtCuff->create()->addData($data)->save();
        }


        //shirtcolor
        // $ShirtColorData = [
        //     [
        //         'title' => 'Loose',
        //         'class' => 'icon-Loose_Fit',
        //         'price' => 5.00,
        //         'gender' => 0,
        //         'glow_front_image' => 'glow_mask_shirt/Front_View/Fit/Shirt_Loosefit_Front_Shad.png',
        //         'mask_front_image' => 'glow_mask_shirt/Front_View/Fit/Shirt_Loosefit_Front_Mask.png',
        //         'highlighted_front_image' => 'glow_mask_shirt/Front_View/Fit/Shirt_Loosefit_Front_Hi.png',
        //         'glow_side_image' => 'glow_mask_shirt/Side_View/Fit/Shirt_Loosefit_Side_Shad.png',
        //         'mask_side_image' => 'glow_mask_shirt/Side_View/Fit/Shirt_Loosefit_Side_Mask.png',
        //         'highlighted_side_image' => 'glow_mask_shirt/Side_View/Fit/Shirt_Loosefit_Side_Hi.png',
        //         'glow_back_image' => 'glow_mask_shirt/Back_View/Fit/Shirt_Loosefit_Back_Shad.png',
        //         'mask_back_image' => 'glow_mask_shirt/Back_View/Fit/Shirt_Loosefit_Back_Mask.png',
        //         'highlighted_back_image' => 'glow_mask_shirt/Back_View/Fit/Shirt_Loosefit_Back_Hi.png',
        //         'glow_fold_image' => 'glow_mask_shirt/Folded_View/Fit/Shirt_Mainfit_Folded_Shad.png',
        //         'mask_fold_image' => 'glow_mask_shirt/Folded_View/Fit/Shirt_Mainfit_Folded_Mask.png',
        //         'highlighted_fold_image' => 'glow_mask_shirt/Folded_View/Fit/Shirt_Mainfit_Folded_Hi.png',
        //         'glow_zoom_image' => 'glow_mask_shirt/Zoom_View/Fit/Torso_Shad.png',
        //         'mask_zoom_image' => 'glow_mask_shirt/Zoom_View/Fit/Torso_Mask.png',
        //         'highlighted_zoom_image' => 'glow_mask_shirt/Zoom_View/Fit/Torso_Hi.png',
        //         'status' => 1
        //     ],
        //     [
        //         'title' => 'Waisted',
        //         'class' => 'icon-Slim_Fit',
        //         'price' => 5.00,
        //         'gender' => 0,
        //         'glow_front_image' => null,
        //         'mask_front_image' => null,
        //         'highlighted_front_image' => null,
        //         'glow_side_image' => null,
        //         'mask_side_image' => null,
        //         'highlighted_side_image' => null,
        //         'glow_back_image' => null,
        //         'mask_back_image' => null,
        //         'highlighted_back_image' => null,
        //         'glow_fold_image' => null,
        //         'mask_fold_image' => null,
        //         'highlighted_fold_image' => null,
        //         'glow_zoom_image' => null,
        //         'mask_zoom_image' => null,
        //         'highlighted_zoom_image' => null,
        //         'status' => 1,
        //     ]
        // ];
        // foreach ($ShirtColorData as $data) {
        //     $this->_shirtColor->create()->addData($data)->save();
        // }
        // shirtFit
        $ShirtFitData = [
                [
                'title' => 'Loose',
                'class' => 'icon-Loose_Fit',
                'price' => 5.00,
                'gender' => 0,
                'glow_front_image' => 'glow_mask_shirt/Front_View/Fit/Shirt_Loosefit_Front_Shad.png',
                'mask_front_image' => 'glow_mask_shirt/Front_View/Fit/Shirt_Loosefit_Front_Mask.png',
                'highlighted_front_image' => 'glow_mask_shirt/Front_View/Fit/Shirt_Loosefit_Front_Hi.png',
                'glow_side_image' => 'glow_mask_shirt/Side_View/Fit/Shirt_Loosefit_Side_Shad.png',
                'mask_side_image' => 'glow_mask_shirt/Side_View/Fit/Shirt_Loosefit_Side_Mask.png',
                'highlighted_side_image' => 'glow_mask_shirt/Side_View/Fit/Shirt_Loosefit_Side_Hi.png',
                'glow_back_image' => 'glow_mask_shirt/Back_View/Fit/Shirt_Loosefit_Back_Shad.png',
                'mask_back_image' => 'glow_mask_shirt/Back_View/Fit/Shirt_Loosefit_Back_Mask.png',
                'highlighted_back_image' => 'glow_mask_shirt/Back_View/Fit/Shirt_Loosefit_Back_Hi.png',
                'glow_fold_image' => 'glow_mask_shirt/Folded_View/Fit/Shirt_Mainfit_Folded_Shad.png',
                'mask_fold_image' => 'glow_mask_shirt/Folded_View/Fit/Shirt_Mainfit_Folded_Mask.png',
                'highlighted_fold_image' => 'glow_mask_shirt/Folded_View/Fit/Shirt_Mainfit_Folded_Hi.png',
                'glow_zoom_image' => 'glow_mask_shirt/Zoom_View/Fit/Torso_Shad.png',
                'mask_zoom_image' => 'glow_mask_shirt/Zoom_View/Fit/Torso_Mask.png',
                'highlighted_zoom_image' => 'glow_mask_shirt/Zoom_View/Fit/Torso_Hi.png',
                'status' => 1
            ],
                [
                'title' => 'Waisted',
                'class' => 'icon-Slim_Fit',
                'price' => 5.00,
                'gender' => 0,
                'glow_front_image' => null,
                'mask_front_image' => null,
                'highlighted_front_image' => null,
                'glow_side_image' => null,
                'mask_side_image' => null,
                'highlighted_side_image' => null,
                'glow_back_image' => null,
                'mask_back_image' => null,
                'highlighted_back_image' => null,
                'glow_fold_image' => null,
                'mask_fold_image' => null,
                'highlighted_fold_image' => null,
                'glow_zoom_image' => null,
                'mask_zoom_image' => null,
                'highlighted_zoom_image' => null,
                'status' => 1,
            ]
        ];

        foreach ($ShirtFitData as $data) {
            $this->_shirtFit->create()->addData($data)->save();
        }

        // shirtmaterial

        $shirtMaterialData = [
                [
                'title' => 'Wool',
                'status' => 1
            ],
                [
                'title' => 'Corduroy',
                'status' => 1
            ],
                [
                'title' => 'linen',
                'status' => 1
            ],
                [
                'title' => 'Polyester',
                'status' => 1
            ],
                [
                'title' => 'Velvet',
                'status' => 1
            ],
                [
                'title' => 'Pure Cotton',
                'status' => 1,
            ]
        ];
        foreach ($shirtMaterialData as $data) {
            $this->_shirtMaterial->create()->addData($data)->save();
        }

        //shirtpattern
        $shirtPatternData = [
                [
                'title' => 'Squared',
                'status' => 1
            ],
                [
                'title' => 'Plain',
                'status' => 1
            ],
                [
                'title' => 'Striped',
                'status' => 1
            ],
                [
                'title' => 'Chevron pattern',
                'status' => 1
            ],
                [
                'title' => 'Houndstooth ',
                'status' => 1
            ],
                [
                'title' => 'paisley',
                'status' => 1
            ],
                [
                'title' => 'herringbone',
                'status' => 1
            ],
                [
                'title' => 'Other',
                'status' => 1,
            ]
        ];
        foreach ($shirtPatternData as $data) {
            $this->_shirtPattern->create()->addData($data)->save();
        }


        //shirtplackets
        $shirtPlacketsData = [
                [
                'title' => 'Standard',
                'class' => 'icon-Standard_Placket',
                'price' => 5.00,
                'thumb' => null,
                'glow_front_image' => 'glow_mask_shirt/Front_View/Plackets/Shirt_Placket_Front_shad.png',
                'mask_front_image' => 'glow_mask_shirt/Front_View/Plackets/Shirt_Placket_Front_mask.png',
                'highlighted_front_image' => 'glow_mask_shirt/Front_View/Plackets/Shirt_Placket_Front_hi.png',
                'glow_side_image' => 'glow_mask_shirt/Side_View/Plackets/Shirt_Placket_Side_shad.png',
                'mask_side_image' => 'glow_mask_shirt/Side_View/Plackets/Shirt_Placket_Side_mask.png',
                'highlighted_side_image' => 'glow_mask_shirt/Side_View/Plackets/Shirt_Placket_Side_hi.png',
                'glow_fold_image' => 'glow_mask_shirt/Folded_View/Plackets/Shirt_Placket_Folded_shad.png',
                'mask_fold_image' => 'glow_mask_shirt/Folded_View/Plackets/Shirt_Placket_Folded_mask.png',
                'highlighted_fold_image' => 'glow_mask_shirt/Folded_View/Plackets/Shirt_Placket_Folded_hi.png',
                'status' => 1
            ],
                [
                'title' => 'Hidden Buttons',
                'class' => 'icon-Hidden_Button_Placket',
                'price' => 5.00,
                'thumb' => null,
                'glow_front_image' => null,
                'mask_front_image' => null,
                'highlighted_front_image' => null,
                'glow_side_image' => null,
                'mask_side_image' => null,
                'highlighted_side_image' => null,
                'glow_fold_image' => null,
                'mask_fold_image' => null,
                'highlighted_fold_image' => null,
                'status' => 1
            ],
                [
                'title' => 'French',
                'class' => 'icon-French_Placket',
                'price' => 5.00,
                'thumb' => null,
                'glow_front_image' => null,
                'mask_front_image' => null,
                'highlighted_front_image' => null,
                'glow_side_image' => null,
                'mask_side_image' => null,
                'highlighted_side_image' => null,
                'glow_fold_image' => null,
                'mask_fold_image' => null,
                'highlighted_fold_image' => null,
                'status' => 1,
            ]
        ];

        foreach ($shirtPlacketsData as $data) {
            $this->_shirtPlackets->create()->addData($data)->save();
        }


        //shirtpleats
        $shirtPleatsData = [
                [
                'title' => 'No Pleats',
                'class' => 'icon-No_Pleats1',
                'price' => 0.00,
                'image' => null,
                'status' => 1
            ],
                [
                'title' => 'Box pleat',
                'class' => 'icon-Box_Pleats2',
                'price' => 10.00,
                'image' => 'shirt_images/pleats/Shirt_pleats_2_Back.png',
                'status' => 1
            ],
                [
                'title' => 'Side folds',
                'class' => 'icon-Side_Folds3',
                'price' => 10.00,
                'image' => 'shirt_images/pleats/Shirt_pleats_3_Back.png',
                'status' => 1,
            ]
        ];

        foreach ($shirtPleatsData as $data) {
            $this->_shirtPleats->create()->addData($data)->save();
        }


        //shirtpocket
        $shirtPocketData = [
                [
                'title' => 'No Pocket',
                'class' => 'icon-No_pocket_Icon',
                'price' => 5.00,
                'type' => 1,
                'thumb' => 'glow_mask/pocket/Pockets_Regular_Flaps_Main_Glow.png',
                'glow_front_left_image' => null,
                'mask_front_left_image' => null,
                'highlighted_front_left_image' => null,
                'glow_front_right_image' => null,
                'mask_front_right_image' => null,
                'highlighted_front_right_image' => null,
                'glow_side_left_image' => null,
                'mask_side_left_image' => null,
                'highlighted_side_left_image' => null,
                'glow_side_right_image' => null,
                'mask_side_right_image' => null,
                'highlighted_side_right_image' => null,
                'glow_fold_left_image' => null,
                'mask_fold_left_image' => null,
                'highlighted_fold_left_image' => null,
                'glow_fold_right_image' => null,
                'mask_fold_right_image' => null,
                'highlighted_fold_right_image' => null,
                'status' => 1
            ],
                [
                'title' => 'Left Flap Pockets',
                'class' => 'icon-left_flap_pocket',
                'price' => 5.00,
                'type' => 1,
                'thumb' => 'glow_mask/pocket/Pockets_Regular_Flaps_Main_Glow.png',
                'glow_front_left_image' => 'glow_mask_shirt/Front_View/Pockets/Flap_left_shad.png',
                'mask_front_left_image' => 'glow_mask_shirt/Front_View/Pockets/Flap_left_mask.png',
                'highlighted_front_left_image' => 'glow_mask_shirt/Front_View/Pockets/Flap_left_hi.png',
                'glow_front_right_image' => null,
                'mask_front_right_image' => null,
                'highlighted_front_right_image' => null,
                'glow_side_left_image' => 'glow_mask_shirt/Side_View/Pockets/FlapPocket_Left_shad.png',
                'mask_side_left_image' => 'glow_mask_shirt/Side_View/Pockets/FlapPocket_Left_mask.png',
                'highlighted_side_left_image' => 'glow_mask_shirt/Side_View/Pockets/FlapPocket_Left_hi.png',
                'glow_side_right_image' => null,
                'mask_side_right_image' => null,
                'highlighted_side_right_image' => null,
                'glow_fold_left_image' => 'glow_mask_shirt/Folded_View/Pockets/FlappedPockets_left_Folded_shad.png',
                'mask_fold_left_image' => 'glow_mask_shirt/Folded_View/Pockets/FlappedPockets_left_Folded_mask.png',
                'highlighted_fold_left_image' => 'glow_mask_shirt/Folded_View/Pockets/FlappedPockets_left_Folded_hi.png',
                'glow_fold_right_image' => null,
                'mask_fold_right_image' => null,
                'highlighted_fold_right_image' => null,
                'status' => 1
            ],
                [
                'title' => 'Left Without flaps',
                'class' => ' icon-Withoutflap_Pocket_Left',
                'price' => 5.00,
                'type' => 1,
                'thumb' => 'glow_mask/pocket/Pockets_Regular_Flaps_Main_Glow.png',
                'glow_front_left_image' => 'glow_mask_shirt/Front_View/Pockets/WithoutFlap_left_shad.png',
                'mask_front_left_image' => 'glow_mask_shirt/Front_View/Pockets/WithoutFlap_left_mask.png',
                'highlighted_front_left_image' => 'glow_mask_shirt/Front_View/Pockets/WithoutFlap_left_hi.png',
                'glow_front_right_image' => null,
                'mask_front_right_image' => null,
                'highlighted_front_right_image' => null,
                'glow_side_left_image' => 'glow_mask_shirt/Side_View/Pockets/WithoutFlap_Left_shad.png',
                'mask_side_left_image' => 'glow_mask_shirt/Side_View/Pockets/WithoutFlap_Left_mask.png',
                'highlighted_side_left_image' => 'glow_mask_shirt/Side_View/Pockets/WithoutFlap_Left_hi.png',
                'glow_side_right_image' => null,
                'mask_side_right_image' => null,
                'highlighted_side_right_image' => null,
                'glow_fold_left_image' => 'glow_mask_shirt/Folded_View/Pockets/WithoutFlap_left_Folded_shad.png',
                'mask_fold_left_image' => 'glow_mask_shirt/Folded_View/Pockets/WithoutFlap_left_Folded_mask.png',
                'highlighted_fold_left_image' => 'glow_mask_shirt/Folded_View/Pockets/WithoutFlap_left_Folded_hi.png',
                'glow_fold_right_image' => null,
                'mask_fold_right_image' => null,
                'highlighted_fold_right_image' => null,
                'status' => 1
            ],
                [
                'title' => 'Right Flap Pockets',
                'class' => 'icon-right_flap_pocket',
                'price' => 5.00,
                'type' => 1,
                'thumb' => 'glow_mask/pocket/Pockets_Regular_Flaps_Main_Glow.png',
                'glow_front_left_image' => null,
                'mask_front_left_image' => null,
                'highlighted_front_left_image' => null,
                'glow_front_right_image' => 'glow_mask_shirt/Front_View/Pockets/Flap_right_shad.png',
                'mask_front_right_image' => 'glow_mask_shirt/Front_View/Pockets/Flap_right_mask.png',
                'highlighted_front_right_image' => 'glow_mask_shirt/Front_View/Pockets/Flap_right_hi.png',
                'glow_side_left_image' => null,
                'mask_side_left_image' => null,
                'highlighted_side_left_image' => null,
                'glow_side_right_image' => 'glow_mask_shirt/Side_View/Pockets/FlapPocket_Right_shad.png',
                'mask_side_right_image' => 'glow_mask_shirt/Side_View/Pockets/FlapPocket_Right_mask.png',
                'highlighted_side_right_image' => 'glow_mask_shirt/Side_View/Pockets/FlapPocket_Right_hi.png',
                'glow_fold_left_image' => null,
                'mask_fold_left_image' => null,
                'highlighted_fold_left_image' => null,
                'glow_fold_right_image' => 'glow_mask_shirt/Folded_View/Pockets/FlappedPockets_right_Folded_shad.png',
                'mask_fold_right_image' => 'glow_mask_shirt/Folded_View/Pockets/FlappedPockets_right_Folded_mask.png',
                'highlighted_fold_right_image' => 'glow_mask_shirt/Folded_View/Pockets/FlappedPockets_right_Folded_hi.png',
                'status' => 1,
            ],
                [
                'title' => 'Right Without flaps',
                'class' => 'icon-Withoutflap_Pocket_Right',
                'price' => 5.00,
                'type' => 1,
                'thumb' => 'glow_mask/pocket/Pockets_Regular_Flaps_Main_Glow.png',
                'glow_front_left_image' => null,
                'mask_front_left_image' => null,
                'highlighted_front_left_image' => null,
                'glow_front_right_image' => 'glow_mask_shirt/Front_View/Pockets/WithoutFlap_right_shad.png',
                'mask_front_right_image' => 'glow_mask_shirt/Front_View/Pockets/WithoutFlap_right_mask.png',
                'highlighted_front_right_image' => 'glow_mask_shirt/Front_View/Pockets/WithoutFlap_right_hi.png',
                'glow_side_left_image' => null,
                'mask_side_left_image' => null,
                'highlighted_side_left_image' => null,
                'glow_side_right_image' => 'glow_mask_shirt/Side_View/Pockets/WithoutFlap_Right_shad.png',
                'mask_side_right_image' => 'glow_mask_shirt/Side_View/Pockets/WithoutFlap_Right_mask.png',
                'highlighted_side_right_image' => 'glow_mask_shirt/Side_View/Pockets/WithoutFlap_Right_hi.png',
                'glow_fold_left_image' => null,
                'mask_fold_left_image' => null,
                'highlighted_fold_left_image' => null,
                'glow_fold_right_image' => 'glow_mask_shirt/Folded_View/Pockets/WithoutFlap_right_Folded_shad.png',
                'mask_fold_right_image' => 'glow_mask_shirt/Folded_View/Pockets/WithoutFlap_right_Folded_mask.png',
                'highlighted_fold_right_image' => 'glow_mask_shirt/Folded_View/Pockets/WithoutFlap_right_Folded_hi.png',
                'status' => 1
            ],
                [
                'title' => 'Flap PocketsX2',
                'class' => 'icon-flap_pocket_x2',
                'price' => 5.00,
                'type' => 1,
                'thumb' => 'glow_mask/pocket/Pockets_Regular_Flaps_Main_Glow.png',
                'glow_front_left_image' => 'glow_mask_shirt/Front_View/Pockets/Flap_left_shad.png',
                'mask_front_left_image' => 'glow_mask_shirt/Front_View/Pockets/Flap_left_mask.png',
                'highlighted_front_left_image' => 'glow_mask_shirt/Front_View/Pockets/Flap_left_hi.png',
                'glow_front_right_image' => 'glow_mask_shirt/Front_View/Pockets/Flap_right_shad.png',
                'mask_front_right_image' => 'glow_mask_shirt/Front_View/Pockets/Flap_right_mask.png',
                'highlighted_front_right_image' => 'glow_mask_shirt/Front_View/Pockets/Flap_right_hi.png',
                'glow_side_left_image' => 'glow_mask_shirt/Side_View/Pockets/FlapPocket_Left_shad.png',
                'mask_side_left_image' => 'glow_mask_shirt/Side_View/Pockets/FlapPocket_Left_mask.png',
                'highlighted_side_left_image' => 'glow_mask_shirt/Side_View/Pockets/FlapPocket_Left_hi.png',
                'glow_side_right_image' => 'glow_mask_shirt/Side_View/Pockets/FlapPocket_Right_shad.png',
                'mask_side_right_image' => 'glow_mask_shirt/Side_View/Pockets/FlapPocket_Right_mask.png',
                'highlighted_side_right_image' => 'glow_mask_shirt/Side_View/Pockets/FlapPocket_Right_hi.png',
                'glow_fold_left_image' => 'glow_mask_shirt/Folded_View/Pockets/FlappedPockets_left_Folded_shad.png',
                'mask_fold_left_image' => 'glow_mask_shirt/Folded_View/Pockets/FlappedPockets_left_Folded_mask.png',
                'highlighted_fold_left_image' => 'glow_mask_shirt/Folded_View/Pockets/FlappedPockets_left_Folded_hi.png',
                'glow_fold_right_image' => 'glow_mask_shirt/Folded_View/Pockets/FlappedPockets_right_Folded_shad.png',
                'mask_fold_right_image' => 'glow_mask_shirt/Folded_View/Pockets/FlappedPockets_right_Folded_mask.png',
                'highlighted_fold_right_image' => 'glow_mask_shirt/Folded_View/Pockets/FlappedPockets_right_Folded_hi.png',
                'status' => 1
            ],
                [
                'title' => 'Without flapsX2',
                'class' => 'icon-Withoutflap_Pocket_x2',
                'price' => 5.00,
                'type' => 1,
                'thumb' => 'glow_mask/pocket/Pockets_Regular_Flaps_Main_Glow.png',
                'glow_front_left_image' => 'glow_mask_shirt/Front_View/Pockets/WithoutFlap_left_shad.png',
                'mask_front_left_image' => 'glow_mask_shirt/Front_View/Pockets/WithoutFlap_left_mask.png',
                'highlighted_front_left_image' => 'glow_mask_shirt/Front_View/Pockets/WithoutFlap_left_hi.png',
                'glow_front_right_image' => 'glow_mask_shirt/Front_View/Pockets/WithoutFlap_right_shad.png',
                'mask_front_right_image' => 'glow_mask_shirt/Front_View/Pockets/WithoutFlap_right_mask.png',
                'highlighted_front_right_image' => 'glow_mask_shirt/Front_View/Pockets/WithoutFlap_right_hi.png',
                'glow_side_left_image' => 'glow_mask_shirt/Side_View/Pockets/WithoutFlap_Left_shad.png',
                'mask_side_left_image' => 'glow_mask_shirt/Side_View/Pockets/WithoutFlap_Left_mask.png',
                'highlighted_side_left_image' => 'glow_mask_shirt/Side_View/Pockets/WithoutFlap_Left_hi.png',
                'glow_side_right_image' => 'glow_mask_shirt/Side_View/Pockets/WithoutFlap_Right_shad.png',
                'mask_side_right_image' => 'glow_mask_shirt/Side_View/Pockets/WithoutFlap_Right_mask.png',
                'highlighted_side_right_image' => 'glow_mask_shirt/Side_View/Pockets/WithoutFlap_Right_hi.png',
                'glow_fold_left_image' => 'glow_mask_shirt/Folded_View/Pockets/WithoutFlap_left_Folded_shad.png',
                'mask_fold_left_image' => 'glow_mask_shirt/Folded_View/Pockets/WithoutFlap_left_Folded_mask.png',
                'highlighted_fold_left_image' => 'glow_mask_shirt/Folded_View/Pockets/WithoutFlap_left_Folded_hi.png',
                'glow_fold_right_image' => 'glow_mask_shirt/Folded_View/Pockets/WithoutFlap_right_Folded_shad.png',
                'mask_fold_right_image' => 'glow_mask_shirt/Folded_View/Pockets/WithoutFlap_right_Folded_mask.png',
                'highlighted_fold_right_image' => 'glow_mask_shirt/Folded_View/Pockets/WithoutFlap_right_Folded_hi.png',
                'status' => 1,
            ]
        ];

        foreach ($shirtPocketData as $data) {
            $this->_shirtPocket->create()->addData($data)->save();
        }

        //shirtseason
        $shirtSeasonData = [
                [
                'title' => 'Summer',
                'status' => 1
            ],
                [
                'title' => 'Winter',
                'status' => 1
            ],
                [
                'title' => 'Year Round',
                'status' => 1,
            ]
        ];

        if (!$this->installer->getTableRow($this->installer->getTable('shirtseason'), 'shirtseason_id', 1)) {
            foreach ($shirtSeasonData as $data) {
                $this->_shirtSeason->create()->addData($data)->save();
            }
        }



        //shirtsleeves
        $shirtSleevesData = [
                [
                'title' => 'Long',
                'class' => 'icon-Sleeve_Full',
                'price' => 5.00,
                'type' => 1,
                'gender' => 0,
                'glow_front_left_image' => 'glow_mask_shirt/Front_View/Sleeves/Shirt_Fullsleeve_Front_Left_shad.png',
                'mask_front_left_image' => 'glow_mask_shirt/Front_View/Sleeves/Shirt_Fullsleeve_Front_Left_mask.png',
                'highlighted_front_left_image' => 'glow_mask_shirt/Front_View/Sleeves/Shirt_Fullsleeve_Front_Left_hi.png',
                'glow_front_right_image' => 'glow_mask_shirt/Front_View/Sleeves/Shirt_Fullsleeve_Front_right_shad.png',
                'mask_front_right_image' => 'glow_mask_shirt/Front_View/Sleeves/Shirt_Fullsleeve_Front_right_mask.png',
                'highlighted_front_right_image' => 'glow_mask_shirt/Front_View/Sleeves/Shirt_Fullsleeve_Front_right_hi.png',
                'glow_side_left_image' => 'glow_mask_shirt/Side_View/Sleeves/Shirt_Fullsleeve_Side_Left_shad.png',
                'mask_side_left_image' => 'glow_mask_shirt/Side_View/Sleeves/Shirt_Fullsleeve_Side_Left_mask.png',
                'highlighted_side_left_image' => 'glow_mask_shirt/Side_View/Sleeves/Shirt_Fullsleeve_Side_Left_hi.png',
                'glow_side_right_image' => 'glow_mask_shirt/Side_View/Sleeves/Shirt_Fullsleeve_Side_Right_shad.png',
                'mask_side_right_image' => 'glow_mask_shirt/Side_View/Sleeves/Shirt_Fullsleeve_Side_Right_mask.png',
                'highlighted_side_right_image' => 'glow_mask_shirt/Side_View/Sleeves/Shirt_Fullsleeve_Side_Right_hi.png',
                'glow_back_left_image' => 'glow_mask_shirt/Back_View/Sleeves/Full_left_shad.png',
                'mask_back_left_image' => 'glow_mask_shirt/Back_View/Sleeves/Full_left_mask.png',
                'highlighted_back_left_image' => 'glow_mask_shirt/Back_View/Sleeves/Full_left_hi.png',
                'glow_back_right_image' => 'glow_mask_shirt/Back_View/Sleeves/Full_right_shad.png',
                'mask_back_right_image' => 'glow_mask_shirt/Back_View/Sleeves/Full_right_mask.png',
                'highlighted_back_right_image' => 'glow_mask_shirt/Back_View/Sleeves/Full_right_hi.png',
                'glow_fold_image' => null,
                'mask_fold_image' => null,
                'highlighted_fold_image' => null,
                'status' => 1
            ],
                [
                'title' => 'Short',
                'class' => 'icon-Half_Sleeves',
                'price' => 5.00,
                'type' => 1,
                'gender' => 0,
                'glow_front_left_image' => 'glow_mask_shirt/Front_View/Sleeves/Shirt_Halfsleeve_Front_Left_shad.png',
                'mask_front_left_image' => 'glow_mask_shirt/Front_View/Sleeves/Shirt_Halfsleeve_Front_Left_mask.png',
                'highlighted_front_left_image' => 'glow_mask_shirt/Front_View/Sleeves/Shirt_Halfsleeve_Front_Left_hi.png',
                'glow_front_right_image' => 'glow_mask_shirt/Front_View/Sleeves/Shirt_Halfsleeve_Front_right_shad.png',
                'mask_front_right_image' => 'glow_mask_shirt/Front_View/Sleeves/Shirt_Halfsleeve_Front_right_mask.png',
                'highlighted_front_right_image' => 'glow_mask_shirt/Front_View/Sleeves/Shirt_Halfsleeve_Front_right_hi.png',
                'glow_side_left_image' => 'glow_mask_shirt/Side_View/Sleeves/Shirt_Halfsleeve_Side_Left_shad.png',
                'mask_side_left_image' => 'glow_mask_shirt/Side_View/Sleeves/Shirt_Halfsleeve_Side_Left_mask.png',
                'highlighted_side_left_image' => 'glow_mask_shirt/Side_View/Sleeves/Shirt_Halfsleeve_Side_Left_hi.png',
                'glow_side_right_image' => 'glow_mask_shirt/Side_View/Sleeves/Shirt_Halfsleeve_Side_Right_shad.png',
                'mask_side_right_image' => 'glow_mask_shirt/Side_View/Sleeves/Shirt_Halfsleeve_Side_Right_mask.png',
                'highlighted_side_right_image' => 'glow_mask_shirt/Side_View/Sleeves/Shirt_Halfsleeve_Side_Right_hi.png',
                'glow_back_left_image' => 'glow_mask_shirt/Back_View/Sleeves/Half_left_shad.png',
                'mask_back_left_image' => 'glow_mask_shirt/Back_View/Sleeves/Half_left_mask.png',
                'highlighted_back_left_image' => 'glow_mask_shirt/Back_View/Sleeves/Half_left_hi.png',
                'glow_back_right_image' => 'glow_mask_shirt/Back_View/Sleeves/Half_right_shad.png',
                'mask_back_right_image' => 'glow_mask_shirt/Back_View/Sleeves/Half_right_mask.png',
                'highlighted_back_right_image' => 'glow_mask_shirt/Back_View/Sleeves/Half_right_hi.png',
                'glow_fold_image' => 'glow_mask_shirt/Folded_View/Sleeves/Shirt_Halfsleeves_Folded_shad.png',
                'mask_fold_image' => 'glow_mask_shirt/Folded_View/Sleeves/Shirt_Halfsleeves_Folded_mask.png',
                'highlighted_fold_image' => 'glow_mask_shirt/Folded_View/Sleeves/Shirt_Halfsleeves_Folded_hi.png',
                'status' => 1
            ],
                [
                'title' => 'Rollup',
                'class' => 'icon-Rollup',
                'price' => 5.00,
                'type' => 1,
                'gender' => 0,
                'glow_front_left_image' => 'glow_mask_shirt/Front_View/Sleeves/Rollup_Left_shad.png',
                'mask_front_left_image' => 'glow_mask_shirt/Front_View/Sleeves/Rollup_Left_mask.png',
                'highlighted_front_left_image' => 'glow_mask_shirt/Front_View/Sleeves/Rollup_Left_hi.png',
                'glow_front_right_image' => 'glow_mask_shirt/Front_View/Sleeves/Rollup_Right_shad.png',
                'mask_front_right_image' => 'glow_mask_shirt/Front_View/Sleeves/Rollup_Right_mask.png',
                'highlighted_front_right_image' => 'glow_mask_shirt/Front_View/Sleeves/Rollup_Right_hi.png',
                'glow_side_left_image' => 'glow_mask_shirt/Side_View/Sleeves/Rollup_Left_shad.png',
                'mask_side_left_image' => 'glow_mask_shirt/Side_View/Sleeves/Rollup_Left_mask.png',
                'highlighted_side_left_image' => 'glow_mask_shirt/Side_View/Sleeves/Rollup_Left_hi.png',
                'glow_side_right_image' => 'glow_mask_shirt/Side_View/Sleeves/Rollup_Right_shad.png',
                'mask_side_right_image' => 'glow_mask_shirt/Side_View/Sleeves/Rollup_Right_mask.png',
                'highlighted_side_right_image' => 'glow_mask_shirt/Side_View/Sleeves/Rollup_Right_hi.png',
                'glow_back_left_image' => 'glow_mask_shirt/Back_View/Sleeves/Rollup_left_shad.png',
                'mask_back_left_image' => 'glow_mask_shirt/Back_View/Sleeves/Rollup_left_mask.png',
                'highlighted_back_left_image' => 'glow_mask_shirt/Back_View/Sleeves/Rollup_left_hi.png',
                'glow_back_right_image' => 'glow_mask_shirt/Back_View/Sleeves/Rollup_right_shad.png',
                'mask_back_right_image' => 'glow_mask_shirt/Back_View/Sleeves/Rollup_right_mask.png',
                'highlighted_back_right_image' => 'glow_mask_shirt/Back_View/Sleeves/Rollup_right_hi.png',
                'glow_fold_image' => null,
                'mask_fold_image' => null,
                'highlighted_fold_image' => null,
                'status' => 1,
            ]
        ];

        foreach ($shirtSleevesData as $data) {
            $this->_shirtSleeves->create()->addData($data)->save();
        }

        //shirtstyle
        $shirtStyleData = [
                [
                'title' => 'Rounded collar',
                'class' => 'icon-collar_rounded',
                'price' => 40.00,
                'type' => 0,
                'thumb' => 'glow_mask/style/Style_Mandarin.png',
                'view1_glow_left_image' => 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Roundedcollar_Front_Left_shad.png',
                'view1_mask_left_image' => 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Roundedcollar_Front_Left_mask.png',
                'view1_highlighted_left_image' => 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Roundedcollar_Front_Left_hi.png',
                'view1_glow_right_image' => 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Roundedcollar_Front_Right_shad.png',
                'view1_mask_right_image' => 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Roundedcollar_Front_Right_mask.png',
                'view1_highlighted_right_image' => 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Roundedcollar_Front_Right_hi.png',
                'view2_glow_left_image' => 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Roundedcollar_Side_Left_shad.png',
                'view2_mask_left_image' => 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Roundedcollar_Side_Left_mask.png',
                'view2_highlighted_left_image' => 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Roundedcollar_Side_Left_hi.png',
                'view2_glow_right_image' => 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Roundedcollar_Side_Right_shad.png',
                'view2_mask_right_image' => 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Roundedcollar_Side_Right_mask.png',
                'view2_highlighted_right_image' => 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Roundedcollar_Side_Right_hi.png',
                'view3_glow_back_image' => null,
                'view3_mask_back_image' => null,
                'view3_highlighted_back_image' => null,
                'view4_glow_left_image' => 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Roundedcollar_Folded_Left_shad.png',
                'view4_mask_left_image' => 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Roundedcollar_Folded_Left_mask.png',
                'view4_highlighted_left_image' => 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Roundedcollar_Folded_Left_hi.png',
                'view4_glow_right_image' => 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Roundedcollar_Folded_Right_shad.png',
                'view4_mask_right_image' => 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Roundedcollar_Folded_Right_mask.png',
                'view4_highlighted_right_image' => 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Roundedcollar_Folded_Right_hi.png',
                'view5_glow_left_image' => 'glow_mask_shirt/Zoom_View/Collar_Styles/RoundedCollar_Left_shad.png',
                'view5_mask_left_image' => 'glow_mask_shirt/Zoom_View/Collar_Styles/RoundedCollar_Left_mask.png',
                'view5_highlighted_left_image' => 'glow_mask_shirt/Zoom_View/Collar_Styles/RoundedCollar_Left_hi.png',
                'view5_glow_right_image' => 'glow_mask_shirt/Zoom_View/Collar_Styles/RoundedCollar_Right_shad.png',
                'view5_mask_right_image' => 'glow_mask_shirt/Zoom_View/Collar_Styles/RoundedCollar_Right_mask.png',
                'view5_highlighted_right_image' => 'glow_mask_shirt/Zoom_View/Collar_Styles/RoundedCollar_Right_hi.png',
                'view1_casual_glow_left_image' => 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Rounded_Left_shad.png',
                'view1_casual_mask_left_image' => 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Rounded_Left_mask.png',
                'view1_casual_highlighted_left_image' => 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Rounded_Left_hi.png',
                'view1_casual_glow_right_image' => 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Rounded_Right_shad.png',
                'view1_casual_mask_right_image' => 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Rounded_Right_mask.png',
                'view1_casual_highlighted_right_image' => 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Rounded_Right_hi.png',
                'view2_casual_glow_left_image' => 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Rounded_Left_shad.png',
                'view2_casual_mask_left_image' => 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Rounded_Left_mask.png',
                'view2_casual_highlighted_left_image' => 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Rounded_Left_hi.png',
                'view2_casual_glow_right_image' => 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Rounded_Right_shad.png',
                'view2_casual_mask_right_image' => 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Rounded_Right_mask.png',
                'view2_casual_highlighted_right_image' => 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Rounded_Right_hi.png',
                'view4_glow_inner_image' => null,
                'view4_mask_inner_image' => null,
                'view4_highlighted_inner_image' => null,
                'view5_glow_inner_image' => null,
                'view5_mask_inner_image' => null,
                'view5_highlighted_inner_image' => null,
                'status' => 1,
                'created_time' => '2017-01-26 22:42:53',
                'update_time' => '2018-05-21 12:06:01'
            ],
                [
                'title' => 'Button down',
                'class' => 'icon-Collar_button_down',
                'price' => 5.00,
                'type' => 1,
                'thumb' => 'glow_mask/style/Style_1_button_Single_breasted.png',
                'view1_glow_left_image' => 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Buttondowncollar_Front_Left_shad.png',
                'view1_mask_left_image' => 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Buttondowncollar_Front_Left_mask.png',
                'view1_highlighted_left_image' => 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Buttondowncollar_Front_Left_hi.png',
                'view1_glow_right_image' => 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Buttondowncollar_Front_Right_shad.png',
                'view1_mask_right_image' => 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Buttondowncollar_Front_Right_mask.png',
                'view1_highlighted_right_image' => 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Buttondowncollar_Front_Right_hi.png',
                'view2_glow_left_image' => 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Buttondowncollar_Side_Left_shad.png',
                'view2_mask_left_image' => 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Buttondowncollar_Side_Left_mask.png',
                'view2_highlighted_left_image' => 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Buttondowncollar_Side_Left_hi.png',
                'view2_glow_right_image' => 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Buttondowncollar_Side_Right_shad.png',
                'view2_mask_right_image' => 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Buttondowncollar_Side_Right_mask.png',
                'view2_highlighted_right_image' => 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Buttondowncollar_Side_Right_hi.png',
                'view3_glow_back_image' => null,
                'view3_mask_back_image' => null,
                'view3_highlighted_back_image' => null,
                'view4_glow_left_image' => 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Buttondowncollar_Folded_Left_shad.png',
                'view4_mask_left_image' => 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Buttondowncollar_Folded_Left_mask.png',
                'view4_highlighted_left_image' => 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Buttondowncollar_Folded_Left_hi.png',
                'view4_glow_right_image' => 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Buttondowncollar_Folded_Right_shad.png',
                'view4_mask_right_image' => 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Buttondowncollar_Folded_Right_mask.png',
                'view4_highlighted_right_image' => 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Buttondowncollar_Folded_Right_hi.png',
                'view5_glow_left_image' => 'glow_mask_shirt/Zoom_View/Collar_Styles/ButtondownCollar_Left_shad.png',
                'view5_mask_left_image' => 'glow_mask_shirt/Zoom_View/Collar_Styles/ButtondownCollar_Left_mask.png',
                'view5_highlighted_left_image' => 'glow_mask_shirt/Zoom_View/Collar_Styles/ButtondownCollar_Left_hi.png',
                'view5_glow_right_image' => 'glow_mask_shirt/Zoom_View/Collar_Styles/ButtondownCollar_Right_shad.png',
                'view5_mask_right_image' => 'glow_mask_shirt/Zoom_View/Collar_Styles/ButtondownCollar_Right_mask.png',
                'view5_highlighted_right_image' => 'glow_mask_shirt/Zoom_View/Collar_Styles/ButtondownCollar_Right_hi.png',
                'view1_casual_glow_left_image' => 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Buttondown_Left_shad.png',
                'view1_casual_mask_left_image' => 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Buttondown_Left_mask.png',
                'view1_casual_highlighted_left_image' => 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Buttondown_Left_hi.png',
                'view1_casual_glow_right_image' => 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Buttondown_Right_shad.png',
                'view1_casual_mask_right_image' => 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Buttondown_Right_mask.png',
                'view1_casual_highlighted_right_image' => 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Buttondown_Right_hi.png',
                'view2_casual_glow_left_image' => 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Buttondown_Left_shad.png',
                'view2_casual_mask_left_image' => 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Buttondown_Left_mask.png',
                'view2_casual_highlighted_left_image' => 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Buttondown_Left_hi.png',
                'view2_casual_glow_right_image' => 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Buttondown_Right_shad.png',
                'view2_casual_mask_right_image' => 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Buttondown_Right_mask.png',
                'view2_casual_highlighted_right_image' => 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Buttondown_Right_hi.png',
                'view4_glow_inner_image' => null,
                'view4_mask_inner_image' => null,
                'view4_highlighted_inner_image' => null,
                'view5_glow_inner_image' => null,
                'view5_mask_inner_image' => null,
                'view5_highlighted_inner_image' => null,
                'status' => 1,
                'created_time' => '2017-01-26 22:41:50',
                'update_time' => '2018-05-21 12:06:51'
            ],
                [
                'title' => 'Cutaway collar',
                'class' => 'icon-Collar_Cutaway',
                'price' => 10.00,
                'type' => 2,
                'thumb' => 'glow_mask/style/Style_4_button_Double_breasted.png',
                'view1_glow_left_image' => 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Cutawaycollar_Front_Left_shad.png',
                'view1_mask_left_image' => 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Cutawaycollar_Front_Left_mask.png',
                'view1_highlighted_left_image' => 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Cutawaycollar_Front_Left_hi.png',
                'view1_glow_right_image' => 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Cutawaycollar_Front_Right_shad.png',
                'view1_mask_right_image' => 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Cutawaycollar_Front_Right_mask.png',
                'view1_highlighted_right_image' => 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Cutawaycollar_Front_Right_hi.png',
                'view2_glow_left_image' => 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Cutaway_Side_Left_shad.png',
                'view2_mask_left_image' => 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Cutaway_Side_Left_mask.png',
                'view2_highlighted_left_image' => 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Cutaway_Side_Left_hi.png',
                'view2_glow_right_image' => 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Cutaway_Side_Right_shad.png',
                'view2_mask_right_image' => 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Cutaway_Side_Right_mask.png',
                'view2_highlighted_right_image' => 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Cutaway_Side_Right_hi.png',
                'view3_glow_back_image' => null,
                'view3_mask_back_image' => null,
                'view3_highlighted_back_image' => null,
                'view4_glow_left_image' => 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Cutawaycollar_Folded_Left_shad.png',
                'view4_mask_left_image' => 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Cutawaycollar_Folded_Left_mask.png',
                'view4_highlighted_left_image' => 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Cutawaycollar_Folded_Left_hi.png',
                'view4_glow_right_image' => 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Cutawaycollar_Folded_Right_shad.png',
                'view4_mask_right_image' => 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Cutawaycollar_Folded_Right_mask.png',
                'view4_highlighted_right_image' => 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Cutawaycollar_Folded_Right_hi.png',
                'view5_glow_left_image' => 'glow_mask_shirt/Zoom_View/Collar_Styles/CutawayCollar_Left_shad.png',
                'view5_mask_left_image' => 'glow_mask_shirt/Zoom_View/Collar_Styles/CutawayCollar_Left_mask.png',
                'view5_highlighted_left_image' => 'glow_mask_shirt/Zoom_View/Collar_Styles/CutawayCollar_Left_hi.png',
                'view5_glow_right_image' => 'glow_mask_shirt/Zoom_View/Collar_Styles/CutawayCollar_Right_shad.png',
                'view5_mask_right_image' => 'glow_mask_shirt/Zoom_View/Collar_Styles/CutawayCollar_Right_mask.png',
                'view5_highlighted_right_image' => 'glow_mask_shirt/Zoom_View/Collar_Styles/CutawayCollar_Right_hi.png',
                'view1_casual_glow_left_image' => 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Cutaway_Left_shad.png',
                'view1_casual_mask_left_image' => 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Cutaway_Left_mask.png',
                'view1_casual_highlighted_left_image' => 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Cutaway_Left_hi.png',
                'view1_casual_glow_right_image' => 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Cutaway_Right_shad.png',
                'view1_casual_mask_right_image' => 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Cutaway_Right_mask.png',
                'view1_casual_highlighted_right_image' => 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Cutaway_Right_hi.png',
                'view2_casual_glow_left_image' => 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Cutaway_Left_shad.png',
                'view2_casual_mask_left_image' => 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Cutaway_Left_mask.png',
                'view2_casual_highlighted_left_image' => 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Cutaway_Left_hi.png',
                'view2_casual_glow_right_image' => 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Cutaway_Right_shad.png',
                'view2_casual_mask_right_image' => 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Cutaway_Right_mask.png',
                'view2_casual_highlighted_right_image' => 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Cutaway_Right_hi.png',
                'view4_glow_inner_image' => null,
                'view4_mask_inner_image' => null,
                'view4_highlighted_inner_image' => null,
                'view5_glow_inner_image' => null,
                'view5_mask_inner_image' => null,
                'view5_highlighted_inner_image' => null,
                'status' => 1,
                'created_time' => '2017-02-02 00:20:06',
                'update_time' => '2018-05-21 12:07:11'
            ],
                [
                'title' => 'Kent collar',
                'class' => 'icon-Collar_Kent',
                'price' => 15.00,
                'type' => 0,
                'thumb' => 'glow_mask/style/Style_Mandarin.png',
                'view1_glow_left_image' => 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Kentcollar_Front_Left_shad.png',
                'view1_mask_left_image' => 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Kentcollar_Front_Left_mask.png',
                'view1_highlighted_left_image' => 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Kentcollar_Front_Left_hi.png',
                'view1_glow_right_image' => 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Kentcollar_Front_Right_shad.png',
                'view1_mask_right_image' => 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Kentcollar_Front_Right_mask.png',
                'view1_highlighted_right_image' => 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Kentcollar_Front_Right_hi.png',
                'view2_glow_left_image' => 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Kent_Side_Left_shad.png',
                'view2_mask_left_image' => 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Kent_Side_Left_mask.png',
                'view2_highlighted_left_image' => 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Kent_Side_Left_hi.png',
                'view2_glow_right_image' => 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Kent_Side_Right_shad.png',
                'view2_mask_right_image' => 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Kent_Side_Right_mask.png',
                'view2_highlighted_right_image' => 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Kent_Side_Right_hi.png',
                'view3_glow_back_image' => null,
                'view3_mask_back_image' => null,
                'view3_highlighted_back_image' => null,
                'view4_glow_left_image' => 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Kentcollar_Folded_Left_shad.png',
                'view4_mask_left_image' => 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Kentcollar_Folded_Left_mask.png',
                'view4_highlighted_left_image' => 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Kentcollar_Folded_Left_hi.png',
                'view4_glow_right_image' => 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Kentcollar_Folded_Right_shad.png',
                'view4_mask_right_image' => 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Kentcollar_Folded_Right_mask.png',
                'view4_highlighted_right_image' => 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Kentcollar_Folded_Right_hi.png',
                'view5_glow_left_image' => 'glow_mask_shirt/Zoom_View/Collar_Styles/KentCollar_Left_shad.png',
                'view5_mask_left_image' => 'glow_mask_shirt/Zoom_View/Collar_Styles/KentCollar_Left_mask.png',
                'view5_highlighted_left_image' => 'glow_mask_shirt/Zoom_View/Collar_Styles/KentCollar_Left_hi.png',
                'view5_glow_right_image' => 'glow_mask_shirt/Zoom_View/Collar_Styles/KentCollar_Right_shad.png',
                'view5_mask_right_image' => 'glow_mask_shirt/Zoom_View/Collar_Styles/KentCollar_Right_mask.png',
                'view5_highlighted_right_image' => 'glow_mask_shirt/Zoom_View/Collar_Styles/KentCollar_Right_hi.png',
                'view1_casual_glow_left_image' => 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Kent_Left_shad.png',
                'view1_casual_mask_left_image' => 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Kent_Left_mask.png',
                'view1_casual_highlighted_left_image' => 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Kent_Left_hi.png',
                'view1_casual_glow_right_image' => 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Kent_Right_shad.png',
                'view1_casual_mask_right_image' => 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Kent_Right_mask.png',
                'view1_casual_highlighted_right_image' => 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Kent_Right_hi.png',
                'view2_casual_glow_left_image' => 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Kent_Left_shad.png',
                'view2_casual_mask_left_image' => 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Kent_Left_mask.png',
                'view2_casual_highlighted_left_image' => 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Kent_Left_hi.png',
                'view2_casual_glow_right_image' => 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Kent_Right_shad.png',
                'view2_casual_mask_right_image' => 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Kent_Right_mask.png',
                'view2_casual_highlighted_right_image' => 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Kent_Right_hi.png',
                'view4_glow_inner_image' => null,
                'view4_mask_inner_image' => null,
                'view4_highlighted_inner_image' => null,
                'view5_glow_inner_image' => null,
                'view5_mask_inner_image' => null,
                'view5_highlighted_inner_image' => null,
                'status' => 1,
                'created_time' => '2017-01-26 22:42:53',
                'update_time' => '2018-05-21 12:07:18'
            ],
                [
                'title' => 'Long collar',
                'class' => 'icon-Collar_Long',
                'price' => 5.00,
                'type' => 0,
                'thumb' => 'glow_mask/style/Style_Mandarin.png',
                'view1_glow_left_image' => 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Longcollar_Front_Left_shad.png',
                'view1_mask_left_image' => 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Longcollar_Front_Left_mask.png',
                'view1_highlighted_left_image' => 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Longcollar_Front_Left_hi.png',
                'view1_glow_right_image' => 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Longcollar_Front_Right_shad.png',
                'view1_mask_right_image' => 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Longcollar_Front_Right_mask.png',
                'view1_highlighted_right_image' => 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Longcollar_Front_Right_hi.png',
                'view2_glow_left_image' => 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Longcollar_Side_Left_shad.png',
                'view2_mask_left_image' => 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Longcollar_Side_Left_mask.png',
                'view2_highlighted_left_image' => 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Longcollar_Side_Left_hi.png',
                'view2_glow_right_image' => 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Longcollar_Side_Right_shad.png',
                'view2_mask_right_image' => 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Longcollar_Side_Right_mask.png',
                'view2_highlighted_right_image' => 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Longcollar_Side_Right_hi.png',
                'view3_glow_back_image' => null,
                'view3_mask_back_image' => null,
                'view3_highlighted_back_image' => null,
                'view4_glow_left_image' => 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Longcollar_Folded_Left_shad.png',
                'view4_mask_left_image' => 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Longcollar_Folded_Left_mask.png',
                'view4_highlighted_left_image' => 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Longcollar_Folded_Left_hi.png',
                'view4_glow_right_image' => 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Longcollar_Folded_Right_shad.png',
                'view4_mask_right_image' => 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Longcollar_Folded_Right_mask.png',
                'view4_highlighted_right_image' => 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Longcollar_Folded_Right_hi.png',
                'view5_glow_left_image' => 'glow_mask_shirt/Zoom_View/Collar_Styles/LongCollar_Left_shad.png',
                'view5_mask_left_image' => 'glow_mask_shirt/Zoom_View/Collar_Styles/LongCollar_Left_mask.png',
                'view5_highlighted_left_image' => 'glow_mask_shirt/Zoom_View/Collar_Styles/LongCollar_Left_hi.png',
                'view5_glow_right_image' => 'glow_mask_shirt/Zoom_View/Collar_Styles/LongCollar_Right_shad.png',
                'view5_mask_right_image' => 'glow_mask_shirt/Zoom_View/Collar_Styles/LongCollar_Right_mask.png',
                'view5_highlighted_right_image' => 'glow_mask_shirt/Zoom_View/Collar_Styles/LongCollar_Right_hi.png',
                'view1_casual_glow_left_image' => 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Longcollar_Left_shad.png',
                'view1_casual_mask_left_image' => 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Longcollar_Left_mask.png',
                'view1_casual_highlighted_left_image' => 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Longcollar_Left_hi.png',
                'view1_casual_glow_right_image' => 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Longcollar_Right_shad.png',
                'view1_casual_mask_right_image' => 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Longcollar_Right_mask.png',
                'view1_casual_highlighted_right_image' => 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Longcollar_Right_hi.png',
                'view2_casual_glow_left_image' => 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Long_Left_shad.png',
                'view2_casual_mask_left_image' => 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Long_Left_mask.png',
                'view2_casual_highlighted_left_image' => 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Long_Left_hi.png',
                'view2_casual_glow_right_image' => 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Long_Right_shad.png',
                'view2_casual_mask_right_image' => 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Long_Right_mask.png',
                'view2_casual_highlighted_right_image' => 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Long_Right_hi.png',
                'view4_glow_inner_image' => null,
                'view4_mask_inner_image' => null,
                'view4_highlighted_inner_image' => null,
                'view5_glow_inner_image' => null,
                'view5_mask_inner_image' => null,
                'view5_highlighted_inner_image' => null,
                'status' => 1,
                'created_time' => '2017-01-26 22:42:53',
                'update_time' => '2018-05-21 12:07:28'
            ],
                [
                'title' => 'Pinned collar',
                'class' => 'icon-Collar_Pinned_Collar',
                'price' => 15.00,
                'type' => 0,
                'thumb' => 'glow_mask/style/Style_Mandarin.png',
                'view1_glow_left_image' => 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Pinnedcollar_Front_Left_shad.png',
                'view1_mask_left_image' => 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Pinnedcollar_Front_Left_mask.png',
                'view1_highlighted_left_image' => 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Pinnedcollar_Front_Left_hi.png',
                'view1_glow_right_image' => 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Pinnedcollar_Front_Right_shad.png',
                'view1_mask_right_image' => 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Pinnedcollar_Front_Right_mask.png',
                'view1_highlighted_right_image' => 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Pinnedcollar_Front_Right_hi.png',
                'view2_glow_left_image' => 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Pinnedcollar_Side_Left_shad.png',
                'view2_mask_left_image' => 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Pinnedcollar_Side_Left_mask.png',
                'view2_highlighted_left_image' => 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Pinnedcollar_Side_Left_hi.png',
                'view2_glow_right_image' => 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Pinnedcollar_Side_Right_shad.png',
                'view2_mask_right_image' => 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Pinnedcollar_Side_Right_mask.png',
                'view2_highlighted_right_image' => 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Pinnedcollar_Side_Right_hi.png',
                'view3_glow_back_image' => null,
                'view3_mask_back_image' => null,
                'view3_highlighted_back_image' => null,
                'view4_glow_left_image' => 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Pinnedcollar_Folded_Left_shad.png',
                'view4_mask_left_image' => 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Pinnedcollar_Folded_Left_mask.png',
                'view4_highlighted_left_image' => 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Pinnedcollar_Folded_Left_hi.png',
                'view4_glow_right_image' => 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Pinnedcollar_Folded_Right_shad.png',
                'view4_mask_right_image' => 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Pinnedcollar_Folded_Right_mask.png',
                'view4_highlighted_right_image' => 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Pinnedcollar_Folded_Right_hi.png',
                'view5_glow_left_image' => 'glow_mask_shirt/Zoom_View/Collar_Styles/Pinned_Left_shad.png',
                'view5_mask_left_image' => 'glow_mask_shirt/Zoom_View/Collar_Styles/Pinned_Left_mask.png',
                'view5_highlighted_left_image' => 'glow_mask_shirt/Zoom_View/Collar_Styles/Pinned_Left_hi.png',
                'view5_glow_right_image' => 'glow_mask_shirt/Zoom_View/Collar_Styles/Pinned_Right_shad.png',
                'view5_mask_right_image' => 'glow_mask_shirt/Zoom_View/Collar_Styles/Pinned_Right_mask.png',
                'view5_highlighted_right_image' => 'glow_mask_shirt/Zoom_View/Collar_Styles/Pinned_Right_hi.png',
                'view1_casual_glow_left_image' => 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Pinned_Left_shad.png',
                'view1_casual_mask_left_image' => 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Pinned_Left_mask.png',
                'view1_casual_highlighted_left_image' => 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Pinned_Left_hi.png',
                'view1_casual_glow_right_image' => 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Pinned_Right_shad.png',
                'view1_casual_mask_right_image' => 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Pinned_Right_mask.png',
                'view1_casual_highlighted_right_image' => 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Pinned_Right_hi.png',
                'view2_casual_glow_left_image' => 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Pinned_Left_shad.png',
                'view2_casual_mask_left_image' => 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Pinned_Left_mask.png',
                'view2_casual_highlighted_left_image' => 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Pinned_Left_hi.png',
                'view2_casual_glow_right_image' => 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Pinned_Right_shad.png',
                'view2_casual_mask_right_image' => 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Pinned_Right_mask.png',
                'view2_casual_highlighted_right_image' => 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Pinned_Right_hi.png',
                'view4_glow_inner_image' => null,
                'view4_mask_inner_image' => null,
                'view4_highlighted_inner_image' => null,
                'view5_glow_inner_image' => null,
                'view5_mask_inner_image' => null,
                'view5_highlighted_inner_image' => null,
                'status' => 1,
                'created_time' => '2017-01-26 22:42:53',
                'update_time' => '2018-05-21 12:07:35'
            ],
                [
                'title' => 'Short Classic',
                'class' => 'icon-Collar_Short_Classic',
                'price' => 30.00,
                'type' => 0,
                'thumb' => 'glow_mask/style/Style_Mandarin.png',
                'view1_glow_left_image' => 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_ShortClassic_Front_Left_shad.png',
                'view1_mask_left_image' => 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_ShortClassic_Front_Left_mask.png',
                'view1_highlighted_left_image' => 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_ShortClassic_Front_Left_hi.png',
                'view1_glow_right_image' => 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_ShortClassic_Front_Right_shad.png',
                'view1_mask_right_image' => 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_ShortClassic_Front_Right_mask.png',
                'view1_highlighted_right_image' => 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_ShortClassic_Front_Right_hi.png',
                'view2_glow_left_image' => 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Shortclassiccollar_Side_Left_shad.png',
                'view2_mask_left_image' => 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Shortclassiccollar_Side_Left_mask.png',
                'view2_highlighted_left_image' => 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Shortclassiccollar_Side_Left_hi.png',
                'view2_glow_right_image' => 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Shortclassiccollar_Side_Right_shad.png',
                'view2_mask_right_image' => 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Shortclassiccollar_Side_Right_mask.png',
                'view2_highlighted_right_image' => 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Shortclassiccollar_Side_Right_hi.png',
                'view3_glow_back_image' => null,
                'view3_mask_back_image' => null,
                'view3_highlighted_back_image' => null,
                'view4_glow_left_image' => 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Shortclassiccollar_Folded_Left_shad.png',
                'view4_mask_left_image' => 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Shortclassiccollar_Folded_Left_mask.png',
                'view4_highlighted_left_image' => 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Shortclassiccollar_Folded_Left_hi.png',
                'view4_glow_right_image' => 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Shortclassiccollar_Folded_Right_shad.png',
                'view4_mask_right_image' => 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Shortclassiccollar_Folded_Right_mask.png',
                'view4_highlighted_right_image' => 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Shortclassiccollar_Folded_Right_hi.png',
                'view5_glow_left_image' => 'glow_mask_shirt/Zoom_View/Collar_Styles/ClassicShortCollar_Left_shad.png',
                'view5_mask_left_image' => 'glow_mask_shirt/Zoom_View/Collar_Styles/ClassicShortCollar_Left_mask.png',
                'view5_highlighted_left_image' => 'glow_mask_shirt/Zoom_View/Collar_Styles/ClassicShortCollar_Left_hi.png',
                'view5_glow_right_image' => 'glow_mask_shirt/Zoom_View/Collar_Styles/ClassicShortCollar_Right_shad.png',
                'view5_mask_right_image' => 'glow_mask_shirt/Zoom_View/Collar_Styles/ClassicShortCollar_Right_mask.png',
                'view5_highlighted_right_image' => 'glow_mask_shirt/Zoom_View/Collar_Styles/ClassicShortCollar_Right_hi.png',
                'view1_casual_glow_left_image' => 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_ShortClassic_Left_shad.png',
                'view1_casual_mask_left_image' => 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_ShortClassic_Left_mask.png',
                'view1_casual_highlighted_left_image' => 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_ShortClassic_Left_hi.png',
                'view1_casual_glow_right_image' => 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_ShortClassic_Right_shad.png',
                'view1_casual_mask_right_image' => 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_ShortClassic_Right_mask.png',
                'view1_casual_highlighted_right_image' => 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_ShortClassic_Right_hi.png',
                'view2_casual_glow_left_image' => 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Short_Left_shad.png',
                'view2_casual_mask_left_image' => 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Short_Left_mask.png',
                'view2_casual_highlighted_left_image' => 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Short_Left_hi.png',
                'view2_casual_glow_right_image' => 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Short_Right_shad.png',
                'view2_casual_mask_right_image' => 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Short_Right_mask.png',
                'view2_casual_highlighted_right_image' => 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Short_Right_hi.png',
                'view4_glow_inner_image' => null,
                'view4_mask_inner_image' => null,
                'view4_highlighted_inner_image' => null,
                'view5_glow_inner_image' => null,
                'view5_mask_inner_image' => null,
                'view5_highlighted_inner_image' => null,
                'status' => 1,
                'created_time' => '2017-01-26 22:42:53',
                'update_time' => '2018-05-21 12:07:51'
            ],
                [
                'title' => 'Stand-up collar',
                'class' => 'icon-Collar_Standup',
                'price' => 10.00,
                'type' => 0,
                'thumb' => 'glow_mask/style/Style_Mandarin.png',
                'view1_glow_left_image' => 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Standupcollar_Front_Left_shad.png',
                'view1_mask_left_image' => 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Standupcollar_Front_Left_mask.png',
                'view1_highlighted_left_image' => 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Standupcollar_Front_Left_hi.png',
                'view1_glow_right_image' => 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Standupcollar_Front_Right_shad.png',
                'view1_mask_right_image' => 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Standupcollar_Front_Right_mask.png',
                'view1_highlighted_right_image' => 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Standupcollar_Front_Right_hi.png',
                'view2_glow_left_image' => 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Standupcollar_Side_Left_shad.png',
                'view2_mask_left_image' => 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Standupcollar_Side_Left_mask.png',
                'view2_highlighted_left_image' => 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Standupcollar_Side_Left_hi.png',
                'view2_glow_right_image' => 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Standupcollar_Side_Right_shad.png',
                'view2_mask_right_image' => 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Standupcollar_Side_Right_mask.png',
                'view2_highlighted_right_image' => 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Standupcollar_Side_Right_hi.png',
                'view3_glow_back_image' => null,
                'view3_mask_back_image' => null,
                'view3_highlighted_back_image' => null,
                'view4_glow_left_image' => 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Standupcollar_Folded_Left_shad.png',
                'view4_mask_left_image' => 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Standupcollar_Folded_Left_mask.png',
                'view4_highlighted_left_image' => 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Standupcollar_Folded_Left_hi.png',
                'view4_glow_right_image' => 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Standupcollar_Folded_Right_shad.png',
                'view4_mask_right_image' => 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Standupcollar_Folded_Right_mask.png',
                'view4_highlighted_right_image' => 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Standupcollar_Folded_Right_hi.png',
                'view5_glow_left_image' => 'glow_mask_shirt/Zoom_View/Collar_Styles/CollarBase_Left_Shad.png',
                'view5_mask_left_image' => 'glow_mask_shirt/Zoom_View/Collar_Styles/CollarBase_Left_Mask.png',
                'view5_highlighted_left_image' => 'glow_mask_shirt/Zoom_View/Collar_Styles/CollarBase_Left_Hi.png',
                'view5_glow_right_image' => 'glow_mask_shirt/Zoom_View/Collar_Styles/CollarBase_Right_Shad.png',
                'view5_mask_right_image' => 'glow_mask_shirt/Zoom_View/Collar_Styles/CollarBase_Right_Mask.png',
                'view5_highlighted_right_image' => 'glow_mask_shirt/Zoom_View/Collar_Styles/CollarBase_Right_Hi.png',
                'view1_casual_glow_left_image' => 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Standup_Left_shad.png',
                'view1_casual_mask_left_image' => 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Standup_Left_mask.png',
                'view1_casual_highlighted_left_image' => 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Standup_Left_hi.png',
                'view1_casual_glow_right_image' => 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Standup_Right_shad.png',
                'view1_casual_mask_right_image' => 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Standup_Right_mask.png',
                'view1_casual_highlighted_right_image' => 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Standup_Right_hi.png',
                'view2_casual_glow_left_image' => 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Standup_Left_shad.png',
                'view2_casual_mask_left_image' => 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Standup_Left_mask.png',
                'view2_casual_highlighted_left_image' => 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Standup_Left_hi.png',
                'view2_casual_glow_right_image' => 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Standup_Right_shad.png',
                'view2_casual_mask_right_image' => 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Standup_Right_mask.png',
                'view2_casual_highlighted_right_image' => 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Standup_Right_hi.png',
                'view4_glow_inner_image' => null,
                'view4_mask_inner_image' => null,
                'view4_highlighted_inner_image' => null,
                'view5_glow_inner_image' => null,
                'view5_mask_inner_image' => null,
                'view5_highlighted_inner_image' => null,
                'status' => 1,
                'created_time' => '2017-01-26 22:42:53',
                'update_time' => '2018-05-21 12:07:59'
            ],
                [
                'title' => 'Wide Cutaway',
                'class' => 'icon-collar_Wide_Cutaway',
                'price' => 20.00,
                'type' => 0,
                'thumb' => 'glow_mask/style/Style_Mandarin.png',
                'view1_glow_left_image' => 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Widecutawaycollar_Front_Left_shad.png',
                'view1_mask_left_image' => 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Widecutawaycollar_Front_Left_mask.png',
                'view1_highlighted_left_image' => 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Widecutawaycollar_Front_Left_hi.png',
                'view1_glow_right_image' => 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Widecutawaycollar_Front_Right_shad.png',
                'view1_mask_right_image' => 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Widecutawaycollar_Front_Right_mask.png',
                'view1_highlighted_right_image' => 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Widecutawaycollar_Front_Right_hi.png',
                'view2_glow_left_image' => 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Widecutaway_Side_Left_shad.png',
                'view2_mask_left_image' => 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Widecutaway_Side_Left_mask.png',
                'view2_highlighted_left_image' => 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Widecutaway_Side_Left_hi.png',
                'view2_glow_right_image' => 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Widecutaway_Side_Right_shad.png',
                'view2_mask_right_image' => 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Widecutaway_Side_Right_mask.png',
                'view2_highlighted_right_image' => 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Widecutaway_Side_Right_hi.png',
                'view3_glow_back_image' => null,
                'view3_mask_back_image' => null,
                'view3_highlighted_back_image' => null,
                'view4_glow_left_image' => 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Widecutawaycollar_Folded_Left_shad.png',
                'view4_mask_left_image' => 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Widecutawaycollar_Folded_Left_mask.png',
                'view4_highlighted_left_image' => 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Widecutawaycollar_Folded_Left_hi.png',
                'view4_glow_right_image' => 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Widecutawaycollar_Folded_Right_shad.png',
                'view4_mask_right_image' => 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Widecutawaycollar_Folded_Right_mask.png',
                'view4_highlighted_right_image' => 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Widecutawaycollar_Folded_Right_hi.png',
                'view5_glow_left_image' => 'glow_mask_shirt/Zoom_View/Collar_Styles/WideCutawayCollar_Left_shad.png',
                'view5_mask_left_image' => 'glow_mask_shirt/Zoom_View/Collar_Styles/WideCutawayCollar_Left_mask.png',
                'view5_highlighted_left_image' => 'glow_mask_shirt/Zoom_View/Collar_Styles/WideCutawayCollar_Left_hi.png',
                'view5_glow_right_image' => 'glow_mask_shirt/Zoom_View/Collar_Styles/WideCutawayCollar_Right_shad.png',
                'view5_mask_right_image' => 'glow_mask_shirt/Zoom_View/Collar_Styles/WideCutawayCollar_Right_mask.png',
                'view5_highlighted_right_image' => 'glow_mask_shirt/Zoom_View/Collar_Styles/WideCutawayCollar_Right_hi.png',
                'view1_casual_glow_left_image' => 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Widecutaway_Left_shad.png',
                'view1_casual_mask_left_image' => 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Widecutaway_Left_mask.png',
                'view1_casual_highlighted_left_image' => 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Widecutaway_Left_hi.png',
                'view1_casual_glow_right_image' => 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Widecutaway_Right_shad.png',
                'view1_casual_mask_right_image' => 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Widecutaway_Right_mask.png',
                'view1_casual_highlighted_right_image' => 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Widecutaway_Right_hi.png',
                'view2_casual_glow_left_image' => 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Widecutaway_Left_shad.png',
                'view2_casual_mask_left_image' => 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Widecutaway_Left_mask.png',
                'view2_casual_highlighted_left_image' => 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Widecutaway_Left_hi.png',
                'view2_casual_glow_right_image' => 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Widecutaway_Right_shad.png',
                'view2_casual_mask_right_image' => 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Widecutaway_Right_mask.png',
                'view2_casual_highlighted_right_image' => 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Widecutaway_Right_hi.png',
                'view4_glow_inner_image' => null,
                'view4_mask_inner_image' => null,
                'view4_highlighted_inner_image' => null,
                'view5_glow_inner_image' => null,
                'view5_mask_inner_image' => null,
                'view5_highlighted_inner_image' => null,
                'status' => 1,
                'created_time' => '2017-01-26 22:42:53',
                'update_time' => '2018-05-21 12:08:11'
            ],
                [
                'title' => 'Wing collar',
                'class' => 'icon-CollarWing',
                'price' => 15.00,
                'type' => 0,
                'thumb' => 'glow_mask/style/Style_Mandarin.png',
                'view1_glow_left_image' => 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Wingcollar_Front_Left_shad.png',
                'view1_mask_left_image' => 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Wingcollar_Front_Left_mask.png',
                'view1_highlighted_left_image' => 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Wingcollar_Front_Left_hi.png',
                'view1_glow_right_image' => 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Wingcollar_Front_Right_shad.png',
                'view1_mask_right_image' => 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Wingcollar_Front_Right_mask.png',
                'view1_highlighted_right_image' => 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Wingcollar_Front_Right_hi.png',
                'view2_glow_left_image' => 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Wingedcollar_Side_Left_shad.png',
                'view2_mask_left_image' => 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Wingedcollar_Side_Left_mask.png',
                'view2_highlighted_left_image' => 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Wingedcollar_Side_Left_hi.png',
                'view2_glow_right_image' => 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Wingedcollar_Side_Right_shad.png',
                'view2_mask_right_image' => 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Wingedcollar_Side_Right_mask.png',
                'view2_highlighted_right_image' => 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Wingedcollar_Side_Right_hi.png',
                'view3_glow_back_image' => null,
                'view3_mask_back_image' => null,
                'view3_highlighted_back_image' => null,
                'view4_glow_left_image' => 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Wingcollar_Folded_Left_shad.png',
                'view4_mask_left_image' => 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Wingcollar_Folded_Left_mask.png',
                'view4_highlighted_left_image' => 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Wingcollar_Folded_Left_hi.png',
                'view4_glow_right_image' => 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Wingcollar_Folded_Right_shad.png',
                'view4_mask_right_image' => 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Wingcollar_Folded_Right_mask.png',
                'view4_highlighted_right_image' => 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Wingcollar_Folded_Right_hi.png',
                'view5_glow_left_image' => 'glow_mask_shirt/Zoom_View/Collar_Styles/WingCollar_Left_shad.png',
                'view5_mask_left_image' => 'glow_mask_shirt/Zoom_View/Collar_Styles/WingCollar_Left_mask.png',
                'view5_highlighted_left_image' => 'glow_mask_shirt/Zoom_View/Collar_Styles/WingCollar_Left_hi.png',
                'view5_glow_right_image' => 'glow_mask_shirt/Zoom_View/Collar_Styles/WingCollar_Right_shad.png',
                'view5_mask_right_image' => 'glow_mask_shirt/Zoom_View/Collar_Styles/WingCollar_Right_mask.png',
                'view5_highlighted_right_image' => 'glow_mask_shirt/Zoom_View/Collar_Styles/WingCollar_Right_hi.png',
                'view1_casual_glow_left_image' => 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Winged_Left_shad.png',
                'view1_casual_mask_left_image' => 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Winged_Left_mask.png',
                'view1_casual_highlighted_left_image' => 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Winged_Left_hi.png',
                'view1_casual_glow_right_image' => 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Winged_Right_shad.png',
                'view1_casual_mask_right_image' => 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Winged_Right_mask.png',
                'view1_casual_highlighted_right_image' => 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Winged_Right_hi.png',
                'view2_casual_glow_left_image' => 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Wing_Left_shad.png',
                'view2_casual_mask_left_image' => 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Wing_Left_mask.png',
                'view2_casual_highlighted_left_image' => 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Wing_Left_hi.png',
                'view2_casual_glow_right_image' => 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Wing_Right_shad.png',
                'view2_casual_mask_right_image' => 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Wing_Right_mask.png',
                'view2_casual_highlighted_right_image' => 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Wing_Right_hi.png',
                'view4_glow_inner_image' => null,
                'view4_mask_inner_image' => null,
                'view4_highlighted_inner_image' => null,
                'view5_glow_inner_image' => null,
                'view5_mask_inner_image' => null,
                'view5_highlighted_inner_image' => null,
                'status' => 1,
                'created_time' => '2017-01-26 22:42:53',
                'update_time' => '2018-05-21 12:08:18',
            ]
        ];

        foreach ($shirtStyleData as $data) {
            $this->_shirtStyle->create()->addData($data)->save();
        }




        //shirt_shirtthreadfabric
        $shirtShirtthreadfabricData = [
                [
                'display_fabric_thumb' => 'fab_shirt_images/display_thumb/green.png',
                'fabric_thumb' => 'fab_shirt_images/thumbs/green.png',
                'fabric_large_image' => 'fab_shirt_images/green.png',
                'title' => 'Green',
                'price' => 12,
                'type' => 0,
                'status' => 1,
                'created_time' => '2018-06-04 13:31:54',
                'update_time' => '2018-06-04 13:31:54'
            ],
                [
                'display_fabric_thumb' => 'fab_shirt_images/display_thumb/blue.png',
                'fabric_thumb' => 'fab_shirt_images/thumbs/blue.png',
                'fabric_large_image' => 'fab_shirt_images/blue.png',
                'title' => 'Blue',
                'price' => 12,
                'type' => 0,
                'status' => 1,
                'created_time' => '2018-06-04 13:36:14',
                'update_time' => '2018-06-04 13:36:14'
            ],
                [
                'display_fabric_thumb' => 'fab_shirt_images/display_thumb/red.png',
                'fabric_thumb' => 'fab_shirt_images/thumbs/red.png',
                'fabric_large_image' => 'fab_shirt_images/red.png',
                'title' => 'Red',
                'price' => 12,
                'type' => 0,
                'status' => 1,
                'created_time' => '2018-06-04 13:38:22',
                'update_time' => '2018-06-04 13:38:22'
            ],
                [
                'display_fabric_thumb' => 'fab_shirt_images/display_thumb/white.png',
                'fabric_thumb' => 'fab_shirt_images/thumbs/white.png',
                'fabric_large_image' => 'fab_shirt_images/white.png',
                'title' => 'White',
                'price' => 34,
                'type' => 0,
                'status' => 1,
                'created_time' => '2018-06-04 13:38:50',
                'update_time' => '2018-06-04 13:38:50'
            ],
                [
                'display_fabric_thumb' => 'fab_shirt_images/display_thumb/yellow.png',
                'fabric_thumb' => 'fab_shirt_images/thumbs/yellow.png',
                'fabric_large_image' => 'fab_shirt_images/yellow.png',
                'title' => 'Yellow',
                'price' => 23,
                'type' => 0,
                'status' => 1,
                'created_time' => '2018-06-04 13:39:23',
                'update_time' => '2018-06-04 13:39:23'
            ]
        ];

        foreach ($shirtShirtthreadfabricData as $data) {
            $this->_shirtShirtthreadfabric->create()->addData($data)->save();
        }



        //shirt_suitfabric
        $shirtSuitfabricData = [
                [
                'display_fabric_thumb' => 'fab_suit_images/display_thumb/fabric_01_100.png',
                'fabric_thumb' => 'fab_suit_images/thumbs/fabric_01_100.png',
                'fabric_large_image' => 'fab_suit_images/fabric_01_1000compressed.jpg',
                'title' => 'Navy Blue',
                'price' => 21,
                'type' => 0,
                'btn_color_id' => 0,
                'tie_color' => 0,
                'shirt_type' => 0,
                'shoes_color' => 0,
                'is_fabric' => null,
                'suit_material_id' => 2,
                'suit_pattern_id' => 8,
                'suit_season_id' => 3,
                'suit_color_id' => 3,
                'suit_category_id' => 2,
                'status' => 1,
                'created_time' => '2018-07-13 13:03:32',
                'update_time' => '2018-07-13 13:03:32'
            ],
                [
                'display_fabric_thumb' => 'fab_suit_images/display_thumb/13.jpg',
                'fabric_thumb' => 'fab_suit_images/thumbs/13.jpg',
                'fabric_large_image' => 'fab_suit_images/13.jpg',
                'title' => 'Herringbone',
                'price' => 30,
                'type' => 0,
                'btn_color_id' => 0,
                'tie_color' => 0,
                'shirt_type' => 0,
                'shoes_color' => 0,
                'is_fabric' => null,
                'suit_material_id' => 3,
                'suit_pattern_id' => 5,
                'suit_season_id' => 2,
                'suit_color_id' => 4,
                'suit_category_id' => 7,
                'status' => 2,
                'created_time' => '2018-07-05 08:52:17',
                'update_time' => '2018-07-05 08:52:17'
            ],
                [
                'display_fabric_thumb' => 'fab_suit_images/display_thumb/13_100_.png',
                'fabric_thumb' => 'fab_suit_images/thumbs/13.png',
                'fabric_large_image' => 'fab_suit_images/13_100_.png',
                'title' => 'Red',
                'price' => 55,
                'type' => 0,
                'btn_color_id' => 0,
                'tie_color' => 0,
                'shirt_type' => 0,
                'shoes_color' => 0,
                'is_fabric' => null,
                'suit_material_id' => 3,
                'suit_pattern_id' => 5,
                'suit_season_id' => 3,
                'suit_color_id' => 6,
                'suit_category_id' => 7,
                'status' => 1,
                'created_time' => '2018-07-13 13:17:11',
                'update_time' => '2018-07-13 13:17:11'
            ],
                [
                'display_fabric_thumb' => 'fab_suit_images/display_thumb/Farbric_02_360_1.png',
                'fabric_thumb' => 'fab_suit_images/thumbs/01.png',
                'fabric_large_image' => 'fab_suit_images/Farbric_02_1000_3.png',
                'title' => 'Black',
                'price' => 23,
                'type' => 0,
                'btn_color_id' => 0,
                'tie_color' => 0,
                'shirt_type' => 0,
                'shoes_color' => 0,
                'is_fabric' => null,
                'suit_material_id' => 3,
                'suit_pattern_id' => 1,
                'suit_season_id' => 2,
                'suit_color_id' => 2,
                'suit_category_id' => 7,
                'status' => 1,
                'created_time' => '2018-07-13 13:22:37',
                'update_time' => '2018-07-13 13:22:37'
            ],
                [
                'display_fabric_thumb' => 'fab_suit_images/display_thumb/360.png',
                'fabric_thumb' => 'f',
                'fabric_large_image' => 'fab_suit_images/1000.png',
                'title' => 'fabric23',
                'price' => 25,
                'type' => 0,
                'btn_color_id' => 0,
                'tie_color' => 0,
                'shirt_type' => 0,
                'shoes_color' => 0,
                'is_fabric' => null,
                'suit_material_id' => 6,
                'suit_pattern_id' => 1,
                'suit_season_id' => 1,
                'suit_color_id' => 1,
                'suit_category_id' => 2,
                'status' => 1,
                'created_time' => '2018-07-23 05:41:43',
                'update_time' => '2018-07-23 05:41:43'
            ],
                [
                'display_fabric_thumb' => 'fab_suit_images/display_thumb/fabric_02_1000.png',
                'fabric_thumb' => 'fab_suit_images/thumbs/fabric_02_100.png',
                'fabric_large_image' => 'fab_suit_images/fabric_02_crumpled.png',
                'title' => 'Light Blue',
                'price' => 10,
                'type' => 0,
                'btn_color_id' => 0,
                'tie_color' => 0,
                'shirt_type' => 0,
                'shoes_color' => 0,
                'is_fabric' => null,
                'suit_material_id' => 3,
                'suit_pattern_id' => 5,
                'suit_season_id' => 2,
                'suit_color_id' => 3,
                'suit_category_id' => 2,
                'status' => 1,
                'created_time' => '2018-07-25 09:23:59',
                'update_time' => '2018-07-25 09:23:59',
            ]
        ];

        foreach ($shirtSuitfabricData as $data) {
            $this->_shirtSuitfabric->create()->addData($data)->save();
        }

        //shirt_accent_collarstyle
        $shirtAccentCollarstyleData = [
                [
                'title' => 'By Default',
                'price' => 0.00,
                'class' => 'icon-By_Default',
                'status' => 1,
                'created_date' => '2017-12-30 12:33:27',
                'modified_date' => '2018-06-27 15:52:51'
            ],
                [
                'title' => 'All',
                'price' => 14.00,
                'class' => 'icon-All',
                'status' => 1,
                'created_date' => '2017-12-30 12:34:46',
                'modified_date' => '2018-06-27 15:53:01'
            ],
                [
                'title' => 'Outer Fabric',
                'price' => 10.00,
                'class' => 'icon-Oruter',
                'status' => 1,
                'created_date' => '2018-01-22 06:17:27',
                'modified_date' => '2018-05-21 12:16:19'
            ],
                [
                'title' => 'Inner Fabric',
                'price' => 10.00,
                'class' => 'icon-Inner',
                'status' => 1,
                'created_date' => '2018-01-22 06:17:42',
                'modified_date' => '2018-05-21 12:16:11'
            ]
        ];

        foreach ($shirtAccentCollarstyleData as $data) {
            $this->_shirtAccentCollarstyle->create()->addData($data)->save();
        }


        //shirt_accent_chestpleats
        $shirt_accent_chestpleatsData = [
                [
                'title' => 'No',
                'price' => 0.00,
                'class' => 'icon-No',
                'view1_image' => null,
                'view2_image' => null,
                'view4_image' => null,
                'status' => 1,
                'created_date' => '2018-01-22 07:09:16',
                'modified_date' => '2018-05-21 12:22:09'
            ],
                [
                'title' => 'Yes',
                'price' => 10.00,
                'class' => 'icon-Yes',
                'view1_image' => 'shirt_images/chestpleats/Shirt_Tuxedo_Front_Glow_1.png',
                'view2_image' => 'shirt_images/chestpleats/Shirt_Tuxedo_Side_Glow_2.png',
                'view4_image' => 'shirt_images/chestpleats/Shirt_Tuxedo_Folded_Glow_4.png',
                'status' => 1,
                'created_date' => '2018-01-22 07:10:28',
                'modified_date' => '2018-05-21 12:21:38'
            ]
        ];

        foreach ($shirt_accent_chestpleatsData as $data) {
            $this->_shirt_accent_chestpleats->create()->addData($data)->save();
        }


        //shirt_accent_cuffs
        $shirtAccentCuffsData = [
                [
                'title' => 'By Default',
                'price' => 0.00,
                'class' => 'icon-By_Default2',
                'status' => 1,
                'created_date' => '2018-01-20 11:33:13',
                'modified_date' => '2018-06-27 15:55:42'
            ],
                [
                'title' => 'All',
                'price' => 10.00,
                'class' => 'icon-All2',
                'status' => 1,
                'created_date' => '2018-01-22 07:06:58',
                'modified_date' => '2018-06-27 15:55:45'
            ],
                [
                'title' => 'Outer Fabric',
                'price' => 10.00,
                'class' => 'icon-Outer',
                'status' => 1,
                'created_date' => '2018-01-22 07:07:11',
                'modified_date' => '2018-05-21 12:11:55'
            ],
                [
                'title' => 'Inner Fabric',
                'price' => 10.00,
                'class' => 'icon-Inner2',
                'status' => 1,
                'created_date' => '2018-01-22 07:07:27',
                'modified_date' => '2018-05-21 12:12:04'
            ]
        ];

        foreach ($shirtAccentCuffsData as $data) {
            $this->_shirtAccentCuffs->create()->addData($data)->save();
        }

        //shirt_accent_elbowpatches
        $shirtAccentElbowpatchesData = [
                [
                'title' => 'Without elbow patch',
                'price' => 10.00,
                'class' => 'icon-Without_Elbow_Patch',
                'gender' => 1,
                'glow_back_image' => null,
                'mask_back_image' => null,
                'highlighted_back_image' => null,
                'status' => 1
            ],
                [
                'title' => 'With elbow patch',
                'price' => 10.00,
                'class' => 'icon-With_Elbow_Patch',
                'gender' => 1,
                'glow_back_image' => 'glow_mask_shirt/Back_View/elbowpatches/back_elbowpatch_shad.png',
                'mask_back_image' => 'glow_mask_shirt/Back_View/elbowpatches/back_elbowpatch_mask.png',
                'highlighted_back_image' => 'glow_mask_shirt/Back_View/elbowpatches/back_elbowpatch_hi.png',
                'status' => 1
            ]
        ];

        foreach ($shirtAccentElbowpatchesData as $data) {
            $this->_shirtAccentElbowpatches->create()->addData($data)->save();
        }

        //shirt_accent_threadcolor
        $shirtAccentThreadcolorData = [
                [
                'title' => 'Green',
                'class' => 'icon-icon-43',
                'price' => null,
                'image' => null,
                'status' => 1
            ]
        ];

        foreach ($shirtAccentThreadcolorData as $data) {
            $this->_shirtAccentThreadcolor->create()->addData($data)->save();
        }


        //shirt_accent_placket
        $shirtAccentPlacketData = [
                [
                'title' => 'By Default',
                'price' => 0.00,
                'class' => 'icon-Default',
                'status' => 1
            ],
                [
                'title' => 'All',
                'price' => 10.00,
                'class' => 'icon-All3',
                'status' => 1
            ],
                [
                'title' => 'Outer Fabric',
                'price' => 10.00,
                'class' => 'icon-outer',
                'status' => 1
            ],
                [
                'title' => 'Inner Fabric',
                'price' => 10.00,
                'class' => 'icon-inner',
                'status' => 1,
            ]
        ];

        foreach ($shirtAccentPlacketData as $data) {
            $this->_shirtAccentPlacket->create()->addData($data)->save();
        }

        //shirt_accent_threads
        $shirtAccentThreadsData = [
                [
                'title' => 'By Default',
                'price' => 0.00,
                'class' => 'icon-By_Default3',
                'mask_front_image' => null,
                'mask_side_image' => null,
                'mask_back_image' => null,
                'mask_fold_image' => null,
                'mask_zoom_view' => null,
                'mask_front_buttonhole_image' => null,
                'mask_side_buttonhole_image' => null,
                'mask_back_buttonhole_image' => null,
                'mask_fold_buttonhole_image' => null,
                'mask_zoomview_buttonhole_image' => null,
                'status' => 1,
                'created_date' => '2018-01-20 12:33:40',
                'modified_date' => '2018-07-02 18:46:24'
            ],
                [
                'title' => 'All',
                'price' => 5.00,
                'class' => 'icon-All4',
                'mask_front_image' => null,
                'mask_side_image' => null,
                'mask_back_image' => null,
                'mask_fold_image' => null,
                'mask_zoom_view' => null,
                'mask_front_buttonhole_image' => null,
                'mask_side_buttonhole_image' => null,
                'mask_back_buttonhole_image' => null,
                'mask_fold_buttonhole_image' => null,
                'mask_zoomview_buttonhole_image' => null,
                'status' => 1,
                'created_date' => '2018-01-24 04:39:26',
                'modified_date' => '2018-07-02 18:45:17'
            ],
                [
                'title' => 'Only Cuff',
                'price' => 5.00,
                'class' => 'icon-Cuff_only',
                'mask_front_image' => null,
                'mask_side_image' => null,
                'mask_back_image' => null,
                'mask_fold_image' => null,
                'mask_zoom_view' => null,
                'mask_front_buttonhole_image' => null,
                'mask_side_buttonhole_image' => null,
                'mask_back_buttonhole_image' => null,
                'mask_fold_buttonhole_image' => null,
                'mask_zoomview_buttonhole_image' => null,
                'status' => 1,
                'created_date' => '2018-01-24 04:39:26',
                'modified_date' => '2018-05-21 19:19:11'
            ],
                [
                'title' => 'Only Placket',
                'price' => 5.00,
                'class' => 'icon-Only_placket',
                'mask_front_image' => null,
                'mask_side_image' => null,
                'mask_back_image' => null,
                'mask_fold_image' => null,
                'mask_zoom_view' => null,
                'mask_front_buttonhole_image' => null,
                'mask_side_buttonhole_image' => null,
                'mask_back_buttonhole_image' => null,
                'mask_fold_buttonhole_image' => null,
                'mask_zoomview_buttonhole_image' => 0,
                'status' => 1,
                'created_date' => '2018-01-24 04:54:07',
                'modified_date' => '2018-05-21 19:19:33'
            ]
        ];

        foreach ($shirtAccentThreadsData as $data) {
            $this->_shirtAccentThreads->create()->addData($data)->save();
        }


        //shirt_button_fabric
        $shirtButtonFabricData = [
                [
                'title' => 'Thread',
                'class' => 'icos-thread',
                'price' => 10.00,
                'btn_count' => 1,
                'fabric_thumb' => 'shirtbuttonfabric_images/thumbs/threadfabric_1_main.png',
                'fabric_real_image' => 'shirtbuttonfabric_images/threadfabric_1_main.png'
            ],
                [
                'title' => 'Brown',
                'class' => null,
                'price' => null,
                'btn_count' => null,
                'fabric_thumb' => 'shirtbuttonfabric_images/thumbs/threadfabric_4_main.png',
                'fabric_real_image' => 'shirtbuttonfabric_images/threadfabric_4_main.png'
            ],
                [
                'title' => 'Oranage',
                'class' => null,
                'price' => null,
                'btn_count' => null,
                'fabric_thumb' => 'shirtbuttonfabric_images/thumbs/solid_01.png',
                'fabric_real_image' => 'shirtbuttonfabric_images/solid_01.png'
            ]
        ];

        foreach ($shirtButtonFabricData as $data) {
            $this->_shirtButtonFabric->create()->addData($data)->save();
        }


        //shirt_fabric
        $ShirtFabricData = [
                [
                'fabric_name' => 'Montana',
                'fabric_thumb' => 'fab_shirt_images/thumbs/5a66d37255d8a_12.png',
                'fabric_real_image' => 'fab_shirt_images/5a66d37257791_12.jpg',
                'price' => 10.00,
                'status' => 1,
                'shirt_material_id' => 3,
                'shirt_pattern_id' => 2,
                'shirt_season_id' => 2,
                'shirt_color_id' => 2,
                'shirt_category_id' => 7,
                'display_fabric_thumb' => 'fab_shirt_images/display_thumb/5a66d37243145_12_Montana.png'
            ],
                [
                'fabric_name' => 'Conrad',
                'fabric_thumb' => 'fab_shirt_images/thumbs/5a66d76f74072_03.png',
                'fabric_real_image' => 'fab_shirt_images/5a66d76f7411c_03.jpg',
                'price' => 40.00,
                'status' => 1,
                'shirt_material_id' => 1,
                'shirt_pattern_id' => 2,
                'shirt_season_id' => 2,
                'shirt_color_id' => 2,
                'shirt_category_id' => 5,
                'display_fabric_thumb' => 'fab_shirt_images/display_thumb/5a66d76f73b25_03_Conrad.png'
            ],
                [
                'fabric_name' => 'Kent',
                'fabric_thumb' => 'fab_shirt_images/thumbs/5a66f669307aa_02.png',
                'fabric_real_image' => 'fab_shirt_images/5a66f66930868_02.jpg',
                'price' => 200.00,
                'status' => 1,
                'shirt_material_id' => 4,
                'shirt_pattern_id' => 3,
                'shirt_season_id' => 1,
                'shirt_color_id' => 3,
                'shirt_category_id' => 1,
                'display_fabric_thumb' => 'fab_shirt_images/display_thumb/5a66f6692a517_02_Kent.png'
            ],
                [
                'fabric_name' => 'Norfolk',
                'fabric_thumb' => 'fab_shirt_images/thumbs/5a672bce25a0f_14.png',
                'fabric_real_image' => 'fab_shirt_images/5a672bce25b05_14.jpg',
                'price' => 78.00,
                'status' => 1,
                'shirt_material_id' => 3,
                'shirt_pattern_id' => 1,
                'shirt_season_id' => 2,
                'shirt_color_id' => 2,
                'shirt_category_id' => 1,
                'display_fabric_thumb' => 'fab_shirt_images/display_thumb/5a672bce252cc_14_Norfolk.png'
            ],
                [
                'fabric_name' => 'Nashville',
                'fabric_thumb' => 'fab_shirt_images/thumbs/5a6732055e752_08.png',
                'fabric_real_image' => 'fab_shirt_images/5a6732055e846_08.jpg',
                'price' => 90.00,
                'status' => 1,
                'shirt_material_id' => 2,
                'shirt_pattern_id' => 4,
                'shirt_season_id' => 1,
                'shirt_color_id' => 5,
                'shirt_category_id' => 1,
                'display_fabric_thumb' => 'fab_shirt_images/display_thumb/5a6732055e0cf_08_Nashville.png'
            ],
                [
                'fabric_name' => 'Kent',
                'fabric_thumb' => 'fab_shirt_images/thumbs/5a699616e444a_02.png',
                'fabric_real_image' => 'fab_shirt_images/5a699616e454a_02.jpg',
                'price' => 100.00,
                'status' => 1,
                'shirt_material_id' => 1,
                'shirt_pattern_id' => 1,
                'shirt_season_id' => 1,
                'shirt_color_id' => 3,
                'shirt_category_id' => 1,
                'display_fabric_thumb' => 'fab_shirt_images/display_thumb/5a699616e3d53_02_Kent.png'
            ],
                [
                'fabric_name' => 'Test1',
                'fabric_thumb' => 'fab_shirt_images/thumbs/5ae9c800ac670_fabric_01_100.png',
                'fabric_real_image' => 'fab_shirt_images/5ae9c800ac9f6_fabric_01_1000.png',
                'price' => 30.00,
                'status' => 1,
                'shirt_material_id' => 5,
                'shirt_pattern_id' => 1,
                'shirt_season_id' => 1,
                'shirt_color_id' => 5,
                'shirt_category_id' => 1,
                'display_fabric_thumb' => 'fab_shirt_images/display_thumb/5ae9c800abcdc_fabric_01_360.png'
            ],
                [
                'fabric_name' => 'Test2',
                'fabric_thumb' => 'fab_shirt_images/thumbs/5ae9cba6be236_fabric_02_100.png',
                'fabric_real_image' => 'fab_shirt_images/5ae9cba6be337_Fabric_02_1000.png',
                'price' => 40.00,
                'status' => 1,
                'shirt_material_id' => 2,
                'shirt_pattern_id' => 1,
                'shirt_season_id' => 3,
                'shirt_color_id' => 6,
                'shirt_category_id' => 7,
                'display_fabric_thumb' => 'fab_shirt_images/display_thumb/5ae9cba6bda96_fabric_02_360.png'
            ]
        ];

        foreach ($ShirtFabricData as $data) {
            $this->_ShirtFabric->create()->addData($data)->save();
        }
    }

}
