-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 20, 2018 at 10:49 AM
-- Server version: 5.7.22-0ubuntu0.16.04.1
-- PHP Version: 7.1.17-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `Magentop`
--

-- --------------------------------------------------------

--
-- Table structure for table `shirt_accent_collarstyle`
--

CREATE TABLE `shirt_accent_collarstyle` (
  `collarstyle_id` int(10) NOT NULL,
  `title` varchar(40) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `class` varchar(100) NOT NULL,
  `status` smallint(6) NOT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `shirt_accent_collarstyle`
--

INSERT INTO `shirt_accent_collarstyle` (`collarstyle_id`, `title`, `price`, `class`, `status`, `created_date`, `modified_date`) VALUES
(1, 'By Default', '0.00', 'icon-By_Default', 1, '2017-12-30 07:03:27', '2018-06-27 10:22:51'),
(2, 'All', '14.00', 'icon-All', 1, '2017-12-30 07:04:46', '2018-06-27 10:23:01'),
(3, 'Outer Fabric', '10.00', 'icon-Oruter', 1, '2018-01-22 00:47:27', '2018-05-21 06:46:19'),
(4, 'Inner Fabric', '10.00', 'icon-Inner', 1, '2018-01-22 00:47:42', '2018-05-21 06:46:11');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `shirt_accent_collarstyle`
--
ALTER TABLE `shirt_accent_collarstyle`
  ADD PRIMARY KEY (`collarstyle_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `shirt_accent_collarstyle`
--
ALTER TABLE `shirt_accent_collarstyle`
  MODIFY `collarstyle_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
