-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 20, 2018 at 10:51 AM
-- Server version: 5.7.22-0ubuntu0.16.04.1
-- PHP Version: 7.1.17-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `Magentop`
--

-- --------------------------------------------------------

--
-- Table structure for table `shirtbottom`
--

CREATE TABLE `shirtbottom` (
  `shirtbottom_id` int(10) UNSIGNED NOT NULL COMMENT 'Shirt Bottom ID',
  `title` varchar(100) DEFAULT NULL COMMENT 'Title',
  `class` varchar(100) DEFAULT NULL COMMENT 'Class',
  `price` float DEFAULT NULL COMMENT 'Price',
  `gender` tinyint(1) DEFAULT NULL COMMENT 'Gender',
  `view1_image` varchar(200) DEFAULT NULL COMMENT 'View 1 Image',
  `view2_image` varchar(255) DEFAULT NULL COMMENT 'View 2 Image',
  `view3_image` varchar(255) DEFAULT NULL COMMENT 'View 3 Image',
  `status` smallint(6) DEFAULT NULL COMMENT 'Status'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='shirtbottom';

--
-- Dumping data for table `shirtbottom`
--

INSERT INTO `shirtbottom` (`shirtbottom_id`, `title`, `class`, `price`, `gender`, `view1_image`, `view2_image`, `view3_image`, `status`) VALUES
(1, 'Tail', 'icon-Tailed', 5, 0, 'shirt_images/bottom/Shirt_Bottom_1_1.png', 'shirt_images/bottom/Shirt_Bottom_2_1.png', 'shirt_images/bottom/Shirt_Bottom_3_1.png', 1),
(2, 'Square', 'icon-Squared', 5, 0, 'shirt_images/bottom/Shirt_Bottom_1_2.png', 'shirt_images/bottom/Shirt_Bottom_2_2.png', 'shirt_images/bottom/Shirt_Bottom_3_2.png', 1);

-- --------------------------------------------------------

--
-- Table structure for table `shirtcategory`
--

CREATE TABLE `shirtcategory` (
  `shirtcategory_id` int(10) UNSIGNED NOT NULL COMMENT 'Shirt Catagory ID',
  `title` varchar(100) DEFAULT NULL COMMENT 'Title',
  `class` varchar(100) DEFAULT NULL COMMENT 'Class',
  `status` smallint(6) DEFAULT NULL COMMENT 'Status'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='shirtcategory';

-- --------------------------------------------------------

--
-- Table structure for table `shirtcolor`
--

CREATE TABLE `shirtcolor` (
  `shirtcolor_id` int(10) UNSIGNED NOT NULL COMMENT 'Shirt Color ID',
  `title` varchar(100) DEFAULT NULL COMMENT 'Title',
  `status` smallint(6) DEFAULT NULL COMMENT 'Status'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='shirtcolor';

--
-- Dumping data for table `shirtcolor`
--

INSERT INTO `shirtcolor` (`shirtcolor_id`, `title`, `status`) VALUES
(1, 'Grey', 1),
(2, 'Black', 1),
(3, 'Blue', 1),
(4, 'Brown', 1),
(5, 'White Shade', 1),
(6, 'pink', 1);

-- --------------------------------------------------------

--
-- Table structure for table `shirtcuff`
--

CREATE TABLE `shirtcuff` (
  `shirtcuff_id` int(11) UNSIGNED NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `class` varchar(200) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `type` enum('0','1','2') DEFAULT NULL,
  `gender` enum('0','1') DEFAULT NULL,
  `glow_front_left_image` varchar(255) DEFAULT NULL,
  `mask_front_left_image` varchar(255) DEFAULT NULL,
  `highlighted_front_left_image` varchar(255) DEFAULT NULL,
  `glow_front_right_image` varchar(255) DEFAULT NULL,
  `mask_front_right_image` varchar(255) DEFAULT NULL,
  `highlighted_front_right_image` varchar(255) DEFAULT NULL,
  `glow_front_opencuff_left_image` varchar(255) DEFAULT NULL,
  `mask_front_opencuff_left_image` varchar(255) DEFAULT NULL,
  `highlighted_front_opencuff_left_image` varchar(255) DEFAULT NULL,
  `glow_front_opencuff_right_image` varchar(255) DEFAULT NULL,
  `mask_front_opencuff_right_image` varchar(255) DEFAULT NULL,
  `highlighted_front_opencuff_right_image` varchar(255) DEFAULT NULL,
  `glow_side_left_image` varchar(255) DEFAULT NULL,
  `mask_side_left_image` varchar(255) DEFAULT NULL,
  `highlighted_side_left_image` varchar(255) DEFAULT NULL,
  `glow_side_right_image` varchar(255) DEFAULT NULL,
  `mask_side_right_image` varchar(255) DEFAULT NULL,
  `highlighted_side_right_image` varchar(255) DEFAULT NULL,
  `glow_side_opencuff_left_image` varchar(255) DEFAULT NULL,
  `mask_side_opencuff_left_image` varchar(255) DEFAULT NULL,
  `highlighted_side_opencuff_left_image` varchar(255) DEFAULT NULL,
  `glow_side_opencuff_right_image` varchar(255) DEFAULT NULL,
  `mask_side_opencuff_right_image` varchar(255) DEFAULT NULL,
  `highlighted_side_opencuff_right_image` varchar(255) DEFAULT NULL,
  `glow_back_left_image` varchar(255) DEFAULT NULL,
  `mask_back_left_image` varchar(255) DEFAULT NULL,
  `highlighted_back_left_image` varchar(255) DEFAULT NULL,
  `glow_back_right_image` varchar(255) DEFAULT NULL,
  `mask_back_right_image` varchar(255) DEFAULT NULL,
  `highlighted_back_right_image` varchar(255) DEFAULT NULL,
  `glow_back_opencuff_left_image` varchar(255) DEFAULT NULL,
  `mask_back_opencuff_left_image` varchar(255) DEFAULT NULL,
  `highlighted_back_opencuff_left_image` varchar(255) DEFAULT NULL,
  `glow_back_opencuff_right_image` varchar(255) DEFAULT NULL,
  `mask_back_opencuff_right_image` varchar(255) DEFAULT NULL,
  `highlighted_back_opencuff_right_image` varchar(255) DEFAULT NULL,
  `glow_fold_main_image` varchar(255) DEFAULT NULL,
  `mask_fold_main_image` varchar(255) DEFAULT NULL,
  `highlighted_fold_main_image` varchar(255) DEFAULT NULL,
  `glow_fold_inner_image` varchar(255) DEFAULT NULL,
  `mask_fold_inner_image` varchar(255) DEFAULT NULL,
  `highlighted_fold_inner_image` varchar(255) DEFAULT NULL,
  `back_button_image` varchar(255) DEFAULT NULL,
  `fold_button_image` varchar(255) DEFAULT NULL,
  `back_thread_image` varchar(255) DEFAULT NULL,
  `fold_thread_image` varchar(255) DEFAULT NULL,
  `status` smallint(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `shirtcuff`
--

INSERT INTO `shirtcuff` (`shirtcuff_id`, `title`, `class`, `price`, `type`, `gender`, `glow_front_left_image`, `mask_front_left_image`, `highlighted_front_left_image`, `glow_front_right_image`, `mask_front_right_image`, `highlighted_front_right_image`, `glow_front_opencuff_left_image`, `mask_front_opencuff_left_image`, `highlighted_front_opencuff_left_image`, `glow_front_opencuff_right_image`, `mask_front_opencuff_right_image`, `highlighted_front_opencuff_right_image`, `glow_side_left_image`, `mask_side_left_image`, `highlighted_side_left_image`, `glow_side_right_image`, `mask_side_right_image`, `highlighted_side_right_image`, `glow_side_opencuff_left_image`, `mask_side_opencuff_left_image`, `highlighted_side_opencuff_left_image`, `glow_side_opencuff_right_image`, `mask_side_opencuff_right_image`, `highlighted_side_opencuff_right_image`, `glow_back_left_image`, `mask_back_left_image`, `highlighted_back_left_image`, `glow_back_right_image`, `mask_back_right_image`, `highlighted_back_right_image`, `glow_back_opencuff_left_image`, `mask_back_opencuff_left_image`, `highlighted_back_opencuff_left_image`, `glow_back_opencuff_right_image`, `mask_back_opencuff_right_image`, `highlighted_back_opencuff_right_image`, `glow_fold_main_image`, `mask_fold_main_image`, `highlighted_fold_main_image`, `glow_fold_inner_image`, `mask_fold_inner_image`, `highlighted_fold_inner_image`, `back_button_image`, `fold_button_image`, `back_thread_image`, `fold_thread_image`, `status`) VALUES
(1, 'Double rounded french', 'icon-double_rounded_french1', '5.00', '1', '1', 'glow_mask_shirt/Front_View/Cuffs/French_left_shad.png', 'glow_mask_shirt/Front_View/Cuffs/French_left_mask.png', 'glow_mask_shirt/Front_View/Cuffs/French_left_hi.png', 'glow_mask_shirt/Front_View/Cuffs/French_right_shad.png', 'glow_mask_shirt/Front_View/Cuffs/French_right_mask.png', 'glow_mask_shirt/Front_View/Cuffs/French_right_hi.png', 'glow_mask_shirt/Open_Cuffs/Front/CasualCuff_Front_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_glow.png', 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png', 'glow_mask_shirt/Side_View/Cuffs/Normal_left_shad.png', 'glow_mask_shirt/Side_View/Cuffs/Normal_left_mask.png', 'glow_mask_shirt/Side_View/Cuffs/Normal_left_hi.png', 'glow_mask_shirt/Side_View/Cuffs/Normal_right_shad.png', 'glow_mask_shirt/Side_View/Cuffs/Normal_right_mask.png', 'glow_mask_shirt/Side_View/Cuffs/Normal_right_hi.png', 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_glow.png', 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_glow.png', 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png', 'glow_mask_shirt/Back_View/Cuffs/DoubleFrenchRounded_left_shad.png', 'glow_mask_shirt/Back_View/Cuffs/DoubleFrenchRounded_left_mask.png', 'glow_mask_shirt/Back_View/Cuffs/DoubleFrenchRounded_left_hi.png', 'glow_mask_shirt/Back_View/Cuffs/DoubleFrenchRounded_right_shad.png', 'glow_mask_shirt/Back_View/Cuffs/DoubleFrenchRounded_right_mask.png', 'glow_mask_shirt/Back_View/Cuffs/DoubleFrenchRounded_right_hi.png', 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_glow.png', 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_glow.png', 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png', 'glow_mask_shirt/Folded_View/Cuffs/DoubleFrenchRounded_Main_shad.png', 'glow_mask_shirt/Folded_View/Cuffs/DoubleFrenchRounded_Main_mask.png', 'glow_mask_shirt/Folded_View/Cuffs/DoubleFrenchRounded_Main_hi.png', 'glow_mask_shirt/Folded_View/Cuffs/DoubleFrenchRounded_Inner_shad.png', 'glow_mask_shirt/Folded_View/Cuffs/DoubleFrenchRounded_Inner_mask.png', 'glow_mask_shirt/Folded_View/Cuffs/DoubleFrenchRounded_Inner_hi.png', NULL, NULL, NULL, NULL, 1),
(2, 'Double Squared french', 'icon-double_squared_french2', '5.00', '1', '1', 'glow_mask_shirt/Front_View/Cuffs/French_left_shad.png', 'glow_mask_shirt/Front_View/Cuffs/French_left_mask.png', 'glow_mask_shirt/Front_View/Cuffs/French_left_hi.png', 'glow_mask_shirt/Front_View/Cuffs/French_right_shad.png', 'glow_mask_shirt/Front_View/Cuffs/French_right_mask.png', 'glow_mask_shirt/Front_View/Cuffs/French_right_hi.png', 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_glow.png', 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_glow.png', 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png', 'glow_mask_shirt/Side_View/Cuffs/Normal_left_shad.png', 'glow_mask_shirt/Side_View/Cuffs/Normal_left_mask.png', 'glow_mask_shirt/Side_View/Cuffs/Normal_left_hi.png', 'glow_mask_shirt/Side_View/Cuffs/Normal_right_shad.png', 'glow_mask_shirt/Side_View/Cuffs/Normal_right_mask.png', 'glow_mask_shirt/Side_View/Cuffs/Normal_right_hi.png', 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_glow.png', 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_glow.png', 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png', 'glow_mask_shirt/Back_View/Cuffs/DoubleFrenchStraight_left_shad.png', 'glow_mask_shirt/Back_View/Cuffs/DoubleFrenchStraight_left_mask.png', 'glow_mask_shirt/Back_View/Cuffs/DoubleFrenchStraight_left_hi.png', 'glow_mask_shirt/Back_View/Cuffs/DoubleFrenchStraight_right_shad.png', 'glow_mask_shirt/Back_View/Cuffs/DoubleFrenchStraight_right_mask.png', 'glow_mask_shirt/Back_View/Cuffs/DoubleFrenchStraight_right_hi.png', 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_glow.png', 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png', 'glow_mask_shirt/Folded_View/Cuffs/DoubleFrenchStraight_left_shad.png', 'glow_mask_shirt/Folded_View/Cuffs/DoubleFrenchStraight_Main_shad.png', 'glow_mask_shirt/Folded_View/Cuffs/DoubleFrenchStraight_Main_mask.png', 'glow_mask_shirt/Folded_View/Cuffs/DoubleFrenchStraight_Main_hi.png', 'glow_mask_shirt/Folded_View/Cuffs/DoubleFrenchStraight_Inner_shad.png', 'glow_mask_shirt/Folded_View/Cuffs/DoubleFrenchStraight_Inner_mask.png', 'glow_mask_shirt/Folded_View/Cuffs/DoubleFrenchStraight_Inner_hi.png', NULL, NULL, NULL, NULL, 1),
(3, 'One-button-cut', 'icon-one_button_cut3', '5.00', '1', '1', 'glow_mask_shirt/Front_View/Cuffs/normal_left_shad.png', 'glow_mask_shirt/Front_View/Cuffs/normal_left_mask.png', 'glow_mask_shirt/Front_View/Cuffs/normal_left_hi.png', 'glow_mask_shirt/Front_View/Cuffs/normal_right_shad.png', 'glow_mask_shirt/Front_View/Cuffs/normal_right_mask.png', 'glow_mask_shirt/Front_View/Cuffs/normal_right_hi.png', 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_glow.png', 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_glow.png', 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png', 'glow_mask_shirt/Side_View/Cuffs/Normal_left_shad.png', 'glow_mask_shirt/Side_View/Cuffs/Normal_left_mask.png', 'glow_mask_shirt/Side_View/Cuffs/Normal_left_hi.png', 'glow_mask_shirt/Side_View/Cuffs/Normal_right_shad.png', 'glow_mask_shirt/Side_View/Cuffs/Normal_right_mask.png', 'glow_mask_shirt/Side_View/Cuffs/Normal_right_hi.png', 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_glow.png', 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_glow.png', 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png', 'glow_mask_shirt/Back_View/Cuffs/NormalCut_left_shad.png', 'glow_mask_shirt/Back_View/Cuffs/NormalCut_left_mask.png', 'glow_mask_shirt/Back_View/Cuffs/NormalCut_left_hi.png', 'glow_mask_shirt/Back_View/Cuffs/NormalCut_right_shad.png', 'glow_mask_shirt/Back_View/Cuffs/NormalCut_right_mask.png', 'glow_mask_shirt/Back_View/Cuffs/NormalCut_right_hi.png', 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_glow.png', 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_glow.png', 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png', 'glow_mask_shirt/Folded_View/Cuffs/NormalCut_Main_shad.png', 'glow_mask_shirt/Folded_View/Cuffs/NormalCut_Main_mask.png', 'glow_mask_shirt/Folded_View/Cuffs/NormalCut_Main_hi.png', 'glow_mask_shirt/Folded_View/Cuffs/NormalCut_Inner_shad.png', 'glow_mask_shirt/Folded_View/Cuffs/NormalCut_Inner_mask.png', 'glow_mask_shirt/Folded_View/Cuffs/NormalCut_Inner_hi.png', NULL, NULL, NULL, NULL, 1),
(4, 'Rounded 1 button', 'icon-one_button_rounded4', '5.00', '1', '1', 'glow_mask_shirt/Front_View/Cuffs/normal_left_shad.png', 'glow_mask_shirt/Front_View/Cuffs/normal_left_mask.png', 'glow_mask_shirt/Front_View/Cuffs/normal_left_hi.png', 'glow_mask_shirt/Front_View/Cuffs/normal_right_shad.png', 'glow_mask_shirt/Front_View/Cuffs/normal_right_mask.png', 'glow_mask_shirt/Front_View/Cuffs/normal_right_hi.png', 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_glow.png', 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_glow.png', 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png', 'glow_mask_shirt/Side_View/Cuffs/Normal_left_shad.png', 'glow_mask_shirt/Side_View/Cuffs/Normal_left_mask.png', 'glow_mask_shirt/Side_View/Cuffs/Normal_left_hi.png', 'glow_mask_shirt/Side_View/Cuffs/Normal_right_shad.png', 'glow_mask_shirt/Side_View/Cuffs/Normal_right_mask.png', 'glow_mask_shirt/Side_View/Cuffs/Normal_right_hi.png', 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_glow.png', 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_glow.png', 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png', 'glow_mask_shirt/Back_View/Cuffs/NormalRounded_left_shad.png', 'glow_mask_shirt/Back_View/Cuffs/NormalRounded_left_mask.png', 'glow_mask_shirt/Back_View/Cuffs/NormalRounded_left_hi.png', 'glow_mask_shirt/Back_View/Cuffs/NormalRounded_right_shad.png', 'glow_mask_shirt/Back_View/Cuffs/NormalRounded_right_mask.png', 'glow_mask_shirt/Back_View/Cuffs/NormalRounded_right_hi.png', 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_glow.png', 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_glow.png', 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png', 'glow_mask_shirt/Folded_View/Cuffs/NormalRounded_Main_shad.png', 'glow_mask_shirt/Folded_View/Cuffs/NormalRounded_Main_mask.png', 'glow_mask_shirt/Folded_View/Cuffs/NormalRounded_Main_hi.png', 'glow_mask_shirt/Folded_View/Cuffs/NormalRounded_Inner_shad.png', 'glow_mask_shirt/Folded_View/Cuffs/NormalRounded_Inner_mask.png', 'glow_mask_shirt/Folded_View/Cuffs/NormalRounded_Inner_hi.png', NULL, NULL, NULL, NULL, 1),
(5, 'Rounded 2 buttons', 'icon-two_button_rounded5', '5.00', '1', '1', 'glow_mask_shirt/Front_View/Cuffs/normal_left_shad.png', 'glow_mask_shirt/Front_View/Cuffs/normal_left_mask.png', 'glow_mask_shirt/Front_View/Cuffs/normal_left_hi.png', 'glow_mask_shirt/Front_View/Cuffs/normal_right_shad.png', 'glow_mask_shirt/Front_View/Cuffs/normal_right_mask.png', 'glow_mask_shirt/Front_View/Cuffs/normal_right_hi.png', 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_glow.png', 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_glow.png', 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png', 'glow_mask_shirt/Side_View/Cuffs/Normal_left_shad.png', 'glow_mask_shirt/Side_View/Cuffs/Normal_left_mask.png', 'glow_mask_shirt/Side_View/Cuffs/Normal_left_hi.png', 'glow_mask_shirt/Side_View/Cuffs/Normal_right_shad.png', 'glow_mask_shirt/Side_View/Cuffs/Normal_right_mask.png', 'glow_mask_shirt/Side_View/Cuffs/Normal_right_hi.png', 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_glow.png', 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_glow.png', 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png', 'glow_mask_shirt/Back_View/Cuffs/NormalRounded_left_shad.png', 'glow_mask_shirt/Back_View/Cuffs/NormalRounded_left_mask.png', 'glow_mask_shirt/Back_View/Cuffs/NormalRounded_left_hi.png', 'glow_mask_shirt/Back_View/Cuffs/NormalRounded_right_shad.png', 'glow_mask_shirt/Back_View/Cuffs/NormalRounded_right_mask.png', 'glow_mask_shirt/Back_View/Cuffs/NormalRounded_right_hi.png', 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_glow.png', 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_glow.png', 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png', 'glow_mask_shirt/Folded_View/Cuffs/NormalRounded_Main_shad.png', 'glow_mask_shirt/Folded_View/Cuffs/NormalRounded_Main_mask.png', 'glow_mask_shirt/Folded_View/Cuffs/NormalRounded_Main_hi.png', 'glow_mask_shirt/Folded_View/Cuffs/NormalRounded_Inner_shad.png', 'glow_mask_shirt/Folded_View/Cuffs/NormalRounded_Inner_mask.png', 'glow_mask_shirt/Folded_View/Cuffs/NormalRounded_Inner_hi.png', NULL, NULL, NULL, NULL, 1),
(6, 'Rounded french', 'icon-rounded_french6', '5.00', '1', '1', 'glow_mask_shirt/Front_View/Cuffs/French_left_shad.png', 'glow_mask_shirt/Front_View/Cuffs/French_left_mask.png', 'glow_mask_shirt/Front_View/Cuffs/French_left_hi.png', 'glow_mask_shirt/Front_View/Cuffs/French_right_shad.png', 'glow_mask_shirt/Front_View/Cuffs/French_right_mask.png', 'glow_mask_shirt/Front_View/Cuffs/French_right_hi.png', 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_glow.png', 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_glow.png', 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png', 'glow_mask_shirt/Side_View/Cuffs/Normal_left_shad.png', 'glow_mask_shirt/Side_View/Cuffs/Normal_left_mask.png', 'glow_mask_shirt/Side_View/Cuffs/Normal_left_hi.png', 'glow_mask_shirt/Side_View/Cuffs/Normal_right_shad.png', 'glow_mask_shirt/Side_View/Cuffs/Normal_right_mask.png', 'glow_mask_shirt/Side_View/Cuffs/Normal_right_hi.png', 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_glow.png', 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_glow.png', 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png', 'glow_mask_shirt/Back_View/Cuffs/FrenchRound_left_shad.png', 'glow_mask_shirt/Back_View/Cuffs/FrenchRound_left_mask.png', 'glow_mask_shirt/Back_View/Cuffs/FrenchRound_left_hi.png', 'glow_mask_shirt/Back_View/Cuffs/FrenchRound_right_shad.png', 'glow_mask_shirt/Back_View/Cuffs/FrenchRound_right_mask.png', 'glow_mask_shirt/Back_View/Cuffs/FrenchRound_right_hi.png', 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_glow.png', 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_glow.png', 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png', 'glow_mask_shirt/Folded_View/Cuffs/FrenchRounded_Main_shad.png', 'glow_mask_shirt/Folded_View/Cuffs/FrenchRounded_Main_mask.png', 'glow_mask_shirt/Folded_View/Cuffs/FrenchRounded_Main_hi.png', 'glow_mask_shirt/Folded_View/Cuffs/FrenchRounded_Inner_shad.png', 'glow_mask_shirt/Folded_View/Cuffs/FrenchRounded_Inner_mask.png', 'glow_mask_shirt/Folded_View/Cuffs/FrenchRounded_Inner_hi.png', NULL, NULL, NULL, NULL, 1),
(7, 'Single cuff 1 button', 'icon-one_button_straight7', '5.00', '1', '1', 'glow_mask_shirt/Front_View/Cuffs/normal_left_shad.png', 'glow_mask_shirt/Front_View/Cuffs/normal_left_mask.png', 'glow_mask_shirt/Front_View/Cuffs/normal_left_hi.png', 'glow_mask_shirt/Front_View/Cuffs/normal_right_shad.png', 'glow_mask_shirt/Front_View/Cuffs/normal_right_mask.png', 'glow_mask_shirt/Front_View/Cuffs/normal_right_hi.png', 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_glow.png', 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_glow.png', 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png', 'glow_mask_shirt/Side_View/Cuffs/Normal_left_shad.png', 'glow_mask_shirt/Side_View/Cuffs/Normal_left_mask.png', 'glow_mask_shirt/Side_View/Cuffs/Normal_left_hi.png', 'glow_mask_shirt/Side_View/Cuffs/Normal_right_shad.png', 'glow_mask_shirt/Side_View/Cuffs/Normal_right_mask.png', 'glow_mask_shirt/Side_View/Cuffs/Normal_right_hi.png', 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_glow.png', 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_glow.png', 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png', 'glow_mask_shirt/Back_View/Cuffs/NormalStraight_left_shad.png', 'glow_mask_shirt/Back_View/Cuffs/NormalStraight_left_mask.png', 'glow_mask_shirt/Back_View/Cuffs/NormalStraight_left_hi.png', 'glow_mask_shirt/Back_View/Cuffs/NormalStraight_right_shad.png', 'glow_mask_shirt/Back_View/Cuffs/NormalStraight_right_mask.png', 'glow_mask_shirt/Back_View/Cuffs/NormalStraight_right_hi.png', 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_glow.png', 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_glow.png', 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png', 'glow_mask_shirt/Folded_View/Cuffs/NormalStraight_Main_shad.png', 'glow_mask_shirt/Folded_View/Cuffs/NormalStraight_Main_mask.png', 'glow_mask_shirt/Folded_View/Cuffs/NormalStraight_Main_hi.png', 'glow_mask_shirt/Folded_View/Cuffs/NormalStraight_Inner_shad.png', 'glow_mask_shirt/Folded_View/Cuffs/NormalStraight_Inner_mask.png', 'glow_mask_shirt/Folded_View/Cuffs/NormalStraight_Inner_hi.png', NULL, NULL, NULL, NULL, 1),
(8, 'Single cuff 2 buttons', 'icon-two_button_straight8', '5.00', '1', '1', 'glow_mask_shirt/Front_View/Cuffs/normal_left_shad.png', 'glow_mask_shirt/Front_View/Cuffs/normal_left_mask.png', 'glow_mask_shirt/Front_View/Cuffs/normal_left_hi.png', 'glow_mask_shirt/Front_View/Cuffs/normal_right_shad.png', 'glow_mask_shirt/Front_View/Cuffs/normal_right_mask.png', 'glow_mask_shirt/Front_View/Cuffs/normal_right_hi.png', 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_glow.png', 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_glow.png', 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png', 'glow_mask_shirt/Side_View/Cuffs/Normal_left_shad.png', 'glow_mask_shirt/Side_View/Cuffs/Normal_left_mask.png', 'glow_mask_shirt/Side_View/Cuffs/Normal_left_hi.png', 'glow_mask_shirt/Side_View/Cuffs/Normal_right_shad.png', 'glow_mask_shirt/Side_View/Cuffs/Normal_right_mask.png', 'glow_mask_shirt/Side_View/Cuffs/Normal_right_hi.png', 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_glow.png', 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_glow.png', 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png', 'glow_mask_shirt/Back_View/Cuffs/NormalStraight_left_shad.png', 'glow_mask_shirt/Back_View/Cuffs/NormalStraight_left_mask.png', 'glow_mask_shirt/Back_View/Cuffs/NormalStraight_left_hi.png', 'glow_mask_shirt/Back_View/Cuffs/NormalStraight_right_shad.png', 'glow_mask_shirt/Back_View/Cuffs/NormalStraight_right_mask.png', 'glow_mask_shirt/Back_View/Cuffs/NormalStraight_right_hi.png', 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_glow.png', 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_glow.png', 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png', 'glow_mask_shirt/Folded_View/Cuffs/NormalStraight_Main_shad.png', 'glow_mask_shirt/Folded_View/Cuffs/NormalStraight_Main_mask.png', 'glow_mask_shirt/Folded_View/Cuffs/NormalStraight_Main_hi.png', 'glow_mask_shirt/Folded_View/Cuffs/NormalStraight_Inner_shad.png', 'glow_mask_shirt/Folded_View/Cuffs/NormalStraight_Inner_mask.png', 'glow_mask_shirt/Folded_View/Cuffs/NormalStraight_Inner_hi.png', NULL, NULL, NULL, NULL, 1),
(9, 'Squared French', 'icon-squared_french9', '5.00', '1', '1', 'glow_mask_shirt/Front_View/Cuffs/French_left_shad.png', 'glow_mask_shirt/Front_View/Cuffs/French_left_mask.png', 'glow_mask_shirt/Front_View/Cuffs/French_left_hi.png', 'glow_mask_shirt/Front_View/Cuffs/French_right_shad.png', 'glow_mask_shirt/Front_View/Cuffs/French_right_mask.png', 'glow_mask_shirt/Front_View/Cuffs/French_right_hi.png', 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_glow.png', 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_glow.png', 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png', 'glow_mask_shirt/Side_View/Cuffs/Normal_left_shad.png', 'glow_mask_shirt/Side_View/Cuffs/Normal_left_mask.png', 'glow_mask_shirt/Side_View/Cuffs/Normal_left_hi.png', 'glow_mask_shirt/Side_View/Cuffs/Normal_right_shad.png', 'glow_mask_shirt/Side_View/Cuffs/Normal_right_mask.png', 'glow_mask_shirt/Side_View/Cuffs/Normal_right_hi.png', 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_glow.png', 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_glow.png', 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png', 'glow_mask_shirt/Back_View/Cuffs/FrenchStraight_left_shad.png', 'glow_mask_shirt/Back_View/Cuffs/FrenchStraight_left_mask.png', 'glow_mask_shirt/Back_View/Cuffs/FrenchStraight_left_hi.png', 'glow_mask_shirt/Back_View/Cuffs/FrenchStraight_right_shad.png', 'glow_mask_shirt/Back_View/Cuffs/FrenchStraight_right_mask.png', 'glow_mask_shirt/Back_View/Cuffs/FrenchStraight_right_hi.png', 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_glow.png', 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_glow.png', 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png', 'glow_mask_shirt/Folded_View/Cuffs/FrenchStraight_Main_shad.png', 'glow_mask_shirt/Folded_View/Cuffs/FrenchStraight_Main_mask.png', 'glow_mask_shirt/Folded_View/Cuffs/FrenchStraight_Main_hi.png', 'glow_mask_shirt/Folded_View/Cuffs/FrenchStraight_Inner_shad.png', 'glow_mask_shirt/Folded_View/Cuffs/FrenchStraight_Inner_mask.png', 'glow_mask_shirt/Folded_View/Cuffs/FrenchStraight_Inner_hi.png', NULL, NULL, NULL, NULL, 1),
(10, 'Two-button-cut', 'icon-two_button_cut10', '5.00', '1', '1', 'glow_mask_shirt/Front_View/Cuffs/normal_left_shad.png', 'glow_mask_shirt/Front_View/Cuffs/normal_left_mask.png', 'glow_mask_shirt/Front_View/Cuffs/normal_left_hi.png', 'glow_mask_shirt/Front_View/Cuffs/normal_right_shad.png', 'glow_mask_shirt/Front_View/Cuffs/normal_right_mask.png', 'glow_mask_shirt/Front_View/Cuffs/normal_right_hi.png', 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_glow.png', 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_glow.png', 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png', 'glow_mask_shirt/Side_View/Cuffs/Normal_left_shad.png', 'glow_mask_shirt/Side_View/Cuffs/Normal_left_mask.png', 'glow_mask_shirt/Side_View/Cuffs/Normal_left_hi.png', 'glow_mask_shirt/Side_View/Cuffs/Normal_right_shad.png', 'glow_mask_shirt/Side_View/Cuffs/Normal_right_mask.png', 'glow_mask_shirt/Side_View/Cuffs/Normal_right_hi.png', 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_glow.png', 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_glow.png', 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png', 'glow_mask_shirt/Back_View/Cuffs/NormalCut_left_shad.png', 'glow_mask_shirt/Back_View/Cuffs/NormalCut_left_mask.png', 'glow_mask_shirt/Back_View/Cuffs/NormalCut_left_hi.png', 'glow_mask_shirt/Back_View/Cuffs/NormalCut_right_shad.png', 'glow_mask_shirt/Back_View/Cuffs/NormalCut_right_mask.png', 'glow_mask_shirt/Back_View/Cuffs/NormalCut_right_hi.png', 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_glow.png', 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_glow.png', 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png', 'glow_mask_shirt/Folded_View/Cuffs/NormalCut_Main_shad.png', 'glow_mask_shirt/Folded_View/Cuffs/NormalCut_Main_mask.png', 'glow_mask_shirt/Folded_View/Cuffs/NormalCut_Main_hi.png', 'glow_mask_shirt/Folded_View/Cuffs/NormalCut_Inner_shad.png', 'glow_mask_shirt/Folded_View/Cuffs/NormalCut_Inner_mask.png', 'glow_mask_shirt/Folded_View/Cuffs/NormalCut_Inner_hi.png', NULL, NULL, NULL, NULL, 1),
(11, 'Narrow', 'icon-Narrow_cuff11', '5.00', '1', '1', 'glow_mask_shirt/Front_View/Cuffs/Narrown_left_shad.png', 'glow_mask_shirt/Front_View/Cuffs/Narrown_left_mask.png', 'glow_mask_shirt/Front_View/Cuffs/Narrown_left_hi.png', 'glow_mask_shirt/Front_View/Cuffs/Narrown_right_shad.png', 'glow_mask_shirt/Front_View/Cuffs/Narrown_right_mask.png', 'glow_mask_shirt/Front_View/Cuffs/Narrown_right_hi.png', 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_glow.png', 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_glow.png', 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png', 'glow_mask_shirt/Side_View/Cuffs/Narrow_left_shad.png', 'glow_mask_shirt/Side_View/Cuffs/Narrow_left_mask.png', 'glow_mask_shirt/Side_View/Cuffs/Narrow_left_hi.png', 'glow_mask_shirt/Side_View/Cuffs/Narrow_right_shad.png', 'glow_mask_shirt/Side_View/Cuffs/Narrow_right_mask.png', 'glow_mask_shirt/Side_View/Cuffs/Narrow_right_hi.png', 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_glow.png', 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_glow.png', 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png', 'glow_mask_shirt/Back_View/Cuffs/NarrowStraight_left_shad.png', 'glow_mask_shirt/Back_View/Cuffs/NarrowStraight_left_mask.png', 'glow_mask_shirt/Back_View/Cuffs/NarrowStraight_left_hi.png', 'glow_mask_shirt/Back_View/Cuffs/NarrowStraight_right_shad.png', 'glow_mask_shirt/Back_View/Cuffs/NarrowStraight_right_mask.png', 'glow_mask_shirt/Back_View/Cuffs/NarrowStraight_right_hi.png', 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_glow.png', 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_glow.png', 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png', 'glow_mask_shirt/Folded_View/Cuffs/NarrownStraight_Main_shad.png', 'glow_mask_shirt/Folded_View/Cuffs/NarrownStraight_Main_mask.png', 'glow_mask_shirt/Folded_View/Cuffs/NarrownStraight_Main_hi.png', 'glow_mask_shirt/Folded_View/Cuffs/NarrownStraight_Inner_shad.png', 'glow_mask_shirt/Folded_View/Cuffs/NarrownStraight_Inner_mask.png', 'glow_mask_shirt/Folded_View/Cuffs/NarrownStraight_Inner_hi.png', NULL, NULL, NULL, NULL, 1),
(12, 'French Cut', 'icon-French_Cut', '5.00', '1', '1', 'glow_mask_shirt/Front_View/Cuffs/French_left_shad.png', 'glow_mask_shirt/Front_View/Cuffs/French_left_mask.png', 'glow_mask_shirt/Front_View/Cuffs/French_left_hi.png', 'glow_mask_shirt/Front_View/Cuffs/French_right_shad.png', 'glow_mask_shirt/Front_View/Cuffs/French_right_mask.png', 'glow_mask_shirt/Front_View/Cuffs/French_right_hi.png', 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_glow.png', 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_glow.png', 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Front/Casualcuff_Front_left_mask.png', 'glow_mask_shirt/Side_View/Cuffs/FrenchCut_left_shad.png', 'glow_mask_shirt/Side_View/Cuffs/FrenchCut_left_mask.png', 'glow_mask_shirt/Side_View/Cuffs/FrenchCut_left_hi.png', 'glow_mask_shirt/Side_View/Cuffs/FrenchCut_right_shad.png', 'glow_mask_shirt/Side_View/Cuffs/FrenchCut_right_mask.png', 'glow_mask_shirt/Side_View/Cuffs/FrenchCut_right_hi.png', 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_glow.png', 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_glow.png', 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Side/Casualcuff_Side_left_mask.png', 'glow_mask_shirt/Back_View/Cuffs/FrenchCut_left_shad.png', 'glow_mask_shirt/Back_View/Cuffs/FrenchCut_left_mask.png', 'glow_mask_shirt/Back_View/Cuffs/FrenchCut_left_hi.png', 'glow_mask_shirt/Back_View/Cuffs/FrenchCut_right_shad.png', 'glow_mask_shirt/Back_View/Cuffs/FrenchCut_right_mask.png', 'glow_mask_shirt/Back_View/Cuffs/FrenchCut_right_hi.png', 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_glow.png', 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_glow.png', 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png', 'glow_mask_shirt/Open_Cuffs/Back/Casualcuff_Back_left_mask.png', 'glow_mask_shirt/Folded_View/Cuffs/FrenchCut_Main_shad.png', 'glow_mask_shirt/Folded_View/Cuffs/FrenchCut_Main_mask.png', 'glow_mask_shirt/Folded_View/Cuffs/FrenchCut_Main_hi.png', 'glow_mask_shirt/Folded_View/Cuffs/FrenchCut_Inner_shad.png', 'glow_mask_shirt/Folded_View/Cuffs/FrenchCut_Inner_mask.png', 'glow_mask_shirt/Folded_View/Cuffs/FrenchCut_Inner_hi.png', NULL, NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `shirtfit`
--

CREATE TABLE `shirtfit` (
  `shirtfit_id` int(10) UNSIGNED NOT NULL COMMENT 'Shirt Fit ID',
  `title` varchar(100) DEFAULT NULL COMMENT 'Title',
  `class` varchar(100) DEFAULT NULL COMMENT 'Class',
  `price` float DEFAULT NULL COMMENT 'Price',
  `gender` tinyint(1) DEFAULT NULL COMMENT 'Gender',
  `glow_front_image` varchar(200) DEFAULT NULL COMMENT 'glow_front_image',
  `mask_front_image` varchar(255) DEFAULT NULL COMMENT 'mask_front_image',
  `highlighted_front_image` varchar(255) DEFAULT NULL COMMENT 'highlighted_front_image',
  `glow_side_image` varchar(255) DEFAULT NULL COMMENT 'glow_side_image',
  `mask_side_image` varchar(255) DEFAULT NULL COMMENT 'mask_side_image',
  `highlighted_side_image` varchar(255) DEFAULT NULL COMMENT 'highlighted_side_image',
  `glow_back_image` varchar(255) DEFAULT NULL COMMENT 'glow_back_image',
  `mask_back_image` varchar(255) DEFAULT NULL COMMENT 'mask_back_image',
  `highlighted_back_image` varchar(255) DEFAULT NULL COMMENT 'highlighted_back_image',
  `glow_fold_image` varchar(255) DEFAULT NULL COMMENT 'glow_fold_image',
  `mask_fold_image` varchar(255) DEFAULT NULL COMMENT 'mask_fold_image',
  `highlighted_fold_image` varchar(255) DEFAULT NULL COMMENT 'highlighted_fold_image',
  `glow_zoom_image` varchar(255) DEFAULT NULL COMMENT 'glow_zoom_image',
  `mask_zoom_image` varchar(255) DEFAULT NULL COMMENT 'mask_zoom_image',
  `highlighted_zoom_image` varchar(255) DEFAULT NULL COMMENT 'highlighted_zoom_image',
  `status` smallint(6) DEFAULT NULL COMMENT 'Status'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='shirtfit';

--
-- Dumping data for table `shirtfit`
--

INSERT INTO `shirtfit` (`shirtfit_id`, `title`, `class`, `price`, `gender`, `glow_front_image`, `mask_front_image`, `highlighted_front_image`, `glow_side_image`, `mask_side_image`, `highlighted_side_image`, `glow_back_image`, `mask_back_image`, `highlighted_back_image`, `glow_fold_image`, `mask_fold_image`, `highlighted_fold_image`, `glow_zoom_image`, `mask_zoom_image`, `highlighted_zoom_image`, `status`) VALUES
(1, 'Loose', 'icon-Loose_Fit', 5, 0, 'glow_mask_shirt/Front_View/Fit/Shirt_Loosefit_Front_Shad.png', 'glow_mask_shirt/Front_View/Fit/Shirt_Loosefit_Front_Mask.png', 'glow_mask_shirt/Front_View/Fit/Shirt_Loosefit_Front_Hi.png', 'glow_mask_shirt/Side_View/Fit/Shirt_Loosefit_Side_Shad.png', 'glow_mask_shirt/Side_View/Fit/Shirt_Loosefit_Side_Mask.png', 'glow_mask_shirt/Side_View/Fit/Shirt_Loosefit_Side_Hi.png', 'glow_mask_shirt/Back_View/Fit/Shirt_Loosefit_Back_Shad.png', 'glow_mask_shirt/Back_View/Fit/Shirt_Loosefit_Back_Mask.png', 'glow_mask_shirt/Back_View/Fit/Shirt_Loosefit_Back_Hi.png', 'glow_mask_shirt/Folded_View/Fit/Shirt_Mainfit_Folded_Shad.png', 'glow_mask_shirt/Folded_View/Fit/Shirt_Mainfit_Folded_Mask.png', 'glow_mask_shirt/Folded_View/Fit/Shirt_Mainfit_Folded_Hi.png', 'glow_mask_shirt/Zoom_View/Fit/Torso_Shad.png', 'glow_mask_shirt/Zoom_View/Fit/Torso_Mask.png', 'glow_mask_shirt/Zoom_View/Fit/Torso_Hi.png', 1),
(2, 'Waisted', 'icon-Slim_Fit', 5, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `shirtmaterial`
--

CREATE TABLE `shirtmaterial` (
  `shirtmaterial_id` int(10) UNSIGNED NOT NULL COMMENT 'Shirt Fit ID',
  `title` varchar(100) DEFAULT NULL COMMENT 'Title',
  `status` smallint(6) DEFAULT NULL COMMENT 'Status'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='shirtmaterial';

--
-- Dumping data for table `shirtmaterial`
--

INSERT INTO `shirtmaterial` (`shirtmaterial_id`, `title`, `status`) VALUES
(1, 'Wool', 1),
(2, 'Corduroy', 1),
(3, 'linen', 1),
(4, 'Polyester', 1),
(5, 'Velvet', 1),
(6, 'Pure Cotton', 1);

-- --------------------------------------------------------

--
-- Table structure for table `shirtpattern`
--

CREATE TABLE `shirtpattern` (
  `shirtpattern_id` int(10) UNSIGNED NOT NULL COMMENT 'Shirt Fit ID',
  `title` varchar(100) DEFAULT NULL COMMENT 'Title',
  `status` smallint(6) DEFAULT NULL COMMENT 'Status'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='shirtpattern';

--
-- Dumping data for table `shirtpattern`
--

INSERT INTO `shirtpattern` (`shirtpattern_id`, `title`, `status`) VALUES
(1, 'Squared', 1),
(2, 'Plain', 1),
(3, 'Striped', 1),
(4, 'Chevron pattern', 1),
(5, 'Houndstooth ', 1),
(6, 'paisley', 1),
(7, 'herringbone', 1),
(8, 'Other', 1);

-- --------------------------------------------------------

--
-- Table structure for table `shirtplackets`
--

CREATE TABLE `shirtplackets` (
  `shirtplackets_id` int(10) UNSIGNED NOT NULL COMMENT 'Shirt Fit ID',
  `title` varchar(100) DEFAULT NULL COMMENT 'Title',
  `class` varchar(100) DEFAULT NULL COMMENT 'Class',
  `price` float DEFAULT NULL COMMENT 'Price',
  `thumb` varchar(100) DEFAULT NULL COMMENT 'Price',
  `glow_front_image` varchar(200) DEFAULT NULL COMMENT 'glow_front_image',
  `mask_front_image` varchar(200) DEFAULT NULL COMMENT 'mask_front_image',
  `highlighted_front_image` varchar(200) DEFAULT NULL COMMENT 'highlighted_front_image',
  `glow_side_image` varchar(200) DEFAULT NULL COMMENT 'glow_side_image',
  `mask_side_image` varchar(200) DEFAULT NULL COMMENT 'mask_side_image',
  `highlighted_side_image` varchar(200) DEFAULT NULL COMMENT 'highlighted_side_image',
  `glow_fold_image` varchar(200) DEFAULT NULL COMMENT 'glow_fold_image',
  `mask_fold_image` varchar(200) DEFAULT NULL COMMENT 'mask_fold_image',
  `highlighted_fold_image` varchar(200) DEFAULT NULL COMMENT 'highlighted_fold_image',
  `status` smallint(6) DEFAULT NULL COMMENT 'Status'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='shirtplackets';

--
-- Dumping data for table `shirtplackets`
--

INSERT INTO `shirtplackets` (`shirtplackets_id`, `title`, `class`, `price`, `thumb`, `glow_front_image`, `mask_front_image`, `highlighted_front_image`, `glow_side_image`, `mask_side_image`, `highlighted_side_image`, `glow_fold_image`, `mask_fold_image`, `highlighted_fold_image`, `status`) VALUES
(1, 'Standard', 'icon-Standard_Placket', 5, NULL, 'glow_mask_shirt/Front_View/Plackets/Shirt_Placket_Front_shad.png', 'glow_mask_shirt/Front_View/Plackets/Shirt_Placket_Front_mask.png', 'glow_mask_shirt/Front_View/Plackets/Shirt_Placket_Front_hi.png', 'glow_mask_shirt/Side_View/Plackets/Shirt_Placket_Side_shad.png', 'glow_mask_shirt/Side_View/Plackets/Shirt_Placket_Side_mask.png', 'glow_mask_shirt/Side_View/Plackets/Shirt_Placket_Side_hi.png', 'glow_mask_shirt/Folded_View/Plackets/Shirt_Placket_Folded_shad.png', 'glow_mask_shirt/Folded_View/Plackets/Shirt_Placket_Folded_mask.png', 'glow_mask_shirt/Folded_View/Plackets/Shirt_Placket_Folded_hi.png', 1),
(2, 'Hidden Buttons', 'icon-Hidden_Button_Placket', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(3, 'French', 'icon-French_Placket', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `shirtpleats`
--

CREATE TABLE `shirtpleats` (
  `shirtpleats_id` int(10) UNSIGNED NOT NULL COMMENT 'Shirt Fit ID',
  `title` varchar(100) DEFAULT NULL COMMENT 'Title',
  `class` varchar(100) DEFAULT NULL COMMENT 'Class',
  `price` float DEFAULT NULL COMMENT 'Price',
  `image` varchar(200) DEFAULT NULL COMMENT 'Image',
  `status` smallint(6) DEFAULT NULL COMMENT 'Status'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='shirtpleats';

--
-- Dumping data for table `shirtpleats`
--

INSERT INTO `shirtpleats` (`shirtpleats_id`, `title`, `class`, `price`, `image`, `status`) VALUES
(1, 'No Pleats', 'icon-No_Pleats1', 0, '', 1),
(2, 'Box pleat', 'icon-Box_Pleats2', 10, 'shirt_images/pleats/Shirt_pleats_2_Back.png', 1),
(3, 'Side folds', 'icon-Side_Folds3', 10, 'shirt_images/pleats/Shirt_pleats_3_Back.png', 1);

-- --------------------------------------------------------

--
-- Table structure for table `shirtpocket`
--

CREATE TABLE `shirtpocket` (
  `shirtpocket_id` int(11) UNSIGNED NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `class` varchar(200) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `type` enum('0','1','2') DEFAULT NULL,
  `thumb` varchar(200) DEFAULT NULL,
  `glow_front_left_image` varchar(255) DEFAULT NULL,
  `mask_front_left_image` varchar(200) DEFAULT NULL,
  `highlighted_front_left_image` varchar(200) DEFAULT NULL,
  `glow_front_right_image` varchar(255) DEFAULT NULL,
  `mask_front_right_image` varchar(200) DEFAULT NULL,
  `highlighted_front_right_image` varchar(200) DEFAULT NULL,
  `glow_side_left_image` varchar(255) DEFAULT NULL,
  `mask_side_left_image` varchar(200) DEFAULT NULL,
  `highlighted_side_left_image` varchar(200) DEFAULT NULL,
  `glow_side_right_image` varchar(255) DEFAULT NULL,
  `mask_side_right_image` varchar(200) DEFAULT NULL,
  `highlighted_side_right_image` varchar(200) DEFAULT NULL,
  `glow_fold_left_image` varchar(255) DEFAULT NULL,
  `mask_fold_left_image` varchar(200) DEFAULT NULL,
  `highlighted_fold_left_image` varchar(200) DEFAULT NULL,
  `glow_fold_right_image` varchar(255) DEFAULT NULL,
  `mask_fold_right_image` varchar(200) DEFAULT NULL,
  `highlighted_fold_right_image` varchar(200) DEFAULT NULL,
  `status` smallint(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `shirtpocket`
--

INSERT INTO `shirtpocket` (`shirtpocket_id`, `title`, `class`, `price`, `type`, `thumb`, `glow_front_left_image`, `mask_front_left_image`, `highlighted_front_left_image`, `glow_front_right_image`, `mask_front_right_image`, `highlighted_front_right_image`, `glow_side_left_image`, `mask_side_left_image`, `highlighted_side_left_image`, `glow_side_right_image`, `mask_side_right_image`, `highlighted_side_right_image`, `glow_fold_left_image`, `mask_fold_left_image`, `highlighted_fold_left_image`, `glow_fold_right_image`, `mask_fold_right_image`, `highlighted_fold_right_image`, `status`) VALUES
(1, 'No Pocket', 'icon-No_pocket_Icon', '5.00', '1', 'glow_mask/pocket/Pockets_Regular_Flaps_Main_Glow.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(2, 'Left Flap Pockets', 'icon-left_flap_pocket', '5.00', '1', 'glow_mask/pocket/Pockets_Regular_Flaps_Main_Glow.png', 'glow_mask_shirt/Front_View/Pockets/Flap_left_shad.png', 'glow_mask_shirt/Front_View/Pockets/Flap_left_mask.png', 'glow_mask_shirt/Front_View/Pockets/Flap_left_hi.png', NULL, NULL, NULL, 'glow_mask_shirt/Side_View/Pockets/FlapPocket_Left_shad.png', 'glow_mask_shirt/Side_View/Pockets/FlapPocket_Left_mask.png', 'glow_mask_shirt/Side_View/Pockets/FlapPocket_Left_hi.png', NULL, NULL, NULL, 'glow_mask_shirt/Folded_View/Pockets/FlappedPockets_left_Folded_shad.png', 'glow_mask_shirt/Folded_View/Pockets/FlappedPockets_left_Folded_mask.png', 'glow_mask_shirt/Folded_View/Pockets/FlappedPockets_left_Folded_hi.png', NULL, NULL, NULL, 1),
(3, 'Left Without flaps', ' icon-Withoutflap_Pocket_Left', '5.00', '1', 'glow_mask/pocket/Pockets_Regular_Flaps_Main_Glow.png', 'glow_mask_shirt/Front_View/Pockets/WithoutFlap_left_shad.png', 'glow_mask_shirt/Front_View/Pockets/WithoutFlap_left_mask.png', 'glow_mask_shirt/Front_View/Pockets/WithoutFlap_left_hi.png', NULL, NULL, NULL, 'glow_mask_shirt/Side_View/Pockets/WithoutFlap_Left_shad.png', 'glow_mask_shirt/Side_View/Pockets/WithoutFlap_Left_mask.png', 'glow_mask_shirt/Side_View/Pockets/WithoutFlap_Left_hi.png', NULL, NULL, NULL, 'glow_mask_shirt/Folded_View/Pockets/WithoutFlap_left_Folded_shad.png', 'glow_mask_shirt/Folded_View/Pockets/WithoutFlap_left_Folded_mask.png', 'glow_mask_shirt/Folded_View/Pockets/WithoutFlap_left_Folded_hi.png', NULL, NULL, NULL, 1),
(4, 'Right Flap Pockets', 'icon-right_flap_pocket', '5.00', '1', 'glow_mask/pocket/Pockets_Regular_Flaps_Main_Glow.png', NULL, NULL, NULL, 'glow_mask_shirt/Front_View/Pockets/Flap_right_shad.png', 'glow_mask_shirt/Front_View/Pockets/Flap_right_mask.png', 'glow_mask_shirt/Front_View/Pockets/Flap_right_hi.png', NULL, NULL, NULL, 'glow_mask_shirt/Side_View/Pockets/FlapPocket_Right_shad.png', 'glow_mask_shirt/Side_View/Pockets/FlapPocket_Right_mask.png', 'glow_mask_shirt/Side_View/Pockets/FlapPocket_Right_hi.png', NULL, NULL, NULL, 'glow_mask_shirt/Folded_View/Pockets/FlappedPockets_right_Folded_shad.png', 'glow_mask_shirt/Folded_View/Pockets/FlappedPockets_right_Folded_mask.png', 'glow_mask_shirt/Folded_View/Pockets/FlappedPockets_right_Folded_hi.png', 1),
(5, 'Right Without flaps', 'icon-Withoutflap_Pocket_Right', '5.00', '1', 'glow_mask/pocket/Pockets_Regular_Flaps_Main_Glow.png', NULL, NULL, NULL, 'glow_mask_shirt/Front_View/Pockets/WithoutFlap_right_shad.png', 'glow_mask_shirt/Front_View/Pockets/WithoutFlap_right_mask.png', 'glow_mask_shirt/Front_View/Pockets/WithoutFlap_right_hi.png', NULL, NULL, NULL, 'glow_mask_shirt/Side_View/Pockets/WithoutFlap_Right_shad.png', 'glow_mask_shirt/Side_View/Pockets/WithoutFlap_Right_mask.png', 'glow_mask_shirt/Side_View/Pockets/WithoutFlap_Right_hi.png', NULL, NULL, NULL, 'glow_mask_shirt/Folded_View/Pockets/WithoutFlap_right_Folded_shad.png', 'glow_mask_shirt/Folded_View/Pockets/WithoutFlap_right_Folded_mask.png', 'glow_mask_shirt/Folded_View/Pockets/WithoutFlap_right_Folded_hi.png', 1),
(6, 'Flap PocketsX2', 'icon-flap_pocket_x2', '5.00', '1', 'glow_mask/pocket/Pockets_Regular_Flaps_Main_Glow.png', 'glow_mask_shirt/Front_View/Pockets/Flap_left_shad.png', 'glow_mask_shirt/Front_View/Pockets/Flap_left_mask.png', 'glow_mask_shirt/Front_View/Pockets/Flap_left_hi.png', 'glow_mask_shirt/Front_View/Pockets/Flap_right_shad.png', 'glow_mask_shirt/Front_View/Pockets/Flap_right_mask.png', 'glow_mask_shirt/Front_View/Pockets/Flap_right_hi.png', 'glow_mask_shirt/Side_View/Pockets/FlapPocket_Left_shad.png', 'glow_mask_shirt/Side_View/Pockets/FlapPocket_Left_mask.png', 'glow_mask_shirt/Side_View/Pockets/FlapPocket_Left_hi.png', 'glow_mask_shirt/Side_View/Pockets/FlapPocket_Right_shad.png', 'glow_mask_shirt/Side_View/Pockets/FlapPocket_Right_mask.png', 'glow_mask_shirt/Side_View/Pockets/FlapPocket_Right_hi.png', 'glow_mask_shirt/Folded_View/Pockets/FlappedPockets_left_Folded_shad.png', 'glow_mask_shirt/Folded_View/Pockets/FlappedPockets_left_Folded_mask.png', 'glow_mask_shirt/Folded_View/Pockets/FlappedPockets_left_Folded_hi.png', 'glow_mask_shirt/Folded_View/Pockets/FlappedPockets_right_Folded_shad.png', 'glow_mask_shirt/Folded_View/Pockets/FlappedPockets_right_Folded_mask.png', 'glow_mask_shirt/Folded_View/Pockets/FlappedPockets_right_Folded_hi.png', 1),
(7, 'Without flapsX2', 'icon-Withoutflap_Pocket_x2', '5.00', '1', 'glow_mask/pocket/Pockets_Regular_Flaps_Main_Glow.png', 'glow_mask_shirt/Front_View/Pockets/WithoutFlap_left_shad.png', 'glow_mask_shirt/Front_View/Pockets/WithoutFlap_left_mask.png', 'glow_mask_shirt/Front_View/Pockets/WithoutFlap_left_hi.png', 'glow_mask_shirt/Front_View/Pockets/WithoutFlap_right_shad.png', 'glow_mask_shirt/Front_View/Pockets/WithoutFlap_right_mask.png', 'glow_mask_shirt/Front_View/Pockets/WithoutFlap_right_hi.png', 'glow_mask_shirt/Side_View/Pockets/WithoutFlap_Left_shad.png', 'glow_mask_shirt/Side_View/Pockets/WithoutFlap_Left_mask.png', 'glow_mask_shirt/Side_View/Pockets/WithoutFlap_Left_hi.png', 'glow_mask_shirt/Side_View/Pockets/WithoutFlap_Right_shad.png', 'glow_mask_shirt/Side_View/Pockets/WithoutFlap_Right_mask.png', 'glow_mask_shirt/Side_View/Pockets/WithoutFlap_Right_hi.png', 'glow_mask_shirt/Folded_View/Pockets/WithoutFlap_left_Folded_shad.png', 'glow_mask_shirt/Folded_View/Pockets/WithoutFlap_left_Folded_mask.png', 'glow_mask_shirt/Folded_View/Pockets/WithoutFlap_left_Folded_hi.png', 'glow_mask_shirt/Folded_View/Pockets/WithoutFlap_right_Folded_shad.png', 'glow_mask_shirt/Folded_View/Pockets/WithoutFlap_right_Folded_mask.png', 'glow_mask_shirt/Folded_View/Pockets/WithoutFlap_right_Folded_hi.png', 1);

-- --------------------------------------------------------

--
-- Table structure for table `shirtseason`
--

CREATE TABLE `shirtseason` (
  `shirtseason_id` int(10) UNSIGNED NOT NULL COMMENT 'Shirt Fit ID',
  `title` text COMMENT 'Title',
  `status` smallint(6) DEFAULT NULL COMMENT 'Status'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='shirtseason';

--
-- Dumping data for table `shirtseason`
--

INSERT INTO `shirtseason` (`shirtseason_id`, `title`, `status`) VALUES
(1, 'Summer', 1),
(2, 'Winter', 1),
(3, 'Year Round', 1);

-- --------------------------------------------------------

--
-- Table structure for table `shirtsleeves`
--

CREATE TABLE `shirtsleeves` (
  `shirtsleeves_id` int(11) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL DEFAULT '',
  `class` varchar(200) NOT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `type` enum('0','1','2') NOT NULL DEFAULT '0',
  `gender` enum('0','1') NOT NULL DEFAULT '0',
  `glow_front_left_image` varchar(255) NOT NULL DEFAULT '',
  `mask_front_left_image` varchar(255) NOT NULL,
  `highlighted_front_left_image` varchar(255) NOT NULL,
  `glow_front_right_image` varchar(255) NOT NULL DEFAULT '',
  `mask_front_right_image` varchar(255) NOT NULL,
  `highlighted_front_right_image` varchar(255) NOT NULL,
  `glow_side_left_image` varchar(255) NOT NULL,
  `mask_side_left_image` varchar(255) NOT NULL,
  `highlighted_side_left_image` varchar(255) NOT NULL,
  `glow_side_right_image` varchar(255) NOT NULL,
  `mask_side_right_image` varchar(255) NOT NULL,
  `highlighted_side_right_image` varchar(255) NOT NULL,
  `glow_back_left_image` varchar(255) NOT NULL,
  `mask_back_left_image` varchar(255) NOT NULL,
  `highlighted_back_left_image` varchar(255) NOT NULL,
  `glow_back_right_image` varchar(255) NOT NULL,
  `mask_back_right_image` varchar(255) NOT NULL,
  `highlighted_back_right_image` varchar(255) NOT NULL,
  `glow_fold_image` varchar(255) NOT NULL,
  `mask_fold_image` varchar(255) NOT NULL,
  `highlighted_fold_image` varchar(255) NOT NULL,
  `status` smallint(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `shirtsleeves`
--

INSERT INTO `shirtsleeves` (`shirtsleeves_id`, `title`, `class`, `price`, `type`, `gender`, `glow_front_left_image`, `mask_front_left_image`, `highlighted_front_left_image`, `glow_front_right_image`, `mask_front_right_image`, `highlighted_front_right_image`, `glow_side_left_image`, `mask_side_left_image`, `highlighted_side_left_image`, `glow_side_right_image`, `mask_side_right_image`, `highlighted_side_right_image`, `glow_back_left_image`, `mask_back_left_image`, `highlighted_back_left_image`, `glow_back_right_image`, `mask_back_right_image`, `highlighted_back_right_image`, `glow_fold_image`, `mask_fold_image`, `highlighted_fold_image`, `status`) VALUES
(1, 'Long', 'icon-Full_Sleeve', '5.00', '1', '0', 'glow_mask_shirt/Front_View/Sleeves/Shirt_Fullsleeve_Front_Left_shad.png', 'glow_mask_shirt/Front_View/Sleeves/Shirt_Fullsleeve_Front_Left_mask.png', 'glow_mask_shirt/Front_View/Sleeves/Shirt_Fullsleeve_Front_Left_hi.png', 'glow_mask_shirt/Front_View/Sleeves/Shirt_Fullsleeve_Front_right_shad.png', 'glow_mask_shirt/Front_View/Sleeves/Shirt_Fullsleeve_Front_right_mask.png', 'glow_mask_shirt/Front_View/Sleeves/Shirt_Fullsleeve_Front_right_hi.png', 'glow_mask_shirt/Side_View/Sleeves/Shirt_Fullsleeve_Side_Left_shad.png', 'glow_mask_shirt/Side_View/Sleeves/Shirt_Fullsleeve_Side_Left_mask.png', 'glow_mask_shirt/Side_View/Sleeves/Shirt_Fullsleeve_Side_Left_hi.png', 'glow_mask_shirt/Side_View/Sleeves/Shirt_Fullsleeve_Side_Right_shad.png', 'glow_mask_shirt/Side_View/Sleeves/Shirt_Fullsleeve_Side_Right_mask.png', 'glow_mask_shirt/Side_View/Sleeves/Shirt_Fullsleeve_Side_Right_hi.png', 'glow_mask_shirt/Back_View/Sleeves/Full_left_shad.png', 'glow_mask_shirt/Back_View/Sleeves/Full_left_mask.png', 'glow_mask_shirt/Back_View/Sleeves/Full_left_hi.png', 'glow_mask_shirt/Back_View/Sleeves/Full_right_shad.png', 'glow_mask_shirt/Back_View/Sleeves/Full_right_mask.png', 'glow_mask_shirt/Back_View/Sleeves/Full_right_hi.png', '', '', '', 1),
(2, 'Short', 'icon-Half_Sleeves', '5.00', '1', '0', 'glow_mask_shirt/Front_View/Sleeves/Shirt_Halfsleeve_Front_Left_shad.png', 'glow_mask_shirt/Front_View/Sleeves/Shirt_Halfsleeve_Front_Left_mask.png', 'glow_mask_shirt/Front_View/Sleeves/Shirt_Halfsleeve_Front_Left_hi.png', 'glow_mask_shirt/Front_View/Sleeves/Shirt_Halfsleeve_Front_right_shad.png', 'glow_mask_shirt/Front_View/Sleeves/Shirt_Halfsleeve_Front_right_mask.png', 'glow_mask_shirt/Front_View/Sleeves/Shirt_Halfsleeve_Front_right_hi.png', 'glow_mask_shirt/Side_View/Sleeves/Shirt_Halfsleeve_Side_Left_shad.png', 'glow_mask_shirt/Side_View/Sleeves/Shirt_Halfsleeve_Side_Left_mask.png', 'glow_mask_shirt/Side_View/Sleeves/Shirt_Halfsleeve_Side_Left_hi.png', 'glow_mask_shirt/Side_View/Sleeves/Shirt_Halfsleeve_Side_Right_shad.png', 'glow_mask_shirt/Side_View/Sleeves/Shirt_Halfsleeve_Side_Right_mask.png', 'glow_mask_shirt/Side_View/Sleeves/Shirt_Halfsleeve_Side_Right_hi.png', 'glow_mask_shirt/Back_View/Sleeves/Half_left_shad.png', 'glow_mask_shirt/Back_View/Sleeves/Half_left_mask.png', 'glow_mask_shirt/Back_View/Sleeves/Half_left_hi.png', 'glow_mask_shirt/Back_View/Sleeves/Half_right_shad.png', 'glow_mask_shirt/Back_View/Sleeves/Half_right_mask.png', 'glow_mask_shirt/Back_View/Sleeves/Half_right_hi.png', 'glow_mask_shirt/Folded_View/Sleeves/Shirt_Halfsleeves_Folded_shad.png', 'glow_mask_shirt/Folded_View/Sleeves/Shirt_Halfsleeves_Folded_mask.png', 'glow_mask_shirt/Folded_View/Sleeves/Shirt_Halfsleeves_Folded_hi.png', 1),
(3, 'Rollup', 'icon-Rollup', '5.00', '1', '0', 'glow_mask_shirt/Front_View/Sleeves/Rollup_Left_shad.png', 'glow_mask_shirt/Front_View/Sleeves/Rollup_Left_mask.png', 'glow_mask_shirt/Front_View/Sleeves/Rollup_Left_hi.png', 'glow_mask_shirt/Front_View/Sleeves/Rollup_Right_shad.png', 'glow_mask_shirt/Front_View/Sleeves/Rollup_Right_mask.png', 'glow_mask_shirt/Front_View/Sleeves/Rollup_Right_hi.png', 'glow_mask_shirt/Side_View/Sleeves/Rollup_Left_shad.png', 'glow_mask_shirt/Side_View/Sleeves/Rollup_Left_mask.png', 'glow_mask_shirt/Side_View/Sleeves/Rollup_Left_hi.png', 'glow_mask_shirt/Side_View/Sleeves/Rollup_Right_shad.png', 'glow_mask_shirt/Side_View/Sleeves/Rollup_Right_mask.png', 'glow_mask_shirt/Side_View/Sleeves/Rollup_Right_hi.png', 'glow_mask_shirt/Back_View/Sleeves/Rollup_left_shad.png', 'glow_mask_shirt/Back_View/Sleeves/Rollup_left_mask.png', 'glow_mask_shirt/Back_View/Sleeves/Rollup_left_hi.png', 'glow_mask_shirt/Back_View/Sleeves/Rollup_right_shad.png', 'glow_mask_shirt/Back_View/Sleeves/Rollup_right_mask.png', 'glow_mask_shirt/Back_View/Sleeves/Rollup_right_hi.png', '', '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `shirtstyle`
--

CREATE TABLE `shirtstyle` (
  `shirtstyle_id` int(11) UNSIGNED NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `class` varchar(200) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `type` enum('0','1','2') DEFAULT NULL,
  `thumb` varchar(200) DEFAULT NULL,
  `view1_glow_left_image` varchar(255) DEFAULT NULL,
  `view1_mask_left_image` varchar(255) DEFAULT NULL,
  `view1_highlighted_left_image` varchar(255) DEFAULT NULL,
  `view1_glow_right_image` varchar(255) DEFAULT NULL,
  `view1_mask_right_image` varchar(255) DEFAULT NULL,
  `view1_highlighted_right_image` varchar(255) DEFAULT NULL,
  `view2_glow_left_image` varchar(255) DEFAULT NULL,
  `view2_mask_left_image` varchar(255) DEFAULT NULL,
  `view2_highlighted_left_image` varchar(255) DEFAULT NULL,
  `view2_glow_right_image` varchar(255) DEFAULT NULL,
  `view2_mask_right_image` varchar(255) DEFAULT NULL,
  `view2_highlighted_right_image` varchar(255) DEFAULT NULL,
  `view3_glow_back_image` varchar(255) DEFAULT NULL,
  `view3_mask_back_image` varchar(255) DEFAULT NULL,
  `view3_highlighted_back_image` varchar(255) DEFAULT NULL,
  `view4_glow_left_image` varchar(255) DEFAULT NULL,
  `view4_mask_left_image` varchar(255) DEFAULT NULL,
  `view4_highlighted_left_image` varchar(255) DEFAULT NULL,
  `view4_glow_right_image` varchar(255) DEFAULT NULL,
  `view4_mask_right_image` varchar(255) DEFAULT NULL,
  `view4_highlighted_right_image` varchar(255) DEFAULT NULL,
  `view5_glow_left_image` varchar(255) DEFAULT NULL,
  `view5_mask_left_image` varchar(255) DEFAULT NULL,
  `view5_highlighted_left_image` varchar(255) DEFAULT NULL,
  `view5_glow_right_image` varchar(255) DEFAULT NULL,
  `view5_mask_right_image` varchar(255) DEFAULT NULL,
  `view5_highlighted_right_image` varchar(255) DEFAULT NULL,
  `view1_casual_glow_left_image` varchar(255) DEFAULT NULL,
  `view1_casual_mask_left_image` varchar(255) DEFAULT NULL,
  `view1_casual_highlighted_left_image` varchar(255) DEFAULT NULL,
  `view1_casual_glow_right_image` varchar(255) DEFAULT NULL,
  `view1_casual_mask_right_image` varchar(255) DEFAULT NULL,
  `view1_casual_highlighted_right_image` varchar(255) DEFAULT NULL,
  `view2_casual_glow_left_image` varchar(255) DEFAULT NULL,
  `view2_casual_mask_left_image` varchar(255) DEFAULT NULL,
  `view2_casual_highlighted_left_image` varchar(255) DEFAULT NULL,
  `view2_casual_glow_right_image` varchar(255) DEFAULT NULL,
  `view2_casual_mask_right_image` varchar(255) DEFAULT NULL,
  `view2_casual_highlighted_right_image` varchar(255) DEFAULT NULL,
  `view4_glow_inner_image` varchar(200) DEFAULT NULL,
  `view4_mask_inner_image` varchar(200) DEFAULT NULL,
  `view4_highlighted_inner_image` varchar(200) DEFAULT NULL,
  `view5_glow_inner_image` varchar(255) DEFAULT NULL,
  `view5_mask_inner_image` varchar(255) DEFAULT NULL,
  `view5_highlighted_inner_image` varchar(255) DEFAULT NULL,
  `status` smallint(6) DEFAULT NULL,
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `shirtstyle`
--

INSERT INTO `shirtstyle` (`shirtstyle_id`, `title`, `class`, `price`, `type`, `thumb`, `view1_glow_left_image`, `view1_mask_left_image`, `view1_highlighted_left_image`, `view1_glow_right_image`, `view1_mask_right_image`, `view1_highlighted_right_image`, `view2_glow_left_image`, `view2_mask_left_image`, `view2_highlighted_left_image`, `view2_glow_right_image`, `view2_mask_right_image`, `view2_highlighted_right_image`, `view3_glow_back_image`, `view3_mask_back_image`, `view3_highlighted_back_image`, `view4_glow_left_image`, `view4_mask_left_image`, `view4_highlighted_left_image`, `view4_glow_right_image`, `view4_mask_right_image`, `view4_highlighted_right_image`, `view5_glow_left_image`, `view5_mask_left_image`, `view5_highlighted_left_image`, `view5_glow_right_image`, `view5_mask_right_image`, `view5_highlighted_right_image`, `view1_casual_glow_left_image`, `view1_casual_mask_left_image`, `view1_casual_highlighted_left_image`, `view1_casual_glow_right_image`, `view1_casual_mask_right_image`, `view1_casual_highlighted_right_image`, `view2_casual_glow_left_image`, `view2_casual_mask_left_image`, `view2_casual_highlighted_left_image`, `view2_casual_glow_right_image`, `view2_casual_mask_right_image`, `view2_casual_highlighted_right_image`, `view4_glow_inner_image`, `view4_mask_inner_image`, `view4_highlighted_inner_image`, `view5_glow_inner_image`, `view5_mask_inner_image`, `view5_highlighted_inner_image`, `status`, `created_time`, `update_time`) VALUES
(1, 'Rounded collar', 'icon-rounded_collar', '40.00', '0', 'glow_mask/style/Style_Mandarin.png', 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Roundedcollar_Front_Left_shad.png', 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Roundedcollar_Front_Left_mask.png', 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Roundedcollar_Front_Left_hi.png', 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Roundedcollar_Front_Right_shad.png', 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Roundedcollar_Front_Right_mask.png', 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Roundedcollar_Front_Right_hi.png', 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Roundedcollar_Side_Left_shad.png', 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Roundedcollar_Side_Left_mask.png', 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Roundedcollar_Side_Left_hi.png', 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Roundedcollar_Side_Right_shad.png', 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Roundedcollar_Side_Right_mask.png', 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Roundedcollar_Side_Right_hi.png', NULL, NULL, NULL, 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Roundedcollar_Folded_Left_shad.png', 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Roundedcollar_Folded_Left_mask.png', 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Roundedcollar_Folded_Left_hi.png', 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Roundedcollar_Folded_Right_shad.png', 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Roundedcollar_Folded_Right_mask.png', 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Roundedcollar_Folded_Right_hi.png', 'glow_mask_shirt/Zoom_View/Collar_Styles/RoundedCollar_Left_shad.png', 'glow_mask_shirt/Zoom_View/Collar_Styles/RoundedCollar_Left_mask.png', 'glow_mask_shirt/Zoom_View/Collar_Styles/RoundedCollar_Left_hi.png', 'glow_mask_shirt/Zoom_View/Collar_Styles/RoundedCollar_Right_shad.png', 'glow_mask_shirt/Zoom_View/Collar_Styles/RoundedCollar_Right_mask.png', 'glow_mask_shirt/Zoom_View/Collar_Styles/RoundedCollar_Right_hi.png', 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Rounded_Left_shad.png', 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Rounded_Left_mask.png', 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Rounded_Left_hi.png', 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Rounded_Right_shad.png', 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Rounded_Right_mask.png', 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Rounded_Right_hi.png', 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Rounded_Left_shad.png', 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Rounded_Left_mask.png', 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Rounded_Left_hi.png', 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Rounded_Right_shad.png', 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Rounded_Right_mask.png', 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Rounded_Right_hi.png', NULL, NULL, NULL, NULL, NULL, NULL, 1, '2017-01-26 17:12:53', '2018-05-21 06:36:01'),
(2, 'Button down', 'icon-button_down', '5.00', '1', 'glow_mask/style/Style_1_button_Single_breasted.png', 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Buttondowncollar_Front_Left_shad.png', 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Buttondowncollar_Front_Left_mask.png', 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Buttondowncollar_Front_Left_hi.png', 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Buttondowncollar_Front_Right_shad.png', 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Buttondowncollar_Front_Right_mask.png', 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Buttondowncollar_Front_Right_hi.png', 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Buttondowncollar_Side_Left_shad.png', 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Buttondowncollar_Side_Left_mask.png', 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Buttondowncollar_Side_Left_hi.png', 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Buttondowncollar_Side_Right_shad.png', 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Buttondowncollar_Side_Right_mask.png', 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Buttondowncollar_Side_Right_hi.png', NULL, NULL, NULL, 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Buttondowncollar_Folded_Left_shad.png', 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Buttondowncollar_Folded_Left_mask.png', 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Buttondowncollar_Folded_Left_hi.png', 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Buttondowncollar_Folded_Right_shad.png', 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Buttondowncollar_Folded_Right_mask.png', 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Buttondowncollar_Folded_Right_hi.png', 'glow_mask_shirt/Zoom_View/Collar_Styles/ButtondownCollar_Left_shad.png', 'glow_mask_shirt/Zoom_View/Collar_Styles/ButtondownCollar_Left_mask.png', 'glow_mask_shirt/Zoom_View/Collar_Styles/ButtondownCollar_Left_hi.png', 'glow_mask_shirt/Zoom_View/Collar_Styles/ButtondownCollar_Right_shad.png', 'glow_mask_shirt/Zoom_View/Collar_Styles/ButtondownCollar_Right_mask.png', 'glow_mask_shirt/Zoom_View/Collar_Styles/ButtondownCollar_Right_hi.png', 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Buttondown_Left_shad.png', 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Buttondown_Left_mask.png', 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Buttondown_Left_hi.png', 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Buttondown_Right_shad.png', 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Buttondown_Right_mask.png', 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Buttondown_Right_hi.png', 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Buttondown_Left_shad.png', 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Buttondown_Left_mask.png', 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Buttondown_Left_hi.png', 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Buttondown_Right_shad.png', 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Buttondown_Right_mask.png', 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Buttondown_Right_hi.png', NULL, NULL, NULL, NULL, NULL, NULL, 1, '2017-01-26 17:11:50', '2018-05-21 06:36:51'),
(3, 'Cutaway collar', 'icon-Cutaway', '10.00', '2', 'glow_mask/style/Style_4_button_Double_breasted.png', 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Cutawaycollar_Front_Left_shad.png', 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Cutawaycollar_Front_Left_mask.png', 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Cutawaycollar_Front_Left_hi.png', 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Cutawaycollar_Front_Right_shad.png', 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Cutawaycollar_Front_Right_mask.png', 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Cutawaycollar_Front_Right_hi.png', 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Cutaway_Side_Left_shad.png', 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Cutaway_Side_Left_mask.png', 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Cutaway_Side_Left_hi.png', 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Cutaway_Side_Right_shad.png', 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Cutaway_Side_Right_mask.png', 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Cutaway_Side_Right_hi.png', NULL, NULL, NULL, 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Cutawaycollar_Folded_Left_shad.png', 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Cutawaycollar_Folded_Left_mask.png', 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Cutawaycollar_Folded_Left_hi.png', 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Cutawaycollar_Folded_Right_shad.png', 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Cutawaycollar_Folded_Right_mask.png', 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Cutawaycollar_Folded_Right_hi.png', 'glow_mask_shirt/Zoom_View/Collar_Styles/CutawayCollar_Left_shad.png', 'glow_mask_shirt/Zoom_View/Collar_Styles/CutawayCollar_Left_mask.png', 'glow_mask_shirt/Zoom_View/Collar_Styles/CutawayCollar_Left_hi.png', 'glow_mask_shirt/Zoom_View/Collar_Styles/CutawayCollar_Right_shad.png', 'glow_mask_shirt/Zoom_View/Collar_Styles/CutawayCollar_Right_mask.png', 'glow_mask_shirt/Zoom_View/Collar_Styles/CutawayCollar_Right_hi.png', 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Cutaway_Left_shad.png', 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Cutaway_Left_mask.png', 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Cutaway_Left_hi.png', 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Cutaway_Right_shad.png', 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Cutaway_Right_mask.png', 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Cutaway_Right_hi.png', 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Cutaway_Left_shad.png', 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Cutaway_Left_mask.png', 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Cutaway_Left_hi.png', 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Cutaway_Right_shad.png', 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Cutaway_Right_mask.png', 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Cutaway_Right_hi.png', NULL, NULL, NULL, NULL, NULL, NULL, 1, '2017-02-01 18:50:06', '2018-05-21 06:37:11'),
(4, 'Kent collar', 'icon-Kent_Collar', '15.00', '0', 'glow_mask/style/Style_Mandarin.png', 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Kentcollar_Front_Left_shad.png', 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Kentcollar_Front_Left_mask.png', 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Kentcollar_Front_Left_hi.png', 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Kentcollar_Front_Right_shad.png', 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Kentcollar_Front_Right_mask.png', 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Kentcollar_Front_Right_hi.png', 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Kent_Side_Left_shad.png', 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Kent_Side_Left_mask.png', 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Kent_Side_Left_hi.png', 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Kent_Side_Right_shad.png', 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Kent_Side_Right_mask.png', 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Kent_Side_Right_hi.png', NULL, NULL, NULL, 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Kentcollar_Folded_Left_shad.png', 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Kentcollar_Folded_Left_mask.png', 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Kentcollar_Folded_Left_hi.png', 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Kentcollar_Folded_Right_shad.png', 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Kentcollar_Folded_Right_mask.png', 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Kentcollar_Folded_Right_hi.png', 'glow_mask_shirt/Zoom_View/Collar_Styles/KentCollar_Left_shad.png', 'glow_mask_shirt/Zoom_View/Collar_Styles/KentCollar_Left_mask.png', 'glow_mask_shirt/Zoom_View/Collar_Styles/KentCollar_Left_hi.png', 'glow_mask_shirt/Zoom_View/Collar_Styles/KentCollar_Right_shad.png', 'glow_mask_shirt/Zoom_View/Collar_Styles/KentCollar_Right_mask.png', 'glow_mask_shirt/Zoom_View/Collar_Styles/KentCollar_Right_hi.png', 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Kent_Left_shad.png', 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Kent_Left_mask.png', 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Kent_Left_hi.png', 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Kent_Right_shad.png', 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Kent_Right_mask.png', 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Kent_Right_hi.png', 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Kent_Left_shad.png', 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Kent_Left_mask.png', 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Kent_Left_hi.png', 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Kent_Right_shad.png', 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Kent_Right_mask.png', 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Kent_Right_hi.png', NULL, NULL, NULL, NULL, NULL, NULL, 1, '2017-01-26 17:12:53', '2018-05-21 06:37:18'),
(5, 'Long collar', 'icon-Long_Collar', '5.00', '0', 'glow_mask/style/Style_Mandarin.png', 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Longcollar_Front_Left_shad.png', 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Longcollar_Front_Left_mask.png', 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Longcollar_Front_Left_hi.png', 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Longcollar_Front_Right_shad.png', 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Longcollar_Front_Right_mask.png', 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Longcollar_Front_Right_hi.png', 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Longcollar_Side_Left_shad.png', 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Longcollar_Side_Left_mask.png', 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Longcollar_Side_Left_hi.png', 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Longcollar_Side_Right_shad.png', 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Longcollar_Side_Right_mask.png', 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Longcollar_Side_Right_hi.png', NULL, NULL, NULL, 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Longcollar_Folded_Left_shad.png', 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Longcollar_Folded_Left_mask.png', 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Longcollar_Folded_Left_hi.png', 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Longcollar_Folded_Right_shad.png', 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Longcollar_Folded_Right_mask.png', 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Longcollar_Folded_Right_hi.png', 'glow_mask_shirt/Zoom_View/Collar_Styles/LongCollar_Left_shad.png', 'glow_mask_shirt/Zoom_View/Collar_Styles/LongCollar_Left_mask.png', 'glow_mask_shirt/Zoom_View/Collar_Styles/LongCollar_Left_hi.png', 'glow_mask_shirt/Zoom_View/Collar_Styles/LongCollar_Right_shad.png', 'glow_mask_shirt/Zoom_View/Collar_Styles/LongCollar_Right_mask.png', 'glow_mask_shirt/Zoom_View/Collar_Styles/LongCollar_Right_hi.png', 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Longcollar_Left_shad.png', 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Longcollar_Left_mask.png', 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Longcollar_Left_hi.png', 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Longcollar_Right_shad.png', 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Longcollar_Right_mask.png', 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Longcollar_Right_hi.png', 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Long_Left_shad.png', 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Long_Left_mask.png', 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Long_Left_hi.png', 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Long_Right_shad.png', 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Long_Right_mask.png', 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Long_Right_hi.png', NULL, NULL, NULL, NULL, NULL, NULL, 1, '2017-01-26 17:12:53', '2018-05-21 06:37:28'),
(6, 'Pinned collar', 'icon-Pinned_Collar', '15.00', '0', 'glow_mask/style/Style_Mandarin.png', 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Pinnedcollar_Front_Left_shad.png', 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Pinnedcollar_Front_Left_mask.png', 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Pinnedcollar_Front_Left_hi.png', 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Pinnedcollar_Front_Right_shad.png', 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Pinnedcollar_Front_Right_mask.png', 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Pinnedcollar_Front_Right_hi.png', 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Pinnedcollar_Side_Left_shad.png', 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Pinnedcollar_Side_Left_mask.png', 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Pinnedcollar_Side_Left_hi.png', 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Pinnedcollar_Side_Right_shad.png', 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Pinnedcollar_Side_Right_mask.png', 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Pinnedcollar_Side_Right_hi.png', NULL, NULL, NULL, 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Pinnedcollar_Folded_Left_shad.png', 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Pinnedcollar_Folded_Left_mask.png', 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Pinnedcollar_Folded_Left_hi.png', 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Pinnedcollar_Folded_Right_shad.png', 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Pinnedcollar_Folded_Right_mask.png', 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Pinnedcollar_Folded_Right_hi.png', 'glow_mask_shirt/Zoom_View/Collar_Styles/Pinned_Left_shad.png', 'glow_mask_shirt/Zoom_View/Collar_Styles/Pinned_Left_mask.png', 'glow_mask_shirt/Zoom_View/Collar_Styles/Pinned_Left_hi.png', 'glow_mask_shirt/Zoom_View/Collar_Styles/Pinned_Right_shad.png', 'glow_mask_shirt/Zoom_View/Collar_Styles/Pinned_Right_mask.png', 'glow_mask_shirt/Zoom_View/Collar_Styles/Pinned_Right_hi.png', 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Pinned_Left_shad.png', 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Pinned_Left_mask.png', 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Pinned_Left_hi.png', 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Pinned_Right_shad.png', 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Pinned_Right_mask.png', 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Pinned_Right_hi.png', 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Pinned_Left_shad.png', 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Pinned_Left_mask.png', 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Pinned_Left_hi.png', 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Pinned_Right_shad.png', 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Pinned_Right_mask.png', 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Pinned_Right_hi.png', NULL, NULL, NULL, NULL, NULL, NULL, 1, '2017-01-26 17:12:53', '2018-05-21 06:37:35'),
(7, 'Short Classic ', 'icon-Short_Classic', '30.00', '0', 'glow_mask/style/Style_Mandarin.png', 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_ShortClassic_Front_Left_shad.png', 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_ShortClassic_Front_Left_mask.png', 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_ShortClassic_Front_Left_hi.png', 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_ShortClassic_Front_Right_shad.png', 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_ShortClassic_Front_Right_mask.png', 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_ShortClassic_Front_Right_hi.png', 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Shortclassiccollar_Side_Left_shad.png', 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Shortclassiccollar_Side_Left_mask.png', 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Shortclassiccollar_Side_Left_hi.png', 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Shortclassiccollar_Side_Right_shad.png', 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Shortclassiccollar_Side_Right_mask.png', 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Shortclassiccollar_Side_Right_hi.png', NULL, NULL, NULL, 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Shortclassiccollar_Folded_Left_shad.png', 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Shortclassiccollar_Folded_Left_mask.png', 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Shortclassiccollar_Folded_Left_hi.png', 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Shortclassiccollar_Folded_Right_shad.png', 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Shortclassiccollar_Folded_Right_mask.png', 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Shortclassiccollar_Folded_Right_hi.png', 'glow_mask_shirt/Zoom_View/Collar_Styles/ClassicShortCollar_Left_shad.png', 'glow_mask_shirt/Zoom_View/Collar_Styles/ClassicShortCollar_Left_mask.png', 'glow_mask_shirt/Zoom_View/Collar_Styles/ClassicShortCollar_Left_hi.png', 'glow_mask_shirt/Zoom_View/Collar_Styles/ClassicShortCollar_Right_shad.png', 'glow_mask_shirt/Zoom_View/Collar_Styles/ClassicShortCollar_Right_mask.png', 'glow_mask_shirt/Zoom_View/Collar_Styles/ClassicShortCollar_Right_hi.png', 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_ShortClassic_Left_shad.png', 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_ShortClassic_Left_mask.png', 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_ShortClassic_Left_hi.png', 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_ShortClassic_Right_shad.png', 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_ShortClassic_Right_mask.png', 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_ShortClassic_Right_hi.png', 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Short_Left_shad.png', 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Short_Left_mask.png', 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Short_Left_hi.png', 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Short_Right_shad.png', 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Short_Right_mask.png', 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Short_Right_hi.png', NULL, NULL, NULL, NULL, NULL, NULL, 1, '2017-01-26 17:12:53', '2018-05-21 06:37:51'),
(8, 'Stand-up collar', 'icon-Standup_Collar', '10.00', '0', 'glow_mask/style/Style_Mandarin.png', 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Standupcollar_Front_Left_shad.png', 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Standupcollar_Front_Left_mask.png', 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Standupcollar_Front_Left_hi.png', 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Standupcollar_Front_Right_shad.png', 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Standupcollar_Front_Right_mask.png', 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Standupcollar_Front_Right_hi.png', 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Standupcollar_Side_Left_shad.png', 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Standupcollar_Side_Left_mask.png', 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Standupcollar_Side_Left_hi.png', 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Standupcollar_Side_Right_shad.png', 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Standupcollar_Side_Right_mask.png', 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Standupcollar_Side_Right_hi.png', NULL, NULL, NULL, 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Standupcollar_Folded_Left_shad.png', 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Standupcollar_Folded_Left_mask.png', 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Standupcollar_Folded_Left_hi.png', 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Standupcollar_Folded_Right_shad.png', 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Standupcollar_Folded_Right_mask.png', 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Standupcollar_Folded_Right_hi.png', 'glow_mask_shirt/Zoom_View/Collar_Styles/CollarBase_Left_Shad.png', 'glow_mask_shirt/Zoom_View/Collar_Styles/CollarBase_Left_Mask.png', 'glow_mask_shirt/Zoom_View/Collar_Styles/CollarBase_Left_Hi.png', 'glow_mask_shirt/Zoom_View/Collar_Styles/CollarBase_Right_Shad.png', 'glow_mask_shirt/Zoom_View/Collar_Styles/CollarBase_Right_Mask.png', 'glow_mask_shirt/Zoom_View/Collar_Styles/CollarBase_Right_Hi.png', 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Standup_Left_shad.png', 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Standup_Left_mask.png', 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Standup_Left_hi.png', 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Standup_Right_shad.png', 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Standup_Right_mask.png', 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Standup_Right_hi.png', 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Standup_Left_shad.png', 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Standup_Left_mask.png', 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Standup_Left_hi.png', 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Standup_Right_shad.png', 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Standup_Right_mask.png', 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Standup_Right_hi.png', NULL, NULL, NULL, NULL, NULL, NULL, 1, '2017-01-26 17:12:53', '2018-05-21 06:37:59'),
(9, 'Wide Cutaway', 'icon-Wide_Cutaway', '20.00', '0', 'glow_mask/style/Style_Mandarin.png', 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Widecutawaycollar_Front_Left_shad.png', 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Widecutawaycollar_Front_Left_mask.png', 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Widecutawaycollar_Front_Left_hi.png', 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Widecutawaycollar_Front_Right_shad.png', 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Widecutawaycollar_Front_Right_mask.png', 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Widecutawaycollar_Front_Right_hi.png', 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Widecutaway_Side_Left_shad.png', 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Widecutaway_Side_Left_mask.png', 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Widecutaway_Side_Left_hi.png', 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Widecutaway_Side_Right_shad.png', 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Widecutaway_Side_Right_mask.png', 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Widecutaway_Side_Right_hi.png', NULL, NULL, NULL, 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Widecutawaycollar_Folded_Left_shad.png', 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Widecutawaycollar_Folded_Left_mask.png', 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Widecutawaycollar_Folded_Left_hi.png', 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Widecutawaycollar_Folded_Right_shad.png', 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Widecutawaycollar_Folded_Right_mask.png', 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Widecutawaycollar_Folded_Right_hi.png', 'glow_mask_shirt/Zoom_View/Collar_Styles/WideCutawayCollar_Left_shad.png', 'glow_mask_shirt/Zoom_View/Collar_Styles/WideCutawayCollar_Left_mask.png', 'glow_mask_shirt/Zoom_View/Collar_Styles/WideCutawayCollar_Left_hi.png', 'glow_mask_shirt/Zoom_View/Collar_Styles/WideCutawayCollar_Right_shad.png', 'glow_mask_shirt/Zoom_View/Collar_Styles/WideCutawayCollar_Right_mask.png', 'glow_mask_shirt/Zoom_View/Collar_Styles/WideCutawayCollar_Right_hi.png', 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Widecutaway_Left_shad.png', 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Widecutaway_Left_mask.png', 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Widecutaway_Left_hi.png', 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Widecutaway_Right_shad.png', 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Widecutaway_Right_mask.png', 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Widecutaway_Right_hi.png', 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Widecutaway_Left_shad.png', 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Widecutaway_Left_mask.png', 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Widecutaway_Left_hi.png', 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Widecutaway_Right_shad.png', 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Widecutaway_Right_mask.png', 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Widecutaway_Right_hi.png', NULL, NULL, NULL, NULL, NULL, NULL, 1, '2017-01-26 17:12:53', '2018-05-21 06:38:11'),
(10, 'Wing collar', 'icon-Wing_Collar', '15.00', '0', 'glow_mask/style/Style_Mandarin.png', 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Wingcollar_Front_Left_shad.png', 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Wingcollar_Front_Left_mask.png', 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Wingcollar_Front_Left_hi.png', 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Wingcollar_Front_Right_shad.png', 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Wingcollar_Front_Right_mask.png', 'glow_mask_shirt/Front_View/Collar_Styles/Shirt_Wingcollar_Front_Right_hi.png', 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Wingedcollar_Side_Left_shad.png', 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Wingedcollar_Side_Left_mask.png', 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Wingedcollar_Side_Left_hi.png', 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Wingedcollar_Side_Right_shad.png', 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Wingedcollar_Side_Right_mask.png', 'glow_mask_shirt/Side_View/Collar_Styles/Shirt_Wingedcollar_Side_Right_hi.png', NULL, NULL, NULL, 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Wingcollar_Folded_Left_shad.png', 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Wingcollar_Folded_Left_mask.png', 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Wingcollar_Folded_Left_hi.png', 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Wingcollar_Folded_Right_shad.png', 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Wingcollar_Folded_Right_mask.png', 'glow_mask_shirt/Folded_View/Collar_Styles/Shirt_Wingcollar_Folded_Right_hi.png', 'glow_mask_shirt/Zoom_View/Collar_Styles/WingCollar_Left_shad.png', 'glow_mask_shirt/Zoom_View/Collar_Styles/WingCollar_Left_mask.png', 'glow_mask_shirt/Zoom_View/Collar_Styles/WingCollar_Left_hi.png', 'glow_mask_shirt/Zoom_View/Collar_Styles/WingCollar_Right_shad.png', 'glow_mask_shirt/Zoom_View/Collar_Styles/WingCollar_Right_mask.png', 'glow_mask_shirt/Zoom_View/Collar_Styles/WingCollar_Right_hi.png', 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Winged_Left_shad.png', 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Winged_Left_mask.png', 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Winged_Left_hi.png', 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Winged_Right_shad.png', 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Winged_Right_mask.png', 'glow_mask_shirt/Front_View/Collar_Casual/Casual-Bust_Front_Winged_Right_hi.png', 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Wing_Left_shad.png', 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Wing_Left_mask.png', 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Wing_Left_hi.png', 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Wing_Right_shad.png', 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Wing_Right_mask.png', 'glow_mask_shirt/Side_View/Collar_Casual/Casual-Bust_Side_Wing_Right_hi.png', NULL, NULL, NULL, NULL, NULL, NULL, 1, '2017-01-26 17:12:53', '2018-05-21 06:38:18');

-- --------------------------------------------------------

--
-- Table structure for table `shirts_shirtfabric`
--

CREATE TABLE `shirts_shirtfabric` (
  `shirtfabric_id` int(10) UNSIGNED NOT NULL COMMENT 'ShirtFabric ID',
  `name` varchar(255) DEFAULT NULL COMMENT 'Name',
  `price` float DEFAULT NULL COMMENT 'Price',
  `thumbnail_image` varchar(255) DEFAULT NULL COMMENT 'Thumbnail Image',
  `fabric_thumbnail` varchar(255) DEFAULT NULL COMMENT 'Fabric Thumbnail',
  `fabric_large_image` varchar(255) DEFAULT NULL COMMENT 'Fabric Large Image',
  `status` int(11) DEFAULT NULL COMMENT 'Status'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='shirts_shirtfabric';

--
-- Dumping data for table `shirts_shirtfabric`
--

INSERT INTO `shirts_shirtfabric` (`shirtfabric_id`, `name`, `price`, `thumbnail_image`, `fabric_thumbnail`, `fabric_large_image`, `status`) VALUES
(1, 'NIsha', 200, '/shirts_shirtfabric/beautiful-blur-bright-326055.jpg', '/shirts_shirtfabric/beautiful-blur-bright-326055_1.jpg', '/shirts_shirtfabric/beautiful-blur-bright-326055_2.jpg', 1),
(2, 'NIsha', 200, '/shirts_shirtfabric/beautiful-blur-bright-326055.jpg', '/shirts_shirtfabric/beautiful-blur-bright-326055_1.jpg', '/shirts_shirtfabric/beautiful-blur-bright-326055_2.jpg', 1),
(3, 'NIsha', 200, '/shirts_shirtfabric/beautiful-blur-bright-326055.jpg', '/shirts_shirtfabric/beautiful-blur-bright-326055_1.jpg', '/shirts_shirtfabric/beautiful-blur-bright-326055_2.jpg', 1),
(4, 'adasd', 123, '/shirts_shirtfabric/tv_3.jpg', '/shirts_shirtfabric/tv_4.jpg', '/shirts_shirtfabric/tv_5.jpg', 1),
(5, 'adasd', 123, '/shirts_shirtfabric/tv_3.jpg', '/shirts_shirtfabric/tv_4.jpg', '/shirts_shirtfabric/tv_5.jpg', 1),
(6, 'sdfsdfsdf', 54, '/shirts_shirtfabric/redmi.jpeg', NULL, NULL, 1),
(7, 'Nisha', 234, '/shirts_shirtfabric/redmi_1.jpeg', NULL, NULL, 1),
(8, 'dfsfsdf', 56, '/shirts_shirtfabric/tv_6.jpg', NULL, NULL, 1),
(9, 'asxdasc', 123, '/shirts_shirtfabric/download.jpeg', NULL, NULL, 1),
(10, 'zxczxc', 123, '/shirts_shirtfabric/download_1.jpeg', NULL, NULL, 1),
(11, 'sdfsdfsdf', 564, '/shirts_shirtfabric/download_2.jpeg', NULL, NULL, 1),
(12, 'hgjg', 564, '/shirts_shirtfabric/download_3.jpeg', '/shirts_shirtfabric/tv_7.jpg', '/shirts_shirtfabric/redmi_2.jpeg', 1),
(13, 'czxczxc', 123, '/shirts_shirtfabric/liverpool100.jpg', '/shirts_shirtfabric/liverpool100_1.jpg', '/shirts_shirtfabric/13.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `shirt_accent_chestpleats`
--

CREATE TABLE `shirt_accent_chestpleats` (
  `chestpleats_id` int(10) NOT NULL,
  `title` varchar(40) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `class` varchar(100) NOT NULL,
  `view1_image` varchar(255) DEFAULT NULL,
  `view2_image` varchar(255) DEFAULT NULL,
  `view4_image` varchar(255) DEFAULT NULL,
  `status` smallint(6) NOT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `shirt_accent_chestpleats`
--

INSERT INTO `shirt_accent_chestpleats` (`chestpleats_id`, `title`, `price`, `class`, `view1_image`, `view2_image`, `view4_image`, `status`, `created_date`, `modified_date`) VALUES
(1, 'No', '0.00', 'icon-No', NULL, NULL, NULL, 1, '2018-01-22 01:39:16', '2018-05-21 06:52:09'),
(2, 'Yes', '10.00', 'icon-Yes', 'shirt_images/chestpleats/Shirt_Tuxedo_Front_Glow_1.png', 'shirt_images/chestpleats/Shirt_Tuxedo_Side_Glow_2.png', 'shirt_images/chestpleats/Shirt_Tuxedo_Folded_Glow_4.png', 1, '2018-01-22 01:40:28', '2018-05-21 06:51:38');

-- --------------------------------------------------------

--
-- Table structure for table `shirt_accent_collarstyle`
--

CREATE TABLE `shirt_accent_collarstyle` (
  `collarstyle_id` int(10) NOT NULL,
  `title` varchar(40) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `class` varchar(100) NOT NULL,
  `status` smallint(6) NOT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `shirt_accent_collarstyle`
--

INSERT INTO `shirt_accent_collarstyle` (`collarstyle_id`, `title`, `price`, `class`, `status`, `created_date`, `modified_date`) VALUES
(1, 'By Default', '0.00', 'icon-By_Default', 1, '2017-12-30 07:03:27', '2018-06-27 10:22:51'),
(2, 'All', '14.00', 'icon-All', 1, '2017-12-30 07:04:46', '2018-06-27 10:23:01'),
(3, 'Outer Fabric', '10.00', 'icon-Oruter', 1, '2018-01-22 00:47:27', '2018-05-21 06:46:19'),
(4, 'Inner Fabric', '10.00', 'icon-Inner', 1, '2018-01-22 00:47:42', '2018-05-21 06:46:11');

-- --------------------------------------------------------

--
-- Table structure for table `shirt_accent_cuffs`
--

CREATE TABLE `shirt_accent_cuffs` (
  `cuff_id` int(10) NOT NULL,
  `title` varchar(40) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `class` varchar(100) NOT NULL,
  `status` smallint(6) NOT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `shirt_accent_cuffs`
--

INSERT INTO `shirt_accent_cuffs` (`cuff_id`, `title`, `price`, `class`, `status`, `created_date`, `modified_date`) VALUES
(1, 'By Default', '0.00', 'icon-By_Default2', 1, '2018-01-20 06:03:13', '2018-06-27 10:25:42'),
(2, 'All', '10.00', 'icon-All2', 1, '2018-01-22 01:36:58', '2018-06-27 10:25:45'),
(3, 'Outer Fabric', '10.00', 'icon-Outer', 1, '2018-01-22 01:37:11', '2018-05-21 06:41:55'),
(4, 'Inner Fabric', '10.00', 'icon-Inner2', 1, '2018-01-22 01:37:27', '2018-05-21 06:42:04');

-- --------------------------------------------------------

--
-- Table structure for table `shirt_accent_elbowpatches`
--

CREATE TABLE `shirt_accent_elbowpatches` (
  `elbowpatches_id` int(10) NOT NULL,
  `title` varchar(40) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `class` varchar(100) DEFAULT NULL,
  `gender` enum('0','1') NOT NULL DEFAULT '0',
  `glow_back_image` varchar(255) DEFAULT NULL,
  `mask_back_image` varchar(255) DEFAULT NULL,
  `highlighted_back_image` varchar(255) NOT NULL,
  `status` smallint(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `shirt_accent_elbowpatches`
--

INSERT INTO `shirt_accent_elbowpatches` (`elbowpatches_id`, `title`, `price`, `class`, `gender`, `glow_back_image`, `mask_back_image`, `highlighted_back_image`, `status`) VALUES
(1, 'Without elbow patch', '10.00', 'icon-Without_Elbow_Patch', '1', '', '', '', 1),
(2, 'With elbow patch', '10.00', 'icon-With_Elbow_Patch', '1', 'glow_mask_shirt/Back_View/elbowpatches/back_elbowpatch_shad.png', 'glow_mask_shirt/Back_View/elbowpatches/back_elbowpatch_mask.png', 'glow_mask_shirt/Back_View/elbowpatches/back_elbowpatch_hi.png', 1);

-- --------------------------------------------------------

--
-- Table structure for table `shirt_accent_placket`
--

CREATE TABLE `shirt_accent_placket` (
  `placket_id` int(10) NOT NULL,
  `title` varchar(40) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `class` varchar(100) NOT NULL,
  `status` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `shirt_accent_placket`
--

INSERT INTO `shirt_accent_placket` (`placket_id`, `title`, `price`, `class`, `status`) VALUES
(1, 'By Default', '0.00', 'icon-Default', 1),
(2, 'All', '10.00', 'icon-All3', 1),
(3, 'Outer Fabric', '10.00', 'icon-outer', 1),
(4, 'Inner Fabric', '10.00', 'icon-inner', 1);

-- --------------------------------------------------------

--
-- Table structure for table `shirt_accent_threadcolor`
--

CREATE TABLE `shirt_accent_threadcolor` (
  `threadcolor_id` int(10) UNSIGNED NOT NULL COMMENT 'Shirt Fit ID',
  `title` varchar(100) DEFAULT NULL COMMENT 'Title',
  `class` varchar(100) DEFAULT NULL COMMENT 'Class',
  `price` float DEFAULT NULL COMMENT 'Price',
  `image` varchar(100) DEFAULT NULL COMMENT 'image',
  `status` smallint(6) DEFAULT NULL COMMENT 'Status'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='shirt_accent_threadcolor';

--
-- Dumping data for table `shirt_accent_threadcolor`
--

INSERT INTO `shirt_accent_threadcolor` (`threadcolor_id`, `title`, `class`, `price`, `image`, `status`) VALUES
(1, 'Green', 'icon-icon-43', NULL, '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `shirt_accent_threads`
--

CREATE TABLE `shirt_accent_threads` (
  `threads_id` int(10) UNSIGNED NOT NULL COMMENT 'Shirt Fit ID',
  `title` varchar(100) DEFAULT NULL COMMENT 'Title',
  `price` float DEFAULT NULL COMMENT 'Price',
  `class` varchar(100) DEFAULT NULL COMMENT 'Class',
  `mask_front_image` varchar(100) DEFAULT NULL COMMENT 'mask_front_image',
  `mask_side_image` varchar(100) DEFAULT NULL COMMENT 'mask_side_image',
  `mask_back_image` varchar(100) DEFAULT NULL COMMENT 'mask_back_image',
  `mask_fold_image` varchar(100) DEFAULT NULL COMMENT 'mask_fold_image',
  `mask_zoom_view` varchar(100) DEFAULT NULL COMMENT 'mask_zoom_view',
  `mask_front_buttonhole_image` varchar(100) DEFAULT NULL COMMENT 'mask_front_buttonhole_image',
  `mask_side_buttonhole_image` varchar(100) DEFAULT NULL COMMENT 'mask_side_buttonhole_image',
  `mask_back_buttonhole_image` varchar(100) DEFAULT NULL COMMENT 'mask_back_buttonhole_image',
  `mask_fold_buttonhole_image` varchar(100) DEFAULT NULL COMMENT 'mask_fold_buttonhole_image',
  `mask_zoomview_buttonhole_image` varchar(100) DEFAULT NULL COMMENT 'mask_zoomview_buttonhole_image',
  `status` smallint(6) DEFAULT NULL COMMENT 'Status',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'created_date',
  `modified_date` varchar(100) NOT NULL DEFAULT 'TIMESTAMP_INIT' COMMENT 'addColum'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='shirt_accent_threads';

--
-- Dumping data for table `shirt_accent_threads`
--

INSERT INTO `shirt_accent_threads` (`threads_id`, `title`, `price`, `class`, `mask_front_image`, `mask_side_image`, `mask_back_image`, `mask_fold_image`, `mask_zoom_view`, `mask_front_buttonhole_image`, `mask_side_buttonhole_image`, `mask_back_buttonhole_image`, `mask_fold_buttonhole_image`, `mask_zoomview_buttonhole_image`, `status`, `created_date`, `modified_date`) VALUES
(1, 'By Default', 0, 'icon-By_Default', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2018-01-20 07:03:40', '2018-05-21 13:49:20'),
(2, 'All', 5, 'icon-All', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2018-01-23 23:09:26', '2018-05-21 13:49:31'),
(3, 'Only Cuff', 5, 'icon-Cuff_only', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, 1, '2018-01-23 23:09:26', '2018-05-21 13:49:11'),
(4, 'Only Placket', 5, 'icon-Only_placket', '', '', '', '', '', '', '', '', '', '0', 1, '2018-01-23 23:24:07', '2018-05-21 13:49:33');

-- --------------------------------------------------------

--
-- Table structure for table `shirt_button_fabric`
--

CREATE TABLE `shirt_button_fabric` (
  `button_fabric_id` int(10) UNSIGNED NOT NULL COMMENT 'Shirt Fit ID',
  `title` varchar(100) DEFAULT NULL COMMENT 'Title',
  `class` varchar(100) DEFAULT NULL COMMENT 'Class',
  `price` float DEFAULT NULL COMMENT 'Price',
  `btn_count` varchar(100) DEFAULT NULL COMMENT 'btn_count',
  `fabric_thumb` varchar(100) DEFAULT NULL COMMENT 'fabric_thumb',
  `fabric_real_image` varchar(100) DEFAULT NULL COMMENT 'fabric_real_image'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='shirt_button_fabric';

--
-- Dumping data for table `shirt_button_fabric`
--

INSERT INTO `shirt_button_fabric` (`button_fabric_id`, `title`, `class`, `price`, `btn_count`, `fabric_thumb`, `fabric_real_image`) VALUES
(1, 'Thread', 'icos-thread', 10, '1', 'shirtbuttonfabric_images/thumbs/threadfabric_1_main.png', 'shirtbuttonfabric_images/threadfabric_1_main.png'),
(2, 'Brown', NULL, NULL, NULL, 'shirtbuttonfabric_images/thumbs/threadfabric_4_main.png', 'shirtbuttonfabric_images/threadfabric_4_main.png'),
(3, 'Oranage', NULL, NULL, NULL, 'shirtbuttonfabric_images/thumbs/solid_01.png', 'shirtbuttonfabric_images/solid_01.png');

-- --------------------------------------------------------

--
-- Table structure for table `shirt_fabric`
--

CREATE TABLE `shirt_fabric` (
  `fabric_id` int(10) UNSIGNED NOT NULL COMMENT 'Shirt Fit ID',
  `fabric_name` varchar(100) DEFAULT NULL COMMENT 'fabric_name',
  `fabric_thumb` varchar(255) DEFAULT NULL COMMENT 'fabric_thumb',
  `fabric_real_image` float DEFAULT NULL COMMENT 'sample',
  `price` decimal(10,0) DEFAULT NULL COMMENT 'price',
  `status` smallint(6) DEFAULT NULL COMMENT 'status',
  `shirt_material_id` int(11) DEFAULT NULL COMMENT 'Column',
  `shirt_pattern_id` int(11) DEFAULT NULL COMMENT 'shirt_season_id',
  `shirt_season_id` int(11) DEFAULT NULL COMMENT 'shirt_color_id',
  `shirt_color_id` int(11) DEFAULT NULL COMMENT 'shirt_category_id',
  `shirt_category_id` varchar(100) DEFAULT NULL COMMENT 'display_fabric_thumb',
  `display_fabric_thumb` varchar(100) DEFAULT NULL COMMENT 'display_fabric_thumb'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='shirt_fabric';

--
-- Dumping data for table `shirt_fabric`
--

INSERT INTO `shirt_fabric` (`fabric_id`, `fabric_name`, `fabric_thumb`, `fabric_real_image`, `price`, `status`, `shirt_material_id`, `shirt_pattern_id`, `shirt_season_id`, `shirt_color_id`, `shirt_category_id`, `display_fabric_thumb`) VALUES
(51, 'Montana', 'fab_shirt_images/thumbs/5a66d37255d8a_12.png', 0, '10', 1, 3, 2, 2, 2, '7', 'fab_shirt_images/display_thumb/5a66d37243145_12_Montana.png'),
(52, 'Conrad', 'fab_shirt_images/thumbs/5a66d76f74072_03.png', 0, '40', 1, 1, 2, 2, 2, '5', 'fab_shirt_images/display_thumb/5a66d76f73b25_03_Conrad.png'),
(53, 'Kent', 'fab_shirt_images/thumbs/5a66f669307aa_02.png', 0, '200', 1, 4, 3, 1, 3, '1', 'fab_shirt_images/display_thumb/5a66f6692a517_02_Kent.png'),
(54, 'Norfolk', 'fab_shirt_images/thumbs/5a672bce25a0f_14.png', 0, '78', 1, 3, 1, 2, 2, '1', 'fab_shirt_images/display_thumb/5a672bce252cc_14_Norfolk.png'),
(56, 'Nashville', 'fab_shirt_images/thumbs/5a6732055e752_08.png', 0, '90', 1, 2, 4, 1, 5, '1', 'fab_shirt_images/display_thumb/5a6732055e0cf_08_Nashville.png'),
(60, 'Kent', 'fab_shirt_images/thumbs/5a699616e444a_02.png', 0, '100', 1, 1, 1, 1, 3, '1', 'fab_shirt_images/display_thumb/5a699616e3d53_02_Kent.png'),
(61, 'Test1', 'fab_shirt_images/thumbs/5ae9c800ac670_fabric_01_100.png', 0, '30', 1, 5, 1, 1, 5, '1', 'fab_shirt_images/display_thumb/5ae9c800abcdc_fabric_01_360.png'),
(64, 'Test2', 'fab_shirt_images/thumbs/5ae9cba6be236_fabric_02_100.png', 0, '40', 1, 2, 1, 3, 6, '7', 'fab_shirt_images/display_thumb/5ae9cba6bda96_fabric_02_360.png');

-- --------------------------------------------------------

--
-- Table structure for table `shirt_shirtfabric`
--

CREATE TABLE `shirt_shirtfabric` (
  `fabric_id` int(11) UNSIGNED NOT NULL,
  `display_fabric_thumb` varchar(255) DEFAULT '',
  `fabric_thumb` varchar(255) DEFAULT '',
  `fabric_large_image` varchar(255) DEFAULT '',
  `btn_color_id` smallint(6) DEFAULT NULL,
  `shoes_color_id` smallint(6) DEFAULT NULL,
  `fabric_material_id` smallint(6) DEFAULT NULL,
  `fabric_pattern_id` smallint(6) DEFAULT NULL,
  `fabric_season_id` smallint(6) DEFAULT NULL,
  `fabric_color_id` smallint(6) DEFAULT NULL,
  `fabric_category_id` smallint(6) DEFAULT NULL,
  `title` varchar(255) DEFAULT '',
  `price` int(11) DEFAULT NULL,
  `type` smallint(6) DEFAULT '0',
  `status` smallint(6) DEFAULT '0',
  `created_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `shirt_shirtfabric`
--

INSERT INTO `shirt_shirtfabric` (`fabric_id`, `display_fabric_thumb`, `fabric_thumb`, `fabric_large_image`, `btn_color_id`, `shoes_color_id`, `fabric_material_id`, `fabric_pattern_id`, `fabric_season_id`, `fabric_color_id`, `fabric_category_id`, `title`, `price`, `type`, `status`, `created_time`, `update_time`) VALUES
(1, 'fab_shirt_images/display_thumb/fabric_04_360.png', 'fab_shirt_images/thumbs/fabric_04_100.png', 'fab_shirt_images/Fabric_04_1000.png', NULL, NULL, 1, 1, 1, 1, 1, 'test', 15, 0, 1, '2018-07-02 09:03:49', '2018-07-02 09:03:49'),
(4, 'fab_shirt_images/display_thumb/fabric_03_360.png', 'fab_shirt_images/thumbs/fabric_03_100.png', 'fab_shirt_images/Fabric_03_1000.png', NULL, NULL, 3, 2, 3, 1, 7, 'Test3', 60, 0, 1, '2018-07-02 09:15:39', '2018-07-02 09:15:39'),
(7, 'fab_shirt_images/display_thumb/fabric_08_360.png', 'fab_shirt_images/thumbs/fabric_08_100.png', 'fab_shirt_images/Fabric_08_1000.png', NULL, NULL, 3, 5, 1, 4, 2, 'fabric 1', 12, 0, 1, '2018-07-02 09:29:34', '2018-07-02 09:29:34');

-- --------------------------------------------------------

--
-- Table structure for table `shirt_shirtthreadfabric`
--

CREATE TABLE `shirt_shirtthreadfabric` (
  `fabric_id` int(10) UNSIGNED NOT NULL COMMENT 'Shirt Fit ID',
  `display_fabric_thumb` varchar(200) DEFAULT NULL COMMENT 'display_fabric_thumb',
  `fabric_thumb` varchar(255) DEFAULT NULL COMMENT 'fabric_thumb',
  `fabric_large_image` varchar(255) DEFAULT NULL COMMENT 'sample',
  `title` varchar(6) DEFAULT NULL COMMENT 'btn_color_id',
  `price` int(11) DEFAULT NULL COMMENT 'shoes_color_id',
  `type` smallint(6) DEFAULT NULL COMMENT 'fabric_material_id',
  `status` smallint(6) DEFAULT NULL COMMENT 'fabric_pattern_id',
  `created_time` datetime DEFAULT NULL COMMENT 'fabric_season_id',
  `update_time` datetime DEFAULT NULL COMMENT 'fabric_color_id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='shirt_shirtthreadfabric';

--
-- Dumping data for table `shirt_shirtthreadfabric`
--

INSERT INTO `shirt_shirtthreadfabric` (`fabric_id`, `display_fabric_thumb`, `fabric_thumb`, `fabric_large_image`, `title`, `price`, `type`, `status`, `created_time`, `update_time`) VALUES
(1, 'fab_shirt_images/display_thumb/green.png', 'fab_shirt_images/thumbs/green.png', 'fab_shirt_images/green.png', 'Green', 12, 0, 1, '2018-06-04 13:31:54', '2018-06-04 13:31:54'),
(2, 'fab_shirt_images/display_thumb/blue.png', 'fab_shirt_images/thumbs/blue.png', 'fab_shirt_images/blue.png', 'Blue', 12, 0, 1, '2018-06-04 13:36:14', '2018-06-04 13:36:14'),
(3, 'fab_shirt_images/display_thumb/red.png', 'fab_shirt_images/thumbs/red.png', 'fab_shirt_images/red.png', 'Red', 12, 0, 1, '2018-06-04 13:38:22', '2018-06-04 13:38:22'),
(4, 'fab_shirt_images/display_thumb/white.png', 'fab_shirt_images/thumbs/white.png', 'fab_shirt_images/white.png', 'White', 34, 0, 1, '2018-06-04 13:38:50', '2018-06-04 13:38:50'),
(5, 'fab_shirt_images/display_thumb/yellow.png', 'fab_shirt_images/thumbs/yellow.png', 'fab_shirt_images/yellow.png', 'Yellow', 23, 0, 1, '2018-06-04 13:39:23', '2018-06-04 13:39:23');

-- --------------------------------------------------------

--
-- Table structure for table `shirt_suitfabric`
--

CREATE TABLE `shirt_suitfabric` (
  `fabric_id` int(10) UNSIGNED NOT NULL COMMENT 'Shirt Fit ID',
  `display_fabric_thumb` varchar(200) DEFAULT NULL COMMENT 'display_fabric_thumb',
  `fabric_thumb` varchar(255) DEFAULT NULL COMMENT 'fabric_thumb',
  `fabric_large_image` varchar(255) DEFAULT NULL COMMENT 'sample',
  `title` varchar(6) DEFAULT NULL COMMENT 'btn_color_id',
  `price` int(11) DEFAULT NULL COMMENT 'shoes_color_id',
  `type` smallint(6) DEFAULT NULL COMMENT 'fabric_material_id',
  `btn_color_id` int(11) DEFAULT NULL COMMENT 'fabric_pattern_id',
  `tie_color` smallint(6) DEFAULT NULL COMMENT 'fabric_season_id',
  `shirt_type` smallint(6) DEFAULT NULL COMMENT 'fabric_color_id',
  `shoes_color` smallint(6) DEFAULT NULL COMMENT 'shoes_color',
  `is_fabric` varchar(255) DEFAULT NULL COMMENT 'is_fabric',
  `suit_material_id` int(11) DEFAULT NULL COMMENT 'suit_material_id',
  `suit_pattern_id` int(11) DEFAULT NULL COMMENT 'suit_pattern_id',
  `suit_season_id` int(11) DEFAULT NULL COMMENT 'suit_season_id',
  `suit_color_id` int(11) DEFAULT NULL COMMENT 'suit_color_id',
  `suit_category_id` int(11) DEFAULT NULL COMMENT 'suit_category_id',
  `status` smallint(6) DEFAULT NULL COMMENT 'status',
  `created_time` datetime DEFAULT NULL COMMENT 'created_time',
  `update_time` datetime DEFAULT NULL COMMENT 'update_time'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='shirt_suitfabric';

--
-- Dumping data for table `shirt_suitfabric`
--

INSERT INTO `shirt_suitfabric` (`fabric_id`, `display_fabric_thumb`, `fabric_thumb`, `fabric_large_image`, `title`, `price`, `type`, `btn_color_id`, `tie_color`, `shirt_type`, `shoes_color`, `is_fabric`, `suit_material_id`, `suit_pattern_id`, `suit_season_id`, `suit_color_id`, `suit_category_id`, `status`, `created_time`, `update_time`) VALUES
(1, 'fab_suit_images/display_thumb/fabric_01_100.png', 'fab_suit_images/thumbs/fabric_01_100.png', NULL, 'Test', 21, 0, 0, 0, 0, 0, '', 2, 7, 3, 3, 2, 1, '2018-06-12 07:16:39', '2018-06-12 07:16:39'),
(2, 'fab_suit_images/display_thumb/13.jpg', 'f', NULL, 'Akshay', 30, 0, 0, 0, 0, 0, '', 3, 5, 2, 4, 7, 1, '2018-06-05 07:30:22', '2018-06-05 07:30:22');

-- --------------------------------------------------------

--
-- Table structure for table `signifyd_case`
--

CREATE TABLE `signifyd_case` (
  `entity_id` int(10) UNSIGNED NOT NULL COMMENT 'Entity_id',
  `order_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'Order_id',
  `case_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'Case_id',
  `guarantee_eligible` tinyint(1) DEFAULT NULL COMMENT 'Guarantee_eligible',
  `guarantee_disposition` varchar(32) DEFAULT 'PENDING' COMMENT 'Guarantee_disposition',
  `status` varchar(32) DEFAULT 'PENDING' COMMENT 'Status',
  `score` int(10) UNSIGNED DEFAULT NULL COMMENT 'Score',
  `associated_team` text COMMENT 'Associated_team',
  `review_disposition` varchar(32) DEFAULT NULL COMMENT 'Review_disposition',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Created_at',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Updated_at'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='signifyd_case';

--
-- Indexes for dumped tables
--

--
-- Indexes for table `shirtbottom`
--
ALTER TABLE `shirtbottom`
  ADD PRIMARY KEY (`shirtbottom_id`);

--
-- Indexes for table `shirtcategory`
--
ALTER TABLE `shirtcategory`
  ADD PRIMARY KEY (`shirtcategory_id`);

--
-- Indexes for table `shirtcolor`
--
ALTER TABLE `shirtcolor`
  ADD PRIMARY KEY (`shirtcolor_id`);

--
-- Indexes for table `shirtcuff`
--
ALTER TABLE `shirtcuff`
  ADD PRIMARY KEY (`shirtcuff_id`);

--
-- Indexes for table `shirtfit`
--
ALTER TABLE `shirtfit`
  ADD PRIMARY KEY (`shirtfit_id`);

--
-- Indexes for table `shirtmaterial`
--
ALTER TABLE `shirtmaterial`
  ADD PRIMARY KEY (`shirtmaterial_id`);

--
-- Indexes for table `shirtpattern`
--
ALTER TABLE `shirtpattern`
  ADD PRIMARY KEY (`shirtpattern_id`);

--
-- Indexes for table `shirtplackets`
--
ALTER TABLE `shirtplackets`
  ADD PRIMARY KEY (`shirtplackets_id`);

--
-- Indexes for table `shirtpleats`
--
ALTER TABLE `shirtpleats`
  ADD PRIMARY KEY (`shirtpleats_id`);

--
-- Indexes for table `shirtpocket`
--
ALTER TABLE `shirtpocket`
  ADD PRIMARY KEY (`shirtpocket_id`);

--
-- Indexes for table `shirtseason`
--
ALTER TABLE `shirtseason`
  ADD PRIMARY KEY (`shirtseason_id`);

--
-- Indexes for table `shirtsleeves`
--
ALTER TABLE `shirtsleeves`
  ADD PRIMARY KEY (`shirtsleeves_id`);

--
-- Indexes for table `shirtstyle`
--
ALTER TABLE `shirtstyle`
  ADD PRIMARY KEY (`shirtstyle_id`);

--
-- Indexes for table `shirts_shirtfabric`
--
ALTER TABLE `shirts_shirtfabric`
  ADD PRIMARY KEY (`shirtfabric_id`);

--
-- Indexes for table `shirt_accent_chestpleats`
--
ALTER TABLE `shirt_accent_chestpleats`
  ADD PRIMARY KEY (`chestpleats_id`);

--
-- Indexes for table `shirt_accent_collarstyle`
--
ALTER TABLE `shirt_accent_collarstyle`
  ADD PRIMARY KEY (`collarstyle_id`);

--
-- Indexes for table `shirt_accent_cuffs`
--
ALTER TABLE `shirt_accent_cuffs`
  ADD PRIMARY KEY (`cuff_id`);

--
-- Indexes for table `shirt_accent_elbowpatches`
--
ALTER TABLE `shirt_accent_elbowpatches`
  ADD PRIMARY KEY (`elbowpatches_id`);

--
-- Indexes for table `shirt_accent_placket`
--
ALTER TABLE `shirt_accent_placket`
  ADD PRIMARY KEY (`placket_id`);

--
-- Indexes for table `shirt_accent_threadcolor`
--
ALTER TABLE `shirt_accent_threadcolor`
  ADD PRIMARY KEY (`threadcolor_id`);

--
-- Indexes for table `shirt_accent_threads`
--
ALTER TABLE `shirt_accent_threads`
  ADD PRIMARY KEY (`threads_id`);

--
-- Indexes for table `shirt_button_fabric`
--
ALTER TABLE `shirt_button_fabric`
  ADD PRIMARY KEY (`button_fabric_id`);

--
-- Indexes for table `shirt_fabric`
--
ALTER TABLE `shirt_fabric`
  ADD PRIMARY KEY (`fabric_id`);

--
-- Indexes for table `shirt_shirtfabric`
--
ALTER TABLE `shirt_shirtfabric`
  ADD PRIMARY KEY (`fabric_id`);

--
-- Indexes for table `shirt_shirtthreadfabric`
--
ALTER TABLE `shirt_shirtthreadfabric`
  ADD PRIMARY KEY (`fabric_id`);

--
-- Indexes for table `shirt_suitfabric`
--
ALTER TABLE `shirt_suitfabric`
  ADD PRIMARY KEY (`fabric_id`);

--
-- Indexes for table `signifyd_case`
--
ALTER TABLE `signifyd_case`
  ADD PRIMARY KEY (`entity_id`),
  ADD UNIQUE KEY `SIGNIFYD_CASE_ORDER_ID` (`order_id`),
  ADD UNIQUE KEY `SIGNIFYD_CASE_CASE_ID` (`case_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `shirtbottom`
--
ALTER TABLE `shirtbottom`
  MODIFY `shirtbottom_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Shirt Bottom ID', AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `shirtcategory`
--
ALTER TABLE `shirtcategory`
  MODIFY `shirtcategory_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Shirt Catagory ID';
--
-- AUTO_INCREMENT for table `shirtcolor`
--
ALTER TABLE `shirtcolor`
  MODIFY `shirtcolor_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Shirt Color ID', AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `shirtcuff`
--
ALTER TABLE `shirtcuff`
  MODIFY `shirtcuff_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `shirtfit`
--
ALTER TABLE `shirtfit`
  MODIFY `shirtfit_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Shirt Fit ID', AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `shirtmaterial`
--
ALTER TABLE `shirtmaterial`
  MODIFY `shirtmaterial_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Shirt Fit ID', AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `shirtpattern`
--
ALTER TABLE `shirtpattern`
  MODIFY `shirtpattern_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Shirt Fit ID', AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `shirtplackets`
--
ALTER TABLE `shirtplackets`
  MODIFY `shirtplackets_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Shirt Fit ID', AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `shirtpleats`
--
ALTER TABLE `shirtpleats`
  MODIFY `shirtpleats_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Shirt Fit ID', AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `shirtseason`
--
ALTER TABLE `shirtseason`
  MODIFY `shirtseason_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Shirt Fit ID', AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `shirts_shirtfabric`
--
ALTER TABLE `shirts_shirtfabric`
  MODIFY `shirtfabric_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ShirtFabric ID', AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `shirt_accent_chestpleats`
--
ALTER TABLE `shirt_accent_chestpleats`
  MODIFY `chestpleats_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `shirt_accent_collarstyle`
--
ALTER TABLE `shirt_accent_collarstyle`
  MODIFY `collarstyle_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `shirt_accent_cuffs`
--
ALTER TABLE `shirt_accent_cuffs`
  MODIFY `cuff_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `shirt_accent_elbowpatches`
--
ALTER TABLE `shirt_accent_elbowpatches`
  MODIFY `elbowpatches_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `shirt_accent_placket`
--
ALTER TABLE `shirt_accent_placket`
  MODIFY `placket_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `shirt_accent_threadcolor`
--
ALTER TABLE `shirt_accent_threadcolor`
  MODIFY `threadcolor_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Shirt Fit ID', AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `shirt_accent_threads`
--
ALTER TABLE `shirt_accent_threads`
  MODIFY `threads_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Shirt Fit ID', AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `shirt_button_fabric`
--
ALTER TABLE `shirt_button_fabric`
  MODIFY `button_fabric_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Shirt Fit ID', AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `shirt_fabric`
--
ALTER TABLE `shirt_fabric`
  MODIFY `fabric_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Shirt Fit ID', AUTO_INCREMENT=65;
--
-- AUTO_INCREMENT for table `shirt_shirtfabric`
--
ALTER TABLE `shirt_shirtfabric`
  MODIFY `fabric_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `shirt_shirtthreadfabric`
--
ALTER TABLE `shirt_shirtthreadfabric`
  MODIFY `fabric_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Shirt Fit ID', AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `shirt_suitfabric`
--
ALTER TABLE `shirt_suitfabric`
  MODIFY `fabric_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Shirt Fit ID', AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `signifyd_case`
--
ALTER TABLE `signifyd_case`
  MODIFY `entity_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Entity_id';
--
-- Constraints for dumped tables
--

--
-- Constraints for table `signifyd_case`
--
ALTER TABLE `signifyd_case`
  ADD CONSTRAINT `SIGNIFYD_CASE_ORDER_ID_SALES_ORDER_ENTITY_ID` FOREIGN KEY (`order_id`) REFERENCES `sales_order` (`entity_id`) ON DELETE SET NULL;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
