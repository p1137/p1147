<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Shirt\Tool\Cron;

class Generateimage {

    protected function getConnection() {
        return $this->getResource()->getConnection();
    }

    protected function getResource() {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        return $objectManager->get('Magento\Framework\App\ResourceConnection');
    }

    protected function getTableName() {
        return $this->getConnection()->getTableName('generation_cmd');
    }

    public function execute() {
        $selectQuery = "SELECT `id`, `flag`, `command` FROM {$this->getTableName()} WHERE `flag`=0 ORDER BY id ASC";
        $result = $this->getConnection()->fetchAll($selectQuery);
        if (count($result) > 0) {
            foreach ($result as $cmd) {
                if (stripos($cmd['command'], 'UPDATE') !== false) {
                    $this->getConnection()->query($cmd['command']);
                } else {
                    exec($cmd['command']);
                }
                $writeQuery = "UPDATE {$this->getTableName()} SET `flag`=1 WHERE `id`='" . $cmd['id'] . "'";
                $this->getConnection()->query($writeQuery);
            }
        }
        $selectQuery = "SELECT `id` FROM {$this->getTableName()} WHERE `flag`=0";
        $result = $this->getConnection()->fetchAll($selectQuery);
        if (count($result) > 0)
            $deleteQuery = "DELETE FROM {$this->getTableName()} WHERE `flag`=1";
        else
            $deleteQuery = "TRUNCATE TABLE {$this->getTableName()}";
        $this->getConnection()->query($deleteQuery);
    }

}
