define(["angular", "jquery", "angular_route", "angular_animate"], function (angular, jQuery) {
    var did = "0";
    var sizeArr = {};
    var selStyleArr = {};
    var basePath;
    var canvas, currSymbol;
    var flg;
    var stylePriceArr = [];
    var styleAccentPriceArr = {};
    var shirtFabricName;
    var jacketSizes = [
        {"id": "19", "parent": "Jacket", "product_type": "1", "gender": 1, "size_name": "Slim", "chest": "15.0", "waist": "36"
            , "hip": "38", "shoulder": "38", "sleeve": "38", "length": "38", "size_order": "1"},
        {"id": "20", "parent": "Jacket", "product_type": "1", "gender": 1, "size_name": "Classic", "chest": "15.0", "waist": "36"
            , "hip": "38", "shoulder": "38", "sleeve": "38", "length": "38", "size_order": "2"},
        {"id": "21", "parent": "Jacket", "product_type": "1", "gender": 1, "size_name": "XL", "chest": "15.0", "waist": "36"
            , "hip": "38", "shoulder": "38", "sleeve": "38", "length": "38", "size_order": "3"}
    ];

    jQuery(document).ready(function (e) {


        basePath = jQuery("#basePath").val();
        currSymbol = jQuery("#currSymbol").val();
        jQuery("#content-5_hor").mCustomScrollbar({
            axis: "x",
            theme: "dark-thin",
            autoExpandScrollbar: true,
            advanced: {autoExpandHorizontalScroll: true}
        });

        // canvas = new fabric.Canvas("canvas");



        jQuery("#validationSubmitButton").on("click", function () {


            myValidation();


        });

        /*fabric popUp hide*/
        jQuery(".navigate-menu").mouseup(function (e)
        {

            var container = jQuery("#fabricSlid");
            if (!container.is(e.target)) // if the target of the click isn't the container...
            {
                console.log("else part");
                jQuery(".fabricInfo a").trigger("click");
            }

        });

//monohide
        var chk = jQuery("#data").val();
        if (chk === "" || chk == undefined)
        {
            console.log("Hide monogram")
            jQuery("#mainImageHolder").find("[rel=Monogram]").hide();
        } else {
            console.log("show monogram")
            jQuery("#mainImageHolder").find("[rel=Monogram]").show();
        }
        setTimeout(function () {
            //jQuery("#div5 ul a").eq(3).find(".option_icon_left").children().css({"display": "none"});
            //jQuery("#div5 ul a").eq(3).find(".option_icon_left").append('<span class="liningIcon"><img width="50" height="50" src="media/tool_data/img/Line.png"></span>');
        }, 1500);

        setTimeout(function () {
            jQuery(".removemin").on("click", function () {

                jQuery("body").removeClass("LeftpanemOpen");

            });

        }, 1500);




    });
    function myValidation() {
        var flg = true;

        var measureMents = jQuery("input[name='unitOptions']:checked").val();
        console.info("measureMents = " + measureMents);

        /*jacket*/
        var chestRangeVar = jQuery("#chestRange").next().attr("rangeval");

        var chestRangemin = chestRangeVar.split("-")[0];
        chestRangemin = parseInt(chestRangemin);

        var chestRangemax = chestRangeVar.split("-")[1];
        chestRangemax = parseInt(chestRangemax)

        jQuery("#ChestRangeHolder").css({"display": "none"});
        jQuery("#Chestrequired").css({"display": "none"});
        var ChestRange = jQuery("#chestRange").val();
        console.log("min = " + chestRangemin);
        console.log("max = " + chestRangemax);
        console.log("val = " + ChestRange);


        if (ChestRange > chestRangemax || ChestRange < chestRangemin) {
            if (ChestRange == "") {
                console.log("iffffffffffffffffffff");
                //setTimeout(function() {
                jQuery("#Chestrequired").css({"display": "block"});
                // }, 300);

            } else {


                console.info("elseeeeeeeeee")
                setTimeout(function () {
                    jQuery("#chestRange").val("");

                    jQuery("#ChestRangeHolder").css({"display": "block"});
                }, 500);

            }

            flg = false;
        }


        var waistRangeVar = jQuery("#waistRange").next().attr("rangeval");
        var waistRangemin = waistRangeVar.split("-")[0];
        waistRangemin = parseInt(waistRangemin);

        var waistRangemax = waistRangeVar.split("-")[1];
        waistRangemax = parseInt(waistRangemax)

        jQuery("#Waistrequired").css({"display": "none"});
        jQuery("#WaistRangeHolder").css({"display": "none"});
        var WaistRange = jQuery("#waistRange").val();
        console.log("min = " + waistRangemin);
        console.log("max = " + waistRangemax);
        console.log("val = " + WaistRange);
        if (WaistRange > waistRangemax || WaistRange < waistRangemin) {

            if (WaistRange == "") {
                console.log("iffffffffffffffffffffffffff");
                // setTimeout(function() {
                jQuery("#Waistrequired").css({"display": "block"});
                // }, 300);
            } else {

                setTimeout(function () {
                    console.log("elseeeeeeeeeeeee");
                    jQuery("#waistRange").val("");
                    jQuery("#WaistRangeHolder").css({"display": "block"});
                }, 500);

            }

            flg = false;
        }

        var HipRange = jQuery("#hipRange").val();
        var hipRangeVar = jQuery("#hipRange").next().attr("rangeval");
        var hipRangemin = hipRangeVar.split("-")[0];
        hipRangemin = parseInt(hipRangemin);

        var hipRangemax = hipRangeVar.split("-")[1];
        hipRangemax = parseInt(hipRangemax);
        jQuery("#Hiprequired").css({"display": "none"});
        jQuery("#HipRangeHolder").css({"display": "none"});
        if (HipRange > hipRangemax || HipRange < hipRangemin) {
            if (HipRange == "") {


                //setTimeout(function() {
                jQuery("#Hiprequired").css({"display": "block"});
                //}, 300);
            } else {

                setTimeout(function () {
                    jQuery("#hipRange").val("");
                    jQuery("#HipRangeHolder").css({"display": "block"});
                }, 500);

            }

            flg = false;
        }

        var ShoulderRange = jQuery("#shoulderRange").val();
        var shoulderRangeVar = jQuery("#shoulderRange").next().attr("rangeval");
        var shoulderRangemin = shoulderRangeVar.split("-")[0];
        shoulderRangemin = parseInt(shoulderRangemin);

        var shoulderRangemax = shoulderRangeVar.split("-")[1];
        shoulderRangemax = parseInt(shoulderRangemax);
        jQuery("#Shoulderrequired").css({"display": "none"});
        jQuery("#ShoulderRangeHolder").css({"display": "none"});
        if (ShoulderRange > shoulderRangemax || ShoulderRange < shoulderRangemin) {
            if (ShoulderRange == "") {
                //setTimeout(function() {
                jQuery("#Shoulderrequired").css({"display": "block"});
                // }, 300);

            } else {

                setTimeout(function () {
                    jQuery("#shoulderRange").val("");
                    jQuery("#ShoulderRangeHolder").css({"display": "block"});
                }, 500);

            }
            flg = false;
        }

        var SleeveRange = jQuery("#sleeveRange").val();
        var sleeveRangeVar = jQuery("#sleeveRange").next().attr("rangeval");
        var sleeveRangemin = sleeveRangeVar.split("-")[0];
        sleeveRangemin = parseInt(sleeveRangemin);

        var sleeveRangemax = sleeveRangeVar.split("-")[1];
        sleeveRangemax = parseInt(sleeveRangemax)
        jQuery("#Sleeverequired").css({"display": "none"});
        jQuery("#SleeveRangeHolder").css({"display": "none"});
        if (SleeveRange > sleeveRangemax || SleeveRange < sleeveRangemin) {
            if (SleeveRange == "") {
                //setTimeout(function() {
                jQuery("#Sleeverequired").css({"display": "block"});
                //}, 300);


            } else {

                setTimeout(function () {
                    jQuery("#sleeveRange").val("");
                    jQuery("#SleeveRangeHolder").css({"display": "block"});
                }, 500);

            }
            flg = false;
        }

        var lengthRange = jQuery("#lengthRange").val();
        var lengthRangeVar = jQuery("#lengthRange").next().attr("rangeval");
        var lengthRangemin = lengthRangeVar.split("-")[0];
        lengthRangemin = parseInt(lengthRangemin);

        var lengthRangemax = lengthRangeVar.split("-")[1];
        lengthRangemax = parseInt(lengthRangemax);
        jQuery("#Lengthrequired").css({"display": "none"});
        jQuery("#LengthRangeHolder").css({"display": "none"});
        if (lengthRange > lengthRangemax || lengthRange < lengthRangemin) {
            if (lengthRange == "") {
                //setTimeout(function() {
                jQuery("#Lengthrequired").css({"display": "block"});
                //}, 300);


            } else {

                setTimeout(function () {
                    jQuery("#lengthRange").val("");
                    jQuery("#LengthRangeHolder").css({"display": "block"});
                }, 500);

            }
            flg = false;
        }

        /*Pant*/
        var PantWaist = jQuery("#PantWaistRange").val();
        var PantWaistRangeVar = jQuery("#PantWaistRange").next().attr("rangeval");
        var PantWaistRangemin = PantWaistRangeVar.split("-")[0];
        PantWaistRangemin = parseInt(PantWaistRangemin);

        var PantWaistRangemax = PantWaistRangeVar.split("-")[1];
        PantWaistRangemax = parseInt(PantWaistRangemax);
        jQuery("#PantWaistrequired").css({"display": "none"});
        jQuery("#PantWaistRangeHolder").css({"display": "none"});
        if (PantWaist > PantWaistRangemax || PantWaist < PantWaistRangemin) {
            if (PantWaist == "") {
                //setTimeout(function() {
                jQuery("#PantWaistrequired").css({"display": "block"});
                // }, 300);


            } else {

                setTimeout(function () {
                    jQuery("#PantWaistRange").val("");
                    jQuery("#PantWaistRangeHolder").css({"display": "block"});
                }, 500);

            }
            flg = false;
        }

        var PantHip = jQuery("#PantHipRange").val();
        var PantHipRangeVar = jQuery("#PantHipRange").next().attr("rangeval");
        var PantHipRangemin = PantHipRangeVar.split("-")[0];
        PantHipRangemin = parseInt(PantHipRangemin);

        var PantHipRangemax = PantHipRangeVar.split("-")[1];
        PantHipRangemax = parseInt(PantHipRangemax);
        jQuery("#PantHiprequired").css({"display": "none"});
        jQuery("#PantHipRangeHolder").css({"display": "none"});
        if (PantHip > PantHipRangemax || PantHip < PantHipRangemin) {
            if (PantHip == "") {
                // setTimeout(function() {
                jQuery("#PantHiprequired").css({"display": "block"});
                // }, 300);


            } else {

                setTimeout(function () {
                    jQuery("#PantHipRange").val("");
                    jQuery("#PantHipRangeHolder").css({"display": "block"});
                }, 500);

            }
            flg = false;

        }

        var PantCrotch = jQuery("#PantCrotchRange").val();
        var PantCrotchRangeVar = jQuery("#PantCrotchRange").next().html();
        ;
        var PantCrotchRangemin = PantCrotchRangeVar.split("-")[0];
        PantCrotchRangemin = parseInt(PantCrotchRangemin);

        var PantCrotchRangemax = PantCrotchRangeVar.split("-")[1];
        PantCrotchRangemax = parseInt(PantCrotchRangemax);
        jQuery("#PantCrotchrequired").css({"display": "none"});
        jQuery("#PantCrotchRangeHolder").css({"display": "none"});

        console.log("PantCrotchRangemax==" + PantCrotchRangemax);
        console.log("PantCrotchRangemin==" + PantCrotchRangemin);
        console.log("PantCrotch==" + PantCrotch);

        if (PantCrotch > PantCrotchRangemax || PantCrotch < PantCrotchRangemin) {

            console.log("in croch")
            if (PantCrotch == "") {

                //setTimeout(function() {
                jQuery("#PantCrotchrequired").css({"display": "block"});
                // }, 300);


            } else {

                setTimeout(function () {
                    jQuery("#PantCrotchRangeHolder").css({"display": "block"});
                    jQuery("#PantCrotchRange").val("");
                }, 500);

            }

            flg = false;

        }

        var PantThigh = jQuery("#PantThighRange").val();
        var PantThighRangeVar = jQuery("#PantThighRange").next().html();
        ;
        var PantThighRangemin = PantThighRangeVar.split("-")[0];
        PantThighRangemin = parseInt(PantThighRangemin);

        var PantThighRangemax = PantThighRangeVar.split("-")[1];
        PantThighRangemax = parseInt(PantThighRangemax);

        jQuery("#PantThighrequired").css({"display": "none"});
        jQuery("#PantThighRangeHolder").css({"display": "none"});
        if (PantThigh > PantThighRangemax || PantThigh < PantThighRangemin) {
            if (PantThigh == "") {
                // setTimeout(function() {
                jQuery("#PantThighrequired").css({"display": "block"});
                // }, 300);


            } else {

                setTimeout(function () {
                    jQuery("#PantThighRange").val("");
                    jQuery("#PantThighRangeHolder").css({"display": "block"});
                }, 500);

            }
            flg = false;
        }

        var PantLength = jQuery("#PantLengthRang").val();
        var PantLengthRangVar = jQuery("#PantLengthRang").next().attr("rangeval");
        var PantLengthRangmin = PantLengthRangVar.split("-")[0];
        PantLengthRangmin = parseInt(PantLengthRangmin);

        var PantLengthRangmax = PantLengthRangVar.split("-")[1];
        PantLengthRangmax = parseInt(PantLengthRangmax);

        jQuery("#PantLengthrequired").css({"display": "none"});
        jQuery("#PantLengthRangeHolder").css({"display": "none"});
        if (PantLength > PantLengthRangmax || PantLength < PantLengthRangmin) {
            if (PantLength == "") {
                //setTimeout(function() {
                jQuery("#PantLengthrequired").css({"display": "block"});
                // }, 300);


            } else {

                setTimeout(function () {
                    jQuery("#PantLengthRang").val("");
                    jQuery("#PantLengthRangeHolder").css({"display": "block"});
                }, 500);

            }
            flg = false;
        }





        if (!flg) {
            return false;

        } else {
            return true;

        }
    }

    function addImage(imgPath)
    {
        //  console.log("add image")
        //i = 101;
        //jQuery(".remove-pr").trigger('click');
        //jQuery(".preloader").show();
        //console.info("add function is called = " + imgPath);
        fabric.Image.fromURL(imgPath, function (image) {

            var theScaleFactor = getScaleFactor(image.width, image.height);

            image.set({
                left: theScaleFactor.x,
                top: theScaleFactor.y,
                angle: 0,
                identity: "img" + canvas.getObjects().length,
                centerTransform: true
            }).scale(theScaleFactor.scale).setCoords();

            canvas.add(image);
            canvas.setActiveObject(image);

            canvas.renderAll();

            jQuery(".preloader").hide();

        });

    }

    function getScaleFactor(w, h)
    {
        var newScaleY = 1;
        var newScaleX = 1;
        var isScaled = false;

        var arrW = canvas.getWidth();
        var arrH = canvas.getHeight();


        if (w > arrW || h > arrH)
        {
            if ((w >= arrW) && (h >= arrH))
            {
                if (w > h)
                {
                    newScaleX = arrW / w;
                    newScaleY = newScaleX;
                } else
                {
                    newScaleY = arrH / h;
                    newScaleX = newScaleY;
                }
                isScaled = true;
            }

            if (!isScaled && (w >= arrW))
            {
                newScaleX = arrW / w;
                newScaleY = newScaleX;
                isScaled = true;
            }

            if (!isScaled && (h >= arrH))
            {
                newScaleY = arrH / h;
                newScaleX = newScaleY;
                isScaled = true;
            }

        }

        var newX = (arrW - (parseFloat(w * newScaleX))) / 2;
        var newY = (arrH - (parseFloat(h * newScaleX))) / 2;

        return {scale: newScaleX, x: newX, y: newY};
    }


    function addToCart() {
        // saveDesign();
        jQuery("#addMeasurement").modal("show");
    }


    function saveStdSizeData()
    {
        var sizeData = {};

        sizeStdFlg = true;

        getProductImage();
    }
    function getStdSizeData()
    {



        var cnt = 0;
        var sizeData = {};

        var fit = jQuery("#stdSizeFlgContainer .fit-select-box option:selected").val().toLowerCase();
        var qty = jQuery("#stdSizeFlgContainer .stdQty option:selected").val();


        for (var s in jacketSizes) {
            if (jacketSizes.hasOwnProperty(s))
            {
                console.log("jacketSizes[s]['size_name'] = " + jacketSizes[s]['size_name'])
                if ((jacketSizes[s]['size_name']).toLowerCase() == fit)
                {
                    sizeData[cnt++] = jacketSizes[s];

                }
            }


        }
        /*//pant
         for (var s in pantSizes) {
         if (jacketSizes.hasOwnProperty(s))
         {
         if ((pantSizes[s]['size_name']).toLowerCase() == fit)
         {
         sizeData[cnt++] = pantSizes[s];
         }
         }
         
         }
         */

        return {
            sizeData: sizeData,
            qty: qty,
            fit: fit,
            type: "Standard"
        };

    }
    function getScanSizeData()
    {

        var sizeData = {};
        var qty = jQuery("#scanSizeFlgContainer .scanQty option:selected").val();
        var obj = {};
        var cnt = 0;

        jQuery("#scanSizeFlgContainer input").each(function () {

            var name = jQuery(this).attr("name");
            var value = jQuery(this).val();
            var parent = jQuery(this).attr("parent");

            if (parent)
            {
                obj.name = name;
                obj.value = value;
                obj.parent = parent;

                sizeData[cnt++] = obj;
                obj = {};
            }
        });

        return {
            sizeData: sizeData,
            qty: qty,
            type: "Scan"
        };

    }
    function saveScanSizeData() {

        var flg;

        if (jQuery("#chest").val() == "") {
//alert("Valid email address.");
            jQuery('#reqNeck').show();
            jQuery('#reqNeck').html("*This field is required.").css({"color": "red"});
            flg = false;
        } else
        {

            jQuery('#reqNeck').hide();
        }



        if (jQuery("#chest").val() == "") {
//alert("Valid email address.");
            jQuery('#reqChest').show();
            jQuery('#reqChest').html("*This field is required.").css({"color": "red"});
            flg = false;
        } else
        {

            jQuery('#reqChest').hide();
        }

        if (jQuery("#waist").val() == "") {
            jQuery('#reqWaist').show();
            jQuery('#reqWaist').html("*This field is required.").css({"color": "red"});
            flg = false;
        } else
        {
            jQuery('#reqWaist').hide();
        }

        if (jQuery("#hip").val() == "") {
//alert("Valid email address.");
            jQuery('#reqHip').show();
            jQuery('#reqHip').html("*This field is required.").css({"color": "red"});
            flg = false;
        } else
        {
            jQuery('#reqHip').hide();
        }


        if (jQuery("#shoulder").val() == "") {
            jQuery('#reqShoulder').show();
            jQuery('#reqShoulder').html("*This field is required.").css({"color": "red"});
            flg = false;
        } else
        {
            jQuery('#reqShoulder').hide();
        }


        if (jQuery("#sleeve").val() == "") {
            jQuery('#reqSleeve').show();
            jQuery('#reqSleeve').html("*This field is required.").css({"color": "red"});
            flg = false;
        } else
        {
            jQuery('#reqSleeve').hide();
        }

        if (jQuery("#length").val() == "") {
            jQuery('#reqLength').show();
            jQuery('#reqLength').html("*This field is required.").css({"color": "red"});
            flg = false;
        } else
        {
            jQuery('#reqLength').hide();
        }


        if (jQuery("#pwaist").val() == "") {
            jQuery('#reqPwaist').show();
            jQuery('#reqPwaist').html("*This field is required.").css({"color": "red"});
            flg = false;
        } else
        {
            jQuery('#reqPwaist').hide();
        }

        if (jQuery("#phip").val() == "") {
            jQuery('#reqPhip').show();
            jQuery('#reqPhip').html("*This field is required.").css({"color": "red"});
            flg = false;
        } else
        {
            jQuery('#reqPhip').hide();
        }

        if (jQuery("#crotch").val() == "") {
            jQuery('#reqCrotch').show();
            jQuery('#reqCrotch').html("*This field is required.").css({"color": "red"});
            flg = false;
        } else
        {
            jQuery('#reqCrotch').hide();
        }

        if (jQuery("#thigh").val() == "") {
            jQuery('#reqThigh').show();
            jQuery('#reqThigh').html("*This field is required.").css({"color": "red"});
            flg = false;
        } else
        {
            jQuery('#reqThigh').hide();
        }

        if (jQuery("#plength").val() == "") {
            jQuery('#reqPlength').show();
            jQuery('#reqPlength').html("*This field is required.").css({"color": "red"});
            flg = false;
        } else
        {
            jQuery('#reqPlength').hide();
        }

        sizeStdFlg = false;

        //return (flg == false) ? false : saveDesign();
        return (flg == false) ? false : getProductImage();

    }
    function camelToDash(str) {
        return str.replace(/\W+/g, ' ').replace(/([a-z\d])([A-Z])/g, '$1 $2');
    }

    var sizeDataArr = {};
    function saveDesign() {

        jQuery(".loaderText").css({"display": "none"});
        jQuery("#theInitialProgress").css({"display": "block"});

        var user_id = "1";
        var productId = jQuery("#productId").val();
        var quantity = jQuery('.stdQty').val();
        var productType = "Shirt";

        var prodImg;
        var finalPrice = jQuery("#basePrice").text();
        var genderId = "Male";




        var dataObj = {};

        jQuery(".custom-part").each(function () {
            var optionVal = jQuery(this).attr('rel');
            var propertyVal = jQuery(this).attr('styleName') ? jQuery(this).attr('styleName') : "";
            var properyIndex = jQuery(this).attr('activeStyleIndex');
            var optionLabel = jQuery(this).attr('label');
            var optionIcon = jQuery(this).attr('icon');
            console.log("optionIcon = " + optionIcon)

            selStyleArr[optionVal + " " + optionIcon] = camelToDash(propertyVal);


        });

        //selStyleArr['jacketFabricName icon-icon-30'] = jacketFabricName;

        selStyleArr['shirtFabricName icon-Fabric'] = shirtFabricName;


        //end


        setTimeout(function () {

            prodImg = jQuery("#prev-holder img").attr("src");//http://192.168.2.62/p1147/c_customer/index/save

            jQuery.post(basePath + "c_customer/index/save", {
                productType: productType,
                userId: user_id,
                style_data: selStyleArr,
                size_Data_Arr: sizeDataArr,
                qty: quantity,
                price: finalPrice,
                gender: genderId,
                product_id: productId,
                did: did,
                action: "save",
                prodImg: prodImg,
            }).done(function (data) {
                console.log(selStyleArr);

                if (data == "done") {
                    window.location.href = basePath + "checkout/cart/";
                }
                if (data == "fail")
                {
                    alert('cannot add to cart, product is out of stock')
                    window.location.href = basePath + 'checkout/cart';
                }

                if (data)
                {
                    did = data;
                    /*finalPrice = parseFloat(finalPrice) * quantity;*/
//                document.location.href = basePath + 'cart/?add-to-cart=' + productId + '&did=' + did + '&prc=' + finalPrice + '&qty=' + quantity;
                    // document.location.href = basePath + '/checkout/cart/add/product/' + productId + '/qty/' + quantity + '/did/' + did + '/prc/' + finalPrice + '/form_key/' + jQuery("#key_id").val();

                }
            });
        }, 500);
    }

    function getProductImage()
    {
        /* jQuery("#theInitialProgress").css({"display": "block"});
         jQuery('.mainToolarea').css({
         'height': '1320px'
         });
         jQuery('.mainToolarea').css({
         'width': '1080px'
         });
         //setTimeout(function () {
         jQuery('#mainImageHolder img').css({
         'width': 'unset'
         });
         html2canvas(document.getElementsByClassName('mainToolarea'), {
         onrendered: function (can) {
         var data = can.toDataURL('image/png');
         var image = new Image();
         image.height = 1320;
         image.width = 1080;
         image.src = data;
         var myEl = angular.element(document.querySelector('#prev-holder'));
         myEl.append(image);
         setTimeout(function () {
         jQuery("#mainImageHolder img").css({"width": "100%"});
         // jQuery("#theInitialProgress").css({"display": "none"});
         }, 100)
         }
         });
         */
        if (sizeStdFlg)
        {
            sizeDataArr = getStdSizeData();
        } else {
            sizeDataArr = getScanSizeData();
        }


        console.log(sizeDataArr);
        //return false;
        document.getElementById("prev-holder").innerHTML = "";

        jQuery(".loaderText").css({"display": "none"});
        jQuery("#theInitialProgress").css({"display": "block"});
        jQuery(".view-thumb").hide();

        html2canvas(jQuery('.rtldf'), {
            onrendered: function (can) {
                var data = can.toDataURL('image/png');
                var image = new Image();
                image.src = data;
                image.height = 1200;//jQuery('#view1 img:first').css('height').replace(/[^-\d\.]/g, '');
                image.width = jQuery('#view1 img:first').css('width').replace(/[^-\d\.]/g, '');
                var myEl = angular.element(document.querySelector('#prev-holder'));
                myEl.append(image);

                saveDesign();

            }
        });
    }

    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : event.keyCode;
        console.log(charCode);
        if (charCode != 46 && charCode > 31
                && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }

    function getFabricPrice()
    {
        var fabPrice = jQuery("#main-container .fabvie.activeDesign .price").text();

        return fabPrice;
    }


    function updatePrice()
    {
        //basePrice

        console.log("in price")

        var totPrice = "0.0";

        var basePrice = jQuery("#basePrice").attr("orgBasePrice");
        //console.log("basePrice="+basePrice)
        //console.log("MonoFabricPrice="+parseFloat(getMonoFabricPrice()))

        totPrice = parseFloat(basePrice) + parseFloat(getStylePrice()) + parseFloat(getFabricPrice()) + parseFloat(getAccentStylePrice()) + parseFloat(getMonoFabricPrice());

        console.log("Baseprice " + parseFloat(basePrice) + " getFabricPrice " + parseFloat(getFabricPrice()) + " getStylePrice " + parseFloat(getStylePrice()) + " getAccentStylePrice " + parseFloat(getAccentStylePrice()) + " getMonoFabricPrice " + parseFloat(getMonoFabricPrice()));
        console.log("totPrice=" + totPrice);
        jQuery("#basePrice").text(totPrice.toFixed(2));

    }

    function getMonoFabricPrice()
    {
        var fabPrice;
        console.log(monotext)
        if (monotext)
        {
            fabPrice = parseFloat(jQuery("#divaccent_1 .accent-price").text()) ? parseFloat(jQuery("#divaccent_1 .accent-price").text().trim()) : "0.0"
        } else if (monotext == undefined) {
            fabPrice = '0.00';
        } else {
            fabPrice = '0.00';
        }

        return fabPrice;
    }

    function getStylePrice()
    {
        var stylePrice = 0.0;
        var a = jQuery("#div1 .box_options>div");

        for (var i = 1; i <= a.length; i++) {
            if (stylePriceArr.hasOwnProperty(i)) {
//            if (key !== "undefined") {
                stylePrice = parseFloat(stylePrice) + parseFloat(stylePriceArr[i]);
                //console.log(" stylePriceArr[" + i + "]= " + stylePriceArr[i] + " price = " + stylePrice)
                //}
            }
        }
        console.log('style  price -== ' + stylePrice)
        return stylePrice;


//    var stylePrice = 0.0;
//
//    for (var key in stylePriceArr) {
//        if (stylePriceArr.hasOwnProperty(key) ){
//        if( key!== "undefined")
//         {
//
//            stylePrice = parseFloat(stylePrice) + parseFloat(stylePriceArr[key]);
//            console.log("key = " + key + "price = " + stylePrice)
//        }
//    }
//}
//
//    return stylePrice;

    }



    function getAccentStylePrice()
    {
        var styleAccentPrice = '0.0';

        for (var key in styleAccentPriceArr) {
            if (styleAccentPriceArr.hasOwnProperty(key)) {
                styleAccentPrice = parseFloat(styleAccentPrice) + parseFloat(styleAccentPriceArr[key]);
            }
        }

        return styleAccentPrice;
    }

    jQuery("#plength,#thigh,#crotch,#phip,#pwaist,#length,#sleeve,#shoulder,#hip,#waist,#chest,#neck").keypress(function (e) {
        //if the letter is not digit then display error and don't type anything
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            //display error message
            //$("#errmsg").html("Digits Only").show().fadeOut("slow");
            return false;
        }
    });

});