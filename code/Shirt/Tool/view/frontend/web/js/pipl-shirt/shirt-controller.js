define(["angular", "jquery", "angular_route", "angular_animate"], function (angular, jQuery) {
    var style, currentId;
    var removeSpace, clrfilter;
    var jacketImage;
    var lapelImage;
    var basePath;
    var custom_ajax;
    var productId;
    var genderId;
    var fabricId = '1';
    var threadColor;
    var textFont, MonoPosition;
    var style_ID;
    var tempPj;
    var fontData;
    var currStyleName = "";

    var shirtFabricName = 'Woolpit White';
    var linid_active_id;
    var monotext;
//for fabric change 
    var tempJk;
    var tempPt;
    var tempVst;
    var tempAccnt;
    var currentThreadsId = "1";
//end
    var temp = {};
    var myFlgVal = true;
    var isMono = true;
    var active_view_Id;

    angular.module("Shirt").controller("shirtController", function ($scope, $filter, designService, viewService, fabricService, vestService, pantService, fabricAllDataService, suitAccentService, $http, measurementDataService) {//measurementDataService

        $scope.isActive = false;
        $scope.IsVisible = true;
        $scope.IsJacket = true;
        $scope.IsShirt = false;

        $scope.myClassForZoom = [];
        $scope.activeView = 1;
        $scope.IsImage = false;
        $scope.IsZoom = true;
        $scope.jacketView = false;
        $scope.myClassForZoom = [];

        productId = angular.element("#productId").val();
        genderId = angular.element("#genderId").val();
        basePath = angular.element("#basePath").val();
        fabricId = angular.element("#defaultFabricId").val();

        $scope.fabricId = '1';//fabricId;

        $scope.outercollarfabid = $scope.fabricId;
        $scope.innercollarfabid = $scope.fabricId;

        $scope.backelbofabId = $scope.fabricId;


        $scope.outercufffabid = $scope.fabricId;
        $scope.innerrcufffabid = $scope.fabricId;



        $scope.sleeveType = "long";
        $scope.actCatName = "";
        $scope.selStyleArr = [];
        $scope.contrastCollarStyle = "";

        $scope.actStyleJKArr = {};
        $scope.actStylePantArr = {};
        $scope.actStyleVestArr = {};
        $scope.actStyleData = {};
        //load views
        $scope.views = viewService.getSuitView();
        $scope.viewsZoom = viewService.getSuitViewZoom();



        $scope.activeProduct = productId; //suit
        $scope.activeModel = genderId; //male


        //mesuarement data
        $scope.measureFlg = true;
        $scope.scanSizeFlg = false;
        $scope.stdSizeFlg = false;
        $scope.unitVal = 1;
        $scope.unitName = "in";
        $scope.unitScanName = "in";
        $scope.jacketSizes = measurementDataService.getJacketSizes();
        //$scope.pantSizes = measurementDataService.getPantSizes();

        //angular.element("#div5").find("ul").children().eq(7).hide();


        //add more fit in standard size
        $scope.addMoreFit = function () {
            jQuery("#fit-holder").append('<div class="row offset-top-10"><label class="col-xs-3 control-label" for="inputPassword3"> <i class="fa fa-arrow-circle-right text-success"> </i>  Select Your Fit </label><div class="col-xs-3"><select class="form-control fit-select-box"><option value="-">Select</option><option value="S">S</option><option value="M">M</option><option value="L">L</option><option value="XL">XL</option><option value="XXL">XXL</option><option value="3XL">3XL</option><option value="4XL">4XL</option></select></div><label class="col-xs-2 control-label" for="inputPassword3">  Quantity </label><div class="col-xs-2"><select class="form-control"><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option></select></div><div class="col-xs-2" onclick="removeFit(this);" ><a class="btn btn-primary btn-xs" href="javascript:void(0);" > <span class="glyphicon glyphicon-minus"></span></a></div></div>');
        }

        $scope.showScanSize = function () {
            console.log("in scan..")
            $scope.scanSizeFlg = true;
            $scope.stdSizeFlg = false;
            $scope.measureFlg = false;
        }

        $scope.showStdSize = function () {

            console.log("in std..")
            $scope.scanSizeFlg = false;
            $scope.stdSizeFlg = true;
            $scope.measureFlg = false;
        }
//change Unit
        $scope.changeUnit = function (val) {
            if (val == "in") {
                $scope.unitVal = 1;
            } else {
                $scope.unitVal = 2.54;
            }
        }
// change unit scan
        $scope.changeScanUnit = function (unitVal) {
            $scope.unitScanName = unitVal;
            var convertVal = 0;
            if (unitVal == "cm") {
                convertVal = 2.54;
            } else {
                convertVal = 0.393701;
            }

            jQuery(".scan_number").each(function (e) {
                if (jQuery(this).val() != "") {
                    var res = parseFloat(parseFloat(jQuery(this).val()) * convertVal).toFixed(2);
                    jQuery(this).val(res);
                }
            });
        }
//mesuarement data end





        $scope.addClassOfZoom = function () {

            $scope.myClassForZoom.push('zoomOut');
            $scope.IsVisible = false;
        }

        $scope.hideFabricDiv = function () {
            $scope.IsImage = false;
        }

        $scope.showFabricDiv = function () {


            $scope.IsImage = true;

        }

        $scope.shirtVisiblefalse = function (obj) {

            $scope.IsShirt = true;
            hideJacket();
        }
        $scope.pantVisiblefalse = function (obj) {

            $scope.IsPant = true;
            hidePant();
        }
        $scope.pantVisibletrue = function () {
            //formal

            $scope.IsPant = false;
            hidePant();
        }
        $scope.tieVisiblefalse = function (obj) {

            $scope.IsTie = false;
            hideTie();
        }
        $scope.tieVisibletrue = function () {


            $scope.IsTie = true;
            hideTie();
        }
        $scope.shirtVisibletrue = function () {


            $scope.IsShirt = false;
            hideJacket();
        }
        function hidePant()
        {

            if ($scope.IsPant)
            {   // jeans
                if ($scope.IsShirt)//is  casual look
                {
                    angular.element("#mainImageHolder").find('.shirt_part[rel="ManPant"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/bottom/Jeans/Shirt_Bottom_1_1.png");
                    angular.element("#mainImageHolder").find('.shirt_part[rel="ManPantSide"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/bottom/Jeans/Shirt_Bottom_2_1.png");
                    angular.element("#mainImageHolder").find('.shirt_part[rel="ManPantBack"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/bottom/Jeans/Shirt_Bottom_3_1.png");


                    //                
//                jQuery("#mainImageHolder").find("[rel=ManPant]").hide();
//                jQuery("#mainImageHolder").find("[rel=ManPantSide]").hide();
//                jQuery("#mainImageHolder").find("[rel=ManPantBack]").hide();

                } else {
                    angular.element("#mainImageHolder").find('.shirt_part[rel="ManPant"]').attr("src", basePath + "pub/media/shirt-tool/default_shirttool_image/Jeans_Front.png");
                    angular.element("#mainImageHolder").find('.shirt_part[rel="ManPantSide"]').attr("src", basePath + "pub/media/shirt-tool/default_shirttool_image/Jeans_Side.png");
                    angular.element("#mainImageHolder").find('.shirt_part[rel="ManPantBack"]').attr("src", basePath + "pub/media/shirt-tool/default_shirttool_image/Jeans_Back.png");
//                angular.element("#mainImageHolder").find('.shirt_part[rel="ManPant"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/bottom/Shirt_Bottom_1_1.png");
//                angular.element("#mainImageHolder").find('.shirt_part[rel="ManPantSide"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/bottom/Shirt_Bottom_2_1.png");
//                angular.element("#mainImageHolder").find('.shirt_part[rel="ManPantBack"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/bottom/Shirt_Bottom_3_1.png");



                }
                $scope.IsJeans = true;
                var styleID = angular.element("#divs_8").find(".activeDesign").attr("id");
                var bot = angular.element("#div1").find("ul").children().eq(7).hasClass("activeDesign");
                //jQuery("#mainImageHolder").find("[rel=ManPant]").show();
                if (bot) {
                    if (styleID == '1')
                    {
                        angular.element("#mainImageHolder").find('.shirt_part[rel="Bottom"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/bottom/Jeans/Shirt_Bottom_1_1.png");
                        angular.element("#mainImageHolder").find('.shirt_part[rel="BottomSide"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/bottom/Jeans/Shirt_Bottom_2_1.png");
                        angular.element("#mainImageHolder").find('.shirt_part[rel="BottomBack"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/bottom/Jeans/Shirt_Bottom_3_1.png");

                        angular.element("#mainImageHolder").find("[rel=CasualLook]").hide();
                        angular.element("#mainImageHolder").find("[rel=ManPant]").hide();
                        angular.element("#mainImageHolder").find("[rel=ManPantSide]").hide();
                        angular.element("#mainImageHolder").find("[rel=ManPantBack]").hide();
                        angular.element("#mainImageHolder").find("[rel=Bottom]").show();
                        angular.element("#mainImageHolder").find("[rel=BottomSide]").show();
                        angular.element("#mainImageHolder").find("[rel=BottomBack]").show();

                    } else if (styleID == '2') {
                        angular.element("#mainImageHolder").find('.shirt_part[rel="Bottom"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/bottom/Jeans/Shirt_Bottom_1_2.png");
                        angular.element("#mainImageHolder").find('.shirt_part[rel="BottomSide"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/bottom/Jeans/Shirt_Bottom_2_2.png");
                        angular.element("#mainImageHolder").find('.shirt_part[rel="BottomBack"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/bottom/Jeans/Shirt_Bottom_3_2.png");

                        angular.element("#mainImageHolder").find("[rel=CasualLook]").hide();
                        angular.element("#mainImageHolder").find("[rel=ManPant]").hide();
                        angular.element("#mainImageHolder").find("[rel=ManPantSide]").hide();
                        angular.element("#mainImageHolder").find("[rel=ManPantBack]").hide();
                        angular.element("#mainImageHolder").find("[rel=Bottom]").show();
                        angular.element("#mainImageHolder").find("[rel=BottomSide]").show();
                        angular.element("#mainImageHolder").find("[rel=BottomBack]").show();

                    }
                }
//
//            angular.element("#mainImageHolder").find("[rel=ManPant]").show();
//            angular.element("#mainImageHolder").find("[rel=ManPantSide]").show();
//            angular.element("#mainImageHolder").find("[rel=ManPantBack]").show();




            } else {
                console.log("Formal pant")
                //formal pant
                if ($scope.IsShirt)// casual look
                {

                    angular.element("#mainImageHolder").find('.shirt_part[rel="ManPant"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/bottom/Shirt_Bottom_1_1.png");
                    angular.element("#mainImageHolder").find('.shirt_part[rel="ManPantSide"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/bottom/Shirt_Bottom_2_1.png");
                    angular.element("#mainImageHolder").find('.shirt_part[rel="ManPantBack"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/bottom/Shirt_Bottom_3_1.png");


                } else {
                    angular.element("#mainImageHolder").find('.shirt_part[rel="ManPant"]').attr("src", basePath + "pub/media/shirt-tool/default_shirttool_image/Pant_front.png");
                    angular.element("#mainImageHolder").find('.shirt_part[rel="ManPantSide"]').attr("src", basePath + "pub/media/shirt-tool/default_shirttool_image/Pant_side.png");
                    angular.element("#mainImageHolder").find('.shirt_part[rel="ManPantBack"]').attr("src", basePath + "pub/media/shirt-tool/default_shirttool_image/Pant_back.png");



                }


                $scope.IsJeans = false;
                var styleID = angular.element("#divs_8").find(".activeDesign").attr("id");
                var bot = angular.element("#div1").find("ul").children().eq(7).hasClass("activeDesign");
                if (bot) {
                    if (styleID == '1')
                    {
                        angular.element("#mainImageHolder").find('.shirt_part[rel="Bottom"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/bottom/Shirt_Bottom_1_1.png");
                        angular.element("#mainImageHolder").find('.shirt_part[rel="BottomSide"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/bottom/Shirt_Bottom_2_1.png");
                        angular.element("#mainImageHolder").find('.shirt_part[rel="BottomBack"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/bottom/Shirt_Bottom_3_1.png");

                        angular.element("#mainImageHolder").find("[rel=CasualLook]").hide();
                        angular.element("#mainImageHolder").find("[rel=ManPant]").hide();
                        angular.element("#mainImageHolder").find("[rel=ManPantSide]").hide();
                        angular.element("#mainImageHolder").find("[rel=ManPantBack]").hide();
                        angular.element("#mainImageHolder").find("[rel=Bottom]").show();
                        angular.element("#mainImageHolder").find("[rel=BottomSide]").show();
                        angular.element("#mainImageHolder").find("[rel=BottomBack]").show();

                    } else if (styleID == '2') {
                        angular.element("#mainImageHolder").find('.shirt_part[rel="Bottom"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/bottom/Shirt_Bottom_1_2.png");
                        angular.element("#mainImageHolder").find('.shirt_part[rel="BottomSide"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/bottom/Shirt_Bottom_2_2.png");
                        angular.element("#mainImageHolder").find('.shirt_part[rel="BottomBack"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/bottom/Shirt_Bottom_3_2.png");

                        angular.element("#mainImageHolder").find("[rel=CasualLook]").hide();
                        angular.element("#mainImageHolder").find("[rel=ManPant]").hide();
                        angular.element("#mainImageHolder").find("[rel=ManPantSide]").hide();
                        angular.element("#mainImageHolder").find("[rel=ManPantBack]").hide();
                        angular.element("#mainImageHolder").find("[rel=Bottom]").show();
                        angular.element("#mainImageHolder").find("[rel=BottomSide]").show();
                        angular.element("#mainImageHolder").find("[rel=BottomBack]").show();
                    }
                }
            }
        }

        function hideTie()
        {

            if ($scope.IsTie) {
                angular.element("#mainImageHolder").find('.shirt_part[rel="ManTie"]').show();
                angular.element("#mainImageHolder").find('.shirt_part[rel="ManTieSide"]').show();
                angular.element("#casual_1").hide();
                angular.element("#casual_2").hide();
                //jQuery("#mainImageHolder").find("[rel=ManPant]").show();
            } else {
                angular.element("#mainImageHolder").find('.shirt_part[rel="ManTie"]').hide();
                angular.element("#mainImageHolder").find('.shirt_part[rel="ManTieSide"]').hide();
                angular.element("#casual_1").show();
                angular.element("#casual_2").show();
            }
        }

        function hideJacket()
        {

            if ($scope.IsShirt)
            {
                console.log("IsShirt if part");
                //casual look
                jQuery("#mainImageHolder").find("[rel=CollarBase]").hide();

                jQuery("#mainImageHolder").find("[rel=CollarBaseSide]").hide();

                jQuery("#mainImageHolder").find("[rel=CollarStyle]").hide();
                jQuery("#mainImageHolder").find("[rel=Pin]").hide();
                jQuery("#mainImageHolder").find("[rel=PinSide]").hide();
                // jQuery("#mainImageHolder").find("[rel=PinFold]").hide();
                // jQuery("#mainImageHolder").find("[rel=PinZoom]").hide();




                angular.element("#mainImageHolder").find('.shirt_part[rel=BottonDown]').hide();
                angular.element("#mainImageHolder").find('.shirt_part[rel=BottonDownSide]').hide();
                angular.element("#mainImageHolder").find('.shirt_part[rel=BottonDownFold]').hide();
                angular.element("#viewport").find('.shirt_part[rel=BottondownZoom]').hide();

                jQuery("#mainImageHolder").find("[rel=CollarStyleSide]").hide();

                jQuery("#mainImageHolder").find("[rel=CasualLook]").show();
                jQuery("#mainImageHolder").find("[rel=CasualLookSide]").show();
                jQuery("#mainImageHolder").find("[rel=CasualLookBack]").show();
                jQuery("#mainImageHolder").find("[rel=backpantfit]").hide();
                jQuery("#mainImageHolder").find("[rel=pantfit]").hide();
                jQuery("#mainImageHolder").find("[rel=PantBackStyle]").hide();
                jQuery("#mainImageHolder").find("[rel=Style]").show();
                jQuery("#mainImageHolder").find("[rel=buttons]").show();
                jQuery("#mainImageHolder").find("[rel=SingleButton]").hide();
                jQuery("#mainImageHolder").find("[rel=SingleButtonSide]").hide();
                jQuery("#mainImageHolder").find("[rel=SingleButtonHole]").hide();
                jQuery("#mainImageHolder").find("[rel=SingleButtonHoleSide]").hide();

                jQuery("#mainImageHolder").find("[rel=casualCollar]").show();
                jQuery("#mainImageHolder").find("[rel=casualBase]").show();
                jQuery("#mainImageHolder").find("[rel=casualMan]").show();
                jQuery("#mainImageHolder").find("[rel=casualWomanBackCollar]").show();
                jQuery("#mainImageHolder").find("[rel=casualCollarSide]").show();
                jQuery("#mainImageHolder").find("[rel=casualBaseSide]").show();
                jQuery("#mainImageHolder").find("[rel=casualManSide]").show();
                jQuery("#mainImageHolder").find("[rel=casualCollarSide]").show();
                // casualInnerCollar
                angular.element("#mainImageHolder").find('.shirt_part[rel=casualMan]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/collarstyle/back_collar/" + $scope.fabricId + "_style_1_casual_left_sidepart_view_1.png");

                angular.element("#mainImageHolder").find('.shirt_part[rel=WomanBackPartSide]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/collarstyle/back_collar/" + $scope.fabricId + "_style_1_left_sidepart_view_2.png");

                jQuery("#mainImageHolder").find("[rel=casualInnerCollar]").show();
                jQuery("#mainImageHolder").find("[rel=casualInnerCollarSide]").show();

                jQuery("#mainImageHolder").find("[rel=casualInnerPlackets]").show();
                jQuery("#mainImageHolder").find("[rel=casualInnerPlacketsSide]").show();
                jQuery("#mainImageHolder").find("[rel=casualPlacket]").show();
                jQuery("#mainImageHolder").find("[rel=casualPlacketSide]").show();
                jQuery("#mainImageHolder").find("[rel=WomanBackPartSide]").show();


                if ($scope.IsJeans)
                {

                    angular.element("#mainImageHolder").find('.shirt_part[rel="ManPant"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/bottom/Jeans/Shirt_Bottom_1_1.png");
                    angular.element("#mainImageHolder").find('.shirt_part[rel="ManPantSide"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/bottom/Jeans/Shirt_Bottom_2_1.png");
                    angular.element("#mainImageHolder").find('.shirt_part[rel="ManPantBack"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/bottom/Jeans/Shirt_Bottom_3_1.png");

                    angular.element("#mainImageHolder").find("[rel=Bottom]").hide();
                    angular.element("#mainImageHolder").find("[rel=BottomSide]").hide();
                    angular.element("#mainImageHolder").find("[rel=BottomBack]").hide();
                    jQuery("#mainImageHolder").find("[rel=ManPant]").show();
                    jQuery("#mainImageHolder").find("[rel=ManPantSide]").show();
                    jQuery("#mainImageHolder").find("[rel=ManPantBack]").show();

                } else {
                    angular.element("#mainImageHolder").find('.shirt_part[rel="ManPant"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/bottom/Shirt_Bottom_1_1.png");
                    angular.element("#mainImageHolder").find('.shirt_part[rel="ManPantSide"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/bottom/Shirt_Bottom_2_1.png");
                    angular.element("#mainImageHolder").find('.shirt_part[rel="ManPantBack"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/bottom/Shirt_Bottom_3_1.png");

                    angular.element("#mainImageHolder").find("[rel=Bottom]").hide();
                    angular.element("#mainImageHolder").find("[rel=BottomSide]").hide();
                    angular.element("#mainImageHolder").find("[rel=BottomBack]").hide();
                    jQuery("#mainImageHolder").find("[rel=ManPant]").show();
                    jQuery("#mainImageHolder").find("[rel=ManPantSide]").show();
                    jQuery("#mainImageHolder").find("[rel=ManPantBack]").show();


                }

                var styleID = angular.element("#divs_1").find(".activeDesign").attr("id");

                if (styleID == '6') {

                    angular.element("#mainImageHolder").find('.shirt_part[rel=Pin]').hide();
                    angular.element("#mainImageHolder").find('.shirt_part[rel=PinSide]').hide();
                    // angular.element("#mainImageHolder").find('.shirt_part[rel=PinFold]').hide();
                    // angular.element("#viewport").find('.shirt_part[rel=PinZoom]').hide();



                } else if (styleID == '2') {
                    angular.element("#mainImageHolder").find('.shirt_part[rel=ButtonDown]').hide();
                    angular.element("#mainImageHolder").find('.shirt_part[rel=ButtonDownSide]').hide();
                    // angular.element("#mainImageHolder").find('.shirt_part[rel=ButtonDownFold]').hide()
                    // angular.element("#viewport").find('.shirt_part[rel=ButtondownZoom]').hide();

                    // angular.element("#viewport").find('.shirt_part[rel=ButtondownThreadZoom]').hide();
                    // angular.element("#viewport").find('.shirt_part[rel=ButtondownHoleZoom]').hide();
                    // angular.element("#mainImageHolder").find('.shirt_part[rel=ButtonDownThreadFold]').hide();

                } else {
                    angular.element("#mainImageHolder").find('.shirt_part[rel=Pin]').hide();
                    angular.element("#mainImageHolder").find('.shirt_part[rel=PinSide]').hide();
                    angular.element("#mainImageHolder").find('.shirt_part[rel=PinFold]').hide();
                    angular.element("#viewport").find('.shirt_part[rel=PinZoom]').hide();


                    angular.element("#mainImageHolder").find('.shirt_part[rel=ButtonDown]').hide();
                    angular.element("#mainImageHolder").find('.shirt_part[rel=ButtonDownSide]').hide();
                    angular.element("#mainImageHolder").find('.shirt_part[rel=ButtonDownFold]').hide();
                    angular.element("#viewport").find('.shirt_part[rel=ButtondownZoom]').hide();

                    angular.element("#viewport").find('.shirt_part[rel=ButtondownThreadZoom]').hide();
                    angular.element("#viewport").find('.shirt_part[rel=ButtondownHoleZoom]').hide();
                    angular.element("#mainImageHolder").find('.shirt_part[rel=ButtonDownThreadFold]').hide();
                }

                //show()

                jQuery("#mainImageHolder").find("[rel=ManTie]").hide();
                jQuery("#mainImageHolder").find("[rel=ManTieSide]").hide();
                jQuery("#tie").hide();

            } else
            {
//front view
                //console.log("IsShirt else part");
                //formal look
                jQuery("#mainImageHolder").find("[rel=CasualLook]").hide();
                jQuery("#mainImageHolder").find("[rel=CasualLookSide]").hide();
                jQuery("#mainImageHolder").find("[rel=CasualLookBack]").hide();
                jQuery("#mainImageHolder").find("[rel=backpantfit]").show();
                jQuery("#mainImageHolder").find("[rel=PantBackStyle]").show();
                jQuery("#mainImageHolder").find("[rel=Bottom]").hide();
                jQuery("#mainImageHolder").find("[rel=pantfit]").show();
                jQuery("#mainImageHolder").find("[rel=buttons]").hide();
                jQuery("#mainImageHolder").find("[rel=casualBase]").hide();
                jQuery("#mainImageHolder").find("[rel=casualMan]").hide();
                jQuery("#mainImageHolder").find("[rel=casualWomanBackCollar]").hide();
                jQuery("#mainImageHolder").find("[rel=casualCollar]").hide();
                jQuery("#mainImageHolder").find("[rel=casualCollarSide]").hide();
                jQuery("#mainImageHolder").find("[rel=casualBaseSide]").hide();
                //jQuery("#mainImageHolder").find("[rel=casualInnerCollarSide]").hide();
                jQuery("#mainImageHolder").find("[rel=casualManSide]").hide();
                jQuery("#mainImageHolder").find("[rel=casualCollarSide]").hide();

                jQuery("#mainImageHolder").find("[rel=WomanBackPartSide]").hide();

                jQuery("#mainImageHolder").find("[rel=CollarBase]").show();
                jQuery("#mainImageHolder").find("[rel=CollarBaseSide]").show();
                jQuery("#mainImageHolder").find("[rel=CollarStyle]").show();
                jQuery("#mainImageHolder").find("[rel=CollarStyleSide]").show();

                if ($scope.IsJeans)
                {
                    angular.element("#mainImageHolder").find('.shirt_part[rel="ManPant"]').attr("src", basePath + "pub/media/shirt-tool/default_shirttool_image/Jeans_Front.png");
                    angular.element("#mainImageHolder").find('.shirt_part[rel="ManPantSide"]').attr("src", basePath + "pub/media/shirt-tool/default_shirttool_image/Jeans_Side.png");
                    angular.element("#mainImageHolder").find('.shirt_part[rel="ManPantBack"]').attr("src", basePath + "pub/media/shirt-tool/default_shirttool_image/Jeans_Back.png");


                } else
                {
                    angular.element("#mainImageHolder").find('.shirt_part[rel="ManPant"]').attr("src", basePath + "pub/media/shirt-tool/default_shirttool_image/Pant_front.png");
                    angular.element("#mainImageHolder").find('.shirt_part[rel="ManPantSide"]').attr("src", basePath + "pub/media/shirt-tool/default_shirttool_image/Pant_side.png");
                    angular.element("#mainImageHolder").find('.shirt_part[rel="ManPantBack"]').attr("src", basePath + "pub/media/shirt-tool/default_shirttool_image/Pant_back.png");

                }
                angular.element("#mainImageHolder").find("[rel=Bottom]").hide();
                angular.element("#mainImageHolder").find("[rel=BottomSide]").hide();
                angular.element("#mainImageHolder").find("[rel=BottomBack]").hide();

                angular.element("#mainImageHolder").find("[rel=ManPant]").show();
                angular.element("#mainImageHolder").find("[rel=ManPantSide]").show();
                angular.element("#mainImageHolder").find("[rel=ManPantBack]").show();

                var styleID = angular.element("#divs_1").find(".activeDesign").attr("id");

                if (styleID == '6') {

                    angular.element("#mainImageHolder").find('.shirt_part[rel=Pin]').show();
                    angular.element("#mainImageHolder").find('.shirt_part[rel=PinSide]').show();
                    angular.element("#mainImageHolder").find('.shirt_part[rel=PinFold]').show();
                    angular.element("#viewport").find('.shirt_part[rel=PinZoom]').show();

                    angular.element("#viewport").find('.shirt_part[rel=ButtondownZoom]').hide();
                    angular.element("#mainImageHolder").find('.shirt_part[rel=ButtonDown]').hide();
                    angular.element("#mainImageHolder").find('.shirt_part[rel=ButtonDownSide]').hide();
                    angular.element("#mainImageHolder").find('.shirt_part[rel=ButtonDownFold]').hide();
                    angular.element("#viewport").find('.shirt_part[rel=ButtondownThreadZoom]').hide();
                    angular.element("#viewport").find('.shirt_part[rel=ButtondownHoleZoom]').hide();
                    angular.element("#mainImageHolder").find('.shirt_part[rel=ButtonDownThreadFold]').hide();



                } else if (styleID == '2') {
                    angular.element("#mainImageHolder").find('.shirt_part[rel=ButtonDown]').show();
                    angular.element("#mainImageHolder").find('.shirt_part[rel=ButtonDownSide]').show();
                    angular.element("#mainImageHolder").find('.shirt_part[rel=ButtonDownFold]').show()
                    angular.element("#viewport").find('.shirt_part[rel=ButtondownZoom]').show();
                    angular.element("#viewport").find('.shirt_part[rel=ButtondownThreadZoom]').show();
                    angular.element("#viewport").find('.shirt_part[rel=ButtondownHoleZoom]').show();
                    angular.element("#mainImageHolder").find('.shirt_part[rel=ButtonDownThreadFold]').show();

                    angular.element("#mainImageHolder").find('.shirt_part[rel=Pin]').hide();
                    angular.element("#mainImageHolder").find('.shirt_part[rel=PinSide]').hide();
                    angular.element("#mainImageHolder").find('.shirt_part[rel=PinFold]').hide();
                    angular.element("#viewport").find('.shirt_part[rel=PinZoom]').hide();

                } else {
                    angular.element("#mainImageHolder").find('.shirt_part[rel=Pin]').hide();
                    angular.element("#mainImageHolder").find('.shirt_part[rel=PinSide]').hide();
                    angular.element("#mainImageHolder").find('.shirt_part[rel=PinFold]').hide();
                    angular.element("#viewport").find('.shirt_part[rel=PinZoom]').hide();

                    angular.element("#viewport").find('.shirt_part[rel=ButtondownZoom]').hide();
                    angular.element("#mainImageHolder").find('.shirt_part[rel=ButtonDown]').hide();
                    angular.element("#mainImageHolder").find('.shirt_part[rel=ButtonDownSide]').hide();
                    angular.element("#mainImageHolder").find('.shirt_part[rel=ButtonDownFold]').hide();
                    angular.element("#viewport").find('.shirt_part[rel=ButtondownThreadZoom]').hide();
                    angular.element("#viewport").find('.shirt_part[rel=ButtondownHoleZoom]').hide();
                    angular.element("#mainImageHolder").find('.shirt_part[rel=ButtonDownThreadFold]').hide();
                }
                jQuery("#mainImageHolder").find("[rel=SingleButton]").show();
                jQuery("#mainImageHolder").find("[rel=SingleButtonSide]").show();
                jQuery("#mainImageHolder").find("[rel=SingleButtonHole]").show();
                jQuery("#mainImageHolder").find("[rel=SingleButtonHoleSide]").show();

                jQuery("#mainImageHolder").find("[rel=casualInnerCollar]").hide();
                jQuery("#mainImageHolder").find("[rel=casualInnerCollarSide]").hide();

                jQuery("#mainImageHolder").find("[rel=casualInnerPlackets]").hide();
                jQuery("#mainImageHolder").find("[rel=casualInnerPlacketsSide]").hide();
                jQuery("#mainImageHolder").find("[rel=casualPlacket]").hide();
                jQuery("#mainImageHolder").find("[rel=casualPlacketSide]").hide();

                if (styleID == "8" || styleID == "10")
                {
                    jQuery("#mainImageHolder").find("[rel=CollarStyle]").hide();
                    jQuery("#mainImageHolder").find("[rel=CollarStyleSide]").hide();
                    jQuery("#mainImageHolder").find("[rel=ManTie]").hide();
                    jQuery("#mainImageHolder").find("[rel=ManTieSide]").hide();
                    jQuery("#tie").hide();
                    jQuery("#tie").hide();
                } else {
//                
                    jQuery("#tie").show();
                    jQuery("#tie").show();
                }
//            if (styleID == "8" || styleID == "10")
//            {
//                jQuery("#tie").hide()
//            } else {
//                jQuery("#tie").show();
//            }
            }

        }


        function zoomOutProdImage()
        {
            angular.element("#mainImageHolder .shirt_part[rel=Monogram]").hide();
            $scope.IsVisible = true;
            $scope.isActive = false;

            if ($scope.myClassForZoom[0] == "zoomOut")
            {
                console.log("ifff")
                $scope.myClassForZoom.pop('zoomOut');
            } else {
                console.log("else")
                angular.element("#mainImageHolder .shirt_part[rel=Monogram]").hide();
                jQuery("#theInitialProgress").css({"display": "block"});
                jQuery(".container-fluid").css({"opacity": "0.3"});
                jQuery(".mainToolarea").css({"height": "1320px"});
                jQuery(".mainToolarea").css({"width": "1080px"});
                document.getElementById("prev-holder").innerHTML = "";
                jQuery("#zoomOfTool").modal("show");
                setTimeout(function () {

                    jQuery("#mainImageHolder img").css({"width": "100%"});
                    html2canvas(document.getElementsByClassName("mainToolarea"), {
                        onrendered: function (can) {

                            var data = can.toDataURL('image/png');
                            var image = new Image();
//                        image.height = 1320;
//                        image.width = 1080;
                            image.src = data;
                            var myEl = angular.element(document.querySelector('#prev-holder'));
                            myEl.append(image);
                            jQuery("#mainImageHolder img").css({"width": "100%"});
                            jQuery("#theInitialProgress").css({"display": "none"});
                            if (jQuery(window).width() > 767) {

//                            jQuery("#prev-holder img").ezPlus({
//                                type: "other",
//                                cursor: "crosshair",
//                                scrollZoom: true,
//                                zIndex: 999999
//                            });


                            }


                        }
                    });
                }, 500);
            }
        }

        $scope.takeScreenShot = function () {

            zoomOutProdImage();
        }
        $scope.leftNavFabCat = function ($event, actCatName)
        {
            jQuery(".leftpanelsection .sidebar-options").hide();
            jQuery("#divOptionFilter").hide();
            jQuery("#div4").show();
            console.log("actCatName = " + actCatName);
            angular.element($event.currentTarget).parent().find(".activeDesign").removeClass("activeDesign")
            angular.element($event.currentTarget).addClass("activeDesign");
            angular.element('#fabircCatName').html(actCatName + " Fabrics");
            $scope.searchFabric = actCatName;
            $scope.fabricData.fabric = [];
            console.log(tempPj.fabric);
            if ($scope.searchFabric == "All" || $scope.searchFabric == "ALL" || $scope.searchFabric == "all")
            {

                for (var data in tempPj.fabric) {
                    $scope.fabricData.fabric.push(tempPj.fabric[data]);
                }

            } else {

                for (var data in tempPj.fabric) {
                    if (tempPj.fabric[data].material_parent == $scope.searchFabric || tempPj.fabric[data].pattern_parent == $scope.searchFabric || tempPj.fabric[data].season_parent == $scope.searchFabric || tempPj.fabric[data].color_parent == $scope.searchFabric || tempPj.fabric[data].category_parent == $scope.searchFabric) {
                        $scope.fabricData.fabric.push(tempPj.fabric[data]);
                    }
                }
            }

//init fabirc zoom
            jQuery(".fabvie img").elevateZoom();
        }


        $scope.leftNavFabCatMulti = function ($event)
        {
            angular.element($event.currentTarget).parent().find(".activeDesign").removeClass("activeDesign")
            angular.element($event.currentTarget).addClass("activeDesign");
            jQuery(".leftpanelsection .sidebar-options").hide();
            //leftMainNav_fabricCat
            setTimeout(function () {
                jQuery("#divOptionMulti").show();
            }, 50);
        }

        $scope.leftNavFabCatInfo = function ($event)
        {
            angular.element($event.currentTarget).parent().find(".activeDesign").removeClass("activeDesign")
            angular.element($event.currentTarget).addClass("activeDesign");
            jQuery(".leftpanelsection .sidebar-options").hide();
            //leftMainNav_fabricCat
            setTimeout(function () {
                jQuery("#divOptionInfo").show();
            }, 50);
        }

        $scope.changeSameDifferentFabric = function (part)
        {
            jQuery(".leftpanelsection .sidebar-options").hide();
            jQuery("#div4").show();
            if (part == 'same')
            {
                jQuery("#divOptionDifferentParts").hide();
                $scope.partOption = 'all';
            } else {
                jQuery("#divOptionDifferentParts").show();
                $scope.partOption = 'jacket';
            }


        }

        //filter start


        $scope.applyFilter = function ()
        {
            jQuery(".leftpanelsection .sidebar-options").hide();
            jQuery("#div4").show();
            if ($scope.test == "" && $scope.filteredFabrics.length == $scope.fabrics.length) {
                jQuery("#clearfilterfab").hide();
            } else
            if ($scope.filteredFabrics.length == $scope.fabrics.length && $scope.test == undefined) {
                jQuery("#clearfilterfab").hide();
            } else {
                jQuery("#clearfilterfab").show();
            }

//        setTimeout(function () {
//            jQuery('.back-btn').trigger('click');
//        }, 200)

            console.log("In applyFilter")
            clrfilter = true;
            jQuery('body').addClass('open-fabric');
            nullfabricTxt();

        }
        function clearFilterActive() {

//        var clrfilter=jQuery("#div4").find(".active");
            if (clrfilter) {

                jQuery("#clearfilterfab").show();
            } else {
                jQuery("#clearfilterfab").hide();
            }
        }

        function nullfabricTxt() {

            console.log($scope.fabricData.fabric)

            if (jQuery(".fabricGene").children().length) {
                jQuery("#nullTxt").hide()
            } else {
                jQuery("#nullTxt").show()
            }

        }

        $scope.search = {};
        $scope.searchMat = {};
        $scope.searchSea = {};
        $scope.searchPat = {};
        $scope.searchCol = {};

        $scope.resetCheckBoxFilter = function ($event) {

            console.log("keyy" + jQuery('#txt_name').val())

            $scope.searchFabricName = jQuery('#txt_name').val();
            var fabricFilterData = tempPj.fabric;
            var data = $filter('filter')(fabricFilterData, {name: $scope.searchFabricName});
            $scope.fabricData.fabric = data;

            //filter disable on input
            $scope.useMaterials = {};
            $scope.usePatterns = {};
            $scope.useSeasons = {};
            $scope.useColors = {};
            $scope.useCategory = {};
            //filter end

            if ($event.keyCode == 13) {
                $scope.applyFilter();
            }

        };


//old filter end


///new filter fabric code start

///fabric filter code start


        $scope.useMaterials = {};
        $scope.usePatterns = {};
        $scope.useSeasons = {};
        $scope.useColors = {};
        $scope.useCategory = {};
// Watch the pants that are selected

        $scope.$watch(function () {
            console.log("in watch")

            return {
                fabrics: $scope.fabrics,
                useMaterials: $scope.useMaterials,
                usePatterns: $scope.usePatterns,
                useSeasons: $scope.useSeasons,
                useColors: $scope.useColors,
                useCategory: $scope.useCategory
            }
        }, function (value) {
            var selected;

            $scope.count = function (prop, value) {
                return function (el) {
                    return el[prop] == value;
                };
            };


            var filterAfterMaterials = [];
            selected = false;
            for (var j in $scope.fabrics) {
                var p = $scope.fabrics[j];
                for (var i in $scope.useMaterials) {
                    if ($scope.useMaterials[i]) {
                        selected = true;
                        if (i == p.material_parent) {
                            filterAfterMaterials.push(p);
                            break;
                        }
                    }
                }
            }
            if (!selected) {
                filterAfterMaterials = $scope.fabrics;
            }



            var filterAfterPatterns = [];
            selected = false;
            for (var j in filterAfterMaterials) {
                var p = filterAfterMaterials[j];
                for (var i in $scope.usePatterns) {
                    console.log("i=" + i)
                    if ($scope.usePatterns[i]) {
                        selected = true;
                        console.log("p.pattern_parent=" + p.pattern_parent)
                        if (i == p.pattern_parent) {
                            filterAfterPatterns.push(p);
                            break;
                        }
                    }
                }
            }
            if (!selected) {
                filterAfterPatterns = filterAfterMaterials;
            }
            //season
            var filterAfterSeasons = [];
            selected = false;
            for (var j in filterAfterPatterns) {
                var p = filterAfterPatterns[j];
                for (var i in $scope.useSeasons) {
                    console.log("i=" + i)
                    if ($scope.useSeasons[i]) {
                        selected = true;
                        console.log("p.season_parent=" + p.season_parent)
                        if (i == p.season_parent) {

                            filterAfterSeasons.push(p);
                            break;
                        }
                    }
                }
            }
            if (!selected) {
                filterAfterSeasons = filterAfterPatterns;
            }
            //season end

            var filterAfterColors = [];
            selected = false;
            for (var j in filterAfterSeasons) {
                var p = filterAfterSeasons[j];
                for (var i in $scope.useColors) {
                    if ($scope.useColors[i]) {
                        selected = true;
                        if (i == p.color_parent) {
                            filterAfterColors.push(p);
                            break;
                        }
                    }
                }
            }
            if (!selected) {
                filterAfterColors = filterAfterSeasons;
            }


            var filterAfterCategory = [];
            selected = false;
            for (var j in filterAfterColors) {
                var p = filterAfterColors[j];
                for (var i in $scope.useCategory) {
                    if ($scope.useCategory[i]) {
                        selected = true;
                        if (i == p.category_parent) {
                            filterAfterCategory.push(p);
                            break;
                        }
                    }
                }
            }
            if (!selected) {
                filterAfterCategory = filterAfterColors;
            }

            $scope.filteredFabrics = filterAfterCategory;
        }, true);


        $scope.$watch('filtered', function (newValue) {
            if (angular.isArray(newValue)) {
                console.log(newValue.length);
            }
        }, true);

//end fabric filter code


//filter fabric code

        angular.module("Shirt").filter('count', function () {
            return function (collection, key) {
                var out = "test";
                for (var i = 0; i < collection.length; i++) {
                    //console.log(collection[i].pants);
                    //var out = myApp.filter('filter')(collection[i].pants, "42", true);
                }
                return out;
            }
        });


        angular.module("Shirt").filter('groupBy',
                function () {
                    return function (collection, key) {
                        if (collection === null)
                            return;
                        return uniqueItems(collection, key);
                    };
                });


        function uniqueItems(data, key) {
            var result = [];

            for (var i = 0; i < data.length; i++) {



                var value = data[i][key];
                console.log("key = " + key + " value=  " + value);
                if (result.indexOf(value) == -1) {
                    result.push(value);
                }

            }
            return result;
        }
        ;
        $scope.clearFilter = function () {

            $scope.test = "";
            var fabId = $scope.fabricId;

            $scope.useMaterials = {};
            $scope.usePatterns = {};
            $scope.useSeasons = {};
            $scope.useColors = {};
            $scope.useCategory = {};

            $scope.fabricData.fabric = [];

            for (var data in tempPj.fabric) {
                if (tempPj.fabric.hasOwnProperty(data))
                {
                    console.log(data)
                    $scope.fabricData.fabric.push(tempPj.fabric[data]);

                }
            }

            jQuery("#clearfilterfab").hide();
            clrfilter = false;
            jQuery("#nullTxt").hide();
            //angular.element("#div4").find(".activeDesign").removeClass('activeDesign');


            //angular.element($event.currentTarget).find(".fabvie").addClass("activeDesign");
            console.log("$scope.fabricId=" + $scope.fabricId);
            setTimeout(function () {
                jQuery("#div4 #" + fabricId).find(".fabvie").addClass("activeDesign");
            }, 200);

        }
        $scope.backClearFilter = function () {

            $scope.test = "";
            var fabId = $scope.fabricId;

            $scope.useMaterials = {};
            $scope.usePatterns = {};
            $scope.useSeasons = {};
            $scope.useColors = {};
            $scope.useCategory = {};

            $scope.fabricData.fabric = [];

            for (var data in tempPj.fabric) {
                if (tempPj.fabric.hasOwnProperty(data))
                {
                    console.log(data)
                    $scope.fabricData.fabric.push(tempPj.fabric[data]);

                }
            }

        }
//filter fabric code end

//new filter fabric code end

//filter end

        $scope.leftNavFabCatFilterClose = function ($event)
        {
            $scope.backClearFilter();
            jQuery("#divOptionFilter").hide();
            nullfabricTxt();
            // alert("654")
            //leftMainNav_fabricCat
            setTimeout(function () {
                jQuery(".leftpanelsection #div4").show();
                $scope.clearFilter();
                jQuery('#accordian').accordion("refresh");
            }, 50);

        }

        $scope.leftNavFabCatFilter = function ($event)
        {

            jQuery(".leftpanelsection .sidebar-options").hide();
            //leftMainNav_fabricCat
            setTimeout(function () {

                jQuery("#divOptionFilter").show();
                jQuery('#accordian').accordion("refresh");
            }, 50);
        }

//end filter

        $scope.leftNav = function ($event, obj, ind) {

            angular.element(".suit-preloader").show();
            angular.element(".mainToolarea").css({"opacity": "0"});
            $scope.actLeftNav = angular.element($event.currentTarget).find("a").attr("target") ? angular.element($event.currentTarget).find("a").attr("target") : "1";
            angular.element($event.currentTarget).parent().find(".active").removeClass("active");
            angular.element(".sidebar-options").hide();
            angular.element("#div" + $scope.actLeftNav).show();
            angular.element($event.currentTarget).addClass("active");
            //customize pant n hide jacket
            var styleType = angular.element($event.currentTarget).attr("type")

            if (styleType == "pant")
            {
                return false;
                if (jQuery(window).width() <= 767) {
                    $scope.isActive = false;
                } else {
                    if ($scope.activeView == 1 || $scope.activeView == 2)
                    {
                        jQuery('#mainImageHolder').find('[rel=veststyle]').hide();
                        jQuery('#mainImageHolder').find('[rel=vestlapel]').hide();
                        jQuery('#mainImageHolder').find('[rel=vestpockets]').hide();
                        jQuery('#mainImageHolder').find('[rel=breastpocket]').hide();
                        jQuery('#mainImageHolder').find('[rel=vestbuttons]').hide();
                        //vest back
                        jQuery('#mainImageHolder').find('[rel=backveststyle]').hide();
                        //lining
                        jQuery('#mainImageHolder').find('[rel=collarstyle]').show();
                        jQuery('#mainImageHolder').find('[rel=lining]').show();
                        jQuery('#mainImageHolder').find('[rel=vestcollar]').hide();
                        jQuery('#mainImageHolder').find('[rel=vestlining]').hide();
                        $scope.isActive = true;
                    } else {
                        jQuery('#mainImageHolder').find('[rel=veststyle]').show();
                        jQuery('#mainImageHolder').find('[rel=vestlapel]').show();
                        jQuery('#mainImageHolder').find('[rel=vestpockets]').hide();
                        jQuery('#mainImageHolder').find('[rel=breastpocket]').hide();
                        jQuery('#mainImageHolder').find('[rel=vestbuttons]').show();
                        //vest back
                        jQuery('#mainImageHolder').find('[rel=backveststyle]').hide();
                        //lining
                        jQuery('#mainImageHolder').find('[rel=collarstyle]').show();
                        jQuery('#mainImageHolder').find('[rel=lining]').show();
                        jQuery('#mainImageHolder').find('[rel=vestcollar]').hide();
                        jQuery('#mainImageHolder').find('[rel=jacketlining]').show();
                        $scope.isActive = false;
                    }
                }


//check for view 3
                if ($scope.activeView == 3)
                {
//show jacket
                    $scope.IsJacket = true;
                } else {
//hide jacket
                    $scope.IsJacket = false;
                }


                hideJacket();
                //hide vest



            } else if (styleType == "vest")
            {


                if (jQuery(window).width() <= 767) {

                    $scope.isActive = false;
                } else {
                    $scope.isActive = false;
                }

//hide jacket
                $scope.IsJacket = false;
                //hide vest
                jQuery('#mainImageHolder').find('[rel=veststyle]').show();
                jQuery('#mainImageHolder').find('[rel=vestlapel]').show();
                jQuery('#mainImageHolder').find('[rel=vestpockets]').show();
                jQuery('#mainImageHolder').find('[rel=breastpocket]').show();
                jQuery('#mainImageHolder').find('[rel=vestbuttons]').show();
                //vest back
                jQuery('#mainImageHolder').find('[rel=backveststyle]').show();
                //lining
                jQuery('#mainImageHolder').find('[rel=collarstyle]').hide();
                jQuery('#mainImageHolder').find('[rel=lining]').hide();
                jQuery('#mainImageHolder').find('[rel=vestcollar]').show();
                jQuery('#mainImageHolder').find('[rel=jacketlining]').hide();
                jQuery('#mainImageHolder').find('[rel=vestlining]').show();
                hideJacket();
            } else {

                console.info("this is else part");
                $scope.isActive = false;
                //show jacket
                $scope.IsJacket = true;
                //hide vest
                jQuery('#mainImageHolder').find('[rel=veststyle]').show();
                jQuery('#mainImageHolder').find('[rel=vestlapel]').show();
                jQuery('#mainImageHolder').find('[rel=vestpockets]').show();
                jQuery('#mainImageHolder').find('[rel=breastpocket]').show();
                jQuery('#mainImageHolder').find('[rel=vestbuttons]').show();
                //vest back
                jQuery('#mainImageHolder').find('[rel=backveststyle]').show();
                //lining
                jQuery('#mainImageHolder').find('[rel=collarstyle]').show();
                jQuery('#mainImageHolder').find('[rel=lining]').show();
                jQuery('#mainImageHolder').find('[rel=vestcollar]').hide();
                jQuery('#mainImageHolder').find('[rel=vestlining]').hide();
                jQuery('#mainImageHolder').find('[rel=jacketlining]').show();
                hideJacket();
            }



            imageLoaded();
            //hide name
            if (jQuery(".sidebar-options").hasClass("min"))
            {
                jQuery(".shorttext").show();
            } else {
                jQuery(".shorttext").hide();
            }

        }

        $scope.categoryClick = function ($event, obj, ind) {

            var navType = jQuery(".topmenu ul li.active").find("a").attr("target");
            var navName = jQuery(".topmenu ul li.active").find("a").text();
            if ($scope.sleeveType == "short") {
                setTimeout(function () {
                    angular.element("#position-4").css({"display": "none"});
                }, 500);
            } else {

                setTimeout(function () {
                    angular.element("#position-4").css({"display": "block"});
                }, 500);
            }

            if (ind == "7") {

                hideJacket();
            }
            console.log(ind + " <== Ind " + navName + "<== navNameStyle");

            if (ind == "0" && navName == "Style") {
                $scope.activeView = 5;
            } else if (ind == "6" && navName == "Style") {
                $scope.activeView = 3;

            } else {
                $scope.activeView = 1;
            }

            jQuery(".shorttext").show();
            if (navType == "5") {
                $scope.actLeftNav = 5;
                // $scope.activeView = 3;
            } else {
                if ($scope.actLeftNav == "5") {
                    $scope.actLeftNav = 1;
                }
            }

            var actCon = angular.element("#div" + $scope.actLeftNav + " .box_option:first").attr("id") ? angular.element("#div" + $scope.actLeftNav + " .box_option:first").attr("id").split("_")[0] : "divs";

            angular.element("#div" + $scope.actLeftNav + " ul li ").removeClass("active");
            angular.element($event.currentTarget).addClass("active");
            angular.element($event.currentTarget).parent().find(".activeDesign").removeClass("activeDesign")
            angular.element($event.currentTarget).addClass("activeDesign");
            angular.element(".sidebar-options").addClass("min");
            angular.element(".box_options .box_option").removeClass("active");
            angular.element(".box_options #" + actCon + "_" + obj.id).addClass("active");
            setTimeout(function () {
                if (navType == "1") {
                    jQuery(".leftMainNav").find(".active").trigger("click");
                }
            }, 50);
            var designStyle = obj.name.replace(/\s+|&/g, "");

            console.log(designStyle);

            $scope.actCatName = designStyle;

            angular.element(".monoFont").css({"display": "none"});

            $scope.IsShirt = false;
            $scope.IsPant = false;
            $scope.IsTie = false;
            hideJacket();
            var chk = jQuery("#data").val();
            if (chk === "" || chk === undefined)
            {  // console.log("Hide monogram")
                $scope.monogramText = "";
                jQuery("#mainImageHolder").find("[rel=Monogram]").hide();
            } else {
                // console.log("show monogram")
                jQuery("#mainImageHolder").find("[rel=Monogram]").show();
            }
            if ($scope.actCatName == "AddMonogram") {
                active_view_Id = $scope.activeView = 1;

                jQuery(".monoFont").css({"display": "block"});
                var chk = jQuery("#data").val();
                if (chk === "" || chk === undefined)
                {  // console.log("Hide monogram")
                    $scope.monogramText = "";
                    jQuery("#mainImageHolder").find("[rel=Monogram]").hide();
                } else {
                    // console.log("show monogram")
                    jQuery("#mainImageHolder").find("[rel=Monogram]").show();
                }
                setTimeout(function () {
                    jQuery("#div8").css({"display": "none"})
                    jQuery("#div9").css({"display": "none"})
                    jQuery("#div4_fabric_accent").css({"display": "none"})
                    jQuery("#div4_cuff_accent").css({"display": "none"})
                    jQuery("#div4_thread_fabric_accent").css({"display": "none"})
                }, 200);
                //initPositionOfText();
            } else if ($scope.actCatName == "CollarStyle") {

                $scope.activeView = 5;

                //hide accent collar
                if (!angular.element('#divaccent_2 ul:first > li.activeDesign').attr('id'))
                {
                    //console.log("In actCatName CollarStyle")
                    angular.element('#divaccent_2 ul:first > li:first').addClass('activeDesign');
                    angular.element('#divaccent_2 ul.CollarStyle').css("display", "none");
                }


            } else if ($scope.actCatName == "Cuffs") {


                $scope.activeView = 3;

                if (!angular.element('#divaccent_3 ul:first > li.activeDesign').attr('id'))
                {
                    angular.element('#divaccent_3 ul:first > li:first').addClass('activeDesign');
                    angular.element('#divaccent_3 ul.Cuffs').css("display", "none");
                }


            } else if ($scope.actCatName == "Threads") {


                $scope.activeView = 4;
                if (!angular.element('#divaccent_4 ul:first > li.activeDesign').attr('id'))
                {
                    angular.element('#divaccent_4 ul:first > li:first').addClass('activeDesign');

                }


            } else if ($scope.actCatName == "ElbowPatch") {

                $scope.activeView = 3;
                if (!angular.element('#divaccent_5 ul:first > li.activeDesign').attr('id'))
                {
                    angular.element('#divaccent_5 ul:first > li:first').addClass('activeDesign');
                    angular.element('#divaccent_5 ul.ElbowPatch').css("display", "none");
                }
            } else if ($scope.actCatName == "Chestpleats") {


                $scope.activeView = 1;
                if (!angular.element('#divaccent_6 ul:first > li.activeDesign').attr('id'))
                {
                    angular.element('#divaccent_6 ul:first > li:first').addClass('activeDesign');
                }


            } else if (designStyle == "Bottom") {

                $scope.activeView = 3;
                $scope.IsShirt = true;
                $scope.IsPant = true;
                // $scope.IsTie=true;
                hideJacket();

            }



            setTimeout(function () {
                angular.element(".fabricInfo").find("a").trigger("click");
            }, 500);


            changeView();
        }


        function changeView()
        {

            angular.element("#main-container .view-thumb button").removeClass("active");
            var actCat = jQuery(".topmenu .active a").html();
            if (actCat == "Fabric") {
                jQuery("#clearfilterfab").hide();
                clearFilterActive();
            }

            var styleID = angular.element("#divs_1").find(".activeDesign").attr("id");

            console.log("is casual" + $scope.IsShirt)
//        if($scope.IsShirt){
//            jQuery("#tie").hide();
//        }else{
//            jQuery("#tie").show();
//        }

            if ($scope.activeView == "1")
            {
                //angular.element("#zoomButton").show();
                setTimeout(function () {
                    angular.element("#position-1").trigger("click");
                }, 500);
                //angular.element(".jHh").css({"margin-left": "10px"});
                //angular.element(".jHh").css({"display": "block"});
                jQuery("#arrowUP,#arrowDown,#zoomButton,#casual_1,#casual_2,#jeans,#tie").show();
                if (angular.element(".leftMainNav").children().find(".active").attr("type") == "pant" && actCat == "Style")
                {

                    if (jQuery(window).width() <= 767) {

                        $scope.isActive = false;
                    } else {
                        $scope.isActive = true;
                    }


                    $scope.IsJacket = false;
                } else if (angular.element(".leftMainNav").children().find(".active").attr("type") == "vest")
                {

                    $scope.IsJacket = false;
                } else
                {

                    if ($scope.IsJacket) {

                        $scope.IsJacket = true;
                    } else {
                        $scope.IsJacket = false;
                    }

                }

                angular.element("#Front").addClass("active");
                //angular.element(".jH").show();
                if (angular.element(".leftMainNav").children().find(".active").attr("type") == "pant") {


                    jQuery('#mainImageHolder').find('[rel=veststyle]').hide();
                    jQuery('#mainImageHolder').find('[rel=vestlapel]').hide();
                    jQuery('#mainImageHolder').find('[rel=vestpockets]').hide();
                    jQuery('#mainImageHolder').find('[rel=breastpocket]').hide();
                    jQuery('#mainImageHolder').find('[rel=vestbuttons]').hide();
                    //vest back
                    jQuery('#mainImageHolder').find('[rel=backveststyle]').hide();
                    //lining
                    jQuery('#mainImageHolder').find('[rel=collarstyle]').hide();
                    jQuery('#mainImageHolder').find('[rel=lining]').hide();
                    jQuery('#mainImageHolder').find('[rel=vestcollar]').hide();
                    jQuery('#mainImageHolder').find('[rel=vestlining]').hide();
                }
                angular.element("#jacketHide").css({"display": "inline-block"});
                if (styleID == '8' || styleID == '10') {
                    jQuery("#mainImageHolder").find("[rel=ManTie]").hide();
                    jQuery("#mainImageHolder").find("[rel=ManTieSide]").hide();
//                jQuery("#tie_1").hide();
//                jQuery("#tie_2").hide();
                    jQuery("#tie").hide();

                }
                if ($scope.IsShirt) {
                    jQuery("#mainImageHolder").find("[rel=ManTie]").hide();
                    jQuery("#mainImageHolder").find("[rel=ManTieSide]").hide();
                    jQuery("#tie").hide();
                } else {
                    if (styleID == '8' || styleID == '10') {
                        jQuery("#mainImageHolder").find("[rel=WomanTie]").hide();
                        jQuery("#mainImageHolder").find("[rel=WomanTieSide]").hide();
                        //jQuery("#tie_1").hide();
                        //jQuery("#tie_2").hide();
                        jQuery("#tie").hide();
                    } else {
                        // jQuery("#tie_1").show();
                        // jQuery("#tie_2").show();
                        jQuery("#tie").show();
                    }

                }
            } else if ($scope.activeView == "2")
            {
// $scope.IsShirt = true;
                //angular.element(".jHh").css({"margin-left": "10px"})
                //angular.element(".jHh").css({"display": "block"});
//            jQuery("#arrowUP,#arrowDown").show();
                angular.element("#Back").addClass("active");
                jQuery("#arrowUP,#arrowDown,#zoomButton,#casual_1,#casual_2,#jeans,#tie").show();
                // angular.element(".jH").show();
                if (angular.element(".leftMainNav").children().find(".active").attr("type") == "pant" && actCat == "Style")
                {
                    if (jQuery(window).width() <= 767) {

                        $scope.isActive = false;
                    } else {
                        $scope.isActive = true;
                    }

                    $scope.IsJacket = false;
                } else if (angular.element(".leftMainNav").children().find(".active").attr("type") == "vest") {
                    $scope.IsJacket = false;
                } else
                {
                    if ($scope.IsJacket) {
                        $scope.IsJacket = true;
                    } else {
                        $scope.IsJacket = false;
                    }
                }

                if (angular.element(".leftMainNav").children().find(".active").attr("type") == "pant") {

                    jQuery('#mainImageHolder').find('[rel=veststyle]').hide();
                    jQuery('#mainImageHolder').find('[rel=vestlapel]').hide();
                    jQuery('#mainImageHolder').find('[rel=vestpockets]').hide();
                    jQuery('#mainImageHolder').find('[rel=breastpocket]').hide();
                    jQuery('#mainImageHolder').find('[rel=vestbuttons]').hide();
                    //vest back
                    jQuery('#mainImageHolder').find('[rel=backveststyle]').hide();
                    //lining
                    jQuery('#mainImageHolder').find('[rel=collarstyle]').hide();
                    jQuery('#mainImageHolder').find('[rel=lining]').hide();
                    jQuery('#mainImageHolder').find('[rel=vestcollar]').hide();
                    jQuery('#mainImageHolder').find('[rel=vestlining]').hide();
                }
                angular.element("#jacketHide").css({"display": "inline-block"});
                if (styleID == '8' || styleID == '10') {
                    jQuery("#mainImageHolder").find("[rel=ManTie]").hide();
                    jQuery("#mainImageHolder").find("[rel=ManTieSide]").hide();
//                jQuery("#tie_1").hide();
//                jQuery("#tie_2").hide();
                    jQuery("#tie").hide();

                }
                if ($scope.IsShirt) {
                    jQuery("#mainImageHolder").find("[rel=ManTie]").hide();
                    jQuery("#mainImageHolder").find("[rel=ManTieSide]").hide();
                    jQuery("#tie").hide();
                } else {
                    if (styleID == '8' || styleID == '10') {
                        jQuery("#mainImageHolder").find("[rel=WomanTie]").hide();
                        jQuery("#mainImageHolder").find("[rel=WomanTieSide]").hide();
                        //jQuery("#tie_1").hide();
                        //jQuery("#tie_2").hide();
                        jQuery("#tie").hide();
                    } else {
                        // jQuery("#tie_1").show();
                        // jQuery("#tie_2").show();
                        jQuery("#tie").show();
                    }

                }
            } else if ($scope.activeView == "4") {

                angular.element("#Fold").addClass("active");


                $scope.IsPant = false;
                $scope.IsTie = false;
                //jQuery("#zoomButton").hide();
                jQuery("#arrowUP,#jeans,#arrowDown,#casual_1,#casual_2,#tie").hide();

                //angular.element(".jHh").css({"display": "none"});
                //angular.element(".jHh").css({"margin-left": "10px"})
                if ($scope.IsShirt) {
                    jQuery("#tie").hide();
                    $scope.IsShirt = true;
                } else {
                    $scope.IsShirt = false;
                    //jQuery("#tie").show();
                }
            } else if ($scope.activeView == "5") {
                //$scope.IsShirt = false;
                $scope.IsPant = false;
                $scope.IsTie = false;
                //angular.element(".jHh").css({"display": "none"});
                //angular.element(".jHh").css({"margin-left": "10px"});
                angular.element("#CollarZoom").addClass("active");
                jQuery("#arrowUP,#arrowDown,#zoomButton,#casual_1,#casual_2,#jeans,#tie").hide();
                if ($scope.IsShirt) {
                    jQuery("#tie").hide();
                    $scope.IsShirt = true;
                } else {
                    $scope.IsShirt = false;
                    //jQuery("#tie").show();
                }



            } else {

                jQuery("#arrowUP,#arrowDown,#zoomButton,#jeans").show();
                jQuery("#casual_1,#casual_2,#tie").hide();
// $scope.IsShirt = true;
                angular.element("#Inside").addClass("active");
                //angular.element(".jHh").css({"display": "block"});
                angular.element("#mainImageHolder").find("[rel=Pleats]").hide();
                $scope.isActive = false;
            }

// hideJacket();

            imageLoaded();
            //check for mobile
            if (jQuery(window).width() <= 767) {

                jQuery("#arrowUP,#arrowDown,#zoomButton,#zoomOutButton").hide();
            }

        }


        $scope.removemin = function ($event) {
            removeActnMin();
        }

        function removeActnMin() {

            jQuery(".shorttext").css({"display": "none"});
            jQuery("#div1").removeClass("min");
            if (jQuery(window).width() <= 767) {

                angular.element(".sidebar-options").removeClass("min");
                angular.element(".LeftpanemOpen .leftpanelsection").css("left", "0");
            }

            angular.element(".box_option").removeClass("active");
            //hide stle name
            var type = jQuery(".topmenu .active a").attr("target");
            setTimeout(function () {
                if (type == "5")
                {
                    angular.element(".topmenu a[target=" + type + "]").trigger("click");
                } else {
                    angular.element(".leftMainNav").find(".active a").trigger("click");
                }
            }, 100);
            //hide stle name end


        }



//change Style of Jacket 
        $scope.designClick = function ($event, obj, ind) {


            style_ID = jQuery(obj).attr("id");
            angular.element(".suit-preloader").show();
            angular.element(".mainToolarea").css({"opacity": "0"});
            var parent, styleImg, styleType, styleName, styleId;

            angular.element($event.currentTarget).parent().find(".activeDesign").removeClass("activeDesign");
            angular.element($event.currentTarget).addClass("activeDesign");
            if (jQuery(window).width() <= 767) {

                angular.element(".sidebar-options").removeClass("min");
                angular.element(".LeftpanemOpen .leftpanelsection").css("left", "0");
            }

            angular.element("body").removeClass("LeftpanemOpen");

            $scope.activeStyle = obj;


            currStyleName = parent = $scope.activeStyle.parent.replace(/\s+|&/g, "");

            style = obj;

            var a = jQuery("#div1 .box_options>div").length;

            for (var i = 1; i <= a; i++) {
                stylePriceArr[i] = jQuery("#div1  #divs_" + i + " .activeDesign").find(".price").text().trim("");
                //  console.log("stylePriceArr[" + i + "]" + stylePriceArr[i])
            }


            styleName = $scope.activeStyle.name.replace(/\s+|&/g, "");
            styleId = $scope.activeStyle.id;
            temp[parent] = $scope.actStyleData[parent] = styleId;


            //get style type single or double
            styleType = $scope.activeStyle.designType; //angular.element("#view" + $scope.activeView).find('.shirt_part[rel=' + parent + ']').attr("type");
            console.log("parent ==" + parent + " style id ==" + styleId);
            if (parent == "CollarStyle")
            {
                if ($scope.threadcolorId == undefined)
                {
                    $scope.threadcolorId = "4";
                }
                $scope.collarId = styleId;
                angular.element("#mainImageHolder").find('.shirt_part[rel=casualWomanBackCollar]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/collarstyle/back_collar/" + $scope.fabricId + "_style_1_casual_front_view_1.png");


                if (styleId == '6') {

                    angular.element("#mainImageHolder").find('.shirt_part[rel=Pin]').show();
                    angular.element("#mainImageHolder").find('.shirt_part[rel=PinSide]').show();
                    angular.element("#mainImageHolder").find('.shirt_part[rel=PinFold]').show();
                    angular.element("#viewport").find('.shirt_part[rel=PinZoom]').show();

                    angular.element("#viewport").find('.shirt_part[rel=ButtondownZoom]').hide();
                    angular.element("#mainImageHolder").find('.shirt_part[rel=ButtonDown]').hide();
                    angular.element("#mainImageHolder").find('.shirt_part[rel=ButtonDownSide]').hide();
                    angular.element("#mainImageHolder").find('.shirt_part[rel=ButtonDownFold]').hide();
                    angular.element("#viewport").find('.shirt_part[rel=ButtondownThreadZoom]').hide();
                    angular.element("#viewport").find('.shirt_part[rel=ButtondownHoleZoom]').hide();
                    angular.element("#mainImageHolder").find('.shirt_part[rel=ButtonDownThreadFold]').hide();


                } else if (styleId == '2') {
                    angular.element("#mainImageHolder").find('.shirt_part[rel=ButtonDown]').show();
                    angular.element("#mainImageHolder").find('.shirt_part[rel=ButtonDownSide]').show();
                    angular.element("#mainImageHolder").find('.shirt_part[rel=ButtonDownFold]').show()
                    angular.element("#viewport").find('.shirt_part[rel=ButtondownZoom]').show();
                    angular.element("#viewport").find('.shirt_part[rel=ButtondownThreadZoom]').show();
                    angular.element("#viewport").find('.shirt_part[rel=ButtondownHoleZoom]').show();
                    angular.element("#mainImageHolder").find('.shirt_part[rel=ButtonDownThreadFold]').show();

                    angular.element("#mainImageHolder").find('.shirt_part[rel=Pin]').hide();
                    angular.element("#mainImageHolder").find('.shirt_part[rel=PinSide]').hide();
                    angular.element("#mainImageHolder").find('.shirt_part[rel=PinFold]').hide();
                    angular.element("#viewport").find('.shirt_part[rel=PinZoom]').hide();

                } else if (styleId == '8' || styleId == '10') {
                    jQuery("#tie").hide();
                    angular.element("#view1").find('.shirt_part[rel=ManTie]').hide();
                    angular.element("#view2").find('.shirt_part[rel=ManTieSide]').hide();
                    angular.element("#mainImageHolder").find('.shirt_part[rel=Pin]').hide();
                    angular.element("#mainImageHolder").find('.shirt_part[rel=PinSide]').hide();
                    angular.element("#mainImageHolder").find('.shirt_part[rel=PinFold]').hide();
                    angular.element("#viewport").find('.shirt_part[rel=PinZoom]').hide();

                    angular.element("#viewport").find('.shirt_part[rel=ButtondownZoom]').hide();
                    angular.element("#mainImageHolder").find('.shirt_part[rel=ButtonDown]').hide();
                    angular.element("#mainImageHolder").find('.shirt_part[rel=ButtonDownSide]').hide();
                    angular.element("#mainImageHolder").find('.shirt_part[rel=ButtonDownFold]').hide();
                    angular.element("#viewport").find('.shirt_part[rel=ButtondownThreadZoom]').hide();
                    angular.element("#viewport").find('.shirt_part[rel=ButtondownHoleZoom]').hide();
                    angular.element("#mainImageHolder").find('.shirt_part[rel=ButtonDownThreadFold]').hide();

                    $scope.actStyleJKArr['CollarBase'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_view_1.png";
                    $scope.actStyleJKArr['CollarBaseSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_view_2.png";
                    angular.element("#mainImageHolder").find('.shirt_part[rel="CollarStyle"]').hide();
                    angular.element("#mainImageHolder").find('.shirt_part[rel="CollarStyleSide"]').hide();
                    styleImg = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_view_1.png";
                    $scope.actStyleJKArr['CollarStyle'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";
                    $scope.actStyleJKArr['CollarStyleSide'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";

                } else {

                    angular.element("#mainImageHolder").find('.shirt_part[rel=Pin]').hide();
                    angular.element("#mainImageHolder").find('.shirt_part[rel=PinSide]').hide();
                    angular.element("#mainImageHolder").find('.shirt_part[rel=PinFold]').hide();
                    angular.element("#viewport").find('.shirt_part[rel=PinZoom]').hide();
                    angular.element("#mainImageHolder").find('.shirt_part[rel=SingleButtonFold]').show();
                    if ($scope.activeView == "1" || $scope.activeView == "2") {
                        jQuery("#tie").show();
                    } else {
                        jQuery("#tie").hide();
                    }

                    angular.element("#viewport").find('.shirt_part[rel=ButtondownZoom]').hide();
                    angular.element("#mainImageHolder").find('.shirt_part[rel=ButtonDown]').hide();
                    angular.element("#mainImageHolder").find('.shirt_part[rel=ButtonDownSide]').hide();
                    angular.element("#mainImageHolder").find('.shirt_part[rel=ButtonDownFold]').hide();
                    angular.element("#viewport").find('.shirt_part[rel=ButtondownThreadZoom]').hide();
                    angular.element("#viewport").find('.shirt_part[rel=ButtondownHoleZoom]').hide();
                    angular.element("#mainImageHolder").find('.shirt_part[rel=ButtonDownThreadFold]').hide();
                }




                if ($scope.activeView == "1" || $scope.activeView == "3") {

                    if (styleId == '8' || styleId == '10') {
                        $scope.actStyleJKArr['CollarBase'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_view_1.png";
                        $scope.actStyleJKArr['CollarBaseSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_view_2.png";
                        angular.element("#mainImageHolder").find('.shirt_part[rel="CollarStyle"]').hide();
                        angular.element("#mainImageHolder").find('.shirt_part[rel="CollarStyleSide"]').hide();
                        styleImg = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_view_1.png";
                        $scope.actStyleJKArr['CollarStyle'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";
                        $scope.actStyleJKArr['CollarStyleSide'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";

                    } else {
                        angular.element("#mainImageHolder").find('.shirt_part[rel="CollarStyle"]').show();
                        angular.element("#mainImageHolder").find('.shirt_part[rel="CollarStyleSide"]').show();
                        $scope.actStyleJKArr['CollarBase'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_front_view_1.png";
                        $scope.actStyleJKArr['CollarBaseSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_front_view_2.png";
                        styleImg = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_view_1.png";
                        $scope.actStyleJKArr['CollarStyleSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_view_2.png";
                        $scope.actStyleJKArr['CollarStyleFold'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_view_4.png";
                    }

                } else if ($scope.activeView == "2") {
                    if (styleId == '8' || styleId == '10') {
                        $scope.actStyleJKArr['CollarBase'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_view_1.png";
                        $scope.actStyleJKArr['CollarBaseSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_view_2.png";
                        angular.element("#mainImageHolder").find('.shirt_part[rel="CollarStyle"]').hide();
                        angular.element("#mainImageHolder").find('.shirt_part[rel="CollarStyleSide"]').hide();
                        styleImg = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_view_1.png";
                        $scope.actStyleJKArr['CollarStyle'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";
                        $scope.actStyleJKArr['CollarStyleSide'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";

                    } else {
                        angular.element("#mainImageHolder").find('.shirt_part[rel="CollarStyle"]').show();
                        angular.element("#mainImageHolder").find('.shirt_part[rel="CollarStyleSide"]').show();
                        $scope.actStyleJKArr['CollarBase'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_front_view_1.png";
                        $scope.actStyleJKArr['CollarBaseSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_front_view_2.png";

                        $scope.actStyleJKArr['CollarStyleSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_view_2.png";
                        angular.element("#view1").find('.shirt_part[rel="CollarStyle"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_view_1.png");
                        $scope.actStyleJKArr['CollarStyleFold'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_view_4.png";

                    }
                } else if ($scope.activeView == "4") {
                    if (styleId == '8' || styleId == '10') {
                        $scope.actStyleJKArr['CollarBase'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_view_1.png";
                        $scope.actStyleJKArr['CollarBaseSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_view_2.png";

                        angular.element("#mainImageHolder").find('.shirt_part[rel="CollarStyle"]').hide();
                        angular.element("#mainImageHolder").find('.shirt_part[rel="CollarStyleSide"]').hide();
                        styleImg = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_view_1.png";
                        $scope.actStyleJKArr['CollarStyle'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";
                        $scope.actStyleJKArr['CollarStyleSide'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";

                    } else {
                        angular.element("#mainImageHolder").find('.shirt_part[rel="CollarStyle"]').show();
                        angular.element("#mainImageHolder").find('.shirt_part[rel="CollarStyleSide"]').show();
                        $scope.actStyleJKArr['CollarBase'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_front_view_1.png";
                        $scope.actStyleJKArr['CollarBaseSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_front_view_2.png";

                        $scope.actStyleJKArr['CollarStyleFold'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_view_4.png";
                        $scope.actStyleJKArr['CollarStyleSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_view_2.png";
                        $scope.actStyleJKArr['innerCollarGlow'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_back_view_4.png";
                        angular.element("#view1").find('.shirt_part[rel="CollarStyle"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_view_1.png");
                    }
                } else if ($scope.activeView == "5") {
                    if (styleId == '8' || styleId == '10') {
                        $scope.actStyleJKArr['CollarBase'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_view_1.png";
                        $scope.actStyleJKArr['CollarBaseSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_view_2.png";
                        angular.element("#mainImageHolder").find('.shirt_part[rel="CollarStyle"]').hide();
                        angular.element("#mainImageHolder").find('.shirt_part[rel="CollarStyleSide"]').hide();
                        styleImg = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_view_1.png";
                        $scope.actStyleJKArr['CollarStyle'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";
                        $scope.actStyleJKArr['CollarStyleSide'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";


                    } else {
                        angular.element("#mainImageHolder").find('.shirt_part[rel="CollarStyle"]').show();
                        angular.element("#mainImageHolder").find('.shirt_part[rel="CollarStyleSide"]').show();
                        $scope.actStyleJKArr['CollarBase'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_front_view_1.png";
                        $scope.actStyleJKArr['CollarBaseSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_front_view_2.png";

                        angular.element("#view1").find('.shirt_part[rel="CollarStyle"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_view_1.png");
                        $scope.actStyleJKArr['CollarStyleSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_view_2.png";
                        $scope.actStyleJKArr['CollarStyleFold'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_view_4.png";
                        $scope.actStyleJKArr['innerCollarGlow'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_back_view_4.png";
                    }
                }


                if ($scope.contrastCollarStyle == "ALL") {
                    $scope.actStyleJKArr['CollarStyleFold'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_view_4.png";
                    $scope.actStyleJKArr['CollarStyleZoom'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_view_5.png";
                    $scope.actStyleJKArr['CollarStyleZoominner'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_back_view_5.png";
                    $scope.actStyleJKArr['innerCollarGlow'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_back_view_4.png";
                }
                //1_style_1_casual_collar_view_1
                $scope.actStyleJKArr['casualCollar'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/casual/" + $scope.fabricId + "_style_" + styleId + "_casual_collar_view_1.png";
                $scope.actStyleJKArr['casualCollarSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/casual/" + $scope.fabricId + "_style_" + styleId + "_casual_collar_view_2.png";
                $scope.actStyleJKArr['CollarStyleZoom'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_view_5.png";
//            $scope.actStyleJKArr['CollarStyleZoom'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_view_5.png";

                $scope.actStyleJKArr['WomanBackCollar'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/back_collar/" + $scope.fabricId + "_style_" + 1 + "_front_view_1.png";
                angular.element("#mainImageHolder").find("[rel=WomanBackCollarSide]").attr("src", basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/back_collar/" + $scope.fabricId + "_style_" + 1 + "_left_view_2.png");
                $scope.actStyleJKArr['CollarStyleBack'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_view_3.png";


                $scope.actStyleJKArr['CollarStyleZoominner'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_back_view_5.png";
                $scope.actStyleJKArr['CollarStyleFold'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_view_4.png";
                $scope.actStyleJKArr['innerCollarGlow'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_back_view_4.png";
                if ($scope.collarId == "1") {

                    $scope.actStyleJKArr['BottonZoomHole'] = basePath + "pub/media/shirt-tool/buttonfabric_images/1_torso_buttonholes_view_5.png";
                    //$scope.actStyleJKArr['BottonZoomHole'] = basePath + "pub/media/shirt-tool/buttonfabric_images/" + $scope.threadFabricId + "_torso_buttonholes_view_5.png";
                    $scope.actStyleJKArr['BottonZoomThread'] = basePath + "pub/media/shirt-tool/buttonfabric_images/1_torso_buttonthread_view_5.png";
                    //$scope.actStyleJKArr['ZoomBottondown'] = basePath + "pub/media/shirt-tool/shirt_images/collarstyle/Static/ButtondownCollar_Button_Zoom.png";//buttonfabric_images/" + $scope.threadFabricId + "_final_button_hole_view_5.png";
                    $scope.actStyleJKArr['BottonZoomHole'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_buttonhole_view_5.png";

                    $scope.actStyleJKArr['BottonZoomThread'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_thread_view_5.png";
                } else {
                    angular.element("#viewport").find('.shirt_part[rel=BottondownZoom]').show();
                    $scope.actStyleJKArr['BottonZoomHole'] = basePath + "pub/media/shirt-tool/buttonfabric_images/1_torso_buttonholes_view_5.png";
//        $scope.actStyleJKArr['BottonZoom'] = basePath + "pub/media/shirt-tool/buttonfabric_images/1_final_view_5.png";
                    $scope.actStyleJKArr['BottonZoomThread'] = basePath + "pub/media/shirt-tool/buttonfabric_images/1_torso_buttonthread_view_5.png";
                    // $scope.actStyleJKArr['ZoomBottondown'] = basePath + "pub/media/shirt-tool/blank.png";
                    $scope.actStyleJKArr['BottonZoomHole'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_buttonhole_view_5.png";

                    $scope.actStyleJKArr['BottonZoomThread'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_thread_view_5.png";
                }
                if ($scope.IsShirt) {
                    hideJacket();
                }


            } else if (parent == "Cuffs") {
                $scope.cuffStyleId = styleId;
                if ($scope.threadcolorId == undefined)
                {
                    $scope.threadcolorId = "4";
                }
                angular.element("#mainImageHolder").find('.shirt_part[rel="CuffsOutBack"]').hide();
                //console.log("----------------------" + $scope.threadcolorId)
                //for Buttons

                if (styleId == "5" || styleId == "8" || styleId == "10") {
                    //2ButtonCuff ButtonThreadCuff ButtonHoleCuffFold

                    angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonCuff"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/Static/Folded_CuffDouble_b1.png");
                    //angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonThreadCuff"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/Static/Folded_CuffDoublethread.png");
                    angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonCuffBack"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/Static/Back_CuffDouble_b1.png");
                    //angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonThreadCuffBack"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/Static/Back_CuffDoublethread.png");
                    angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonHoleCuffFold"]').show();
                    angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonThreadCuffBack"]').show();
                    //angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonHoleFold"]').show();
                    //angular.element("#mainImageHolder").find('.shirt_part[rel="ThreadsFold"]').show();
                    $scope.actStyleJKArr['ButtonThreadCuffBack'] = styleImg = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + $scope.threadcolorId + "_double_buttonhole_view_3.png";
                    $scope.actStyleJKArr['ButtonThreadCuff'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + $scope.threadcolorId + "_double_thread_view_4.png";
                    $scope.actStyleJKArr['ButtonHoleCuffFold'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + $scope.threadcolorId + "_double_buttonhole_view_4.png";
                    //console.log("-----------5-8-10-----------" + styleId)

                    $scope.actStyleJKArr['ButtonCuffBack'] = styleImg = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/Static/Back_CuffDouble_b1.png";
                    jQuery("#divaccent_4 ul li#2").show();
                    jQuery("#divaccent_4 ul li#3").show();

                } else if (styleId == "1" || styleId == "2" || styleId == "6" || styleId == "9" || styleId == "12") {
                    //French 
                    angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonCuff"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/Static/Folded_Common_French_Cufflink.png");
                    angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonThreadCuff"]').attr("src", basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png");
                    angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonCuffBack"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/Static/Back_Common_French_Cufflink.png");
                    angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonThreadCuffBack"]').attr("src", basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png");
                    angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonHoleCuffFold"]').hide();
                    angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonThreadCuffBack"]').hide();
                    jQuery("#mainImageHolder").find(".shirt_part[rel=ButtonThreadCuff]").hide();
                    console.log("----------1-2-6-9-12------------" + styleId)

                    $scope.actStyleJKArr['ButtonCuffBack'] = styleImg = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/Static/Back_Common_French_Cufflink.png";
                    jQuery("#divaccent_4 ul li#2").hide();
                    jQuery("#divaccent_4 ul li#3").hide();
                } else {
                    //BottonSingle 4 3 7 11

                    angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonCuff"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/Static/Folded_CuffSingle_b1.png");
                    // angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonThreadCuff"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/Static/Folded_CuffSinglethread.png");
                    angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonCuffBack"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/Static/Back_CuffSingle_b1.png");
                    //angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonThreadCuffBack"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/Static/Back_CuffSinglethread.png");
                    angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonHoleCuffFold"]').show();
                    angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonThreadCuffBack"]').show();
                    angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonThreadCuff"]').show();

                    $scope.actStyleJKArr['ButtonThreadCuffBack'] = styleImg = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + $scope.threadcolorId + "_single_buttonhole_view_3.png";
                    $scope.actStyleJKArr['ButtonThreadCuff'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + $scope.threadcolorId + "_single_buttonhole_view_4.png";
                    $scope.actStyleJKArr['ButtonHoleCuffFold'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + $scope.threadcolorId + "_single_thread_view_4.png";
                    console.log("---------4-3-7-11-------------" + styleId)

                    $scope.actStyleJKArr['ButtonCuffBack'] = styleImg = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/Static/Back_CuffSingle_b1.png";
                    jQuery("#divaccent_4 ul li#2").show();
                    jQuery("#divaccent_4 ul li#3").show();
                }

                //buttons End here
                $scope.actStyleJKArr['CuffsFold'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_cuffstyle_1_size__cufftype__" + styleId + "_view_4.png";
                $scope.actStyleJKArr['CuffsFoldInner'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + $scope.fabricId + "_cuffstyle_1_size__inner__" + styleId + "_inner_view_4.png";
                $scope.actStyleJKArr['CuffsBack'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_cuffstyle_1_size__cufftype__" + styleId + "_view_3.png";
                angular.element("#view1").find('.shirt_part[rel="Cuffs"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_cuffstyle_1_size__cufftype__" + styleId + "_view_1.png");
                styleImg = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_cuffstyle_1_size__cufftype__" + styleId + "_view_1.png";
                $scope.actStyleJKArr['Cuffs'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_cuffstyle_1_size__cufftype__" + styleId + "_view_1.png";
                $scope.actStyleJKArr['CuffsSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_cuffstyle_1_size__cufftype__" + styleId + "_view_2.png";

                $scope.actStyleJKArr['SleevePlacketLeft'] = basePath + "pub/media/shirt-tool/shirt_images/sleeveplacket/" + $scope.fabricId + "_sleeve_1_left_view_3.png";
                $scope.actStyleJKArr['SleevePlacketRight'] = basePath + "pub/media/shirt-tool/shirt_images/sleeveplacket/" + $scope.fabricId + "_sleeve_1_right_view_3.png";
//console.log("style img for cuff on fab click" + styleImg)

                $scope.actStyleJKArr['CuffsOutBack'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";
            } else if (parent == "Sleeves")
            {
                if ($scope.cuffStyleId === undefined) {
                    $scope.cuffStyleId = "1";
                    $scope.threadcolorId = "4";
                }
                /*hide Cuff For Short View*/
//            if ($scope.activeView == "1") {
                if (styleId == "2" || styleId == "3") {
                    $scope.sleeveType = "short";
                    angular.element("#mainImageHolder").find("[rel=Cuffs]").hide();
                    angular.element("#mainImageHolder").find("[rel=CuffsSide]").hide();
                    angular.element("#mainImageHolder").find("[rel=CuffsBack]").hide();
                    angular.element("#mainImageHolder").find("[rel=ButtonCuffBack]").hide();
                    angular.element("#mainImageHolder").find("[rel=ButtonThreadCuffBack]").hide();
                    angular.element("#mainImageHolder").find("[rel=ButtonCuff]").hide();
                    angular.element("#mainImageHolder").find("[rel=ButtonThreadCuff]").hide();
                    angular.element("#mainImageHolder").find("[rel=backelbow]").hide();

                    angular.element("#div1").find("ul").children().eq(1).css({"display": "none"});
                    angular.element("#div5").find("ul").children().eq(2).css({"display": "none"});

                    angular.element("#mainImageHolder").find("[rel=SleevePlacketLeft]").hide();
                    angular.element("#mainImageHolder").find("[rel=SleevePlacketRight]").hide();

                    angular.element("#mainImageHolder").find("[rel=SleevePlacketButtonLeft]").hide();
                    angular.element("#mainImageHolder").find("[rel=SleevePlacketButtonRight]").hide();

                    angular.element("#mainImageHolder").find("[rel=SleevePlacketThreadLeft]").hide();
                    angular.element("#mainImageHolder").find("[rel=SleevePlacketThreadRight]").hide();

                    $scope.actStyleJKArr['CuffsFold'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";
                    $scope.actStyleJKArr['CuffsFoldInner'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";
                    $scope.actStyleJKArr['SleevesFold'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_sleevesstyle_1_size__sleevestype__" + styleId + "_view_4.png";
                    angular.element("#mainImageHolder").find("[rel=ButtonCuff]").hide();
                    angular.element("#mainImageHolder").find("[rel=ButtonHoleCuffFold]").hide();
                    angular.element("#mainImageHolder").find("[rel=ButtonThreadCuffBack]").hide();
                    $scope.actStyleJKArr['Sleeves'] = styleImg = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_sleevesstyle_1_size__sleevestype__" + styleId + "_view_1.png";
                    $scope.actStyleJKArr['SleevesSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_sleevesstyle_1_size__sleevestype__" + styleId + "_view_2.png";
                    $scope.actStyleJKArr['SleevesBack'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_sleevesstyle_1_size__sleevestype__" + styleId + "_view_3.png";

                    jQuery("#divaccent_4 ul li#2").hide();
                    jQuery("#divaccent_4 ul li#3").hide();

                    if ("checked" == jQuery("#position-4").find("#radio-13").attr("checked")) {
                        if (jQuery("#position-4").find("#radio-13").prop("checked")) {
                            jQuery("#radio-10").prop("checked", true);
                            $scope.changeTextPos();
                        }

                    }
                } else {

                    $scope.sleeveType = "long";
                    angular.element("#mainImageHolder").find("[rel=Cuffs]").show();
                    angular.element("#mainImageHolder").find("[rel=CuffsSide]").show();
                    angular.element("#mainImageHolder").find("[rel=CuffsBack]").show();
                    angular.element("#mainImageHolder").find("[rel=ButtonCuffBack]").show();
                    angular.element("#mainImageHolder").find("[rel=ButtonThreadCuffBack]").show();
                    angular.element("#mainImageHolder").find("[rel=ButtonCuff]").show();
                    angular.element("#mainImageHolder").find("[rel=ButtonThreadCuff]").show();
                    angular.element("#div1").find("ul").children().eq(1).css({"display": "block"});
                    angular.element("#div5").find("ul").children().eq(2).css({"display": "block"});
                    angular.element("#mainImageHolder").find("[rel=SleevePlacketLeft]").show();
                    angular.element("#mainImageHolder").find("[rel=SleevePlacketRight]").show();
                    angular.element("#mainImageHolder").find("[rel=backelbow]").show();

                    angular.element("#mainImageHolder").find("[rel=SleevePlacketButtonLeft]").show();
                    angular.element("#mainImageHolder").find("[rel=SleevePlacketButtonRight]").show();

                    angular.element("#mainImageHolder").find("[rel=SleevePlacketThreadLeft]").show();
                    angular.element("#mainImageHolder").find("[rel=SleevePlacketThreadRight]").show();

                    $scope.actStyleJKArr['CuffsFold'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + $scope.fabricId + "_cuffstyle_1_size__cufftype__" + styleId + "_view_4.png";
                    $scope.actStyleJKArr['CuffsFoldInner'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + $scope.fabricId + "_cuffstyle_1_size__inner__" + styleId + "_inner_view_4.png";
                    $scope.actStyleJKArr['SleevesFold'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";

                    //for Buttons
                    if ($scope.cuffStyleId == "5" || $scope.cuffStyleId == "8" || $scope.cuffStyleId == "10") {
                        //2ButtonCuff ButtonThreadCuff ButtonHoleCuffFold

                        angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonCuff"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/cuffs/Static/Folded_CuffDouble_b1.png");
                        //angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonThreadCuff"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/cuffs/Static/Folded_CuffDoublethread.png");
                        angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonCuffBack"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/cuffs/Static/Back_CuffDouble_b1.png");
                        //angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonThreadCuffBack"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/cuffs/Static/Back_CuffDoublethread.png");
                        angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonHoleCuffFold"]').show();
                        angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonThreadCuffBack"]').show();
                        //angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonHoleFold"]').show();
                        //angular.element("#mainImageHolder").find('.shirt_part[rel="ThreadsFold"]').show();
                        $scope.actStyleJKArr['ButtonThreadCuffBack'] = styleImg = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + $scope.threadcolorId + "_double_buttonhole_view_3.png";
                        $scope.actStyleJKArr['ButtonThreadCuff'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + $scope.threadcolorId + "_double_thread_view_4.png";
                        $scope.actStyleJKArr['ButtonHoleCuffFold'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + $scope.threadcolorId + "_double_buttonhole_view_4.png";
                        //console.log("-----------5-8-10-----------" + $scope.cuffStyleId)

                        $scope.actStyleJKArr['ButtonCuffBack'] = styleImg = basePath + "pub/media/shirt-tool/shirt_images/cuffs/Static/Back_CuffDouble_b1.png";
                        jQuery("#divaccent_4 ul li#2").show();
                        jQuery("#divaccent_4 ul li#3").show();

                    } else if ($scope.cuffStyleId == "1" || $scope.cuffStyleId == "2" || $scope.cuffStyleId == "6" || $scope.cuffStyleId == "9" || $scope.cuffStyleId == "12") {
                        //French 
                        angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonCuff"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/cuffs/Static/Folded_Common_French_Cufflink.png");
                        angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonThreadCuff"]').attr("src", basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png");
                        angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonCuffBack"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/cuffs/Static/Back_Common_French_Cufflink.png");
                        angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonThreadCuffBack"]').attr("src", basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png");
                        angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonHoleCuffFold"]').hide();
                        angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonThreadCuffBack"]').hide();
                        jQuery("#mainImageHolder").find(".shirt_part[rel=ButtonThreadCuff]").hide();
                        console.log("----------1-2-6-9-12------------" + $scope.cuffStyleId)

                        $scope.actStyleJKArr['ButtonCuffBack'] = styleImg = basePath + "pub/media/shirt-tool/shirt_images/cuffs/Static/Back_Common_French_Cufflink.png";
                        jQuery("#divaccent_4 ul li#2").hide();
                        jQuery("#divaccent_4 ul li#3").hide();
                    } else {
                        //BottonSingle 4 3 7 11

                        angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonCuff"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/cuffs/Static/Folded_CuffSingle_b1.png");
                        // angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonThreadCuff"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/cuffs/Static/Folded_CuffSinglethread.png");
                        angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonCuffBack"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/cuffs/Static/Back_CuffSingle_b1.png");
                        //angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonThreadCuffBack"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/cuffs/Static/Back_CuffSinglethread.png");
                        angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonHoleCuffFold"]').show();
                        angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonThreadCuffBack"]').show();
                        angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonThreadCuff"]').show();

                        $scope.actStyleJKArr['ButtonThreadCuffBack'] = styleImg = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + $scope.threadcolorId + "_single_buttonhole_view_3.png";
                        $scope.actStyleJKArr['ButtonThreadCuff'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + $scope.threadcolorId + "_single_buttonhole_view_4.png";
                        $scope.actStyleJKArr['ButtonHoleCuffFold'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + $scope.threadcolorId + "_single_thread_view_4.png";
                        console.log("---------4-3-7-11-------------" + $scope.cuffStyleId)

                        $scope.actStyleJKArr['ButtonCuffBack'] = styleImg = basePath + "pub/media/shirt-tool/shirt_images/cuffs/Static/Back_CuffSingle_b1.png";
                        jQuery("#divaccent_4 ul li#2").show();
                        jQuery("#divaccent_4 ul li#3").show();
                    }

                    //buttons End here
//                angular.element("#mainImageHolder").find("[rel=ButtonCuff]").show();
//                angular.element("#mainImageHolder").find("[rel=ButtonHoleCuffFold]").show();
//                angular.element("#mainImageHolder").find("[rel=ButtonThreadCuffBack]").show();
//                

                    styleImg = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_sleevesstyle_1_size__sleevestype__" + styleId + "_view_1.png";
                    $scope.actStyleJKArr['SleevesSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_sleevesstyle_1_size__sleevestype__" + styleId + "_view_2.png";
                    $scope.actStyleJKArr['SleevesBack'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_sleevesstyle_1_size__sleevestype__" + styleId + "_view_3.png";

                    $scope.actStyleJKArr['SleevePlacketLeft'] = basePath + "pub/media/shirt-tool/shirt_images/sleeveplacket/" + $scope.fabricId + "_sleeve_1_left_view_3.png";
                    $scope.actStyleJKArr['SleevePlacketRight'] = basePath + "pub/media/shirt-tool/shirt_images/sleeveplacket/" + $scope.fabricId + "_sleeve_1_right_view_3.png";

                    jQuery("#divaccent_4 ul li#2").show();
                    jQuery("#divaccent_4 ul li#3").show();
                }


            } else if (parent == "Fit") {

//            if ($scope.activeView == "1") {
                if (styleId == "2") {//1_fitstyle__size__fittype__1_view_1
                    $scope.actStyleJKArr['FitWaisted'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/Static/Shirt_Waistedfit_Front_Mask.png";
                    $scope.actStyleJKArr['FitWaistedSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/Static/Shirt_Waistedfit_Side_Mask.png";
                    $scope.actStyleJKArr['FitWaistedback'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/Static/Shirt_Waistedfit_Back_Mask.png";
                } else {
                    $scope.actStyleJKArr['FitWaisted'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";
                    $scope.actStyleJKArr['FitWaistedSide'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";
                    $scope.actStyleJKArr['FitWaistedback'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";
                }


            } else if (parent == "Pocket") {

                if ($scope.activeView == "1") {

                    if (styleId == "1") {

                        styleImg = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";
                        $scope.actStyleJKArr['PocketSide'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";
                        $scope.actStyleJKArr['PocketFold'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";
                    } else {


                        styleImg = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_pockets_" + styleId + "_view_1.png";
                        $scope.actStyleJKArr['PocketSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_pockets_" + styleId + "_view_2.png";
                        $scope.actStyleJKArr['PocketFold'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_pockets_" + styleId + "_view_4.png";
                    }

                } else if ($scope.activeView == "2") {

                    if (styleId == "1") {

                        $scope.actStyleJKArr['PocketSide'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";
                        angular.element("#view1").find('.shirt_part[rel="Pocket"]').attr("src", basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png");
                        $scope.actStyleJKArr['PocketFold'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";
                    } else {

                        $scope.actStyleJKArr['PocketSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_pockets_" + styleId + "_view_2.png";
                        angular.element("#view1").find('.shirt_part[rel="Pocket"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_pockets_" + styleId + "_view_1.png");
                        $scope.actStyleJKArr['PocketFold'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_pockets_" + styleId + "_view_4.png";
                        //styleImg = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_pockets_" + styleId + "_view_1.png";
                    }
                } else if ($scope.activeView == "3") {

                    if (styleId == "1") {

                        styleImg = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";
                        $scope.actStyleJKArr['PocketSide'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";
                        $scope.actStyleJKArr['PocketFold'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";
                    } else {


                        styleImg = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_pockets_" + styleId + "_view_1.png";
                        $scope.actStyleJKArr['PocketSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_pockets_" + styleId + "_view_2.png";
                        $scope.actStyleJKArr['PocketFold'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_pockets_" + styleId + "_view_4.png";
                    }


                } else if ($scope.activeView == "4") {

                    if (styleId == "1") {

                        $scope.actStyleJKArr['PocketFold'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";
                        $scope.actStyleJKArr['PocketSide'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";
                        angular.element("#view1").find('.shirt_part[rel="Pocket"]').attr("src", basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png");
                    } else {
                        $scope.actStyleJKArr['PocketFold'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_pockets_" + styleId + "_view_4.png";
                        $scope.actStyleJKArr['PocketSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_pockets_" + styleId + "_view_2.png";
                        angular.element("#view1").find('.shirt_part[rel="Pocket"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_pockets_" + styleId + "_view_1.png");
                    }

                }
            } else if (parent == "Plackets") {
                if ($scope.outerplacketfabid === undefined) {
                    $scope.outerplacketfabid = $scope.fabricId;
                } else if (!$scope.isContrast) {
                    // console.log($scope.isContrast)
                    $scope.outerplacketfabid = $scope.fabricId;
                }
//            if ($scope.activeView == "1") {
                if (styleId == "2") {
//                    angular.element("#mainImageHolder").find("[rel=casualPlacket]").show();
//                    angular.element("#mainImageHolder").find("[rel=casualPlacketSide]").show();
//                    $scope.actStyleJKArr['casualPlacket'] = basePath + "pub/media/shirt-tool/shirt_images/collarstyle/casual/" + $scope.fabricId + "_style_1_casual_common_placket_view_1.png";
//                $scope.actStyleJKArr['casualPlacketSide'] = basePath + "pub/media/shirt-tool/shirt_images/collarstyle/casual/" + $scope.fabricId + "_style_1_casual_common_placket_view_2.png";
                    angular.element("#mainImageHolder").find("[rel=Button]").hide();
                    angular.element("#mainImageHolder").find("[rel=BottonSide]").hide();
                    angular.element("#mainImageHolder").find("[rel=BottonFold]").hide();
                    jQuery("#div5 ul:first").find("li:last").show()
                    angular.element("#mainImageHolder").find("[rel=ButtonHole]").hide();
                    angular.element("#mainImageHolder").find("[rel=ButtonHoleSide]").hide();
                    angular.element("#mainImageHolder").find("[rel=ButtonHoleFold]").hide();
                    angular.element("#mainImageHolder").find("[rel=Threads]").hide();
                    angular.element("#mainImageHolder").find("[rel=ThreadsSide]").hide();
                    angular.element("#mainImageHolder").find("[rel=ThreadsFold]").hide();
                    angular.element("#mainImageHolder").find("[rel=casualPlacket]").hide();
                    angular.element("#mainImageHolder").find("[rel=casualPlacketSide]").hide();
                    //angular.element("#viewport #view5").find("[rel=PlacketsZoom]").hide();

                    angular.element("#mainImageHolder").find("[rel=Plackets]").show();
                    angular.element("#mainImageHolder").find("[rel=PlacketsSide]").show();
                    angular.element("#mainImageHolder").find("[rel=PlacketsFold]").show();
                    angular.element("#viewport #view5").find("[rel=PlacketsZoom]").show();

                    angular.element("#viewport #view5").find("[rel=BottonZoomHole]").hide();
                    angular.element("#viewport #view5").find("[rel=BottonZoom]").hide();
                    angular.element("#viewport #view5").find("[rel=BottonZoomThread]").hide();
                } else if (styleId == "3") {
//                    angular.element("#mainImageHolder").find("[rel=casualPlacket]").hide();
//                    angular.element("#mainImageHolder").find("[rel=casualPlacketSide]").hide();
//                    $scope.actStyleJKArr['casualPlacket'] = basePath + "pub/media/shirt-tool/shirt_images/collarstyle/casual/" + $scope.fabricId + "_style_1_casual_common_placket_view_1.png";
//                    $scope.actStyleJKArr['casualPlacketSide'] = basePath + "pub/media/shirt-tool/shirt_images/collarstyle/casual/" + $scope.fabricId + "_style_1_casual_common_placket_view_2.png";
                    angular.element("#mainImageHolder").find("[rel=Plackets]").hide();
                    angular.element("#mainImageHolder").find("[rel=PlacketsSide]").hide();
                    angular.element("#mainImageHolder").find("[rel=PlacketsFold]").hide();
                    angular.element("#mainImageHolder").find("[rel=Button]").show();
                    angular.element("#mainImageHolder").find("[rel=BottonSide]").show();
                    angular.element("#mainImageHolder").find("[rel=BottonFold]").show();
                    jQuery("#div5 ul:first").find("li:last").hide()
                    angular.element("#mainImageHolder").find("[rel=ButtonHole]").show();
                    angular.element("#mainImageHolder").find("[rel=ButtonHoleSide]").show();
                    angular.element("#mainImageHolder").find("[rel=ButtonHoleFold]").show();
                    angular.element("#mainImageHolder").find("[rel=Threads]").show();
                    angular.element("#mainImageHolder").find("[rel=ThreadsSide]").show();
                    angular.element("#mainImageHolder").find("[rel=ThreadsFold]").show();
                    angular.element("#mainImageHolder").find("[rel=casualPlacket]").hide();
                    angular.element("#mainImageHolder").find("[rel=casualPlacketSide]").hide();
                    angular.element("#viewport #view5").find("[rel=PlacketsZoom]").hide();

                    angular.element("#viewport #view5").find("[rel=BottonZoomHole]").show();
                    angular.element("#viewport #view5").find("[rel=BottonZoom]").show();
                    angular.element("#viewport #view5").find("[rel=BottonZoomThread]").show();
                } else {
                    angular.element("#mainImageHolder").find("[rel=Button]").show();
                    angular.element("#mainImageHolder").find("[rel=BottonSide]").show();
                    angular.element("#mainImageHolder").find("[rel=BottonFold]").show();
                    angular.element("#mainImageHolder").find("[rel=Plackets]").show();
                    angular.element("#mainImageHolder").find("[rel=PlacketsSide]").show();
                    angular.element("#mainImageHolder").find("[rel=PlacketsFold]").show();
                    angular.element("#mainImageHolder").find("[rel=casualPlacket]").show();
                    angular.element("#mainImageHolder").find("[rel=casualPlacketSide]").show();
                    $scope.actStyleJKArr['Plackets'] = styleImg = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outerplacketfabid + "_plackets_1_view_1.png";
                    $scope.actStyleJKArr['PlacketsSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outerplacketfabid + "_plackets_" + styleId + "_view_2.png";
                    $scope.actStyleJKArr['PlacketsZoom'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outerplacketfabid + "_plackets_" + styleId + "_view_5.png";

                    // basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.outerplacketfabid + "_plackets_1_view_1.png";
                    $scope.actStyleJKArr['PlacketsFold'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.outerplacketfabid + "_plackets_1_view_4.png";


                    angular.element("#viewport #view5").find("[rel=PlacketsZoom]").show();
                    jQuery("#div5 ul:first").find("li:last").show();
                    angular.element("#mainImageHolder").find("[rel=ButtonHole]").show();
                    angular.element("#mainImageHolder").find("[rel=ButtonHoleSide]").show();
                    angular.element("#mainImageHolder").find("[rel=ButtonHoleFold]").show();

                    angular.element("#viewport #view5").find("[rel=BottonZoomHole]").show();
                    angular.element("#viewport #view5").find("[rel=BottonZoom]").show();
                    angular.element("#viewport #view5").find("[rel=BottonZoomThread]").show();

                    angular.element("#mainImageHolder").find("[rel=casualPlacketSide]").hide();
                    angular.element("#mainImageHolder").find("[rel=casualPlacket]").hide();
                    // angular.element("#mainImageHolder").find("[rel=casualPlacket]").hide();casualPlacket
                    //angular.element("#mainImageHolder").find("[rel=casualPlacketSide]").hide();
                    // $scope.actStyleJKArr['casualPlacket'] = basePath + "pub/media/shirt-tool/shirt_images/collarstyle/casual/" + $scope.fabricId + "_style_1_casual_common_placket_view_1.png";
                    //$scope.actStyleJKArr['casualPlacketSide'] = basePath + "pub/media/shirt-tool/shirt_images/collarstyle/casual/" + $scope.fabricId + "_style_1_casual_common_placket_view_2.png";
                }


            } else if (parent == "Pleats") {

                if ($scope.activeView == "3") {

                    if (styleId == "1") {
                        $scope.actStyleJKArr['PleatsBack'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";
                    } else {
                        $scope.actStyleJKArr['PleatsBack'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + "Shirt_pleats_" + styleId + "_Back.png";
                    }
                } else if ($scope.activeView == "1") {

                    if (styleId == "1") {
                        $scope.actStyleJKArr['PleatsBack'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";
                    } else {
                        $scope.actStyleJKArr['PleatsBack'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + "Shirt_pleats_" + styleId + "_Back.png";
                    }

                } else if ($scope.activeView == "2") {

                    if (styleId == "1") {

                        $scope.actStyleJKArr['PleatsBack'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";
                    } else {
                        $scope.actStyleJKArr['PleatsBack'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + "Shirt_pleats_" + styleId + "_Back.png";
                    }

                } else if ($scope.activeView == "4") {

                    if (styleId == "1") {

                        $scope.actStyleJKArr['PleatsBack'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";
                    } else {
                        $scope.actStyleJKArr['PleatsBack'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + "Shirt_pleats_" + styleId + "_Back.png";
                    }

                }
            } else if (parent == "Bottom")
            {
                if (styleId == "1")
                {
                    //  jQuery("#jeans").trigger("click");
                    angular.element("#mainImageHolder").find("[rel=CasualLook]").hide();
                    angular.element("#mainImageHolder").find("[rel=ManPant]").hide();
                    angular.element("#mainImageHolder").find("[rel=ManPantSide]").hide();
                    angular.element("#mainImageHolder").find("[rel=ManPantBack]").hide();
                    angular.element("#mainImageHolder").find("[rel=Bottom]").show();
                    angular.element("#mainImageHolder").find("[rel=BottomSide]").show();
                    angular.element("#mainImageHolder").find("[rel=BottomBack]").show();


//                $scope.actStyleJKArr['Bottom'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + "Shirt_Bottom_1_" + styleId + ".png";
//                $scope.actStyleJKArr['BottomSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + "Shirt_Bottom_2_" + styleId + ".png";
//                $scope.actStyleJKArr['BottomBack'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + "Shirt_Bottom_3_" + styleId + ".png";
                    if ($scope.IsJeans)
                    {
                        styleImg = basePath + "pub/media/shirt-tool/shirt_images/bottom/Jeans/Shirt_Bottom_1_1.png";
                        angular.element("#mainImageHolder").find('.shirt_part[rel="Bottom"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/bottom/Jeans/Shirt_Bottom_1_1.png");
                        angular.element("#mainImageHolder").find('.shirt_part[rel="BottomSide"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/bottom/Jeans/Shirt_Bottom_2_1.png");
                        angular.element("#mainImageHolder").find('.shirt_part[rel="BottomBack"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/bottom/Jeans/Shirt_Bottom_3_1.png");
                        angular.element("#mainImageHolder").find('.shirt_part[rel="ManPantBack"]').hide();
                    } else {
                        styleImg = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + "Shirt_Bottom_1_" + styleId + ".png";
                        $scope.actStyleJKArr['Bottom'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + "Shirt_Bottom_1_" + styleId + ".png";
                        $scope.actStyleJKArr['BottomSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + "Shirt_Bottom_2_" + styleId + ".png";
                        $scope.actStyleJKArr['BottomBack'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + "Shirt_Bottom_3_" + styleId + ".png";
                    }
                } else if (styleId == "2")
                {
                    // jQuery("#jeans").trigger("click");
                    angular.element("#mainImageHolder").find("[rel=CasualLookSide]").hide();
                    angular.element("#mainImageHolder").find("[rel=ManPant]").hide();
                    angular.element("#mainImageHolder").find("[rel=ManPantSide]").hide();
                    angular.element("#mainImageHolder").find("[rel=ManPantBack]").hide();
                    angular.element("#mainImageHolder").find("[rel=Bottom]").show();
                    angular.element("#mainImageHolder").find("[rel=BottomSide]").show();
                    angular.element("#mainImageHolder").find("[rel=BottomBack]").show();
                    styleImg = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + "Shirt_Bottom_1_" + styleId + ".png";
//                $scope.actStyleJKArr['Bottom'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + "Shirt_Bottom_1_" + styleId + ".png";
//                $scope.actStyleJKArr['BottomSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + "Shirt_Bottom_2_" + styleId + ".png";
//                $scope.actStyleJKArr['BottomBack'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + "Shirt_Bottom_3_" + styleId + ".png";

                    // angular.element("#view1").find('.shirt_part[rel="Bottom"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + "Shirt_Bottom_1_" + styleId + ".png");
                    if ($scope.IsJeans)
                    {
                        styleImg = basePath + "pub/media/shirt-tool/shirt_images/bottom/Jeans/Shirt_Bottom_1_2.png";
                        angular.element("#mainImageHolder").find('.shirt_part[rel="Bottom"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/bottom/Jeans/Shirt_Bottom_1_2.png");
                        angular.element("#mainImageHolder").find('.shirt_part[rel="BottomSide"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/bottom/Jeans/Shirt_Bottom_2_2.png");
                        angular.element("#mainImageHolder").find('.shirt_part[rel="BottomBack"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/bottom/Jeans/Shirt_Bottom_3_2.png");
                        angular.element("#mainImageHolder").find('.shirt_part[rel="ManPantBack"]').hide();

                    } else {
                        styleImg = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + "Shirt_Bottom_1_" + styleId + ".png";
                        $scope.actStyleJKArr['Bottom'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + "Shirt_Bottom_1_" + styleId + ".png";
                        $scope.actStyleJKArr['BottomSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + "Shirt_Bottom_2_" + styleId + ".png";
                        $scope.actStyleJKArr['BottomBack'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + "Shirt_Bottom_3_" + styleId + ".png";

                    }

                }
//            } else if ($scope.activeView == "3") {
//
//                angular.element("#mainImageHolder").find("[rel=CasualLookBack]").hide();
//                angular.element("#mainImageHolder").find("[rel=ManPantBack]").hide();
//                angular.element("#mainImageHolder").find("[rel=BottomBack]").show();
//                $scope.actStyleJKArr['BottomBack'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + "Shirt_Bottom_3_" + styleId + ".png";
//                $scope.actStyleJKArr['BottomSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + "Shirt_Bottom_2_" + styleId + ".png";
//                angular.element("#view1").find('.shirt_part[rel="Bottom"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + "Shirt_Bottom_1_" + styleId + ".png");
//            } else if ($scope.activeView == "4") {
//                angular.element("#mainImageHolder").find("[rel=CasualLook]").hide();
//                angular.element("#mainImageHolder").find("[rel=CasualLookSide]").hide();
//                angular.element("#mainImageHolder").find("[rel=CasualLookBack]").hide();
//                angular.element("#mainImageHolder").find("[rel=Bottom]").show();
//                angular.element("#mainImageHolder").find("[rel=BottomSide]").show();
//                angular.element("#mainImageHolder").find("[rel=BottomBack]").show();
//                angular.element("#view1").find('.shirt_part[rel="Bottom"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + "Shirt_Bottom_1_" + styleId + ".png");
//                $scope.actStyleJKArr['BottomBack'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + "Shirt_Bottom_3_" + styleId + ".png";
//                $scope.actStyleJKArr['BottomSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + "Shirt_Bottom_2_" + styleId + ".png";
//            }

            }


            if (styleType == "shirt")
            {

                $scope.actStyleJKArr[parent] = styleImg;
                $scope.actStyleJKArr[parent + "_index"] = ind;
                $scope.actStyleJKArr[parent + '_styleName'] = styleName;
                $scope.actStyleJKArr[parent + '_id'] = styleId;
                $scope.actStyleJKArr[parent + '_btnCount'] = $scope.activeStyle.btn_count;
            } else if (styleType == "pant") {

                $scope.actStylePantArr[parent] = styleImg;
                $scope.actStylePantArr[parent + "_index"] = ind;
                $scope.actStylePantArr[parent + '_styleName'] = styleName;
                $scope.actStylePantArr[parent + '_id'] = styleId;
            } else if (styleType == "vest") {

                $scope.actStyleVestArr[parent] = styleImg;
                $scope.actStyleVestArr[parent + "_index"] = ind;
                $scope.actStyleVestArr[parent + '_styleName'] = styleName;
                $scope.actStyleVestArr[parent + '_id'] = styleId;
                $scope.actStyleVestArr[parent + '_btnCount'] = $scope.activeStyle.btn_count;
            }

            updatePrice();
            updateProduct();
            //hideJacket();
            imageLoaded();
        }
        /*change mono text*/

        $scope.changeMonoTxt = function (txtVal) {


            if (isMono) {
                if (jQuery("#radio-1").prop("checked", true) && jQuery("#radio-2").prop("checked", true) && jQuery("#radio-3").prop("checked", true) && jQuery("#radio-4").prop("checked", true)) {
                    jQuery("#radio-1").prop("checked", true);
                    var id = jQuery("#radio-1").attr("id");
                    $scope.changeTextFont(id);
                }
                if (jQuery("#radio-10").prop("checked", true) && jQuery("#radio-11").prop("checked", true) && jQuery("#radio-12").prop("checked", true) && jQuery("#radio-13").prop("checked", true)) {
                    jQuery("#radio-10").prop("checked", true);
                    $scope.changeTextPos();
                    angular.element("#monoColorHolder ul li").eq(2).addClass("activeDesign")//trigger("click");
                    threadColor = "Blue";
                }
                isMono = false;
            }
            var yourInput = txtVal;//jQuery('#data').val();
            var re = /[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi;
            var isSplChar = re.test(yourInput);

            if (isSplChar)
            {
                var no_spl_char = yourInput.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
                txtVal = no_spl_char;
                jQuery('#data').val(no_spl_char);

                console.log("no_spl_char " + no_spl_char + " txtVal " + txtVal);
            }

            // if (!angular.element("#mainImageHolder .shirt_part[rel=Monogram]").hasClass("myClass")) {
            //     angular.element("#mainImageHolder .shirt_part[rel=Monogram]").addClass("myClass");
            // }

            angular.element("#clearText").on("click", function () {

                angular.element("#data").val("");
                $scope.monogramText = "";
                txtVal = "";
                angular.element("#mainImageHolder").find("[rel=Monogram]").hide();
                updatePrice();
            });

            if (txtVal === undefined || threadColor === undefined || textFont === undefined)
                    //alert("hi")
                    {

                        txtVal = "";
                        threadColor = "Blue";
                        textFont = 'black_jack.ttf';

                    }

//     $scope.initPositionOfText();


            // angular.element("#radio-1").is("checked");

            console.log("txtVal - = " + txtVal);

            angular.element("#mainImageHolder .shirt_part[rel=Monogram]").attr("name", txtVal);
            angular.element("#mainImageHolder .shirt_part[rel=Monogram]").attr("src", basePath + "custom_ajax/create_txt_img.php?txt=" + txtVal + "&color=" + threadColor + "&font=" + textFont + "&pos=" + MonoClass());




            updatePrice();

            monotext = txtVal;
            angular.element("#mainImageHolder").find("[rel=Monogram]").show();
        }

        function MonoClass() {
            var myString = angular.element("#mainImageHolder .shirt_part[rel=Monogram]").attr('class');
            if (myString == "shirt_part part") {
                angular.element("#mainImageHolder .shirt_part[rel=Monogram]").addClass("highPos");
                var myString = angular.element("#mainImageHolder .shirt_part[rel=Monogram]").attr('class');
            }
            var arr = myString.split(' ');
            var strFile = arr[arr.length - 1];
            return strFile;
        }
        /*chnage mono font*/
        $scope.changeTextFont = function (Id)
        {
            //alert("Id=" + Id);
            $scope.actFontId = Id;
            changeMonoTextFont(Id);
        }
        $scope.changeTextPos = function (pos)
        {
            // console.log("pos=" + pos);
            // MonoPosition = pos;
//alert("hi")
            if (active_view_Id == "1") {
                //alert("hi")
                if (jQuery("#radio-10").is(':checked')) {//if (pos == "High") {
                    angular.element("#mainImageHolder .shirt_part[rel=Monogram]").removeClass("highPos");
                    angular.element("#mainImageHolder .shirt_part[rel=Monogram]").addClass("highPos");

                    angular.element("#mainImageHolder .shirt_part[rel=Monogram]").removeClass("mediumPos");
                    angular.element("#mainImageHolder .shirt_part[rel=Monogram]").removeClass("lowPos");
                    angular.element("#mainImageHolder .shirt_part[rel=Monogram]").removeClass("cuffPos");
                    jQuery("#main-container .shirt_part[rel=Monogram]").attr("src", basePath + "custom_ajax/create_txt_img.php?txt=" + monotext + "&color=" + threadColor + "&font=" + textFont + "&pos=" + "highPos");

                } else if (jQuery("#radio-11").is(':checked'))//if (pos == "Medium")
                {
                    angular.element("#mainImageHolder .shirt_part[rel=Monogram]").addClass("mediumPos");

                    angular.element("#mainImageHolder .shirt_part[rel=Monogram]").removeClass("lowPos");
                    angular.element("#mainImageHolder .shirt_part[rel=Monogram]").removeClass("cuffPos");
                    angular.element("#mainImageHolder .shirt_part[rel=Monogram]").removeClass("highPos");
                    jQuery("#main-container .shirt_part[rel=Monogram]").attr("src", basePath + "custom_ajax/create_txt_img.php?txt=" + monotext + "&color=" + threadColor + "&font=" + textFont + "&pos=" + "mediumPos");
                } else if (jQuery("#radio-12").is(':checked'))//if (pos == "Low")
                {
                    angular.element("#mainImageHolder .shirt_part[rel=Monogram]").addClass("lowPos");

                    angular.element("#mainImageHolder .shirt_part[rel=Monogram]").removeClass("cuffPos");
                    angular.element("#mainImageHolder .shirt_part[rel=Monogram]").removeClass("highPos");
                    angular.element("#mainImageHolder .shirt_part[rel=Monogram]").removeClass("mediumPos");
                    jQuery("#main-container .shirt_part[rel=Monogram]").attr("src", basePath + "custom_ajax/create_txt_img.php?txt=" + monotext + "&color=" + threadColor + "&font=" + textFont + "&pos=" + "lowPos");

                } else if (jQuery("#radio-13").is(':checked'))
                {
                    angular.element("#mainImageHolder .shirt_part[rel=Monogram]").addClass("cuffPos");

                    angular.element("#mainImageHolder .shirt_part[rel=Monogram]").removeClass("highPos");
                    angular.element("#mainImageHolder .shirt_part[rel=Monogram]").removeClass("mediumPos");
                    angular.element("#mainImageHolder .shirt_part[rel=Monogram]").removeClass("lowPos");
                    jQuery("#main-container .shirt_part[rel=Monogram]").attr("src", basePath + "custom_ajax/create_txt_img.php?txt=" + monotext + "&color=" + threadColor + "&font=" + textFont + "&pos=" + "cuffPos");
                }
            }

        }

        $scope.changeTextColor = function ($event, color)
        {
            if (jQuery("#data").val() == "") {

//            alert("please enter text first");
            } else {
                angular.element("#monoColorHolder ul").find(".activeDesign").removeClass("activeDesign");
                angular.element($event.currentTarget).addClass("activeDesign");
                threadColor = color;
                jQuery("#main-container .shirt_part[rel=Monogram]").attr("src", basePath + "custom_ajax/create_txt_img.php?txt=" + monotext + "&color=" + threadColor + "&font=" + textFont + "&pos=" + MonoClass());
                //jQuery("#main-container .shirt_part[rel=MonogramFold]").attr("src", basePath + "custom_ajax/create_txt_img.php?txt=" + monotext + "&color=" + threadColor + "&font=" + textFont);

            }

            updatePrice();

        }
        $scope.initPositionOfText = function ()
        {

            if (active_view_Id == "1" && monotext == "") {
                // alert("hi")
                angular.element("#data").on("keyup", function () {
                    //alert("hi")
                    angular.element("#radio-1").prop("checked", true);//trigger("click");
                    angular.element("#monoColorHolder ul li").eq(2).trigger("click");
                    angular.element("#radio-10").prop("checked", true);//trigger("click");

                });
            }

            angular.element('#data').keypress(function () {
                var $th = angular.element(this);
                $th.val($th.val().replace(/[^a-zA-Z0-9]/g, function (str) {
                    return '';
                }));
            });


        }

        function changeMonoTextFont(Id) {

            var monoTextLabel = "Exclusively tailored for";
            if ($scope.fonts) {
                for (var kk = 0; kk < $scope.fonts.length; kk++) {
                    if ($scope.fonts[kk].id == Id) {
                        textFont = $scope.fonts[kk].font;
                        break;
                    }
                }


                //$scope.monogramText
                jQuery("#main-container .shirt_part[rel=Monogram]").attr("textFont", jQuery(".monoFont").find("span[data-id=" + Id + "] .ng-binding").text())
                jQuery(".monoFont").find("span[data-id=" + Id + "] ").addClass("active");
                jQuery("#main-container .shirt_part[rel=Monogram]").attr("src", basePath + "custom_ajax/create_txt_img.php?txt=" + monotext + "&color=" + threadColor + "&font=" + textFont + "&pos=" + MonoClass());

            }
        }
        $scope.changeCuffContrast = function ($event, obj) {

            angular.element(".Cuffs").find(".activeDesign").removeClass('activeDesign');
            angular.element($event.currentTarget).find(".fabvie").addClass("activeDesign");

            var styleId;

            var parent = "Cuffs";

            var styleTypeId = angular.element("#divs_2").find('.activeDesign').attr("id");
            if ($scope.threadcolorId == undefined)
            {
                $scope.threadcolorId = "4";
            }


            styleId = angular.element('#divaccent_3 ul:first').find('.activeDesign').attr('id') ? angular.element('#divaccent_3 ul:first').find('.activeDesign').attr('id') : "1";


            angular.element('#divaccent_3 ul.Cuffs').css("display", "block");



            /* $scope.actStyleJKArr['Cuffs'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.cuffFabricId + "_cuffstyle_1_size__cufftype__" + styleTypeId + "_view_1.png";
             $scope.actStyleJKArr['CuffsSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.cuffFabricId + "_cuffstyle_1_size__cufftype__" + styleTypeId + "_view_2.png";
             $scope.actStyleJKArr['CuffsBack'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.cuffFabricId + "_cuffstyle_1_size__cufftype__" + styleTypeId + "_view_3.png";
             */
            var styleImg;

            console.log("styleId = " + styleId)

            if (styleId == "2")//all
            {

                $scope.outercufffabid = obj.id;
                $scope.innercufffabid = obj.id;

                styleImg = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercufffabid + "_cuffstyle_1_size__cufftype__" + styleTypeId + "_view_1.png";

                $scope.actStyleJKArr['Cuffs'] = styleImg;

                $scope.actStyleJKArr['CuffsSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercufffabid + "_cuffstyle_1_size__cufftype__" + styleTypeId + "_view_2.png";
                $scope.actStyleJKArr['CuffsBack'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercufffabid + "_cuffstyle_1_size__cufftype__" + styleTypeId + "_view_3.png";
                $scope.actStyleJKArr['CuffsFold'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercufffabid + "_cuffstyle_1_size__cufftype__" + styleTypeId + "_view_4.png";
                $scope.actStyleJKArr['CuffsFoldInner'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercufffabid + "_cuffstyle_1_size__inner__" + styleTypeId + "_inner_view_4.png";


                //for Buttons
                if (styleTypeId == "5" || styleTypeId == "8" || styleTypeId == "10") {
                    //2ButtonCuff ButtonThreadCuff ButtonHoleCuffFold

                    $scope.actStyleJKArr['ButtonCuffBack'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/Static/Back_CuffDouble_b1.png";
                    angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonHoleCuffFold"]').show();
                    angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonThreadCuffBack"]').show();

                    $scope.actStyleJKArr['ButtonThreadCuffBack'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + $scope.threadcolorId + "_double_buttonhole_view_3.png";


                } else if (styleTypeId == "1" || styleTypeId == "2" || styleTypeId == "6" || styleTypeId == "9" || styleTypeId == "12") {
                    //French 
                    $scope.actStyleJKArr['ButtonCuffBack'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/Static/Back_Common_French_Cufflink.png";
                    $scope.actStyleJKArr['ButtonThreadCuffBack'] = basePath + "pub/media/shirt-tool/default_shirttool_image/woman/blank.png";
                    angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonHoleCuffFold"]').hide();
                    angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonThreadCuffBack"]').hide();

                } else {
                    //BottonSingle 4 3 7 11


                    $scope.actStyleJKArr['ButtonCuffBack'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/Static/Back_CuffSingle_b1.png";
                    angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonThreadCuffBack"]').show();
                    $scope.actStyleJKArr['ButtonThreadCuffBack'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + $scope.threadcolorId + "_single_buttonhole_view_3.png";

                }

                //buttons End here
                $scope.actStyleJKArr['CuffsOutBack'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";


            } else if (styleId == "3") {//outer
                $scope.outercufffabid = obj.id;


                $scope.actStyleJKArr['Cuffs'] = styleImg = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercufffabid + "_cuffstyle_1_size__cufftype__" + styleTypeId + "_view_1.png";



                $scope.actStyleJKArr['CuffsSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercufffabid + "_cuffstyle_1_size__cufftype__" + styleTypeId + "_view_2.png";
                $scope.actStyleJKArr['CuffsBack'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercufffabid + "_cuffstyle_1_size__cufftype__" + styleTypeId + "_view_3.png";
                $scope.actStyleJKArr['CuffsFold'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercufffabid + "_cuffstyle_1_size__cufftype__" + styleTypeId + "_view_4.png";
                $scope.actStyleJKArr['CuffsFoldInner'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_cuffstyle_1_size__inner__" + styleTypeId + "_inner_view_4.png";


                //for Buttons
                if (styleTypeId == "5" || styleTypeId == "8" || styleTypeId == "10") {
                    //2ButtonCuff ButtonThreadCuff ButtonHoleCuffFold

                    $scope.actStyleJKArr['ButtonCuffBack'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/Static/Back_CuffDouble_b1.png";
                    angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonHoleCuffFold"]').show();
                    angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonThreadCuffBack"]').show();

                    $scope.actStyleJKArr['ButtonThreadCuffBack'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + $scope.threadcolorId + "_double_buttonhole_view_3.png";


                } else if (styleTypeId == "1" || styleTypeId == "2" || styleTypeId == "6" || styleTypeId == "9" || styleTypeId == "12") {
                    //French 
                    $scope.actStyleJKArr['ButtonCuffBack'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/Static/Back_Common_French_Cufflink.png";
                    $scope.actStyleJKArr['ButtonThreadCuffBack'] = basePath + "pub/media/shirt-tool/default_shirttool_image/woman/blank.png";
                    angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonHoleCuffFold"]').hide();
                    angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonThreadCuffBack"]').hide();

                } else {
                    //BottonSingle 4 3 7 11


                    $scope.actStyleJKArr['ButtonCuffBack'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/Static/Back_CuffSingle_b1.png";
                    angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonThreadCuffBack"]').show();
                    $scope.actStyleJKArr['ButtonThreadCuffBack'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + $scope.threadcolorId + "_single_buttonhole_view_3.png";

                }

                //buttons End here
                $scope.actStyleJKArr['CuffsOutBack'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";

            } else if (styleId == "4") {//inner                                                                        1_cuffstyle_1_size__opencuff__1_final_back_cuff

                $scope.innercufffabid = obj.id;

                $scope.actStyleJKArr['Cuffs'] = styleImg = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.innercufffabid + "_cuffstyle_1_size__opencuff__1_final_front_cuff.png";



                $scope.actStyleJKArr['CuffsSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.innercufffabid + "_cuffstyle_1_size__opencuff__1_final_side_cuff.png";

                $scope.actStyleJKArr['CuffsBack'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.innercufffabid + "_cuffstyle_1_size__opencuff__1_final_back_cuff.png";
                //1_cuffstyle_1_size__contrast_outer_cuff__1_final_back_cuff.png
                $scope.actStyleJKArr['CuffsOutBack'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_cuffstyle_1_size__contrast_outer_cuff__1_final_back_cuff.png";
                $scope.actStyleJKArr['CuffsFold'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_cuffstyle_1_size__cufftype__" + styleTypeId + "_view_4.png";
                $scope.actStyleJKArr['CuffsFoldInner'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.innercufffabid + "_cuffstyle_1_size__inner__" + styleTypeId + "_inner_view_4.png";
                //angular.element("#mainImageHolder").find("rel=CuffsOutBack").show();
                $scope.actStyleJKArr['ButtonCuffBack'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";
                $scope.actStyleJKArr['ButtonThreadCuffBack'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";

            }

            updateProduct();


        }
        $scope.changeThreadContrast = function ($event, obj) {

            angular.element(".Threads").find(".activeDesign").removeClass('activeDesign');
            angular.element($event.currentTarget).find(".fabvie").addClass("activeDesign");

            var styleId;

            var parent = "Threads";

            var styleTypeId = angular.element("#divs_2").find('.activeDesign').attr("id");



            styleId = angular.element('#divaccent_4 ul:first').find('.activeDesign').attr('id') ? angular.element('#divaccent_4 ul:first').find('.activeDesign').attr('id') : "1";


            angular.element('#divaccent_4 ul.Threads').css("display", "none");



            /* $scope.actStyleJKArr['Cuffs'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.cuffFabricId + "_cuffstyle_1_size__cufftype__" + styleTypeId + "_view_1.png";
             $scope.actStyleJKArr['CuffsSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.cuffFabricId + "_cuffstyle_1_size__cufftype__" + styleTypeId + "_view_2.png";
             $scope.actStyleJKArr['CuffsBack'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.cuffFabricId + "_cuffstyle_1_size__cufftype__" + styleTypeId + "_view_3.png";
             */
            var styleImg;

            console.log("styleId = " + styleId + " styleTypeId =" + styleTypeId);

            $scope.threadcolorId = angular.element('#divaccent_4 ul:first').find('.activeDesign').attr('id') ? angular.element('#divaccent_4 ul:first').find('.activeDesign').attr('id') : "1";

            if (styleId == "1")
            {
                angular.element('#divaccent_4 ul.Threads').css("display", "none");
//                styleImg = $scope.actStyleJKArr['ButtonHole'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_buttonhole_view_1.png";
//                $scope.actStyleJKArr['ButtonHole'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_buttonhole_view_1.png";
//                
//                //$scope.actStyleJKArr['Threads'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_thread_view_1.png";
//                $scope.actStyleJKArr['ButtonHoleSide'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_buttonhole_view_2.png";
//                $scope.actStyleJKArr['ButtonHoleFold'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_buttonhole_view_4.png";
//                $scope.actStyleJKArr['BottonZoomHole'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_buttonhole_view_5.png";
//                
//                $scope.actStyleJKArr['BottonZoomThread'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_thread_view_5.png";
//                $scope.actStyleJKArr['Threads'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_thread_view_1.png";
//                $scope.actStyleJKArr['ThreadsSide'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_thread_view_2.png";
//                $scope.actStyleJKArr['ThreadsFold'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_thread_view_4.png";                                                    
//                //shirt_images/cuffs/1_buttonhole_view_3.png
//                $scope.actStyleJKArr['ButtonThreadCuffBack'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + $scope.threadcolorId + "_buttonhole_view_3.png";
//                $scope.actStyleJKArr['ButtonThreadCuff'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + $scope.threadcolorId + "_thread_view_4.png";
//                $scope.actStyleJKArr['ButtonHoleCuffFold'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + $scope.threadcolorId + "_buttonhole_view_4.png";
//                            //Threads


                //for Buttons
                if (styleTypeId == "5" || styleTypeId == "8" || styleTypeId == "9" || styleTypeId == "10") {
                    //2ButtonCuff ButtonThreadCuff ButtonHoleCuffFold

                    angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonCuff"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/cuffs/Static/Folded_CuffDouble_b1.png");
                    angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonThreadCuff"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/cuffs/Static/Folded_CuffDoublethread.png");
                    angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonCuffBack"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/cuffs/Static/Back_CuffDouble_b1.png");
                    angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonThreadCuffBack"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/cuffs/Static/Back_CuffDoublethread.png");
                    angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonHoleCuffFold"]').show();
                    angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonThreadCuffBack"]').show();
                    //angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonHoleFold"]').show();
                    //angular.element("#mainImageHolder").find('.shirt_part[rel="ThreadsFold"]').show();
                    jQuery("#divaccent_4 ul li#2").show();
                    jQuery("#divaccent_4 ul li#3").show();

                } else if (styleTypeId == "1" || styleTypeId == "2" || styleTypeId == "6" || styleTypeId == "9" || styleTypeId == "12") {
                    //French 
                    angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonCuff"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/cuffs/Static/Folded_Common_French_Cufflink.png");
                    angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonThreadCuff"]').attr("src", basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png");
                    angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonCuffBack"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/cuffs/Static/Back_Common_French_Cufflink.png");
                    angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonThreadCuffBack"]').attr("src", basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png");
                    angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonHoleCuffFold"]').hide();
                    angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonThreadCuffBack"]').hide();
                    //angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonHoleFold"]').hide();
                    //angular.element("#mainImageHolder").find('.shirt_part[rel="ThreadsFold"]').hide();
                    jQuery("#divaccent_4 ul li#2").hide();
                    jQuery("#divaccent_4 ul li#3").hide();
                } else {
                    //BottonSingle6 4 3 7 11

                    angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonCuff"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/cuffs/Static/Folded_CuffSingle_b1.png");
                    angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonThreadCuff"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/cuffs/Static/Folded_CuffSinglethread.png");
                    angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonCuffBack"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/cuffs/Static/Back_CuffSingle_b1.png");
                    angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonThreadCuffBack"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/cuffs/Static/Back_CuffSinglethread.png");
                    angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonHoleCuffFold"]').show();
                    angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonThreadCuffBack"]').show();
                    //angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonHoleFold"]').show();
                    //angular.element("#mainImageHolder").find('.shirt_part[rel="ThreadsFold"]').show();
                    jQuery("#divaccent_4 ul li#2").show();
                    jQuery("#divaccent_4 ul li#3").show();
                }
                //buttons End here            

                $scope.actStyleJKArr['SleevePlacketThreadLeft'] = basePath + "pub/media/shirt-tool/shirt_images/sleeveplacket/4_placket_thread_left_view_3.png";
                $scope.actStyleJKArr['SleevePlacketThreadRight'] = basePath + "pub/media/shirt-tool/shirt_images/sleeveplacket/4_placket_thread_right_view_3.png";





            } else if (styleId == "2")
            {//all
                angular.element('#divaccent_4 ul.Threads').css("display", "block");
                $scope.threadcolorId = angular.element(".Threads").find(".activeDesign").parent().attr("id") ? angular.element(".Threads").find(".activeDesign").parent().attr("id") : angular.element(".Threads threadcolor:first").attr("id");
                //$scope.threadcolorId = angular.element(".Threads").find(".activeDesign").parent().attr("id") ? angular.element(".Threads").find(".activeDesign").parent().attr("id") : angular.element(".Threads fabricviewaccent:first").attr("id");
                //shirt_images/plackets/1_buttonhole_view_1.png
                styleImg = $scope.actStyleJKArr['ButtonHole'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_buttonhole_view_1.png";
                $scope.actStyleJKArr['ButtonHole'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_buttonhole_view_1.png";
                $scope.actStyleJKArr['ButtonHoleSide'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_buttonhole_view_2.png";
                $scope.actStyleJKArr['ButtonHoleFold'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_buttonhole_view_4.png";
                $scope.actStyleJKArr['BottonZoomHole'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_buttonhole_view_5.png";

                $scope.actStyleJKArr['BottonZoomThread'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_thread_view_5.png";
                $scope.actStyleJKArr['Threads'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_thread_view_1.png";
                $scope.actStyleJKArr['ThreadsSide'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_thread_view_2.png";
                $scope.actStyleJKArr['ThreadsFold'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_thread_view_4.png";
                //
                $scope.actStyleJKArr['BottonBaseZoomHole'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_base_final_buttonhole_view_5.png";

                $scope.actStyleJKArr['BottonBaseZoomThread'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_base_final_buttonthread_view_5.png";

//shirt_images/cuffs/1_buttonhole_view_3.png
                //$scope.actStyleJKArr['ButtonThreadCuffBack'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + $scope.threadcolorId + "_buttonhole_view_3.png";
                // $scope.actStyleJKArr['ButtonThreadCuff'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + $scope.threadcolorId + "_thread_view_4.png";
                //$scope.actStyleJKArr['ButtonHoleCuffFold'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + $scope.threadcolorId + "_buttonhole_view_4.png";   

                if (styleTypeId == "5" || styleTypeId == "8" || styleTypeId == "9" || styleTypeId == "10") {
                    //2ButtonCuff 
                    $scope.actStyleJKArr['ButtonThreadCuffBack'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + $scope.threadcolorId + "_double_buttonhole_view_3.png";
                    $scope.actStyleJKArr['ButtonThreadCuff'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + $scope.threadcolorId + "_double_thread_view_4.png";
                    $scope.actStyleJKArr['ButtonHoleCuffFold'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + $scope.threadcolorId + "_double_buttonhole_view_4.png";


                } else if (styleTypeId == "1" || styleTypeId == "2" || styleTypeId == "6" || styleTypeId == "9" || styleTypeId == "12") {
                    //French 
                    jQuery("#mainImageHolder").find(".shirt_part[rel=ButtonThreadCuff]").hide();
                    jQuery("#mainImageHolder").find(".shirt_part[rel=ButtonHoleCuffFold]").hide();
                } else {
                    //BottonSingle6 4 3 7 11
                    $scope.actStyleJKArr['ButtonThreadCuffBack'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + $scope.threadcolorId + "_single_buttonhole_view_3.png";
                    $scope.actStyleJKArr['ButtonThreadCuff'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + $scope.threadcolorId + "_single_thread_view_4.png";
                    $scope.actStyleJKArr['ButtonHoleCuffFold'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + $scope.threadcolorId + "_single_buttonhole_view_4.png";

                    //$scope.actStyleJKArr['ButtonThreadCuff'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + $scope.threadcolorId + "_single_thread_view_4.png";
                }//button end here

                $scope.actStyleJKArr['SleevePlacketThreadLeft'] = basePath + "pub/media/shirt-tool/shirt_images/sleeveplacket/" + $scope.threadcolorId + "_placket_thread_left_view_3.png";
                $scope.actStyleJKArr['SleevePlacketThreadRight'] = basePath + "pub/media/shirt-tool/shirt_images/sleeveplacket/" + $scope.threadcolorId + "_placket_thread_right_view_3.png";

                $scope.actStyleJKArr['SingleButtonHole'] = basePath + "pub/media/shirt-tool/shirt_images/collarstyle/" + $scope.threadcolorId + "_single_thread_view_1.png";
                $scope.actStyleJKArr['SingleButtonHoleSide'] = basePath + "pub/media/shirt-tool/shirt_images/collarstyle/" + $scope.threadcolorId + "_single_thread_view_2.png";



            } else if (styleId == "3")
            {//cuff
                angular.element('#divaccent_4 ul.Threads').css("display", "block");
                //  angular.element(".Threads").find(".activeDesign").parent().attr("id") ? angular.element(".Threads").find(".activeDesign").parent().attr("id") : angular.element(".Threads threadcolor:first").attr("id");
                $scope.threadcolorId = angular.element(".Threads").find(".activeDesign").parent().attr("id") ? angular.element(".Threads").find(".activeDesign").parent().attr("id") : angular.element(".Threads threadcolor:first").attr("id");

                styleImg = $scope.actStyleJKArr['ButtonThreadCuffBack'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + $scope.threadcolorId + "_buttonhole_view_3.png";
                // $scope.actStyleJKArr['ButtonThreadCuff'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + $scope.threadcolorId + "_thread_view_4.png";
                //$scope.actStyleJKArr['ButtonHoleCuffFold'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + $scope.threadcolorId + "_buttonhole_view_4.png";
//                styleImg = $scope.actStyleJKArr['ButtonHoleSide'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";
//                styleImg = $scope.actStyleJKArr['ButtonHoleFold'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";
//                styleImg = $scope.actStyleJKArr['BottonZoomThread'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";
                if (styleTypeId == "5" || styleTypeId == "8" || styleTypeId == "9" || styleTypeId == "10") {
                    //2ButtonCuff 
                    $scope.actStyleJKArr['ButtonThreadCuffBack'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + $scope.threadcolorId + "_double_buttonhole_view_3.png";
                    $scope.actStyleJKArr['ButtonThreadCuff'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + $scope.threadcolorId + "_double_thread_view_4.png";
                    $scope.actStyleJKArr['ButtonHoleCuffFold'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + $scope.threadcolorId + "_double_buttonhole_view_4.png";


                } else if (styleTypeId == "1" || styleTypeId == "2" || styleTypeId == "6" || styleTypeId == "9" || styleTypeId == "12") {
                    //French 
                    jQuery("#mainImageHolder").find(".shirt_part[rel=ButtonThreadCuff]").hide();
                    jQuery("#mainImageHolder").find(".shirt_part[rel=ButtonHoleCuffFold]").hide();
                } else {
                    //BottonSingle6 4 3 7 11
                    $scope.actStyleJKArr['ButtonThreadCuffBack'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + $scope.threadcolorId + "_single_buttonhole_view_3.png";
                    $scope.actStyleJKArr['ButtonThreadCuff'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + $scope.threadcolorId + "_single_thread_view_4.png";
                    $scope.actStyleJKArr['ButtonHoleCuffFold'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + $scope.threadcolorId + "_single_buttonhole_view_4.png";

                    //$scope.actStyleJKArr['ButtonThreadCuff'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + $scope.threadcolorId + "_single_thread_view_4.png";
                }//button end here

                $scope.actStyleJKArr['SleevePlacketThreadLeft'] = basePath + "pub/media/shirt-tool/shirt_images/sleeveplacket/" + $scope.threadcolorId + "_placket_thread_left_view_3.png";
                $scope.actStyleJKArr['SleevePlacketThreadRight'] = basePath + "pub/media/shirt-tool/shirt_images/sleeveplacket/" + $scope.threadcolorId + "_placket_thread_right_view_3.png";

            } else if (styleId == "4")
            {//placket thread
                angular.element('#divaccent_4 ul.Threads').css("display", "block");
                $scope.threadcolorId = angular.element(".Threads").find(".activeDesign").parent().attr("id") ? angular.element(".Threads").find(".activeDesign").parent().attr("id") : angular.element(".Threads threadcolor:first").attr("id");

                //styleImg = $scope.actStyleJKArr['ButtonHole'] = basePath + "pub/media/shirt-tool/shirt_images/elbowpatches/" + $scope.backelbofabId + "_elbowPatches_view_3.png";
                styleImg = $scope.actStyleJKArr['ButtonHole'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_buttonhole_view_1.png";
                $scope.actStyleJKArr['ButtonHole'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_buttonhole_view_1.png";
                $scope.actStyleJKArr['ButtonHoleSide'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_buttonhole_view_2.png";
                $scope.actStyleJKArr['ButtonHoleFold'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_buttonhole_view_4.png";
                $scope.actStyleJKArr['BottonZoomHole'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_buttonhole_view_5.png";

                $scope.actStyleJKArr['BottonZoomThread'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_thread_view_5.png";
                $scope.actStyleJKArr['Threads'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_thread_view_1.png";
                $scope.actStyleJKArr['ThreadsSide'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_thread_view_2.png";
                $scope.actStyleJKArr['ThreadsFold'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_thread_view_4.png";

                $scope.actStyleJKArr['SingleButtonHole'] = basePath + "pub/media/shirt-tool/shirt_images/collarstyle/" + $scope.threadcolorId + "_single_thread_view_1.png";
                $scope.actStyleJKArr['SingleButtonHoleSide'] = basePath + "pub/media/shirt-tool/shirt_images/collarstyle/" + $scope.threadcolorId + "_single_thread_view_2.png";

                $scope.actStyleJKArr['BottonBaseZoomHole'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_base_final_buttonhole_view_5.png";

                $scope.actStyleJKArr['BottonBaseZoomThread'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_base_final_buttonthread_view_5.png";

            }
            updateProduct();


        }
        $scope.changeElbowContrast = function ($event, obj) {




            var styleImg, parent = "backelbow";

            $scope.backelbofabId = obj.id;


            angular.element(".fabcolelbow").find(".activeDesign").removeClass('activeDesign');
            angular.element($event.currentTarget).find(".fabvie").addClass("activeDesign");


            var styleId = angular.element('#divaccent_5 ul:first').find('.activeDesign').attr('id') ? angular.element('#divaccent_5 ul:first').find('.activeDesign').attr('id') : "1";

            if (styleId == 2)
            {

                styleImg = $scope.actStyleJKArr['backelbow'] = basePath + "pub/media/shirt-tool/shirt_images/elbowpatches/" + $scope.backelbofabId + "_elbowPatches_view_3.png";
            } else
            {

                styleImg = $scope.actStyleJKArr['backelbow'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";
            }




            updateProduct();

        }

        $scope.changePlacketContrast = function ($event, obj) {



            angular.element(".FrontPlacket").find(".activeDesign").removeClass('activeDesign');
            angular.element($event.currentTarget).find(".fabvie").addClass("activeDesign");

            var styleId = angular.element('#divaccent_7 ul:first').find('.activeDesign').attr('id') ? angular.element('#divaccent_7 ul:first').find('.activeDesign').attr('id') : "1";

            var parent = "FrontPlacket";

            var styleImg;

            //$scope.actStyleJKArr[parent] = styleImg;

//        $scope.actStyleJKArr['casualBase'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/casual/" + $scope.fabricId + "_style_casual_common_torso_view_1.png";
//        $scope.actStyleJKArr['casualBaseSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/casual/" + $scope.fabricId + "_style_casual_common_torso_view_2.png";
//

            angular.element('#divaccent_7 ul.CollarStyle').css("display", "block");


            if (styleId == "2")//all
            {
                $scope.isContrast = true;
                $scope.outerplacketfabid = obj.id;
                $scope.innerplacketfabid = obj.id;
//
//            styleImg = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercollarfabid + "_style" + "_" + styleId + "_view_1.png";
//
//            $scope.actStyleJKArr['CollarStyle'] = styleImg;
//
//            $scope.actStyleJKArr['casualCollar'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/casual/" + $scope.outercollarfabid + "_style_" + styleId + "_casual_collar_view_1.png";
//            $scope.actStyleJKArr['casualCollarSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/casual/" + $scope.outercollarfabid + "_style_" + styleId + "_casual_collar_view_2.png";
//
//            $scope.actStyleJKArr['CollarStyleSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercollarfabid + "_style_" + styleId + "_view_2.png";
//            $scope.actStyleJKArr['CollarStyleBack'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercollarfabid + "_style_" + styleId + "_view_3.png";
//            $scope.actStyleJKArr['CollarStyleFold'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercollarfabid + "_style_" + styleId + "_view_4.png";
//            $scope.actStyleJKArr['CollarStyleZoom'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercollarfabid + "_style_" + styleId + "_view_5.png";
//
//
//            $scope.actStyleJKArr['innerCollarGlow'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercollarfabid + "_style_" + styleId + "_back_view_4.png";
//            $scope.actStyleJKArr['CollarStyleZoominner'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercollarfabid + "_style_" + styleId + "_back_view_5.png";
                //1_plackets_1_view_1.png
                $scope.actStyleJKArr['Plackets'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.outerplacketfabid + "_plackets_1_view_1.png";
                $scope.actStyleJKArr['PlacketsFold'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.outerplacketfabid + "_plackets_1_view_4.png";
                //1_inner_placket_view_1.png  casualInnerCollarSide
                $scope.actStyleJKArr['casualInnerPlackets'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.outerplacketfabid + "_inner_placket_view_1.png";

                $scope.actStyleJKArr['PlacketsSide'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.outerplacketfabid + "_plackets_1_view_2.png";
                $scope.actStyleJKArr['casualInnerPlacketsSide'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.outerplacketfabid + "_inner_placket_view_2.png";
                //1_style_1_casual_common_placket_view_1.png

                $scope.actStyleJKArr['casualPlacket'] = basePath + "pub/media/shirt-tool/shirt_images/collarstyle/casual/" + $scope.outerplacketfabid + "_style_1_casual_common_placket_view_1.png";
                $scope.actStyleJKArr['casualPlacketSide'] = basePath + "pub/media/shirt-tool/shirt_images/collarstyle/casual/" + $scope.outerplacketfabid + "_style_1_casual_common_placket_view_2.png";
                $scope.actStyleJKArr['PlacketsZoom'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.outerplacketfabid + "_plackets_1_view_5.png"

            } else if (styleId == "3") {//outer
                $scope.isContrast = true;

                $scope.outerplacketfabid = obj.id;

//
//            $scope.actStyleJKArr['CollarStyle'] = styleImg;
//
//            styleImg = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + obj.id + "_style" + "_" + styleId + "_view_1.png";
//            $scope.actStyleJKArr['CollarStyle'] = styleImg;
//
//            angular.element("#mainImageHolder").find("[rel=innerCollarGlow]").show();
//
//            $scope.actStyleJKArr['casualCollar'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/casual/" + obj.id + "_style_" + styleId + "_casual_collar_view_1.png";
//            $scope.actStyleJKArr['casualCollarSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/casual/" + obj.id + "_style_" + styleId + "_casual_collar_view_2.png";
//
//            $scope.actStyleJKArr['CollarStyleSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + obj.id + "_style_" + styleId + "_view_2.png";
//            $scope.actStyleJKArr['CollarStyleBack'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + obj.id + "_style_" + styleId + "_view_3.png";
//
//            $scope.actStyleJKArr['CollarStyleFold'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + obj.id + "_style_" + styleId + "_view_4.png";
//            $scope.actStyleJKArr['CollarStyleZoom'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + obj.id + "_style_" + styleId + "_view_5.png";
//
//
//            $scope.actStyleJKArr['innerCollarGlow'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_back_view_4.png";
//            $scope.actStyleJKArr['CollarStyleZoominner'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_back_view_5.png";

                $scope.actStyleJKArr['Plackets'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.outerplacketfabid + "_plackets_" + 1 + "_view_1.png";
                $scope.actStyleJKArr['PlacketsFold'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.outerplacketfabid + "_plackets_1_view_4.png";                                                                                                 //1_inner_placket_view_1.png
                $scope.actStyleJKArr['casualInnerPlackets'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.fabricId + "_inner_placket_view_1.png";

                $scope.actStyleJKArr['PlacketsSide'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.outerplacketfabid + "_plackets_" + 1 + "_view_2.png";
                $scope.actStyleJKArr['casualInnerPlacketsSide'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.fabricId + "_inner_placket_view_2.png";
                $scope.actStyleJKArr['casualPlacket'] = basePath + "pub/media/shirt-tool/shirt_images/collarstyle/casual/" + $scope.outerplacketfabid + "_style_1_casual_common_placket_view_1.png";
                $scope.actStyleJKArr['casualPlacketSide'] = basePath + "pub/media/shirt-tool/shirt_images/collarstyle/casual/" + $scope.outerplacketfabid + "_style_1_casual_common_placket_view_2.png";

                $scope.actStyleJKArr['PlacketsZoom'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.outerplacketfabid + "_plackets_1_view_5.png"
            } else if (styleId == "4") {//inner

                $scope.isContrast = false;
                $scope.innerplacketfabid = obj.id;

                $scope.actStyleJKArr['Plackets'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.fabricId + "_plackets_" + 1 + "_view_1.png";
                $scope.actStyleJKArr['PlacketsFold'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.fabricId + "_plackets_1_view_4.png";                                                                                                  //1_inner_placket_view_1.png
                $scope.actStyleJKArr['casualInnerPlackets'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.innerplacketfabid + "_inner_placket_view_1.png";

                $scope.actStyleJKArr['PlacketsSide'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.fabricId + "_plackets_" + 1 + "_view_2.png";
                $scope.actStyleJKArr['casualInnerPlacketsSide'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.innerplacketfabid + "_inner_placket_view_2.png";
                $scope.actStyleJKArr['casualPlacket'] = basePath + "pub/media/shirt-tool/shirt_images/collarstyle/casual/" + $scope.fabricId + "_style_1_casual_common_placket_view_1.png";
                $scope.actStyleJKArr['casualPlacketSide'] = basePath + "pub/media/shirt-tool/shirt_images/collarstyle/casual/" + $scope.fabricId + "_style_1_casual_common_placket_view_2.png";
                $scope.actStyleJKArr['PlacketsZoom'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.fabricId + "_plackets_1_view_5.png"
            } else {
                $scope.isContrast = false;
            }


            updateProduct();


        }
        $scope.changeCollarContrast = function ($event, obj) {


            angular.element(".CollarStyle").find(".activeDesign").removeClass('activeDesign');
            angular.element($event.currentTarget).find(".fabvie").addClass("activeDesign");

            var styleId = angular.element('#divaccent_2 ul:first').find('.activeDesign').attr('id') ? angular.element('#divaccent_2 ul:first').find('.activeDesign').attr('id') : "1";

            var parent = "CollarStyle";
            var styleTypeId = angular.element("#divs_1").find(".activeDesign").attr("id");
            var styleImg;

            $scope.actStyleJKArr[parent] = styleImg;

            $scope.actStyleJKArr['casualBase'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/casual/" + $scope.fabricId + "_style_casual_common_torso_view_1.png";
            $scope.actStyleJKArr['casualBaseSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/casual/" + $scope.fabricId + "_style_casual_common_torso_view_2.png";


            angular.element('#divaccent_2 ul.CollarStyle').css("display", "block");


            if (styleId == "2")//all
            {

                $scope.outercollarfabid = obj.id;
                $scope.innercollarfabid = obj.id;

                styleImg = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercollarfabid + "_style" + "_" + styleTypeId + "_view_1.png";

                $scope.actStyleJKArr['CollarStyle'] = styleImg;

                $scope.actStyleJKArr['casualCollar'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/casual/" + $scope.outercollarfabid + "_style_" + styleTypeId + "_casual_collar_view_1.png";
                $scope.actStyleJKArr['casualCollarSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/casual/" + $scope.outercollarfabid + "_style_" + styleTypeId + "_casual_collar_view_2.png";
                //1_style_3_casual_inner_left_view_1      
                $scope.actStyleJKArr['casualInnerCollar'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/casual/" + $scope.outercollarfabid + "_style_" + styleTypeId + "_casual_inner_view_1.png";
                $scope.actStyleJKArr['casualInnerCollarSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/casual/" + $scope.outercollarfabid + "_style_" + styleTypeId + "_casual_inner_view_2.png";


                $scope.actStyleJKArr['CollarStyleSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercollarfabid + "_style_" + styleTypeId + "_view_2.png";
                $scope.actStyleJKArr['CollarStyleBack'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercollarfabid + "_style_" + styleTypeId + "_view_3.png";
                $scope.actStyleJKArr['CollarStyleFold'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercollarfabid + "_style_" + styleTypeId + "_view_4.png";
                $scope.actStyleJKArr['CollarStyleZoom'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercollarfabid + "_style_" + styleTypeId + "_view_5.png";


                $scope.actStyleJKArr['innerCollarGlow'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercollarfabid + "_style_" + styleTypeId + "_back_view_4.png";
                $scope.actStyleJKArr['CollarStyleZoominner'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercollarfabid + "_style_" + styleTypeId + "_back_view_5.png";

                $scope.actStyleJKArr['CollarBase'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercollarfabid + "_style_" + styleId + "_front_view_1.png";
                $scope.actStyleJKArr['CollarBaseSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercollarfabid + "_style_" + styleId + "_front_view_2.png";

                $scope.actStyleJKArr['WomanBackCollar'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/back_collar/" + $scope.outercollarfabid + "_style_" + 1 + "_front_view_1.png";

                //$scope.actStyleJKArr['WomanBackCollarSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/back_collar/" + $scope.outercollarfabid + "_style_" + 1 + "_left_view_2.png";
                angular.element("#mainImageHolder").find("[rel=WomanBackCollarSide]").attr("src", basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/back_collar/" + $scope.outercollarfabid + "_style_" + 1 + "_left_view_2.png");
                angular.element("#mainImageHolder").find('.shirt_part[rel=casualWomanBackCollar]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/collarstyle/back_collar/" + $scope.outercollarfabid + "_style_1_casual_front_view_1.png");

                if (styleTypeId == '8' || styleTypeId == '10') {
                    $scope.actStyleJKArr['CollarBase'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercollarfabid + "_style_" + styleTypeId + "_view_1.png";
                    $scope.actStyleJKArr['CollarBaseSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercollarfabid + "_style_" + styleTypeId + "_view_2.png";
                    angular.element("#mainImageHolder").find('.shirt_part[rel="CollarStyle"]').hide();
                    angular.element("#mainImageHolder").find('.shirt_part[rel="CollarStyleSide"]').hide();
                    styleImg = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercollarfabid + "_style_" + styleTypeId + "_view_1.png";
                    $scope.actStyleJKArr['CollarStyle'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";
                    $scope.actStyleJKArr['CollarStyleSide'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";

                } else {
                    angular.element("#mainImageHolder").find('.shirt_part[rel="CollarStyle"]').show();
                    angular.element("#mainImageHolder").find('.shirt_part[rel="CollarStyleSide"]').show();
                    $scope.actStyleJKArr['CollarBase'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercollarfabid + "_style_" + styleTypeId + "_front_view_1.png";
                    $scope.actStyleJKArr['CollarBaseSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercollarfabid + "_style_" + styleTypeId + "_front_view_2.png";
                    styleImg = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercollarfabid + "_style_" + styleTypeId + "_view_1.png";
                    $scope.actStyleJKArr['CollarStyleSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercollarfabid + "_style_" + styleTypeId + "_view_2.png";
                    $scope.actStyleJKArr['CollarStyleFold'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercollarfabid + "_style_" + styleTypeId + "_view_4.png";
                }

            } else if (styleId == "3") {//outer


                $scope.outercollarfabid = obj.id;


                $scope.actStyleJKArr['CollarStyle'] = styleImg;

                styleImg = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + obj.id + "_style" + "_" + styleTypeId + "_view_1.png";
                $scope.actStyleJKArr['CollarStyle'] = styleImg;

                angular.element("#mainImageHolder").find("[rel=innerCollarGlow]").show();

                $scope.actStyleJKArr['casualCollar'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/casual/" + obj.id + "_style_" + styleTypeId + "_casual_collar_view_1.png";
                $scope.actStyleJKArr['casualCollarSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/casual/" + obj.id + "_style_" + styleTypeId + "_casual_collar_view_2.png";

                $scope.actStyleJKArr['casualInnerCollar'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/casual/" + $scope.fabricId + "_style_" + styleTypeId + "_casual_inner_view_1.png";
                $scope.actStyleJKArr['casualInnerCollarSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/casual/" + $scope.fabricId + "_style_" + styleTypeId + "_casual_inner_view_2.png";

                $scope.actStyleJKArr['CollarStyleSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + obj.id + "_style_" + styleTypeId + "_view_2.png";
                $scope.actStyleJKArr['CollarStyleBack'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + obj.id + "_style_" + styleTypeId + "_view_3.png";

                $scope.actStyleJKArr['CollarStyleFold'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + obj.id + "_style_" + styleTypeId + "_view_4.png";
                $scope.actStyleJKArr['CollarStyleZoom'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + obj.id + "_style_" + styleTypeId + "_view_5.png";


                $scope.actStyleJKArr['innerCollarGlow'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleTypeId + "_back_view_4.png";
                $scope.actStyleJKArr['CollarStyleZoominner'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleTypeId + "_back_view_5.png";

                $scope.actStyleJKArr['CollarBase'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + obj.id + "_style_" + styleId + "_front_view_1.png";
                $scope.actStyleJKArr['CollarBaseSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + obj.id + "_style_" + styleId + "_front_view_2.png";

                $scope.actStyleJKArr['WomanBackCollar'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/back_collar/" + $scope.fabricId + "_style_" + 1 + "_front_view_1.png";
                //$scope.actStyleJKArr['WomanBackCollarSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/back_collar/" + $scope.fabricId + "_style_" + 1 + "_left_view_2.png";
                angular.element("#mainImageHolder").find("[rel=WomanBackCollarSide]").attr("src", basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/back_collar/" + $scope.fabricId + "_style_" + 1 + "_left_view_2.png");
                angular.element("#mainImageHolder").find('.shirt_part[rel=casualWomanBackCollar]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/collarstyle/back_collar/" + $scope.fabricId + "_style_1_casual_front_view_1.png");



                if (styleTypeId == '8' || styleTypeId == '10') {
                    $scope.actStyleJKArr['CollarBase'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + obj.id + "_style_" + styleTypeId + "_view_1.png";
                    $scope.actStyleJKArr['CollarBaseSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + obj.id + "_style_" + styleTypeId + "_view_2.png";
                    angular.element("#mainImageHolder").find('.shirt_part[rel="CollarStyle"]').hide();
                    angular.element("#mainImageHolder").find('.shirt_part[rel="CollarStyleSide"]').hide();
                    styleImg = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + obj.id + "_style_" + styleTypeId + "_view_1.png";
                    $scope.actStyleJKArr['CollarStyle'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";
                    $scope.actStyleJKArr['CollarStyleSide'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";

                } else {
                    angular.element("#mainImageHolder").find('.shirt_part[rel="CollarStyle"]').show();
                    angular.element("#mainImageHolder").find('.shirt_part[rel="CollarStyleSide"]').show();
                    $scope.actStyleJKArr['CollarBase'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + obj.id + "_style_" + styleTypeId + "_front_view_1.png";
                    $scope.actStyleJKArr['CollarBaseSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + obj.id + "_style_" + styleTypeId + "_front_view_2.png";
                    styleImg = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + obj.id + "_style_" + styleTypeId + "_view_1.png";
                    $scope.actStyleJKArr['CollarStyleSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + obj.id + "_style_" + styleTypeId + "_view_2.png";
                    $scope.actStyleJKArr['CollarStyleFold'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + obj.id + "_style_" + styleTypeId + "_view_4.png";
                }
            } else if (styleId == "4") {//inner


                $scope.innercollarfabid = obj.id;

                styleImg = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style" + "_" + styleTypeId + "_view_1.png";


                $scope.actStyleJKArr['CollarStyle'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleTypeId + "_view_1.png";
                $scope.actStyleJKArr['casualCollar'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/casual/" + $scope.fabricId + "_style_" + styleTypeId + "_casual_collar_view_1.png";
                $scope.actStyleJKArr['casualCollarSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/casual/" + $scope.fabricId + "_style_" + styleTypeId + "_casual_collar_view_2.png";

                $scope.actStyleJKArr['casualInnerCollar'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/casual/" + obj.id + "_style_" + styleTypeId + "_casual_inner_view_1.png";
                $scope.actStyleJKArr['casualInnerCollarSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/casual/" + obj.id + "_style_" + styleTypeId + "_casual_inner_view_2.png";

                $scope.actStyleJKArr['CollarStyleSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleTypeId + "_view_2.png";
                $scope.actStyleJKArr['CollarStyleBack'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleTypeId + "_view_3.png";

                $scope.actStyleJKArr['CollarStyleFold'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleTypeId + "_view_4.png";
                $scope.actStyleJKArr['CollarStyleZoom'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleTypeId + "_view_5.png";


                $scope.actStyleJKArr['innerCollarGlow'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + obj.id + "_style_" + styleTypeId + "_back_view_4.png";
                $scope.actStyleJKArr['CollarStyleZoominner'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + obj.id + "_style_" + styleTypeId + "_back_view_5.png";

                $scope.actStyleJKArr['CollarBase'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_front_view_1.png";
                $scope.actStyleJKArr['CollarBaseSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_front_view_2.png";


                $scope.actStyleJKArr['WomanBackCollar'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/back_collar/" + obj.id + "_style_" + 1 + "_front_view_1.png";
                // $scope.actStyleJKArr['WomanBackCollarSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/back_collar/" + obj.id + "_style_" + 1 + "_left_view_2.png";
                angular.element("#mainImageHolder").find("[rel=WomanBackCollarSide]").attr("src", basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/back_collar/" + obj.id + "_style_" + 1 + "_left_view_2.png");
                angular.element("#mainImageHolder").find('.shirt_part[rel=casualWomanBackCollar]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/collarstyle/back_collar/" + obj.id + "_style_1_casual_front_view_1.png");

                if (styleTypeId == '8' || styleTypeId == '10') {
                    $scope.actStyleJKArr['CollarBase'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleTypeId + "_view_1.png";
                    $scope.actStyleJKArr['CollarBaseSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleTypeId + "_view_2.png";
                    angular.element("#mainImageHolder").find('.shirt_part[rel="CollarStyle"]').hide();
                    angular.element("#mainImageHolder").find('.shirt_part[rel="CollarStyleSide"]').hide();
                    styleImg = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleTypeId + "_view_1.png";
                    $scope.actStyleJKArr['CollarStyle'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";
                    $scope.actStyleJKArr['CollarStyleSide'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";

                } else {
                    angular.element("#mainImageHolder").find('.shirt_part[rel="CollarStyle"]').show();
                    angular.element("#mainImageHolder").find('.shirt_part[rel="CollarStyleSide"]').show();
                    $scope.actStyleJKArr['CollarBase'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleTypeId + "_front_view_1.png";
                    $scope.actStyleJKArr['CollarBaseSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleTypeId + "_front_view_2.png";
                    styleImg = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleTypeId + "_view_1.png";
                    $scope.actStyleJKArr['CollarStyleSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleTypeId + "_view_2.png";
                    $scope.actStyleJKArr['CollarStyleFold'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleTypeId + "_view_4.png";
                }

            }


            updateProduct();

        }

        //change Accent of Suit 
        $scope.accentClick = function ($event, obj, ind) {

            angular.element(".suit-preloader").show();
            angular.element(".mainToolarea").css({"opacity": "0"});

            var parent, styleImg, styleType, styleName, styleId, styleTypeId;

            angular.element($event.currentTarget).parent().find(".activeDesign").removeClass("activeDesign");
            angular.element($event.currentTarget).addClass("activeDesign");

            linid_active_id = angular.element($event.currentTarget).attr("id");



            if (jQuery(window).width() <= 767) {
                jQuery("body").removeClass("LeftpanemOpen")
                jQuery(".leftpanelsection").css("left", "0");
            }

            $scope.activeStyle = obj;

            currStyleName = parent = $scope.activeStyle.parent.replace(/\s+|&/g, "");

            styleName = $scope.activeStyle.name.replace(/\s+|&/g, "");

            styleId = $scope.activeStyle.id;


            if ($scope.activeStyle.price) {
                styleAccentPriceArr[parent] = $scope.activeStyle.price;
                // console.log("styleAccentPriceArr[parent] ="+styleAccentPriceArr[parent])
            } else {
                styleAccentPriceArr[parent] = "0.0";
            }

            console.log("actCatName = " + parent + " StyleId = " + styleId)

            styleType = $scope.activeStyle.designType; //angular.element("#view" + $scope.activeView).find('.shirt_part[rel=' + parent + ']').attr("type");



            if (parent == "CollarStyle")
            {
                styleTypeId = angular.element("#divs_1").find(".activeDesign").attr("id");

                styleId = angular.element('#divaccent_2 ul:first').find('.activeDesign').attr('id') ? angular.element('#divaccent_2 ul:first').find('.activeDesign').attr('id') : "1";


                // console.log("$scope.outercollarfabid = " + $scope.outercollarfabid + " $scope.innercollarfabid= " + $scope.innercollarfabid + " $scope.fabricId= " + $scope.fabricId)

                if (styleId == 1)
                {
                    angular.element('#divaccent_2 ul.CollarStyle').css("display", "none");

                    /*$scope.outercollarfabid = $scope.fabricId;
                     $scope.innercollarfabid = $scope.fabricId;*/

                    styleImg = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style" + "_" + styleId + "_view_1.png";

                    $scope.actStyleJKArr['CollarStyle'] = styleImg;

                    $scope.actStyleJKArr['casualCollar'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/casual/" + $scope.fabricId + "_style_" + styleTypeId + "_casual_collar_view_1.png";

                    $scope.actStyleJKArr['casualCollarSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/casual/" + $scope.fabricId + "_style_" + styleTypeId + "_casual_collar_view_2.png";

                    $scope.actStyleJKArr['casualInnerCollar'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/casual/" + $scope.fabricId + "_style_" + styleTypeId + "_casual_inner_view_1.png";
                    $scope.actStyleJKArr['casualInnerCollarSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/casual/" + $scope.fabricId + "_style_" + styleTypeId + "_casual_inner_view_2.png";

                    $scope.actStyleJKArr['CollarStyleSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleTypeId + "_view_2.png";
                    $scope.actStyleJKArr['CollarStyleBack'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleTypeId + "_view_3.png";

                    $scope.actStyleJKArr['CollarStyleFold'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleTypeId + "_view_4.png";
                    $scope.actStyleJKArr['CollarStyleZoom'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleTypeId + "_view_5.png";


                    $scope.actStyleJKArr['innerCollarGlow'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_back_view_4.png";
                    $scope.actStyleJKArr['CollarStyleZoominner'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleTypeId + "_back_view_5.png";

                    $scope.actStyleJKArr['CollarBase'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_front_view_1.png";
                    $scope.actStyleJKArr['CollarBaseSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_front_view_2.png";
                    //1_single_thread_view_1.png
//                $scope.actStyleJKArr['SingleButtonHole'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_single_thread_view_1.png";
//                $scope.actStyleJKArr['SingleButtonHoleSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_single_thread_view_2.png";
                    if (styleTypeId == '8' || styleTypeId == '10') {
                        $scope.actStyleJKArr['CollarBase'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleTypeId + "_view_1.png";
                        $scope.actStyleJKArr['CollarBaseSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleTypeId + "_view_2.png";
                        angular.element("#mainImageHolder").find('.shirt_part[rel="CollarStyle"]').hide();
                        angular.element("#mainImageHolder").find('.shirt_part[rel="CollarStyleSide"]').hide();
                        styleImg = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleTypeId + "_view_1.png";
                        $scope.actStyleJKArr['CollarStyle'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";
                        $scope.actStyleJKArr['CollarStyleSide'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";

                    } else {
                        angular.element("#mainImageHolder").find('.shirt_part[rel="CollarStyle"]').show();
                        angular.element("#mainImageHolder").find('.shirt_part[rel="CollarStyleSide"]').show();
                        $scope.actStyleJKArr['CollarBase'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleTypeId + "_front_view_1.png";
                        $scope.actStyleJKArr['CollarBaseSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleTypeId + "_front_view_2.png";
                        styleImg = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleTypeId + "_view_1.png";
                        $scope.actStyleJKArr['CollarStyleSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleTypeId + "_view_2.png";
                        $scope.actStyleJKArr['CollarStyleFold'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleTypeId + "_view_4.png";
                    }
                    $scope.actStyleJKArr['WomanBackCollar'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/back_collar/" + $scope.fabricId + "_style_" + 1 + "_front_view_1.png";
                    //$scope.actStyleJKArr['WomanBackPartSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/back_collar/" + $scope.fabricId + "_style_" + styleId + "_left_sidepart_view_2.png";
                    $scope.actStyleJKArr['WomanBackCollarSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/back_collar/" + $scope.fabricId + "_style_" + 1 + "_left_view_2.png";
                    angular.element("#mainImageHolder").find('.shirt_part[rel=casualWomanBackCollar]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/collarstyle/back_collar/" + $scope.fabricId + "_style_1_casual_front_view_1.png");

                } else
                {
                    angular.element('#divaccent_2 ul.CollarStyle').css("display", "block");


                    if (styleId == "2")//all
                    {

                        $scope.innercollarfabid = $scope.outercollarfabid = angular.element(".CollarStyle").find(".activeDesign").parent().attr("id") ? angular.element(".CollarStyle").find(".activeDesign").parent().attr("id") : angular.element(".CollarStyle fabricviewaccent:first").attr("id");
                        styleImg = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercollarfabid + "_style" + "_" + styleTypeId + "_view_1.png";

                        $scope.actStyleJKArr['CollarStyle'] = styleImg;

                        $scope.actStyleJKArr['casualCollar'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/casual/" + $scope.outercollarfabid + "_style_" + styleTypeId + "_casual_collar_view_1.png";
                        $scope.actStyleJKArr['casualCollarSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/casual/" + $scope.outercollarfabid + "_style_" + styleTypeId + "_casual_collar_view_2.png";

                        $scope.actStyleJKArr['casualInnerCollar'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/casual/" + $scope.outercollarfabid + "_style_" + styleTypeId + "_casual_inner_view_1.png";
                        $scope.actStyleJKArr['casualInnerCollarSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/casual/" + $scope.outercollarfabid + "_style_" + styleTypeId + "_casual_inner_view_2.png";

                        $scope.actStyleJKArr['CollarStyleSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercollarfabid + "_style_" + styleTypeId + "_view_2.png";
                        $scope.actStyleJKArr['CollarStyleBack'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercollarfabid + "_style_" + styleTypeId + "_view_3.png";

                        $scope.actStyleJKArr['CollarStyleFold'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercollarfabid + "_style_" + styleTypeId + "_view_4.png";
                        $scope.actStyleJKArr['CollarStyleZoom'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercollarfabid + "_style_" + styleTypeId + "_view_5.png";

                        $scope.actStyleJKArr['innerCollarGlow'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercollarfabid + "_style_" + styleId + "_back_view_4.png";
                        $scope.actStyleJKArr['CollarStyleZoominner'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercollarfabid + "_style_" + styleTypeId + "_back_view_5.png";

                        $scope.actStyleJKArr['CollarBase'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercollarfabid + "_style_" + styleId + "_front_view_1.png";
                        $scope.actStyleJKArr['CollarBaseSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercollarfabid + "_style_" + styleId + "_front_view_2.png";

                        $scope.actStyleJKArr['WomanBackCollar'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/back_collar/" + $scope.outercollarfabid + "_style_" + 1 + "_front_view_1.png";
                        //$scope.actStyleJKArr['WomanBackPartSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/back_collar/" + $scope.fabricId + "_style_" + styleId + "_left_sidepart_view_2.png";
                        //$scope.actStyleJKArr['WomanBackCollarSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/back_collar/" + $scope.outercollarfabid + "_style_" + 1 + "_left_view_2.png";
                        angular.element("#mainImageHolder").find("[rel=WomanBackCollarSide]").attr("src", basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/back_collar/" + $scope.outercollarfabid + "_style_" + 1 + "_left_view_2.png");
//                   
                        angular.element("#mainImageHolder").find('.shirt_part[rel=casualWomanBackCollar]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/collarstyle/back_collar/" + $scope.outercollarfabid + "_style_1_casual_front_view_1.png");

//                    $scope.actStyleJKArr['SingleButtonHole'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_single_thread_view_1.png";
//                $scope.actStyleJKArr['SingleButtonHoleSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_single_thread_view_2.png";

                        if (styleTypeId == '8' || styleTypeId == '10') {
                            $scope.actStyleJKArr['CollarBase'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercollarfabid + "_style_" + styleTypeId + "_view_1.png";
                            $scope.actStyleJKArr['CollarBaseSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercollarfabid + "_style_" + styleTypeId + "_view_2.png";
                            angular.element("#mainImageHolder").find('.shirt_part[rel="CollarStyle"]').hide();
                            angular.element("#mainImageHolder").find('.shirt_part[rel="CollarStyleSide"]').hide();
                            styleImg = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercollarfabid + "_style_" + styleTypeId + "_view_1.png";
                            $scope.actStyleJKArr['CollarStyle'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";
                            $scope.actStyleJKArr['CollarStyleSide'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";

                        } else {
                            angular.element("#mainImageHolder").find('.shirt_part[rel="CollarStyle"]').show();
                            angular.element("#mainImageHolder").find('.shirt_part[rel="CollarStyleSide"]').show();
                            $scope.actStyleJKArr['CollarBase'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercollarfabid + "_style_" + styleTypeId + "_front_view_1.png";
                            $scope.actStyleJKArr['CollarBaseSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercollarfabid + "_style_" + styleTypeId + "_front_view_2.png";
                            styleImg = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercollarfabid + "_style_" + styleTypeId + "_view_1.png";
                            $scope.actStyleJKArr['CollarStyleSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercollarfabid + "_style_" + styleTypeId + "_view_2.png";
                            $scope.actStyleJKArr['CollarStyleFold'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercollarfabid + "_style_" + styleTypeId + "_view_4.png";
                        }

                    } else if (styleId == "3") {//outer

                        $scope.outercollarfabid = angular.element(".CollarStyle").find(".activeDesign").parent().attr("id") ? angular.element(".CollarStyle").find(".activeDesign").parent().attr("id") : angular.element(".CollarStyle fabricviewaccent:first").attr("id");


                        angular.element("#mainImageHolder").find("[rel=innerCollarGlow]").show();

                        styleImg = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercollarfabid + "_style" + "_" + styleTypeId + "_view_1.png";

                        $scope.actStyleJKArr['CollarStyle'] = styleImg;

                        $scope.actStyleJKArr['casualCollar'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/casual/" + $scope.outercollarfabid + "_style_" + styleTypeId + "_casual_collar_view_1.png";
                        $scope.actStyleJKArr['casualCollarSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/casual/" + $scope.outercollarfabid + "_style_" + styleTypeId + "_casual_collar_view_2.png";

                        $scope.actStyleJKArr['casualInnerCollar'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/casual/" + $scope.fabricId + "_style_" + styleTypeId + "_casual_inner_view_1.png";
                        $scope.actStyleJKArr['casualInnerCollarSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/casual/" + $scope.fabricId + "_style_" + styleTypeId + "_casual_inner_view_2.png";

                        $scope.actStyleJKArr['CollarStyleSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercollarfabid + "_style_" + styleTypeId + "_view_2.png";
                        $scope.actStyleJKArr['CollarStyleBack'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercollarfabid + "_style_" + styleTypeId + "_view_3.png";

                        $scope.actStyleJKArr['CollarStyleFold'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercollarfabid + "_style_" + styleTypeId + "_view_4.png";
                        $scope.actStyleJKArr['CollarStyleZoom'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercollarfabid + "_style_" + styleTypeId + "_view_5.png";

                        $scope.actStyleJKArr['innerCollarGlow'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_back_view_4.png";
                        $scope.actStyleJKArr['CollarStyleZoominner'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleTypeId + "_back_view_5.png";

                        $scope.actStyleJKArr['CollarBase'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercollarfabid + "_style_" + styleId + "_front_view_1.png";
                        $scope.actStyleJKArr['CollarBaseSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercollarfabid + "_style_" + styleId + "_front_view_2.png";

                        $scope.actStyleJKArr['WomanBackCollar'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/back_collar/" + $scope.fabricId + "_style_" + 1 + "_front_view_1.png";
                        //$scope.actStyleJKArr['WomanBackPartSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/back_collar/" + $scope.fabricId + "_style_" + styleId + "_left_sidepart_view_2.png";
                        //$scope.actStyleJKArr['WomanBackCollarSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/back_collar/" + $scope.fabricId + "_style_" + 1 + "_left_view_2.png";
                        angular.element("#mainImageHolder").find("[rel=WomanBackCollarSide]").attr("src", basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/back_collar/" + $scope.fabricId + "_style_" + 1 + "_left_view_2.png");
                        angular.element("#mainImageHolder").find('.shirt_part[rel=casualWomanBackCollar]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/collarstyle/back_collar/" + $scope.fabricId + "_style_1_casual_front_view_1.png");

                        if (styleTypeId == '8' || styleTypeId == '10') {
                            $scope.actStyleJKArr['CollarBase'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercollarfabid + "_style_" + styleTypeId + "_view_1.png";
                            $scope.actStyleJKArr['CollarBaseSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercollarfabid + "_style_" + styleTypeId + "_view_2.png";
                            angular.element("#mainImageHolder").find('.shirt_part[rel="CollarStyle"]').hide();
                            angular.element("#mainImageHolder").find('.shirt_part[rel="CollarStyleSide"]').hide();
                            styleImg = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercollarfabid + "_style_" + styleTypeId + "_view_1.png";
                            $scope.actStyleJKArr['CollarStyle'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";
                            $scope.actStyleJKArr['CollarStyleSide'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";

                        } else {
                            angular.element("#mainImageHolder").find('.shirt_part[rel="CollarStyle"]').show();
                            angular.element("#mainImageHolder").find('.shirt_part[rel="CollarStyleSide"]').show();
                            $scope.actStyleJKArr['CollarBase'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercollarfabid + "_style_" + styleTypeId + "_front_view_1.png";
                            $scope.actStyleJKArr['CollarBaseSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercollarfabid + "_style_" + styleTypeId + "_front_view_2.png";
                            styleImg = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercollarfabid + "_style_" + styleTypeId + "_view_1.png";
                            $scope.actStyleJKArr['CollarStyleSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercollarfabid + "_style_" + styleTypeId + "_view_2.png";
                            $scope.actStyleJKArr['CollarStyleFold'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercollarfabid + "_style_" + styleTypeId + "_view_4.png";
                        }
                    } else if (styleId == "4") {//inner

                        $scope.innercollarfabid = angular.element(".CollarStyle").find(".activeDesign").parent().attr("id") ? angular.element(".CollarStyle").find(".activeDesign").parent().attr("id") : angular.element(".CollarStyle fabricviewaccent:first").attr("id");


                        styleImg = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style" + "_" + styleTypeId + "_view_1.png";

                        $scope.actStyleJKArr['CollarStyle'] = styleImg;


                        $scope.actStyleJKArr['casualCollar'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/casual/" + $scope.fabricId + "_style_" + styleTypeId + "_casual_collar_view_1.png";
                        $scope.actStyleJKArr['casualCollarSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/casual/" + $scope.fabricId + "_style_" + styleTypeId + "_casual_collar_view_2.png";

                        $scope.actStyleJKArr['casualInnerCollar'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/casual/" + $scope.innercollarfabid + "_style_" + styleTypeId + "_casual_inner_view_1.png";
                        $scope.actStyleJKArr['casualInnerCollarSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/casual/" + $scope.innercollarfabid + "_style_" + styleTypeId + "_casual_inner_view_2.png";

                        $scope.actStyleJKArr['CollarStyleSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleTypeId + "_view_2.png";
                        $scope.actStyleJKArr['CollarStyleBack'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleTypeId + "_view_3.png";

                        $scope.actStyleJKArr['CollarStyleFold'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleTypeId + "_view_4.png";
                        $scope.actStyleJKArr['CollarStyleZoom'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleTypeId + "_view_5.png";


                        $scope.actStyleJKArr['innerCollarGlow'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.innercollarfabid + "_style_" + styleId + "_back_view_4.png";
                        $scope.actStyleJKArr['CollarStyleZoominner'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.innercollarfabid + "_style_" + styleTypeId + "_back_view_5.png";

                        $scope.actStyleJKArr['CollarBase'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_front_view_1.png";
                        $scope.actStyleJKArr['CollarBaseSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_front_view_2.png";

                        $scope.actStyleJKArr['WomanBackCollar'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/back_collar/" + $scope.innercollarfabid + "_style_" + 1 + "_front_view_1.png";
                        //$scope.actStyleJKArr['WomanBackCollarSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/back_collar/" + $scope.innercollarfabid + "_style_" + 1 + "_left_view_2.png";
                        angular.element("#mainImageHolder").find("[rel=WomanBackCollarSide]").attr("src", basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/back_collar/" + $scope.innercollarfabid + "_style_" + 1 + "_left_view_2.png");
                        angular.element("#mainImageHolder").find('.shirt_part[rel=casualWomanBackCollar]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/collarstyle/back_collar/" + $scope.innercollarfabid + "_style_1_casual_front_view_1.png");

                        if (styleTypeId == '8' || styleTypeId == '10') {
                            $scope.actStyleJKArr['CollarBase'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleTypeId + "_view_1.png";
                            $scope.actStyleJKArr['CollarBaseSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleTypeId + "_view_2.png";
                            angular.element("#mainImageHolder").find('.shirt_part[rel="CollarStyle"]').hide();
                            angular.element("#mainImageHolder").find('.shirt_part[rel="CollarStyleSide"]').hide();
                            styleImg = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleTypeId + "_view_1.png";
                            $scope.actStyleJKArr['CollarStyle'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";
                            $scope.actStyleJKArr['CollarStyleSide'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";

                        } else {
                            angular.element("#mainImageHolder").find('.shirt_part[rel="CollarStyle"]').show();
                            angular.element("#mainImageHolder").find('.shirt_part[rel="CollarStyleSide"]').show();
                            $scope.actStyleJKArr['CollarBase'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleTypeId + "_front_view_1.png";
                            $scope.actStyleJKArr['CollarBaseSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleTypeId + "_front_view_2.png";
                            styleImg = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleTypeId + "_view_1.png";
                            $scope.actStyleJKArr['CollarStyleSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleTypeId + "_view_2.png";
                            $scope.actStyleJKArr['CollarStyleFold'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleTypeId + "_view_4.png";
                        }


                    }



                }

                console.log("styleName= = " + styleName)

            } else if (parent == "FrontPlacket") {
                //alert();
                $scope.IsShirt = true;
                hideJacket();
                if (styleId == "2")//all
                {
                    $scope.isContrast = true;
                    $scope.innerplacketfabid = $scope.outerplacketfabid = angular.element(".FrontPlacket").find(".activeDesign").parent().attr("id") ? angular.element(".FrontPlacket").find(".activeDesign").parent().attr("id") : angular.element(".FrontPlacket fabricviewaccent:first").attr("id");
                    //$scope.outerplacketfabid = jQuery('.FrontPlacket').find('.activeDesign').parent().attr('id');
                    //$scope.innerplacketfabid = jQuery('.FrontPlacket').find('.activeDesign').parent().attr('id');
                    ////
//            styleImg = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercollarfabid + "_style" + "_" + styleId + "_view_1.png";
//
//            $scope.actStyleJKArr['CollarStyle'] = styleImg;
//
//            $scope.actStyleJKArr['casualCollar'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/casual/" + $scope.outercollarfabid + "_style_" + styleId + "_casual_collar_view_1.png";
//            $scope.actStyleJKArr['casualCollarSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/casual/" + $scope.outercollarfabid + "_style_" + styleId + "_casual_collar_view_2.png";
//
//            $scope.actStyleJKArr['CollarStyleSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercollarfabid + "_style_" + styleId + "_view_2.png";
//            $scope.actStyleJKArr['CollarStyleBack'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercollarfabid + "_style_" + styleId + "_view_3.png";
//            $scope.actStyleJKArr['CollarStyleFold'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercollarfabid + "_style_" + styleId + "_view_4.png";
//            $scope.actStyleJKArr['CollarStyleZoom'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercollarfabid + "_style_" + styleId + "_view_5.png";
//
//
//            $scope.actStyleJKArr['innerCollarGlow'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercollarfabid + "_style_" + styleId + "_back_view_4.png";
//            $scope.actStyleJKArr['CollarStyleZoominner'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercollarfabid + "_style_" + styleId + "_back_view_5.png";
                    //1_plackets_1_view_1.png
                    $scope.actStyleJKArr['Plackets'] = styleImg = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.outerplacketfabid + "_plackets_" + 1 + "_view_1.png";
                    $scope.actStyleJKArr['PlacketsFold'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.outerplacketfabid + "_plackets_1_view_4.png";                                                                                                  //1_inner_placket_view_1.png
                    $scope.actStyleJKArr['casualInnerPlackets'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.outerplacketfabid + "_inner_placket_view_1.png";

                    $scope.actStyleJKArr['PlacketsSide'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.outerplacketfabid + "_plackets_" + 1 + "_view_2.png";
                    $scope.actStyleJKArr['casualInnerPlacketsSide'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.outerplacketfabid + "_inner_placket_view_2.png";

                    $scope.actStyleJKArr['casualPlacket'] = basePath + "pub/media/shirt-tool/shirt_images/collarstyle/casual/" + $scope.outerplacketfabid + "_style_1_casual_common_placket_view_1.png";
                    $scope.actStyleJKArr['casualPlacketSide'] = basePath + "pub/media/shirt-tool/shirt_images/collarstyle/casual/" + $scope.outerplacketfabid + "_style_1_casual_common_placket_view_2.png";

                    $scope.actStyleJKArr['PlacketsZoom'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.outerplacketfabid + "_plackets_1_view_5.png"
                    //angular.element("#mainImageHolder").find("[rel=PlacketsZoom]").hide();
                } else if (styleId == "3") {//outer
                    $scope.isContrast = true;

                    $scope.outerplacketfabid = angular.element(".FrontPlacket").find(".activeDesign").parent().attr("id") ? angular.element(".FrontPlacket").find(".activeDesign").parent().attr("id") : angular.element(".FrontPlacket fabricviewaccent:first").attr("id");

//
//            $scope.actStyleJKArr['CollarStyle'] = styleImg;
//
//            styleImg = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + obj.id + "_style" + "_" + styleId + "_view_1.png";
//            $scope.actStyleJKArr['CollarStyle'] = styleImg;
//
//            angular.element("#mainImageHolder").find("[rel=innerCollarGlow]").show();
//
//            $scope.actStyleJKArr['casualCollar'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/casual/" + obj.id + "_style_" + styleId + "_casual_collar_view_1.png";
//            $scope.actStyleJKArr['casualCollarSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/casual/" + obj.id + "_style_" + styleId + "_casual_collar_view_2.png";
//
//            $scope.actStyleJKArr['CollarStyleSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + obj.id + "_style_" + styleId + "_view_2.png";
//            $scope.actStyleJKArr['CollarStyleBack'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + obj.id + "_style_" + styleId + "_view_3.png";
//
//            $scope.actStyleJKArr['CollarStyleFold'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + obj.id + "_style_" + styleId + "_view_4.png";
//            $scope.actStyleJKArr['CollarStyleZoom'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + obj.id + "_style_" + styleId + "_view_5.png";
//
//
//            $scope.actStyleJKArr['innerCollarGlow'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_back_view_4.png";
//            $scope.actStyleJKArr['CollarStyleZoominner'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_back_view_5.png";

                    $scope.actStyleJKArr['Plackets'] = styleImg = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.outerplacketfabid + "_plackets_" + 1 + "_view_1.png";
                    $scope.actStyleJKArr['PlacketsFold'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.outerplacketfabid + "_plackets_1_view_4.png";                                                                                                  //1_inner_placket_view_1.png
                    $scope.actStyleJKArr['casualInnerPlackets'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.fabricId + "_inner_placket_view_1.png";

                    $scope.actStyleJKArr['PlacketsSide'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.outerplacketfabid + "_plackets_" + 1 + "_view_2.png";
                    $scope.actStyleJKArr['casualInnerPlacketsSide'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.fabricId + "_inner_placket_view_2.png";
                    $scope.actStyleJKArr['casualPlacket'] = basePath + "pub/media/shirt-tool/shirt_images/collarstyle/casual/" + $scope.outerplacketfabid + "_style_1_casual_common_placket_view_1.png";
                    $scope.actStyleJKArr['casualPlacketSide'] = basePath + "pub/media/shirt-tool/shirt_images/collarstyle/casual/" + $scope.outerplacketfabid + "_style_1_casual_common_placket_view_2.png";

                    $scope.actStyleJKArr['PlacketsZoom'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.outerplacketfabid + "_plackets_1_view_5.png"
                } else if (styleId == "4") {//inner
                    $scope.isContrast = false;
                    $scope.innerplacketfabid = angular.element(".FrontPlacket").find(".activeDesign").parent().attr("id") ? angular.element(".FrontPlacket").find(".activeDesign").parent().attr("id") : angular.element(".FrontPlacket fabricviewaccent:first").attr("id");
                    //$scope.innerplacketfabid = jQuery('.FrontPlacket').find('.activeDesign').parent().attr('id');
//
//            styleImg = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style" + "_" + styleId + "_view_1.png";
//
//
//            $scope.actStyleJKArr['CollarStyle'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_view_1.png";
//            $scope.actStyleJKArr['casualCollar'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/casual/" + $scope.fabricId + "_style_" + styleId + "_casual_collar_view_1.png";
//            $scope.actStyleJKArr['casualCollarSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/casual/" + $scope.fabricId + "_style_" + styleId + "_casual_collar_view_2.png";
//            $scope.actStyleJKArr['CollarStyleSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_view_2.png";
//            $scope.actStyleJKArr['CollarStyleBack'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_view_3.png";
//
//            $scope.actStyleJKArr['CollarStyleFold'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_view_4.png";
//            $scope.actStyleJKArr['CollarStyleZoom'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_view_5.png";
//
//
//            $scope.actStyleJKArr['innerCollarGlow'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + obj.id + "_style_" + styleId + "_back_view_4.png";
//            $scope.actStyleJKArr['CollarStyleZoominner'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + obj.id + "_style_" + styleId + "_back_view_5.png";

                    $scope.actStyleJKArr['Plackets'] = styleImg = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.fabricId + "_plackets_" + 1 + "_view_1.png";
                    $scope.actStyleJKArr['PlacketsFold'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.fabricId + "_plackets_1_view_4.png";                                                                                                  //1_inner_placket_view_1.png
                    $scope.actStyleJKArr['casualInnerPlackets'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.innerplacketfabid + "_inner_placket_view_1.png";

                    $scope.actStyleJKArr['PlacketsSide'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.fabricId + "_plackets_" + 1 + "_view_2.png";
                    $scope.actStyleJKArr['casualInnerPlacketsSide'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.innerplacketfabid + "_inner_placket_view_2.png";
                    $scope.actStyleJKArr['casualPlacket'] = basePath + "pub/media/shirt-tool/shirt_images/collarstyle/casual/" + $scope.fabricId + "_style_1_casual_common_placket_view_1.png";
                    $scope.actStyleJKArr['casualPlacketSide'] = basePath + "pub/media/shirt-tool/shirt_images/collarstyle/casual/" + $scope.fabricId + "_style_1_casual_common_placket_view_2.png";

                    $scope.actStyleJKArr['PlacketsZoom'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.fabricId + "_plackets_1_view_5.png"
                }
            } else if (parent == "Cuffs") {//accent
                if ($scope.threadcolorId == undefined)
                {
                    $scope.threadcolorId = "4";
                }

                styleTypeId = angular.element("#divs_2").find(".activeDesign").attr("id");
                styleId = angular.element('#divaccent_3 ul:first').find('.activeDesign').attr('id') ? angular.element('#divaccent_3 ul:first').find('.activeDesign').attr('id') : "1";

                if (styleId == 1)
                {
                    angular.element('#divaccent_3 ul.Cuffs').css("display", "none");


                    styleImg = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_cuffstyle_1_size__cufftype__" + styleTypeId + "_view_1.png";
                    $scope.actStyleJKArr['Cuffs'] = styleImg;
                    $scope.actStyleJKArr['CuffsSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_cuffstyle_1_size__cufftype__" + styleTypeId + "_view_2.png";
                    $scope.actStyleJKArr['CuffsBack'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_cuffstyle_1_size__cufftype__" + styleTypeId + "_view_3.png";
                    $scope.actStyleJKArr['CuffsFold'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_cuffstyle_1_size__cufftype__" + styleTypeId + "_view_4.png";
                    $scope.actStyleJKArr['CuffsFoldInner'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_cuffstyle_1_size__inner__" + styleTypeId + "_inner_view_4.png";
                    $scope.actStyleJKArr['CuffsOutBack'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";
                } else
                {
                    angular.element('#divaccent_3 ul.Cuffs').css("display", "block");



                    if (styleId == "2")//all
                    {

                        $scope.innercufffabid = $scope.outercufffabid = angular.element(".Cuffs").find(".activeDesign").parent().attr("id") ? angular.element(".Cuffs").find(".activeDesign").parent().attr("id") : angular.element(".Cuffs fabricviewaccent:first").attr("id");
                        styleImg = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercufffabid + "_cuffstyle_1_size__cufftype__" + styleTypeId + "_view_1.png";
                        $scope.actStyleJKArr['Cuffs'] = styleImg;
                        $scope.actStyleJKArr['CuffsSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercufffabid + "_cuffstyle_1_size__cufftype__" + styleTypeId + "_view_2.png";
                        $scope.actStyleJKArr['CuffsBack'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercufffabid + "_cuffstyle_1_size__cufftype__" + styleTypeId + "_view_3.png";
                        $scope.actStyleJKArr['CuffsFold'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercufffabid + "_cuffstyle_1_size__cufftype__" + styleTypeId + "_view_4.png";
                        $scope.actStyleJKArr['CuffsFoldInner'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercufffabid + "_cuffstyle_1_size__inner__" + styleTypeId + "_inner_view_4.png";


//for Buttons
                        if (styleTypeId == "5" || styleTypeId == "8" || styleTypeId == "10") {
                            //2ButtonCuff ButtonThreadCuff ButtonHoleCuffFold

                            $scope.actStyleJKArr['ButtonCuffBack'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/Static/Back_CuffDouble_b1.png";
                            angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonHoleCuffFold"]').show();
                            angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonThreadCuffBack"]').show();

                            $scope.actStyleJKArr['ButtonThreadCuffBack'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + $scope.threadcolorId + "_double_buttonhole_view_3.png";


                        } else if (styleTypeId == "1" || styleTypeId == "2" || styleTypeId == "6" || styleTypeId == "9" || styleTypeId == "12") {
                            //French 
                            $scope.actStyleJKArr['ButtonCuffBack'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/Static/Back_Common_French_Cufflink.png";
                            $scope.actStyleJKArr['ButtonThreadCuffBack'] = basePath + "pub/media/shirt-tool/default_shirttool_image/woman/blank.png";
                            angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonHoleCuffFold"]').hide();
                            angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonThreadCuffBack"]').hide();

                        } else {
                            //BottonSingle 4 3 7 11


                            $scope.actStyleJKArr['ButtonCuffBack'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/Static/Back_CuffSingle_b1.png";
                            angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonThreadCuffBack"]').show();
                            $scope.actStyleJKArr['ButtonThreadCuffBack'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + $scope.threadcolorId + "_single_buttonhole_view_3.png";

                        }

                        //buttons End here
                        $scope.actStyleJKArr['CuffsOutBack'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";
                    } else if (styleId == "3") {//outer


                        $scope.outercufffabid = angular.element(".Cuffs").find(".activeDesign").parent().attr("id") ? angular.element(".Cuffs").find(".activeDesign").parent().attr("id") : angular.element(".Cuffs fabricviewaccent:first").attr("id");

                        styleImg = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercufffabid + "_cuffstyle_1_size__cufftype__" + styleTypeId + "_view_1.png";
                        $scope.actStyleJKArr['Cuffs'] = styleImg;


                        $scope.actStyleJKArr['CuffsSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercufffabid + "_cuffstyle_1_size__cufftype__" + styleTypeId + "_view_2.png";
                        $scope.actStyleJKArr['CuffsBack'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercufffabid + "_cuffstyle_1_size__cufftype__" + styleTypeId + "_view_3.png";
                        //1_cuffstyle_1_size__contrast_outer_cuff__1_final_back_cuff.png
                        // $scope.actStyleJKArr['CuffsOutBack'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_cuffstyle_1_size__contrast_outer_cuff__1_final_back_cuff.png";
                        $scope.actStyleJKArr['CuffsFold'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.outercufffabid + "_cuffstyle_1_size__cufftype__" + styleTypeId + "_view_4.png";
                        $scope.actStyleJKArr['CuffsFoldInner'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_cuffstyle_1_size__inner__" + styleTypeId + "_inner_view_4.png";
                        // angular.element("#mainImageHolder").find("rel=CuffsOutBack").hide();

                        //for Buttons
                        if (styleTypeId == "5" || styleTypeId == "8" || styleTypeId == "10") {
                            //2ButtonCuff ButtonThreadCuff ButtonHoleCuffFold

                            $scope.actStyleJKArr['ButtonCuffBack'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/Static/Back_CuffDouble_b1.png";
                            angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonHoleCuffFold"]').show();
                            angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonThreadCuffBack"]').show();

                            $scope.actStyleJKArr['ButtonThreadCuffBack'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + $scope.threadcolorId + "_double_buttonhole_view_3.png";


                        } else if (styleTypeId == "1" || styleTypeId == "2" || styleTypeId == "6" || styleTypeId == "9" || styleTypeId == "12") {
                            //French 
                            $scope.actStyleJKArr['ButtonCuffBack'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/Static/Back_Common_French_Cufflink.png";
                            $scope.actStyleJKArr['ButtonThreadCuffBack'] = basePath + "pub/media/shirt-tool/default_shirttool_image/woman/blank.png";
                            angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonHoleCuffFold"]').hide();
                            angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonThreadCuffBack"]').hide();

                        } else {
                            //BottonSingle 4 3 7 11


                            $scope.actStyleJKArr['ButtonCuffBack'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/Static/Back_CuffSingle_b1.png";
                            angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonThreadCuffBack"]').show();
                            $scope.actStyleJKArr['ButtonThreadCuffBack'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + $scope.threadcolorId + "_single_buttonhole_view_3.png";

                        }
                        $scope.actStyleJKArr['CuffsOutBack'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";
                        //buttons End here

                    } else if (styleId == "4") {//inner
                        angular.element("#mainImageHolder").find('.shirt_part[rel="CuffsOutBack"]').show();
                        $scope.innercufffabid = angular.element(".Cuffs").find(".activeDesign").parent().attr("id") ? angular.element(".Cuffs").find(".activeDesign").parent().attr("id") : angular.element(".Cuffs fabricviewaccent:first").attr("id");
                        //1_cuffstyle_1_size__opencuff__1_final_back_cuff
                        styleImg = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.innercufffabid + "_cuffstyle_1_size__opencuff__1_final_front_cuff.png";
                        $scope.actStyleJKArr['Cuffs'] = styleImg;



                        $scope.actStyleJKArr['CuffsSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.innercufffabid + "_cuffstyle_1_size__opencuff__1_final_side_cuff.png";

                        $scope.actStyleJKArr['CuffsBack'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.innercufffabid + "_cuffstyle_1_size__opencuff__1_final_back_cuff.png";
                        $scope.actStyleJKArr['CuffsFold'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_cuffstyle_1_size__cufftype__" + styleTypeId + "_view_4.png";
                        $scope.actStyleJKArr['CuffsFoldInner'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.innercufffabid + "_cuffstyle_1_size__inner__" + styleTypeId + "_inner_view_4.png";

                        $scope.actStyleJKArr['ButtonCuffBack'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";
                        $scope.actStyleJKArr['ButtonThreadCuffBack'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";

                        $scope.actStyleJKArr['CuffsOutBack'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_cuffstyle_1_size__contrast_outer_cuff__1_final_back_cuff.png";
                        $scope.innercufffabid = obj.id;

                    }
                }





            } else if (parent == "Threads") {

                styleId = angular.element('#divaccent_4 ul:first').find('.activeDesign').attr('id') ? angular.element('#divaccent_4 ul:first').find('.activeDesign').attr('id') : "1";
                styleTypeId = angular.element("#divs_2").find(".activeDesign").attr("id");
                angular.element('#divaccent_4 ul.Threads').css("display", "none");
                if (styleId == "1")
                {
//all
                    angular.element('#divaccent_4 ul.Threads').css("display", "none");
                    $scope.threadcolorId = 4;//angular.element(".Threads").find(".activeDesign").parent().attr("id") ? angular.element(".Threads").find(".activeDesign").parent().attr("id") : angular.element(".Threads threadcolor:first").attr("id");
                    //$scope.threadcolorId = angular.element(".Threads").find(".activeDesign").parent().attr("id") ? angular.element(".Threads").find(".activeDesign").parent().attr("id") : angular.element(".Threads fabricviewaccent:first").attr("id");
                    //shirt_images/plackets/1_buttonhole_view_1.png
                    styleImg = $scope.actStyleJKArr['ButtonHole'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_buttonhole_view_1.png";
                    $scope.actStyleJKArr['ButtonHole'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_buttonhole_view_1.png";
                    $scope.actStyleJKArr['ButtonHoleSide'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_buttonhole_view_2.png";
                    $scope.actStyleJKArr['ButtonHoleFold'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_buttonhole_view_4.png";
                    $scope.actStyleJKArr['BottonZoomHole'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_buttonhole_view_5.png";

                    $scope.actStyleJKArr['BottonZoomThread'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_thread_view_5.png";
                    $scope.actStyleJKArr['Threads'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_thread_view_1.png";
                    $scope.actStyleJKArr['ThreadsSide'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_thread_view_2.png";
                    $scope.actStyleJKArr['ThreadsFold'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_thread_view_4.png";
                    //shirt_images/cuffs/1_buttonhole_view_3.png

                    $scope.actStyleJKArr['SingleButtonHole'] = basePath + "pub/media/shirt-tool/shirt_images/collarstyle/" + $scope.threadcolorId + "_single_thread_view_1.png";
                    $scope.actStyleJKArr['SingleButtonHoleSide'] = basePath + "pub/media/shirt-tool/shirt_images/collarstyle/" + $scope.threadcolorId + "_single_thread_view_2.png";

                    $scope.actStyleJKArr['SleevePlacketThreadLeft'] = basePath + "pub/media/shirt-tool/shirt_images/sleeveplacket/" + $scope.threadcolorId + "_placket_thread_left_view_3.png";
                    $scope.actStyleJKArr['SleevePlacketThreadRight'] = basePath + "pub/media/shirt-tool/shirt_images/sleeveplacket/" + $scope.threadcolorId + "_placket_thread_right_view_3.png";

                    $scope.actStyleJKArr['BottonBaseZoomHole'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_base_final_buttonhole_view_5.png";

                    $scope.actStyleJKArr['BottonBaseZoomThread'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_base_final_buttonthread_view_5.png";

                    if (styleTypeId == "5" || styleTypeId == "8" || styleTypeId == "9" || styleTypeId == "10") {
                        //2ButtonCuff 
                        $scope.actStyleJKArr['ButtonThreadCuffBack'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + $scope.threadcolorId + "_double_buttonhole_view_3.png";
                        $scope.actStyleJKArr['ButtonThreadCuff'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + $scope.threadcolorId + "_double_thread_view_4.png";
                        $scope.actStyleJKArr['ButtonHoleCuffFold'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + $scope.threadcolorId + "_double_buttonhole_view_4.png";


                    } else if (styleTypeId == "1" || styleTypeId == "2" || styleTypeId == "6" || styleTypeId == "9" || styleTypeId == "12") {
                        //French 
                        jQuery("#mainImageHolder").find(".shirt_part[rel=ButtonThreadCuff]").hide();
                        jQuery("#mainImageHolder").find(".shirt_part[rel=ButtonHoleCuffFold]").hide();
                    } else {
                        //BottonSingle6 4 3 7 11
                        $scope.actStyleJKArr['ButtonThreadCuffBack'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + $scope.threadcolorId + "_single_buttonhole_view_3.png";
                        $scope.actStyleJKArr['ButtonThreadCuff'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + $scope.threadcolorId + "_single_thread_view_4.png";
                        $scope.actStyleJKArr['ButtonHoleCuffFold'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + $scope.threadcolorId + "_single_buttonhole_view_4.png";

                        //$scope.actStyleJKArr['ButtonThreadCuff'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + $scope.threadcolorId + "_single_thread_view_4.png";
                    }//button end here


                    //   angular.element('#divaccent_4 ul.Threads').css("display", "none");
//                angular.element('#divaccent_4 ul.Threads').css("display", "none");
//                styleImg = $scope.actStyleJKArr['ButtonHole'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_buttonhole_view_1.png";
//                $scope.actStyleJKArr['ButtonHole'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_buttonhole_view_1.png";
//                
//                //$scope.actStyleJKArr['Threads'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_thread_view_1.png";
//                $scope.actStyleJKArr['ButtonHoleSide'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_buttonhole_view_2.png";
//                $scope.actStyleJKArr['ButtonHoleFold'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_buttonhole_view_4.png";
//                $scope.actStyleJKArr['BottonZoomHole'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_buttonhole_view_5.png";
//                
//                $scope.actStyleJKArr['BottonZoomThread'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_thread_view_5.png";
//                $scope.actStyleJKArr['Threads'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_thread_view_1.png";
//                $scope.actStyleJKArr['ThreadsSide'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_thread_view_2.png";
//                $scope.actStyleJKArr['ThreadsFold'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_thread_view_4.png";                                                    
//                //shirt_images/cuffs/1_buttonhole_view_3.png
//                $scope.actStyleJKArr['ButtonThreadCuffBack'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + $scope.threadcolorId + "_buttonhole_view_3.png";
//                $scope.actStyleJKArr['ButtonThreadCuff'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + $scope.threadcolorId + "_buttonhole_view_4.png";
//                            //Threads
                    //for cuff Buttons
                    if (styleTypeId == "5" || styleTypeId == "8" || styleTypeId == "9" || styleTypeId == "10") {
                        //2ButtonCuff ButtonThreadCuff ButtonHoleCuffFold

                        angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonCuff"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/cuffs/Static/Folded_CuffDouble_b1.png");
                        angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonThreadCuff"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/cuffs/Static/Folded_CuffDoublethread.png");
                        angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonCuffBack"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/cuffs/Static/Back_CuffDouble_b1.png");
                        angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonThreadCuffBack"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/cuffs/Static/Back_CuffDoublethread.png");
                        angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonHoleCuffFold"]').show();
                        angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonThreadCuffBack"]').show();
                        //angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonHoleFold"]').show();
                        //angular.element("#mainImageHolder").find('.shirt_part[rel="ThreadsFold"]').show();
                        jQuery("#divaccent_4 ul li#2").show();
                        jQuery("#divaccent_4 ul li#3").show();

                    } else if (styleTypeId == "1" || styleTypeId == "2" || styleTypeId == "6" || styleTypeId == "9" || styleTypeId == "12") {
                        //French 
                        angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonCuff"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/cuffs/Static/Folded_Common_French_Cufflink.png");
                        angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonThreadCuff"]').attr("src", basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png");
                        angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonCuffBack"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/cuffs/Static/Back_Common_French_Cufflink.png");
                        angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonThreadCuffBack"]').attr("src", basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png");
                        angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonHoleCuffFold"]').hide();
                        angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonThreadCuffBack"]').hide();
                        //angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonHoleFold"]').hide();
                        //angular.element("#mainImageHolder").find('.shirt_part[rel="ThreadsFold"]').hide();
                        jQuery("#divaccent_4 ul li#2").hide();
                        jQuery("#divaccent_4 ul li#3").hide();
                    } else {
                        //BottonSingle6 4 3 7 11

                        angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonCuff"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/cuffs/Static/Folded_CuffSingle_b1.png");
                        angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonThreadCuff"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/cuffs/Static/Folded_CuffSinglethread.png");
                        angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonCuffBack"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/cuffs/Static/Back_CuffSingle_b1.png");
                        angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonThreadCuffBack"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/cuffs/Static/Back_CuffSinglethread.png");
                        angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonHoleCuffFold"]').show();
                        angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonThreadCuffBack"]').show();
                        //angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonHoleFold"]').show();
                        //angular.element("#mainImageHolder").find('.shirt_part[rel="ThreadsFold"]').show();
                        jQuery("#divaccent_4 ul li#2").show();
                        jQuery("#divaccent_4 ul li#3").show();
                    }
                    //cuff buttons End here   
                    $scope.actStyleJKArr['SingleButtonHole'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_single_thread_view_1.png";
                    $scope.actStyleJKArr['SingleButtonHoleSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_single_thread_view_2.png";

                    $scope.actStyleJKArr['SleevePlacketThreadLeft'] = basePath + "pub/media/shirt-tool/shirt_images/sleeveplacket/" + $scope.fabricId + "_placket_thread_left_view_3.png";
                    $scope.actStyleJKArr['SleevePlacketThreadRight'] = basePath + "pub/media/shirt-tool/shirt_images/sleeveplacket/" + $scope.fabricId + "_placket_thread_right_view_3.png";
                } else if (styleId == "2")
                {//all
                    angular.element('#divaccent_4 ul.Threads').css("display", "block");
                    $scope.threadcolorId = angular.element(".Threads").find(".activeDesign").parent().attr("id") ? angular.element(".Threads").find(".activeDesign").parent().attr("id") : angular.element(".Threads threadcolor:first").attr("id");
                    //$scope.threadcolorId = angular.element(".Threads").find(".activeDesign").parent().attr("id") ? angular.element(".Threads").find(".activeDesign").parent().attr("id") : angular.element(".Threads fabricviewaccent:first").attr("id");
                    //shirt_images/plackets/1_buttonhole_view_1.png
                    styleImg = $scope.actStyleJKArr['ButtonHole'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_buttonhole_view_1.png";
                    $scope.actStyleJKArr['ButtonHole'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_buttonhole_view_1.png";
                    $scope.actStyleJKArr['ButtonHoleSide'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_buttonhole_view_2.png";
                    $scope.actStyleJKArr['ButtonHoleFold'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_buttonhole_view_4.png";
                    $scope.actStyleJKArr['BottonZoomHole'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_buttonhole_view_5.png";

                    $scope.actStyleJKArr['BottonZoomThread'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_thread_view_5.png";
                    $scope.actStyleJKArr['Threads'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_thread_view_1.png";
                    $scope.actStyleJKArr['ThreadsSide'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_thread_view_2.png";
                    $scope.actStyleJKArr['ThreadsFold'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_thread_view_4.png";
                    //shirt_images/cuffs/1_buttonhole_view_3.png

                    $scope.actStyleJKArr['SingleButtonHole'] = basePath + "pub/media/shirt-tool/shirt_images/collarstyle/" + $scope.threadcolorId + "_single_thread_view_1.png";
                    $scope.actStyleJKArr['SingleButtonHoleSide'] = basePath + "pub/media/shirt-tool/shirt_images/collarstyle/" + $scope.threadcolorId + "_single_thread_view_2.png";

                    $scope.actStyleJKArr['SleevePlacketThreadLeft'] = basePath + "pub/media/shirt-tool/shirt_images/sleeveplacket/" + $scope.threadcolorId + "_placket_thread_left_view_3.png";
                    $scope.actStyleJKArr['SleevePlacketThreadRight'] = basePath + "pub/media/shirt-tool/shirt_images/sleeveplacket/" + $scope.threadcolorId + "_placket_thread_right_view_3.png";

                    $scope.actStyleJKArr['BottonBaseZoomHole'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_base_final_buttonhole_view_5.png";

                    $scope.actStyleJKArr['BottonBaseZoomThread'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_base_final_buttonthread_view_5.png";

                    if (styleTypeId == "5" || styleTypeId == "8" || styleTypeId == "9" || styleTypeId == "10") {
                        //2ButtonCuff 
                        $scope.actStyleJKArr['ButtonThreadCuffBack'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + $scope.threadcolorId + "_double_buttonhole_view_3.png";
                        $scope.actStyleJKArr['ButtonThreadCuff'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + $scope.threadcolorId + "_double_thread_view_4.png";
                        $scope.actStyleJKArr['ButtonHoleCuffFold'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + $scope.threadcolorId + "_double_buttonhole_view_4.png";


                    } else if (styleTypeId == "1" || styleTypeId == "2" || styleTypeId == "6" || styleTypeId == "9" || styleTypeId == "12") {
                        //French 
                        jQuery("#mainImageHolder").find(".shirt_part[rel=ButtonThreadCuff]").hide();
                        jQuery("#mainImageHolder").find(".shirt_part[rel=ButtonHoleCuffFold]").hide();
                    } else {
                        //BottonSingle6 4 3 7 11
                        $scope.actStyleJKArr['ButtonThreadCuffBack'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + $scope.threadcolorId + "_single_buttonhole_view_3.png";
                        $scope.actStyleJKArr['ButtonThreadCuff'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + $scope.threadcolorId + "_single_thread_view_4.png";
                        $scope.actStyleJKArr['ButtonHoleCuffFold'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + $scope.threadcolorId + "_single_buttonhole_view_4.png";

                        //$scope.actStyleJKArr['ButtonThreadCuff'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + $scope.threadcolorId + "_single_thread_view_4.png";
                    }//button end here



                } else if (styleId == "3")
                {// only cuff
                    angular.element('#divaccent_4 ul.Threads').css("display", "block");
                    //  angular.element(".Threads").find(".activeDesign").parent().attr("id") ? angular.element(".Threads").find(".activeDesign").parent().attr("id") : angular.element(".Threads threadcolor:first").attr("id");
                    $scope.threadcolorId = angular.element(".Threads").find(".activeDesign").parent().attr("id") ? angular.element(".Threads").find(".activeDesign").parent().attr("id") : angular.element(".Threads threadcolor:first").attr("id");

                    //styleImg = $scope.actStyleJKArr['ButtonThreadCuffBack'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + $scope.threadcolorId + "_buttonhole_view_3.png";
                    //styleImg = $scope.actStyleJKArr['ButtonThreadCuff'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + $scope.threadcolorId + "_buttonhole_view_4.png";


                    if (styleTypeId == "5" || styleTypeId == "8" || styleTypeId == "9" || styleTypeId == "10") {
                        //2ButtonCuff 
                        $scope.actStyleJKArr['ButtonThreadCuffBack'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + $scope.threadcolorId + "_double_buttonhole_view_3.png";
                        $scope.actStyleJKArr['ButtonThreadCuff'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + $scope.threadcolorId + "_double_thread_view_4.png";
                        $scope.actStyleJKArr['ButtonHoleCuffFold'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + $scope.threadcolorId + "_double_buttonhole_view_4.png";


                    } else if (styleTypeId == "1" || styleTypeId == "2" || styleTypeId == "6" || styleTypeId == "9" || styleTypeId == "12") {
                        //French 
                        jQuery("#mainImageHolder").find(".shirt_part[rel=ButtonThreadCuff]").hide();
                        jQuery("#mainImageHolder").find(".shirt_part[rel=ButtonHoleCuffFold]").hide();
                    } else {
                        //BottonSingle6 4 3 7 11
                        $scope.actStyleJKArr['ButtonThreadCuffBack'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + $scope.threadcolorId + "_single_buttonhole_view_3.png";
                        $scope.actStyleJKArr['ButtonThreadCuff'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + $scope.threadcolorId + "_single_thread_view_4.png";
                        $scope.actStyleJKArr['ButtonHoleCuffFold'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + $scope.threadcolorId + "_single_buttonhole_view_4.png";

                        //$scope.actStyleJKArr['ButtonThreadCuff'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + $scope.threadcolorId + "_single_thread_view_4.png";
                    }//button end here


                    $scope.actStyleJKArr['SleevePlacketThreadLeft'] = basePath + "pub/media/shirt-tool/shirt_images/sleeveplacket/" + $scope.threadcolorId + "_placket_thread_left_view_3.png";
                    $scope.actStyleJKArr['SleevePlacketThreadRight'] = basePath + "pub/media/shirt-tool/shirt_images/sleeveplacket/" + $scope.threadcolorId + "_placket_thread_right_view_3.png";

//                styleImg = $scope.actStyleJKArr['ButtonHoleSide'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";
//                styleImg = $scope.actStyleJKArr['ButtonHoleFold'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";
//                styleImg = $scope.actStyleJKArr['BottonZoomThread'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";


                    $scope.actStyleJKArr['ButtonHole'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + 4 + "_buttonhole_view_1.png";
                    $scope.actStyleJKArr['ButtonHoleSide'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + 4 + "_buttonhole_view_2.png";
                    $scope.actStyleJKArr['ButtonHoleFold'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + 4 + "_buttonhole_view_4.png";
                    $scope.actStyleJKArr['BottonZoomHole'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + 4 + "_buttonhole_view_5.png";

                    $scope.actStyleJKArr['BottonZoomThread'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + 4 + "_thread_view_5.png";
                    $scope.actStyleJKArr['Threads'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + 4 + "_thread_view_1.png";
                    $scope.actStyleJKArr['ThreadsSide'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + 4 + "_thread_view_2.png";
                    $scope.actStyleJKArr['ThreadsFold'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + 4 + "_thread_view_4.png";
                    //collar single button thread
                    $scope.actStyleJKArr['SingleButtonHole'] = basePath + "pub/media/shirt-tool/shirt_images/collarstyle/" + 4 + "_single_thread_view_1.png";
                    $scope.actStyleJKArr['SingleButtonHoleSide'] = basePath + "pub/media/shirt-tool/shirt_images/collarstyle/" + 4 + "_single_thread_view_2.png";
                    $scope.actStyleJKArr['BottonBaseZoomHole'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + 4 + "_base_final_buttonhole_view_5.png";

                    $scope.actStyleJKArr['BottonBaseZoomThread'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + 4 + "_base_final_buttonthread_view_5.png";
                    angular.element("#mainImageHolder").find('.shirt_part[rel="Threads"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/plackets/" + 4 + "_thread_view_1.png");

                } else if (styleId == "4")
                {//placket thread
                    angular.element('#divaccent_4 ul.Threads').css("display", "block");
                    $scope.threadcolorId = angular.element(".Threads").find(".activeDesign").parent().attr("id") ? angular.element(".Threads").find(".activeDesign").parent().attr("id") : angular.element(".Threads threadcolor:first").attr("id");

                    //styleImg = $scope.actStyleJKArr['ButtonHole'] = basePath + "pub/media/shirt-tool/shirt_images/elbowpatches/" + $scope.backelbofabId + "_elbowPatches_view_3.png";
                    styleImg = $scope.actStyleJKArr['ButtonHole'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_buttonhole_view_1.png";
                    $scope.actStyleJKArr['ButtonHole'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_buttonhole_view_1.png";
                    $scope.actStyleJKArr['ButtonHoleSide'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_buttonhole_view_2.png";
                    $scope.actStyleJKArr['ButtonHoleFold'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_buttonhole_view_4.png";
                    $scope.actStyleJKArr['BottonZoomHole'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_buttonhole_view_5.png";

                    $scope.actStyleJKArr['BottonZoomThread'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_thread_view_5.png";
                    $scope.actStyleJKArr['Threads'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_thread_view_1.png";
                    $scope.actStyleJKArr['ThreadsSide'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_thread_view_2.png";
                    $scope.actStyleJKArr['ThreadsFold'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_thread_view_4.png";
                    //collar single button thread
                    $scope.actStyleJKArr['SingleButtonHole'] = basePath + "pub/media/shirt-tool/shirt_images/collarstyle/" + $scope.threadcolorId + "_single_thread_view_1.png";
                    $scope.actStyleJKArr['SingleButtonHoleSide'] = basePath + "pub/media/shirt-tool/shirt_images/collarstyle/" + $scope.threadcolorId + "_single_thread_view_2.png";
                    $scope.actStyleJKArr['BottonBaseZoomHole'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_base_final_buttonhole_view_5.png";

                    $scope.actStyleJKArr['BottonBaseZoomThread'] = basePath + "pub/media/shirt-tool/shirt_images/plackets/" + $scope.threadcolorId + "_base_final_buttonthread_view_5.png";




                    if (styleTypeId == "5" || styleTypeId == "8" || styleTypeId == "9" || styleTypeId == "10") {
                        //2ButtonCuff 
                        $scope.actStyleJKArr['ButtonThreadCuffBack'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + 4 + "_double_buttonhole_view_3.png";
                        $scope.actStyleJKArr['ButtonThreadCuff'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + 4 + "_double_thread_view_4.png";
                        $scope.actStyleJKArr['ButtonHoleCuffFold'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + 4 + "_double_buttonhole_view_4.png";


                    } else if (styleTypeId == "1" || styleTypeId == "2" || styleTypeId == "6" || styleTypeId == "9" || styleTypeId == "12") {
                        //French 
                        jQuery("#mainImageHolder").find(".shirt_part[rel=ButtonThreadCuff]").hide();
                        jQuery("#mainImageHolder").find(".shirt_part[rel=ButtonHoleCuffFold]").hide();
                    } else {
                        //BottonSingle6 4 3 7 11
                        $scope.actStyleJKArr['ButtonThreadCuffBack'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + 4 + "_single_buttonhole_view_3.png";
                        $scope.actStyleJKArr['ButtonThreadCuff'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + 4 + "_single_thread_view_4.png";
                        $scope.actStyleJKArr['ButtonHoleCuffFold'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + 4 + "_single_buttonhole_view_4.png";

                        //$scope.actStyleJKArr['ButtonThreadCuff'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + 4 + "_single_thread_view_4.png";
                    }//button end here


                    $scope.actStyleJKArr['SleevePlacketThreadLeft'] = basePath + "pub/media/shirt-tool/shirt_images/sleeveplacket/" + 4 + "_placket_thread_left_view_3.png";
                    $scope.actStyleJKArr['SleevePlacketThreadRight'] = basePath + "pub/media/shirt-tool/shirt_images/sleeveplacket/" + 4 + "_placket_thread_right_view_3.png";



                }

            } else if (parent == "ElbowPatch") {

                styleId = angular.element('#divaccent_5 ul:first').find('.activeDesign').attr('id') ? angular.element('#divaccent_5 ul:first').find('.activeDesign').attr('id') : "1";

                if (styleId == 1)
                {
                    angular.element('#divaccent_5 ul.ElbowPatch').css("display", "none");
                    styleImg = $scope.actStyleJKArr['backelbow'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";
                } else
                {
                    angular.element('#divaccent_5 ul.ElbowPatch').css("display", "block");
                    $scope.backelbofabId = angular.element(".ElbowPatch").find(".activeDesign").parent().attr("id") ? angular.element(".ElbowPatch").find(".activeDesign").parent().attr("id") : angular.element(".ElbowPatch fabricviewaccent:first").attr("id");

                    styleImg = $scope.actStyleJKArr['backelbow'] = basePath + "pub/media/shirt-tool/shirt_images/elbowpatches/" + $scope.backelbofabId + "_elbowPatches_view_3.png";

                }

            } else if (parent == "Chestpleats") {

                console.log("IsShirt= = " + $scope.IsShirt)//false
                if ($scope.IsShirt) {

                    if (styleId == "1") {

                        $scope.actStyleJKArr['Tuxido'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";
                        $scope.actStyleJKArr['TuxidoSide'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";
                        $scope.actStyleJKArr['TuxidoFold'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";
                        $scope.actStyleJKArr['TuxidoZoom'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";

                        $scope.actStyleJKArr['casualTuxido'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";
                        $scope.actStyleJKArr['casualTuxidoSide'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";

                    } else {

                        $scope.actStyleJKArr['Tuxido'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + "Shirt_Tuxedo_Front_Glow_1.png";
                        $scope.actStyleJKArr['TuxidoSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + "Shirt_Tuxedo_Side_Glow_2.png";
                        $scope.actStyleJKArr['TuxidoFold'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + "Shirt_Tuxedo_Folded_Glow_4.png";
                        $scope.actStyleJKArr['TuxidoZoom'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + "Tuxedo_Zoom_View.png";

                        $scope.actStyleJKArr['casualTuxido'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + "Casual_Front_Tuxedo_Glow.png";
                        $scope.actStyleJKArr['casualTuxidoSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + "Casual_Side_Tuxedo_Glow.png";
                    }
                } else
                {
                    if (styleId == "1") {

                        $scope.actStyleJKArr['Tuxido'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";
                        $scope.actStyleJKArr['TuxidoSide'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";
                        $scope.actStyleJKArr['TuxidoFold'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";
                        $scope.actStyleJKArr['TuxidoZoom'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";

                        $scope.actStyleJKArr['casualTuxido'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";
                        $scope.actStyleJKArr['casualTuxidoSide'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";

                    } else {


                        $scope.actStyleJKArr['Tuxido'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + "Shirt_Tuxedo_Front_Glow_1.png";
                        $scope.actStyleJKArr['TuxidoSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + "Shirt_Tuxedo_Side_Glow_2.png";
                        $scope.actStyleJKArr['TuxidoFold'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + "Shirt_Tuxedo_Folded_Glow_4.png";
                        $scope.actStyleJKArr['TuxidoZoom'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + "Tuxedo_Zoom_View.png";

                        $scope.actStyleJKArr['casualTuxido'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + "Casual_Front_Tuxedo_Glow.png";
                        $scope.actStyleJKArr['casualTuxidoSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + "Casual_Side_Tuxedo_Glow.png";
                    }
                }
            }


            if (styleType == "shirt")
            {

                $scope.actStyleJKArr[parent] = styleImg;
                $scope.actStyleJKArr[parent + "_index"] = ind;
                $scope.actStyleJKArr[parent + '_styleName'] = styleName;
                $scope.actStyleJKArr[parent + '_id'] = styleId;
            } else if (styleType == "pant") {

                $scope.actStylePantArr[parent] = styleImg;
                $scope.actStylePantArr[parent + "_index"] = ind;
                $scope.actStylePantArr[parent + '_styleName'] = styleName;
                $scope.actStylePantArr[parent + '_id'] = styleId;
            } else if (styleType == "vest") {

                $scope.actStyleVestArr[parent] = styleImg;
                $scope.actStyleVestArr[parent + "_index"] = ind;
                $scope.actStyleVestArr[parent + '_styleName'] = styleName;
                $scope.actStyleVestArr[parent + '_id'] = styleId;
            }


            updatePrice();
            updateProduct();
            //hideJacket();
            imageLoaded();
        }

        function dispStyleBtn(styleId)
        {

            jQuery("#divs_7 li").each(function () {

                if (jQuery(this).attr("style_id") == styleId) {
                    jQuery(this).show();
                } else {
                    jQuery(this).hide();
                }
            });
            jQuery("#divvest_7 li").each(function () {

                if (jQuery(this).attr("style_id") == styleId) {
                    jQuery(this).show();
                } else {
                    jQuery(this).hide();
                }
            });
        }

        function updateProduct()
        {


//check this use view id dynamic
            jQuery("#view1 img").each(function () {

                var parent = jQuery(this).attr('rel');
                var type = jQuery(this).attr('type');
                if (type == "shirt")
                {

                    if ($scope.actStyleJKArr[parent])
                    {

                        jQuery(this).attr('src', $scope.actStyleJKArr[parent]);
                        jQuery(this).attr("index", $scope.actStyleJKArr[parent + "_index"]);
                        jQuery(this).attr('styleName', $scope.actStyleJKArr[parent + '_styleName']);
                        jQuery(this).attr('id', $scope.actStyleJKArr[parent + '_id']);
                        jQuery(this).attr('btnCount', $scope.actStyleJKArr[parent + '_btnCount']);
                        jQuery(this).attr('label', $scope.actStyleJKArr[parent + '_label']);
                    }

                }

            });
            jQuery("#view2 img").each(function () {

                var parent = jQuery(this).attr('rel');
                var type = jQuery(this).attr('type');
                if (type == "shirt")
                {
                    if ($scope.actStyleJKArr[parent])
                    {

                        jQuery(this).attr('src', $scope.actStyleJKArr[parent]);
                        jQuery(this).attr("index", $scope.actStyleJKArr[parent + "_index"]);
                        jQuery(this).attr('styleName', $scope.actStyleJKArr[parent + '_styleName']);
                        jQuery(this).attr('id', $scope.actStyleJKArr[parent + '_id']);
                        jQuery(this).attr('label', $scope.actStyleJKArr[parent + '_label']);
                    }

                }

//check this use view id dynamic
                jQuery("#view3 img").each(function () {

                    var parent = jQuery(this).attr('rel');
                    var type = jQuery(this).attr('type');
                    if (type == "shirt")
                    {
                        if ($scope.actStyleJKArr[parent])
                        {


                            jQuery(this).attr('src', $scope.actStyleJKArr[parent]);
                            jQuery(this).attr("index", $scope.actStyleJKArr[parent + "_index"]);
                            jQuery(this).attr('styleName', $scope.actStyleJKArr[parent + '_styleName']);
                            jQuery(this).attr('id', $scope.actStyleJKArr[parent + '_id']);
                            jQuery(this).attr('btnCount', $scope.actStyleJKArr[parent + '_btnCount']);
                            jQuery(this).attr('label', $scope.actStyleJKArr[parent + '_label']);
                        }

                    }


                });
                jQuery("#view4 img").each(function () {

                    var parent = jQuery(this).attr('rel');
                    var type = jQuery(this).attr('type');
                    if (type == "shirt")
                    {
                        if ($scope.actStyleJKArr[parent])
                        {

                            jQuery(this).attr('src', $scope.actStyleJKArr[parent]);
                            jQuery(this).attr("index", $scope.actStyleJKArr[parent + "_index"]);
                            jQuery(this).attr('styleName', $scope.actStyleJKArr[parent + '_styleName']);
                            jQuery(this).attr('id', $scope.actStyleJKArr[parent + '_id']);
                            jQuery(this).attr('btnCount', $scope.actStyleJKArr[parent + '_btnCount']);
                            jQuery(this).attr('label', $scope.actStyleJKArr[parent + '_label']);
                        }

                    }

                });
                jQuery("#view5 img").each(function () {

                    var parent = jQuery(this).attr('rel');
                    var type = jQuery(this).attr('type');
                    if (type == "shirt")
                    {

                        if ($scope.actStyleJKArr[parent])
                        {

                            jQuery(this).attr('src', $scope.actStyleJKArr[parent]);
                            jQuery(this).attr("index", $scope.actStyleJKArr[parent + "_index"]);
                            jQuery(this).attr('styleName', $scope.actStyleJKArr[parent + '_styleName']);
                            jQuery(this).attr('id', $scope.actStyleJKArr[parent + '_id']);
                            jQuery(this).attr('btnCount', $scope.actStyleJKArr[parent + '_btnCount']);
                            jQuery(this).attr('label', $scope.actStyleJKArr[parent + '_label']);
                        }

                    }

                });
            });
        }


//load fabric for suit
        $scope.fabricClick = function ($event, obj) {

            currStyleName = "fabric";

            angular.element(".suit-preloader").show();
            angular.element(".mainToolarea").css({"opacity": "0"});
            angular.element("#div4").find(".activeDesign").removeClass('activeDesign');
            angular.element($event.currentTarget).find(".fabvie").addClass("activeDesign");

            fabricId = $scope.fabricId = obj.id;



            shirtFabricName = $scope.fabricName = obj.name;

            $scope.fabricBtnId = obj.button_color ? obj.button_color : "1";



            //update fabric preview image
            var fabricVal = angular.element($event.currentTarget).find("img").attr("data-zoom-image");

            angular.element('#slide').attr('src', fabricVal);
            angular.element('.fabricName').html(obj.name);
            angular.element('.fabricType').html(obj.type);
            angular.element('.fabricPrice').html(obj.price);
            //for responsive

            if (jQuery(window).width() <= 767) {
                jQuery("body").removeClass("LeftpanemOpen")
                jQuery(".leftpanelsection").css("left", "0");
            }

            applyFabricOnSuit();
            updatePrice();
            imageLoaded();
        }

        $scope.changeView = function ($event, viewId) {

            angular.element(".suit-preloader").show();
            angular.element(".mainToolarea").css({"opacity": "0"});
            $scope.activeView = viewId;
            active_view_Id = viewId;
            changeView();
        }


        /*
         * load data static way
         $scope.fabricData = fabricService.getFabricData();
         */

        /*
         * load data dynamic way
         $scope.fabricData = fabricService.getFabricData();
         */
//get fabric data
        var handleSuccessFabric = function (data, status) {

            if (data)
            {
//for clone data
                var strData = JSON.stringify(data);
                $scope.fabricData = JSON.parse(strData);
                tempPj = JSON.parse(strData);
                //fabric filter start
                $scope.fabrics = $scope.fabricData.fabric;
                $scope.categories = $scope.fabricData.category;

                //Material
                $scope.materialGroup = uniqueItems($scope.fabrics, 'material_parent');
                //Pattern
                $scope.patternGroup = uniqueItems($scope.fabrics, 'pattern_parent');
                //seasons
                $scope.seasonGroup = uniqueItems($scope.fabrics, 'season_parent');
                //colors
                $scope.colorGroup = uniqueItems($scope.fabrics, 'color_parent');
                //collection
                $scope.categoryGroup = uniqueItems($scope.fabrics, 'category_parent');

                //fabric filter end

                $scope.backColElboFabricData = tempPj.fabric;

                fabricService.getThreadFabricData().success(handleSuccessFabricThread).error(function (data, status) {

                    console.log(status);
                });
            } else {
                console.log(data.error);
            }
        }

        var handleSuccessFabricThread = function (data, status) {

            if (data)
            {
                $scope.fabricDataThread = data;
                //call designData
                designService.getDesignData().success(handleSuccessDesign).error(function (data, status) {
                    console.log(status);
                });
            } else {
                console.log(data.error);
            }
        }

        $scope.fonts = fabricService.getFonts();
        $scope.monoColors = fabricService.getMonoColors();
        $scope.position = fabricService.getPos();
        fabricService.getFabricData().success(handleSuccessFabric).error(function (data, status) {

            console.log(status);
        });
        //get design data
        var handleSuccessDesign = function (data, status) {

            if (data)
            {
                $scope.designData = data;
                tempJk = $scope.designData;
                suitAccentService.getSuitAccentData().success(handleSuccessSuitAccent).error(function (data, status) {
                    console.log(status);
                });
            } else {
                console.log(data.error);
            }
        }



        /*//load suit vest data
         var handleSuccessVest = function (data, status) {
         
         if (data)
         {
         $scope.vestData = data;
         tempVst = data;
         
         }
         else {
         console.log(data.error);
         }
         }*/


        /*//load suit vest data
         var handleSuccessPant = function (data, status) {
         
         if (data)
         {
         $scope.pantData = data;
         
         tempPt = data;
         setTimeout(function () {
         applyFabricOnSuitInit();
         
         }, 500);
         
         }
         else {
         console.log(data.error);
         }
         }
         
         pantService.getPantData().success(handleSuccessPant).error(function(data, status) {
         console.log(status);
         });*/

//load suit vest data
        var handleSuccessSuitAccent = function (data, status) {

            if (data)
            {
                $scope.suitAccentData = data;
                tempAccnt = data;
                $scope.threadFabricData = $scope.suitAccentData[7].style;
                //console.log($scope.threadFabricData);
                applyFabricOnSuitInit();
                updatePrice();
            } else {
                console.log(data.error);
            }
        }




        function applyFabricOnSuit()
        {

            var first = tempJk;

            var suitDesignData = first;//jQuery.merge(jQuery.merge([], data1), forth);

            var parent, styleImg, styleType, styleName, styleId, labelName;

            $scope.allDesignStyles = "";
            var styleCnt;


            // var a = jQuery("#div1 .box_options>div").length;

            for (var design in suitDesignData) {


                if (suitDesignData.hasOwnProperty(design)) {

                    //console.log("IN fab loop " + " design=" + design);
                    $scope.allDesignStyles = suitDesignData[design]["style"][0];
                    if ($scope.allDesignStyles) {

                        if (!$scope.allDesignStyles.parent)
                        {

                            continue;
                        }

                        parent = $scope.allDesignStyles.parent.replace(/\s+|&/g, "");

                        styleName = $scope.allDesignStyles.name.replace(/\s+|&/g, "");

                        labelName = $scope.allDesignStyles.labelName;
                        styleId = $scope.allDesignStyles.id;



//                    if ($scope.allDesignStyles.price) {
//                        stylePriceArr[parent] = $scope.allDesignStyles.price;
//                    } else {
//                        stylePriceArr[parent] = "0.0";
//                    }
//console.log("suitDesignData[design].price="+suitDesignData[design].price)

//                    for (var i = 1; i <= design; i++) {
//var a=jQuery("#div1 .box_options ul").find(".activeDesign");

                        //console.log("IN fab loop Cnt = " + Cnt + " design = " + design);
                        //     stylePriceArr[design] = jQuery("#div1  #divs_" + design + " .activeDesign").find(".price").text().trim("");

//                    }


//get style type single or double
                        styleType = $scope.allDesignStyles.designType; //angular.element("#view" + $scope.activeView).find('.shirt_part[rel=' + parent + ']').attr("type");

                        if ($scope.actStyleData[parent]) {
                            styleId = $scope.actStyleData[parent];
                        }
                        console.log("appfab parent=" + parent)


//                    styleImg = basePath + "pub/media/shirt-tool/shirt_images/sleeves/" + $scope.fabricId + "_sleevesstyle" + "_1_size__sleevestype__" + styleId + "_view_1.png";
//                    $scope.actStyleJKArr['SleevesSide'] = basePath + "pub/media/shirt-tool/shirt_images/sleeves/" + $scope.fabricId + "_sleevesstyle" + "_1_size__sleevestype__" + styleId + "_view_2.png";
//                    $scope.actStyleJKArr['SleevesBack'] = basePath + "pub/media/shirt-tool/shirt_images/sleeves/" + $scope.fabricId + "_sleevesstyle" + "_1_size__sleevestype__" + styleId + "_view_3.png";

                        if (parent == "CollarStyle") {
                            angular.element("#mainImageHolder").find('.shirt_part[rel=casualWomanBackCollar]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/collarstyle/back_collar/" + $scope.fabricId + "_style_1_casual_front_view_1.png");
                            if (styleId == '6') {

                                angular.element("#mainImageHolder").find('.shirt_part[rel=Pin]').show();
                                angular.element("#mainImageHolder").find('.shirt_part[rel=PinSide]').show();
                                angular.element("#mainImageHolder").find('.shirt_part[rel=PinFold]').show();
                                angular.element("#viewport").find('.shirt_part[rel=PinZoom]').show();

                                angular.element("#viewport").find('.shirt_part[rel=ButtondownZoom]').hide();
                                angular.element("#mainImageHolder").find('.shirt_part[rel=ButtonDown]').hide();
                                angular.element("#mainImageHolder").find('.shirt_part[rel=ButtonDownSide]').hide();
                                angular.element("#mainImageHolder").find('.shirt_part[rel=ButtonDownFold]').hide();
                                angular.element("#viewport").find('.shirt_part[rel=ButtondownThreadZoom]').hide();
                                angular.element("#viewport").find('.shirt_part[rel=ButtondownHoleZoom]').hide();
                                angular.element("#mainImageHolder").find('.shirt_part[rel=ButtonDownThreadFold]').hide();



                            } else if (styleId == '2') {
                                angular.element("#mainImageHolder").find('.shirt_part[rel=BottonDown]').show();
                                angular.element("#mainImageHolder").find('.shirt_part[rel=BottonDownSide]').show();
                                angular.element("#mainImageHolder").find('.shirt_part[rel=BottonDownFold]').show()
                                angular.element("#viewport").find('.shirt_part[rel=BottondownZoom]').show();

                                angular.element("#viewport").find('.shirt_part[rel=ButtondownZoom]').show();
                                angular.element("#mainImageHolder").find('.shirt_part[rel=ButtonDown]').show();
                                angular.element("#mainImageHolder").find('.shirt_part[rel=ButtonDownSide]').show();
                                angular.element("#mainImageHolder").find('.shirt_part[rel=ButtonDownFold]').show();
                                angular.element("#viewport").find('.shirt_part[rel=ButtondownThreadZoom]').show();
                                angular.element("#viewport").find('.shirt_part[rel=ButtondownHoleZoom]').show();
                                angular.element("#mainImageHolder").find('.shirt_part[rel=ButtonDownThreadFold]').show();


                            } else if (styleId == '8' || styleId == '10') {
                                jQuery("#tie").hide();
                                angular.element("#view1").find('.shirt_part[rel=ManTie]').hide();
                                angular.element("#view2").find('.shirt_part[rel=ManTieSide]').hide();
                                angular.element("#mainImageHolder").find('.shirt_part[rel=Pin]').hide();
                                angular.element("#mainImageHolder").find('.shirt_part[rel=PinSide]').hide();
                                angular.element("#mainImageHolder").find('.shirt_part[rel=PinFold]').hide();
                                angular.element("#viewport").find('.shirt_part[rel=PinZoom]').hide();

                                angular.element("#viewport").find('.shirt_part[rel=ButtondownZoom]').hide();
                                angular.element("#mainImageHolder").find('.shirt_part[rel=ButtonDown]').hide();
                                angular.element("#mainImageHolder").find('.shirt_part[rel=ButtonDownSide]').hide();
                                angular.element("#mainImageHolder").find('.shirt_part[rel=ButtonDownFold]').hide();
                                angular.element("#viewport").find('.shirt_part[rel=ButtondownThreadZoom]').hide();
                                angular.element("#viewport").find('.shirt_part[rel=ButtondownHoleZoom]').hide();
                                angular.element("#mainImageHolder").find('.shirt_part[rel=ButtonDownThreadFold]').hide();

                                $scope.actStyleJKArr['CollarBase'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_view_1.png";
                                $scope.actStyleJKArr['CollarBaseSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_view_2.png";
                                angular.element("#mainImageHolder").find('.shirt_part[rel="CollarStyle"]').hide();
                                angular.element("#mainImageHolder").find('.shirt_part[rel="CollarStyleSide"]').hide();
                                styleImg = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_view_1.png";
                                $scope.actStyleJKArr['CollarStyle'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";
                                $scope.actStyleJKArr['CollarStyleSide'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";

                            } else {
                                angular.element("#mainImageHolder").find('.shirt_part[rel=Pin]').hide();
                                angular.element("#mainImageHolder").find('.shirt_part[rel=PinSide]').hide();
                                angular.element("#mainImageHolder").find('.shirt_part[rel=PinFold]').hide();
                                angular.element("#viewport").find('.shirt_part[rel=PinZoom]').hide();

                                angular.element("#viewport").find('.shirt_part[rel=ButtondownZoom]').hide();
                                angular.element("#mainImageHolder").find('.shirt_part[rel=ButtonDown]').hide();
                                angular.element("#mainImageHolder").find('.shirt_part[rel=ButtonDownSide]').hide();
                                angular.element("#mainImageHolder").find('.shirt_part[rel=ButtonDownFold]').hide();
                                angular.element("#viewport").find('.shirt_part[rel=ButtondownThreadZoom]').hide();
                                angular.element("#viewport").find('.shirt_part[rel=ButtondownHoleZoom]').hide();
                                angular.element("#mainImageHolder").find('.shirt_part[rel=ButtonDownThreadFold]').hide();


                                angular.element("#mainImageHolder").find('.shirt_part[rel=ButtonDown]').hide();
                                angular.element("#mainImageHolder").find('.shirt_part[rel=BottonDownSide]').hide();
                                angular.element("#mainImageHolder").find('.shirt_part[rel=BottonDownFold]').hide();
                                angular.element("#viewport").find('.shirt_part[rel=BottondownZoom]').hide();
                            }
                            $scope.actStyleJKArr['CollarBase'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_front_view_1.png";
                            $scope.actStyleJKArr['CollarBaseSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_front_view_2.png";
                            // $scope.innercollarfabid = $scope.outercollarfabid = fabricId;
                            styleImg = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + fabricId + "_style" + "_" + styleId + "_view_1.png";
                            $scope.actStyleJKArr['CollarStyleSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + fabricId + "_style_" + styleId + "_view_2.png";
                            $scope.actStyleJKArr['CollarStyleBack'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + fabricId + "_style_" + styleId + "_view_3.png";

                            $scope.actStyleJKArr['CollarStyleFold'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + fabricId + "_style_" + styleId + "_view_4.png";
                            $scope.actStyleJKArr['CollarStyleZoom'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + fabricId + "_style_" + styleId + "_view_5.png";
                            $scope.actStyleJKArr['CollarStyleZoominner'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + fabricId + "_style_" + styleId + "_back_view_5.png";
                            $scope.actStyleJKArr['innerCollarGlow'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + fabricId + "_style_" + styleId + "_back_view_4.png";
                            $scope.actStyleJKArr['casualCollar'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/casual/" + fabricId + "_style_" + styleId + "_casual_collar_view_1.png";
                            $scope.actStyleJKArr['casualCollarSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/casual/" + fabricId + "_style_" + styleId + "_casual_collar_view_2.png";//1_style_casual_common_torso_view_1
                            $scope.actStyleJKArr['casualBase'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/casual/" + fabricId + "_style_casual_common_torso_view_1.png";
                            $scope.actStyleJKArr['casualBaseSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/casual/" + fabricId + "_style_casual_common_torso_view_2.png";

                            $scope.actStyleJKArr['WomanBackCollar'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/back_collar/" + $scope.fabricId + "_style_" + 1 + "_front_view_1.png";
                            $scope.actStyleJKArr['WomanBackPart'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/back_collar/" + $scope.fabricId + "_style_" + 1 + "_front_backpart_view_1.png";
                            $scope.actStyleJKArr['WomanBackPartSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/back_collar/" + $scope.fabricId + "_style_" + 1 + "_left_sidepart_view_2.png";
                            angular.element("#mainImageHolder").find("[rel=WomanBackCollarSide]").attr("src", basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/back_collar/" + $scope.fabricId + "_style_" + 1 + "_left_view_2.png");
                            angular.element("#mainImageHolder").find('.shirt_part[rel=casualMan]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/collarstyle/back_collar/" + $scope.fabricId + "_style_1_casual_left_sidepart_view_1.png");

                            angular.element("#mainImageHolder").find('.shirt_part[rel=WomanBackPartSide]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/collarstyle/back_collar/" + $scope.fabricId + "_style_1_left_sidepart_view_2.png");
                            angular.element('[rel=WomanBackPart]').show();
                            angular.element('[rel=WomanBackPartSide]').show();
                            $scope.actStyleJKArr['casualInnerCollar'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";//basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/casual/" + $scope.fabricId + "_style_" + styleId + "_casual_inner_view_1.png";
                            $scope.actStyleJKArr['casualInnerCollarSide'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";//basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/casual/" + $scope.fabricId + "_style_" + styleId + "_casual_inner_view_2.png";

                            angular.element("#mainImageHolder").find('.shirt_part[rel=casualInnerCollar]').attr("src", basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png");
                            angular.element("#mainImageHolder").find('.shirt_part[rel=casualInnerCollarSide]').attr("src", basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png");
                            if (styleId == '8' || styleId == '10') {
                                $scope.actStyleJKArr['CollarBase'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_view_1.png";
                                $scope.actStyleJKArr['CollarBaseSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_view_2.png";
                                angular.element("#mainImageHolder").find('.shirt_part[rel="CollarStyle"]').hide();
                                angular.element("#mainImageHolder").find('.shirt_part[rel="CollarStyleSide"]').hide();
                                styleImg = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_view_1.png";
                                $scope.actStyleJKArr['CollarStyle'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";
                                $scope.actStyleJKArr['CollarStyleSide'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";

                            } else {
                                angular.element("#mainImageHolder").find('.shirt_part[rel="CollarStyle"]').show();
                                angular.element("#mainImageHolder").find('.shirt_part[rel="CollarStyleSide"]').show();
                                $scope.actStyleJKArr['CollarBase'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_front_view_1.png";
                                $scope.actStyleJKArr['CollarBaseSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_front_view_2.png";
                                styleImg = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_view_1.png";
                                $scope.actStyleJKArr['CollarStyleSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_view_2.png";
                                $scope.actStyleJKArr['CollarStyleFold'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_view_4.png";
                            }


                        } else if (parent == "Sleeves") {//1_sleevesstyle_1_size__sleevestype__3_view_3
                            $scope.actStyleJKArr['Sleeves'] = styleImg = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + fabricId + "_sleevesstyle" + "_1_size__sleevestype__" + styleId + "_view_1.png";
                            $scope.actStyleJKArr['SleevesSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + fabricId + "_sleevesstyle" + "_1_size__sleevestype__" + styleId + "_view_2.png";
                            $scope.actStyleJKArr['SleevesBack'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + fabricId + "_sleevesstyle" + "_1_size__sleevestype__" + styleId + "_view_3.png";
                            console.log("sleeves styleId " + styleId)
//                         angular.element("#mainImageHolder").find('.shirt_part[rel=SleevesSide]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + fabricId + "_sleevesstyle" + "_1_size__sleevestype__" + styleId + "_view_2.png");
//                          angular.element("#mainImageHolder").find('.shirt_part[rel=SleevesBack]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + fabricId + "_sleevesstyle" + "_1_size__sleevestype__" + styleId + "_view_3.png");
                            if ($scope.sleeveType == "short") {
                                $scope.sleeveType = "short";
                                angular.element("#mainImageHolder").find("[rel=Cuffs]").hide();
                                angular.element("#mainImageHolder").find("[rel=CuffsSide]").hide();
                                angular.element("#mainImageHolder").find("[rel=CuffsBack]").hide();
                                angular.element("#mainImageHolder").find("[rel=ButtonCuffBack]").hide();
                                angular.element("#mainImageHolder").find("[rel=ButtonThreadCuffBack]").hide();
                                angular.element("#mainImageHolder").find("[rel=ButtonCuff]").hide();
                                angular.element("#mainImageHolder").find("[rel=ButtonThreadCuff]").hide();
                                angular.element("#mainImageHolder").find("[rel=backelbow]").hide();

                                angular.element("#div1").find("ul").children().eq(1).css({"display": "none"});
                                angular.element("#div5").find("ul").children().eq(2).css({"display": "none"});

                                angular.element("#mainImageHolder").find("[rel=SleevePlacketLeft]").hide();
                                angular.element("#mainImageHolder").find("[rel=SleevePlacketRight]").hide();

                                angular.element("#mainImageHolder").find("[rel=SleevePlacketButtonLeft]").hide();
                                angular.element("#mainImageHolder").find("[rel=SleevePlacketButtonRight]").hide();

                                angular.element("#mainImageHolder").find("[rel=SleevePlacketThreadLeft]").hide();
                                angular.element("#mainImageHolder").find("[rel=SleevePlacketThreadRight]").hide();

                                $scope.actStyleJKArr['CuffsFold'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";
                                $scope.actStyleJKArr['CuffsFoldInner'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";
                                $scope.actStyleJKArr['SleevesFold'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_sleevesstyle_1_size__sleevestype__" + styleId + "_view_4.png";
                                angular.element("#mainImageHolder").find("[rel=ButtonCuff]").hide();
                                angular.element("#mainImageHolder").find("[rel=ButtonHoleCuffFold]").hide();
                                angular.element("#mainImageHolder").find("[rel=ButtonThreadCuffBack]").hide();
//                $scope.actStyleJKArr['Sleeves'] = styleImg = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_sleevesstyle_1_size__sleevestype__" + styleId + "_view_1.png";
//                $scope.actStyleJKArr['SleevesSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_sleevesstyle_1_size__sleevestype__" + styleId + "_view_2.png";
//                $scope.actStyleJKArr['SleevesBack'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_sleevesstyle_1_size__sleevestype__" + styleId + "_view_3.png";

                                jQuery("#divaccent_4 ul li#2").hide();
                                jQuery("#divaccent_4 ul li#3").hide();
                            } else if ($scope.sleeveType == "long") {
                                $scope.actStyleJKArr['SleevesFold'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";
                                $scope.actStyleJKArr['CuffsFold'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + fabricId + "_cuffstyle_1_size__cufftype__" + styleId + "_view_4.png";
                                //2_cuffstyle_1_size__inner__1_inner_view_4
                                $scope.actStyleJKArr['CuffsFoldInner'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + fabricId + "_cuffstyle_1_size__inner__" + styleId + "_inner_view_4.png";
                            }

                        } else if (parent == "Cuffs") {//fab
                            angular.element("#mainImageHolder").find('.shirt_part[rel="CuffsOutBack"]').hide();
                            //alert(styleId+"styleId")
                            //for Buttons
                            if (styleId == "5" || styleId == "8" || styleId == "10") {
                                //2ButtonCuff ButtonThreadCuff ButtonHoleCuffFold
                                //alert("hi")
                                angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonCuff"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/Static/Folded_CuffDouble_b1.png");
                                angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonThreadCuff"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/Static/Folded_CuffDoublethread.png");
                                angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonCuffBack"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/Static/Back_CuffDouble_b1.png");
                                angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonThreadCuffBack"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/Static/Back_CuffDoublethread.png");
                                angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonHoleCuffFold"]').show();
                                angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonThreadCuffBack"]').show();
                                //angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonHoleFold"]').show();
                                //angular.element("#mainImageHolder").find('.shirt_part[rel="ThreadsFold"]').show();
                                $scope.actStyleJKArr['ButtonThreadCuffBack'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + $scope.threadcolorId + "_double_buttonhole_view_3.png";
                                $scope.actStyleJKArr['ButtonThreadCuff'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + $scope.threadcolorId + "_double_thread_view_4.png";
                                $scope.actStyleJKArr['ButtonHoleCuffFold'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + $scope.threadcolorId + "_double_buttonhole_view_4.png";
                                $scope.actStyleJKArr['ButtonCuffBack'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/Static/Back_CuffDouble_b1.png";
                                jQuery("#divaccent_4 ul li#2").show();
                                jQuery("#divaccent_4 ul li#3").show();

                            } else if (styleId == "1" || styleId == "2" || styleId == "6" || styleId == "9" || styleId == "12") {
                                //French 
                                angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonCuff"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/Static/Folded_Common_French_Cufflink.png");
                                angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonThreadCuff"]').attr("src", basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png");
                                angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonCuffBack"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/Static/Back_Common_French_Cufflink.png");
                                angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonThreadCuffBack"]').attr("src", basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png");
                                angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonHoleCuffFold"]').hide();
                                angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonThreadCuffBack"]').hide();
                                jQuery("#mainImageHolder").find(".shirt_part[rel=ButtonThreadCuff]").hide();
                                $scope.actStyleJKArr['ButtonCuffBack'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/Static/Back_Common_French_Cufflink.png";
                                jQuery("#divaccent_4 ul li#2").hide();
                                jQuery("#divaccent_4 ul li#3").hide();
                            } else {
                                //BottonSingle 4 3 7 11

                                angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonCuff"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/Static/Folded_CuffSingle_b1.png");
                                angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonThreadCuff"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/Static/Folded_CuffSinglethread.png");
                                angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonCuffBack"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/Static/Back_CuffSingle_b1.png");
                                angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonThreadCuffBack"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/Static/Back_CuffSinglethread.png");
                                angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonHoleCuffFold"]').show();
                                angular.element("#mainImageHolder").find('.shirt_part[rel="ButtonThreadCuffBack"]').show();

                                $scope.actStyleJKArr['ButtonThreadCuffBack'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + $scope.threadcolorId + "_single_buttonhole_view_3.png";
                                $scope.actStyleJKArr['ButtonThreadCuff'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + $scope.threadcolorId + "_single_thread_view_4.png";
                                $scope.actStyleJKArr['ButtonHoleCuffFold'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + $scope.threadcolorId + "_single_buttonhole_view_4.png";
                                $scope.actStyleJKArr['ButtonCuffBack'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/Static/Back_CuffSingle_b1.png";

                                jQuery("#divaccent_4 ul li#2").show();
                                jQuery("#divaccent_4 ul li#3").show();
                            }
                            //buttons End here
                            $scope.actStyleJKArr['CuffsFold'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_cuffstyle_1_size__cufftype__" + styleId + "_view_4.png";
                            $scope.actStyleJKArr['CuffsFoldInner'] = basePath + "pub/media/shirt-tool/shirt_images/cuffs/" + $scope.fabricId + "_cuffstyle_1_size__inner__" + styleId + "_inner_view_4.png";
                            $scope.actStyleJKArr['CuffsBack'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_cuffstyle_1_size__cufftype__" + styleId + "_view_3.png";
                            //angular.element("#mainImageHolder").find('.shirt_part[rel="Cuffs"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_cuffstyle_1_size__cufftype__" + styleId + "_view_1.png");
                            $scope.actStyleJKArr['CuffsSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_cuffstyle_1_size__cufftype__" + styleId + "_view_2.png";
                            $scope.actStyleJKArr['Cuffs'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_cuffstyle_1_size__cufftype__" + styleId + "_view_1.png";
                            styleImg = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_cuffstyle_1_size__cufftype__" + styleId + "_view_1.png";
                            $scope.actStyleJKArr['SleevePlacketLeft'] = basePath + "pub/media/shirt-tool/shirt_images/sleeveplacket/" + $scope.fabricId + "_sleeve_1_left_view_3.png";
                            $scope.actStyleJKArr['SleevePlacketRight'] = basePath + "pub/media/shirt-tool/shirt_images/sleeveplacket/" + $scope.fabricId + "_sleeve_1_right_view_3.png";
                            //console.log("Style img on fab click ="+styleImg)
                        } else if (parent == "Fit") {

                            styleImg = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_fitstyle" + "__size__fittype__1_view_1.png";
                            $scope.actStyleJKArr['FitSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_fitstyle" + "__size__fittype__1_view_2.png";
                            $scope.actStyleJKArr['FitBack'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_fitstyle" + "__size__fittype__1_view_3.png";
                            $scope.actStyleJKArr['FitFold'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_fitstyle" + "__size__fittype__1_view_4.png";
                            $scope.actStyleJKArr['FitZoom'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_fitstyle" + "__size__fittype__1_view_5.png";
                        } else if (parent == "Pocket") {
                            if (styleId == "1") {

                                styleImg = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";
                                $scope.actStyleJKArr['PocketSide'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";
                                $scope.actStyleJKArr['PocketFold'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";
                            } else {

                                styleImg = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_pockets_" + styleId + "_view_1.png";
                                $scope.actStyleJKArr['PocketSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_pockets_" + styleId + "_view_2.png";
                                $scope.actStyleJKArr['PocketFold'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_pockets_" + styleId + "_view_4.png";
                            }
                        } else if (parent == "Plackets") {
                            $scope.isContrast = false;
                            if (styleId == "2") {//hide button

                                angular.element("#mainImageHolder").find("[rel=Button]").hide();
                                angular.element("#mainImageHolder").find("[rel=BottonSide]").hide();
                                angular.element("#mainImageHolder").find("[rel=BottonFold]").hide();
                                angular.element("#mainImageHolder").find("[rel=PlacketsFold]").hide();
                                angular.element("#mainImageHolder").find("[rel=PlacketsSide]").hide();

                                angular.element("#mainImageHolder").find("[rel=casualPlacket]").hide();
                                angular.element("#mainImageHolder").find("[rel=casualPlacketSide]").hide();
                                angular.element("#viewport #view5").find("[rel=PlacketsZoom]").hide();
                                // alert("hi")
                            } else if (styleId == "3") {
                                angular.element("#mainImageHolder").find("[rel=Button]").show();
                                angular.element("#mainImageHolder").find("[rel=BottonSide]").show();
                                angular.element("#mainImageHolder").find("[rel=BottonFold]").show();
                                angular.element("#mainImageHolder").find("[rel=Plackets]").hide();
                                angular.element("#mainImageHolder").find("[rel=PlacketsSide]").hide();
                                angular.element("#mainImageHolder").find("[rel=PlacketsFold]").hide();
                                angular.element("#viewport #view5").find("[rel=PlacketsZoom]").hide();
                            } else
                            {
                                angular.element("#mainImageHolder").find("[rel=Button]").show();
                                angular.element("#mainImageHolder").find("[rel=BottonSide]").show();
                                angular.element("#mainImageHolder").find("[rel=BottonFold]").show();
                                angular.element("#mainImageHolder").find("[rel=Plackets]").show();
                                angular.element("#mainImageHolder").find("[rel=PlacketsSide]").show();
                                angular.element("#mainImageHolder").find("[rel=PlacketsFold]").show();

                                styleImg = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_plackets_" + styleId + "_view_1.png";
                                $scope.actStyleJKArr['Plackets'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_plackets_" + styleId + "_view_1.png";
                                $scope.actStyleJKArr['PlacketsSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_plackets_" + styleId + "_view_2.png";
                                $scope.actStyleJKArr['PlacketsFold'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_plackets_" + styleId + "_view_4.png";
                                $scope.actStyleJKArr['PlacketsZoom'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_plackets_" + styleId + "_view_5.png";
                                angular.element("#mainImageHolder").find('.shirt_part[rel="PlacketsFold"]').attr("src", basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_plackets_" + styleId + "_view_4.png");
                                angular.element("#viewport #view5").find("[rel=PlacketsZoom]").show();
                                //alert("78")
                            }
                            $scope.actStyleJKArr['casualInnerPlackets'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";//basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/casual/" + $scope.fabricId + "_style_" + styleId + "_casual_inner_view_1.png";
                            $scope.actStyleJKArr['casualPlacket'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";//basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/casual/" + $scope.fabricId + "_style_" + styleId + "_casual_inner_view_2.png";
                            $scope.actStyleJKArr['casualInnerPlacketsSide'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";
                            $scope.actStyleJKArr['casualPlacketSide'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";
                        } else if (parent == "Chestpleats") {

                            //console.log("IsShirt= = " + $scope.IsShirt)//false
                            if ($scope.IsShirt) {

                                if (styleId == "1") {

                                    $scope.actStyleJKArr['Tuxido'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";
                                    $scope.actStyleJKArr['TuxidoSide'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";
                                    $scope.actStyleJKArr['TuxidoFold'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";
                                    $scope.actStyleJKArr['TuxidoZoom'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";

                                    $scope.actStyleJKArr['casualTuxido'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";
                                    $scope.actStyleJKArr['casualTuxidoSide'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";

                                } else {

                                    $scope.actStyleJKArr['Tuxido'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + "Shirt_Tuxedo_Front_Glow_1.png";
                                    $scope.actStyleJKArr['TuxidoSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + "Shirt_Tuxedo_Side_Glow_2.png";
                                    $scope.actStyleJKArr['TuxidoFold'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + "Shirt_Tuxedo_Folded_Glow_4.png";
                                    $scope.actStyleJKArr['TuxidoZoom'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + "Tuxedo_Zoom_View.png";

                                    $scope.actStyleJKArr['casualTuxido'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + "Casual_Front_Tuxedo_Glow.png";
                                    $scope.actStyleJKArr['casualTuxidoSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + "Casual_Side_Tuxedo_Glow.png";
                                }
                            } else
                            {
                                if (styleId == "1") {

                                    $scope.actStyleJKArr['Tuxido'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";
                                    $scope.actStyleJKArr['TuxidoSide'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";
                                    $scope.actStyleJKArr['TuxidoFold'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";
                                    $scope.actStyleJKArr['TuxidoZoom'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";

                                    $scope.actStyleJKArr['casualTuxido'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";
                                    $scope.actStyleJKArr['casualTuxidoSide'] = basePath + "pub/media/shirt-tool/default_shirttool_image/blank.png";

                                } else {


                                    $scope.actStyleJKArr['Tuxido'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + "Shirt_Tuxedo_Front_Glow_1.png";
                                    $scope.actStyleJKArr['TuxidoSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + "Shirt_Tuxedo_Side_Glow_2.png";
                                    $scope.actStyleJKArr['TuxidoFold'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + "Shirt_Tuxedo_Folded_Glow_4.png";
                                    $scope.actStyleJKArr['TuxidoZoom'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + "Tuxedo_Zoom_View.png";

                                    $scope.actStyleJKArr['casualTuxido'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + "Casual_Front_Tuxedo_Glow.png";
                                    $scope.actStyleJKArr['casualTuxidoSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + "Casual_Side_Tuxedo_Glow.png";
                                }
                            }
                        }
                        var designStyleId = suitDesignData[design].id;
//                    console.log("suitDesignData[design].id="+suitDesignData[design].id)
                        if (styleType == "shirt")
                        {


                            var hClass = jQuery("#divs_" + designStyleId + " li").hasClass("activeDesign");
                            if (!hClass)
                            {
                                jQuery("#divs_" + designStyleId + " li:first").addClass("activeDesign");
                            }


                            $scope.actStyleJKArr[parent] = styleImg;
                            $scope.actStyleJKArr[parent + '_styleName'] = styleName;
                            $scope.actStyleJKArr[parent + '_id'] = styleId;
                            $scope.actStyleJKArr[parent + '_btnCount'] = $scope.allDesignStyles.btn_count;
                            $scope.actStyleJKArr[parent + '_label'] = labelName;
                        }

                    }
                }
            }
            var chk = jQuery("#data").val();
            if (chk === "" || chk === undefined)
            {  // console.log("Hide monogram")
                $scope.monogramText = "";
                jQuery("#mainImageHolder").find("[rel=Monogram]").hide();
            } else {
                // console.log("show monogram")
                jQuery("#mainImageHolder").find("[rel=Monogram]").show();
            }
            setTimeout(function () {
                IsShirt = false;
                jQuery("#casual_1").trigger("click");
            });

            var cnt = 0;
            jQuery("#div1 .box_option ul .activeDesign").each(function () {
                cnt++;
                stylePriceArr[cnt] = jQuery(this).attr("price");

                // console.log("stylePriceArr[" + cnt + "]" + stylePriceArr[cnt])

            })
            updateProduct();
            imageLoaded();
        }

        function applyFabricOnSuitInit()
        {
            var first = tempJk;
            var forth = tempAccnt;

            var data;
            var data1;
            var suitDesignData;
            /*if (first && second && third && forth)
             {*/


            suitDesignData = jQuery.merge(jQuery.merge([], first), forth);
            var parent, styleImg, styleType, styleName, styleId, labelName;
            $scope.allDesignStyles = "";
            var styleCnt = "0";
            var a = jQuery("#div1 .box_options>div");
            for (var design in suitDesignData)
            {

                if (suitDesignData.hasOwnProperty(design))
                {
                    $scope.allDesignStyles = suitDesignData[design]["style"];

                    if ($scope.allDesignStyles) {
                        if (!$scope.allDesignStyles.parent)
                        {
                            continue;
                        }


                        if ($scope.allDesignStyles.parent) {
                            parent = $scope.allDesignStyles.parent.replace(/\s+|&/g, "");
                            styleName = $scope.allDesignStyles.name.replace(/\s+|&/g, "");
                            labelName = $scope.allDesignStyles.parent;
                            styleId = $scope.allDesignStyles.id;
                            //get style type single or double
                            styleType = $scope.allDesignStyles.designType;

                            if ($scope.allDesignStyles.price) {
                                stylePriceArr[parent] = $scope.allDesignStyles.price;
                            } else {
                                stylePriceArr[parent] = "0.0";
                            }



                            styleId = $scope.actStyleData[parent];
                        }

                        var designStyleId = suitDesignData[design].id;

                        if (styleType == "shirt")
                        {
                            var hClass = jQuery("#divs_" + designStyleId + " li").hasClass("activeDesign");
                            if (!hClass)
                            {

                                jQuery("#divs_" + designStyleId + " li:first").addClass("activeDesign");
                            }
                        }
                    }
//          
                }
            }



            angular.element("#mainImageHolder").find("[rel=CasualLook]").hide();
            angular.element("#mainImageHolder").find("[rel=CasualLookBack]").hide();
            //disable casual collar
            angular.element("#view1").find('.shirt_part[rel=casualMan],.shirt_part[rel=casualWomanBackCollar],.shirt_part[rel=casualBase],.shirt_part[rel=casualCollar],.shirt_part[rel=WomanTie]').hide();
            angular.element("#view2").find('.shirt_part[rel=casualWomanSide],.shirt_part[rel=casualBaseSide],.shirt_part[rel=casualCollarSide],.shirt_part[rel=ManTieSide],.shirt_part[rel=WomanBackPartSide]').hide();

            angular.element("#view1").find('.shirt_part[rel=casualMan],.shirt_part[rel=casualBase],.shirt_part[rel=casualCollar],.shirt_part[rel=ManTie]').hide();
            angular.element("#view2").find('.shirt_part[rel=casualManSide],.shirt_part[rel=casualBaseSide],.shirt_part[rel=casualCollarSide],.shirt_part[rel=ManTieSide]').hide();
            jQuery("#displayLargeImg").html('<img id="slide" src="' + basePath + 'pub/media/shirt-tool/fabric_images/01.png">');
            console.log("in fabric loaded ");
            //angular.element("#view1").find('.shirt_part[rel=ViewStatic]').hide();
            updateInItZoom();

            angular.element("#div4").find(".fabric-bx").children().eq(0).children().addClass("activeDesign");
            angular.element("#theInitialProgress").hide();

            angular.element("html, body").animate({scrollTop: jQuery('.toolheader').offset().top}, 1000);

            angular.element(".suit-preloader").show();
            angular.element(".mainToolarea").css({"opacity": "0"});

            //apply default/first fabric from the list
            setTimeout(function () {

                angular.element("#div4 fabricview:first").trigger("click");
                jQuery("#div5 .slde-in ul > li:last").remove();

            });


        }

//change the contrast of collar
        $scope.changeCollarInnerOuterContrast = function ($event) {


            jQuery($event.currentTarget).parent().find('.activeDesign').removeClass('activeDesign');
            jQuery($event.currentTarget).addClass("activeDesign");
            if (angular.element($event.currentTarget).find('.identity').text() !== "BY DEFAULT") {
                angular.element('#div4_fabric_accent').show();
                $scope.contrastCollarStyle = jQuery($event.currentTarget).find('.identity').text();
            } else {
                var styleId = angular.element(".shirt_part[rel='CollarStyle']").attr("id");
                var parent = "CollarStyle";

                if (styleId) {




                    $scope.actStyleJKArr['CollarStyle'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_view_1.png";
                    $scope.actStyleJKArr['casualCollar'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/casual/" + $scope.fabricId + "_style_" + styleId + "_casual_collar_view_1.png";
                    $scope.actStyleJKArr['casualCollarSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/casual/" + $scope.fabricId + "_style_" + styleId + "_casual_collar_view_2.png";
                    $scope.actStyleJKArr['CollarStyleSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_view_2.png";
                    $scope.actStyleJKArr['CollarStyleBack'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_view_3.png";
                    $scope.actStyleJKArr['CollarStyleFold'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_view_4.png";
                    $scope.actStyleJKArr['innerCollarGlow'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_back_view_4.png";
                    $scope.actStyleJKArr['CollarStyleZoom'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_view_5.png";
                    $scope.actStyleJKArr['CollarStyleZoominner'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_back_view_5.png";
                    updateProduct();
                }



            }
        };
        /*change the contrast of collar*/
        $scope.changeCuffInnerOuterContrast = function ($event) {

            jQuery($event.currentTarget).parent().find('.activeDesign').removeClass('activeDesign');
            jQuery($event.currentTarget).addClass("activeDesign");
            if (angular.element($event.currentTarget).find('.identity').text() !== "BY DEFAULT") {

                angular.element("#div9").hide();
                angular.element('#divaccent_3').show();
                $scope.contrastCuffStyle = jQuery($event.currentTarget).find('.identity').text();
            } else {
                angular.element("#div9").show();
                angular.element('#divaccent_3').hide();
                var styleId = angular.element(".shirt_part[rel='Cuffs']").attr("id");
                var parent = "Cuffs";
                return false;
                if (styleId) {

                    alert(styleId)


                    $scope.actStyleJKArr['CollarStyle'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_view_1.png";
                    $scope.actStyleJKArr['casualCollar'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/casual/" + $scope.fabricId + "_style_" + styleId + "_casual_collar_view_1.png";
                    $scope.actStyleJKArr['casualCollarSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/casual/" + $scope.fabricId + "_style_" + styleId + "_casual_collar_view_2.png";
                    $scope.actStyleJKArr['CollarStyleSide'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_view_2.png";
                    $scope.actStyleJKArr['CollarStyleBack'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_view_3.png";
                    $scope.actStyleJKArr['CollarStyleFold'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_view_4.png";
                    $scope.actStyleJKArr['innerCollarGlow'] = basePath + "pub/media/shirt-tool/shirt_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + styleId + "_back_view_4.png";
                    updateProduct();
                }



            }


            /*//alert("ffff")
             //        angular.element('#div4_cuff_accent').show();
             //        $scope.contrastCuffStyle = jQuery($event.currentTarget).find('.identity').text();
             
             //        angular.element('#divaccent_3').css{{"display":block}}
             if (jQuery('#div9').find('li.active').find('.identity').text() !== "BY DEFAULT") {
             jQuery('#div5').find('#divaccent_3').show();
             
             jQuery("#div9").css({"display": "none"});
             
             }*/

        };
        /*change contract collar fabric in accent*/
        $scope.changeContrastFabric = function ($event, obj) {

            angular.element("#div4_fabric_accent").find(".activeDesign").removeClass('activeDesign');
            angular.element($event.currentTarget).find(".fabvie").addClass("activeDesign");

            $scope.collarFabricId = obj.id;
            $scope.cuffFabricId = obj.id;
            //update fabric preview image
            var fabricVal;
            fabricVal = basePath + "pub/media/shirt-tool/fab_shirt_images/fabric_" + $scope.fabricId + "_show.png"; //angular.element("#div4 [id=" + $scope.fabricId + "] .start-view img").attr("src");


            jQuery('#slide').attr('src', fabricVal);
            //for responsive

            if (jQuery(window).width() <= 767) {
                jQuery("body").removeClass("LeftpanemOpen")
                jQuery(".leftpanelsection").css("left", "0");
            }

            applyFabricOnCollar();
        }

        /*change contract thread fabric in accent*/
        $scope.changeContrastThreadFabric = function ($event, obj) {

            angular.element("#div4_thread_fabric_accent").find(".activeDesign").removeClass('activeDesign');
            angular.element($event.currentTarget).find(".fabvie").addClass("activeDesign");
            $scope.threadFabricId = "2";//obj.id;
//        $scope.cuffFabricId = obj.id;

            //update fabric preview image
            var fabricVal;
            fabricVal = basePath + "pub/media/shirt-tool/fab_shirt_images/fabric_" + $scope.fabricId + "_show.png"; //angular.element("#div4 [id=" + $scope.fabricId + "] .start-view img").attr("src");


            jQuery('#slide').attr('src', fabricVal);
            //for responsive

            if (jQuery(window).width() <= 767) {
                jQuery("body").removeClass("LeftpanemOpen")
                jQuery(".leftpanelsection").css("left", "0");
            }

            updateProduct();
            // applyFabricOnThread();
        }


        /*applying fabric on collar*/





        function applyFabricOnThread() {

            var first = tempJk;
            var second = tempPt = "";
            var third = tempVst = "";
            var forth = tempAccnt = "";
            var data = jQuery.merge(jQuery.merge([], first), second);
            var data1 = jQuery.merge(jQuery.merge([], data), third);
            var suitDesignData = jQuery.merge(jQuery.merge([], data1), forth);
            var parent, styleImg, styleType, styleName, styleId, labelName;
            $scope.allDesignStyles = "";
            $scope.threadFabricId = "1";
            var styleCnt = "0";



            angular.element("#viewport").find('.shirt_part[rel=BottondownZoom]').show();
            if ($scope.collarId == "1") {

                $scope.actStyleJKArr['BottonZoomHole'] = basePath + "pub/media/shirt-tool/buttonfabric_images/1_torso_buttonholes_view_5.png";
                $scope.actStyleJKArr['BottonZoomThread'] = basePath + "pub/media/shirt-tool/buttonfabric_images/1_torso_buttonthread_view_5.png";
                //$scope.actStyleJKArr['ZoomBottondown'] = basePath + "pub/media/shirt-tool/shirt_images/collarstyle/Static/ButtondownCollar_Button_Zoom.png";//buttonfabric_images/" + $scope.threadFabricId + "_final_button_hole_view_5.png";
            } else {
                $scope.actStyleJKArr['BottonZoomHole'] = basePath + "pub/media/shirt-tool/buttonfabric_images/1_torso_buttonholes_view_5.png";
                $scope.actStyleJKArr['BottonZoomThread'] = basePath + "pub/media/shirt-tool/buttonfabric_images/1_torso_buttonthread_view_5.png";
                //$scope.actStyleJKArr['ZoomBottondown'] = basePath + "pub/media/shirt-tool/blank.png";
            }

            updateProduct();
        }



    });
    function CurrentActiveClass(obj) {

        return false;
        var currentAttr;
        jQuery("#toolHeader").find(".active").removeClass("active");
        jQuery(obj).addClass("active");
        currentAttr = jQuery(obj).attr("id");
        if (currentAttr == "stylSec")
        {
            jQuery("#stylePartOfTool").css({"display": "block"});
            jQuery("#fabricPartOfTool").css({"display": "none"});
            jQuery("#accentPartOfTool").css({"display": "blobk"});
        } else if (currentAttr == "febSec")
        {

            //trigger to active fab if not trig first
            if (fabricId != "0")
            {
                //check active if not act then only trigger
                var fabId = angular.element("#fabricInnerPart fabricview").find(".activeDesign").parent().attr("id");
                if (!fabId) {
                    angular.element("#fabricInnerPart fabricview[id=" + fabricId + "]").trigger("click");
                }

            } else {
                angular.element("#fabricInnerPart fabricview:first").trigger("click");
            }
            //end

            jQuery("#fabricPartOfTool").css({"display": "block"});
            jQuery("#stylePartOfTool").css({"display": "none"});
            jQuery("#accentPartOfTool").css({"display": "blobk"});
        } else if (currentAttr == "accSec")
        {
            jQuery("#accentPartOfTool").css({"display": "blobk"});
            jQuery("#stylePartOfTool").css({"display": "none"});
            jQuery("#fabricPartOfTool").css({"display": "none"});
            //
        }

    }
    function closeButton() {

        jQuery(".container-fluid").css({"opacity": "1"});
        jQuery(".mainToolarea").css({"height": "0px"});
        jQuery(".mainToolarea").css({"width": "0px"});
        var chk = jQuery("#data").val();
        if (chk === "" || chk === undefined)
        {  // console.log("Hide monogram")
            $scope.monogramText = "";
            jQuery("#mainImageHolder").find("[rel=Monogram]").hide();
        } else {
            // console.log("show monogram")
            jQuery("#mainImageHolder").find("[rel=Monogram]").show();
        }
        // jQuery(".modal-content .modal-body").css({"margin-left": "0px"});
        //angular.element("#mainImageHolder .shirt_part[rel=MonogramFold]").show();
    }
// Get the modal
    var modal = document.getElementById('zoomOfTool');

// When the user clicks anywhere outside of the modal, close it
    window.onclick = function (event) {

        if (event.target == modal) {
            closeButton();

        }
    }
    function zoomButtonHide() {

        jQuery("#zoomButton").css({"display": "none"});
    }
    function zoomButtonShow() {

        jQuery("#zoomButton").css({"display": "block"});
        jQuery("#zoomButton").css({"margin-left": "10px"});

    }

    function imageLoaded() {


        console.log("currStyleName " + currStyleName);

        if (currStyleName == "fabric" || currStyleName == "Threads")
        {

            angular.element('.shirt_part[rel="Fit"]').load(function () {

                angular.element(".mainToolarea").fadeIn("slow", function (e) {
                    setTimeout(function () {
                        angular.element(".suit-preloader").hide();
                        angular.element(".mainToolarea").css({"opacity": "1"});
                        angular.element("#theInitialProgress").hide();
                        jQuery(".container-fluid").css({"opacity": "1"});
                        currStyleName = "";
                    }, 500);
                });
            });

        } else if (currStyleName != "")
        {
            if (currStyleName == "Fit") {
                currStyleName = "FitWaisted";
            }

            angular.element('.shirt_part[rel="' + currStyleName + '"]').load(function () {

                angular.element(".mainToolarea").fadeIn("slow", function (e) {
                    setTimeout(function () {
                        angular.element(".suit-preloader").hide();
                        angular.element(".mainToolarea").css({"opacity": "1"});
                        angular.element("#theInitialProgress").hide();
                        jQuery(".container-fluid").css({"opacity": "1"});
                        currStyleName = "";
                    }, 100);
                });
            });
        } else {

            setTimeout(function () {
                angular.element(".suit-preloader").hide();
                angular.element(".mainToolarea").css({"opacity": "1"});
                angular.element("#theInitialProgress").hide();
                jQuery(".container-fluid").css({"opacity": "1"});
                currStyleName = "";
            }, 500);
        }



        if (jQuery("#divs_2 li.activeDesign").attr("id") == "7") {
            jQuery("#mainImageHolder").find("[rel=Lapel]").hide();
        }


    }

    function imageLoadedTemp() {
        return false;
        angular.element('.shirt_part[rel=Pockets]').load(function () {
            angular.element(".mainToolarea").fadeIn("slow", function (e) {

                setTimeout(function () {
                    jQuery(".preloader").hide();
                    angular.element(".suit-preloader").hide();
                    jQuery(".container-fluid").css({"opacity": "1"});
                }, 500);
            });
        });
        setTimeout(function () {
            angular.element(".suit-preloader").hide();
        }, 500);
        if (jQuery("#divs_2 li.activeDesign").attr("id") == "7") {
            jQuery("#mainImageHolder").find("[rel=Lapel]").hide();
        }

    }

    /* call after DOM loaded */
    angular.element(window).load(function () {
        return false;
        //trigger to first design ele
        angular.element("#theMainInitialProgress").trigger("click");
        angular.element("#theInitialProgress").hide();
        angular.element(".container-fluid").css({"opacity": "1"});
    });
    function updateInItZoom()
    {
        return false;
        if (jQuery(window).width() > 767) {

            jQuery('.zoomContainer').remove();
            jQuery('.fabvie img').removeData('elevateZoom');
            inZoomEvalate();
        }
    }

    function inZoomEvalate()
    {
        jQuery('.fabvie img').data('zoom-image', jQuery('.fabvie img').attr("data-image")).elevateZoom({
            responsive: true
        });
    }

    function nullfabricTxt() {



        if (jQuery(".fabricGenerater").children().length) {
            jQuery("#nullTxt").hide();
        } else {
            jQuery("#nullTxt").show();
        }

    }
});