define(["angular", "jquery", "angular_route", "angular_animate"], function (angular, jQuery) {
    var currSymbol = jQuery("#currSymbol").val();
    var basePath = jQuery("#basePath").val();
//category element
    angular.module("Shirt").directive("imageofpant", function () {
        var dirObj = {};
        dirObj.restrict = "E";
        dirObj.template = '<div class="panel-heading" role="tab" id="headingThree_{{pantStyles.id}}"> <h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#pants_{{pantStyles.id}}" aria-expanded="false" aria-controls="collapseTwo"><span>{{pantStyles.name}}</span> <div class="style-view"><img src="{{pantStyles.img}}"></div></a> </h4> </div> <div id="pants_{{pantStyles.id}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo_{{pantStyles.id}}"> <div class="panel-body"><div class="tabs-navs-sub"> <ul> <li class="optiontrigger" ng-repeat=" style in pantStyles.style" index="{{$index}}" name="style" ng-disabled="" ng-click="designClick($event, style, $index);"><a href="javascript:void(0)"> <img class="innerImg" ng-src="{{style.img}}" err-src="' + basePath + 'pub/media/shirt-tool/default_tool_img/blank.png" alt="toolimg"> <span>{{style.name}} </span></a></li></ul></div> </div> </div>';
        /*dirObj.scope = {
         listcat: "=name"
         }*/
        return dirObj;
    });
//vest view element
    angular.module("Shirt").directive("imagesofvest", function () {
        var dirObj = {};
        dirObj.restrict = "E";
        dirObj.template = '<div class="panel-heading" role="tab" id="headingThree_{{vestStyles.id}}"> <h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#catThree_{{vestStyles.id}}" aria-expanded="false" aria-controls="collapseTwo"><span>{{vestStyles.name}}</span> <div class="style-view"><img src="{{vestStyles.img}}"></div></a> </h4> </div> <div id="catThree_{{vestStyles.id}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree_{{vestStyles.id}}"> <div class="panel-body"><div class="tabs-navs-sub"> <ul> <li class="optiontrigger" ng-repeat=" style in vestStyles.style" index="{{$index}}" name="style" ng-disabled="" ng-click="designClick($event, style, $index);"><a href="javascript:void(0)"> <img class="innerImg" ng-src="{{style.img}}" err-src="' + basePath + 'pub/media/shirt-tool/default_tool_img/blank.png" alt="toolimg"> <span>{{style.name}} </span></a></li></ul></div> </div> </div>';
        return dirObj;
    });
//suit view element

    angular.module("Shirt").directive("fabricview", function () {
        var dirObj = {};
        dirObj.restrict = "E";
        dirObj.template = "<div class='fabvie' ><div class='start-view'><img data-zoom-image='{{fabric.real_img}}' ng-src='{{fabric.fabric_thumb}}' err-src='" + basePath + "pub/media/shirt-tool/default_tool_img/blank.png' alt='' width='100'></div><div class='info fabric-detail clearfix'><div class='faric-name ng-binding'>{{fabric.name}}</div> <div class='fab-type'>{{fabric.type}}</div>   <div class='faric-price ng-binding'><span class='currency'>" + currSymbol + "<span><span class='price'>{{fabric.price}}</span></div></div></div>";
        return dirObj;
    });
    angular.module("Shirt").directive("fabricviewaccent", function () {
        var dirObj = {};
//    dirObj.restrict = "E";
        dirObj.template = "<div class='fabvie' ><div class='start-view'><img data-zoom-image='{{fabric.real_img}}' ng-src='{{fabric.fabric_thumb}}' err-src='" + basePath + "pub/media/shirt-tool/default_tool_img/blank.png' alt='' width='100'></div><div class='info fabric-detail clearfix'><div class='faric-name ng-binding'>{{fabric.name}}</div> <div class='fab-type'>{{fabric.type}}</div> </div></div>";
        return dirObj;
    });
    angular.module("Shirt").directive("threadcolor", function () {
        var dirObj = {};
//    dirObj.restrict = "E";
        dirObj.template = "<div class='fabvie' ><div class='start-view'><img data-zoom-image='{{fabric.img}}' ng-src='{{fabric.img}}' err-src='" + basePath + "pub/media/shirt-tool/default_tool_img/blank.png' alt='' width='100'></div><div class='info fabric-detail clearfix'><div class='faric-name ng-binding'>{{fabric.name}}</div> <div class='fab-type'>{{fabric.type}}</div> </div></div>";
        return dirObj;
    });
//fabric view element
    angular.module("Shirt").directive("designview", function () {
        var dirObj = {};
        dirObj.restrict = "E"; //ng-click="ParentClick($event, style, $index)"
        dirObj.template = '<div ng-click="categoryClick($event, style, $index)" class="panel-heading" role="tab" id="headingTwo_{{designStyles.id}}"> <h4 class="panel-title"> <a id="dataHolder" class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#catTwo_{{designStyles.id}}" aria-expanded="false" aria-controls="collapseTwo"><div class="style-view"><img src="{{designStyles.img}}"><span>{{designStyles.name}}</span> </div></a> </h4> </div> <div id="catTwo_{{designStyles.id}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo_{{designStyles.id}}"> <div class="panel-body"><div class="tabs-navs-sub"> <ul> <li ng-repeat=" style in designStyles.style" index="{{$index}}" name="style" ng-disabled="" ng-click="designClick($event, style, $index);"><a href="javascript:void(0)"> <img class="innerImg" ng-src="{{style.img}}" err-src="' + basePath + 'pub/media/shirt-tool/default_tool_img/blank.png" alt="toolimg"> <span>{{style.name}} </span></a></li></ul></div> </div> </div>';
        /*dirObj.scope = {
         fabricview: "=name"
         }*/

        return dirObj;

    });
    angular.module("Shirt").directive('sbLoad', ['$parse', function ($parse) {
            return {
                restrict: 'AE',
                link: function (scope, elem, attrs) {
                    var fn = $parse(attrs.sbLoad);
                    elem.on('load', function (event) {
                        scope.$apply(function () {
                            fn(scope, {$event: event});
                        });
                    });
                }
            };
        }
    ]);
});