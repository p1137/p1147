define(["angular", "angular_route", "angular_animate"], function (angular) {
    angular.module("Shirt").factory("viewService", function ($http) {

        var viewDataSuit = [
            {id: '1', name: 'view1', viewimages: [
//                 {id: '0', rel: 'Man', label: "", type: 'shirt', name: 'Man', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/test_front.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    //{id: '1', rel: 'Man', label: "", type: 'shirt', name: 'Man', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/temp/man_static_front.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    //{id: '21', rel: 'ManShoe', label: "", type: 'shirt', name: 'Man', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/Shoes_Front.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '1', rel: 'WomanBackPart', label: "", type: 'shirt', name: 'Woman', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/shirt_images/collarstyle/back_collar/7_style_1_front_backpart_view_1.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '1', rel: 'WomanBackCollar', label: "", type: 'shirt', name: 'Woman', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/shirt_images/collarstyle/back_collar/7_style_1_front_view_1.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '2', rel: 'Fit', label: "", type: 'shirt', name: 'Fit', classname: 'shirt_part part custom-part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '3', rel: 'FitWaisted', label: "", type: 'shirt', name: 'FitWaisted', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '4', rel: 'Tuxido', label: "", type: 'shirt', name: 'Tuxido', classname: 'shirt_part custom-part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '5', rel: 'Pocket', label: "", type: 'shirt', name: 'No Pocket', classname: 'shirt_part custom-part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '6', rel: 'Plackets', label: "", type: 'shirt', name: 'Standard', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '22', rel: 'Bottom', label: "", id: '1', type: 'shirt', name: 'Bottom', classname: 'shirt_part custom-part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},

                    {id: '7', rel: 'Sleeves', label: "", type: 'shirt', name: 'Long', classname: 'shirt_part part custom-part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},

//                {id: '8', rel: 'ManShoes', label: "", type: 'jacket', name: 'Man Shoe', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '9', rel: 'Cuffs', label: "", type: 'shirt', name: 'Single cuff 1 button ', classname: 'shirt_part part custom-part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '10', rel: 'CasualLook', label: "", id: '1', type: 'shirt', name: 'CasualLook', classname: 'shirt_part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '11', rel: 'Monogram', name: '', textFont: "Bold", classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},

                    {id: '15', rel: 'pantfit', label: "", type: 'shirt', name: 'Regular Fit', classname: 'shirt_part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '16', rel: 'casualMan', label: "", btnCount: '1', type: 'shirt', name: 'casual collar', classname: 'shirt_part', img: basePath + 'pub/media/shirt-tool/shirt_images/collarstyle/Man_Static/Casual-Bust_Man_Static1.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
//                {id: '17', rel: 'casualMan', label: "", btnCount: '1', type: 'shirt', name: 'casual collar', classname: 'shirt_part', img: basePath + 'pub/media/shirt-tool/women_shirt_images/collarstyle/back_collar/1_style_1_casual_left_sidepart_view_1.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '17', rel: 'casualWomanBackCollar', label: "", btnCount: '1', type: 'shirt', name: 'casual collar', classname: 'shirt_part', img: basePath + 'pub/media/shirt-tool/shirt_images/collarstyle/back_collar/1_style_1_casual_front_view_1.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},

                    {id: '17', rel: 'casualBase', label: "", btnCount: '1', type: 'shirt', name: 'casual collar', classname: 'shirt_part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},

                    {id: '18', rel: 'casualTuxido', label: "", type: 'shirt', name: 'Tuxido', classname: 'shirt_part custom-part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '20', rel: 'casualPlacket', label: "", type: 'shirt', name: 'casualPlacket', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '19', rel: 'casualCollar', label: "", btnCount: '1', type: 'shirt', name: 'casual collar', classname: 'shirt_part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},

                    {id: '20', rel: 'casualInnerCollar', label: "", type: 'shirt', name: 'casualInnerCollar', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '20', rel: 'casualInnerPlackets', label: "", type: 'shirt', name: 'casualInnerPlackets', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},

                    {id: '14', rel: 'ButtonHole', label: "", type: 'shirt', name: 'ButtonHole', classname: 'shirt_part custom-part', img: basePath + 'pub/media/shirt-tool/shirt_images/plackets/Placket_Front_buttonhole.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '13', rel: 'Button', label: "", type: 'shirt', name: 'Button', classname: 'shirt_part custom-part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/Shirt_Placket_Front_Buttons.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '12', rel: 'Threads', label: "", id: '1', type: 'shirt', name: 'Threads', classname: 'shirt_part custom-part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},

                    {id: '23', rel: 'ManPant', label: "", type: 'shirt', name: 'Man', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/Pant_front.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '24', rel: 'CollarBase', label: "", type: 'shirt', name: 'CollarBase', classname: 'shirt_part part custom-part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},

                    {id: '25', rel: 'SingleButton', label: "", type: 'shirt', name: 'SingleButton', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/Front_single_button.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '26', rel: 'SingleButtonHole', label: "", type: 'shirt', name: 'SingleButtonHole', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/Front_single_button_thread.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '27', rel: 'ManTie', label: "", type: 'shirt', name: 'Man', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/Tie_front.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '28', rel: 'CollarStyle', label: "", type: 'shirt', name: 'Kent Collar', classname: 'shirt_part part custom-part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '29', rel: 'Pin', label: "", type: 'shirt', name: 'Pin', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/shirt_images/collarstyle/Static/Pin_static_front.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '30', rel: 'ButtonDown', label: "", type: 'shirt', name: 'ButtonDown', classname: 'shirt_part custom-part', img: basePath + 'pub/media/shirt-tool/shirt_images/collarstyle/Static/ButtondownCollar_Button_Front.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                ]},
            {id: '2', name: 'view2', viewimages: [

                    {id: '1', rel: 'WomanBackCollarSide', label: "", type: 'jacket', name: 'Woman', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/shirt_images/collarstyle/back_collar/1_style_1_left_view_2.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '2', rel: 'FitSide', label: "", type: 'shirt', name: 'FitSide', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '3', rel: 'FitWaistedSide', label: "", type: 'shirt', name: 'FitWaistedSide', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '8', rel: 'TuxidoSide', label: "", type: 'shirt', name: 'TuxidoSide', classname: 'shirt_part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '4', rel: 'PocketSide', label: "", type: 'shirt', name: 'Pocket', classname: 'shirt_part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '5', rel: 'PlacketsSide', label: "", type: 'shirt', name: 'Plackets', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '1', rel: 'WomanBackPartSide', label: "", type: 'jacket', name: 'Woman', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/shirt_images/collarstyle/back_collar/1_style_1_left_sidepart_view_2.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '10', rel: 'CasualLookSide', label: "", id: '1', type: 'shirt', name: 'CasualLook', classname: 'shirt_part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    // {id: '11', rel: 'WomanShoes', label: "", type: 'shirt', name: 'Woman Shoe', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},

                    {id: '13', rel: 'backpantfit', label: "", type: 'pant', name: 'Regular Fit', classname: 'shirt_part ', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    //{id: '14', rel: 'casualWomanSide', label: "", btnCount: '1', type: 'shirt', name: 'casual collar', classname: 'shirt_part ', img: basePath + 'pub/media/shirt-tool/women_shirt_images/collarstyle/Woman_Static/Woman_Side.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '15', rel: 'casualBaseSide', label: "", btnCount: '1', type: 'shirt', name: 'casual collar', classname: 'shirt_part ', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '16', rel: 'casualTuxidoSide', label: "", type: 'shirt', name: 'Tuxido', classname: 'shirt_part custom-part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '18', rel: 'casualPlacketSide', label: "", type: 'shirt', name: 'casualPlacketSide', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '6', rel: 'BottonSide', label: "", type: 'shirt', name: 'BottonSide', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/Shirt_Placket_Side_Buttons.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '7', rel: 'ButtonHoleSide', label: "", type: 'shirt', name: 'ButtonHoleSide', classname: 'shirt_part custom-part', img: basePath + 'pub/media/shirt-tool/shirt_images/plackets/Placket_side_buttonhole.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '12', rel: 'ThreadsSide', label: "", id: '1', type: 'shirt', name: 'White', classname: 'shirt_part custom-part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '17', rel: 'casualCollarSide', label: "", btnCount: '1', type: 'shirt', name: 'casual collar', classname: 'shirt_part ', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '15', rel: 'casualInnerCollarSide', label: "", btnCount: '1', type: 'shirt', name: 'casual collar', classname: 'shirt_part ', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '18', rel: 'casualInnerPlacketsSide', label: "", type: 'shirt', name: 'Standard', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    // {id: '19', rel: 'WomanShoeSide', label: "", type: 'shirt', name: 'Woman', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/Shoes_Side.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '20', rel: 'BottomSide', label: "", id: '1', type: 'shirt', name: 'BottomSide', classname: 'shirt_part ', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '21', rel: 'ManPantSide', label: "", type: 'shirt', name: 'Man', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/Pant_side.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '22', rel: 'CollarBaseSide', label: "", type: 'shirt', name: 'CollarStyleSide', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '9', rel: 'SleevesSide', label: "", type: 'shirt', name: 'SleevesSide', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '12', rel: 'CuffsSide', label: "", type: 'shirt', name: 'CuffsSide', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '23', rel: 'SingleButtonSide', label: "", type: 'shirt', name: 'SingleButtonSide', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/Side_Collar_Single_button.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '24', rel: 'SingleButtonHoleSide', label: "", type: 'shirt', name: 'SingleButtonSide', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/Side_Collar_Single_button_thread.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '25', rel: 'ManTieSide', label: "", type: 'shirt', name: 'Man', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/Tie_side.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '26', rel: 'CollarStyleSide', label: "", type: 'shirt', name: 'CollarStyleSide', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '27', rel: 'PinSide', label: "", type: 'shirt', name: 'PinSide', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/shirt_images/collarstyle/Static/Pin_static_side.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '28', rel: 'ButtonDownSide', label: "", type: 'shirt', name: 'ButtonSide', classname: 'shirt_part custom-part', img: basePath + 'pub/media/shirt-tool/shirt_images/collarstyle/Static/ButtondownCollar_Button_Side.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                            //{id: '1', rel: 'Man', label: "", type: 'jacket', name: 'Man', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/Man_side.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},

//                {id: '1', rel: 'WomanBackCollarSide', label: "", type: 'jacket', name: 'Woman', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/shirt_images/collarstyle/back_collar/7_style_1_left_view_2.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
//                {id: '2', rel: 'FitSide', label: "", type: 'shirt', name: 'FitSide', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
//                {id: '3', rel: 'FitWaistedSide', label: "", type: 'shirt', name: 'FitWaistedSide', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
//                {id: '8', rel: 'TuxidoSide', label: "", type: 'shirt', name: 'TuxidoSide', classname: 'shirt_part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
//                {id: '5', rel: 'PlacketsSide', label: "", type: 'shirt', name: 'Plackets', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
//                {id: '1', rel: 'WomanBackPartSide', label: "", type: 'shirt', name: 'WomanBackPartSide', classname: 'shirt_part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
//                {id: '4', rel: 'PocketSide', label: "", type: 'shirt', name: 'Pocket', classname: 'shirt_part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
//                
//                {id: '10', rel: 'CasualLookSide', label: "", id: '1', type: 'shirt', name: 'CasualLook', classname: 'shirt_part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
//                {id: '13', rel: 'backpantfit', label: "", type: 'pant', name: 'Regular Fit', classname: 'shirt_part ', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
//                {id: '15', rel: 'casualBaseSide', label: "", btnCount: '1', type: 'shirt', name: 'casual collar', classname: 'shirt_part ', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
//
//                {id: '16', rel: 'casualTuxidoSide', label: "", type: 'shirt', name: 'Tuxido', classname: 'shirt_part custom-part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
//                {id: '18', rel: 'casualPlacketSide', label: "", type: 'shirt', name: 'casualPlacketSide', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
//
//                //{id: '11', rel: 'ManShoes', label: "", type: 'shirt', name: 'Man Shoe', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
//                {id: '12', rel: 'CuffsSide', label: "", type: 'shirt', name: 'CuffsSide', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
//
//                //{id: '14', rel: 'casualManSide', label: "", btnCount: '1', type: 'shirt', name: 'casual collar', classname: 'shirt_part ', img: basePath + 'pub/media/shirt-tool/shirt_images/collarstyle/Man_Static/Casual-Bust_Man_Static2.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
//                {id: '6', rel: 'BottonSide', label: "", type: 'shirt', name: 'BottonSide', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/Shirt_Placket_Side_Buttons.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
//                {id: '7', rel: 'ButtonHoleSide', label: "", type: 'shirt', name: 'ButtonHoleSide', classname: 'shirt_part custom-part', img: basePath + 'pub/media/shirt-tool/shirt_images/plackets/Placket_side_buttonhole.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
//                {id: '12', rel: 'ThreadsSide', label: "", id: '1', type: 'shirt', name: 'White', classname: 'shirt_part custom-part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
//                {id: '17', rel: 'casualCollarSide', label: "", btnCount: '1', type: 'shirt', name: 'casual collar', classname: 'shirt_part ', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
//
//                {id: '15', rel: 'casualInnerCollarSide', label: "", btnCount: '1', type: 'shirt', name: 'casual collar', classname: 'shirt_part ', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
//                {id: '18', rel: 'casualInnerPlacketsSide', label: "", type: 'shirt', name: 'Standard', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
//                //{id: '19', rel: 'ManShoeSide', label: "", type: 'shirt', name: 'Man', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/Shoes_Side.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
//                {id: '20', rel: 'BottomSide', label: "", id: '1', type: 'shirt', name: 'BottomSide', classname: 'shirt_part ', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
//                {id: '21', rel: 'ManPantSide', label: "", type: 'shirt', name: 'Man', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/Pant_side.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
//                {id: '22', rel: 'CollarBaseSide', label: "", type: 'shirt', name: 'CollarStyleSide', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
//                {id: '9', rel: 'SleevesSide', label: "", type: 'shirt', name: 'SleevesSide', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
//                {id: '23', rel: 'SingleButtonSide', label: "", type: 'shirt', name: 'SingleButtonSide', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/Side_Collar_Single_button.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
//                {id: '24', rel: 'SingleButtonHoleSide', label: "", type: 'shirt', name: 'SingleButtonSide', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/Side_Collar_Single_button_thread.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
//                {id: '25', rel: 'ManTieSide', label: "", type: 'shirt', name: 'Man', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/Tie_side.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
//                {id: '26', rel: 'CollarStyleSide', label: "", type: 'shirt', name: 'CollarStyleSide', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
//                {id: '27', rel: 'PinSide', label: "", type: 'shirt', name: 'PinSide', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/shirt_images/collarstyle/Static/Pin_static_side.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
//                {id: '28', rel: 'ButtonDownSide', label: "", type: 'shirt', name: 'ButtonSide', classname: 'shirt_part custom-part', img: basePath + 'pub/media/shirt-tool/shirt_images/collarstyle/Static/ButtondownCollar_Button_Side.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                ]},
            {id: '3', name: 'view3', viewimages: [
                    //{id: '1', rel: 'Man', label: "", type: 'shirt', name: 'Man', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/Man_Static_back.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '2', rel: 'FitBack', label: "", type: 'shirt', name: 'FitBack', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '3', rel: 'FitWaistedback', label: "", type: 'shirt', name: 'FitWaistedback', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '4', rel: 'SleevesBack', label: "", type: 'shirt', name: 'SleevesBack', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '5', rel: 'SleevePlacketLeft', label: "", type: 'shirt', name: 'SleevePlacketLeft', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '6', rel: 'SleevePlacketRight', label: "", type: 'shirt', name: 'SleevePlacketRight', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '7', rel: 'SleevePlacketButtonLeft', label: "", type: 'shirt', name: 'SleevePlacketButtonLeft', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/shirt_images/sleeveplacket/static/b3_left.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '8', rel: 'SleevePlacketButtonRight', label: "", type: 'shirt', name: 'SleevePlacketButtonRight', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/shirt_images/sleeveplacket/static/b3_right.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '9', rel: 'SleevePlacketThreadLeft', label: "", type: 'shirt', name: 'SleevePlacketThreadLeft', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/shirt_images/sleeveplacket/4_placket_thread_left_view_3.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '10', rel: 'SleevePlacketThreadRight', label: "", type: 'shirt', name: 'SleevePlacketThreadRight', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/shirt_images/sleeveplacket/4_placket_thread_right_view_3.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},

                    {id: '11', rel: 'CuffsBack', label: "", type: 'shirt', name: 'CuffsBack', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '12', rel: 'CuffsOutBack', label: "", type: 'shirt', name: 'CuffsOutBack', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '13', rel: 'ButtonCuffBack', label: "", type: 'shirt', name: 'ButtonCuffBack', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '14', rel: 'ButtonThreadCuffBack', label: "", type: 'shirt', name: 'ButtonThreadCuffBack', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '15', rel: 'backelbow', label: "", type: 'shirt', name: 'backelbow', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    //{id: '10', rel: 'CuffsThreadBack', label: "", type: 'shirt', name: 'CuffsThreadBack', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '16', rel: 'CollarStyleBack', label: "", type: 'shirt', name: 'CollarStyleBack', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '17', rel: 'PantBackStyle', label: "", type: 'shirt', name: 'PantBackStyle', classname: 'shirt_part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    //{id: '18', rel: 'ManShoes', label: "", type: 'shirt', name: 'Man Shoe', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '19', rel: 'CasualLookBack', label: "", id: '1', type: 'shirt', name: 'CasualLook', classname: 'shirt_part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '20', rel: 'PleatsBack', label: "", type: 'shirt', name: 'No Pleats', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    //{id: '21', rel: 'ManShoeBack', label: "", type: 'shirt', name: 'Man', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/Shoes_Back.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '22', rel: 'BottomBack', label: "", id: '1', type: 'shirt', name: 'Tail', classname: 'shirt_part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '23', rel: 'ManPantBack', label: "", type: 'shirt', name: 'Man', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/Pant_back.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                ]},
            {id: '4', name: 'view4', viewimages: [
                    {id: '1', rel: 'FitFold', label: "", type: 'shirt', name: 'FitFold', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '2', rel: 'Logo', label: "", type: 'shirt', name: 'FitFold', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/company_tag.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '3', rel: 'PlacketsFold', label: "", type: 'shirt', name: 'PlacketsFold', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '4', rel: 'TuxidoFold', label: "", type: 'shirt', name: 'TuxidoFold', classname: 'shirt_part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '5', rel: 'PocketFold', label: "", type: 'shirt', name: 'PocketFold', classname: 'shirt_part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '6', rel: 'CollarStyleFold', label: "", type: 'shirt', name: 'CollarStyleFold', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '23', rel: 'SingleButtonFold', label: "", type: 'shirt', name: 'SingleButtonSide', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/Fold_Collar_Single_button.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    // {id: '24', rel: 'SingleButtonHoleFold', label: "", type: 'shirt', name: 'SingleButtonSide', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/Fold_Collar_Single_button_thread.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '7', rel: 'PinFold', label: "", type: 'shirt', name: 'PinFold', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/shirt_images/collarstyle/Static/Pinned_Collar_Folded.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '8', rel: 'CuffsFold', label: "", type: 'shirt', name: 'CuffsFold', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '9', rel: 'CuffsFoldInner', label: "", type: 'shirt', name: 'CuffsFoldInner', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '10', rel: 'SleevesFold', label: "", type: 'shirt', name: 'SleevesFold', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '11', rel: 'BottonFold', label: "", type: 'shirt', name: 'BottonFold', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/Shirt_Placket_Folded_Buttons.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '12', rel: 'ButtonHoleFold', label: "", type: 'shirt', name: 'ButtonHoleFold', classname: 'shirt_part custom-part', img: basePath + 'pub/media/shirt-tool/shirt_images/plackets/Folded_Placket_buttonhole.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '12', rel: 'ThreadsFold', label: "", id: '1', type: 'shirt', name: 'White', classname: 'shirt_part custom-part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},

                    {id: '12', rel: 'ButtonHoleCuffFold', label: "", type: 'shirt', name: 'ButtonHoleCuffFold', classname: 'shirt_part custom-part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '13', rel: 'ButtonCuff', label: "", type: 'shirt', name: 'ButtonCuff', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/00_Buttons_double.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '14', rel: 'ButtonThreadCuff', label: "", type: 'shirt', name: 'ButtonThreadCuff', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/00_Buttons_double.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '15', rel: 'innerCollarGlow', label: "", type: 'shirt', name: 'innerCollarGlow', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '16', rel: 'ButtonDownFold', label: "", type: 'shirt', name: 'ButtonDownFold', classname: 'shirt_part custom-part', img: basePath + 'pub/media/shirt-tool/shirt_images/collarstyle/Static/ButtondownCollar_Button_Fold.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '16', rel: 'ButtonDownThreadFold', label: "", type: 'shirt', name: 'ButtonDownThreadFold', classname: 'shirt_part custom-part', img: basePath + 'pub/media/shirt-tool/shirt_images/collarstyle/Static/buttondown_thread_fold.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                            //{id: '16', rel: 'ButtonDownHoleFold', label: "", type: 'shirt', name: 'ButtonDownHoleFold', classname: 'shirt_part custom-part', img: basePath + 'pub/media/shirt-tool/shirt_images/collarstyle/Static/ButtondownCollar_Button_Fold.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0}


                ]},
//        {id: '5', name: 'view5', viewimages: [
//                {id: '1', rel: 'FitZoom', label: "", type: 'shirt', name: 'SleevesFold', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/fit.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
//                {id: '2', rel: 'TuxidoZoom', label: "", type: 'shirt', name: 'TuxidoFold', classname: 'shirt_part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
//                {id: '3', rel: 'CollarStyleZoominner', label: "", type: 'shirt', name: 'BottonFold', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/collarinner.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
//                {id: '4', rel: 'CollarStyleZoom', label: "", type: 'shirt', name: 'CollarStyleFold', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/collar.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
//                {id: '5', rel: 'BottonZoomHole', label: "", type: 'shirt', name: 'BottonFold', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/Torso_Buttonhole.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
//                {id: '6', rel: 'BottonZoom', label: "", type: 'shirt', name: 'BottonFold', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/Torso_Buttons.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
//                {id: '7', rel: 'BottonZoomThread', label: "", type: 'shirt', name: 'BottonFold', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/Torso_ButtonThreads.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
//                {id: '8', rel: 'ZoomBottondown', label: "", type: 'shirt', name: 'BottonFold', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
//            ]}

        ];


        var viewDataSuitZoom = [
            {id: '1', name: 'view5', viewimages: [
                    {id: '1', rel: 'FitZoom', label: "", type: 'shirt', name: 'FitZoom', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/fit.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '3', rel: 'PlacketsZoom', label: "", type: 'shirt', name: 'PlacketsZoom', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '2', rel: 'TuxidoZoom', label: "", type: 'shirt', name: 'TuxidoZoom', classname: 'shirt_part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '3', rel: 'CollarStyleZoominner', label: "", type: 'shirt', name: 'CollarStyleZoominner', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/collarinner.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '4', rel: 'CollarStyleZoom', label: "", type: 'shirt', name: 'CollarStyleZoom', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/collar.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '5', rel: 'BottonZoomHole', label: "", type: 'shirt', name: 'BottonZoomHole', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/Torso_Buttonhole.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '6', rel: 'BottonZoom', label: "", type: 'shirt', name: 'BottonZoom', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/Torso_Buttons.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '7', rel: 'BottonZoomThread', label: "", type: 'shirt', name: 'BottonZoomThread', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/Torso_ButtonThreads.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '8', rel: 'ZoomBottondown', label: "", type: 'shirt', name: 'ZoomBottondown', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '9', rel: 'PinZoom', label: "", type: 'shirt', name: 'PinZoom', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/shirt_images/collarstyle/Static/Pin_static_zoom.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},

                    {id: '5', rel: 'BottonBaseZoomHole', label: "", type: 'shirt', name: 'BottonZoomHole', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/Torso_base_Buttonhole.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '6', rel: 'BottonBaseZoom', label: "", type: 'shirt', name: 'BottonZoom', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/Torso_base_Buttons.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '7', rel: 'BottonBaseZoomThread', label: "", type: 'shirt', name: 'BottonZoomThread', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/default_shirttool_image/Torso_base_ButtonThreads.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},

                    {id: '11', rel: 'ButtondownThreadZoom', label: "", type: 'shirt', name: 'ButtondownThreadZoom', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/buttonfabric_images/1_final_buttonhole_view_5.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '10', rel: 'ButtondownZoom', label: "", type: 'shirt', name: 'BottondownZoom', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/shirt_images/collarstyle/Static/ButtondownCollar_Button_Zoom.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '12', rel: 'ButtondownHoleZoom', label: "", type: 'shirt', name: 'ButtondownHoleZoom', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/buttonfabric_images/1_final_buttonthread_view_5.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                ]}
        ];


        return{
            getSuitView: function () {
                return viewDataSuit;
            },
            getSuitViewZoom: function () {
                return viewDataSuitZoom;
            },
        }
//end


    });

    angular.module("Shirt").factory("designService", function ($http) {

        var designs = [
            {"id": "1", "name": "Fit", "flag": "false", img: basePath + "media/jacket_img/thumbnail/00Fit_Main_Icon.png", "target": "0", "style": [
                    {"id": "1", "name": "Slim Fit", "parent": "Fit", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Fit_Slim.png", "prev": "", "live": [
                            {id: '1', name: 'Front', img: basePath + 'pub/media/shirt-tool/jacket_img/images/fit_red.png'},
                            {id: '2', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/images/Jacket_slim_back_final.png'},
                            {id: '3', name: 'Inside', img: basePath + 'pub/media/shirt-tool/jacket_img/images/suit_1_fabric_13_style_1_view_1.png'}
                        ]},
                    {"id": "2", "name": "Classic Fit", "parent": "Fit", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Fit_Classic.png", "prev": "", "live": [
                            {id: '1', name: 'Front', img: basePath + 'pub/media/shirt-tool/jacket_img/images/suit_1_fabric_13_style_3_view_1.png'},
                            {id: '2', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/images/Jacket_regular_back_final.png'},
                            {id: '3', name: 'Inside', img: basePath + 'pub/media/shirt-tool/jacket_img/images/Jacket_slim_back_final.png'}
                        ]}
                ]},
            {"id": "2", "name": "Style", "flag": "false", img: basePath + "media/jacket_img/thumbnail/00_Style_Main_Icon.png", "target": "0", "style": [
                    {"id": "1", "name": "Single-breasted 1 button", "parent": "Style", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Style_1_button_Single_breasted.png", "prev": "", "live": [
                            {id: '1', name: 'Front', img: basePath + 'pub/media/shirt-tool/jacket_img/images/suit_1_fabric_13_style_3_view_1.png'},
                            {id: '2', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/images/blank.png'},
                            {id: '3', name: 'Inside', img: basePath + 'pub/media/shirt-tool/jacket_img/images/Jacket_slim_back_final.png'}
                        ]},
                    {"id": "2", "name": "Single-breasted 2 buttons", "parent": "Style", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Style_2_button_Single_breasted.png", "prev": "media/jacket_img/preview/2_button_single_breasted.png", "live": [
                            {id: '1', name: 'Front', img: basePath + 'pub/media/shirt-tool/jacket_img/images/suit_1_fabric_13_style_3_view_1.png'},
                            {id: '2', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/images/blank.png'},
                            {id: '3', name: 'Inside', img: basePath + 'pub/media/shirt-tool/jacket_img/images/Jacket_slim_back_final.png'}
                        ]},
                    {"id": "3", "name": "Single-breasted 3 buttons", "parent": "Style", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Style_3_button_Single_breasted.png", "prev": "media/jacket_img/preview/3_button_single_breasted.png", "live": [
                            {id: '1', name: 'Front', img: basePath + 'pub/media/shirt-tool/jacket_img/images/suit_1_fabric_13_style_3_view_1.png'},
                            {id: '2', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/images/blank.png'},
                            {id: '3', name: 'Inside', img: basePath + 'pub/media/shirt-tool/jacket_img/images/Jacket_slim_back_final.png'}
                        ]},
                    {"id": "4", "name": "Double-breasted 2 buttons", "parent": "Style", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Style_2_button_Double_breasted.png", "prev": "media/jacket_img/preview/4_button_double_breasted_1_close.png", "live": [
                            {id: '1', name: 'Front', img: basePath + 'pub/media/shirt-tool/jacket_img/images/suit_1_fabric_13_style_1_view_1.png'},
                            {id: '2', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/images/blank.png'},
                            {id: '3', name: 'Inside', img: basePath + 'pub/media/shirt-tool/jacket_img/images/Jacket_slim_back_final.png'}
                        ]},
                    {"id": "5", "name": "Double-breasted 4 buttons", "parent": "Style", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Style_4_button_Double_breasted.png", "prev": "media/jacket_img/preview/4_button_double_breasted_2_close.png", "live": [
                            {id: '1', name: 'Front', img: basePath + 'pub/media/shirt-tool/jacket_img/images/suit_1_fabric_13_style_1_view_1.png'},
                            {id: '2', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/images/blank.png'},
                            {id: '3', name: 'Inside', img: basePath + 'pub/media/shirt-tool/jacket_img/images/Jacket_slim_back_final.png'}
                        ]},
                    {"id": "6", "name": "Double-breasted 6 buttons", "parent": "Style", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Style_6_button_Double_breasted.png", "prev": "media/jacket_img/preview/6_button_double_breasted_2_close.png", "live": [
                            {id: '1', name: 'Front', img: basePath + 'pub/media/shirt-tool/jacket_img/images/suit_1_fabric_13_style_1_view_1.png'},
                            {id: '2', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/images/blank.png'},
                            {id: '3', name: 'Inside', img: basePath + 'pub/media/shirt-tool/jacket_img/images/Jacket_slim_back_final.png'}
                        ]},
                    {"id": "6", "name": "Mandarin", "parent": "Style", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Style_Mandarin.png", "prev": "media/jacket_img/preview/6_button_double_breasted_2_close.png", "live": [
                            {id: '1', name: 'Front', img: basePath + 'pub/media/shirt-tool/jacket_img/images/suit_1_fabric_13_style_2_view_11.png'},
                            {id: '2', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/images/blank.png'},
                            {id: '3', name: 'Inside', img: basePath + 'pub/media/shirt-tool/jacket_img/images/Jacket_slim_back_final.png'}
                        ]}
                ]},
            {"id": "3", "name": "Lapel", "flag": "false", img: basePath + "media/jacket_img/thumbnail/Width_Wide.png", "target": "0", "style": [
                    {"id": "1", "name": "Notch", "parent": "Lapel", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Lapel_Notch.png", "prev": "media/jacket_img/preview/Notch.png", "width": [
                            {id: '1', name: 'Slim', img: basePath + 'pub/media/shirt-tool/jacket_img/Width_Slim.png', live: [
                                    {id: '1', name: 'Front', img: basePath + 'pub/media/shirt-tool/jacket_img/accessories/6_btn_single_breasted.png'},
                                    {id: '2', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/images/blank.png'},
                                    {id: '3', name: 'Inside', img: basePath + 'pub/media/shirt-tool/jacket_img/images/Jacket_slim_back_final.png'}]},
                            {id: '2', name: 'Standard', img: basePath + 'pub/media/shirt-tool/jacket_img/Width_Standard.png', live: [
                                    {id: '1', name: 'Front', img: basePath + 'pub/media/shirt-tool/jacket_img/accessories/6_btn_single_breasted.png'},
                                    {id: '2', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/images/blank.png'},
                                    {id: '3', name: 'Inside', img: basePath + 'pub/media/shirt-tool/jacket_img/images/Jacket_slim_back_final.png'}]},
                            {id: '3', name: 'Wide', img: basePath + 'pub/media/shirt-tool/jacket_img/Width_Wide.png', live: [
                                    {id: '1', name: 'Front', img: basePath + 'pub/media/shirt-tool/jacket_img/accessories/6_btn_single_breasted.png'},
                                    {id: '2', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/images/blank.png'},
                                    {id: '3', name: 'Inside', img: basePath + 'pub/media/shirt-tool/jacket_img/images/Jacket_slim_back_final.png'}]}]},
                    {"id": "2", "name": "Peak", "parent": "Lapel", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Lapel_Peak.png", "prev": "", "width": [
                            {id: '1', name: 'Slim', img: basePath + 'pub/media/shirt-tool/jacket_img/Width_Slim.png', live: [
                                    {id: '1', name: 'Front', img: basePath + 'pub/media/shirt-tool/jacket_img/accessories/6_btn_single_breasted.png'},
                                    {id: '2', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/images/blank.png'},
                                    {id: '3', name: 'Inside', img: basePath + 'pub/media/shirt-tool/jacket_img/images/Jacket_slim_back_final.png'}]},
                            {id: '2', name: 'Standard', img: basePath + 'pub/media/shirt-tool/jacket_img/Width_Standard.png', live: [
                                    {id: '1', name: 'Front', img: basePath + 'pub/media/shirt-tool/jacket_img/accessories/6_btn_single_breasted.png'},
                                    {id: '2', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/images/blank.png'},
                                    {id: '3', name: 'Inside', img: basePath + 'pub/media/shirt-tool/jacket_img/images/Jacket_slim_back_final.png'}]},
                            {id: '3', name: 'Wide', img: basePath + 'pub/media/shirt-tool/jacket_img/Width_Wide.png', live: [
                                    {id: '1', name: 'Front', img: basePath + 'pub/media/shirt-tool/jacket_img/accessories/6_btn_single_breasted.png'},
                                    {id: '2', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/images/blank.png'},
                                    {id: '3', name: 'Inside', img: basePath + 'pub/media/shirt-tool/jacket_img/images/Jacket_slim_back_final.png'}]}]},
                    {"id": "3", "name": "Shawl", "parent": "Lapel", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Lapel_Shawl.png", "prev": "", "width": [
                            {id: '1', name: 'Slim', img: basePath + 'pub/media/shirt-tool/jacket_img/Width_Slim.png', live: [
                                    {id: '1', name: 'Front', img: basePath + 'pub/media/shirt-tool/jacket_img/accessories/6_btn_single_breasted.png'},
                                    {id: '2', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/images/blank.png'},
                                    {id: '3', name: 'Inside', img: basePath + 'pub/media/shirt-tool/jacket_img/images/Jacket_slim_back_final.png'}]},
                            {id: '2', name: 'Standard', img: basePath + 'pub/media/shirt-tool/jacket_img/Width_Standard.png', live: [
                                    {id: '1', name: 'Front', img: basePath + 'pub/media/shirt-tool/jacket_img/accessories/6_btn_single_breasted.png'},
                                    {id: '2', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/images/blank.png'},
                                    {id: '3', name: 'Inside', img: basePath + 'pub/media/shirt-tool/jacket_img/images/Jacket_slim_back_final.png'}]},
                            {id: '3', name: 'Wide', img: basePath + 'pub/media/shirt-tool/jacket_img/Width_Wide.png', live: [
                                    {id: '1', name: 'Front', img: basePath + 'pub/media/shirt-tool/jacket_img/accessories/6_btn_single_breasted.png'},
                                    {id: '2', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/images/blank.png'},
                                    {id: '3', name: 'Inside', img: basePath + 'pub/media/shirt-tool/jacket_img/images/Jacket_slim_back_final.png'}]}]}
                ]},
            {"id": "4", "name": "Pockets_Regular", img: basePath + "media/jacket_img/thumbnail/00_Pocket_Main_Icon.png", "flag": "false", "target": "0", "style": [
                    //{"id": "1", "name": "Regular", "parent": "Pockets", "part": "1", "price": "0", "desc": "description", img: basePath + "", "prev": "", "live": [
                    {id: '1', name: 'No Pocket', "parent": "Pockets_Regular", img: basePath + 'pub/media/shirt-tool/jacket_img/No_pocket-icon.png',
                        live: [
                            {id: '1', name: 'Front', img: basePath + 'pub/media/shirt-tool/jacket_img/accessories/6_btn_single_breasted.png'},
                            {id: '2', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/images/blank.png'},
                            {id: '3', name: 'Inside', img: basePath + 'pub/media/shirt-tool/jacket_img/Jacket_slim_back_final.png'}
                        ]},
                    {id: '2', name: 'Flap', "parent": "Pockets Regular", img: basePath + 'pub/media/shirt-tool/jacket_img/Pocket_Regular_Flap.png',
                        live: [
                            {id: '1', name: 'Front', img: basePath + 'pub/media/shirt-tool/jacket_img/accessories/6_btn_single_breasted.png'},
                            {id: '2', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/images/blank.png'},
                            {id: '3', name: 'Inside', img: basePath + 'pub/media/shirt-tool/jacket_img/imagesJacket_slim_back_final.png'}
                        ]},
                    {id: '3', name: 'Patch', "parent": "Pockets Regular", img: basePath + 'pub/media/shirt-tool/jacket_img/Pocket_Regular_Patch.png',
                        live: [
                            {id: '1', name: 'Front', img: basePath + 'pub/media/shirt-tool/jacket_img/accessories/6_btn_single_breasted.png'},
                            {id: '2', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/images/blank.png'},
                            {id: '3', name: 'Inside', img: basePath + 'pub/media/shirt-tool/jacket_img/images/Jacket_slim_back_final.png'}
                        ]},
                    {id: '4', name: 'Welted', "parent": "Pockets Regular", img: basePath + 'pub/media/shirt-tool/jacket_img/Pocket_Regular_Welted.png',
                        live: [
                            {id: '1', name: 'Front', img: basePath + 'pub/media/shirt-tool/jacket_img/accessories/6_btn_single_breasted.png'},
                            {id: '2', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/images/blank.png'},
                            {id: '3', name: 'Inside', img: basePath + 'pub/media/shirt-tool/jacket_img/images/Jacket_slim_back_final.png'}
                        ]}
//]},
                ]},
            {"id": "5", "name": "Pockets_Slanted", img: basePath + "media/jacket_img/thumbnail/Pocket_Slanted_Flap.png", "flag": "false", "target": "0", "style": [
                    {id: '1', name: 'No Pocket', "parent": "Pockets_Slanted", img: basePath + 'pub/media/shirt-tool/jacket_img/No_pocket-icon.png',
                        live: [
                            {id: '1', name: 'Front', img: basePath + 'pub/media/shirt-tool/jacket_img/accessories/6_btn_single_breasted.png'},
                            {id: '2', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/images/blank.png'},
                            {id: '3', name: 'Inside', img: basePath + 'pub/media/shirt-tool/jacket_img/images/Jacket_slim_back_final.png'}
                        ]},
                    {id: '2', name: 'Flap', "parent": "Pockets Slanted", img: basePath + 'pub/media/shirt-tool/jacket_img/Pocket_Slanted_Flap.png',
                        live: [
                            {id: '1', name: 'Front', img: basePath + 'pub/media/shirt-tool/jacket_img/accessories/6_btn_single_breasted.png'},
                            {id: '2', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/images/blank.png'},
                            {id: '3', name: 'Inside', img: basePath + 'pub/media/shirt-tool/jacket_img/images/Jacket_slim_back_final.png'}
                        ]},
                    {id: '3', name: 'Patch', "parent": "Pockets Slanted", img: basePath + 'pub/media/shirt-tool/jacket_img/Pocket_Slanted_Patch.png',
                        live: [
                            {id: '1', name: 'Front', img: basePath + 'pub/media/shirt-tool/jacket_img/accessories/6_btn_single_breasted.png'},
                            {id: '2', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/images/blank.png'},
                            {id: '3', name: 'Inside', img: basePath + 'pub/media/shirt-tool/jacket_img/images/Jacket_slim_back_final.png'}
                        ]},
                    {id: '4', name: 'Welted', "parent": "Pockets Slanted", img: basePath + 'pub/media/shirt-tool/jacket_img/Pocket_Slanted_Welted.png',
                        live: [
                            {id: '1', name: 'Front', img: basePath + 'pub/media/shirt-tool/jacket_img/accessories/6_btn_single_breasted.png'},
                            {id: '2', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/images/blank.png'},
                            {id: '3', name: 'Inside', img: basePath + 'pub/media/shirt-tool/jacket_img/images/Jacket_slim_back_final.png'}
                        ]},
                ]},
            {"id": "6", "name": "Sleeve_Button", img: basePath + "media/jacket_img/thumbnail/00_vents_Main_Icon.png", "flag": "false", img: basePath + "media/jacket_img/thumbnail/00_Main_Icon.png", "target": "0", "style": [
                    {"id": "1", "name": "0 Standard Button", "parent": "Sleeve_Button", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Sleeve_Button_2.png", "prev": "media/jacket_img/preview/01_Three_Standard_Buttons.png", "live": [
                            {id: '1', name: 'Front', img: basePath + 'pub/media/shirt-tool/jacket_img/accessories/3_btn_std_sleeve.png'},
                            {id: '2', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/accessories/blank.png'}
                        ]},
                    {"id": "2", "name": "2 Standard Button", "parent": "Sleeve Button", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Sleeve_Button_2.png", "prev": "media/jacket_img/preview/01_Three_Standard_Buttons.png", "live": [
                            {id: '1', name: 'Front', img: basePath + 'pub/media/shirt-tool/jacket_img/accessories/3_btn_std_sleeve.png'},
                            {id: '2', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/accessories/blank.png'}
                        ]},
                    {"id": "3", "name": "3 Standard Button", "parent": "Sleeve Button", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Sleeve_Button_3.png", "prev": "media/jacket_img/preview/02_Four_Standard_Buttons.png", "live": [
                            {id: '1', name: 'Front', img: basePath + 'pub/media/shirt-tool/jacket_img/accessories/4_btn_std_sleeve.png'},
                            {id: '2', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/accessories/blank.png'}
                        ]},
                    {"id": "4", "name": "4 Standard Button", "parent": "Sleeve Button", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Sleeve_Button_4.png", "prev": "media/jacket_img/preview/03_Five_Standard_Buttons.png", "live": [
                            {id: '1', name: 'Front', img: basePath + 'pub/media/shirt-tool/jacket_img/accessories/5_btn_std_sleeve.png'},
                            {id: '2', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/accessories/blank.png'}
                        ]}
                ]},
            {"id": "7", "name": "Vents", img: basePath + "media/jacket_img/thumbnail/00_vents_Main_Icon.png", "flag": "false", "target": "1", "style": [
                    {"id": "1", "name": "ventless", "parent": "Vents", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Vents_Side.png", "prev": "media/jacket_img/Vents_No_Vents.png", "live": [
                            {id: '1', name: 'Front', img: basePath + 'pub/media/shirt-tool/jacket_img/images/blank.png'},
                            {id: '2', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/images/blank.png'},
                            {id: '3', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/images/blank.png'}
                        ]},
                    {"id": "2", "name": "Center Vent", "parent": "Vents", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Vents_No_Vents.png", "prev": "media/jacket_img/Vents_Center_Vents.png", "live": [
                            {id: '1', name: 'Front', img: basePath + 'pub/media/shirt-tool/jacket_img/images/blank.png'},
                            {id: '2', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/images/blank.png'},
                            {id: '3', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/images/blank.png'}
                        ]},
                    {"id": "3", "name": "Side Vents", "parent": "Vents", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Vents_Center_Vents.png", "prev": "media/jacket_img/Vents_Side.png", "live": [
                            {id: '1', name: 'Front', img: basePath + 'pub/media/shirt-tool/jacket_img/images/blank.png'},
                            {id: '2', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/accessories/jacket_vent.png'},
                            {id: '3', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/images/blank.png'}
                        ]}
                ]}

        ];

        //load data static way
        /* return{
         getDesignData: function(e) {
         return designs;
         }
         }*/

        //load data dynamic way
        return {
            getDesignData: function (fabricId, genderId, productId) {

                return $http.post(basePath + 'men-shirt/index/getShirtDesign', {
                    fabricId: fabricId,
                    genderId: genderId,
                    productId: productId
                });
            }
        };
    });

    angular.module("Shirt").factory("fabricService", function ($http) {

        var fabrics = [
            {"id": "1", "name": "Fabric 1", img: basePath + "media/jacket_img/all_fabric_images/01.png", "target": "", "style": [
                    {"id": "1", "name": "Violet", "code": "AA1", "price": "134", img: basePath + "media/jacket_img/all_fabric_images/01.png", "color": "Violet Light", "pattern": "Plain"}

                ]},
            {"id": "2", "name": "Fabric 2", img: basePath + "media/jacket_img/all_fabric_images/02.png", "target": "", "style": [
                    {"id": "21", "name": "Blue1", "code": "AA3", "price": "130", img: basePath + "media/jacket_img/all_fabric_images/02.png", "color": "Blue", "pattern": "Plain"}
                ]},
            {"id": "3", "name": "Fabric 3", img: basePath + "media/jacket_img/all_fabric_images/03.png", "target": "", "style": [
                    {"id": "31", "name": "Blue2", "code": "AA4", "price": "130", img: basePath + "media/jacket_img/all_fabric_images/03.png", "color": "Blue", "pattern": "Plain"}
                ]},
            {"id": "4", "name": "Fabric 4", img: basePath + "media/jacket_img/all_fabric_images/04.png", "target": "", "style": [
                    {"id": "41", "name": "Blue3", "code": "AA6", "price": "130", img: basePath + "media/jacket_img/all_fabric_images/04.png", "color": "Blue", "pattern": "Plain"}
                ]},
            {"id": "5", "name": "Fabric 5", img: basePath + "media/jacket_img/all_fabric_images/05.png", "target": "", "style": [
                    {"id": "51", "name": "Blue4", "code": "AA3", "price": "130", img: basePath + "media/jacket_img/all_fabric_images/05.png", "color": "Blue", "pattern": "Plain"}
                ]},
            {"id": "6", "name": "Fabric 6", img: basePath + "media/jacket_img/all_fabric_images/06.png", "target": "", "style": [
                    {"id": "61", "name": "Blue5", "code": "AA3", "price": "130", img: basePath + "media/jacket_img/all_fabric_images/06.png", "color": "Blue", "pattern": "Plain"}
                ]},
            {"id": "7", "name": "Fabric 6", img: basePath + "media/jacket_img/all_fabric_images/07.png", "target": "", "style": [
                    {"id": "71", "name": "Blue6", "code": "AA3", "price": "130", img: basePath + "media/jacket_img/all_fabric_images/07.png", "color": "Blue", "pattern": "Plain"}
                ]},
            {"id": "8", "name": "Fabric 7", img: basePath + "media/jacket_img/all_fabric_images/08.png", "target": "", "style": [
                    {"id": "81", "name": "Blue7", "code": "AA3", "price": "130", img: basePath + "media/jacket_img/all_fabric_images/08.png", "color": "Blue", "pattern": "Plain"}
                ]},
            {"id": "9", "name": "Fabric 8", img: basePath + "media/jacket_img/all_fabric_images/09.png", "target": "", "style": [
                    {"id": "91", "name": "Blue8", "code": "AA3", "price": "130", img: basePath + "media/jacket_img/all_fabric_images/09.png", "color": "Blue", "pattern": "Plain"}
                ]},
            {"id": "10", "name": "Fabric 9", img: basePath + "media/jacket_img/all_fabric_images/10.png", "target": "", "style": [
                    {"id": "101", "name": "Blue9", "code": "AA3", "price": "130", img: basePath + "media/jacket_img/all_fabric_images/10.png", "color": "Blue", "pattern": "Plain"}
                ]},
            {"id": "11", "name": "Fabric 10", img: basePath + "media/jacket_img/all_fabric_images/11.png", "target": "", "style": [
                    {"id": "111", "name": "Blue10", "code": "AA3", "price": "130", img: basePath + "media/jacket_img/all_fabric_images/11.png", "color": "Blue", "pattern": "Plain"}
                ]},
            {"id": "12", "name": "Fabric 11", img: basePath + "media/jacket_img/all_fabric_images/12.png", "target": "", "style": [
                    {"id": "121", "name": "Blue611", "code": "AA3", "price": "130", img: basePath + "media/jacket_img/all_fabric_images/12.png", "color": "Blue", "pattern": "Plain"}
                ]},
            {"id": "13", "name": "Fabric 12", img: basePath + "media/jacket_img/all_fabric_images/13.png", "target": "", "style": [
                    {"id": "131", "name": "Blue", "code": "AA3", "price": "130", img: basePath + "media/jacket_img/all_fabric_images/13.png", "color": "Blue", "pattern": "Plain"}
                ]},
            {"id": "14", "name": "Fabric 13", img: basePath + "media/jacket_img/all_fabric_images/14.png", "target": "", "style": [
                    {"id": "141", "name": "Blue", "code": "AA3", "price": "130", img: basePath + "media/jacket_img/all_fabric_images/14.png", "color": "Blue", "pattern": "Plain"}
                ]},
            {"id": "15", "name": "Fabric 14", img: basePath + "media/jacket_img/all_fabric_images/15.png", "target": "", "style": [
                    {"id": "151", "name": "Blue", "code": "AA3", "price": "130", img: basePath + "media/jacket_img/all_fabric_images/15.png", "color": "Blue", "pattern": "Plain"}
                ]},
            {"id": "16", "name": "Fabric 15", img: basePath + "media/jacket_img/all_fabric_images/16.png", "target": "", "style": [
                    {"id": "161", "name": "Blue", "code": "AA3", "price": "130", img: basePath + "media/jacket_img/all_fabric_images/16.png", "color": "Blue", "pattern": "Plain"}
                ]},
            {"id": "17", "name": "Fabric 16", img: basePath + "media/jacket_img/all_fabric_images/17.png", "target": "", "style": [
                    {"id": "171", "name": "Blue", "code": "AA3", "price": "130", img: basePath + "media/jacket_img/all_fabric_images/17.png", "color": "Blue", "pattern": "Plain"}
                ]},
            {"id": "18", "name": "Fabric 17", img: basePath + "media/jacket_img/all_fabric_images/18.png", "target": "", "style": [
                    {"id": "181", "name": "Blue", "code": "AA3", "price": "130", img: basePath + "media/jacket_img/all_fabric_images/18.png", "color": "Blue", "pattern": "Plain"}
                ]},
            {"id": "19", "name": "Fabric 18", img: basePath + "media/jacket_img/all_fabric_images/19.png", "target": "", "style": [
                    {"id": "191", "name": "Blue", "code": "AA3", "price": "130", img: basePath + "media/jacket_img/all_fabric_images/19.png", "color": "Blue", "pattern": "Plain"}
                ]},
            {"id": "20", "name": "Fabric 19", img: basePath + "media/jacket_img/all_fabric_images/20.png", "target": "", "style": [
                    {"id": "201", "name": "Blue", "code": "AA3", "price": "130", img: basePath + "media/jacket_img/all_fabric_images/20.png", "color": "Blue", "pattern": "Plain"}
                ]}
        ];
        //staic way
        /* return {
         getFabricData: function (e) {
         return fabrics;
         
         }
         };*/
        //dynamic way

        var fonts = [
            {"id": "1", "name": "Bold", "price": "5", "font": "comicbd.ttf", img: basePath + "pub/media/shirt-tool/tool_data/img/abc-04.svg"},
            {"id": "2", "name": "Arial", "price": "5", "font": "black_jack.ttf", img: basePath + "pub/media/shirt-tool/tool_data/img/abc-01.svg"},
            {"id": "3", "name": "Italic", "price": "5", "font": "brushscn.ttf", img: basePath + "pub/media/shirt-tool/tool_data/img/abc-03.svg"},
            {"id": "4", "name": "Brush", "price": "5", "font": "COMSCRTN.TTF", img: basePath + "pub/media/shirt-tool/tool_data/img/abc-02.svg"},
        ];
        var monoColor = [
            {"id": "1", "name": "color4", "price": "11", "color": "Orange", img: basePath + "pub/media/shirt-tool/tool_data/img/color_img/18.png"},
            {"id": "2", "name": "color2", "price": "5", "color": "Green", img: basePath + "pub/media/shirt-tool/tool_data/img/color_img/03.png"},
            {"id": "3", "name": "color3", "price": "20", "color": "Blue", img: basePath + "pub/media/shirt-tool/tool_data/img/color_img/08.png"},
            {"id": "4", "name": "color1", "price": "10", "color": "Silver", img: basePath + "pub/media/shirt-tool/tool_data/img/color_img/01.png"},
            {"id": "5", "name": "color5", "price": "7", "color": "Brown", img: basePath + "pub/media/shirt-tool/tool_data/img/color_img/33.png"},
            {"id": "6", "name": "color6", "price": "5", "color": "Red", img: basePath + "pub/media/shirt-tool/tool_data/img/color_img/13.png"},
        ];
        var position = [
            {"id": "10", "name": "position-1", "price": "5", "poss": "High", img: basePath + ""},
            {"id": "11", "name": "position-2", "price": "5", "poss": "Medium", img: basePath + ""},
            {"id": "12", "name": "position-3", "price": "5", "poss": "Low", img: basePath + ""},
            {"id": "13", "name": "position-4", "price": "5", "poss": "Cuff", img: basePath + ""},
        ];

        return {
            getFabricData: function (genderId, productId) {

                return $http.post(basePath + 'men-shirt/index/getShirtFabrics', {
                    genderId: genderId,
                    productId: productId
                });
            },
            getThreadFabricData: function (genderId, productId) {

                return $http.post(basePath + 'men-shirt/index/getShirtButtonFabric', {
                    genderId: genderId,
                    productId: productId
                });
            },
            getFonts: function () {

                return fonts;
            },
            getMonoColors: function () {

                return monoColor;
            },
            getPos: function () {

                return position;
            }

        };





    });

    angular.module("Shirt").factory("vestService", function ($http) {

        var vestDesign = [
            {"id": "1", "name": "Style", "flag": "false", img: basePath + "media/jacket_img/thumbnail/00_Vest_Style_Main_Icon.png", "target": "0", "style": [
                    {"id": "1", "name": "Double Breasted", "parent": "Fit", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Vest_Style_Double_Breasted.png", "prev": "", "live": [
                            {id: '1', name: 'Front', img: basePath + 'pub/media/shirt-tool/jacket_img/images/jacket_slim_fit_final.png'},
                            {id: '2', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/images/Jacket_slim_back_final.png'},
                            {id: '3', name: 'Inside', img: basePath + 'pub/media/shirt-tool/jacket_img/images/Jacket_slim_back_final.png'}
                        ]},
                    {"id": "2", "name": "Single Breasted", "parent": "Fit", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Vest_Style_Single_Breasted.png", "prev": "", "live": [
                            {id: '1', name: 'Front', img: basePath + 'pub/media/shirt-tool/jacket_img/images/jacket_std_fit_final.png'},
                            {id: '2', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/images/Jacket_regular_back_final.png'},
                            {id: '3', name: 'Inside', img: basePath + 'pub/media/shirt-tool/jacket_img/images/Jacket_slim_back_final.png'}
                        ]}
                ]},
            {"id": "2", "name": "Vest", "flag": "false", img: basePath + "media/jacket_img/thumbnail/00_Style_Main_Icon.png", "target": "0", "style": [
                    {"id": "1", "name": "No vest", "parent": "Style", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Style_1_button_Single_breasted.png", "prev": "", "live": [
                            {id: '1', name: 'Front', img: basePath + 'pub/media/shirt-tool/jacket_img/accessories/1_btn_single_breasted.png'},
                            {id: '2', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/images/blank.png'},
                            {id: '3', name: 'Inside', img: basePath + 'pub/media/shirt-tool/jacket_img/images/Jacket_slim_back_final.png'}
                        ]},
                    {"id": "2", "name": "3 Piece Suite", "parent": "Style", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Style_2_button_Single_breasted.png", "prev": "media/jacket_img/preview/2_button_single_breasted.png", "live": [
                            {id: '1', name: 'Front', img: basePath + 'pub/media/shirt-tool/jacket_img/accessories/2_btn_single_breasted.png'},
                            {id: '2', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/images/blank.png'},
                            {id: '3', name: 'Inside', img: basePath + 'pub/media/shirt-tool/jacket_img/images/Jacket_slim_back_final.png'}
                        ]}
                ]},
            {"id": "3", "name": "Lapel", "flag": "false", img: basePath + "media/jacket_img/thumbnail/00_Vest_Main_icon.png", "target": "0", "style": [
                    {"id": "1", "name": "Notch", "parent": "Lapel", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Lapel_Notch.png", "prev": "media/jacket_img/preview/Notch.png", "width": [
                            {
                                id: '1', name: 'Slim', img: basePath + 'pub/media/shirt-tool/jacket_img/Lapel_Width_Narrow.png', live: [
                                    {id: '1', name: 'Front', img: basePath + 'pub/media/shirt-tool/jacket_img/accessories/6_btn_single_breasted.png'},
                                    {id: '2', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/images/blank.png'},
                                    {id: '3', name: 'Inside', img: basePath + 'pub/media/shirt-tool/jacket_img/images/Jacket_slim_back_final.png'}
                                ]
                            },
                            {
                                id: '2', name: 'Standard', img: basePath + 'pub/media/shirt-tool/jacket_img/Lapel_Width_Wide.png', live: [
                                    {id: '1', name: 'Front', img: basePath + 'pub/media/shirt-tool/jacket_img/accessories/6_btn_single_breasted.png'},
                                    {id: '2', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/images/blank.png'},
                                    {id: '3', name: 'Inside', img: basePath + 'pub/media/shirt-tool/jacket_img/images/Jacket_slim_back_final.png'}
                                ]
                            }
                        ]},
                    {"id": "2", "name": "Peak", "parent": "Lapel", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Vest_Peak.png", "prev": "", "width": [
                            {
                                id: '1',
                                name: 'Slim',
                                img: basePath + 'pub/media/shirt-tool/jacket_img/Lapel_Width_Narrow.png',
                                live: [
                                    {id: '1', name: 'Front', img: basePath + 'pub/media/shirt-tool/jacket_img/accessories/6_btn_single_breasted.png'},
                                    {id: '2', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/images/blank.png'},
                                    {id: '3', name: 'Inside', img: basePath + 'pub/media/shirt-tool/jacket_img/images/Jacket_slim_back_final.png'}
                                ]
                            },
                            {
                                id: '2',
                                name: 'Standard',
                                img: basePath + 'pub/media/shirt-tool/jacket_img/Lapel_Width_Wide.png',
                                live: [
                                    {id: '1', name: 'Front', img: basePath + 'pub/media/shirt-tool/jacket_img/accessories/6_btn_single_breasted.png'},
                                    {id: '2', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/images/blank.png'},
                                    {id: '3', name: 'Inside', img: basePath + 'pub/media/shirt-tool/jacket_img/images/Jacket_slim_back_final.png'}
                                ]
                            }
                        ]},
                    {"id": "3", "name": "Shawl", "parent": "Lapel", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Vest_Shawl.png", "prev": "", "width": [
                            {
                                id: '1',
                                name: 'Slim',
                                img: basePath + 'pub/media/shirt-tool/jacket_img/Lapel_Width_Narrow.png',
                                live: [
                                    {id: '1', name: 'Front', img: basePath + 'pub/media/shirt-tool/jacket_img/accessories/6_btn_single_breasted.png'},
                                    {id: '2', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/images/blank.png'},
                                    {id: '3', name: 'Inside', img: basePath + 'pub/media/shirt-tool/jacket_img/images/Jacket_slim_back_final.png'}
                                ]
                            },
                            {
                                id: '2',
                                name: 'Standard',
                                img: basePath + 'pub/media/shirt-tool/jacket_img/Lapel_Width_Wide.png',
                                live: [
                                    {id: '1', name: 'Front', img: basePath + 'pub/media/shirt-tool/jacket_img/accessories/6_btn_single_breasted.png'},
                                    {id: '2', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/images/blank.png'},
                                    {id: '3', name: 'Inside', img: basePath + 'pub/media/shirt-tool/jacket_img/images/Jacket_slim_back_final.png'}
                                ]
                            }
                        ]}
                ]},
            {"id": "5", "name": "Pocket", img: basePath + "media/jacket_img/thumbnail/Breast_Pocket.png", "flag": "false", "target": "0", "style": [
                    {"id": "1", "name": "Without Breast Pocket", "parent": "Pocket", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Without_Breast_Pocket.png", "prev": "", "live": [
                            {id: '1', name: 'Front', img: basePath + 'pub/media/shirt-tool/jacket_img/accessories/3_btn_std_sleeve.png'},
                            {id: '2', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/accessories/blank.png'},
                            {id: '3', name: 'Inside', img: basePath + 'pub/media/shirt-tool/jacket_img/images/Jacket_slim_back_final.png'}
                        ]},
                    {"id": "2", "name": "Breast_Pocket.png", "parent": "Pocket", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Breast_Pocket.png", "prev": "", "live": [
                            {id: '1', name: 'Front', img: basePath + 'pub/media/shirt-tool/jacket_img/accessories/3_btn_std_sleeve.png'},
                            {id: '2', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/accessories/blank.png'},
                            {id: '3', name: 'Inside', img: basePath + 'pub/media/shirt-tool/jacket_img/images/Jacket_slim_back_final.png'}
                        ]}

                ]},
            {"id": "5", "name": "Bottom", img: basePath + "media/jacket_img/thumbnail/00_Bottom_Main_Icon.png", "flag": "false", "target": "0", "style": [
                    {"id": "1", "name": "Diagonal", "parent": "Pocket", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Bottom_Diagonal.png", "prev": "", "live": [
                            {id: '1', name: 'Front', img: basePath + 'pub/media/shirt-tool/jacket_img/accessories/3_btn_std_sleeve.png'},
                            {id: '2', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/accessories/blank.png'},
                            {id: '3', name: 'Inside', img: basePath + 'pub/media/shirt-tool/jacket_img/images/Jacket_slim_back_final.png'}
                        ]},
                    {"id": "2", "name": "Rounded", "parent": "Pocket", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Bottom_Rounded.png", "prev": "", "live": [
                            {id: '1', name: 'Front', img: basePath + 'pub/media/shirt-tool/jacket_img/accessories/3_btn_std_sleeve.png'},
                            {id: '2', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/accessories/blank.png'},
                            {id: '3', name: 'Inside', img: basePath + 'pub/media/shirt-tool/jacket_img/images/Jacket_slim_back_final.png'}
                        ]},
                    {"id": "2", "name": "Straight", "parent": "Pocket", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Bottom_Straight.png", "prev": "", "live": [
                            {id: '1', name: 'Front', img: basePath + 'pub/media/shirt-tool/jacket_img/accessories/3_btn_std_sleeve.png'},
                            {id: '2', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/accessories/blank.png'},
                            {id: '3', name: 'Inside', img: basePath + 'pub/media/shirt-tool/jacket_img/images/Jacket_slim_back_final.png'}
                        ]}

                ]}
        ];
        return{
            getVestData: function (e) {
                return vestDesign;
            }
        }
        /*return {
         getVestData: function(genderId, productId) {
         
         return $http.post(basePath + 'custom_ajax/get_suit_vest.php', {
         genderId: genderId,
         productId: productId
         });
         }
         };*/
    });
    angular.module("Shirt").factory("pantService", function ($http) {

        var pants = [
            {"id": "1", "name": "Fit", "flag": "false", img: basePath + "media/jacket_img/thumbnail/00_Pant_Main_Icon.png", "target": "0", "style": [
                    {"id": "1", "name": "Regular", "parent": "Fit", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Pant_Regular_Fit.png", "prev": "", "live": [
                            {id: '1', name: 'Front', img: basePath + ''},
                            {id: '2', name: 'Back', img: basePath + ''},
                            {id: '3', name: 'Inside', img: basePath + ''}
                        ]},
                    {"id": "2", "name": "Slim", "parent": "Fit", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Pant_Slim_Fit.png", "prev": "", "live": [
                            {id: '1', name: 'Front', img: basePath + ''},
                            {id: '2', name: 'Back', img: basePath + ''},
                            {id: '3', name: 'Inside', img: basePath + ''}
                        ]}
                ]},
            {"id": "2", "name": "Pleats", "flag": "false", img: basePath + "media/jacket_img/thumbnail/00_Front_Main_Icon.png", "target": "0", "style": [
                    {"id": "1", "name": "No Pleats", "parent": "Style", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Front_No_Pleats.png", "prev": "", "live": [
                            {id: '1', name: 'Front', img: basePath + 'pub/media/shirt-tool/jacket_img/accessories/1_btn_single_breasted.png'},
                            {id: '2', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/images/blank.png'},
                            {id: '3', name: 'Inside', img: basePath + 'pub/media/shirt-tool/jacket_img/images/Jacket_slim_back_final.png'}
                        ]},
                    {"id": "2", "name": "Single Pleat", "parent": "Style", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Front_Single_Pleat.png", "prev": "", "live": [
                            {id: '1', name: 'Front', img: basePath + 'pub/media/shirt-tool/jacket_img/accessories/2_btn_single_breasted.png'},
                            {id: '2', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/images/blank.png'},
                            {id: '3', name: 'Inside', img: basePath + 'pub/media/shirt-tool/jacket_img/images/Jacket_slim_back_final.png'}
                        ]},
                    {"id": "3", "name": "Double Pleats", "parent": "Style", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Front_Double_Pleats.png", "prev": "", "live": [
                            {id: '1', name: 'Front', img: basePath + 'pub/media/shirt-tool/jacket_img/accessories/3_btn_single_breasted.png'},
                            {id: '2', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/images/blank.png'},
                            {id: '3', name: 'Inside', img: basePath + 'pub/media/shirt-tool/jacket_img/images/Jacket_slim_back_final.png'}
                        ]}
                ]},
            {"id": "3", "name": "Fastening", img: basePath + "media/jacket_img/thumbnail/00_Fastening_Main_Icon.png", "flag": "false", "target": "0", "style": [
                    {"id": "1", "name": "Centered", "parent": "Sleeve Button", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Fastening_Centered.png", "prev": "media/jacket_img/preview/01_Three_Standard_Buttons.png", "live": [
                            {id: '1', name: 'Front', img: basePath + 'pub/media/shirt-tool/jacket_img/accessories/3_btn_std_sleeve.png'},
                            {id: '2', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/accessories/blank.png'}
                        ]},
                    {"id": "1", "name": "Off-centered", "parent": "Sleeve Button", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Fastening_Centered_Buttonless.png", "prev": "media/jacket_img/preview/01_Three_Standard_Buttons.png", "live": [
                            {id: '1', name: 'Front', img: basePath + 'pub/media/shirt-tool/jacket_img/accessories/3_btn_std_sleeve.png'},
                            {id: '2', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/accessories/blank.png'}
                        ]},
                    {"id": "2", "name": "No Button", "parent": "Sleeve Button", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Fastening_Off_Centered.png", "prev": "media/jacket_img/preview/02_Four_Standard_Buttons.png", "live": [
                            {id: '1', name: 'Front', img: basePath + 'pub/media/shirt-tool/jacket_img/accessories/4_btn_std_sleeve.png'},
                            {id: '2', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/accessories/blank.png'}
                        ]},
                    {"id": "3", "name": "Off-centered: Buttonless", "parent": "Sleeve Button", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Fastening_Off_Centered_Buttonless.png", "prev": "media/jacket_img/preview/03_Five_Standard_Buttons.png", "live": [
                            {id: '1', name: 'Front', img: basePath + 'pub/media/shirt-tool/jacket_img/accessories/5_btn_std_sleeve.png'},
                            {id: '2', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/accessories/blank.png'}
                        ]}
                ]},
            {"id": "4", "name": "Pockets Back", img: basePath + "media/jacket_img/thumbnail/Back_Pocket_Flaps.png", "flag": "false", "target": "1", "style": [
                    {"id": "1", "name": "Pocket Flaps", "parent": "Vents", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Back_Pocket_Flaps.png", "prev": "media/jacket_img/Vents_No_Vents.png", "live": [
                            {id: '1', name: 'Front', img: basePath + 'pub/media/shirt-tool/jacket_img/images/blank.png'},
                            {id: '2', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/images/blank.png'},
                            {id: '2', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/images/blank.png'}
                        ]},
                    {"id": "2", "name": "Pocket Patched", "parent": "Vents", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Back_Pocket_Patched.png", "prev": "media/jacket_img/Vents_Center_Vents.png", "live": [
                            {id: '1', name: 'Front', img: basePath + 'pub/media/shirt-tool/jacket_img/images/blank.png'},
                            {id: '2', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/images/blank.png'},
                            {id: '2', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/images/blank.png'}
                        ]},
                    {"id": "3", "name": "Pocket Welted With Buttons", "parent": "Vents", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Back_Pocket_Welted_With_Buttons.png", "prev": "media/jacket_img/Vents_Side.png", "live": [
                            {id: '1', name: 'Front', img: basePath + 'pub/media/shirt-tool/jacket_img/images/blank.png'},
                            {id: '2', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/accessories/jacket_vent.png'},
                            {id: '2', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/images/blank.png'}
                        ]}
                ]},
            {"id": "5", "name": "Pockets Front", img: basePath + "media/jacket_img/thumbnail/00_Pockets_Main_icon.png", "flag": "false", "target": "1", "style": [
                    {"id": "1", "name": "Rounded", "parent": "Vents", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Front_Pockets_Rounded.png", "prev": "media/jacket_img/Vents_No_Vents.png", "live": [
                            {id: '1', name: 'Front', img: basePath + 'pub/media/shirt-tool/jacket_img/images/blank.png'},
                            {id: '2', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/images/blank.png'},
                            {id: '2', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/images/blank.png'}
                        ]},
                    {"id": "2", "name": "Slanted", "parent": "Vents", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Front_Pockets_Slanted.png", "prev": "media/jacket_img/Vents_Center_Vents.png", "live": [
                            {id: '1', name: 'Front', img: basePath + 'pub/media/shirt-tool/jacket_img/images/blank.png'},
                            {id: '2', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/images/blank.png'},
                            {id: '2', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/images/blank.png'}
                        ]},
                    {"id": "3", "name": "Straignt", "parent": "Vents", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Front_Pockets_Straignt.png", "prev": "media/jacket_img/Vents_Side.png", "live": [
                            {id: '1', name: 'Front', img: basePath + 'pub/media/shirt-tool/jacket_img/images/blank.png'},
                            {id: '2', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/accessories/jacket_vent.png'},
                            {id: '2', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/images/blank.png'}
                        ]}
                ]},
            {"id": "6", "name": "Cuffs", img: basePath + "media/jacket_img/thumbnail/00_Cuffs_Main_Icon.png", "flag": "false", "target": "1", "style": [
                    {"id": "1", "name": "No Cuffs", "parent": "Vents", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/No_Cuffs.png", "prev": "media/jacket_img/Vents_No_Vents.png", "live": [
                            {id: '1', name: 'Front', img: basePath + 'pub/media/shirt-tool/jacket_img/images/blank.png'},
                            {id: '2', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/images/blank.png'},
                            {id: '2', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/images/blank.png'}
                        ]},
                    {"id": "2", "name": "With Cuffs", "parent": "Vents", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/With_Cuffs.png", "prev": "media/jacket_img/Vents_Center_Vents.png", "live": [
                            {id: '1', name: 'Front', img: basePath + 'pub/media/shirt-tool/jacket_img/images/blank.png'},
                            {id: '2', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/images/blank.png'},
                            {id: '2', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/images/blank.png'}
                        ]}
                ]},
            {"id": "7", "name": "Pleats", img: basePath + "media/jacket_img/thumbnail/Front_Double_Pleats.png", "flag": "false", "target": "1", "style": [
                    {"id": "1", "name": "Double Pleats", "parent": "Vents", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Front_Double_Pleats.png", "prev": "media/jacket_img/Vents_No_Vents.png", "live": [
                            {id: '1', name: 'Front', img: basePath + 'pub/media/shirt-tool/jacket_img/images/blank.png'},
                            {id: '2', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/images/blank.png'},
                            {id: '2', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/images/blank.png'}
                        ]},
                    {"id": "2", "name": "No Pleats", "parent": "Vents", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Front_No_Pleats.png", "prev": "media/jacket_img/Vents_Center_Vents.png", "live": [
                            {id: '1', name: 'Front', img: basePath + 'pub/media/shirt-tool/jacket_img/images/blank.png'},
                            {id: '2', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/images/blank.png'},
                            {id: '2', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/images/blank.png'}
                        ]},
                    {"id": "3", "name": "Single Pleats", "parent": "Vents", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Front_Single_Pleat.png", "prev": "media/jacket_img/Vents_Side.png", "live": [
                            {id: '1', name: 'Front', img: basePath + 'pub/media/shirt-tool/jacket_img/images/blank.png'},
                            {id: '2', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/accessories/jacket_vent.png'},
                            {id: '2', name: 'Back', img: basePath + 'pub/media/shirt-tool/jacket_img/images/blank.png'}
                        ]}
                ]},
        ];
        return{
            getPantData: function (e) {
                return pants;
            }
        }

        /*return {
         getPantData: function(genderId, productId) {
         
         return $http.post(basePath + 'custom_ajax/get_suit_pant.php', {
         genderId: genderId,
         productId: productId
         });
         }
         };*/
    });
    angular.module("Shirt").factory("fabricAllDataService", function ($http) {

        var fabricAllData = [
            {"id": "1", "name": "Fabric 1", img: basePath + "media/jacket_img/all_fabric_images/01.png", "target": "", "style": [
                    {"id": "1", "name": "Violet", "code": "AA1", "price": "134", img: basePath + "media/jacket_img/all_fabric_images/01.png", "color": "Violet Light", "pattern": "Plain"}

                ]},
            {"id": "2", "name": "Fabric 2", img: basePath + "media/jacket_img/all_fabric_images/02.png", "target": "", "style": [
                    {"id": "21", "name": "Blue1", "code": "AA3", "price": "130", img: basePath + "media/jacket_img/all_fabric_images/02.png", "color": "Blue", "pattern": "Plain"}
                ]},
            {"id": "3", "name": "Fabric 3", img: basePath + "media/jacket_img/all_fabric_images/03.png", "target": "", "style": [
                    {"id": "31", "name": "Blue2", "code": "AA4", "price": "130", img: basePath + "media/jacket_img/all_fabric_images/03.png", "color": "Blue", "pattern": "Plain"}
                ]},
            {"id": "4", "name": "Fabric 4", img: basePath + "media/jacket_img/all_fabric_images/04.png", "target": "", "style": [
                    {"id": "41", "name": "Blue3", "code": "AA6", "price": "130", img: basePath + "media/jacket_img/all_fabric_images/04.png", "color": "Blue", "pattern": "Plain"}
                ]},
            {"id": "5", "name": "Fabric 5", img: basePath + "media/jacket_img/all_fabric_images/05.png", "target": "", "style": [
                    {"id": "51", "name": "Blue4", "code": "AA3", "price": "130", img: basePath + "media/jacket_img/all_fabric_images/05.png", "color": "Blue", "pattern": "Plain"}
                ]},
            {"id": "6", "name": "Fabric 6", img: basePath + "media/jacket_img/all_fabric_images/06.png", "target": "", "style": [
                    {"id": "61", "name": "Blue5", "code": "AA3", "price": "130", img: basePath + "media/jacket_img/all_fabric_images/06.png", "color": "Blue", "pattern": "Plain"}
                ]},
            {"id": "7", "name": "Fabric 6", img: basePath + "media/jacket_img/all_fabric_images/07.png", "target": "", "style": [
                    {"id": "71", "name": "Blue6", "code": "AA3", "price": "130", img: basePath + "media/jacket_img/all_fabric_images/07.png", "color": "Blue", "pattern": "Plain"}
                ]},
            {"id": "8", "name": "Fabric 7", img: basePath + "media/jacket_img/all_fabric_images/08.png", "target": "", "style": [
                    {"id": "81", "name": "Blue7", "code": "AA3", "price": "130", img: basePath + "media/jacket_img/all_fabric_images/08.png", "color": "Blue", "pattern": "Plain"}
                ]},
            {"id": "9", "name": "Fabric 8", img: basePath + "media/jacket_img/all_fabric_images/09.png", "target": "", "style": [
                    {"id": "91", "name": "Blue8", "code": "AA3", "price": "130", img: basePath + "media/jacket_img/all_fabric_images/09.png", "color": "Blue", "pattern": "Plain"}
                ]},
            {"id": "10", "name": "Fabric 9", img: basePath + "media/jacket_img/all_fabric_images/10.png", "target": "", "style": [
                    {"id": "101", "name": "Blue9", "code": "AA3", "price": "130", img: basePath + "media/jacket_img/all_fabric_images/10.png", "color": "Blue", "pattern": "Plain"}
                ]},
            {"id": "11", "name": "Fabric 10", img: basePath + "media/jacket_img/all_fabric_images/11.png", "target": "", "style": [
                    {"id": "111", "name": "Blue10", "code": "AA3", "price": "130", img: basePath + "media/jacket_img/all_fabric_images/11.png", "color": "Blue", "pattern": "Plain"}
                ]},
            {"id": "12", "name": "Fabric 11", img: basePath + "media/jacket_img/all_fabric_images/12.png", "target": "", "style": [
                    {"id": "121", "name": "Blue611", "code": "AA3", "price": "130", img: basePath + "media/jacket_img/all_fabric_images/12.png", "color": "Blue", "pattern": "Plain"}
                ]},
            {"id": "13", "name": "Fabric 12", img: basePath + "media/jacket_img/all_fabric_images/13.png", "target": "", "style": [
                    {"id": "131", "name": "Blue", "code": "AA3", "price": "130", img: basePath + "media/jacket_img/all_fabric_images/13.png", "color": "Blue", "pattern": "Plain"}
                ]},
            {"id": "14", "name": "Fabric 13", img: basePath + "media/jacket_img/all_fabric_images/14.png", "target": "", "style": [
                    {"id": "141", "name": "Blue", "code": "AA3", "price": "130", img: basePath + "media/jacket_img/all_fabric_images/14.png", "color": "Blue", "pattern": "Plain"}
                ]},
            {"id": "15", "name": "Fabric 14", img: basePath + "media/jacket_img/all_fabric_images/15.png", "target": "", "style": [
                    {"id": "151", "name": "Blue", "code": "AA3", "price": "130", img: basePath + "media/jacket_img/all_fabric_images/15.png", "color": "Blue", "pattern": "Plain"}
                ]},
            {"id": "16", "name": "Fabric 15", img: basePath + "media/jacket_img/all_fabric_images/16.png", "target": "", "style": [
                    {"id": "161", "name": "Blue", "code": "AA3", "price": "130", img: basePath + "media/jacket_img/all_fabric_images/16.png", "color": "Blue", "pattern": "Plain"}
                ]},
            {"id": "17", "name": "Fabric 16", img: basePath + "media/jacket_img/all_fabric_images/17.png", "target": "", "style": [
                    {"id": "171", "name": "Blue", "code": "AA3", "price": "130", img: basePath + "media/jacket_img/all_fabric_images/17.png", "color": "Blue", "pattern": "Plain"}
                ]},
            {"id": "18", "name": "Fabric 17", img: basePath + "media/jacket_img/all_fabric_images/18.png", "target": "", "style": [
                    {"id": "181", "name": "Blue", "code": "AA3", "price": "130", img: basePath + "media/jacket_img/all_fabric_images/18.png", "color": "Blue", "pattern": "Plain"}
                ]},
            {"id": "19", "name": "Fabric 18", img: basePath + "media/jacket_img/all_fabric_images/19.png", "target": "", "style": [
                    {"id": "191", "name": "Blue", "code": "AA3", "price": "130", img: basePath + "media/jacket_img/all_fabric_images/19.png", "color": "Blue", "pattern": "Plain"}
                ]},
            {"id": "20", "name": "Fabric 19", img: basePath + "media/jacket_img/all_fabric_images/20.png", "target": "", "style": [
                    {"id": "201", "name": "Blue", "code": "AA3", "price": "130", img: basePath + "media/jacket_img/all_fabric_images/20.png", "color": "Blue", "pattern": "Plain"}
                ]}
        ];
        //staic way
        /* return {
         getFabricData: function (e) {
         return fabrics;
         
         }
         };*/
        //dynamic way
        return {
            getFabricAllDesignData: function (genderId, productId) {

                return $http.post(basePath + 'custom_ajax/get_fabric_all_data.php', {
                    genderId: genderId,
                    productId: productId
                });
            }
        };


    });


    angular.module("Shirt").factory("suitAccentService", function ($http) {

        //dynamic way
        return {
            getSuitAccentData: function (genderId, productId) {

                return $http.post(basePath + 'men-shirt/index/getShirtAccent', {
                    genderId: genderId,
                    productId: productId
                });
            }
        };
    });


    angular.module("Shirt").factory("measurementDataService", function ($http) {

        var measurementViewDataSuit = [
            {id: '1', name: 'viewM1', viewimages: [
                    {id: '1', rel: 'Body', label: "", type: 'jacket', name: 'Man', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/Body_Measurement_images/2/Char_2_1_View_01.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '2', rel: 'Suit', label: "", type: 'jacket', name: 'Man', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/Fabric_Measurement_images/Fabric_01_1_View_01.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                ]},
            {id: '2', name: 'viewM2', viewimages: [
                    {id: '1', rel: 'Body', label: "", type: 'jacket', name: 'Man', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/Body_Measurement_images/2/Char_2_1_View_02.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                    {id: '2', rel: 'Suit', label: "", type: 'jacket', name: 'Man', classname: 'shirt_part part', img: basePath + 'pub/media/shirt-tool/Fabric_Measurement_images/Fabric_01_1_View_02.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                ]}
        ];

        var fabrics = [
            {"id": "1", "name": "Black", img: basePath + "pub/media/shirt-tool/tool_data/img/01.png", "price": "134", "target": "", "style": [
                    {"id": "1", "name": "Violet", "code": "AA1", "price": "134", img: basePath + "pub/media/shirt-tool/tool_data/img/01.png", "color": "Violet Light", "pattern": "Plain"}

                ]},
            {"id": "2", "name": "Gray", img: basePath + "pub/media/shirt-tool/tool_data/img/02.png", "price": "134", "target": "", "style": [
                    {"id": "21", "name": "Blue1", "code": "AA3", "price": "130", img: basePath + "pub/media/shirt-tool/tool_data/img/02.png", "color": "Blue", "pattern": "Plain"}
                ]},
            {"id": "3", "name": "Bourne", img: basePath + "pub/media/shirt-tool/tool_data/img/03.png", "price": "134", "target": "", "style": [
                    {"id": "31", "name": "Blue2", "code": "AA4", "price": "130", img: basePath + "pub/media/shirt-tool/tool_data/img/03.png", "color": "Blue", "pattern": "Plain"}
                ]},
            {"id": "4", "name": "Dark Green", img: basePath + "pub/media/shirt-tool/tool_data/img/04.png", "price": "134", "target": "", "style": [
                    {"id": "41", "name": "Blue3", "code": "AA6", "price": "130", img: basePath + "pub/media/shirt-tool/tool_data/img/04.png", "color": "Blue", "pattern": "Plain"}
                ]},
            {"id": "5", "name": "Aliai Dark Red", img: basePath + "pub/media/shirt-tool/tool_data/img/05.png", "price": "134", "target": "", "style": [
                    {"id": "51", "name": "Blue4", "code": "AA3", "price": "130", img: basePath + "pub/media/shirt-tool/tool_data/img/05.png", "color": "Blue", "pattern": "Plain"}
                ]}
        ];



        var measurementBackgrounds = [
            {"id": "0", "name": "No Background", img: basePath + "media/background_images/0.png", "price": "50", "target": "", "style": [
                    {"id": "0", "name": "No Background", "code": "AA1", "price": "0", img: basePath + "media/background_images/0.png", "color": "Violet Light", "pattern": "Plain"}

                ]}, {"id": "1", "name": "Background-1", img: basePath + "media/background_images/1.png", "price": "51", "target": "", "style": [
                    {"id": "1", "name": "Violet", "code": "AA1", "price": "134", img: basePath + "media/background_images/1.png", "color": "Violet Light", "pattern": "Plain"}

                ]},
            {"id": "2", "name": "Background-1", img: basePath + "media/background_images/2.png", "price": "60", "target": "", "style": [
                    {"id": "2", "name": "Blue1", "code": "AA3", "price": "130", img: basePath + "media/background_images/2.png", "color": "Blue", "pattern": "Plain"}
                ]},
            {"id": "3", "name": "Background-3", img: basePath + "media/background_images/3.png", "price": "50", "target": "", "style": [
                    {"id": "3", "name": "Blue2", "code": "AA4", "price": "130", img: basePath + "media/background_images/3.png", "color": "Blue", "pattern": "Plain"}
                ]},
            {"id": "4", "name": "Background-4", img: basePath + "media/background_images/4.png", "price": "50", "target": "", "style": [
                    {"id": "4", "name": "Blue3", "code": "AA6", "price": "130", img: basePath + "media/background_images/4.png", "color": "Blue", "pattern": "Plain"}
                ]},
            {"id": "5", "name": "Background-5", img: basePath + "media/background_images/5.png", "price": "50", "target": "", "style": [
                    {"id": "5", "name": "Blue4", "code": "AA3", "price": "130", img: basePath + "media/background_images/5.png", "color": "Blue", "pattern": "Plain"}
                ]},
            {"id": "6", "name": "Background-6", img: basePath + "media/background_images/6.png", "price": "50", "target": "", "style": [
                    {"id": "7", "name": "Blue4", "code": "AA3", "price": "130", img: basePath + "media/background_images/6.png", "color": "Blue", "pattern": "Plain"}
                ]},
            {"id": "7", "name": "Background-7", img: basePath + "media/background_images/7.png", "price": "50", "target": "", "style": [
                    {"id": "7", "name": "Blue4", "code": "AA3", "price": "130", img: basePath + "media/background_images/7.png", "color": "Blue", "pattern": "Plain"}
                ]},
            {"id": "8", "name": "Background-8", img: basePath + "media/background_images/8.png", "price": "50", "target": "", "style": [
                    {"id": "8", "name": "Blue4", "code": "AA3", "price": "130", img: basePath + "media/background_images/8.png", "color": "Blue", "pattern": "Plain"}
                ]},
            {"id": "9", "name": "Background-9", img: basePath + "media/background_images/9.png", "price": "50", "target": "", "style": [
                    {"id": "9", "name": "Blue4", "code": "AA3", "price": "130", img: basePath + "media/background_images/9.png", "color": "Blue", "pattern": "Plain"}
                ]},
            {"id": "10", "name": "Background-10", img: basePath + "media/background_images/10.png", "price": "50", "target": "", "style": [
                    {"id": "10", "name": "Blue4", "code": "AA3", "price": "130", img: basePath + "media/background_images/10.png", "color": "Blue", "pattern": "Plain"}
                ]}
        ];



        var custSizeJacktData = [
            {id: '1', name: 'Slim', chest: '20.00-40.00', waist: '20.00-40.00', hip: "20.00-40.00", shoulder: "20.00-40.00", sleeve: "15.00-30.00", length: "15.00-30.00"},
            {id: '2', name: 'Fit', chest: '40.00-60.00', waist: '40.00-60.00', hip: "40.00-60.00", shoulder: "40.00-60.00", sleeve: "30.00-45.00", length: "30.00-45.00"},
            {id: '3', name: 'XL', chest: '60.00-80.00', waist: '60.00-80.00', hip: "60.00-80.00", shoulder: "60.00-80.00", sleeve: "45.00-60.00", length: "45.00-60.00"},
        ];

        var custSizeJacktDataCm = [
            {id: '1', name: 'Slim', chest: '50.8-101.6', waist: '50.8-101.6', hip: "50.8-101.6", shoulder: "50.8-101.6", sleeve: "38.1-76.2", length: "38.1-76.2"},
            {id: '2', name: 'Fit', chest: '101.6-152.4', waist: '101.6-152.4', hip: "101.6-152.4", shoulder: "101.6-152.4", sleeve: "76.2-114.3", length: "76.2-114.3"},
            {id: '3', name: 'XL', chest: '152.4-203.2', waist: '152.4-203.2', hip: "152.4-203.2", shoulder: "152.4-203.2", sleeve: "114.3-152.4", length: "114.3-152.4"},
        ];





        var custSizePantData = [
            {id: '1', name: 'Slim', waist: '20.00-35.00', hip: "20.00-35.00", crotch: "20.00-25.00", thigh: "15.00-20.00", length: "15.00-25.00"},
            {id: '2', name: 'Fit', waist: '35.00-45.00', hip: "35.00-45.00", crotch: "25.00-30.00", thigh: "20.00-30.00", length: "20.00-30.00"},
            {id: '3', name: 'XL', waist: '45.00-60.00', hip: "45.00-65.00", crotch: "30.00-40.00", thigh: "30.00-40.00", length: "30.00-45.00"},
        ];



        var custSizePantDataCm = [
            {id: '1', name: 'Slim', waist: '50.8-88.9', hip: "50.8-88.9", crotch: "50.8-63.5", thigh: "38.1-50.8", length: "38.1-63.5"},
            {id: '2', name: 'Fit', waist: '88.9-114.3', hip: "88.9-114.3", crotch: "63.5-76.2", thigh: "63.5-76.2", length: "63.5-76.2"},
            {id: '3', name: 'XL', waist: '114.3-152.4', hip: "114.3-165.1", crotch: "76.2-101.6", thigh: "76.2-101.6", length: "76.2-114.3"},
        ];

        var staticBodies = [
            {"id": "2", "name": "Ramesh", img: basePath + "media/default_tool_measurement_img/character_01.png", "target": "", "style": [
                    {"id": "2", "name": "Violet", "code": "AA1", "price": "134", img: basePath + "media/default_tool_measurement_img/character_01.png", "color": "Violet Light", "pattern": "Plain"}

                ]},
            {"id": "1", "name": "Dinesh", img: basePath + "media/default_tool_measurement_img/character_02.png", "target": "", "style": [
                    {"id": "1", "name": "Blue1", "code": "AA3", "price": "130", img: basePath + "media/default_tool_measurement_img/character_02.png", "color": "Blue", "pattern": "Plain"}
                ]},
            {"id": "3", "name": "Suresh", img: basePath + "media/default_tool_measurement_img/character_03.png", "target": "", "style": [
                    {"id": "3", "name": "Blue2", "code": "AA4", "price": "130", img: basePath + "media/default_tool_measurement_img/character_03.png", "color": "Blue", "pattern": "Plain"}
                ]}
        ];


        //measurment sizes

        /* var jacketSizes = [
         {"id": "19", "product_type": "1", "gender": 1, "size_name": "Slim", "chest": "15.0", "waist": "36"
         , "hip": "38", "shoulder": "38", "sleeve": "38", "length": "38", "size_order": "1"},
         {"id": "20", "product_type": "1", "gender": 1, "size_name": "Classic", "chest": "15.0", "waist": "36"
         , "hip": "38", "shoulder": "38", "sleeve": "38", "length": "38", "size_order": "2"},
         {"id": "21", "product_type": "1", "gender": 1, "size_name": "XL", "chest": "15.0", "waist": "36"
         , "hip": "38", "shoulder": "38", "sleeve": "38", "length": "38", "size_order": "3"}
         ];*/


        var jacketSizes = [
            {"id": "19", "product_type": "1", "gender": 1, "size_name": "Slim", "chest": "89", "waist": "37.5"
                , "hip": "38", "shoulder": "39", "sleeve": "38", "length": "38", "size_order": "1"},
            {"id": "20", "product_type": "1", "gender": 1, "size_name": "Classic", "chest": "96.5", "waist": "40"
                , "hip": "38", "shoulder": "38", "sleeve": "38", "length": "38", "size_order": "2"},
            {"id": "21", "product_type": "1", "gender": 1, "size_name": "XL", "chest": "112", "waist": "42"
                , "hip": "38", "shoulder": "38", "sleeve": "38", "length": "38", "size_order": "3"}
        ];

        /*var pantSizes = [
         {"id": "19", "product_type": "1", "gender": 1, "size_name": "Slim", "waist": "36"
         , "hip": "38", "crotch": "38", "thigh": "38", "length": "38", "size_order": "1"},
         {"id": "20", "product_type": "1", "gender": 1, "size_name": "Classic", "waist": "36"
         , "hip": "38", "crotch": "38", "thigh": "38", "length": "38", "size_order": "2"},
         {"id": "21", "product_type": "1", "gender": 1, "size_name": "XL", "waist": "36"
         , "hip": "38", "crotch": "38", "thigh": "38", "length": "38", "size_order": "3"}
         ];*/


        return {
            getMeasurementBackrounds: function (e) {
                return measurementBackgrounds;

            },
            getMeasurementFabrics: function (e) {
                return fabrics;

            },
            getCustSizeJacktData: function (e) {
                return custSizeJacktData;
            },
            getCustSizePantData: function (e) {
                return custSizePantData;
            },
            getCustSizeJacktDataCm: function (e) {
                return custSizeJacktDataCm;
            },
            getCustSizePantDataCm: function (e) {
                return custSizePantDataCm;
            },
            getMeasurementSuitView: function () {
                return measurementViewDataSuit;
            },
            getMeasurementBodies: function () {
                return staticBodies;
            },
            getJacketSizes: function () {
                return jacketSizes;
            },
            /* getPantSizes: function() {
             return pantSizes;
             }*/
        }
    });
});