/********************************************************************************
 * This script is brought to you by Vasplus Programming Blog
 * Website: www.vasplus.info
 * Email: info@vasplus.info
 *********************************************************************************/

//This does the file Uploads

jQuery(document).ready(function ()
{

    //jQuery('input[type="file"]').on('click', function(e){
    // code here

    var uploaded_files_location = "customized-images/uploaded_files";
    new AjaxUpload(jQuery('#browse-image'),
            {
                action: jQuery("#basePath").val() + 'custom_ajax/uploader.php',
                name: 'file_to_upload',
                onSubmit: function (file, file_extensions)
                {




                    //This is the main wrapper for the uploaded items which is hidden by default

                    //Allowed file formats jpg|png|jpeg|gif|pdf|docx|doc|rtf|txt - You can add as more file formats or remove as you wish.
                    if (!(file_extensions && /^(jpg|png|jpeg|gif|ai|eps|tiff|bmp|svg|pdf)$/.test(file_extensions)))
                    {
                        //If file format is not allowed then, display an error message to the user
                        alert("Sorry, you can only upload the following file formats:  JPG,PNG,JPEG,GIF,EPS,BMP,AI. Thanks...");
                        jQuery(".preloader").hide();
                        jQuery('.vpb_main_demo_wrapper').hide();
                        return false;
                    }
                    else
                    {
                        jQuery('#vpb_uploads_error_displayer').html('<div class="uplading_image">Uploading <img src="pub/media/shirt-tool/shirt-tool/image/loadings.gif" align="absmiddle" /></div>');
                        jQuery(".preloader").hide();
                        jQuery('.vpb_main_demo_wrapper').hide();
                        return true;
                    }
                },
                onComplete: function (file, response)
                {



                    //event.stopPropagation();
                    if (response === "file_uploaded_successfully")
                    {
                        jQuery('#vpb_uploads_error_displayer').html(''); //Empty the error message box				
                        //Check the type of file uploaded and display it rightly on the screen to the user and that's cool man
                        var type_of_file_uploaded = file.substring(file.lastIndexOf('.') + 1); //Get files extensions

                        var files_name_without_extensions = file.substr(0, file.lastIndexOf('.')) || file;
                        vpb_file_names = files_name_without_extensions.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '');

                        if (type_of_file_uploaded == "gif" || type_of_file_uploaded == "GIF" || type_of_file_uploaded == "JPEG" || type_of_file_uploaded == "jpeg" || type_of_file_uploaded == "jpg" || type_of_file_uploaded == "JPG" || type_of_file_uploaded == "png" || type_of_file_uploaded == "PNG")
                        {
                             addImage(jQuery("#basePath").val() + uploaded_files_location + '/' + file);
//                            jQuery("#imageHolder").append('<li onclick="addImage(this)" img-src="' + jQuery("#basePath").val() + uploaded_files_location + '/' + file + '"><img  width="100" height="100" src="' + jQuery("#basePath").val() + uploaded_files_location + '/' + file + '"></li>');
                        }


                    }
                    else if (response.substring(response.lastIndexOf('.') + 1) == "png")
                    {

                        var aifile = response;
                        var aifileExt = aifile.substring(aifile.lastIndexOf('.') + 1);
                        if (aifileExt == "png")
                        {
                            var aifile = response;
                            var aifileExt = aifile.substring(aifile.lastIndexOf('.') + 1);
                             addImage(jQuery("#basePath").val() + uploaded_files_location + '/' + aifile);
                            
//                            jQuery("#imageHolder").append('<li onclick="addImage(this)" img-src="'+jQuery("#basePath").val() + uploaded_files_location + '/' + aifile+'"></li>');

                        }

                    }
                    else
                    {
                        alert("Sorry, your file upload was unsuccessful. Please reduce the size of your file and try again or contact this site admin to report this error message if the problem persist. Thanks...");
                    }
                }
            });
//});


});

//This removes all unwanted files
function remove_unwanted_file(id, file)
{
    if (confirm("If you are sure that you really want to remove the file " + file + " then click on OK otherwise, Cancel it."))
    {
        var dataString = "file_to_remove=" + file;
        jQuery.ajax({
            type: "POST",
            url: "remove_unwanted_files.php",
            data: dataString,
            cache: false,
            beforeSend: function ()
            {
                jQuery("#vpb_remove_button" + id).html('<img src="images/loadings.gif" align="absmiddle" />');
            },
            success: function (response)
            {
                jQuery('div#fileID' + id).fadeOut('slow');
            }
        });
    }
    return false;
}
