define(["jquery", "skrollr", "jquery/ui", "scrollIt"], function (jQuery, skrollr) {
    jQuery(document).ready(function () {

        // alert("inside custom js ready");
        fullSize();
        function fullSize() {
            var heights = window.innerHeight;
            jQuery(".fullHt").css('min-height', (heights + 0) + "px");
        }

        fullSize();
        applyOrientation();

        jQuery("#accordian").accordion({
            header: "> div > h3",
            collapsible: true,
            active: false,
            autoHeight: false,
            autoActivate: true
        });

        jQuery('#accordian').accordion("refresh");

        jQuery('#banner-sliders').owlCarousel({
            loop: true,
            margin: 0,
            nav: false,
            autoplay: true,
            animateOut: 'fadeOut',
            animateIn: 'fadeIn',
            items: 1,
            dots: false,
            autoplayTimeout: 6000,
            navText: [
                "<i class='fa fa-angle-left'></i>",
                "<i class='fa fa-angle-right'></i>"
            ],
        })

        jQuery('.drager').click(function () {
            jQuery('body').toggleClass('langfix-open')
            jQuery(this).toggleClass('icone-animation')
        });

        jQuery('.tabs-navs ul li').click(function () {
            jQuery('body').addClass('langfix-five')
        });

        jQuery('.backbtn').click(function () {
            jQuery('body').removeClass('langfix-five')
        });

        jQuery('.drager-one').click(function () {
            jQuery('body').toggleClass('langfix-open-one')
            jQuery(this).toggleClass('icone-animation-one')
        });

        jQuery('.leftPanel').click(function (e) {
            jQuery('body').toggleClass('leftpanel-open')
        });



        jQuery('.rightPanel').click(function (e) {
            jQuery('body').toggleClass('rightpanel-open')
        });



        jQuery('.rightPanel').click(function (e) {
            jQuery('body').removeClass('leftpanel-open')

        });

        jQuery('.leftPanel').click(function (e) {
            jQuery('body').removeClass('rightpanel-open')

        });


        jQuery('.menuBtns').click(function (e) {
            jQuery('body').toggleClass('manupanel-open')
        });



        jQuery(".menuBtn").click(function (e) {
            jQuery("body").toggleClass("menuOpen")
            jQuery(this).toggleClass("crossMenu")
        });







        jQuery(".rightMenuBtn").click(function (e) {

            jQuery("body").removeClass("rightmenuOpen")
        });

        jQuery(".leftMenuBtn").click(function (e) {

            jQuery("body").removeClass("lefrmenuOpen")
        });

        jQuery('[data-toggle="tooltip"]').tooltip({trigger: 'manual'}).tooltip('show');


//    if (jQuery('html').hasClass('desktop')) {
//        //new WOW().init();
//    }


        jQuery.scrollIt({
            upKey: 40, // key code to navigate to the next section
            downKey: 40, // key code to navigate to the previous section
            easing: 'ease-in-out', // the easing function for animation
            scrollTime: 1500, // how long (in ms) the animation takes
            activeClass: 'active', // class given to the active nav element
            onPageChange: null, // function(pageIndex) that is called when page is changed
            topOffset: 0           // offste (in px) for fixed top navigation
        });

        jQuery('#toTop').click(function () {
            jQuery("html, body").animate({scrollTop: 0}, 1500);
            return false;
        });


        jQuery(".topmenu ul li ").bind("click", function () {


            currStyleName = "";

            if (jQuery(window).width() <= 767) {

                jQuery("body").addClass("LeftpanemOpen");
                jQuery(".leftpanelsection").css("left", "300px");
            }


            jQuery(".topmenu ul li ").removeClass("active");

            jQuery(this).addClass("active");
            var type = jQuery(this).find("a").attr("target");
            console.log("type = " + type)

            if (type == "5")
            {
                jQuery("#jacketHide").trigger("click");
                jQuery(".leftMainNav").hide();
                jQuery("#leftMainNav_fabricCat").hide();

                if (jQuery(window).width() > 767) {
                    jQuery('.lining img').data('zoom-image', jQuery('.lining img').attr("data-image")).elevateZoom({
                        responsive: true
                    });

                    jQuery('.fabcolelbow img').data('zoom-image', jQuery('.fabcolelbow img').attr("data-image")).elevateZoom({
                        responsive: true
                    });
                }


            } else if (type == "4")
            {
                jQuery(".leftMainNav").show();
                jQuery(".leftpanelsection .sidebar-options").hide();
                jQuery("#div4").show();

                jQuery(".leftMainNav").hide();

                //leftMainNav_fabricCat
                setTimeout(function () {

                    jQuery("#leftMainNav_fabricCat").show();
                    jQuery("#leftMainNav_fabricCat ul li").removeClass("activeDesign");
                    jQuery("#leftMainNav_fabricCat ul li:first").addClass("activeDesign")
                }, 10);



            } else {
                jQuery(".leftMainNav").show();
                jQuery("#leftMainNav_fabricCat").hide();
            }

            //trigger active view
            jQuery('.view-thumb').find('.active').trigger('click')



            if (jQuery('#div' + type + ' .box_options > div.active').children().length)
            {
                jQuery(".shorttext").show();
            } else {
                jQuery(".shorttext").hide();
            }



        });

        jQuery(".sidebar-options  ul li ").bind("click", function () {
            console.log("innn")
            jQuery(".sidebar-options  ul li ").removeClass("active");
            jQuery(this).addClass("active");

        });

        jQuery('.sidebar-length').hide();
        jQuery('#div1').show();

    });

    function leftMenuBtn(actObj)
    {

        console.log("in left menu")

        jQuery("body").toggleClass("LeftpanemOpen");

        if (jQuery("body").hasClass("LeftpanemOpen")) {

            if (jQuery(window).width() <= 767) {

                jQuery(".LeftpanemOpen .leftpanelsection").css("left", "300px");

            } else {

                jQuery(".LeftpanemOpen .leftpanelsection").css("left", "0px");
            }

        } else {

            jQuery(".leftpanelsection").css("left", "0px");

        }
    }

    jQuery(function () {
        jQuery('.showsingal').click(function () {
            jQuery('.sidebar-length').hide();
            jQuery('.sidebar-options ').removeClass("min");
            jQuery('#div' + jQuery(this).attr('target')).show();
        });


        jQuery('.show-singal').click(function () {
            jQuery('.sidebar-options').addClass("min");
            jQuery('.sidebarlength').removeClass("active");
            jQuery('#divs' + jQuery(this).attr('target')).addClass("active");

        });
        jQuery('.show-singal-accent').click(function () {
            jQuery('.sidebar-options ').addClass("min");
            jQuery('.sidebarlength-accent').removeClass("active");
            jQuery('#diva' + jQuery(this).attr('target')).addClass("active");

        });

        jQuery('.show-singal-pant').click(function () {
            jQuery('.sidebar-options').addClass("min");
            jQuery('.sidebarlength-pant').removeClass("active");
            jQuery('#divpant' + jQuery(this).attr('target')).addClass("active");

        });
    });


    jQuery(window).load(function () {
        if (window.innerWidth > 1024) {
            var s = skrollr.init();
        }
    });

    jQuery(window).resize(function () {
        fullSize();
    });

    jQuery(function () {
        jQuery('[data-toggle="tooltip"]').tooltip()
    })

    function fullSize() {
        var heights = window.innerHeight;
        jQuery(".fullHt").css('min-height', (heights + 0) + "px");
    }

    function applyOrientation() {
        if (window.innerHeight > window.innerWidth) {
            jQuery("body").addClass("potrait");
            jQuery("body").removeClass("landscape");
        } else {
            jQuery("body").addClass("landscape");
            jQuery("body").removeClass("potrait");
        }
    }

    var banner_Ht = window.innerHeight - jQuery('header').innerHeight();
    jQuery(window).scroll(function () {
        var sticky = jQuery('body'),
                scroll = jQuery(window).scrollTop();

        if (scroll >= 100)
            sticky.addClass('sticky-header');
        else
            sticky.removeClass('sticky-header');
    });

    jQuery(function () {
        jQuery('a[title]').tooltip();
    });

    jQuery('body').append('<div id="toTop" class="btn"><span class="fa fa-angle-up"></span></div>');
    jQuery(window).scroll(function () {
        if (jQuery(this).scrollTop() != 0) {
            jQuery('#toTop').fadeIn();
        } else {
            jQuery('#toTop').fadeOut();
        }
    });



    function openNav() {
        document.getElementById("toolpanel-nav").style.top = "0px";
    }

    function closeNav() {
        document.getElementById("toolpanel-nav").style.top = "-95px";
    }
    /*----------piyush---------*/

    function moved() {

        var owl = jQuery(".owl-carousel").data('owlCarousel');
        if (owl.currentItem + 1 === owl.itemsAmount) {

        }
    }



    function changerightMenuBtn(actObj)
    {
        jQuery(".leftMenuBtn").removeClass("RcrossMenu");
        jQuery("body").toggleClass("lefrmenuOpen")
        jQuery(actObj).toggleClass("LcrossMenu")
    }





    function changeleftMenuBtn(actObj) {
        jQuery(".rightMenuBtn").removeClass("LcrossMenu");
        jQuery("body").toggleClass("rightmenuOpen");
        jQuery(actObj).toggleClass("RcrossMenu");
    }

});