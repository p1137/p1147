var dynamicTexture = new THREEx.DynamicTexture(512, 512);
var canvas, ctx;

var monogramSymbols = ["❤", "♡", "♥", "☆", "★", "☚", "☛", "☺", "☹", "☻", "♀", "♂", "♔", "♚", "♕", "♛", "⚓", "⚽"];

var fontsToSelect = [
    {"name": "KBAStitchInTime", "file": "KBAStitchInTime.ttf"},
    {"name": "A Stitch Plus Nine", "file": "A_Stitch_Plus_Nine.ttf"},
    {"name": "AP Applique Stitched", "file": "AP-Applique-Stitched.ttf"},
    {"name": "Bigdey Demo", "file": "Bigdey_Demo.ttf"},
    {"name": "CFIndustrialFabrics-Regular", "file": "CFIndustrialFabrics-Regular.ttf"},
    {"name": "Digidon Demo", "file": "Digidon_Demo.ttf"},
    {"name": "DJB Monogram", "file": "DJB_Monogram.ttf"},
    {"name": "Edyra Demo", "file": "Edyra_Demo.ttf"},
    {"name": "Hytag Demo", "file": "Hytag_Demo.ttf"},
    {"name": "Kingthings Embroidery", "file": "Kingthings_Embroidery.ttf"},
    {"name": "KR His N Hers", "file": "KR_His_N_Hers.ttf"},
    {"name": "MarmeladPersonaUse", "file": "MarmeladPersonaUse.ttf"},
    {"name": "Merveille", "file": "Merveille.ttf"},
    {"name": "Needlework Perfect", "file": "Needlework_Perfect.ttf"},
    {"name": "PORN FASHION trial", "file": "PORN_FASHION_trial.ttf"},
    {"name": "Sajou Fancy Gothic", "file": "Sajou_Fancy_Gothic.ttf"},
    {"name": "Scars before christmas", "file": "Scars_before_christmas.ttf"},
    {"name": "Sofye Regular", "file": "Sofye_Demo.ttf"},
    {"name": "TRACE", "file": "TRACE.TTF"}
];

jQuery(document).ready(function (e) {


    canvas = document.getElementById('canvas');

    ctx = canvas.getContext('2d');

    //load fonts 
    var strFontOptions = '';
    jQuery(fontsToSelect).each(function (indx, ele) {
        strFontOptions += '<option style="font-family:\'' + ele['name'] + '\'" value="' + ele['name'] + '" data-file="' + ele['file'] + '">' + ele['name'] + '</option>';
    })
    jQuery("#font-list").html(strFontOptions);
    //load fonts end

    var strMonoSyms = "";

    jQuery(monogramSymbols).each(function (indx, ele) {
        strMonoSyms += '<li title="Click to add this monogram in text" style="padding:5px;cursor:pointer" onclick="useMonoGram(this)">' + ele + '</li>';
    });

    jQuery("#monogramSyms").html(strMonoSyms);



    jQuery("#font_size").slider({
        range: "max",
        min: 12,
        max: 40,
        value: 35,
        create: function () {
            jQuery("#font_size_handle").text(jQuery(this).slider("value"));
        },
        slide: function (event, ui) {

            jQuery("#font_size_handle").text(ui.value);

            jQuery("#textBoxList option:selected").attr("fontSize", ui.value);

            updateTextImageData();
        }
    });

    jQuery("#font_left").slider({
        range: "max",
        min: -50,
        max: 50,
        value: 0,
        create: function () {
            jQuery("#font_left_handle").text(jQuery(this).slider("value"));
        },
        slide: function (event, ui) {

            var left = parseInt(ui.value);
            var x = parseInt(jQuery("#textBoxList option:selected").attr("org-x"));
            jQuery("#font_left_handle").text(ui.value);

            jQuery("#textBoxList option:selected").attr("x", left + x);
            updateTextImageData();
        }
    });

    jQuery("#font_top").slider({
        range: "max",
        min: -50,
        max: 50,
        value: 0,
        create: function () {
            jQuery("#font_top_handle").text(jQuery(this).slider("value"));
        },
        slide: function (event, ui) {
            var top = parseInt(ui.value);
            var y = parseInt(jQuery("#textBoxList option:selected").attr("org-y"));

            jQuery("#font_top_handle").text(ui.value);

            jQuery("#textBoxList option:selected").attr("y", top + y);
            updateTextImageData();
        }
    });

    jQuery("#image_left").slider({
        range: "max",
        min: -50,
        max: 50,
        value: 0,
        create: function () {
            jQuery("#image_left_handle").text(jQuery(this).slider("value"));
        },
        slide: function (event, ui) {

            var left = parseInt(ui.value);
            var x = parseInt(jQuery("#imageBoxList option:selected").attr("org-x"));
            jQuery("#image_left_handle").text(left);

            jQuery("#imageBoxList option:selected").attr("x", left + x);
            updateTextImageData();
        }
    });

    jQuery("#image_top").slider({
        range: "max",
        min: -50,
        max: 50,
        value: 0,
        create: function () {
            jQuery("#image_top_handle").text(jQuery(this).slider("value"));
        },
        slide: function (event, ui) {
            var top = parseInt(ui.value);
            var y = parseInt(jQuery("#imageBoxList option:selected").attr("org-y"));

            jQuery("#image_top_handle").text(top);

            jQuery("#imageBoxList option:selected").attr("y", top + y);
            updateTextImageData();
        }
    });

    jQuery("#image_width").slider({
        range: "max",
        min: -50,
        max: 50,
        value: 0,
        create: function () {
            jQuery("#image_width_handle").text(jQuery(this).slider("value"));
        },
        slide: function (event, ui) {

            var widthVal = parseInt(ui.value);
            var width = parseInt(jQuery("#imageBoxList option:selected").attr("org-width"));

            jQuery("#image_width_handle").text(widthVal);

            jQuery("#imageBoxList option:selected").attr("width", widthVal + width);
            updateTextImageData();
        }
    });

    jQuery("#image_height").slider({
        range: "max",
        min: -50,
        max: 50,
        value: 0,
        create: function () {
            jQuery("#image_height_handle").text(jQuery(this).slider("value"));
        },
        slide: function (event, ui) {

            var heightVal = parseInt(ui.value);
            var height = parseInt(jQuery("#imageBoxList option:selected").attr("org-height"));

            jQuery("#image_height_handle").text(heightVal);

            jQuery("#imageBoxList option:selected").attr("height", heightVal + height);
            updateTextImageData();
        }
    });




    jQuery("#curveTextRadius").slider({
        range: "max",
        min: -300,
        max: 300,
        value: 150,
        slide: function (event, ui) {
            var zoomVal = ui.value;
            updateTextImageData();
        }
    });
    jQuery("#curveTextAngle").slider({
        range: "max",
        min: 0,
        max: 1,
        step: 0.1,
        value: 0.8,
        slide: function (event, ui) {
            var zoomVal = ui.value;
            updateTextImageData();
        }
    });

    jQuery("#text-color-box").spectrum({
        color: "#00000",
        flat: false,
        showInput: true,
        className: "fontColorChange",
        showInitial: true,
        showPalette: true,
        clickoutFiresChange: true,
        showSelectionPalette: true,
        maxPaletteSize: 10,
        preferredFormat: "hex",
        palette: [
            ["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)", "rgb(255, 255, 255)"],
            ["rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)",
                "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"],
            ["rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)",
                "rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)",
                "rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)",
                "rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)",
                "rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)",
                "rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)",
                "rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)",
                "rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)",
                "rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)",
                "rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)"]
        ],
        change: function (color) {

            //jQuery("#text-color-box").css('background', 'none repeat scroll 0 0 ' + color.toHexString());
            jQuery("#color-text-input").val(color.toHexString());

            jQuery("#textBoxList option:selected").attr("fontColor", color.toHexString());

            updateTextImageData();

        }
    });
    //text border color
    jQuery(".border-color-box").spectrum({
        color: "#00000",
        flat: false,
        showInput: true,
        className: "fontBorderColorChange",
        showInitial: true,
        showPalette: true,
        clickoutFiresChange: true,
        showSelectionPalette: true,
        maxPaletteSize: 10,
        preferredFormat: "hex",
        palette: [
            ["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)", "rgb(255, 255, 255)"],
            ["rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)",
                "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"],
            ["rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)",
                "rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)",
                "rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)",
                "rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)",
                "rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)",
                "rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)",
                "rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)",
                "rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)",
                "rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)",
                "rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)"]
        ],
        change: function (color) {

            //jQuery(".border-color-box").css('background', 'none repeat scroll 0 0 ' + color.toHexString());
            jQuery(".border-text-input").val(color.toHexString());

            jQuery("#textBoxList option:selected").attr("fontBorderColor", color.toHexString());

            updateTextImageData();

        }
    });


    jQuery(".text-input-holder input").keyup(function () {

        jQuery("#textBoxList option:selected").attr("text", jQuery(this).val());

        updateTextImageData();
    });

    jQuery("#font-list").change(function () {

        jQuery("#textBoxList option:selected").attr("fontFamily", jQuery("#font-list").val());
        jQuery("#textBoxList option:selected").attr("data-file", jQuery("#font-list option:selected").attr("data-file"));
        updateTextImageData();
    });

    jQuery("#textBorderSizeList").change(function () {

        jQuery("#textBoxList option:selected").attr("fontBorder", jQuery("#textBorderSizeList").val());
        updateTextImageData();
    });


});


function addText() {

    var text = jQuery("#textBoxList option:selected").attr("text");
    if (!text)
        return false;

    var temData = {}, dataObj = [];

    for (var i = 0; i < jQuery("#textBoxList").children().length; i++)
    {
        var textBoxName = "text-" + jQuery("#textBoxList option:eq(" + i + ")").val();
        var text = jQuery(".text-input-holder ." + textBoxName).val();

        var x = jQuery("#textBoxList option:eq(" + i + ")").attr("x");
        var y = jQuery("#textBoxList option:eq(" + i + ")").attr("y");


        var fontSize = jQuery("#textBoxList option:eq(" + i + ")").attr("fontSize") ? jQuery("#textBoxList option:eq(" + i + ")").attr("fontSize") : jQuery("#font_size").slider("value");
        var fontFamily = jQuery("#textBoxList option:eq(" + i + ")").attr("fontFamily") ? jQuery("#textBoxList option:eq(" + i + ")").attr("fontFamily") : jQuery("#font-list").val();


        var fontColor = jQuery("#textBoxList option:eq(" + i + ")").attr("fontColor") ? jQuery("#textBoxList option:eq(" + i + ")").attr("fontColor") : jQuery(".fontColorChange").find(".sp-input").val();
        var fontBorderColor = jQuery("#textBoxList option:eq(" + i + ")").attr("fontBorderColor") ? jQuery("#textBoxList option:eq(" + i + ")").attr("fontBorderColor") : jQuery(".fontBorderColorChange").find(".sp-input").val();

        var fontBorder = jQuery("#textBoxList option:eq(" + i + ")").attr("fontBorder") ? jQuery("#textBoxList option:eq(" + i + ")").attr("fontBorder") : jQuery("#textBorderSizeList").val();

        temData['textBoxName'] = textBoxName;
        temData['text'] = text;
        temData['x'] = x;
        temData['y'] = y;

        temData['fontSize'] = fontSize;
        temData['fontFamily'] = fontFamily;

        temData['fontColor'] = fontColor;
        temData['fontBorderColor'] = fontBorderColor;

        temData['fontBorder'] = fontBorder;




        dataObj[i] = temData;
        temData = {};

    }


    addTextOnBoxName(dataObj);






    /*if (isTextCurve) {
     
     jQuery(".curver-text-angle-holder").show();
     
     drawTextAlongArc(ctx, text, 300, 950, radius, angle);
     } else {
     
     jQuery(".curver-text-angle-holder").hide();
     
     //ctx.fillText(text, 264, 402);//text , left , top
     //ctx.strokeText(text, 264, 402);//text , left , top
     
     ctx.fillText(text, 780, 342);//text , left , top
     ctx.strokeText(text, 780, 342);//text , left , top
     
     ctx.fill();
     ctx.stroke();
     
     
     
     }*/



}


function addTextOnBoxName(data)
{


    ctx.textAlign = "center";
    ctx.textBaseline = "middle";

    ctx.font = '' + data[0]['fontSize'] + 'px ' + data[0]['fontFamily'] + '';
    ctx.fillStyle = data[0]['fontColor'];
    ctx.strokeStyle = data[0]['fontBorderColor'];
    ctx.lineWidth = data[0]['fontBorder'];



    ctx.fillText(data[0]["text"], data[0]["x"], data[0]["y"]);//text , left , top
    ctx.strokeText(data[0]["text"], data[0]["x"], data[0]["y"]);//text , left , top

    ctx.font = '' + data[1]['fontSize'] + 'px ' + data[1]['fontFamily'] + '';


    ctx.fillStyle = data[1]['fontColor'];
    ctx.strokeStyle = data[1]['fontBorderColor'];
    ctx.lineWidth = data[1]['fontBorder'];

    ctx.fillText(data[1]["text"], data[1]["x"], data[1]["y"]);//text , left , top
    ctx.strokeText(data[1]["text"], data[1]["x"], data[1]["y"]);//text , left , top
    //check embroidery text
    isEmbroidery();


}

function updateTextImageData()
{
return false;
    theCurrentObjM[0] = new THREE.MeshLambertMaterial({map: textTexture});



    var background = new Image();
    background.src = jQuery(".socks-style-ul[id!='socks-style'] .activeClass img").attr("src");
    //Make sure the image is loaded first otherwise nothing will draw.
    background.onload = function () {
        ctx.drawImage(background, 0, 0);
        updateProperties();


    }
}



function drawTextAlongArc(context, str, centerX, centerY, radius, angle) {
    context.save();
    context.translate(centerX, centerY);
    context.rotate(-1 * angle / 2);
    context.rotate(-1 * (angle / str.length) / 2);
    for (var n = 0; n < str.length; n++) {
        context.rotate(angle / str.length);
        context.save();
        context.translate(0, -1 * radius);
        var char = str[n];
        context.fillText(char, 0, 0);
        context.restore();
    }
    context.restore();
}

function changeTextBox(actObj)
{
    var boxName;

    boxName = jQuery(actObj).val();

    targetRotation = parseFloat(jQuery("#textBoxList option:selected").attr("targetRotation"));

    jQuery(".text-input-holder input").hide();
    jQuery(".text-input-holder .text-" + boxName).show();


}

function useMonoGram(obj) {
    var monoSymb = jQuery(obj).html();

    var textBoxName = "text-" + jQuery("#textBoxList option:selected").val();
    var existingText = jQuery(".text-input-holder ." + textBoxName).val();

    if (existingText.length < 10) {
        jQuery(".text-input-holder ." + textBoxName).val(existingText + monoSymb);
        jQuery(".text-input-holder ." + textBoxName).trigger("keyup");
    }
}

function isEmbroidery()
{
    var isEmbroideryC = jQuery("#isEmbroidery").prop("checked");
    var embosClass;
    var textBoxName = "text-" + jQuery("#textBoxList option:selected").val();
    var existingText = jQuery(".text-input-holder ." + textBoxName).val();



    if (isEmbroideryC) {

        if (existingText != "") {
            jQuery("#embroPreviewCon").show();
            generateEmbroImage();
        }

    } else {

        jQuery("#embroPreviewCon").hide();

    }





}


function generateEmbroImage()
{
    jQuery("#theInitialProgress2").show();

    var textData = [], temp = {}, cnt = 0;
    var text, fontSize, color;



    var textBoxName = "text-" + jQuery("#textBoxList option:selected").val();

    text = jQuery(".text-input-holder ." + textBoxName).val().replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, ' ');


    fontSize = jQuery("#textBoxList option:selected").attr("fontSize") ? jQuery("#textBoxList option:selected").attr("fontSize") : jQuery("#font_size").slider("value");
    var fontFamily = jQuery("#textBoxList option:selected").attr("data-file") ? jQuery("#textBoxList option:selected").attr("data-file") : jQuery("#font-list option:selected,this").attr("data-file");


    var fontColor = jQuery("#textBoxList option:selected").attr("fontColor") ? jQuery("#textBoxList option:selected").attr("fontColor") : jQuery(".fontColorChange").find(".sp-input").val();
    var fontBorderColor = jQuery("#textBoxList option:selected").attr("fontBorderColor") ? jQuery("#textBoxList option:selected").attr("fontBorderColor") : jQuery(".fontBorderColorChange").find(".sp-input").val();

    var fontBorder = jQuery("#textBoxList option:selected").attr("fontBorder") ? jQuery("#textBoxList option:selected").attr("fontBorder") : jQuery("#textBorderSizeList").val();


    var design = {};

    design['text'] = text.trim();
    design['size'] = fontSize;
    design['color'] = fontColor;
    design['font'] = fontFamily;

    jQuery.ajax({
        url: ajaxPath + "getEmbroImg.php",
        data: {
            design: design
        },
        type: "POST",
        success: function (msg) {
            console.log(msg);

            var data = JSON.parse(msg);
            //var imgPath = basePath + 'tool-data/img/customized-images/embro/' + data['image'];
            var imgPathPre = basePath + 'tool-data/img/customized-images/embro/' + data['preview'];

            jQuery("#embroPreviewCon img").attr("src", imgPathPre);

            // addEmbroideryImage(imgPath, actText, viewID, currID, data);

            jQuery("#theInitialProgress2").hide();
        }
    });
}

/*
 function isEmbroidery()
 {
 var isEmbroideryC = jQuery("#isEmbroidery").prop("checked");
 var embosClass;
 var boxName = actRect = jQuery("#textBoxList li a.active").parent().attr("class").split(" ")[1];
 
 
 
 
 jQuery(".views").each(function () {
 
 var viewID = jQuery(this).attr("id");
 var viewNo = jQuery(this).attr("id").split("_")[1];
 
 var currID = currProductName + "-" + viewNo + "-" + "box-" + boxName;
 
 if (jQuery("#" + viewID + " #" + currID + " text").text()) {
 
 if (isEmbroideryC) {
 
 embosClass = "font-effect-emboss";
 
 jQuery("#" + viewID + " #" + currID + " text").hide();
 jQuery("#" + viewID + " #" + currID + " image").show();
 jQuery("#embroPreviewCon").show();
 generateEmbroImage(jQuery("#" + viewID + " #" + currID + " text"), viewID, currID);
 
 } else {
 jQuery("#" + viewID + " #" + currID + " text").show();
 jQuery("#" + viewID + " #" + currID + " image").hide();
 jQuery("#embroPreviewCon").hide();
 embosClass = "";
 }
 
 jQuery("#" + viewID + " #" + currID + " text").attr("class", embosClass);
 }
 
 });
 }
 
 
 function generateEmbroImage(actText, viewID, currID)
 {
 jQuery("#theInitialProgress2").show();
 
 var textData = [], temp = {}, cnt = 0;
 var text, font, size, color, part;
 
 
 
 
 text = jQuery("#text-input").val().replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, ' ');
 size = jQuery("#font_size").val();
 color = jQuery("#textColors .active-iconSecond").attr("data-color") ? jQuery("#textColors .active-iconSecond").attr("data-color") : "#008996";
 font = jQuery("#textFontFamily option:selected,this").attr("data-file");
 
 var design = {};
 
 design['text'] = text.trim();
 design['size'] = size;
 design['color'] = color;
 design['font'] = font;
 
 jQuery.ajax({
 url: ajaxPath + "getEmbroImg.php",
 data: {
 design: design
 },
 type: "POST",
 success: function (msg) {
 console.log(msg);
 
 var data = JSON.parse(msg);
 var imgPath = basePath + 'tool-data/img/customized-images/embro/' + data['image'];
 var imgPathPre = basePath + 'tool-data/img/customized-images/embro/' + data['preview'];
 
 jQuery("#embroPreviewCon img").attr("src", imgPathPre);
 
 addEmbroideryImage(imgPath, actText, viewID, currID, data);
 
 jQuery("#theInitialProgress2").hide();
 }
 });
 }*/
