jQuery(document).ready(function () {

    fullSize();
    applyOrientation();


    jQuery('.nav-open-btn').click(function () {
        jQuery('body').toggleClass('log-dash-open')
        jQuery(this).toggleClass('cross-icon')
    });

    jQuery('.sub-menu li a').click(function () {
        jQuery('body').addClass('sub-detail-open')
        jQuery(this).addClass('sub-detail-close')
    });

    jQuery('.accent-menus li a').click(function () {
        jQuery('body').addClass('sub-detail-open')
    });

    jQuery('.accent-menus-togle').click(function () {
        jQuery('body').addClass('ac-menu-open')
        jQuery(this).addClass('ac-menu-open-this')
    });

    jQuery('.reload-remove').click(function () {
        jQuery('body').removeClass('sub-detail-open');
    });

    jQuery('.responsive-buttonse').click(function () {
        jQuery('body').toggleClass('right-pannels-open')
        jQuery(this).toggleClass('right-pannels-this')
    });

    function moved() {
        // alert('in');
        var owl = jQuery(".owl-carousel").data('owlCarousel');
        if (owl.currentItem + 1 === owl.itemsAmount) {
            alert('THE END');
        }
    }

    jQuery(document).ready(function () {
        if (jQuery('html').hasClass('desktop')) {
            new WOW().init();
        }
    });

    jQuery('#prod-slider').owlCarousel({
        loop:true,
        margin:10,
        nav:true,
        dots:false,
        navText: [
            "<i class='fa fa-angle-left'></i>",
            "<i class='fa fa-angle-right'></i>"
        ],
        responsive:{
            0:{
                items:1
            },
            600:{
                items:3
            },
            1000:{
                items:5
            }
        }
    })

     jQuery.scrollIt({
         upKey: 40, // key code to navigate to the next section
         downKey: 40, // key code to navigate to the previous section
         easing: 'ease-in-out', // the easing function for animation
         scrollTime: 1000, // how long (in ms) the animation takes
         activeClass: 'active', // class given to the active nav element
         onPageChange: null, // function(pageIndex) that is called when page is changed
         topOffset: 0           // offste (in px) for fixed top navigation
     });
});

 jQuery(window).load(function () {
     if (window.innerWidth > 1024) {
         var s = skrollr.init();
     }
 });

jQuery(window).resize(function () {
    fullSize();
});


function fullSize() {
    var heights = window.innerHeight;
    jQuery(".fullHt").css('min-height', (heights + 0) + "px");
}

function applyOrientation() {
    if (window.innerHeight > window.innerWidth) {
        jQuery("body").addClass("potrait");
        jQuery("body").removeClass("landscape");
    } else {
        jQuery("body").addClass("landscape");
        jQuery("body").removeClass("potrait");
    }
}

var banner_Ht = window.innerHeight - jQuery('header').innerHeight();
jQuery(window).scroll(function () {
    var sticky = jQuery('body'),
            scroll = jQuery(window).scrollTop();
    if (scroll >= 100)
        sticky.addClass('sticky-header');
    else
        sticky.removeClass('sticky-header');
});

/*jQuery('body').append('<div id="toTop" class="btn"><span class="fa fa-angle-up"></span></div>');
 jQuery(window).scroll(function () {
 if (jQuery(this).scrollTop() != 0) {
 jQuery('#toTop').fadeIn();
 } else {
 jQuery('#toTop').fadeOut();
 }
 }); 
 jQuery('#toTop').click(function(){
 jQuery("html, body").animate({ scrollTop: 0 }, 1500);
 return false;
 });*/

jQuery(document).ready(function () {
    jQuery('.gallery-left-option a').click(function () {
        jQuery(this).parent().toggleClass('active').siblings().removeClass('active');
        jQuery(this).parent().siblings().find("ul").slideUp();
        if (jQuery(this).parent().hasClass('active')) {
            jQuery(this).parent().find("ul").slideDown();
        } else {
            jQuery(this).parent().find("ul").slideUp();
        }
    });
});


