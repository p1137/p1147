var libraryImages = [];
var canvasImg;
function inItImageData() {

    return false;

    jQuery("input[id=filePhoto]").change(function (e) {


        handleImage(e);
    });

    //clip color change
    jQuery(".badgeeditor__color").click(function () {


        jQuery(this).parent().find(".is-active").removeClass("is-active");
        jQuery(this).addClass("is-active");

        jQuery("#clipColors li").removeClass("active-iconSecond");
        var colo = jQuery(this).find("span").attr("data-color");


        if (colo) {
            jQuery("#clipColors").find("[data-color='" + colo + "']").addClass("active-iconSecond");
        } else {
            jQuery("#clipColors li:first").addClass("active-iconSecond");
        }

    });

//init canvas for svg to png

    canvasImg = new fabric.Canvas("svgToPng");

    jQuery.ajax({
        url: ajaxPath + "getCliparts.php",
        data: {
            pid: pid
        },
        type: "POST",
        success: function (msg) {
            libraryImages = JSON.parse(msg);

            loadclipartsCategory();

            loadcliparts();
        }
    });


}


function loadclipartsCategory()
{
    var category = libraryImages['category'];
    for (var cat in category)
    {
        jQuery("#gallery-cat").append('<option value="' + category[cat]["id"] + '">' + category[cat]["name"] + '</option>');
    }
}


function changeGalleryCat(actObj)
{

    var clips = libraryImages['clips'];

    var id = jQuery("#gallery-cat option:selected").attr("value");

    jQuery("#clipDataHolder li").hide();

    for (var i in clips)
    {
        jQuery("#clipDataHolder li[parent=" + id + "]").show();

    }
}

function handleImage(e) {


    var reader = new FileReader();
    reader.onload = function (event) {


        jQuery("#imageContainer img").css("opacity", "1");
        jQuery("#imageContainer img").attr('src', event.target.result);
        jQuery("#imageContainerNote").css("display", "none");
        jQuery(".delete-uploaded-img").css("display", "block");
        jQuery(".image-position-holder").css("display", "block");
        jQuery("#imageContainer").show();


        jQuery("#imageBoxList option:selected").attr("image", event.target.result);
        jQuery("#imageBoxList option:selected").attr("addedImageType", "image");

        setTimeout(function () {
            updateTextImageData();
        }, 500);
    }

    reader.readAsDataURL(e.target.files[0]);



}



function addImage() {


//    var image = jQuery("#imageBoxList option:selected").attr("image");
//    if (!image)
//        return false;


    var temData = {}, dataObj = [];

    for (var i = 0; i < jQuery("#imageBoxList").children().length; i++)
    {
        var imageBoxName = "image-" + jQuery("#imageBoxList option:eq(" + i + ")").val();
        var image = jQuery("#imageBoxList option:eq(" + i + ")").attr("image");




        var x = jQuery("#imageBoxList option:eq(" + i + ")").attr("x");
        var y = jQuery("#imageBoxList option:eq(" + i + ")").attr("y");

        var width = jQuery("#imageBoxList option:eq(" + i + ")").attr("width");
        var height = jQuery("#imageBoxList option:eq(" + i + ")").attr("height");



        temData['imageBoxName'] = imageBoxName;
        temData['image'] = image;
        temData['x'] = x;
        temData['y'] = y;
        temData['width'] = width;
        temData['height'] = height;


        dataObj[i] = temData;
        temData = {};

    }


    addImageOnBoxName(dataObj);

}

function deleteImage(actObj) {

    jQuery("#imageContainerNote").show();
    jQuery("#imageContainerNote_inner").show();
    jQuery(actObj).hide();
    //delete image from svg rect
    jQuery("#imageContainer img").css("opacity", "0");
    jQuery("#imageContainer img").attr("src", "");
    jQuery("#imageBoxList option:selected").attr("image", "");
    jQuery(".image-position-holder").css("display", "none");

    updateTextImageData();
}

function deleteImageGraphics(actObj) {


    jQuery(actObj).hide();


    jQuery("#imageBoxList option:selected").attr("image", "");
    jQuery("#imageBoxList option:selected").attr("svgpath", "");

    jQuery(".image-position-holder").css("display", "none");
    jQuery(".dropdown-menuColor-third").hide();


    updateTextImageData();
}



function changeImageBox(actObj)
{
    var boxName;

    boxName = jQuery(actObj).val();
    targetRotation = parseFloat(jQuery("#imageBoxList option:selected").attr("targetRotation"));
    var image = jQuery("#imageBoxList option:selected").attr("image");
    var addedImageType = jQuery("#imageBoxList option:selected").attr("addedImageType");


    if (addedImageType == "image") {

        if (!image) {
            jQuery("#imageContainer").show();
            jQuery("#imageContainer img").css("opacity", "0");
            jQuery("#imageContainer img").attr('src', "");
            jQuery("#imageContainerNote").css("display", "block");
            jQuery(".delete-uploaded-img").css("display", "none");

            jQuery(".image-position-holder").css("display", "none");
        } else {
            jQuery("#imageContainer").show();
            jQuery("#imageContainer img").css("opacity", "1");
            jQuery("#imageContainer img").attr('src', image);
            jQuery("#imageContainerNote").css("display", "none");
            jQuery(".delete-uploaded-img").css("display", "block");

            jQuery(".image-position-holder").css("display", "block");

        }


    } else if (addedImageType == "art") {

        if (!image) {


            jQuery(".dropdown-menuColor-third").hide();
            jQuery(".delete-uploaded-img-graphics").css("display", "none");

            jQuery(".image-position-holder").css("display", "none");
        } else {

            jQuery(".dropdown-menuColor-third").show();
            jQuery(".delete-uploaded-img-graphics").css("display", "block");
            jQuery(".image-position-holder").css("display", "block");

        }

    }

    //trigger to act image type
    jQuery("#imageTypeLink .active").trigger("click")

}

function addImageOnBoxName(data)
{



    if (data[0]['image']) {
        var upload = new Image();
        upload.src = data[0]['image'];

        //Make sure the image is loaded first otherwise nothing will draw.
        ctx.drawImage(upload, data[0]['x'], data[0]['y'], data[0]['width'], data[0]['height']);
    }


    if (data[1]['image']) {
        var upload1 = new Image();
        upload1.src = data[1]['image'];
        ctx.drawImage(upload1, data[1]['x'], data[1]['y'], data[1]['width'], data[1]['height']);

    }

    jQuery(".jacket-preloader").hide();

}

function changeImageloadLink(actObj)
{
    jQuery(actObj).parent().find(".active").removeClass("active");
    jQuery(actObj).addClass("active");

    var image = jQuery("#imageBoxList option:selected").attr("image");
    var addedImageType = jQuery("#imageBoxList option:selected").attr("addedImageType");
    var actObjCon = jQuery(actObj).find("a").attr("data-target");

    console.log("actObjCon = " + actObjCon)

    jQuery(".gallery-holder").hide();
    jQuery(actObjCon).show();

    if (actObjCon == "#adDzinfour")
    {
        jQuery(".dropdown-menuColor-third").hide();

        if (!image) {
            jQuery(".image-position-holder").css("display", "none");
        } else {
            jQuery(".image-position-holder").css("display", "block");
        }

    } else {


        if (addedImageType == "art" && image) {
            jQuery(".dropdown-menuColor-third").show();
            jQuery(".image-position-holder").css("display", "block");
        } else {
            jQuery(".image-position-holder").css("display", "none");
        }

    }


}


function loadcliparts()
{
    jQuery("#clipDataHolder").html('');

    var clips = libraryImages['clips'];
    var img_src;

    for (var i in clips)
    {

        img_src = basePath + 'clipart-images/' + clips[i]['image'];
        jQuery("#clipDataHolder").append('<li onclick="convertToPngLoad(this)" data-src="' + img_src + '" parent="' + clips[i]['parent'] + '" noofcolors="' + clips[i]['noofcolors'] + '" class="clips" id="' + i + '"></li>');

        loadSvgClips(i, img_src);

    }

    //trigger to first cat
    jQuery("#gallery-cat").trigger("change");

}

function loadSvgClips(container, ele) {
    Snap.load(ele, function (data) {
        Snap("#" + container + "").append(data);
        if (Snap("#" + container + " svg rect")) {
            Snap("#" + container + " svg rect").attr({"width": "74", "height": "74"});
        }
    });
}

function convertToPngLoad(actObj)
{

    var imgScr = jQuery(actObj).attr("data-src");
    var parent = jQuery(actObj).attr("parent");

    var id = jQuery(actObj).attr("id");
    var noofcolors = jQuery(actObj).attr("noofcolors");



    jQuery(".delete-uploaded-img-graphics").show();
    jQuery(".image-position-holder").css("display", "block");

    jQuery(actObj).parent().find(".clips").removeClass("actClip");
    jQuery(actObj).addClass("actClip");

    jQuery(".badgeeditor__colors").find(".is-active").removeClass("is-active");


    if (noofcolors == "0")
    {
        jQuery(".dropdown-menuColor-third").hide();

    } else if (noofcolors == "2") {
        jQuery(".dropdown-menuColor-third").show();

        jQuery(".badgeeditor__colors a:eq(0)").addClass("is-active");

        jQuery(".badgeeditor__colors a:eq(0)").css("opacity", "1");
        jQuery(".badgeeditor__colors a:eq(1)").css("opacity", "1");
        jQuery(".badgeeditor__colors a:eq(2)").css("opacity", "0");
    } else if (noofcolors == "1") {
        jQuery(".dropdown-menuColor-third").show();

        jQuery(".badgeeditor__colors a:eq(0)").addClass("is-active");

        jQuery(".badgeeditor__colors a:eq(0)").css("opacity", "1");
        jQuery(".badgeeditor__colors a:eq(1)").css("opacity", "0");
        jQuery(".badgeeditor__colors a:eq(2)").css("opacity", "0");
    } else if (noofcolors == "3") {

        jQuery(".dropdown-menuColor-third").show();


        jQuery(".badgeeditor__colors a:eq(0)").addClass("is-active");

        jQuery(".badgeeditor__colors a:eq(0)").css("opacity", "1");
        jQuery(".badgeeditor__colors a:eq(1)").css("opacity", "1");
        jQuery(".badgeeditor__colors a:eq(2)").css("opacity", "1");
    }

    /*if (id == "clips1")
     {
     
     jQuery(".badgeeditor__colors a:eq(0)").addClass("is-active");
     
     jQuery(".badgeeditor__colors a:eq(0)").css("opacity", "1");
     jQuery(".badgeeditor__colors a:eq(1)").css("opacity", "1");
     jQuery(".badgeeditor__colors a:eq(2)").css("opacity", "0");
     
     
     } else {
     
     jQuery(".badgeeditor__colors a:eq(0)").addClass("is-active");
     
     jQuery(".badgeeditor__colors a:eq(0)").css("opacity", "1");
     jQuery(".badgeeditor__colors a:eq(1)").css("opacity", "0");
     jQuery(".badgeeditor__colors a:eq(2)").css("opacity", "0");
     }*/








    getSvgToPng(imgScr);

}

function getSvgToPng(clipsrc)
{

    var col0 = jQuery(".badgeeditor__colors a:eq(0) span").attr("data-color");
    var col1 = jQuery(".badgeeditor__colors a:eq(1) span").attr("data-color");
    var col2 = jQuery(".badgeeditor__colors a:eq(2) span").attr("data-color");

    var colors = {'1': col0, '2': col1, 3: col2};

    //

    jQuery("#imageBoxList option:selected").attr("colors", colors);


    canvasImg.clear();

    fabric.loadSVGFromURL(clipsrc, function (objects, options) {
        var loadedObject = fabric.util.groupSVGElements(objects, options);
        var theScaleFactor = getScaleFactor(loadedObject.width, loadedObject.height, parent);
        loadedObject.set({
            left: 0,
            top: 0
        }).scale(theScaleFactor.scale).setCoords();

        if (colors) {
            var currIndx = 0;
            loadedObject.paths.forEach(function (path) {
                if (path.d) {
                    if (colors['' + (currIndx + 1)] != '') {
                        path.fill = colors['' + (currIndx + 1)];
                    }
                    currIndx++;
                }
            });
        }

        canvasImg.add(loadedObject);
        loadedObject.setCoords();
        canvasImg.renderAll();

        getPngImg(clipsrc);
    });


}



function getScaleFactor(w, h)
{
    var newScaleY = 1;
    var newScaleX = 1;
    var isScaled = false;

    var arrW = canvasImg.getWidth();
    var arrH = canvasImg.getHeight();


    if (w > arrW || h > arrH)
    {
        if ((w >= arrW) && (h >= arrH))
        {
            if (w > h)
            {
                newScaleX = arrW / w;
                newScaleY = newScaleX;
            }
            else
            {
                newScaleY = arrH / h;
                newScaleX = newScaleY;
            }
            isScaled = true;
        }

        if (!isScaled && (w >= arrW))
        {
            newScaleX = arrW / w;
            newScaleY = newScaleX;
            isScaled = true;
        }

        if (!isScaled && (h >= arrH))
        {
            newScaleY = arrH / h;
            newScaleX = newScaleY;
            isScaled = true;
        }

    } else if (w < arrW && h < arrH) {

        if (w > h || w === h) {
            newScaleX = arrW / w;
            newScaleY = newScaleX;
        } else if (h > w) {
            newScaleY = arrH / h;
            newScaleX = newScaleY;
        }
    }

    var newX = (arrW - (parseFloat(w * newScaleX))) / 2;
    var newY = (arrH - (parseFloat(h * newScaleX))) / 2;

    return {scale: newScaleX, x: newX, y: newY};
}




function getPngImg(clipsrc)
{
    var pngImg = canvasImg.toDataURL("png");


    jQuery("#imageBoxList option:selected").attr("image", pngImg);
    jQuery("#imageBoxList option:selected").attr("svgpath", clipsrc);
    jQuery("#imageBoxList option:selected").attr("addedImageType", "art");



    setTimeout(function () {
        updateTextImageData();
    }, 1000);

}


function changeClipColor(actObj)
{

    console.log("in color change")


    var currColor = jQuery(actObj).attr("data-color");


    //change color of list 
    jQuery('.badgeeditor__colors .is-active span').css("background", currColor);
    jQuery('.badgeeditor__colors .is-active span').attr("data-color", currColor);


    jQuery("#clipColors li").removeClass("active-iconSecond");
    jQuery(actObj).addClass("active-iconSecond");


    //change color of clip path on preview
    var pathNo = jQuery('.badgeeditor__colors .is-active').attr("data-id");

    jQuery(".badgeeditor__badge svg").find(".badge__color__" + pathNo).css("fill", currColor);



    var colors = jQuery("#imageBoxList option:selected").attr("colors");
    var svgSource = jQuery("#imageBoxList option:selected").attr("svgpath");



    if (svgSource) {
        colors['' + pathNo] = currColor;
        getSvgToPng(svgSource);
    }
}
