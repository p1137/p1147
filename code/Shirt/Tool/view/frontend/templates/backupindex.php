<?php
// echo "one"; die;
$objectManager = \Magento\Framework\App\ObjectManager::getInstance();

$directory = $objectManager->get('\Magento\Framework\Filesystem\DirectoryList');

$myRootBasePath = $directory->getRoot(); // to get base root path: \var\www\...

$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
$basePath = $storeManager->getStore()->getBaseUrl();  // get base url...

$key_form = $objectManager->get('\Magento\Framework\Data\Form\FormKey');

$form_Key = $key_form->getFormKey(); // this will give from key

$product_id = 182;
$_product = $objectManager->create('\Magento\Catalog\Model\Product')->load($product_id);
$productPrice = $_product->getFinalPrice();

$current_currency_symbol = $objectManager->get('Magento\Store\Model\StoreManagerInterface');
$currency = $current_currency_symbol->getStore()->getCurrentCurrencyCode();

$currencySymbol = $currency;
/* $baseCurrencyCode = Mage::app()->getBaseCurrencyCode();
  $allowedCurrencies = Mage::getModel('directory/currency')->getConfigAllowCurrencies();
  $currencyRates = Mage::getModel('directory/currency')->getCurrencyRates($baseCurrencyCode, array_values($allowedCurrencies));
  $inrCurrencyRate = $currencyRates['INR'];
  $productPrice = $productPrice * $inrCurrencyRate; */
?>
<title>Customize Your Shirt</title>

    <style type="text/css">
        #monoColorHolder{
            padding-top: 10%;
        }
        </style>
    <link href="<?= $basePath ?>pub/media/shirt-tool/tool_data/css/bootstrap.min.css" rel="stylesheet"/>

    <link href="<?= $basePath ?>pub/media/shirt-tool/tool_data/css/jquery-ui.css" rel="stylesheet"/>
    <link href="<?= $basePath ?>pub/media/shirt-tool/tool_data/css/font-awesome.min.css" rel="stylesheet"/>  
    <link href="<?= $basePath ?>pub/media/shirt-tool/tool_data/css/jquery-ui.css" rel="stylesheet"/>
    <link href="<?= $basePath ?>pub/media/shirt-tool/tool_data/css/owl.carousel.css" rel="stylesheet"/>
    <link href="<?= $basePath ?>pub/media/shirt-tool/tool_data/css/owl.theme.css" rel="stylesheet"/>
    <link href="<?= $basePath ?>pub/media/shirt-tool/tool_data/css/jquery.mCustomScrollbar.css" rel="stylesheet"/>
    <link href="<?= $basePath ?>pub/media/shirt-tool/tool_data/css/icon-library-v1.css" rel="stylesheet"/>
    <link href="<?= $basePath ?>pub/media/shirt-tool/tool_data/css/preloader.css" rel="stylesheet"/>
    
    <link href="<?= $basePath ?>pub/media/shirt-tool/tool_data/css/main.css" rel="stylesheet"/>
    <link href="<?= $basePath ?>pub/media/shirt-tool/tool_data/css/shirt-responsive.css" rel="stylesheet"/>
    <link href="<?= $basePath ?>pub/media/shirt-tool/tool_data/css/shirt-main.css" rel="stylesheet"/>

    <main ng-app="Shirt" ng-controller="shirtController">


        <!--main outer loader start-->
        <div id="theInitialProgress" class="preloader">
            <div class="progressInner">
                <img width="70" height="70" src="<?= $basePath ?>pub/media/shirt-tool/tool_data/img/Idesignibuy_loader.gif">
                <p class="loaderText">Please wait while shirt configuration is loading....</p>
            </div>
        </div>
        <!--end-->
        
        
        <header class="toolheader">
            <nav class="tool-navbar">
                <div class="topmenu clearfix">
                    <ul>
                        <li class="active"> <a id="linkStyle" class=" showsingal" target="1">Style</a></li>
                        <li> <a  id="linkFabric" class=" showsingal " target="4">Fabric</a> </li>
                        <li> <a  id="linkAccent" class=" showsingal" target="5">Accent</a> </li>
                    </ul>
                </div>
            </nav>
        </header>

        <span>
            <input type="hidden" id="key_id" value="<?= $form_Key ?>" >
            <input type="hidden" id="basePath" value="<?= $basePath ?>" >
            <input type="hidden" id="productId" value="<?= $product_id ?>" >
            <input type="hidden" id="productPrice" value="<?= $productPrice ?>" >
            <input type="hidden" id="currSymbol" value="<?= $currencySymbol ?>" >
        </span>
        
        
        <section class="ToolSection  clearfix" id="main-container">
            <a href="javascript:void(0)" class="left_menu-btn ws-left-menu-btn" onclick="leftMenuBtn(this)">
                <span></span>
            </a>
            <a href="javascript:void(0)" class="rightPanel"> <i class="fa fa-bars"></i> </a>
            <div class="container-fluid">
                <!-- <div class="row"> -->
                    <div class="col-md-3 col-xs-12 col-sm-4 left-slide">

                        <div class="leftpanelsection clearfix">
                       
                            <div class="sidebar-options sidebar-length" id="divOptionFilter">
                                <h4>Add Filter</h4>
                                <span class="back-btn" ng-click="leftNavFabCatFilterClose($event)"><i class="fa fa-arrow-circle-o-left"></i></span>
                                <div class="filter-search">
                                    <div class="input-group">
                                        <input type="search" ng-model="test" placeholder="Search..." class="form-control ng-pristine ng-untouched ng-valid" ng-model="test" ng-keyup="resetCheckBoxFilter($event)">
                                        <div class="input-group-btn">
                                            <button ng-click="applyFilter()" class="btn btn-default search-button" type="button"><i class="fa fa-search"></i></button>
                                        </div>
                                    </div>
                                </div>
                                <div id="accordian">
                                    <div ng-repeat="fabCat in fabricData.category">
                                        <h3 class="rotate-menu">
                                            <a href="javascript:void(0)">{{fabCat[0].parent}}</a>
                                        </h3>
                                        <ul class="fabricFilterCat">
                                            <li ng-repeat="fab in fabCat">
                                                <span><input type="checkbox"  ng-model="search[fab.name]" ng-change="filterCheckedFabric($event, $index, this)"></span>
                                                <span >{{fab.name}}</span>
                                            </li>
                                        </ul>
                                    </div>

                                </div>

                                <div class="rt-btn btn_filter"> <a ng-click="applyFilter()" href="javascript:void(0)"> Apply Filter <i aria-hidden="true" class="fa fa-angle-double-right"> </i> </a> </div> 
                            </div>

                            <div class="sidebar-options sidebar-length" id="divOptionMulti">
                                <h4>Select Fabric for each piece</h4>
                                <div class="box_options">

                                    <div class="box_option">
                                        <div class="slde-inoption">
                                            <ul>
                                                <p class="title">Fabrics</p>
                                                <li class="optiontrigger same" ng-click="changeSameDifferentFabric('same')">
                                                    <a class="showsingalf" target="1">
                                                        <span class="option_icon"> <i class="icon-icon-25"></i></span>
                                                        <span> Same Fabric </span>
                                                        <p class="price-info">
                                                            <span><?= $currencySymbol ?></span> 
                                                        <!-- </span> -->
                                                        <span class="price">0.00</span></p>
                                                    </a>
                                                </li>
                                                <li class="optiontrigger different" ng-click="changeSameDifferentFabric('different')">
                                                    <a class="showsingalf" target="2">
                                                        <span class="option_icon"> <i class="icon-icon-25"></i></span>
                                                        <span> Different Fabric  </span>
                                                        <p class="price-info" >
                                                            <span><?= $currencySymbol ?></span> 
                                                            <!-- </span> -->
                                                            <span class="price">0.00</span>
                                                        </p>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="sidebar-options sidebar-length" id="divOptionInfo">
                                <h4>Info</h4>
                                <div>
                                    <ul>
                                        <li>
                                            <div class="infor_holder">
                                                <h5><span><i class="icon-icon-20"></i></span> <span>Faves</span></h5>
                                                <div class="info_disp">
                                                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.</p>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="infor_holder">
                                                <h5><span><i class="icon-icon-22"></i></span> <span>Winter</span></h5>
                                                <div class="info_disp">
                                                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.</p>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="infor_holder">
                                                <h5><span><i class="icon-icon-21"></i></span> <span>Premium</span></h5>
                                                <div class="info_disp">
                                                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.</p>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="infor_holder">
                                                <h5><span><i class="icon-icon-19"></i></span> <span>New</span></h5>
                                                <div class="info_disp">
                                                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.</p>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <div class="sidebar-options sidebar-length style" id="div1">
                                <div class="slde-in" >
                                    <ul>
                                        <li class="" ng-repeat="designStyles in designData" ng-click="categoryClick($event, designStyles, $index)">
                                            <a class="show-singal" target="{{designStyles.id}}">
                                                <span class="option_icon_left">
                                                    <i class="{{designStyles.class}}"></i>
                                                </span>
                                                <span class="option_title">{{designStyles.name}} </span>
                                                <span class="shorttext" style="display:none">{{designStyles.name}}</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>

                                <div class="box_options" style="z-index: 99">
                                    <div class="box_option sidebarlength"  ng-repeat="designStyles in designData" id="divs_{{designStyles.id}}" >
                                        <div class="slde-inoption mCustomScrollbar" data-mcs-theme="dark" >
                                            <ul>
                                                <p class="title">{{designStyles.name}}</p>
                                                <a href="javascript:void(0)" class="removemin" ng-click="removemin($event)">
                                                    <i class="fa fa-angle-left"></i>
                                                </a>
                                                <li class="optiontrigger"  id="{{style.id}}" style_id="{{style.style_id}}" ng-repeat=" style in designStyles.style" index="{{$index}}" name="style" price="{{style.price}}" ng-disabled="" ng-click="designClick($event, style, $index, style.class);">
                                                    <a href="javascript:void(0)">

                                                        <span class="option_icon"> <i class="{{style.class}}"></i></span>
                                                        <span>{{style.name}} </span>
                                                        <p class="price-info"><span><?= $currencySymbol ?> </span><span class="price">{{style.price ?style.price:"0.0"}} </span></p>
                                                    </a>

                                                </li>
                                            </ul>
                                            <!--******sub menu ******* -->
                                            <!--******sub menu for Single pockets ******* -->
                                            <ul ng-if="designStyles.style[0].name == 'Single Pocket'" class="{{designStyles.style[0].name}}" >
                                                <p class="title">select {{designStyles.style[0].name}}</p>
                                                <li ng-repeat="stylee in designStyles.style[0].style" class="optiontrigger" name="style" ng-disabled="" ng-click="designClick($event, stylee, $index, stylee.class);">
                                                    <!--<span ng-repeat="stylees in stylee">-->
                                                    <a href="javascript:void(0)">

                                                        <span class="option_icon"> <i class="{{stylee.class}}"></i></span>
                                                        <span>{{stylee.name}} </span>
                                                        <p class="price-info"><span><?= $currencySymbol ?> </span><span class="price">{{stylee.price ?stylee.price:"0.0"}} </span></p>
                                                    </a>
                                                </li>
                                            </ul>
                                            <!--******sub menu for Double pockets ******* -->
                                            <ul ng-if="designStyles.style[1].name == 'Double Pocket'" class="{{designStyles.style[1].name}}" >
                                                <p class="title">select {{designStyles.style[1].name}}</p>
                                                <li ng-repeat="stylee in designStyles.style[1].style" class="optiontrigger" name="style" ng-disabled="" ng-click="designClick($event, stylee, $index, stylee.class);">
                                                    <!--<span ng-repeat="stylees in stylee">-->
                                                    <a href="javascript:void(0)">

                                                        <span class="option_icon"> <i class="{{stylee.class}}"></i></span>
                                                        <span>{{stylee.name}} </span>
                                                        <p class="price-info"><span><?= $currencySymbol ?> </span><span class="price">{{stylee.price ?stylee.price:"0.0"}} </span></p>
                                                    </a>
                                                </li>
                                            </ul>
                                            <!--******sub menu for "Normal Cuff" ******* -->
                                            <ul ng-if="designStyles.style[0].name == 'Normal Cuff'" class="{{designStyles.style[0].name}}" >
                                                <p class="title">select {{designStyles.style[0].name}}</p>
                                                <li ng-repeat="stylee in designStyles.style[0].style" class="optiontrigger" name="style" ng-disabled="" ng-click="designClick($event, stylee, $index, stylee.class);">
                                                    <!--<span ng-repeat="stylees in stylee">-->
                                                    <a href="javascript:void(0)">

                                                        <span class="option_icon"> <i class="{{stylee.class}}"></i></span>
                                                        <span>{{stylee.name}} </span>
                                                        <p class="price-info"><span><?= $currencySymbol ?> </span><span class="price">{{stylee.price ?stylee.price:"0.0"}} </span></p>
                                                    </a>
                                                </li>
                                            </ul>
                                            <!--******sub menu for "Rounded Cuff" ******* -->
                                            <ul ng-if="designStyles.style[2].name == 'Rounded Cuff'" class="{{designStyles.style[2].name}}" >
                                                <p class="title">select {{designStyles.style[2].name}}</p>
                                                <li ng-repeat="stylee in designStyles.style[2].style" class="optiontrigger" name="style" ng-disabled="" ng-click="designClick($event, stylee, $index, stylee.class);">
                                                    <!--<span ng-repeat="stylees in stylee">-->
                                                    <a href="javascript:void(0)">

                                                        <span class="option_icon"> <i class="{{stylee.class}}"></i></span>
                                                        <span>{{stylee.name}} </span>
                                                        <p class="price-info"><span><?= $currencySymbol ?> </span><span class="price">{{stylee.price ?stylee.price:"0.0"}} </span></p>
                                                    </a>
                                                </li>
                                            </ul>
                                            <!--******sub menu for "Bevelled Cuff" ******* -->
                                            <ul ng-if="designStyles.style[1].name == 'Bevelled Cuff'" class="{{designStyles.style[1].name}}" >
                                                <p class="title">select {{designStyles.style[1].name}}</p>
                                                <li ng-repeat="stylee in designStyles.style[1].style" class="optiontrigger" name="style" ng-disabled="" ng-click="designClick($event, stylee, $index, stylee.class);">
                                                    <!--<span ng-repeat="stylees in stylee">-->
                                                    <a href="javascript:void(0)">

                                                        <span class="option_icon"> <i class="{{stylee.class}}"></i></span>
                                                        <span>{{stylee.name}} </span>
                                                        <p class="price-info"><span><?= $currencySymbol ?> </span><span class="price">{{stylee.price ?stylee.price:"0.0"}} </span></p>
                                                    </a>
                                                </li>
                                            </ul>
                                            <p ng-if="designStyles.name == 'Tool'" class="title">{{designStyles.name}} Size</p>
                                            <ul ng-if="designStyles.name == 'Tool'" id="toolSize">
                                                <a href="javascript:void(0)" class="removemin" ng-click="removemin($event)"> <i class="fa fa-angle-left"></i></a>
                                                <li class="optiontrigger" id="{{size.id}}" ng-repeat=" size in designStyles.size" index="{{$index}}" name="style" ng-disabled="" ng-click="designClick($event, size, $index);">
                                                    <a href="javascript:void(0)">
                                                        <span class="option_icon"> <i class="{{size.class}}"></i></span>
                                                        <span>{{size.name}} </span></a>
                                                    <p class="price-info"><span><?= $currencySymbol ?> </span><span class="price">{{style.price ?style.price:"0.0"}} </span></p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="sidebar-options sidebar-length-pant style" id="div2" style="display: none">
                                <div class="slde-in mCustomScrollbar"  data-mcs-theme="dark">
                                    <ul >
                                        <li class="" ng-repeat="designStyles in pantData" ng-click="categoryClick($event, designStyles, $index)">
                                            <a class="show-singal-pant" target="{{designStyles.id}}">
                                                <span class="option_icon_left">
                                                    <i class="{{designStyles.class}}"></i>
                                                </span>
                                                <span class="option_title">{{designStyles.name}} </span>
                                                <span class="shorttext" style="display:none">{{designStyles.name}}</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>

                                <div class="box_options" style="z-index: 99">
                                    <div class="box_option sidebarlength-pant"  ng-repeat="designStyles in pantData" id="divpant_{{designStyles.id}}" >
                                        <div class="slde-inoption mCustomScrollbar" data-mcs-theme="dark" >
                                            <ul>
                                                <p class="title">{{designStyles.name}}</p>
                                                <a href="javascript:void(0)" class="removemin" ng-click="removemin($event)"> <i class="fa fa-angle-left"></i></a>
                                                <li class="optiontrigger" id="{{style.id}}" ng-repeat=" style in designStyles.style" index="{{$index}}" name="style" ng-disabled="" ng-click="designClick($event, style, $index);">
                                                    <a href="javascript:void(0)">
                                                        <span class="option_icon"><i class="{{style.class}}"></i></span>
                                                        <span>{{style.name}} </span>
                                                        <p class="price-info"><span><?= $currencySymbol ?> </span><span class="price">{{style.price ?style.price:"0.0"}} </span></p>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>



                            <div class="sidebar-options sidebar-length box_options_parts clearfix" id="divOptionDifferentParts" 
                                style="display:none">
                                <label class="left check_button" rel="man_shirt">
                                    <span class="check_radio">
                                        <input  ng-change="changePart($event)" type="radio" checked="checked" ng-model="partOption" name="radio"  value="jacket" >
                                    </span>
                                    <!--<span class="checkmark"></span>-->
                                    <span class="title">Jacket</span>
                                </label>
                                <label class="left check_button checked" rel="man_pants">
                                    <span class="check_radio">
                                        <input ng-change="changePart($event)" type="radio" ng-model="partOption" value='pant'>
                                    </span>
                                    <!--<span class="checkmark"></span>-->
                                    <span class="title">Pant </span>
                                </label>
                                <label class="left check_button" rel="man_waistcoat" >
                                    <span class="check_radio">
                                        <input ng-change="changePart($event)" type="radio" ng-model="partOption" value='vest'>
                                    </span>
                                    <!--<span class="checkmark"></span>-->
                                    <span class="title">Vest</span>
                                </label>
                            </div>

                            <div class="sidebar-options sidebar-length Fabric  " id="div4">
                                <h4 id="fabircCatName">All Fabrics</h4>
                                <div class="filter-button" ng-click="leftNavFabCatFilter($event)">
                                    <span class="icon-Filter filter-icon" title="Filter Fabrics">
                                    </span>
                                </div>


                                <div class="slde-fabin ">
                                    <div class="fabricGenerater clearfix" >
                                        <fabricview class="clearfix "  ng-repeat="fabric in fabricData.fabric|filter:test" ng-click="fabricClick($event, fabric)"   index="{{$index}}" activeStyle="0" id="{{fabric.id}}" fabricName="{{fabric.name}}" prodType="{{fabric.fabric}}" data-zoom-image="{{fabric.real_img}}"></fabricview>
                                        <!--<fabricview ng-repeat="fabric in fabricData.fabric" ng-click="fabricClick($event, fabric)"   index="{{$index}}" activeStyle="0" id="{{fabric.id}}" fabricName="{{fabric.name}}" prodType="{{fabric.fabric}}"></fabricview>-->
                                    </div>
                                    <div style="display: none" id="nullTxt">
                                        <p>Fabrics of this name are not available.</p>
                                    </div>
                                </div>
                            </div>

                            <div class="sidebar-options sidebar-length Accent" id="div5">
                                <div class="slde-in">
                                    <ul>
                                        <li ng-repeat="designStyles in suitAccentData" ng-click="categoryClick($event, designStyles, $index)" 
                                            id="li_id_{{designStyles.name}}">
                                            <a class="show-singal" target="{{designStyles.id}}">
                                                <span class="option_icon_left" >
                                                    <i class="{{designStyles.class}}"></i>
                                                </span>

                                                <span class="option_title">{{designStyles.name}} </span>
                                                <span class="shorttext">{{designStyles.name}}</span>
                                            </a>
                                        </li>
                                        <!-- 
                                        <li id="collarinAccent">
                                        <a target="25" class="show-singal">
                                            <span class="colls-all-ic"><i class="icos-all"></i></span>
                                            <span class="option_title ng-binding">Collar Style </span>
                                            <span class="shorttext ng-binding" style="display: none;">Collar Style</span>
                                        </a>
                                        </li>

                                        <li id="ThreadinAccent">
                                            <a target="26" class="show-singal">
                                                <span class="colls-all-ic"><i class="icon-thread"></i></span>
                                                <span class="option_title ng-binding">Button Threads </span>
                                                <span class="shorttext ng-binding" style="display: none;">Threads</span>
                                            </a>
                                        </li>
                                        -->
                                    </ul>
                                </div>

                                <div class="box_options">
                                    <div class="box_option sidebarlength"  ng-repeat="designStyles in suitAccentData" 
                                        id="divaccent_{{designStyles.id}}" >

                                        <div class="slde-inoption mCustomScrollbar" data-mcs-theme="dark" >
                                            <ul>
                                                <p class="title">{{designStyles.name}}</p>
                                                <p class="price-info" ng-show="actCatName =='AddMonogram'">  (+ <span><?= $currencySymbol ?> </span>
                                                    <span class="accent-price">{{designStyles.price ? designStyles.price:"0.0"}} </span> )</p>
                                                <a href="javascript:void(0)" class="removemin" ng-click="removemin($event)"> <i class="fa fa-angle-left"></i></a>

                                                <div class="monoFont">
                                                    <div class="srch-mono-blk clearfix">
                                                        <input type="text" ng-model="monogramText" placeholder="Enter Text" class="form-control ng-valid ng-valid-maxlength ng-dirty ng-valid-parse ng-touched" id="data" ng-keyup="changeMonoTxt(monogramText);" ng-k maxlength="5" >

                                                        <span id="clearText"></span>
                                                        <br/>
                                                        <p style="font-size: 10px;">Note : Special Characters will not be accepted in monogram text.</p>
                                                    </div>

                                                    <div class="ws-monofont clearfix">
                                                        <h6>FONT:</h6>
                                                        <div data-id="{{font.id}}" class="cust-font-radio " ng-click="changeTextFont(font.id)"  ng-repeat="font in fonts">
                                                            <input  id="radio-{{font.id}}" class="radio-custom" name="radio" checked="" type="radio">
                                                            <label for="radio-{{font.id}}" class="radio-custom-label"><img class="mCS_img_loaded" ng-src="{{font.img}}" err-src="<?= $basePath ?>pub/media/shirt-tool/default_shirttool_image/blank.png"/>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="ws-position clearfix">
                                                        <h6>INITIAL POSITION:</h6>
                                                        <div id="{{pos.name}}" class="cust-font-radio" ng-click="changeTextPos(pos.poss)"  ng-repeat="pos in position">
                                                            <input  id="radio-{{pos.id}}" class="radio-custom" name="radio-group" checked="" type="radio">
                                                            <label for="radio-{{pos.id}}" class="radio-custom-label">{{pos.poss}}</label>
                                                        </div>
                                                    </div>
                                                    <div class="ws-MONOGRAM clearfix">
                                                        <h6>MONOGRAM THREAD COLOR:</h6><br/>
                                                        <div id="monoColorHolder">
                                                            <ul class="custom-mono-color">
                                                                <li id="{{mono.id}}" ng-click="changeTextColor($event, mono.color)" ng-repeat="mono in monoColors">
                                                                    <img ng-src="{{mono.img}}" err-src="<?= $basePath ?>pub/media/shirt-tool/default_shirttool_image/blank.png">
                                                                    <!--<p class="price-info"><span><?php // echo $currencySymbol;             ?> </span><span class="price">{{mono.price ? mono.price:"0.0"}} </span></p>-->
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>


                                                <li class="optiontrigger" style_id="{{style.style_id}}" id="{{style.id}}" 
                                                    ng-repeat=" style in designStyles.style" index="{{$index}}" name="style" ng-disabled="" ng-click="accentClick($event, style, $index);">
                                                    <a href="javascript:void(0)">
                                                        <span class="option_icon"><i class="{{style.class}}"></i></span>
                                                        <!--<img class='innerImg' ng-src="<?php // echo $basePath; ?>{{style.img}}" err-src="<?php // echo $basePath; ?>pub/media/shirt-tool/default_shirttool_image/blank.png" alt="toolimg">-->
                                                        <span>{{style.name}} </span>
                                                        <p class="price-info"><span><?= $currencySymbol ?></span><span class="price">{{style.price ?style.price:"0.0"}} </span></p>
                                                    </a>
                                                </li>
                                            </ul>

                                            <ul ng-show="actCatName == 'CollarStyle'" class="CollarStyle">
                                                <p class="title">Select Fabric</p>
                                                <div class="fabric-bx clearfix fabcolelbow">
                                                    <fabricviewaccent ng-repeat="fabric in backColElboFabricData" ng-click="changeCollarContrast($event, fabric)"   index="{{$index}}" activeStyle="0" id="{{fabric.id}}" fabricName="{{fabric.name}}" ></fabricviewaccent>
                                                </div>
                                            </ul>

                                            <ul ng-show="actCatName == 'Cuffs'" class="Cuffs">
                                                <p class="title">Select Fabric</p>
                                                <div class="fabric-bx clearfix fabcolelbow" >
                                                    <fabricviewaccent ng-repeat="fabric in backColElboFabricData" ng-click="changeCuffContrast($event, fabric)"   index="{{$index}}" activeStyle="0" id="{{fabric.id}}" fabricName="{{fabric.name}}" ></fabricviewaccent>
                                                </div>
                                            </ul>
                                            <ul ng-show="actCatName == 'ElbowPatch'" class="ElbowPatch">
                                                <p class="title">Select Fabric</p>
                                                <div class="fabric-bx clearfix fabcolelbow">
                                                    <fabricviewaccent ng-repeat="fabric in backColElboFabricData" ng-click="changeElbowContrast($event, fabric)"   index="{{$index}}" activeStyle="0" id="{{fabric.id}}" fabricName="{{fabric.name}}" ></fabricviewaccent>
                                                </div>
                                            </ul>
                                            <ul ng-show="actCatName == 'Threads'" class="Threads">
                                                <p class="title">Select Thread</p>
                                                <div class="fabric-bx clearfix fabcolelbow">
                                                    <threadcolor ng-repeat="fabric in threadFabricData" ng-click="changeThreadContrast($event, fabric)"   index="{{$index}}" activeStyle="0" id="{{fabric.id}}" fabricName="{{fabric.name}}" ></threadcolor>
                                                </div>
                                            </ul>
                                            <ul ng-show="actCatName == 'FrontPlacket'" class="FrontPlacket">
                                                <p class="title">Select Fabric</p>
                                                <div class="fabric-bx clearfix fabcolelbow">
                                                    <fabricviewaccent ng-repeat="fabric in backColElboFabricData" ng-click="changePlacketContrast($event, fabric)"   index="{{$index}}" activeStyle="0" id="{{fabric.id}}" fabricName="{{fabric.name}}" ></fabricviewaccent>
                                                </div>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="col-md-6 col-sm-8  col-xs-12 rtldf" ng-class=" isActive ? 'activeUp':'activeDown'">
                        <!--inner loader start-->
                        <div class="suit-preloader">
                            <div class="suit-preloader-inner">
                                <img width="50" height="50" 
                                    src="<?= $basePath ?>pub/media/shirt-tool/tool_data/img/Idesignibuy_loader.gif">
                            </div>
                        </div>
                        <!--end-->

                        <div class="mainToolarea" style="margin-left: -2%;">
                            <div id="mainImageHolder" ng-click="addClassOfZoom">
                                <div ng-repeat="view in views" id="{{view.name}}" class="theView" 
                                    ng-class="{'active': ($index + 1) === activeView}">
                                    <img alt="" id="{{viewImg.id}}" rel="{{viewImg.rel}}" name="{{viewImg.name}}" 
                                        label="{{viewImg.parent}}" type="{{viewImg.type}}" 
                                        style="left:{{viewImg.xPos}}px; top:{{viewImg.yPos}}px;" ng-class="myClassForZoom" 
                                        class="{{viewImg.classname}}" ng-repeat="viewImg in view.viewimages" ng-src="{{viewImg.img}}" 
                                        err-src="<?= $basePath ?>pub/media/shirt-tool/default_tool_img/blank.png" 
                                        activeStyle="{{viewImg.activeStyle}}" activeStyleIndex="{{viewImg.activeStyleIndex}}" 
                                        price="{{viewImg.price}}" icon="{{viewImg.icon}}" 
                                    />  
                                </div>
                            </div>
                        </div>
                        <div class="view-thumb">
                            <button class="buttonStyl active" title="Front" ng-click="changeView($event, 1)" id="Front"></button>
                            <button class="buttonStyl" title="Side" ng-click="changeView($event, 2)" id="Back"></button>
                            <button class="buttonStyl" title="Back" ng-click="changeView($event, 3)" id="Inside"></button>
                            <button class="buttonStyl" title="Folded" ng-click="changeView($event, 4)" id="Fold"></button>
                            <button class="buttonStyl" title="CollarZoom" ng-click="changeView($event, 5)" id="CollarZoom"></button>
                        </div>
                    </div>

                    <div class="fabricInfo" ng-show="IsImage">
                        <a title="cancel" data-placement="top" class="cl" ng-click="hideFabricDiv()">X</a>

                        <span id="displayLargeImg"></span>
                        <div class="fabricDetails">
                            <p class="fabricName " >Upper Side</p>
                            <p class="fabricType " >Wool</p>
                            <p><span><?= $currencySymbol ?></span><span class="fabricPrice ng-binding">200</span></p>
                        </div>
                    </div>

                    <div class="navigate-menu">
                        <ul>
                            <!--<li><button data-toggle="modal" data-target="#measurement"> Add Body Type</button>  </li>-->
                            <li id="zoomButton" ng-show="IsZoom" ng-click="takeScreenShot()">
                                <a title="Zoom In" href="javascript:void(0)" data-toggle="modal"  class="ct-tooltip tool_preview_tip">
                                    <i class="icon-Zoom_In" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li id="casual_1" ng-click="shirtVisibletrue()" ng-show="IsShirt">
                                <a title="Formal" href="javascript:void(0)" style="font-size: 26px">
                                    <i aria-hidden="true" class=" icon-Cutaway"></i>
                                </a>
                            </li>
                            <li id="casual_2" ng-click="shirtVisiblefalse(this)" ng-show="!IsShirt">
                                <a style="font-size: 26px" title="Casual" href="javascript:void(0)">
                                    <i aria-hidden="true" class="icon-Casual_View"></i>
                                </a>
                            </li>
                            <!--For Formal Pant   -->
                            <li id="jeans" ng-click="pantVisibletrue()" ng-show="IsPant">
                                <a title="Formal Pant" href="javascript:void(0)" style="font-size: 26px">
                                    <i aria-hidden="true" class=" icon-Formal_Pant"></i>
                                </a>
                            </li>
                            <!--For Jeans  -->
                            <li  id="jeans" ng-click="pantVisiblefalse(this)" ng-show="!IsPant">
                                <a style="font-size: 26px" title="Show Jeans" href="javascript:void(0)">
                                    <i aria-hidden="true" class=" icon-Jeans"></i>
                                </a>
                            </li>
                            <!--Tie-->
                            <li id="tie" ng-click="tieVisibletrue()" ng-show="!IsTie">
                                <a title="Show Tie" href="javascript:void(0)" style="font-size: 26px">
                                    <i aria-hidden="true" class=" icon-Show_Tie"></i>
                                </a>
                            </li>
                            <li  id="tie" ng-click="tieVisiblefalse(this)" ng-show="IsTie">
                                <a style="font-size: 26px" title="Hide Tie" href="javascript:void(0)">
                                    <i aria-hidden="true" class="icon-Hide_Tie"></i>
                                </a>
                            </li>
                            <!--<li id="zoomOutButton" ng-show = "IsVisible" ng-click="addClassOfZoom()"><a title="Zoom Out" href="javascript:void(0)"> <i class=" icon-icon-116 " aria-hidden="true"></i> </a></li>-->

                            <li onClick="zoomButtonHide()" ng-click="isActive = !isActive" ng-show="!isActive" id="arrowUP"><a title="Up" href="javascript:void(0)"> <i class="icon-up" aria-hidden="true"></i> </a></li>

                            <li onClick="zoomButtonShow()" ng-click="isActive = !isActive" ng-show="isActive" id="arrowDown">
                                <a title="Down" href="javascript:void(0)"> <i  class="icon-down" aria-hidden="true" ></i> </a>
                            </li>
                            <!--
                                <li class="jH" ng-click="jacketVisibletrue()" ng-show="IsJacket">
                                    <a title="Hide Jacket" href="javascript:void(0)" style="font-size: 26px">
                                        <i aria-hidden="true" class="icon-icon-117"></i> </a>
                                </li>
                                <li  id="jacketHide" class="jH" ng-click="jacketVisiblefalse(this)" ng-show="!IsJacket">
                                    <a style="font-size: 26px" title="Show Jacket" href="javascript:void(0)">
                                        <i aria-hidden="true" class="icon-icon-118"></i>
                                    </a>
                                </li>
                            -->
                            <!--
                                <li class="jV" id="showVest" ng-click="vestVisibletrue()" ng-show="IsVest">
                                    <a title="Show Vest" href="javascript:void(0)" style="font-size: 26px">
                                        <i aria-hidden="true" class="icon-icon-120"></i> </a>
                                </li>
                                <li class="jV" id="vestHide" ng-click="vestVisiblefalse(this)" ng-show="!IsVest">
                                    <a style="font-size: 26px" title="Hide Vest" href="javascript:void(0)">
                                        <i aria-hidden="true" class="icon-icon-122"></i>
                                    </a>
                                </li>
                            -->
                            <li id="fabricSlid" ng-click="showFabricDiv()" >
                                <a title="Fabric" href="javascript:void(0)">
                                    <i class="icon-Fabric" aria-hidden="true"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-3 col-sm-12 col-xs-12 right-slide">
                        <div class="rightTool-nav">

                            <div class="rt-txt2">
                                <h1>Custom <span> Shirt </span></h1>
                            </div>
                            <div class="rt-price"> <span><?= $currencySymbol ?></span> <span orgBasePrice="<?= $productPrice ?>" id="basePrice"><?= $productPrice ?></span></div>
                            <div class="rt-btn"> <a href="javascript:void(0)" onClick="addToCart()"> Add To Cart <i class="fa fa-angle-double-right" aria-hidden="true"> </i> </a> </div>
                        </div>
                    </div>
                    <div id="viewport">
                        <div ng-repeat="view in viewsZoom" id="{{view.name}}" class="theView" ng-class="{'active': ($index + 5) === activeView}">
                            <img alt="" id="{{viewImg.id}}" rel="{{viewImg.rel}}" name="{{viewImg.name}}"  label="{{viewImg.parent}}" 
                                type="{{viewImg.type}}" style="left:{{viewImg.xPos}}px; top:{{viewImg.yPos}}px;" ng-class="myClassForZoom" class="{{viewImg.classname}} " ng-repeat="viewImg in view.viewimages" ng-src="{{viewImg.img}}" err-src="http://a2ztailor.com/WP_a2ztailor/media/default_shirttool_image/blank.png" icon="{{viewImg.icon}}" activeStyle="{{viewImg.activeStyle}}" activeStyleIndex="{{viewImg.activeStyleIndex}}" price="{{viewImg.price}}" 
                            />
                        </div>
                    </div>
                <!-- </div> -->
            </div>
        </section>

        <!-- Zoom PopUP start -->
        <div class="modal fade " id="zoomOfTool"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" >
                <div class="modal-content">

                    <div id="zoomClose" class="modal-header">
                        <button onClick="closeButton()" type="button" class="close close-btn" data-dismiss="modal" aria-label="Close">X</button>
                    </div>
                    <div class="modal-body">
                        <div id="prev-holder" class="prev-holder">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn pattern-btn save-btn tool_btn_submit" onClick="closeButton()">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Zoom PopUP end -->


        <!-- add measurement start-->
        <div class="modal fade" id="addMeasurement" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index:9999;">
            <div class="modal-dialog modal-lg" >
                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close close-btn" data-dismiss="modal" aria-label="Close">X</button>
                    </div>
                    <div class="modal-body row" style="margin-right:0px;margin-left: 0px;">
                        <div class="measurement col-xs-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-6  offset-bottom-15 size-opt" ng-click="showScanSize();">
                                    <div class="scnsize">
                                        <h3> Scan Size</h3>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, </p>
                                        <p> <a href="javascript:void(0);"> <i class="fa fa-arrow-circle-down fa-2x"> </i></a></p>
                                        <p><img src="<?= $basePath ?>pub/media/shirt-tool/tool_data/img/measurement-sc.png" class="img-responsive" /></p>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 offset-bottom-15 size-opt" ng-click="showStdSize();">
                                    <div class="scnsize">
                                        <h3>Standard Size</h3>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, </p>
                                        <p> <a href="javascript:void(0);"> <i class="fa fa-arrow-circle-down fa-2x"> </i></a></p>
                                        <p><img src="<?= $basePath ?>pub/media/shirt-tool/tool_data/img/SML.png" class="img-responsive">
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="measurement col-xs-12"  ng-show="scanSizeFlg" id="scanSizeFlgContainer">
                            <div class="">
                                <form class="form-horizontal">
                                    <div class="form-group">
                                        <label for="inputPassword3" class="col-xs-2 control-label"> <i class="fa fa-arrow-circle-right text-success"> </i> &nbsp; </label>
                                        <div class="col-xs-10">
                                            <label class="radio-inline">
                                                <input type="radio" name="unitScanOptions" value="cm" ng-model="unitScanName" value="cm" ng-change="changeScanUnit(unitScanName);">
                                                Cm </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="unitScanOptions" value="in" ng-model="unitScanName" value="inch" ng-change="changeScanUnit(unitScanName);"   checked="checked">
                                                Inch </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputPassword3" class="col-xs-2 control-label"> <i class="fa fa-arrow-circle-right text-success"> </i> &nbsp; </label>
                                        <div class="col-xs-10">  <label class="control-label"> Enter your measurements in the corresponding boxes </label> </div>
                                    </div>
                                    <hr>


                                    <div class="col-xs-12 col-sm-6" id="jacketScanSizes">
                                        <div class="form-group">

                                            <label for="inputEmail3" class="col-xs-5 control-label" style="font-weight:bold">Shirt</label>

                                            <div class="col-xs-7"> </div>
                                        </div>
                                        <div class="form-group">

                                            <label for="inputEmail3" class="col-xs-5 control-label">Chest</label>
                                            <div class="col-xs-6">
                                                <input type="text" id="chest" parent="Shirt" name="chest" class="form-control scan_input scan_number"  placeholder="Chest" required>
                                                <span id="reqNeck"></span>
                                            </div>
                                            <div class="col-xs-7"> </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-xs-5 control-label">Waist</label>

                                            <div class="col-xs-6">
                                                <input type="text" id="waist" parent="Shirt" name="waist" class="form-control scan_input scan_number"  placeholder="Waist">
                                                <span id="reqWaist"></span>
                                            </div>
                                            <div class="col-xs-7"> </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-xs-5 control-label">Hip</label>
                                            <div class="col-xs-6">
                                                <input type="text" id="hip" name="hip" parent="Shirt" class="form-control scan_input scan_number"  placeholder="Hip">
                                                <span id="reqHip"></span>
                                            </div>
                                            <div class="col-xs-7"> </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-xs-5 control-label">Shoulder</label>
                                            <div class="col-xs-6">
                                                <input type="text" id="shoulder" parent="Shirt" name="shoulder" class="form-control scan_input scan_number"  placeholder="Shoulder">
                                                <span id="reqShoulder"></span>
                                            </div>
                                            <div class="col-xs-7"> </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-xs-5 control-label">Sleeve</label>
                                            <div class="col-xs-6">
                                                <input type="text" id="sleeve" name="sleeve" parent="Shirt" class="form-control scan_input scan_number"  placeholder="Sleeve">
                                                <span id="reqSleeve"></span>
                                            </div>
                                            <div class="col-xs-7"> </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-xs-5 control-label">Length</label>
                                            <div class="col-xs-6">
                                                <input type="text" id="length" name="length" parent="Shirt" class="form-control scan_input scan_number"  placeholder="Length">
                                                <span id="reqLength"></span>
                                            </div>
                                            <div class="col-xs-7"> </div>
                                        </div>
                                    </div>
                                    <!--    
                                        <div class="col-xs-12 col-sm-6" id="pantScanSizes">
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-xs-5 control-label" style="font-weight:bold">Pant</label>
                                            <span></span>

                                            <div class="col-xs-7"> </div>
                                        </div>
                                        <div class="form-group">

                                            <label for="inputEmail3" class="col-xs-5 control-label">Waist</label>
                                            <div class="col-xs-6">
                                                <input type="text" id="pwaist" name="pwaist" parent="Pant" class="form-control scan_input scan_number"  placeholder="waist">
                                                <span id="reqPwaist"></span>
                                            </div>
                                            <div class="col-xs-7"> </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-xs-5 control-label">Hip</label>
                                            <div class="col-xs-6">
                                                <input type="text" id="phip" name="phip" parent="Pant" class="form-control scan_input scan_number"  placeholder="Hip">
                                                <span id="reqPhip"></span>
                                            </div>
                                            <div class="col-xs-7"> </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-xs-5 control-label">Crotch</label>
                                            <div class="col-xs-6">
                                                <input type="text" id="crotch" name="crotch" parent="Pant" class="form-control scan_input scan_number"  placeholder="Crotch">
                                                <span id="reqCrotch"></span>
                                            </div>
                                            <div class="col-xs-7"> </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-xs-5 control-label">Thigh</label>
                                            <div class="col-xs-6">
                                                <input type="text" id="thigh" name="thigh" parent="Pant" class="form-control scan_input scan_number"  placeholder="Thigh">
                                                <span id="reqThigh"></span>
                                            </div>
                                            <div class="col-xs-7"> </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-xs-5 control-label">Length</label>
                                            <div class="col-xs-6">
                                                <input type="text" id="plength" name="plength" parent="Pant" class="form-control scan_input scan_number"  placeholder="Length">
                                                <span id="reqPlength"></span>
                                            </div>
                                            <div class="col-xs-7"> </div>
                                        </div>
                                        </div
                                    >-->
                                    <div class="form-group">
                                        <div class="col-xs-12 col-sm-6">
                                            <label for="inputPassword3" class="col-xs-5 control-label">  Quantity </label>

                                            <div class="col-xs-6 col-sm-6">
                                                <select class="form-control scanQty">
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                    <option value="7">7</option>
                                                    <option value="8">8</option>
                                                    <option value="9">9</option>
                                                    <option value="10">10</option>
                                                    <option value="11">11</option>
                                                    <option value="12">12</option>
                                                    <option value="13">13</option>
                                                    <option value="14">14</option>
                                                    <option value="15">15</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <hr>
                                            <a href="javascript:void(0);" class="btn btn-success pull-right " onclick="saveScanSizeData();">Save</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="measurement col-xs-12" ng-show="stdSizeFlg" id="stdSizeFlgContainer">
                            <div class="">
                                <form class="form-horizontal">
                                    <div class="form-group">
                                        <label for="inputPassword3" class="col-xs-2 control-label"> <i class="fa fa-arrow-circle-right text-success"> </i> &nbsp; </label>
                                        <div class="col-xs-10">
                                            <label class="radio-inline">
                                                <input type="radio" name="unitStdRadioOptions" ng-model="unitName" value="cm" ng-change="changeUnit(unitName);">
                                                Cm </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="unitStdRadioOptions" ng-model="unitName" value="in" checked="checked" ng-change="changeUnit(unitName);">
                                                Inch </label>
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="table-responsive ">
                                            <table class="table table-bordered size">
                                                <thead>
                                                    <tr>
                                                        <th style="font-weight:bold">Shirt</th>
                                                    </tr>
                                                    <tr>
                                                        <th>SIZE</th>
                                                        <th ng-repeat="size in jacketSizes">{{size.size_name}}</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>Chest</td>
                                                        <td ng-repeat="size in jacketSizes" >{{size.chest * unitVal| number:0}}</td>

                                                    </tr>
                                                    <tr>
                                                        <td>Waist</td>
                                                        <td ng-repeat="size in jacketSizes">{{size.waist * unitVal| number:0}}</td>

                                                    </tr>
                                                    <tr>
                                                        <td>Hip</td>
                                                        <td ng-repeat="size in jacketSizes">{{size.hip * unitVal| number:0}}</td>

                                                    </tr>
                                                    <tr>
                                                        <td>Shoulder</td>
                                                        <td ng-repeat="size in jacketSizes">{{size.shoulder * unitVal| number:0}}</td>

                                                    </tr>
                                                    <tr>
                                                        <td>Sleeve</td>
                                                        <td ng-repeat="size in jacketSizes">{{size.sleeve * unitVal| number:0}}</td>

                                                    </tr>
                                                    <tr>
                                                        <td>Length</td>
                                                        <td ng-repeat="size in jacketSizes">{{size.length * unitVal| number:0}}</td>

                                                    </tr>

                                                </tbody>
                                            </table>
                                        </div>

                                        <div id="fit-holder">
                                            <div class="row">
                                                <label for="inputPassword3" class="col-xs-12 col-sm-3 control-label"> <i class="fa fa-arrow-circle-right text-success"> </i>  Select Your Fit </label>
                                                <div class="col-xs-12 col-sm-3">
                                                    <select class="form-control fit-select-box fit-select-box-default"  >

                                                        <!--<option value="-">Select</option> onChange="changeFit()"-->
                                                        <option ng-repeat="size in jacketSizes" value="{{size.size_name}}">{{size.size_name}}</option>

                                                    </select>
                                                </div>
                                                <label for="inputPassword3" class="col-xs-12 col-sm-2 control-label">  Quantity </label>

                                                <div class="col-xs-12 col-sm-2">
                                                    <select class="form-control stdQty">
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                        <option value="6">6</option>
                                                        <option value="7">7</option>
                                                        <option value="8">8</option>
                                                        <option value="9">9</option>
                                                        <option value="10">10</option>
                                                        <option value="11">11</option>
                                                        <option value="12">12</option>
                                                        <option value="13">13</option>
                                                        <option value="14">14</option>
                                                        <option value="15">15</option>
                                                    </select>
                                                </div>


                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <hr>
                                                <a href="javascript:void(0);" class="btn btn-success pull-right " onclick="saveStdSizeData();" >Save</a>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn pattern-btn save-btn">Close</button>
                    </div>
                </div>
            </div>
        </div>
</main>        

        <!--measurement end-->
    
        

        <!---toool ja files start-->
        <!-- <script>
            var basePath = "<?php //echo $basePath; ?>";
        </script>
        -->

        <link rel="stylesheet" type="text/css" href="">

        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js">
        </script>

        <script type="text/javascript">
            
        // function loadCssFiles(){
        //     var arrCssFiles = [
        //         '<?= $basePath ?>pub/media/shirt-tool/tool_data/css/bootstrap.min.css',
        //         '<?= $basePath ?>pub/media/shirt-tool/tool_data/css/jquery-ui.css',
        //         '<?= $basePath ?>pub/media/shirt-tool/tool_data/css/font-awesome.min.css',
        //         '<?= $basePath ?>pub/media/shirt-tool/tool_data/css/jquery-ui.css',
        //         '<?= $basePath ?>pub/media/shirt-tool/tool_data/css/owl.carousel.css',
        //         '<?= $basePath ?>pub/media/shirt-tool/tool_data/css/owl.theme.css',
        //         '<?= $basePath ?>pub/media/shirt-tool/tool_data/css/jquery.mCustomScrollbar.css',
        //         '<?= $basePath ?>pub/media/shirt-tool/tool_data/css/icon-library-v1.css',
        //         '<?= $basePath ?>pub/media/shirt-tool/tool_data/css/preloader.css',
        //         '<?= $basePath ?>pub/media/shirt-tool/tool_data/css/main.css',
        //         '<?= $basePath ?>pub/media/shirt-tool/tool_data/css/shirt-responsive.css',
        //         '<?= $basePath ?>pub/media/shirt-tool/tool_data/css/shirt-main.css'

        //     ];

            
        //     jQuery(arrCssFiles).each(function(indx, ele) {
        //         var strToappend = '<link rel="stylesheet" type="text/css" href="'+ele+'">';
        //         jQuery("main").prepend(strToappend);
        //     });
        // }



            function loadJsFiles() {

                var arrJsFiles = [
                    '<?= $basePath ?>pub/media/shirt-tool/tool_data/js/myjs.js',
                    '<?= $basePath ?>pub/media/shirt-tool/tool_data/js/library/jquery-1.10.2.min.js',
                    '<?= $basePath ?>pub/media/shirt-tool/tool_data/js/library/jquery-ui.js',
                    '<?= $basePath ?>pub/media/shirt-tool/tool_data/js/library/bootstrap.min.js',
                    '<?= $basePath ?>pub/media/shirt-tool/tool_data/js/library/owl.carousel.min.js',
                    '<?= $basePath ?>pub/media/shirt-tool/tool_data/js/library/scrollIt.min.js',
                    '<?= $basePath ?>pub/media/shirt-tool/tool_data/js/library/skrollr.min.js',
                    '<?= $basePath ?>pub/media/shirt-tool/tool_data/js/library/jquery.mCustomScrollbar.js',
                    '<?= $basePath ?>pub/media/shirt-tool/tool_data/js/library/jquery.elevatezoom.js',
                    '<?= $basePath ?>pub/media/shirt-tool/tool_data/js/library/angular.js',
                    '<?= $basePath ?>pub/media/shirt-tool/tool_data/js/library/angular-route.js',
                    '<?= $basePath ?>pub/media/shirt-tool/tool_data/js/library/angular-animate.js',
                    '<?= $basePath ?>pub/media/shirt-tool/tool_data/js/library/html2canvas.js',
                    '<?= $basePath ?>pub/media/shirt-tool/tool_data/js/pipl-shirt/app.js',
                    '<?= $basePath ?>pub/media/shirt-tool/tool_data/js/pipl-shirt/factory.js',
                    '<?= $basePath ?>pub/media/shirt-tool/tool_data/js/pipl-shirt/shirt-controller.js',
                    '<?= $basePath ?>pub/media/shirt-tool/tool_data/js/pipl-shirt/tool_custom.js',
                    '<?= $basePath ?>pub/media/shirt-tool/tool_data/js/pipl-shirt/custom-directive.js',
                    '<?= $basePath ?>pub/media/shirt-tool/tool_data/js/library/custom.js'

                ];
                jQuery(arrJsFiles).each(function(indx, ele) {
                    var strToappend = '<script type="text/javascript" src="' + ele + '"><' + '/script>';
                    jQuery("body").append(strToappend);
                });
            }
            jQuery(document).ready(function(){
                // loadCssFiles();
                setTimeout(function() {
                        loadJsFiles();
                        
                }, 500);
            });
        </script>
        <!-- 
        <script src="<?php //echo $basePath; ?>pub/media/shirt-tool/tool_data/js/library/jquery-1.10.2.min.js"></script>
        <script src="<?php //echo $basePath; ?>pub/media/shirt-tool/tool_data/js/library/jquery-ui.js"></script>
        <script src="<?php //echo $basePath; ?>pub/media/shirt-tool/tool_data/js/library/bootstrap.min.js"></script> 
        <script src="<?php //echo $basePath; ?>pub/media/shirt-tool/tool_data/js/library/owl.carousel.min.js"></script> 
        <script src="<?php //echo $basePath; ?>pub/media/shirt-tool/tool_data/js/library/scrollIt.min.js"></script>
        <script src="<?php //echo $basePath; ?>pub/media/shirt-tool/tool_data/js/library/skrollr.min.js"></script>
        <script src="<?php //echo $basePath; ?>pub/media/shirt-tool/tool_data/js/library/jquery.mCustomScrollbar.js"></script> 
        <script src="<?php //echo $basePath; ?>pub/media/shirt-tool/tool_data/js/library/jquery.elevatezoom.js"></script>

        <script src="<?php //echo $basePath; ?>pub/media/shirt-tool/tool_data/js/library/angular.js"></script>
        <script src="<?php //echo $basePath; ?>pub/media/shirt-tool/tool_data/js/library/angular-route.js"></script>
        <script src="<?php //echo $basePath; ?>pub/media/shirt-tool/tool_data/js/library/angular-animate.js"></script>
        <script src="<?php //echo $basePath; ?>pub/media/shirt-tool/tool_data/js/library/html2canvas.js"></script>


        <script src="<?php //echo $basePath; ?>pub/media/shirt-tool/tool_data/js/pipl-shirt/app.js"></script>
        <script src="<?php //echo $basePath; ?>pub/media/shirt-tool/tool_data/js/pipl-shirt/factory.js"></script>
        <script src="<?php //echo $basePath; ?>pub/media/shirt-tool/tool_data/js/pipl-shirt/shirt-controller.js"></script>
        <script src="<?php //echo $basePath; ?>pub/media/shirt-tool/tool_data/js/pipl-shirt/tool_custom.js"></script>
        <script src="<?php //echo $basePath; ?>pub/media/shirt-tool/tool_data/js/pipl-shirt/custom-directive.js"></script>
        <script src="<?php //echo $basePath; ?>pub/media/shirt-tool/tool_data/js/library/custom.js"></script>
 -->
        <!--- toool ja files end-->
       <!-- <script>
            $('#accordian h3').click(function(){
                $('body').toggleClass('rotate-menu');
            });
        </script>   
 --> 
        
<?php //die; ?>