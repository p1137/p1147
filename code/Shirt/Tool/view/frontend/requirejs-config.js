//var config = {
//    map: {
//        '*': {
//            myjs: 'Shirt_Tool/js/myjs',
//            jquery_ui: 'Shirt_Tool/js/library/jquery-ui',
//            jquery_1_10_2: 'Shirt_Tool/js/library/jquery-1.10.2.min',
//            bootstrap: 'Shirt_Tool/js/library/bootstrap.min',
//            owl: 'Shirt_Tool/js/library/owl.carousel.min',
//            scrollIt: 'Shirt_Tool/js/library/scrollIt.min',
//            skrollr: 'Shirt_Tool/js/library/skrollr.min',
//            mCustomScrollbar: 'Shirt_Tool/js/library/jquery.mCustomScrollbar',
//            jquery_elevatezoom: 'Shirt_Tool/js/library/jquery.elevatezoom',
//            angular: 'Shirt_Tool/js/library/angular',
//            angular_route: 'Shirt_Tool/js/library/angular-route',
//            angular_animate: 'Shirt_Tool/js/library/angular-animate',
//            html2canvas: 'Shirt_Tool/js/library/html2canvas',
//            app: 'Shirt_Tool/js/pipl-shirt/app',
//            factory: 'Shirt_Tool/js/pipl-shirt/factory',
//            shirt: 'Shirt_Tool/js/pipl-shirt/shirt-controller',
//            tool_custom: 'Shirt_Tool/js/pipl-shirt/tool_custom',
//            custom_directory: 'Shirt_Tool/js/pipl-shirt/custom-directive',
//            custom: 'Shirt_Tool/js/library/custom'
//        }
//    }
//};

/* 
 var config = {
    "paths": {
        bootstrap: 'Shirt_Tool/js/library/bootstrap.min',
        owl: 'Shirt_Tool/js/library/owl.carousel.min',
        scrollIt: 'Shirt_Tool/js/library/scrollIt.min',
        skrollr: 'Shirt_Tool/js/library/skrollr.min',
        mCustomScrollbar: 'Shirt_Tool/js/library/jquery.mCustomScrollbar',
        jquery_elevatezoom: 'Shirt_Tool/js/library/jquery.elevatezoom',
        angular: 'Shirt_Tool/js/library/angular',
        angular_route: 'Shirt_Tool/js/library/angular-route',
        angular_animate: 'Shirt_Tool/js/library/angular-animate',
        html2canvas: 'Shirt_Tool/js/library/html2canvas',
    },
    deps: [
        'Shirt_Tool/js/pipl-shirt/app',
        'Shirt_Tool/js/pipl-shirt/factory',
        'Shirt_Tool/js/pipl-shirt/shirt-controller',
        'Shirt_Tool/js/pipl-shirt/tool_custom',
        'Shirt_Tool/js/pipl-shirt/custom-directive',
        'Shirt_Tool/js/library/custom',
    ],
    "shim": {
        'angular': {},
        'angular_route': {},
        'angular_animate': {},
        'bootstrap':
                {
                    deps: ['jquery']
                },

        'scrollit':
                {
                    deps: ['jquery']
                },

        'skrollr':
                {
                    deps: ['jquery']
                },
        'owl':
                {
                    deps: ['jquery']
                },

        'mCustomScrollbar':
                {
                    deps: ['jquery']
                },

        'jquery_elevatezoom':
                {
                    deps: ['jquery']
                },
        'html2canvas':
                {
                    deps: ['jquery']
                }


    }
};