<?php
/**
* @author Dhirajkumar Deore    
* Copyright © 2018 Magento. All rights reserved.
* See COPYING.txt for license details.
*/
namespace Shirt\Tool\Model;

class Tool extends \Magento\Framework\Model\AbstractModel
{
        
        
    protected function _construct()


    {
        $this->_init('Shirt\Tool\Model\ResourceModel\Tool');
    }
        
        
    public function getAvailableStatuses()
    {
                
                
        $availableOptions = ['1' => 'Enable',
                          '0' => 'Disable'];
                
        return $availableOptions;
    }
}
