<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Shirt\Tool\Model\Config\Source;
/**
 * Catalog category landing page attribute source
 *
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Options extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{
    /**
     * {@inheritdoc}
     * @codeCoverageIgnore
     */
    public function getAllOptions()
    {
        if (!$this->_options) {
            $this->_options = [
                ['value' => 1, 'label' => __('Yes')],
                ['value' => 0, 'label' => __('No')],
               
            ];
        }
        return $this->_options;
    }
      /**
     * Get options in "key-value" format
     *
     * @return array
     */
    
}