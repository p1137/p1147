<?php

namespace Shirt\Tool\Model\Config\Source;

class Season implements \Magento\Framework\Option\ArrayInterface {

    public function toOptionArray() {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $colors = $objectManager->create('Shirt\Tool\Model\FabricSeason')->getCollection();
        foreach ($colors as $color) {
            $arr[] = ['value' => $color->getId(), 'label' => __($color->getTitle())];
        }
        return $arr;
    }

}
