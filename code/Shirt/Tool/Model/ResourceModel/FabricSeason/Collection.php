<?php
/**
* @author Akshay Shelke    
* Copyright © 2018 Magento. All rights reserved.
* See COPYING.txt for license details.
*/
namespace Shirt\Tool\Model\ResourceModel\FabricSeason;

use \Shirt\Tool\Model\ResourceModel\AbstractCollection;

class Collection extends AbstractCollection
{
    protected function _construct()
    {
        $this->_init('Shirt\Tool\Model\FabricSeason', 'Shirt\Tool\Model\ResourceModel\FabricSeason');
        $this->_map['fields']['season_id'] = 'main_table.season_id';
    }
}