<?php
/**
* @author Dhirajkumar Deore    
* Copyright © 2018 Magento. All rights reserved.
* See COPYING.txt for license details.
*/
namespace Shirt\Tool\Model\ResourceModel\Fabriccata;

use \Shirt\Tool\Model\ResourceModel\AbstractCollection;

class Collection extends AbstractCollection
{
    protected function _construct()
    {
        $this->_init('Shirt\Tool\Model\Fabriccata', 'Shirt\Tool\Model\ResourceModel\Fabriccata');
        $this->_map['fields']['category_id'] = 'main_table.category_id';
    }
}
