<?php
	/**
	* @author Akshay Shelke    
	* Copyright © 2018 Magento. All rights reserved.
	* See COPYING.txt for license details.
	*/
	namespace Shirt\Tool\Model\ResourceModel\ShirtShirtfabric;

	use \Shirt\Tool\Model\ResourceModel\AbstractCollection;

	class Collection extends AbstractCollection
	{
		protected $_idFieldName = 'fabric_id';
	    protected function _construct()
	    {
	        $this->_init('Shirt\Tool\Model\ShirtShirtfabric', 'Shirt\Tool\Model\ResourceModel\ShirtShirtfabric');
	        $this->_map['fields']['fabric_id'] = 'main_table.fabric_id';
	    }
	}
	