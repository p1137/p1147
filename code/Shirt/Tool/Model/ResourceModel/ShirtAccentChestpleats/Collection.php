<?php
	/**
	* @author Akshay Shelke    
	* Copyright © 2018 Magento. All rights reserved.
	* See COPYING.txt for license details.
	*/
	namespace Shirt\Tool\Model\ResourceModel\ShirtAccentChestpleats;

	use \Shirt\Tool\Model\ResourceModel\AbstractCollection;

	class Collection extends AbstractCollection
	{
	    protected function _construct()
	    {
	        $this->_init('Shirt\Tool\Model\ShirtAccentChestpleats', 'Shirt\Tool\Model\ResourceModel\ShirtAccentChestpleats');
	        $this->_map['fields']['chestpleats_id'] = 'main_table.chestpleats_id';
	    }
	}
	