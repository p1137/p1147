<?php
	/**
	* @author Akshay Shelke    
	* Copyright © 2018 Magento. All rights reserved.
	* See COPYING.txt for license details.
	*/
	namespace Shirt\Tool\Model\ResourceModel\ShirtStyle;

	use \Shirt\Tool\Model\ResourceModel\AbstractCollection;

	class Collection extends AbstractCollection
	{
	    protected function _construct()
	    {
	        $this->_init('Shirt\Tool\Model\ShirtStyle', 'Shirt\Tool\Model\ResourceModel\ShirtStyle');
	        $this->_map['fields']['shirtstyle_id'] = 'main_table.shirtstyle_id';
	    }
	}
	