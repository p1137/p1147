<?php 
	/**
	* @author Akshay Shelke    
	* Copyright © 2018 Magento. All rights reserved.
	* See COPYING.txt for license details.
	*/
	namespace Shirt\Tool\Model\ResourceModel;
	
	class ShirtShirtfabric extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
	{         
		 
	    protected function _construct()
	    {
	        $this->_init('shirt_shirtfabric', 'fabric_id');
	    }
	}
	