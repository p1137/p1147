<?php
	/**
	* @author Akshay Shelke    
	* Copyright © 2018 Magento. All rights reserved.
	* See COPYING.txt for license details.
	*/
	namespace Shirt\Tool\Model\ResourceModel\ShirtAccentElbowpatches;

	use \Shirt\Tool\Model\ResourceModel\AbstractCollection;

	class Collection extends AbstractCollection
	{
	    protected function _construct()
	    {
	        $this->_init('Shirt\Tool\Model\ShirtAccentElbowpatches', 'Shirt\Tool\Model\ResourceModel\ShirtAccentElbowpatches');
	        $this->_map['fields']['elbowpatches_id'] = 'main_table.elbowpatches_id';
	    }
	}
	