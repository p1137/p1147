<?php
/**
* @author Dhirajkumar Deore    
* Copyright © 2018 Magento. All rights reserved.
* See COPYING.txt for license details.
*/
namespace Shirt\Tool\Model\ResourceModel\Tool;

use \Shirt\Tool\Model\ResourceModel\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'shirt_tool_id';

    /**
     * Load data for preview flag
     *
     * @var bool
     */
    protected $_previewFlag;

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()


    {
        $this->_init('Shirt\Tool\Model\Tool', 'Shirt\Tool\Model\ResourceModel\Tool');
        $this->_map['fields']['shirt_tool_id'] = 'main_table.shirt_tool_id';
    }
}
