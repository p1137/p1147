<?php
namespace Shirt\Tool\Model\ResourceModel\ShirtAdmin;

	/**
	* @author Akshay Shelke    
	* Copyright © 2018 Magento. All rights reserved.
	* See COPYING.txt for license details.
	*/

	// use \Shirt\Tool\Model\ResourceModel\AbstractCollection;

	class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
	{
		protected $_idFieldName = 'fabric_id';
	    protected function _construct()
	    {
	        $this->_init('Shirt\Tool\Model\ShirtAdmin', 'Shirt\Tool\Model\ResourceModel\ShirtAdmin');
	        $this->_map['fields']['fabric_id'] = 'main_table.fabric_id';
	    }
	}