<?php
	/**
	* @author Akshay Shelke    
	* Copyright © 2018 Magento. All rights reserved.
	* See COPYING.txt for license details.
	*/
	namespace Shirt\Tool\Model\ResourceModel\ShirtAccentThreadcolor;

	use \Shirt\Tool\Model\ResourceModel\AbstractCollection;

	class Collection extends AbstractCollection
	{
	    protected function _construct()
	    {
	        $this->_init('Shirt\Tool\Model\ShirtAccentThreadcolor', 'Shirt\Tool\Model\ResourceModel\ShirtAccentThreadcolor');
	        $this->_map['fields']['threadcolor_id'] = 'main_table.threadcolor_id';
	    }
	}
	