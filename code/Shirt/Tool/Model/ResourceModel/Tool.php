<?php
/**
* @author Dhirajkumar Deore    
* Copyright © 2018 Magento. All rights reserved.
* See COPYING.txt for license details.
*/
namespace Shirt\Tool\Model\ResourceModel;

class Tool extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{          
    protected function _construct()
    {
        $this->_init('shirt_tool', 'shirt_tool_id');
    }
}
