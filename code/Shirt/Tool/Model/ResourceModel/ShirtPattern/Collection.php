<?php
	/**
	* @author Akshay Shelke    
	* Copyright © 2018 Magento. All rights reserved.
	* See COPYING.txt for license details.
	*/
	namespace Shirt\Tool\Model\ResourceModel\ShirtPattern;

	use \Shirt\Tool\Model\ResourceModel\AbstractCollection;

	class Collection extends AbstractCollection
	{
	    protected function _construct()
	    {
	        $this->_init('Shirt\Tool\Model\ShirtPattern', 'Shirt\Tool\Model\ResourceModel\ShirtPattern');
	        $this->_map['fields']['shirtpattern_id'] = 'main_table.shirtpattern_id';
	    }
	}
	