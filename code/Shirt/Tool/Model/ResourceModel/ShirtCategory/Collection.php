<?php
/**
* @author Akshay Shelke    
* Copyright © 2018 Magento. All rights reserved.
* See COPYING.txt for license details.
*/
namespace Shirt\Tool\Model\ResourceModel\ShirtCategory;

use \Shirt\Tool\Model\ResourceModel\AbstractCollection;

class Collection extends AbstractCollection
{
    protected function _construct()
    {
        $this->_init('Shirt\Tool\Model\ShirtCategory', 'Shirt\Tool\Model\ResourceModel\ShirtCategory');
        $this->_map['fields']['shirtcategory_id'] = 'main_table.shirtcategory_id';
    }
}