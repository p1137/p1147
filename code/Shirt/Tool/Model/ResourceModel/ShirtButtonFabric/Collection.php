<?php
	/**
	* @author Akshay Shelke    
	* Copyright © 2018 Magento. All rights reserved.
	* See COPYING.txt for license details.
	*/
	namespace Shirt\Tool\Model\ResourceModel\ShirtButtonFabric;

	use \Shirt\Tool\Model\ResourceModel\AbstractCollection;

	class Collection extends AbstractCollection
	{
	    protected function _construct()
	    {
	        $this->_init('Shirt\Tool\Model\ShirtButtonFabric', 'Shirt\Tool\Model\ResourceModel\ShirtButtonFabric');
	        $this->_map['fields']['button_fabric_id'] = 'main_table.button_fabric_id';
	    }
	}
	