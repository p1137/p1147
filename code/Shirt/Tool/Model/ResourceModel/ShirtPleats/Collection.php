<?php
	/**
	* @author Akshay Shelke    
	* Copyright © 2018 Magento. All rights reserved.
	* See COPYING.txt for license details.
	*/
	namespace Shirt\Tool\Model\ResourceModel\ShirtPleats;

	use \Shirt\Tool\Model\ResourceModel\AbstractCollection;

	class Collection extends AbstractCollection
	{
	    protected function _construct()
	    {
	        $this->_init('Shirt\Tool\Model\ShirtPleats', 'Shirt\Tool\Model\ResourceModel\ShirtPleats');
	        $this->_map['fields']['shirtpleats_id'] = 'main_table.shirtpleats_id';
	    }
	}
	