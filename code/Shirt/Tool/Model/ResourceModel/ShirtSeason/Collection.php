<?php
	/**
	* @author Akshay Shelke    
	* Copyright © 2018 Magento. All rights reserved.
	* See COPYING.txt for license details.
	*/
	namespace Shirt\Tool\Model\ResourceModel\ShirtSeason;

	use \Shirt\Tool\Model\ResourceModel\AbstractCollection;

	class Collection extends AbstractCollection
	{
	    protected function _construct()
	    {
	        $this->_init('Shirt\Tool\Model\ShirtSeason', 'Shirt\Tool\Model\ResourceModel\ShirtSeason');
	        $this->_map['fields']['shirtseason_id'] = 'main_table.shirtseason_id';
	    }
	}
	