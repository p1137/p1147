<?php
/**
* @author Akshay Shelke    
* Copyright © 2018 Magento. All rights reserved.
* See COPYING.txt for license details.
*/
namespace Shirt\Tool\Model\ResourceModel\FabricColor;

use \Shirt\Tool\Model\ResourceModel\AbstractCollection;

class Collection extends AbstractCollection
{
    protected function _construct()
    {
        $this->_init('Shirt\Tool\Model\FabricColor', 'Shirt\Tool\Model\ResourceModel\FabricColor');
        $this->_map['fields']['category_id'] = 'main_table.category_id';
    }
}
