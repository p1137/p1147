<?php
	/**
	* @author Akshay Shelke    
	* Copyright © 2018 Magento. All rights reserved.
	* See COPYING.txt for license details.
	*/
	namespace Shirt\Tool\Model\ResourceModel\ShirtPlackets;

	use \Shirt\Tool\Model\ResourceModel\AbstractCollection;

	class Collection extends AbstractCollection
	{
	    protected function _construct()
	    {
	        $this->_init('Shirt\Tool\Model\ShirtPlackets', 'Shirt\Tool\Model\ResourceModel\ShirtPlackets');
	        $this->_map['fields']['shirtplackets_id'] = 'main_table.shirtplackets_id';
	    }
	}
	