<?php
	/**
	* @author Akshay Shelke    
	* Copyright © 2018 Magento. All rights reserved.
	* See COPYING.txt for license details.
	*/
	namespace Shirt\Tool\Model\ResourceModel\ShirtSuitfabric;

	use \Shirt\Tool\Model\ResourceModel\AbstractCollection;

	class Collection extends AbstractCollection
	{
	    protected function _construct()
	    {
	        $this->_init('Shirt\Tool\Model\ShirtSuitfabric', 'Shirt\Tool\Model\ResourceModel\ShirtSuitfabric');
	        $this->_map['fields']['fabric_id'] = 'main_table.fabric_id';
	    }
	}
	