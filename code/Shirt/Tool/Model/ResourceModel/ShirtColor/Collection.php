<?php
/**
* @author Akshay Shelke    
* Copyright © 2018 Magento. All rights reserved.
* See COPYING.txt for license details.
*/
namespace Shirt\Tool\Model\ResourceModel\ShirtColor;

use \Shirt\Tool\Model\ResourceModel\AbstractCollection;

class Collection extends AbstractCollection
{
    protected function _construct()
    {
        $this->_init('Shirt\Tool\Model\ShirtColor', 'Shirt\Tool\Model\ResourceModel\ShirtColor');
        $this->_map['fields']['shirtcolor_id'] = 'main_table.shirtcolor_id';
    }
}