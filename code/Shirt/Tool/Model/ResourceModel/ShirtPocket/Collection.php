<?php
	/**
	* @author Akshay Shelke    
	* Copyright © 2018 Magento. All rights reserved.
	* See COPYING.txt for license details.
	*/
	namespace Shirt\Tool\Model\ResourceModel\ShirtPocket;

	use \Shirt\Tool\Model\ResourceModel\AbstractCollection;

	class Collection extends AbstractCollection
	{
	    protected function _construct()
	    {
	        $this->_init('Shirt\Tool\Model\ShirtPocket', 'Shirt\Tool\Model\ResourceModel\ShirtPocket');
	        $this->_map['fields']['shirtpocket_id'] = 'main_table.shirtpocket_id';
	    }
	}
	