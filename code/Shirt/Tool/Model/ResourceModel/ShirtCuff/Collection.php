<?php
/**
* @author Akshay Shelke    
* Copyright © 2018 Magento. All rights reserved.
* See COPYING.txt for license details.
*/


namespace Shirt\Tool\Model\ResourceModel\ShirtCuff;

use \Shirt\Tool\Model\ResourceModel\AbstractCollection;

class Collection extends AbstractCollection
{
    protected function _construct()

    {
        $this->_init('Shirt\Tool\Model\ShirtCuff', 'Shirt\Tool\Model\ResourceModel\ShirtCuff');
        $this->_map['fields']['shirtcuff_id'] = 'main_table.shirtcuff_id';
    }
}