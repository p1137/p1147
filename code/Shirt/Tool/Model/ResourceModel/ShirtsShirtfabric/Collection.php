<?php
	/**
	* @author Akshay Shelke    
	* Copyright © 2018 Magento. All rights reserved.
	* See COPYING.txt for license details.
	*/
	namespace Shirt\Tool\Model\ResourceModel\ShirtsShirtfabric;

	use \Shirt\Tool\Model\ResourceModel\AbstractCollection;

	class Collection extends AbstractCollection
	{
	    protected function _construct()
	    {
	        $this->_init('Shirt\Tool\Model\ShirtsShirtfabric', 'Shirt\Tool\Model\ResourceModel\ShirtsShirtfabric');
	        $this->_map['fields']['shirtfabric_id'] = 'main_table.shirtfabric_id';
	    }
	}
	