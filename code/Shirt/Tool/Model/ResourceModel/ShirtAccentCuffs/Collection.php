<?php
namespace Shirt\Tool\Model\ResourceModel\ShirtAccentCuffs;

	/**
	* @author Akshay Shelke    
	* Copyright © 2018 Magento. All rights reserved.
	* See COPYING.txt for license details.
	*/

	use \Shirt\Tool\Model\ResourceModel\AbstractCollection;

	class Collection extends AbstractCollection
	{
	    protected function _construct()
	    {
	        $this->_init('Shirt\Tool\Model\ShirtAccentCuffs', 'Shirt\Tool\Model\ResourceModel\ShirtAccentCuffs');
	        $this->_map['fields']['cuff_id'] = 'main_table.cuff_id';
	    }
	}
	