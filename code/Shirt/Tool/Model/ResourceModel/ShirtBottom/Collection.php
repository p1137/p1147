<?php
/**
* @author Akshay Shelke    
* Copyright © 2018 Magento. All rights reserved.
* See COPYING.txt for license details.
*/
namespace Shirt\Tool\Model\ResourceModel\ShirtBottom;

use \Shirt\Tool\Model\ResourceModel\AbstractCollection;

class Collection extends AbstractCollection
{
    protected function _construct()
    {
        $this->_init('Shirt\Tool\Model\ShirtBottom', 'Shirt\Tool\Model\ResourceModel\ShirtBottom');
        $this->_map['fields']['shirtbottom_id'] = 'main_table.shirtbottom_id';
    }
}