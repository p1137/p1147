<?php
/**
* @author Dhirajkumar Deore    
* Copyright © 2018 Magento. All rights reserved.
* See COPYING.txt for license details.
*/

namespace Shirt\Tool\Model\FabricImages;

use Shirt\Tool\Model\ResourceModel\ShirtShirtfabric\CollectionFactory;
use Magento\Framework\App\Request\DataPersistorInterface;

/**
 * Class DataProvider
 */
class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider {

    /**
     * @var \Magento\Cms\Model\ResourceModel\Block\Collection
     */
    protected $collection;

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;
    public $_storeManager;

    /**
     * @var array
     */
    protected $loadedData;

    /**
     * Constructor
     *
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $blockCollectionFactory
     * @param DataPersistorInterface $dataPersistor
     * @param array $meta
     * @param array $data
     */
    public function __construct(
    $name, $primaryFieldName, $requestFieldName, CollectionFactory $blockCollectionFactory, DataPersistorInterface $dataPersistor, \Magento\Store\Model\StoreManagerInterface $storeManager, array $meta = [], array $data = []
    ) {
        $this->collection = $blockCollectionFactory->create();
        $this->_storeManager = $storeManager;
        $this->dataPersistor = $dataPersistor;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData() {
        $temp=[];
        $tempthread=[];
        $baseurl = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);

        $baseurl = $baseurl.'shirt-tool/';   // edit fabric page admin panel
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        $items = $this->collection->getItems();
        /** @var \Magento\Cms\Model\Block $block */
        foreach ($items as $block) {
            $this->loadedData[$block->getId()] = $block->getData();


            $temp = $block->getData();

            // used to display the image in edit page after ulpoad that image
            if(isset($temp['display_fabric_thumb']) && $temp['display_fabric_thumb']!=''){
                $display_fabric_thumb = [];
                $display_fabric_thumb[0]['name'] = $temp['display_fabric_thumb'];
                $display_fabric_thumb[0]['url'] = $baseurl . $temp['display_fabric_thumb'];
                $temp['display_fabric_thumb'] = $display_fabric_thumb;
            }

            if(isset($temp['fabric_thumb']) && $temp['fabric_thumb']!=''){     
                $fabric_thumb = [];
                $fabric_thumb[0]['name'] = $temp['fabric_thumb'];
                $fabric_thumb[0]['url'] = $baseurl . $temp['fabric_thumb'];
                $temp['fabric_thumb'] = $fabric_thumb;
            }

            if(isset($temp['fabric_large_image']) && $temp['fabric_large_image']!=''){     
                $fabric_large_image = [];
                $fabric_large_image[0]['name']  = $temp['fabric_large_image'];
                $fabric_large_image[0]['url']   = $baseurl . $temp['fabric_large_image'];
                $temp['fabric_large_image']     = $fabric_large_image;
            }
        }

        $data = $this->dataPersistor->get('shirt_shirtfabric');

        if (!empty($data)) {
            $block = $this->collection->getNewEmptyItem();
            $block->setData($data);

            $this->loadedData[$block->getId()] = $block->getData();

            $this->dataPersistor->clear('shirt_shirtfabric');
        }

        if (empty($this->loadedData)) {
            return $this->loadedData;
        } else {
            
            if ($block->getData('display_fabric_thumb') != null) {
                $t2thread[$block->getId()] = $temp;
                return $t2thread;
            } else {
                return $this->loadedData;
            }
            
        }
    }
}
