<?php
/**
* @author Akshay Shelke    
* Copyright © 2018 Magento. All rights reserved.
* See COPYING.txt for license details.
*/
namespace Shirt\Tool\Model;

class ShirtCategory extends \Magento\Framework\Model\AbstractModel
{
    protected function _construct()

    {
        $this->_init('Shirt\Tool\Model\ResourceModel\ShirtCategory');
    }
}