<?php
/**
* @author Dhirajkumar Deore    
* Copyright © 2018 Magento. All rights reserved.
* See COPYING.txt for license details.
*/
namespace Shirt\Tool\Model\Tool\Source;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class Status
 */
class Type implements OptionSourceInterface
{
    
    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        
        return [
            ['value' => '1', 'label' => __('Notch')],
            ['value' => '2', 'label' => __('Peak')],
            ['value' => '3', 'label' => __('Shawl')]
        ];
    }
}
