<?php

/**
 * Webkul Hello CustomPrice Observer
 *
 * @category    Webkul
 * @package     Webkul_Hello
 * @author      Webkul Software Private Limited
 *
 * Created a event.xml
 */

namespace Shirt\Tool\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\RequestInterface;

class CustomPrice implements ObserverInterface {

    protected $_request;

    public function __construct(
    \Magento\Framework\App\RequestInterface $request
    ) {
        $this->_request = $request;
        $this->_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    }

    public function execute(\Magento\Framework\Event\Observer $observer) {
        // if (isset($_POST['prodImg'])) {
        //     $item = $observer->getEvent()->getData('quote_item');
        //     $item = ( $item->getParentItem() ? $item->getParentItem() : $item );
        //     $price = $this->_request->getParam('price');
        //     // print_r($json);
        //     $item->setCustomPrice($price);
        //     $item->setOriginalCustomPrice($price);

        //     $json_img = $this->_request->getParam('prodImg');

        //     $image = str_replace('data:image/png;base64,', '', $json_img);
        //     $name = 'img_' . time();

        //     // $storeManager = $this->_objectManager->get('\Magento\Store\Model\StoreManagerInterface');
        //     $directory = $this->_objectManager->get('\Magento\Framework\Filesystem\DirectoryList');
        //     // echo $directory->getRoot();
        //     $fp = fopen($directory->getRoot() . "/pub/media/shirt-tool/tool_data/img/customize-image/" . $name . ".png", "w+");

        //     fwrite($fp, base64_decode($image));
        //     fclose($fp);

        //     $product_image = "pub/media/shirt-tool/tool_data/img/customize-image/" . $name . ".png";

        //     $item->setJson(
        //             json_encode(array('product' => $product_image, 'style' => $_POST['style_data'], 'size' => $_POST['size_Data_Arr']))
        //     );
        //     $item->getProduct()->setIsSuperMode(true);
        // }
    }

}
