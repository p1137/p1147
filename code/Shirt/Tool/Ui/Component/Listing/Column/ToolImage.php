<?php
/**
* @author Dhirajkumar Deore    
* Copyright © 2018 Magento. All rights reserved.
* See COPYING.txt for license details.
*/
namespace Shirt\Tool\Ui\Component\Listing\Column;

use Magento\Catalog\Helper\Image;
use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Ui\Component\Listing\Columns\Column;

class ToolImage extends Column
{
    const ALT_FIELD = 'title';

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param Image $imageHelper
     * @param UrlInterface $urlBuilder
     * @param StoreManagerInterface $storeManager
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        Image $imageHelper,
        UrlInterface $urlBuilder,
        StoreManagerInterface $storeManager,
        array $components = [],
        array $data = []
    ) {
        $this->storeManager = $storeManager;
        $this->imageHelper = $imageHelper;
        $this->urlBuilder = $urlBuilder;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        // echo "<pre> Inside ToolImage";
        // print_r($this->getData());
        //     die;
        if (isset($dataSource['data']['items'])) {

            $fieldName = $this->getData('name');
            foreach ($dataSource['data']['items'] as & $item) {
                $url = '';
                if ($item[$fieldName] != '') {
                    $url = $this->storeManager->getStore()->getBaseUrl(
                        \Magento\Framework\UrlInterface::URL_TYPE_MEDIA
                    ).'shirt-tool/'.$item[$fieldName];     // grid list url in admin panel
                }
                $item[$fieldName . '_src'] = $url;
                $item[$fieldName . '_alt'] = $item['title'] ? $item['title'] : '';
                // $item[$fieldName . '_link'] = $this->urlBuilder->getUrl(
                //     'shirt_tool/index/edit',
                //     ['fabric_id' => $item['fabric_id']]
                // );
                $item[$fieldName . '_link'] = $this->urlBuilder->getUrl(
                    'shirt_tool/fabric/edit',
                    ['fabric_id' => $item['fabric_id']]
                );
                $item[$fieldName . '_orig_src'] = $url;
            }
        }
        
        return $dataSource;
    }
}
