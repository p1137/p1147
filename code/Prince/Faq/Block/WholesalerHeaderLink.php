<?php

namespace Prince\Faq\Block;

class WholesalerHeaderLink extends \Magento\Framework\View\Element\Html\Link {

    public function _toHtml() {
        $om = \Magento\Framework\App\ObjectManager::getInstance();
        $customerSession = $om->get('Magento\Customer\Model\Session');
        if (!($customerSession->isLoggedIn())) {
            return '<li><a ' . $this->getLinkAttributes() . ' >' . $this->escapeHtml($this->getLabel()) . '</a></li>';
        }        
    }

}
