<?php

namespace Wholesaler\Mgmt\Helper;

use Magento\Customer\Model\Session;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\UrlInterface;

class CustomerSession extends AbstractHelper
{

    const WHOLESALERID = 2;

    /**
     * @var Session
     */
    private $customerSession;
    /**
     * @var UrlInterface
     */
    private $urlInterface;

    public function __construct(
        Context $context,
        \Magento\Framework\Message\ManagerInterface $messageManager, 
        Session $customerSession
    )
    {
        parent::__construct($context);
        $this->customerSession = $customerSession;
        $this->messageManager = $messageManager;
        $this->urlInterface = $context->getUrlBuilder();
    }

    public function redirectIfNotLoggedIn()
    {
        if (!$this->customerSession->isLoggedIn()) {
            $this->customerSession->setAfterAuthUrl($this->urlInterface->getCurrentUrl());
            $this->customerSession->authenticate();
        }
    }
    /*
     *  Check wheather current customer is wholesaler or not
     */

    public function redirectIfNotWholesaler() {
        if ($this->customerSession->isLoggedIn()){
            $group_id = $this->customerSession->getCustomer()->getGroupId();
            if($group_id!= self::WHOLESALERID){

                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                $directory = $objectManager->get('\Magento\Framework\Filesystem\DirectoryList');
                $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
                $redirect = $objectManager->get('\Magento\Framework\App\Response\Http');
                $redirect->setRedirect($storeManager->getStore()->getBaseUrl()."customer/account/");            }
        }
    }

}