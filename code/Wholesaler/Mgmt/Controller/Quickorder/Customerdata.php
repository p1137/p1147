<?php

namespace Wholesaler\Mgmt\Controller\Quickorder;

class Customerdata extends \Magento\Framework\App\Action\Action {

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    private $resultPageFactory;

    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
    \Magento\Framework\App\Action\Context $context, 
    \Magento\Customer\Model\Session $customerSession,
    \Magento\Framework\View\Result\PageFactory $resultPageFactory, 
    \Magento\Framework\Controller\Result\JsonFactory $jsonFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->_customerSession = $customerSession;
        $this->jsonFactory = $jsonFactory;
        parent::__construct($context);
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute() {


        $data = $this->getRequest()->getPostValue();

        $cust_id = $data['cust_id'];
        $this->setSelectedCustomer($cust_id);

        $session_cust_id = $this->getSelectedCustomer();
        $arr = array();
        $arr['customerId'] = $session_cust_id;
        $result = $this->jsonFactory->create();
        return $result->setData($arr);
    }

    public function getCusomerSession() 
    {
        return $this->_customerSession;
    } 

    public function setSelectedCustomer($WholesalerCustomer)
    {
        $this->_customerSession->setWholesalerCustomer($WholesalerCustomer); //set value in customer session
    }

    public function getSelectedCustomer()
    {
        return $this->_customerSession->getWholesalerCustomer(); //Get value from customer session
    }

    public function unSetSelectedCustomer(){
        $this->_customerSession->start();
        return $this->_customerSession->unsWholesalerCustomer();
    }





}
