<?php
namespace Wholesaler\Mgmt\Controller\Quickorder;

use Magento\Sales\Controller\OrderInterface;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Index extends \Magento\Framework\App\Action\Action
{

 protected $resultPageFactory;

    public function __construct(
        Context $context,
        \Magento\Customer\Model\Session $customerSession,
        PageFactory $resultPageFactory
    ) {
        $this->_customerSession = $customerSession;
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }


    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->set(__('Order Management'));
        return $resultPage;

       // $this->_view->loadLayout();
       // $this->_view->renderLayout();


    }

    public function getCusomerSession() 
    {
        return $this->_customerSession;
    } 

    public function setSelectedCustomer($WholesalerCustomer)
    {
        $this->_customerSession->setWholesalerCustomer($WholesalerCustomer); //set value in customer session
    }

    public function getSelectedCustomer()
    {
        return $this->_customerSession->getWholesalerCustomer(); //Get value from customer session
    }

    public function unSetSelectedCustomer(){
        $this->_customerSession->start();
        return $this->_customerSession->unsWholesalerCustomer();
    }




}


