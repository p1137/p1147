<?php
namespace Wholesaler\Mgmt\Controller\Account;


class Deletecustomer extends \Magento\Framework\App\Action\Action {


    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param CommentFactory $commentFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Customer\Model\Customer $customerFactory,
        \Magento\Framework\Registry $registry    
    ) {
        $this->_customerFactory = $customerFactory;
        $this->registry = $registry;

        parent::__construct($context);
    }

    public function execute() 
    {

        $customerId = $this->getRequest()->getParam('id', false);
            try {
                $this->registry->register('isSecureArea', true);
                $customer = $this->_customerFactory->load($customerId);
                $customer->delete();
                $this->messageManager->addSuccess(__('You deleted the customer.'));
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
            }
        return $this->resultRedirectFactory->create()->setPath('manage_wholesaler/customer/');
        
 
    }

}