<?php
/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Wholesaler\Mgmt\Controller\Account;

use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Exception\InputException;
use Magento\Customer\Model\Customer\CredentialsValidator;

class ResetPasswordPost extends \Magento\Customer\Controller\AbstractAccount
{
    /**
     * @var \Magento\Customer\Api\AccountManagementInterface
     */
    protected $accountManagement;

    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    protected $customerRepository;

    /**
     * @var Session
     */
    protected $session;

    /**
     * @param Context $context
     * @param Session $customerSession
     * @param AccountManagementInterface $accountManagement
     * @param CustomerRepositoryInterface $customerRepository
     * @param CredentialsValidator|null $credentialsValidator
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function __construct(
        Context $context,
        Session $customerSession,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Customer\Model\Customer $customer,
        AccountManagementInterface $accountManagement,
        CustomerRepositoryInterface $customerRepository,
        CredentialsValidator $credentialsValidator = null
    ) {
        $this->session = $customerSession;
        $this->accountManagement = $accountManagement;
        $this->customerRepository = $customerRepository;
        $this->_storeManager = $storeManager;
        $this->customer = $customer;
        parent::__construct($context);
    }


    public function isWholesaler($email)
    {
            $website_id = $this->_storeManager->getStore()->getWebsiteId();
            $customer = $this->customer;
            if ($website_id) {
                $customer->setWebsiteId($website_id);
            }
            $customer->loadByEmail($email);
            if ($customer) {
                return $customer->getGroupId();
            }


    }


    /**
     * Reset forgotten password
     *
     * Used to handle data received from reset forgotten password form
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPost();

        $resetPasswordToken = (string)$this->getRequest()->getQuery('token');

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION'); 

        $result1 = $connection->fetchAll("SELECT * FROM customer_entity WHERE rp_token='".$resetPasswordToken."'");

        $cust_email = $result1[0]['email'];

        $password = (string)$this->getRequest()->getPost('password');
        $passwordConfirmation = (string)$this->getRequest()->getPost('password_confirmation');

        if ($password !== $passwordConfirmation) {
            $this->messageManager->addError(__("New Password and Confirm New Password values didn't match."));
            $resultRedirect->setPath(
                '*/*/createPassword',
                ['token' => $resetPasswordToken]
            );
            return $resultRedirect;
        }
        if (iconv_strlen($password) <= 0) {
            $this->messageManager->addError(__('Please enter a new password.'));
            $resultRedirect->setPath(
                '*/*/createPassword',
                ['token' => $resetPasswordToken]
            );
            return $resultRedirect;
        }

        try {

            $this->accountManagement->resetPassword(
                '',
                $resetPasswordToken,
                $password
            );
            $this->session->unsRpToken();

            $grpid = $this->isWholesaler($cust_email);

            $this->messageManager->addSuccess(__('You updated your password.'));

            if($grpid == 2)
                $resultRedirect->setPath('c_customer/wholesaler/login/');                
            else
                $resultRedirect->setPath('*/*/login');

            return $resultRedirect;
        } catch (InputException $e) {
            $this->messageManager->addError($e->getMessage());
            foreach ($e->getErrors() as $error) {
                $this->messageManager->addError($error->getMessage());
            }
        } catch (\Exception $exception) {
            $this->messageManager->addError(__('Something went wrong while saving the new password.'));
        }

        $resultRedirect->setPath(
            '*/*/createPassword',
            ['token' => $resetPasswordToken]
        );
        return $resultRedirect;
    }
}
