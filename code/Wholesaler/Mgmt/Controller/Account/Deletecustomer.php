<?php
namespace Wholesaler\Mgmt\Controller\Account;


class Deletecustomer extends \Magento\Framework\App\Action\Action {


    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param CommentFactory $commentFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Customer\Model\Customer $customerFactory,
        \Magento\Framework\Controller\Result\JsonFactory $jsonFactory,
        \Magento\Framework\Registry $registry    
    ) {
        $this->_customerFactory = $customerFactory;
        $this->registry = $registry;
        $this->jsonFactory = $jsonFactory;
        parent::__construct($context);
    }

    public function execute() 
    {

        $customerId = $this->getRequest()->getPost('id');

        try {
                $this->registry->register('isSecureArea', true);
                $customer = $this->_customerFactory->load($customerId);
                $customer->delete();
                $this->messageManager->addSuccess(__('You deleted the customer.'));
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
            }
            $result = $this->jsonFactory->create();
            return $result->setData(array('success'=>true));
 
    }

}