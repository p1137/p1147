<?php

namespace Wholesaler\Mgmt\Controller\Order;

use Magento\Sales\Controller\OrderInterface;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Filter extends \Magento\Framework\App\Action\Action {

    protected $_resultPageFactory;
    protected $_request;
    protected $_messageManager;

    public function __construct(
    \Magento\Framework\Message\ManagerInterface $messageManager, \Magento\Framework\Registry $registry, \Magento\Sales\Model\Order $order, Context $context, PageFactory $resultPageFactory, \Magento\Framework\App\RequestInterface $request
    ) {
        $this->_resultPageFactory = $resultPageFactory;
        $this->_request = $request;
        $this->_registry = $registry;
        $this->_order = $order;
        $this->_messageManager = $messageManager;
        parent::__construct($context);
    }

    public function execute() {
        return $this->_resultPageFactory->create();
    }

}
