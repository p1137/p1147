<?php

namespace Wholesaler\Mgmt\Controller\Order;

use Magento\Sales\Controller\OrderInterface;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Index extends \Magento\Framework\App\Action\Action {

    protected $resultPageFactory;
    protected $request;
    protected $messageManager;

    public function __construct(
    \Magento\Framework\Message\ManagerInterface $messageManager,
    \Magento\Framework\Registry $registry,
    \Magento\Sales\Model\Order $order, 
    Context $context, 
    \Magento\Framework\Controller\Result\JsonFactory $jsonFactory,
    PageFactory $resultPageFactory, 
    \Magento\Framework\App\RequestInterface $request
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->request = $request;
        $this->_registry = $registry;
        $this->jsonFactory = $jsonFactory;
        $this->_order = $order;
        $this->messageManager = $messageManager;
        parent::__construct($context);
    }

    public function execute() {
        try {
            $orderId = $this->getRequest()->getPost('id');
            $order = $this->_order->load($orderId);
            $this->_registry->register('isSecureArea', 'true');
            $order->delete();
            $this->_registry->unregister('isSecureArea');
            $this->messageManager->addSuccess(__("Order has been deleted successfully"));
        } catch (Exception $e) {
            $this->messageManager->addError(__("Unable to delete order #" . $orderId));
        }

        $result = $this->jsonFactory->create();
        return $result->setData(array('success'=>true));

    }

}
