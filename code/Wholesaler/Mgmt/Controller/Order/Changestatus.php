<?php

namespace Wholesaler\Mgmt\Controller\Order;

use Magento\Sales\Controller\OrderInterface;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\ResultFactory;

class Changestatus extends \Magento\Framework\App\Action\Action {

    protected $resultPageFactory;
    protected $request;
    protected $messageManager;
    protected $_resultFactory;

    /**
     * @var \Magento\Sales\Api\OrderRepositoryInterface
     */
    protected $_orderRepository;
 
    /**
     * @var \Magento\Sales\Model\Service\InvoiceService
     */
    protected $_invoiceService;
 
    /**
     * @var \Magento\Framework\DB\Transaction
     */
    protected $_transaction;
    /**
     * @var \Magento\Sales\Model\Convert\Order
     */
    protected $_convertOrder;
 
    /**
     * @var \Magento\Shipping\Model\ShipmentNotifier
     */
    protected $_shipmentNotifier;


    public function __construct(
    ResultFactory $resultFactory, 
    \Magento\Framework\Message\ManagerInterface $messageManager, 
    \Magento\Framework\Registry $registry, 
    \Magento\Sales\Model\Order $order, 
    Context $context, 
    PageFactory $resultPageFactory, 
    \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
    \Magento\Sales\Model\Service\InvoiceService $invoiceService,
    \Magento\Framework\DB\Transaction $transaction,
    \Magento\Sales\Model\Convert\Order $convertOrder,
    \Magento\Shipping\Model\ShipmentNotifier $shipmentNotifier,
    \Magento\Framework\App\RequestInterface $request
    ) {
        $this->_resultFactory = $resultFactory;
        $this->resultPageFactory = $resultPageFactory;
        $this->request = $request;
        $this->_registry = $registry;
        $this->_order = $order;
        $this->messageManager = $messageManager;
        $this->_orderRepository = $orderRepository;
        $this->_invoiceService = $invoiceService;
        $this->_transaction = $transaction;
        $this->_convertOrder = $convertOrder;
        $this->_shipmentNotifier = $shipmentNotifier;

        parent::__construct($context);
    }

    public function execute() {
        try {
            $orderIncrementId = $this->request->getParam('order_id');
            $orderStatus = $this->request->getParam('order_status');
            $order = $this->_order->loadByIncrementId($orderIncrementId);
            $orderId = $order->getId();

            $this->_registry->register('isSecureArea', 'true');
            if($orderStatus == 'pending'){
                $state = 'new';
            }else if($orderStatus == 'fraud'){
                $state = 'processing';
            }else{
                $state = $orderStatus;
            }


            if($orderStatus == 'complete'){

                $this->generateInvoice($orderId,$order,$orderStatus,$state);

            }else{

                $this->statusChange($order,$orderStatus,$state);
            }
        } catch (Exception $e) {
            $this->messageManager->addError(__("Unable to update order #" . $orderIncrementId));
        }        

    }

    public function statusChange($order,$orderStatus,$state)
    {
        $order->setState($state)->setStatus($orderStatus);
        $order->save();
        $this->_registry->unregister('isSecureArea');
        $this->messageManager->addSuccess(__("Order has been updated successfully"));
        $this->_redirect('sales/order/history');
    }

    public function generateInvoice($orderId,$order,$orderStatus,$state){
          $ordertoInvoice = $this->_orderRepository->get($orderId);
          if($ordertoInvoice->canInvoice()) {
            $invoice = $this->_invoiceService->prepareInvoice($ordertoInvoice);
            $invoice->register();
            $invoice->save();
            $transactionSave = $this->_transaction->addObject(
                $invoice
            )->addObject(
                $invoice->getOrder()
            );
            $transactionSave->save();
            // $this->invoiceSender->send($invoice);
            //send notification code
            $ordertoInvoice->addStatusHistoryComment(
                __('Notified customer about invoice #%1.', $invoice->getId())
            )
            ->setIsCustomerNotified(true)
            ->save();

            // --------------- Also generate shipmemt ------------------------//

            $this->generateShipment($orderId,$order,$orderStatus,$state);

            // --------------- Save status ------------------------//

            $order->setState($state)->setStatus($orderStatus);
            $order->save();
            $this->_registry->unregister('isSecureArea');
            $this->messageManager->addSuccess(__("Order has been updated successfully"));
            $this->_redirect('sales/order/view/order_id/'.$orderId);

        }else{
            $order->setState($state)->setStatus($orderStatus);
            $order->save();
            $this->_registry->unregister('isSecureArea');
            $this->messageManager->addError(__("Invoice and Shipment already submitted"));
            $this->_redirect('sales/order/history');                
        }
    }

    public function generateShipment($orderId,$order,$orderStatus,$state){
        $ordertoShip = $this->_orderRepository->get($orderId);
 
        // to check order can ship or not
        if ($ordertoShip->canShip()) {
            $orderShipment = $this->_convertOrder->toShipment($ordertoShip);
            foreach ($ordertoShip->getAllItems() AS $orderItem) {
     
             // Check virtual item and item Quantity
             if (!$orderItem->getQtyToShip() || $orderItem->getIsVirtual()) {
                continue;
             }
     
             $qty = $orderItem->getQtyToShip();
             $shipmentItem = $this->_convertOrder->itemToShipmentItem($orderItem)->setQty($qty);
             $orderShipment->addItem($shipmentItem);

            }
     
            $orderShipment->register();
            $orderShipment->getOrder()->setIsInProcess(true);
            // Save created Order Shipment
            $orderShipment->save();
            $orderShipment->getOrder()->save();
            // Send Shipment Email
            $this->_shipmentNotifier->notify($orderShipment);
            $orderShipment->save();
            $order->setState($state)->setStatus($orderStatus);
            $order->save();
            $this->_registry->unregister('isSecureArea');
            $this->messageManager->addSuccess(__("Order has been updated successfully"));
            $this->_redirect('sales/order/view/order_id/'.$orderId);

        }else{
            $order->setState($state)->setStatus($orderStatus);
            $order->save();
            $this->_registry->unregister('isSecureArea');
            $this->messageManager->addError(__("Shipment already submitted"));
            $this->_redirect('sales/order/history');                
        }
 

    }

}
