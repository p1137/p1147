<?php

namespace Wholesaler\Mgmt\Controller\Finance;

use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\ResultFactory;

class Export extends \Magento\Framework\App\Action\Action {

    protected $resultPageFactory;
    protected $request;
    protected $messageManager;
    protected $resultFactory;

    protected $fileFactory;
    protected $csvProcessor;
    protected $directoryList;

    public function __construct(
        PageFactory $resultPageFactory, 
        \Magento\Sales\Model\ResourceModel\Order\Invoice\CollectionFactory $invoiceCollection, 
        ResultFactory $resultFactory, 
        \Magento\Sales\Model\OrderFactory $order,
        \Magento\Framework\Message\ManagerInterface $messageManager, 
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Framework\Registry $registry, 
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
        \Magento\Sales\Model\Order\InvoiceFactory $invoice,
        \Magento\Customer\Model\Customer $customers, 
        \Magento\Framework\File\Csv $csvProcessor,
        \Magento\Framework\App\Filesystem\DirectoryList $directoryList,
        \Magento\Framework\View\Result\LayoutFactory $resultLayoutFactory,
        \Magento\Customer\Model\ResourceModel\Customer\CollectionFactory $customerFactory,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory, 
        Context $context 
    ) {
        $this->_resultFactory = $resultFactory;
        $this->resultPageFactory = $resultPageFactory;
        $this->request = $request;
        $this->_registry = $registry;
        $this->_order = $order;
        $this->invoiceModel = $invoice;
        $this->_invoiceCollection = $invoiceCollection;
        $this->messageManager = $messageManager;
        $this->_customers = $customers;
        $this->_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->_customerSession = $customerSession;
        $this->fileFactory = $fileFactory;
        $this->csvProcessor = $csvProcessor;
        $this->directoryList = $directoryList;
        $this->_resultLayoutFactory = $resultLayoutFactory;
        $this->_customerFactory = $customerFactory;
        $this->_orderCollectionFactory = $orderCollectionFactory;
        parent::__construct($context);

    }

    public function execute()
    {

        $params = $this->getRequest()->getPostValue();

        $fileName = 'invoice_report.csv';
        $filePath = $this->directoryList->getPath(\Magento\Framework\App\Filesystem\DirectoryList::VAR_DIR). "/" . $fileName;

        $invoice = $this->getWholesalerInvoices($params);

        $exportData = $this->getExportData($invoice);

        if(count($exportData)>1){
        $this->csvProcessor
            ->setDelimiter(';')
            ->setEnclosure('"')
            ->saveData(
                $filePath,
                $exportData
            );

        return $this->fileFactory->create(
            $fileName,
            [
                'type' => "filename",
                'value' => $fileName,
                'rm' => true,
            ],
            \Magento\Framework\App\Filesystem\DirectoryList::VAR_DIR,
            'application/octet-stream'
        );

        }else{
        
        $this->messageManager->addError(__('Cannot found data to export.'));
        return $this->resultRedirectFactory->create()->setPath('manage_wholesaler/finance/');

        }

    }


    public function getExportData($invoice)
    {
        $result = [];
        $result[] = [
            'Invoice No',
            'Invoice Date',
            'Order No',
            'Order Date',
            'Bill To Name',
            'Client Name',
            'Status',
            'Grand Total(Base)',
            'Grand Total(Purchased)'
        ];

        foreach ($invoice as $_invoice) {
            $result[] = [
                $_invoice->getIncrementId(),
                $_invoice->getCreatedAt(),
                $this->getOrderIncrementId($_invoice->getIncrementId()),
                $this->getOrderDate($_invoice->getIncrementId()),
                $this->getCustomerName($_invoice->getIncrementId()),
                $this->getClientName($_invoice->getIncrementId()),
                'Paid',
                $_invoice->getBaseGrandTotal(),
                $_invoice->getGrandTotal()
            ];
        }

        return $result;
    }
    public function getWholesalerInvoices($params) {
       
        $data = $params;

        $customerId = $this->_customerSession->getId();
        $orderCollection = $this->_orderCollectionFactory->create($customerId)
                    ->addFieldToSelect('*');

        $invoiceIds = array();

        foreach ($orderCollection as $key=>$value) {
            $orderIncrementId = $value->getIncrementId();
            $order = $this->_order->create()->loadByIncrementId($orderIncrementId);
             foreach ($order->getInvoiceCollection() as $invoice)
             {
                $invoiceIds[] = $invoice->getId();
             }
               
        }

        $invoiceCollection = $this->_invoiceCollection->create()->addFieldToSelect('*');
        $invoiceCollection->addFieldToFilter('entity_id', ['in' => $invoiceIds]);

        if (array_key_exists('reset', $data)){
            $this->_customerSession->unsInvoiceFilter();
        }

        if (count($data) > 5 || count($this->_customerSession->getInvoiceFilter())>4) {

            if (!array_key_exists('reset', $data)) {

                $invoice_filter = $this->_customerSession->getInvoiceFilter();


                $this->_customerSession->unsOrderFilter();
                
                if(isset($invoice_filter) && !isset($data['submit'])){

                    if($invoice_filter['date_from']!=''){
                        $data['date_from'] = $invoice_filter['date_from'];
                    }
                    if($invoice_filter['date_to']!=''){
                        $data['date_to'] = $invoice_filter['date_to'];
                    }
                    if($invoice_filter['order_date_from']!=''){
                        $data['order_date_from'] = $invoice_filter['order_date_from'];
                    }
                    if($invoice_filter['order_date_to']!=''){
                        $data['order_date_to'] = $invoice_filter['order_date_to'];
                    }
                    if($invoice_filter['key_word']!=''){
                        $data['key_word'] = $invoice_filter['key_word'];
                    }
                }



                $dateFrom = (isset($data['date_from'])) ? $data['date_from'] : "";
                $dateTo = (isset($data['date_to'])) ? $data['date_to'] : "";
                $orderDateFrom = (isset($data['order_date_from'])) ? $data['order_date_from'] : "";
                $orderDateTo = (isset($data['order_date_to'])) ? $data['order_date_to'] : "";

                $keyWord = (isset($data['key_word'])) ? $data['key_word'] : "";
                $keyArray = explode(" ",$keyWord);   

                $orderCollection = $this->_orderCollectionFactory->create($customerId)
                            ->addFieldToSelect('*');

                $invoiceIds = array();

                foreach ($orderCollection as $key=>$value) {
                    $orderIncrementId = $value->getIncrementId();
                    $order = $this->_order->create()->loadByIncrementId($orderIncrementId);
                     foreach ($order->getInvoiceCollection() as $invoice)
                     {
                        $invoiceIds[] = $invoice->getId();
                     }
                       
                }

                $invoiceCollection = $this->_invoiceCollection->create()->addFieldToSelect('*');
                $invoiceCollection->addFieldToFilter('entity_id', ['in' => $invoiceIds]);



                $filter = array('date_from'=>$dateFrom, 'date_to'=>$dateTo,'order_date_from'=>$orderDateFrom, 'order_date_to'=>$orderDateTo,'key_word'=>$keyWord ); 

                $this->_customerSession->setInvoiceFilter($filter);


                /*
                 * Filter by invoice Create at
                 */
                if (!empty($dateFrom)) {
                    if (!empty($dateTo)) {
                        $from = date('Y-m-d H:i:s', strtotime($dateFrom));
                        $to = date('Y-m-d H:i:s', strtotime($dateTo));
                        $invoiceCollection->addFieldToFilter('created_at', array('from' => $from, 'to' => $to))->setOrder(
                                'created_at', 'asc'
                        );
                        $this->_requestedParameters['date_from'] = $dateFrom;
                        $this->_requestedParameters['date_to'] = $dateTo;
                    } else {
                        $from = date('Y-m-d H:i:s', strtotime($dateFrom));
                        $invoiceCollection->addFieldToFilter('created_at', array('from' => $from))->setOrder(
                                'created_at', 'asc'
                        );
                        $this->_requestedParameters['date_from'] = $dateFrom;
                    }
                }
                /*
                 * Filter by order Create at
                 */
                if (!empty($orderDateFrom)) {
                    $orderIds = array();
                    $orderCollection = $this->_orderCollectionFactory->create($customerId)
                                ->addFieldToSelect('*');

                    if (!empty($orderDateTo)) {
                        $o_from = date('Y-m-d H:i:s', strtotime($orderDateFrom));
                        $o_to = date('Y-m-d H:i:s', strtotime($orderDateTo));
                        $orderCollection->addFieldToFilter('created_at', array('from' => $o_from, 'to' => $o_to))->setOrder(
                                'created_at', 'asc'
                        );
                        $this->_requestedParameters['order_date_from'] = $orderDateFrom;
                        $this->_requestedParameters['order_date_to'] = $orderDateTo;
                    } else {
                        $o_from = date('Y-m-d H:i:s', strtotime($orderDateFrom));
                        $orderCollection->addFieldToFilter('created_at', array('from' => $o_from))->setOrder(
                                'created_at', 'asc'
                        );
                        $this->_requestedParameters['order_date_from'] = $orderDateFrom;
                    }


                    if($orderCollection->getData()){
                        foreach ($orderCollection as $order){
                            $orderIds[] = $order->getId();
                        }
                        $invoiceCollection->addFieldToFilter('order_id', $orderIds)->setOrder(
                                'created_at', 'asc'
                        );
                  
                    }

                }

                /*
                 * Filter by key word
                 */

                if (!empty($keyWord)) {
                    $orderIds = array();
                    $orderCollection = $this->_orderCollectionFactory->create($customerId)
                                ->addFieldToSelect('*');

                    if(count($keyArray)>1){
                        $customerCollection = $this->getCustomerCollection()->addAttributeToFilter(
                                array(
                                    array('attribute' => 'firstname', 'eq' => $keyArray[0])
                                ),
                                array(
                                    array('attribute' => 'lastname', 'eq' => $keyArray[1])
                                )
                        );
                    }else{
                        $customerCollection = $this->getCustomerCollection()->addAttributeToFilter(
                                array(
                                    array('attribute' => 'firstname', 'like' => "%" . $keyWord . "%"),
                                    array('attribute' => 'lastname', 'like' => "%" . $keyWord . "%")
                                )
                        );

                    }

                    if($customerCollection->getData()){
                    foreach ($customerCollection as $customer){
                        $customerIds[] = $customer->getId();
                    }
                    $orderCollection->addFieldToFilter('wholesalers_customer_id', $customerIds)->setOrder(
                            'created_at', 'asc'
                    );
                  
                    }else{

                        $invoiceCollection->addFieldToFilter(['increment_id'],
                        [
                            ['like' => "%" . $keyWord . "%"]
                        ]);
                    }

                    if($orderCollection->getData()){
                        foreach ($orderCollection as $order){
                            $orderIds[] = $order->getId();
                        }
                        $invoiceCollection->addFieldToFilter('order_id', $orderIds)->setOrder(
                                'created_at', 'asc'
                        );
                  
                    }
                    $this->_requestedParameters['key_word'] = $keyWord;
                }


            }
        }
        
            return $invoiceCollection;

    }

    protected function getCustomerCollection() {
        return $this->_customerFactory->create();
    }

    public function getOrderIncrementId($invoiceId)
    {
        $invoice = $this->invoiceModel->create()->loadByIncrementId($invoiceId);
        $order = $invoice->getOrder();
        $incrementId = $order->getIncrementId();
        return $incrementId;
    }

    public function getOrderDate($invoiceId)
    {
        $invoice = $this->invoiceModel->create()->loadByIncrementId($invoiceId);
        $order = $invoice->getOrder();
        $orderDate = $order->getCreatedAt();
        return $orderDate;
    }

    public function getCustomerName($invoiceId)
    {
        $invoice = $this->invoiceModel->create()->loadByIncrementId($invoiceId);
        $order = $invoice->getOrder();
        $name = $order->getCustomerName();
        return $name;
    }    

    public function getClientName($invoiceId)
    {
        $invoice = $this->invoiceModel->create()->loadByIncrementId($invoiceId);
        $order = $invoice->getOrder();
        $wholesaler_custId = $order->getWholesalersCustomerId();

        if($wholesaler_custId){
            $customer = $this->_customers->load($wholesaler_custId);
            return $customer->getFirstname() . " " . $customer->getLastname();
        }else
        return "-";

    }   


}
