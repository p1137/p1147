<?php
/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Wholesaler\Mgmt\Controller\Measurement;

use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class SaveMeasurement extends \Magento\Framework\App\Action\Action
{

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Wholesaler\Mgmt\Model\Measurement $measurement,
        \Wholesaler\Mgmt\Model\MeasurementFactory $measurementfactory
    ) {
        $this->measurement = $measurement;
        $this->measurementfactory = $measurementfactory;
        parent::__construct($context);
    }   
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        $cust_id = $data['customer_id'];
        $cnt = 0;


        $sample = $this->measurementfactory->create();

        $collection = $sample->getCollection()->addFieldToFilter('customer_id', array('eq' => $cust_id));
         foreach($collection as $item)
          {
            $cnt++;
            $cust_data = $item->getData();
            $measurement_id = $cust_data['measurement_id'];
          } 

        if($cnt>=1){
            try 
            {
                $model1 = $this->measurementfactory->create();
                $postUpdate = $model1->load($measurement_id);
                $postUpdate->setJson(json_encode($data));
                $postUpdate->save();
                $this->messageManager->addSuccess( __('Your measurement details are successfully added') );
                $this->_redirect('manage_wholesaler/account/info/id/'.$cust_id);
                return;        
            }
            catch (\Exception $e)
            {
                $redirectUrl = $this->_buildUrl('manage_wholesaler/account/info/id/'.$cust_id);
                $this->messageManager->addExceptionMessage($e, __('We can\'t save the measurement details.'));
            }


        }
        else
        {
            try 
            {
                $model = $this->measurement;
                $model->setJson(json_encode($data));
                $model->setCustomerId($cust_id);
                $model->save();
                $this->messageManager->addSuccess( __('Your measurement details are successfully added') );
                $this->_redirect('manage_wholesaler/account/info/id/'.$cust_id);
                return;        
            }
            catch (\Exception $e)
            {
                $redirectUrl = $this->_buildUrl('manage_wholesaler/account/info/id/'.$cust_id);
                $this->messageManager->addExceptionMessage($e, __('We can\'t save the measurement details.'));
            }
        }




    }

}