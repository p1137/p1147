<?php
namespace Wholesaler\Mgmt\Controller\Measurement;


class Deletemeasurement extends \Magento\Framework\App\Action\Action {


    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param CommentFactory $commentFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Wholesaler\Mgmt\Model\MeasurementFactory $measurementfactory,
        \Magento\Framework\Registry $registry    
    ) {
        $this->measurementfactory = $measurementfactory;
        $this->registry = $registry;
        parent::__construct($context);
    }

    public function execute() 
    {

        $cust_id = $this->getRequest()->getParam('id', false);
        $measurementId = $this->getRequest()->getParam('m_id', false);
            try {
                $this->registry->register('isSecureArea', true);
                $measurement = $this->measurementfactory->create()->load($measurementId);
                $measurement->delete();
                $this->messageManager->addSuccess(__('You deleted your Measurement Record.'));
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
            }
        return $this->resultRedirectFactory->create()->setPath('manage_wholesaler/account/info/id/'.$cust_id);
    }

}