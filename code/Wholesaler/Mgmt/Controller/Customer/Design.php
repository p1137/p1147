<?php
namespace Wholesaler\Mgmt\Controller\Customer;

use Magento\Sales\Controller\OrderInterface;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Design extends \Magento\Framework\App\Action\Action
{

 protected $resultPageFactory;

    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }


    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->set(__('Design History'));
        return $resultPage;

       // $this->_view->loadLayout();
       // $this->_view->renderLayout();


    }

}


