<?php
namespace Wholesaler\Mgmt\Controller\Customer;


class Deletedesign extends \Magento\Framework\App\Action\Action {


    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param CommentFactory $commentFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Wholesaler\Mgmt\Model\DesignFactory $designfactory,
        \Magento\Framework\Registry $registry    
    ) {
        $this->designfactory = $designfactory;
        $this->registry = $registry;
        parent::__construct($context);
    }

    public function execute() 
    {

        $designId = $this->getRequest()->getParam('m_id', false);
        $type = $this->getRequest()->getParam('type', false);
        $gender = $this->getRequest()->getParam('gender', false);
            try {
                $design = $this->designfactory->create()->load($designId);
                $design->delete();
                $this->messageManager->addSuccess(__('You deleted your Design Record.'));
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
            }
            // type/Shirt/gender/Men
        return $this->resultRedirectFactory->create()->setPath('manage_wholesaler/customer/design/type/'.$type.'/gender/'.$gender);
    }

}