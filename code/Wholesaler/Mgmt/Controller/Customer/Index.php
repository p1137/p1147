<?php
namespace Wholesaler\Mgmt\Controller\Customer;

use Magento\Sales\Controller\OrderInterface;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Index extends \Magento\Framework\App\Action\Action
{

 protected $resultPageFactory;

    public function __construct(
        Context $context,
         \Magento\Framework\App\RequestInterface $request,
        PageFactory $resultPageFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->_request = $request;
        parent::__construct($context);
    }


    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->set(__('Customer Management'));
        return $resultPage;

       // $this->_view->loadLayout();
       // $this->_view->renderLayout();


    }

}


