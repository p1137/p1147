<?php
namespace Wholesaler\Mgmt\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface {

    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context) {


        $installer = $setup;
        $installer->startSetup();

        /**
         * Create table 'shoes_texture'
         */
        $table = $installer->getConnection()->newTable($installer->getTable('customer_body_measurements'))
            ->addColumn(
                'measurement_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'Measurement ID'
            )
            ->addColumn(
                'customer_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                255,
                [],
                'Customer Id'
            )
            ->addColumn(
                'json',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                [],
                'Json'
            );

        $installer->getConnection()->createTable($table);

        $table1 = $installer->getConnection()->newTable($installer->getTable('wholesaler_customer_designs'))
            ->addColumn(
                'design_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'Design ID'
            )
            ->addColumn(
                'wholesaler_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                255,
                [],
                'Wholesaler Id'
            )
            ->addColumn(
                'product_type',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                [],
                'Product Type'
            )
            ->addColumn(
                'gender',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                [],
                'Gender'
            )
            ->addColumn(
                'json',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                [],
                'Json'
            );

        $installer->getConnection()->createTable($table1);

        $table2 = $installer->getConnection()->newTable($installer->getTable('standard_measurements_shirt'))
        ->addColumn(
            'id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'Measurement ID'
        )
        ->addColumn(
            'size_name',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'Size Name'
        )
        ->addColumn(
            'json',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            [],
            'Json'
        );

        $installer->getConnection()->createTable($table2);

        $installer->endSetup();
    }

}
