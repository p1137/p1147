<?php
namespace Wholesaler\Mgmt\Setup;

use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class UpgradeData implements UpgradeDataInterface {

	private $_eavSetupFactory;
	private $_attributeRepository;

	public function __construct(
		\Magento\Eav\Setup\EavSetupFactory $eavSetupFactory,
		\Magento\Eav\Model\AttributeRepository $attributeRepository
	)
	{
		$this->_eavSetupFactory = $eavSetupFactory;
		$this->_attributeRepository = $attributeRepository;
	}

	public function upgrade( ModuleDataSetupInterface $setup, ModuleContextInterface $context )
	{
		$eavSetup = $this->_eavSetupFactory->create(['setup' => $setup]);
		if (version_compare($context->getVersion(), '2.1.6', '<')) {
		

			$eavSetup->addAttribute(
				\Magento\Catalog\Model\Category::ENTITY, 'disable_category_link', [
			    'type' => 'int',
			    'label' => 'Disable Category Link',
			    'input' => 'boolean',
			    'source' => 'Magento\Eav\Model\Entity\Attribute\Source\Boolean',
			    'visible' => true,
			    'default' => '0',
			    'required' => false,
			    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
			    'group' => 'Display Settings',
			]);
		}
	}
}
