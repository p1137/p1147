<?php
namespace Wholesaler\Mgmt\Model\ResourceModel\StandardMeasurement;

use \Wholesaler\Mgmt\Model\ResourceModel\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'id';

    /**
     * Load data for preview flag
     *
     * @var bool
     */
    protected $_previewFlag;

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Wholesaler\Mgmt\Model\StandardMeasurement', 'Wholesaler\Mgmt\Model\ResourceModel\StandardMeasurement');
        $this->_map['fields']['id'] = 'main_table.id';
    }
}
