<?php
namespace Wholesaler\Mgmt\Model\ResourceModel\Design;

use \Wholesaler\Mgmt\Model\ResourceModel\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'design_id';

    /**
     * Load data for preview flag
     *
     * @var bool
     */
    protected $_previewFlag;

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Wholesaler\Mgmt\Model\Design', 'Wholesaler\Mgmt\Model\ResourceModel\Design');
        $this->_map['fields']['design_id'] = 'main_table.design_id';
    }
}
