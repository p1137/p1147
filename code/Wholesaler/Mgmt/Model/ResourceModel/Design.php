<?php
namespace Wholesaler\Mgmt\Model\ResourceModel;

class Design extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    protected function _construct()
    {
        $this->_init('wholesaler_customer_designs', 'design_id');
    }
}
