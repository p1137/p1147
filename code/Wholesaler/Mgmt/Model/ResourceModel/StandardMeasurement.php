<?php
namespace Wholesaler\Mgmt\Model\ResourceModel;

class StandardMeasurement extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    protected function _construct()
    {
        $this->_init('standard_measurements_shirt', 'id');
    }
}
