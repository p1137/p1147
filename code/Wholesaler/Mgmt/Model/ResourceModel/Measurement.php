<?php
namespace Wholesaler\Mgmt\Model\ResourceModel;

class Measurement extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    protected function _construct()
    {
        $this->_init('customer_body_measurements', 'measurement_id');
    }
}
