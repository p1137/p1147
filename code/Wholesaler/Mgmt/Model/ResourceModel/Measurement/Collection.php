<?php
namespace Wholesaler\Mgmt\Model\ResourceModel\Measurement;

use \Wholesaler\Mgmt\Model\ResourceModel\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'measurement_id';

    /**
     * Load data for preview flag
     *
     * @var bool
     */
    protected $_previewFlag;

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Wholesaler\Mgmt\Model\Measurement', 'Wholesaler\Mgmt\Model\ResourceModel\Measurement');
        $this->_map['fields']['measurement_id'] = 'main_table.measurement_id';
    }
}
