<?php
namespace Wholesaler\Mgmt\Model;

class StandardMeasurement extends \Magento\Framework\Model\AbstractModel
{


    protected function _construct()
    {
        $this->_init('Wholesaler\Mgmt\Model\ResourceModel\StandardMeasurement');
    }

}
