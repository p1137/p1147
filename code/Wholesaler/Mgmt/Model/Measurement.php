<?php
namespace Wholesaler\Mgmt\Model;

class Measurement extends \Magento\Framework\Model\AbstractModel
{


    protected function _construct()
    {
        $this->_init('Wholesaler\Mgmt\Model\ResourceModel\Measurement');
    }

}
