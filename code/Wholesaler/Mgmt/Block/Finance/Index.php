<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Wholesaler\Mgmt\Block\Finance;


class Index extends \Magento\Framework\View\Element\Template
{

 protected $_customerFactory;
 protected $_requestedParameters;

    public function __construct(\Magento\Framework\View\Element\Template\Context $context,
    \Magento\Customer\Model\ResourceModel\Customer\CollectionFactory $customerFactory,
    \Magento\Customer\Model\Session $customerSession,
    \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory, 
    \Magento\Sales\Model\ResourceModel\Order\Invoice\CollectionFactory $invoiceCollection, 
    \Magento\Sales\Model\OrderFactory $order,
    \Magento\Framework\Message\ManagerInterface $messageManager,
    \Magento\Sales\Model\Order\InvoiceFactory $invoice,
    \Magento\Customer\Model\Customer $customers, 
    \Magento\Framework\App\Request\Http $request)
     {
        $this->_customerSession = $customerSession;
        $this->_orderCollectionFactory = $orderCollectionFactory;
        $this->_invoiceCollection = $invoiceCollection;
        $this->_order = $order;
        $this->_customerFactory = $customerFactory;
        $this->invoiceModel = $invoice;
        $this->_customers = $customers;
        $this->messageManager = $messageManager;
        $this->request = $request;
        parent::__construct($context);
     }

    protected function _prepareLayout()
    {

        if ($this->getInvoiceCollection()) {
            $pager = $this->getLayout()->createBlock(
                'Magento\Theme\Block\Html\Pager',
                'invoice.record.pager'
            )->setAvailableLimit(array(10=>10,20=>20,30=>30,50=>50))
                ->setShowPerPage(true)->setCollection(
                $this->getInvoiceCollection()
            );
            $this->setChild('pager', $pager);
            $this->getInvoiceCollection()->load();
        }
        return $this;
    }

    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }

    public function getPagenum()
    {
        return $this->request->getParam('p');
    }

    public function getLimit()
    {
        return $this->request->getParam('limit');
    }

    public function getInvoiceCollection()
    {

        $customerId = $this->_customerSession->getId();
        $orderCollection = $this->_orderCollectionFactory->create($customerId)
                    ->addFieldToSelect('*');

        $invoiceIds = array();

        foreach ($orderCollection as $key=>$value) {
            $orderIncrementId = $value->getIncrementId();
            $order = $this->_order->create()->loadByIncrementId($orderIncrementId);
             foreach ($order->getInvoiceCollection() as $invoice)
             {
                $invoiceIds[] = $invoice->getId();
             }
               
        }

        $invoiceCollection = $this->_invoiceCollection->create()->addFieldToSelect('*');
        $invoiceCollection->addFieldToFilter('entity_id', ['in' => $invoiceIds]);

        $data = $this->_request->getParams();

        if (array_key_exists('reset', $data)){
            $this->_customerSession->unsInvoiceFilter();
        }

        if (count($data) > 5 || count($this->_customerSession->getInvoiceFilter())>4) {

            if (!array_key_exists('reset', $data)) {

                $invoice_filter = $this->_customerSession->getInvoiceFilter();


                $this->_customerSession->unsOrderFilter();
                
                if(isset($invoice_filter) && !isset($data['submit'])){

                    if($invoice_filter['date_from']!=''){
                        $data['date_from'] = $invoice_filter['date_from'];
                    }
                    if($invoice_filter['date_to']!=''){
                        $data['date_to'] = $invoice_filter['date_to'];
                    }
                    if($invoice_filter['order_date_from']!=''){
                        $data['order_date_from'] = $invoice_filter['order_date_from'];
                    }
                    if($invoice_filter['order_date_to']!=''){
                        $data['order_date_to'] = $invoice_filter['order_date_to'];
                    }
                    if($invoice_filter['key_word']!=''){
                        $data['key_word'] = $invoice_filter['key_word'];
                    }
                }



                $dateFrom = (isset($data['date_from'])) ? $data['date_from'] : "";
                $dateTo = (isset($data['date_to'])) ? $data['date_to'] : "";
                $orderDateFrom = (isset($data['order_date_from'])) ? $data['order_date_from'] : "";
                $orderDateTo = (isset($data['order_date_to'])) ? $data['order_date_to'] : "";

                $keyWord = (isset($data['key_word'])) ? $data['key_word'] : "";
                $keyArray = explode(" ",$keyWord);   

                $orderCollection = $this->_orderCollectionFactory->create($customerId)
                            ->addFieldToSelect('*');

                $invoiceIds = array();

                foreach ($orderCollection as $key=>$value) {
                    $orderIncrementId = $value->getIncrementId();
                    $order = $this->_order->create()->loadByIncrementId($orderIncrementId);
                     foreach ($order->getInvoiceCollection() as $invoice)
                     {
                        $invoiceIds[] = $invoice->getId();
                     }
                       
                }

                $invoiceCollection = $this->_invoiceCollection->create()->addFieldToSelect('*');
                $invoiceCollection->addFieldToFilter('entity_id', ['in' => $invoiceIds]);



                $filter = array('date_from'=>$dateFrom, 'date_to'=>$dateTo,'order_date_from'=>$orderDateFrom, 'order_date_to'=>$orderDateTo,'key_word'=>$keyWord ); 

                $this->_customerSession->setInvoiceFilter($filter);


                /*
                 * Filter by invoice Create at
                 */
                if (!empty($dateFrom)) {
                    if (!empty($dateTo)) {
                        $from = date('Y-m-d H:i:s', strtotime($dateFrom));
                        $to = date('Y-m-d H:i:s', strtotime($dateTo));
                        $invoiceCollection->addFieldToFilter('created_at', array('from' => $from, 'to' => $to))->setOrder(
                                'created_at', 'asc'
                        );
                        $this->_requestedParameters['date_from'] = $dateFrom;
                        $this->_requestedParameters['date_to'] = $dateTo;
                    } else {
                        $from = date('Y-m-d H:i:s', strtotime($dateFrom));
                        $invoiceCollection->addFieldToFilter('created_at', array('from' => $from))->setOrder(
                                'created_at', 'asc'
                        );
                        $this->_requestedParameters['date_from'] = $dateFrom;
                    }
                }
                /*
                 * Filter by order Create at
                 */
                if (!empty($orderDateFrom)) {
                    $orderIds = array();
                    $orderCollection = $this->_orderCollectionFactory->create($customerId)
                                ->addFieldToSelect('*');

                    if (!empty($orderDateTo)) {
                        $o_from = date('Y-m-d H:i:s', strtotime($orderDateFrom));
                        $o_to = date('Y-m-d H:i:s', strtotime($orderDateTo));
                        $orderCollection->addFieldToFilter('created_at', array('from' => $o_from, 'to' => $o_to))->setOrder(
                                'created_at', 'asc'
                        );
                        $this->_requestedParameters['order_date_from'] = $orderDateFrom;
                        $this->_requestedParameters['order_date_to'] = $orderDateTo;
                    } else {
                        $o_from = date('Y-m-d H:i:s', strtotime($orderDateFrom));
                        $orderCollection->addFieldToFilter('created_at', array('from' => $o_from))->setOrder(
                                'created_at', 'asc'
                        );
                        $this->_requestedParameters['order_date_from'] = $orderDateFrom;
                    }


                    if($orderCollection->getData()){
                        foreach ($orderCollection as $order){
                            $orderIds[] = $order->getId();
                        }
                        $invoiceCollection->addFieldToFilter('order_id', $orderIds)->setOrder(
                                'created_at', 'asc'
                        );
                  
                    }

                }

                /*
                 * Filter by key word
                 */

                if (!empty($keyWord)) {
                    $orderIds = array();
                    $orderCollection = $this->_orderCollectionFactory->create($customerId)
                                ->addFieldToSelect('*');

                    if(count($keyArray)>1){
                        $customerCollection = $this->getCustomerCollection()->addAttributeToFilter(
                                array(
                                    array('attribute' => 'firstname', 'eq' => $keyArray[0])
                                ),
                                array(
                                    array('attribute' => 'lastname', 'eq' => $keyArray[1])
                                )
                        );
                    }else{
                        $customerCollection = $this->getCustomerCollection()->addAttributeToFilter(
                                array(
                                    array('attribute' => 'firstname', 'like' => "%" . $keyWord . "%"),
                                    array('attribute' => 'lastname', 'like' => "%" . $keyWord . "%")
                                )
                        );

                    }

                    if($customerCollection->getData()){
                    foreach ($customerCollection as $customer){
                        $customerIds[] = $customer->getId();
                    }
                    $orderCollection->addFieldToFilter('wholesalers_customer_id', $customerIds)->setOrder(
                            'created_at', 'asc'
                    );
                  
                    }else{

                        $invoiceCollection->addFieldToFilter(['increment_id'],
                        [
                            ['like' => "%" . $keyWord . "%"]
                        ]);
                    }

                    if($orderCollection->getData()){
                        foreach ($orderCollection as $order){
                            $orderIds[] = $order->getId();
                        }
                        $invoiceCollection->addFieldToFilter('order_id', $orderIds)->setOrder(
                                'created_at', 'asc'
                        );
                  
                    }
                    $this->_requestedParameters['key_word'] = $keyWord;
                }


            }
        }
        
            $data['p'] = (isset($data['p']))? $data['p'] : 1;
            $data['limit'] = (isset($data['limit']))? $data['limit'] : 10;

            $invoiceCollection->setPageSize($data['limit'])->setCurPage($data['p']);

            return $invoiceCollection;

    }

    /*
     * Get customer collection
     */

    protected function getCustomerCollection() {
        return $this->_customerFactory->create();
    }


    public function getOrderIncrementId($invoiceId)
    {
        $invoice = $this->invoiceModel->create()->loadByIncrementId($invoiceId);
        $order = $invoice->getOrder();
        $incrementId = $order->getIncrementId();
        return $incrementId;
    }

    public function getOrderDate($invoiceId)
    {
        $invoice = $this->invoiceModel->create()->loadByIncrementId($invoiceId);
        $order = $invoice->getOrder();
        $orderDate = $order->getCreatedAt();
        return $orderDate;
    }

    public function getCustomerName($invoiceId)
    {
        $invoice = $this->invoiceModel->create()->loadByIncrementId($invoiceId);
        $order = $invoice->getOrder();
        $name = $order->getCustomerName();
        return $name;
    }    

    public function getClientName($invoiceId)
    {
        $invoice = $this->invoiceModel->create()->loadByIncrementId($invoiceId);
        $order = $invoice->getOrder();
        $wholesaler_custId = $order->getWholesalersCustomerId();

        if($wholesaler_custId){
            $customer = $this->_customers->load($wholesaler_custId);
            return $customer->getFirstname() . " " . $customer->getLastname();
        }else
        return "-";

    }   

    public function getViewUrl($orderId)
    {
        return $this->getUrl('sales/order/invoice/order_id/'.$orderId);
    }    

    public function getNotAvailable()
    {
        return $this->messageManager->addWarning(__("Invoices are not available for any order"));
    }

    public function getApplyedFilters() {
        return $this->_requestedParameters;
    }
    /*
     *  Get Order filter form action
     */

    public function getFormAction() {
        return $this->getUrl('manage_wholesaler/finance/index');
    }


}
