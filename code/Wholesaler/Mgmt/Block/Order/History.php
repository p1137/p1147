<?php

/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
use Magento\Framework\Registry;

namespace Wholesaler\Mgmt\Block\Order;

class History extends \Magento\Sales\Block\Order\History {

    const WHOLESALERID = 2;

    protected $_customers;
    protected $_statusCollectionFactory;
    protected $_request;
    protected $orders;
    protected $_registry;
    protected $_customerFactory;
    protected $_requestedParameters;

    public function __construct(
    \Magento\Customer\Model\ResourceModel\Customer\CollectionFactory $customerFactory,
    \Magento\Framework\Registry $registry,
    \Magento\Framework\App\RequestInterface $request,
    \Magento\Sales\Model\ResourceModel\Order\Status\CollectionFactory $statusCollectionFactory,
    \Magento\Store\Model\StoreManagerInterface $StoreManager, 
    \Magento\Framework\View\Element\Template\Context $context, 
    \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory, 
    \Magento\Customer\Model\Session $customerSession, 
    \Magento\Sales\Model\Order\Config $orderConfig, 
    \Magento\Customer\Model\Customer $customers, 
    \Magento\Framework\Message\ManagerInterface $messageManager,
    array $data = []
    ) {
        $this->_storeManagerInterface = $StoreManager;
        $this->_orderCollectionFactory = $orderCollectionFactory;
        $this->_customerSession = $customerSession;
        $this->_customerFactory = $customerFactory;
        $this->_orderConfig = $orderConfig;
        $this->_customers = $customers;
        $this->_request = $request;
        $this->_registry = $registry;
        $this->_messageManager = $messageManager;
        $this->_statusCollectionFactory = $statusCollectionFactory;
        parent::__construct($context, $orderCollectionFactory, $customerSession, $orderConfig, $data);
    }

    /*
     *  Get wholesalers Customer Name
     */

    public function getWholesalersCustomerName($customerId) {
        if($customerId){
            $customer = $this->_customers->load($customerId);
            return $customer->getFirstname() . " " . $customer->getLastname();
        }else
        return "-";
    }

    /*
     * Will return URL for deleteding order
     */

    public function getOrderDeleteUrl($orderId) {
        return $this->_storeManager->getStore()->getBaseUrl() . "manage_wholesaler/order/index/id/" . $orderId;
    }

    /*
     *  Check wheather current customer is wholesaler or not
     */

    public function isWholesaler() {
        if ($this->_customerSession->isLoggedIn()):
            return $this->_customerSession->getCustomer()->getGroupId() == self::WHOLESALERID ? true : false;
        endif;
        return false;
    }

    /*
     *  Get Order Status List
     */

    public function getStatusOptions() {

        $statusArray = $this->_statusCollectionFactory->create()->toOptionArray();
        $optionaArray = array();
        foreach ($statusArray as $key => $value) {
            if($value['value']=='paypal_canceled_reversal' || $value['value']=='paypal_reversed' || $value['value']=='pending_paypal' || $value['value']=='payment_review' || $value['value']=='pending_payment'){

            }else{
                $optionaArray[] = $value;
            }
        }
        return $optionaArray;

        // return $this->_statusCollectionFactory->create()->toOptionArray();
    }

    /*
     *  Get Order filter form action
     */

    public function getFormAction() {
        return $this->_storeManager->getStore()->getBaseUrl() . "manage_wholesaler/order/filter";
    }

    /**
     * @return bool|\Magento\Sales\Model\ResourceModel\Order\Collection
     */
    public function getWholesalerOrders() {
        $data = $this->_request->getParams();
               
        if (array_key_exists('reset', $data)){
            $this->_customerSession->unsOrderFilter();
        }



        if (!($customerId = $this->_customerSession->getCustomerId())) {
            return false;
        }
        
        if (count($data) > 4 || count($this->_customerSession->getOrderFilter())>3) {
            if (!array_key_exists('reset', $data)) {

                $order_filter = $this->_customerSession->getOrderFilter();


                $this->_customerSession->unsOrderFilter();
                
                if(isset($order_filter) && !isset($data['submit'])){

                    if($order_filter['date_from']!=''){
                        $data['date_from'] = $order_filter['date_from'];
                    }
                    if($order_filter['date_to']!=''){
                        $data['date_to'] = $order_filter['date_to'];
                    }
                    if($order_filter['order_status']!=''){
                        $data['order_status'] = $order_filter['order_status'];
                    }
                    if($order_filter['key_word']!=''){
                        $data['key_word'] = $order_filter['key_word'];
                    }
                }



                $dateFrom = (isset($data['date_from'])) ? $data['date_from'] : "";
                $dateTo = (isset($data['date_to'])) ? $data['date_to'] : "";
                $orderStatus = (isset($data['order_status'])) ? $data['order_status'] : "";

                $keyWord = (isset($data['key_word'])) ? $data['key_word'] : "";
                $keyArray = explode(" ",$keyWord);   
                $this->orders = $this->_orderCollectionFactory->create($customerId)
                        ->addFieldToSelect('*');



                $filter = array('date_from'=>$dateFrom, 'date_to'=>$dateTo,'order_status'=>$orderStatus,'key_word'=>$keyWord ); 

                $this->_customerSession->setOrderFilter($filter);

                /*
                 * Filter by Order Status
                 */
                if (!empty($orderStatus)) {
                    $this->orders->addFieldToFilter('status', ['in' => $orderStatus]);
                    $this->_requestedParameters['order_status'] = $orderStatus;
                }

                /*
                 * Filter by Order Create at
                 */
                if (!empty($dateFrom)) {
                    if (!empty($dateTo)) {
                        $from = date('Y-m-d H:i:s', strtotime($dateFrom));
                        $to = date('Y-m-d H:i:s', strtotime($dateTo));
                        $this->orders->addFieldToFilter('created_at', array('from' => $from, 'to' => $to))->setOrder(
                                'created_at', 'asc'
                        );
                        $this->_requestedParameters['date_from'] = $dateFrom;
                        $this->_requestedParameters['date_to'] = $dateTo;
                    } else {
                        $from = date('Y-m-d H:i:s', strtotime($dateFrom));
                        $this->orders->addFieldToFilter('created_at', array('from' => $from))->setOrder(
                                'created_at', 'asc'
                        );
                        $this->_requestedParameters['date_from'] = $dateFrom;
                    }
                }

                /*
                 * Filter by key word
                 */

                if (!empty($keyWord)) {

                    if(count($keyArray)>1){
                        $customerCollection = $this->getCustomerCollection()->addAttributeToFilter(
                                array(
                                    array('attribute' => 'firstname', 'eq' => $keyArray[0])
                                ),
                                array(
                                    array('attribute' => 'lastname', 'eq' => $keyArray[1])
                                )
                        );
                    }else{
                        $customerCollection = $this->getCustomerCollection()->addAttributeToFilter(
                                array(
                                    array('attribute' => 'firstname', 'like' => "%" . $keyWord . "%"),
                                    array('attribute' => 'lastname', 'like' => "%" . $keyWord . "%")
                                )
                        );

                    }

                    if($customerCollection->getData()){
                    foreach ($customerCollection as $customer){
                        $customerIds[] = $customer->getId();
                    }
                    $this->orders->addFieldToFilter('wholesalers_customer_id', $customerIds)->setOrder(
                            'created_at', 'asc'
                    );
                  
                    }else{

                        $this->orders->addFieldToFilter(['increment_id', 'grand_total'],
                        [
                            ['like' => "%" . $keyWord . "%"],
                            ['like' => "%" . $keyWord . "%"]
                        ]);
                    }
                    $this->_requestedParameters['key_word'] = $keyWord;
                }
            }
        } else if(!$this->orders) {
            $this->orders = $this->_orderCollectionFactory->create($customerId);
        }
            $data['p'] = (isset($data['p']))? $data['p'] : 1;
            $data['limit'] = (isset($data['limit']))? $data['limit'] : 10;
            $this->orders->setPageSize($data['limit'])->setCurPage($data['p']);
            return $this->orders;
    }


    public function getOrders()
    {
        if (!($customerId = $this->_customerSession->getCustomerId())) {
            return false;
        }
        if (!$this->orders) {
            $this->orders = $this->_orderCollectionFactory->create($customerId)->addFieldToSelect(
                '*'
            )->addFieldToFilter(
                'status',
                ['in' => $this->_orderConfig->getVisibleOnFrontStatuses()]
            )->setOrder(
                'created_at',
                'desc'
            );
        }

        if($this->_customerSession->getCustomer()->getGroupId() == '2'){
            $this->orders = $this->getWholesalerOrders();
        }
        return $this->orders;
    }


    /*
     * Get customer collection
     */

    protected function getCustomerCollection() {
        return $this->_customerFactory->create();
    }

    public function getApplyedFilters() {
        return $this->_requestedParameters;
    }

    public function getPagenum()
    {
    
    $this->_request->getParams(); 
        return $this->_request->getParam('p');
    }
    public function getLimit()
    {
    $this->_request->getParams();
       return $this->_request->getParam('limit');
    }


}
