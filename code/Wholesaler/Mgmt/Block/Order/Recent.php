<?php

/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Wholesaler\Mgmt\Block\Order;

use Magento\Framework\View\Element\Template\Context;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
use Magento\Customer\Model\Session;
use Magento\Sales\Model\Order\Config;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\ObjectManager;

class Recent extends \Magento\Sales\Block\Order\Recent {

    const WHOLESALERID = 2;

    protected $_customers;

    public function __construct(
    Context $context, CollectionFactory $orderCollectionFactory, Session $customerSession, Config $orderConfig, \Magento\Customer\Model\Customer $customers, array $data = [], StoreManagerInterface $storeManager = null
    ) {
        $this->_orderCollectionFactory = $orderCollectionFactory;
        $this->_customerSession = $customerSession;
        $this->_orderConfig = $orderConfig;
        $this->_isScopePrivate = true;
        $this->_customers = $customers;
        $this->storeManager = $storeManager ?: ObjectManager::getInstance()
                        ->get(StoreManagerInterface::class);
        parent::__construct($context, $orderCollectionFactory, $customerSession, $orderConfig, $data);
    }

    public function getWholesalersCustomerName($customerId) {
        if($customerId){
            $customer = $this->_customers->load($customerId);
            return $customer->getFirstname() . " " . $customer->getLastname();
        }else
        return "-";
    }

    public function getOrderDeleteUrl($orderId) {
        return $this->_storeManager->getStore()->getBaseUrl() . "manage_wholesaler/order/index/id/" . $orderId;
    }

    public function isWholesaler() {
        if ($this->_customerSession->isLoggedIn()):
            return $this->_customerSession->getCustomer()->getGroupId() == self::WHOLESALERID ? true : false;
        endif;
    }

}
