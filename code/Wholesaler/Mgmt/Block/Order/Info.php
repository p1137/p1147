<?php

/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Wholesaler\Mgmt\Block\Order;

use Magento\Sales\Model\Order\Address;
use Magento\Framework\View\Element\Template\Context as TemplateContext;
use Magento\Framework\Registry;
use Magento\Payment\Helper\Data as PaymentHelper;
use Magento\Sales\Model\Order\Address\Renderer as AddressRenderer;

class Info extends \Magento\Sales\Block\Order\Info {

    const WHOLESALERID = 2;

    protected $_customers;
    protected $_statusCollectionFactory;

    public function __construct(
    \Magento\Sales\Model\ResourceModel\Order\Status\CollectionFactory $statusCollectionFactory, \Magento\Customer\Model\Session $customerSession, \Magento\Customer\Model\Customer $customers, TemplateContext $context, Registry $registry, PaymentHelper $paymentHelper, AddressRenderer $addressRenderer, array $data = []
    ) {
        $this->addressRenderer = $addressRenderer;
        $this->paymentHelper = $paymentHelper;
        $this->coreRegistry = $registry;
        $this->_isScopePrivate = true;
        $this->_customerSession = $customerSession;
        $this->_customers = $customers;
        $this->_statusCollectionFactory = $statusCollectionFactory;
        parent::__construct($context, $registry, $paymentHelper, $addressRenderer, $data);
    }

    public function getCustomerName($customerId) {
        $customer = $this->_customers->load($customerId);
        return $customer->getFirstname() . " " . $customer->getLastname();
    }

    public function isWholesaler() {
        if ($this->_customerSession->isLoggedIn()):
            return $this->_customerSession->getCustomer()->getGroupId() == self::WHOLESALERID ? true : false;
        endif;
    }

    public function getStatusOptions($currentStatus) {
        $statusArray = $this->_statusCollectionFactory->create()->toOptionArray();
        $optionaArray = array();

        if($currentStatus == 'closed'){

            $optionaArray[0]['value'] = 'closed'; 
            $optionaArray[0]['label'] = 'Closed'; 

        }else if($currentStatus == 'canceled'){

            $optionaArray[0]['value'] = 'canceled'; 
            $optionaArray[0]['label'] = 'Canceled'; 

        }else if($currentStatus == 'complete'){

            $optionaArray[0]['value'] = 'complete'; 
            $optionaArray[0]['label'] = 'Complete'; 
            $optionaArray[1]['value'] = 'closed'; 
            $optionaArray[1]['label'] = 'Closed'; 

        }else{
        foreach ($statusArray as $key => $value) {
            if($value['value']=='paypal_canceled_reversal' || $value['value']=='paypal_reversed' || $value['value']=='pending_paypal' || $value['value']=='payment_review' || $value['value']=='pending_payment'){

            }else{
                $optionaArray[] = $value;
            }
         }
       } 
        return $optionaArray;
    }

    /*
     *  Get Order filter form action
     */

    public function getFormAction() {
        return $this->_storeManager->getStore()->getBaseUrl() . "manage_wholesaler/order/changestatus";
    }

}
