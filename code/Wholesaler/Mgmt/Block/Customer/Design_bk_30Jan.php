<?php

namespace Wholesaler\Mgmt\Block\Customer;

class Design extends \Magento\Framework\View\Element\Template
{

 protected $_customerFactory;

	public function __construct(\Magento\Framework\View\Element\Template\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Wholesaler\Mgmt\Model\DesignFactory $designfactory,
        \Magento\Framework\App\Request\Http $request	
)
	 {
        $this->_customerSession = $customerSession;
	    $this->_designfactory = $designfactory;
        $this->request = $request;
	    parent::__construct($context);
	 }


	public function getDesignCollection()

	{
        $type = $this->getRequest()->getParam('type');
        $gender = $this->getRequest()->getParam('gender');
        $wholesaler_id = $this->getWholesalerId(); 

        //get values of current page. If not the param value then it will set to 1
        $page=($this->getRequest()->getParam('p'))? $this->getRequest()->getParam('p') : 1;
        //get values of current limit. If not the param value then it will set to 1
        $pageSize=($this->getRequest()->getParam('limit'))? $this->getRequest()->getParam('limit') : 10;

        $dsignmodel = $this->_designfactory->create();
        $collection = $dsignmodel->getCollection()
           ->addFieldToFilter('wholesaler_id', array('eq' => $wholesaler_id))
           ->addFieldToFilter('product_type', array('eq' => $type))
           ->addFieldToFilter('gender', array('eq' => $gender));

        $collection->setPageSize($pageSize);
        $collection->setCurPage($page);

        if(count($collection))
            return $collection;
        
        
	}

    public function getMessages()
    {
        $type = $this->getRequest()->getParam('type');
        if($type == 'WomenSuit'){
            $type = 'Suit';
        }else if($type == 'WomenShirt'){
            $type = 'Shirt';
        }

        $gender = $this->getRequest()->getParam('gender');
        $message = 'You dont have any designs saved for '.$gender.' '.$type;
        return $message;
    }



    protected function getWholesalerId()
    {    
        if($this->_customerSession->isLoggedIn()) {

            $customerGroup = $this->_customerSession->getCustomer()->getGroupId();

            if($customerGroup == '2') {
                return $this->_customerSession->getCustomer()->getId();
            } 
        }
    }


     /**
     * @return string
     */
    public function getDeleteDesignUrl()
    {
        $type = $this->request->getParam('type');
        $gender = $this->request->getParam('gender');
        return $this->getUrl('manage_wholesaler/customer/deletedesign/type/'.$type.'/gender/'.$gender);
    }
    
    protected function _prepareLayout()
    {

        if ($this->getDesignCollection()) {
            $pager = $this->getLayout()->createBlock(
                'Magento\Theme\Block\Html\Pager',
                'design.record.pager'
            )->setAvailableLimit(array(10=>10,20=>20,30=>30,50=>50))
                ->setShowPerPage(true)->setCollection(
                $this->getDesignCollection()
            );
            $this->setChild('pager', $pager);
            $this->getDesignCollection()->load();
        }
        return $this;
    }



    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }

    public function getPagenum()
    {
        return $this->request->getParam('p');
    }
    public function getLimit()
    {
        return $this->request->getParam('limit');
    }
    public function getProductType()
    {
        return $this->request->getParam('type');
    }
    public function getEditUrl($p_type)
    {
        if($p_type == "Shirt"){ 
            $editUrl = 'men-shirt/index/index/';
        }else if($p_type == "WomenShirt"){ 
            $editUrl = 'women-shirt/index/index/';
        }else if($p_type == "Suit"){ 
            $editUrl = 'men-suit/index/index/';
        }if($p_type == "WomenSuit"){ 
            $editUrl = 'women-suit/index/index/';
        }

        return $editUrl;
    }


}
