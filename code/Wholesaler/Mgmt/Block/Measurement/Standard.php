<?php

namespace Wholesaler\Mgmt\Block\Measurement;

class Standard extends \Magento\Framework\View\Element\Template
{

 protected $_customerFactory;

	public function __construct(\Magento\Framework\View\Element\Template\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Wholesaler\Mgmt\Model\StandardMeasurementFactory $standardfactory,
        \Magento\Framework\App\Request\Http $request
)
	 {
        $this->_customerSession = $customerSession;
	    $this->_standardfactory = $standardfactory;
        $this->request = $request;
	    parent::__construct($context);
	 }


	public function getStandardMeasurementCollection($category)

	{
        $standardmodel = $this->_standardfactory->create();
        $collection = $standardmodel->getCollection()->addFieldToFilter('product_type', array('eq' => $category));
        $data = $collection->getData();
        $size_name_array = array();
        foreach ($data as $key => $value) {
        $size_name_array[$key] = json_decode($value['json'],true);
        }
         return $size_name_array;

	}

    public function getStandardMeasurement()

    {
        $standardmodel = $this->_standardfactory->create();
        $collection = $standardmodel->getCollection();
        $data = $collection->getData();
        $size_name_array = array();
        foreach ($data as $key => $value) {
        $size_name_array[$key] = json_decode($value['json'],true);
        }
         return $size_name_array;

    }

}
