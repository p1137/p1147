<?php

namespace Wholesaler\Mgmt\Block\Quickorder;

class Index extends \Magento\Framework\View\Element\Template
{

 protected $_customerFactory;
 protected $_requestedParameters;

	public function __construct(\Magento\Framework\View\Element\Template\Context $context,
        \Magento\Customer\Model\Session $customerSession,
		\Magento\Customer\Model\ResourceModel\Customer\CollectionFactory $customerFactory,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Framework\App\Request\Http $request	
)
	 {
        $this->_customerSession = $customerSession;
	    $this->_customerFactory = $customerFactory;
        $this->messageManager = $messageManager;
        $this->request = $request;
	    parent::__construct($context);
	 }

    public function getCusomerSession() 
    {
        return $this->_customerSession;
    } 

    public function setSelectedCustomer($WholesalerCustomer)
    {
        $this->_customerSession->setWholesalerCustomer($WholesalerCustomer); //set value in customer session
    }

    public function getSelectedCustomer()
    {
        return $this->_customerSession->getWholesalerCustomer(); //Get value from customer session
    }

    public function unSetSelectedCustomer(){
        $this->_customerSession->start();
        return $this->_customerSession->unsWholesalerCustomer();
    }

    protected function _prepareLayout()
    {

        parent::_prepareLayout();
        $this->pageConfig->getTitle()->set(__('Customers'));

        if ($this->getCustomerCollection()) {
            $pager = $this->getLayout()->createBlock(
                'Magento\Theme\Block\Html\Pager',
                'quickorder.record.pager'
            )->setAvailableLimit(array(10=>10,20=>20,30=>30,50=>50))
                ->setShowPerPage(true)->setCollection(
                $this->getCustomerCollection()
            );
            $this->setChild('pager', $pager);
            $this->getCustomerCollection()->load();
        }
        return $this;
    }



    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }

     /**
     * @return string
     */
    public function getAddCustomerUrl()
    {
        return $this->getUrl('manage_wholesaler/account/create/id/'.$this->getWholesalerId(), ['_secure' => true]);
    }

	public function getCustomerCollection()

	{

	    // return $this->_customerFactory->create()->addAttributeToFilter('parent_wholesaler', $this->getWholesalerId());


        //get values of current page. If not the param value then it will set to 1
        $page=($this->getRequest()->getParam('p'))? $this->getRequest()->getParam('p') : 1;
        //get values of current limit. If not the param value then it will set to 1
        $pageSize=($this->getRequest()->getParam('limit'))? $this->getRequest()->getParam('limit') : 10;

        $collection = $this->_customerFactory->create();

        $collection->addAttributeToFilter('parent_wholesaler', $this->getWholesalerId());

        $data = $this->_request->getParams();

        $keyWord_search = $this->_customerSession->getQuickorderKeywordSearch();

        if (array_key_exists('reset', $data)){
            $this->_customerSession->unsQuickorderKeywordSearch();
            $this->_requestedParameters['key_word'] = NULL;

            $collection->addAttributeToFilter('parent_wholesaler', $this->getWholesalerId());
        }


        if($keyWord_search!= NULL){
            $collection->addAttributeToFilter(
            array(
                array('attribute' => 'firstname', 'like' => "%" . $keyWord_search . "%"),
                array('attribute' => 'lastname', 'like' => "%" . $keyWord_search . "%"),
                array('attribute' => 'entity_id', 'eq' => $keyWord_search )
                ),
            array(
                array('attribute' =>  'parent_wholesaler', 'eq' => $this->getWholesalerId())
                )
        );
            $this->_requestedParameters['key_word'] = $keyWord_search;
        }

        /*
        * Filter by key word
        */
        if (array_key_exists('key_word', $data) && $data['key_word']!= NULL) 
        {
            if (!array_key_exists('reset', $data)){

                $keyWord = $data['key_word'] ? $data['key_word'] : "";
                $this->_customerSession->setQuickorderKeywordSearch($keyWord);
                $keyWord_search = $this->_customerSession->getQuickorderKeywordSearch();

                $collection->addAttributeToFilter(
                    array(
                        array('attribute' => 'firstname', 'like' => "%" . $keyWord_search . "%"),
                        array('attribute' => 'lastname', 'like' => "%" . $keyWord_search . "%"),
                        array('attribute' => 'entity_id', 'eq' => $keyWord_search )
                        ),
                    array(
                        array('attribute' =>  'parent_wholesaler', 'eq' => $this->getWholesalerId())
                        )
                );
                if(!count($collection)){
                    
                    $this->getNotAvailable();
                }

                $this->_requestedParameters['key_word'] = $keyWord_search;
              }

            else{
                $collection->addAttributeToFilter('parent_wholesaler', $this->getWholesalerId());
            }

        }elseif (array_key_exists('submit', $data) && $data['key_word'] == NULL) {
            $this->getNotMentioned();
            $collection->addAttributeToFilter('parent_wholesaler', $this->getWholesalerId());

        }else{
                $collection->addAttributeToFilter('parent_wholesaler', $this->getWholesalerId());
        }
                    
            $collection->setPageSize($pageSize);
            $collection->setCurPage($page);
            return $collection;

	}

    protected function getWholesalerId()
    {    
        if($this->_customerSession->isLoggedIn()) {

            $customerGroup = $this->_customerSession->getCustomer()->getGroupId();

            if($customerGroup == '2') {
                return $this->_customerSession->getCustomer()->getId();
            } 
        }
    }

     /**
     * @return string
     */
    public function getShirtDesignUrl()
    {
        return $this->getUrl('manage_wholesaler/customer/', ['_secure' => true]);
    }
     /**
     * @return string
     */
    public function getSuitDesignUrl()
    {
        return $this->getUrl('manage_wholesaler/customer/');
    }
    
    public function getApplyedFilters() {
        return $this->_requestedParameters;
    }

    public function getPagenum()
    {
        return $this->request->getParam('p');
    }
    public function getLimit()
    {
        return $this->request->getParam('limit');
    }
    public function getNotAvailable()
    {
        return $this->messageManager->addWarning(__("No record found for given search"));
    }
    public function getNotMentioned()
    {
        return $this->messageManager->addWarning(__("Mention Customer Name or Number to Search"));
    }


}
