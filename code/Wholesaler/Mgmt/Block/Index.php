<?php

namespace Wholesaler\Mgmt\Block;

class Index extends \Magento\Framework\View\Element\Template
{

 protected $_customerFactory;
 protected $_requestedParameters;

	public function __construct(\Magento\Framework\View\Element\Template\Context $context,
        \Magento\Customer\Model\Session $customerSession,
		\Magento\Customer\Model\ResourceModel\Customer\CollectionFactory $customerFactory,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Framework\App\Request\Http $request	
)
	 {
        $this->_customerSession = $customerSession;
	    $this->_customerFactory = $customerFactory;
        $this->request = $request;
        $this->messageManager = $messageManager;
	    parent::__construct($context);
	 }

    protected function _prepareLayout()
    {

        if ($this->getCustomerCollection()) {
            $pager = $this->getLayout()->createBlock(
                'Magento\Theme\Block\Html\Pager',
                'customer.record.pager'
            )->setAvailableLimit(array(10=>10,20=>20,30=>30,50=>50))
                ->setShowPerPage(true)->setCollection(
                $this->getCustomerCollection()
            );
            $this->setChild('pager', $pager);
            $this->getCustomerCollection()->load();
        }
        return $this;
    }



    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }


	public function getCustomerCollection()

	{
        // return $this->_customerFactory->create()->addAttributeToFilter('parent_wholesaler', $this->getWholesalerId());

        //get values of current page. If not the param value then it will set to 1
        $page=($this->getRequest()->getParam('p'))? $this->getRequest()->getParam('p') : 1;
        //get values of current limit. If not the param value then it will set to 1
        $pageSize=($this->getRequest()->getParam('limit'))? $this->getRequest()->getParam('limit') : 10;

        $collection = $this->_customerFactory->create();

        $collection->addAttributeToFilter('parent_wholesaler', $this->getWholesalerId());

        $data = $this->_request->getParams();

        $keyWord_search = $this->_customerSession->getKeywordSearch();

        if (array_key_exists('reset', $data)){
            $this->_customerSession->unsKeywordSearch();
            $this->_requestedParameters['key_word'] = NULL;

            $collection->addAttributeToFilter('parent_wholesaler', $this->getWholesalerId());
        }

        if($keyWord_search!= NULL){
            $collection->addAttributeToFilter(
            array(
                array('attribute' => 'firstname', 'like' => "%" . $keyWord_search . "%"),
                array('attribute' => 'lastname', 'like' => "%" . $keyWord_search . "%"),
                array('attribute' => 'entity_id', 'eq' => $keyWord_search )
                ),
            array(
                array('attribute' =>  'parent_wholesaler', 'eq' => $this->getWholesalerId())
                )
        );
            $this->_requestedParameters['key_word'] = $keyWord_search;
        }
        /*
        * Filter by key word
        */
        if (array_key_exists('key_word', $data) && $data['key_word']!= NULL) 
        {
            if (!array_key_exists('reset', $data)){

                $keyWord = $data['key_word'] ? $data['key_word'] : "";

                $this->_customerSession->setKeywordSearch($keyWord);
                $keyWord_search = $this->_customerSession->getKeywordSearch();

                $collection->addAttributeToFilter(
                    array(
                        array('attribute' => 'firstname', 'like' => "%" . $keyWord_search . "%"),
                        array('attribute' => 'lastname', 'like' => "%" . $keyWord_search . "%"),
                        array('attribute' => 'entity_id', 'eq' => $keyWord_search )
                        ),
                    array(
                        array('attribute' =>  'parent_wholesaler', 'eq' => $this->getWholesalerId())
                        )
                );
                if(!count($collection)){
                    $this->getNotAvailable();
                }
                $this->_requestedParameters['key_word'] = $keyWord_search;
              }

            else{
                $collection->addAttributeToFilter('parent_wholesaler', $this->getWholesalerId());
            }

        }elseif (array_key_exists('submit', $data) && $data['key_word']== NULL) {
            $this->getNotMentioned();
            $collection->addAttributeToFilter('parent_wholesaler', $this->getWholesalerId());

        }
        else{
            $collection->addAttributeToFilter('parent_wholesaler', $this->getWholesalerId());
        }
                    
            $collection->setPageSize($pageSize);
            $collection->setCurPage($page);
            return $collection;

	}

    protected function getWholesalerId()
    {    
        if($this->_customerSession->isLoggedIn()) {

            $customerGroup = $this->_customerSession->getCustomer()->getGroupId();

            if($customerGroup == '2') {
                return $this->_customerSession->getCustomer()->getId();
            } 
        }
    }

     /**
     * @return string
     */
    public function getAddCustomerUrl()
    {
        return $this->getUrl('manage_wholesaler/account/create/id/'.$this->getWholesalerId(), ['_secure' => true]);
    }
     /**
     * @return string
     */
    public function getDeleteCustomerUrl()
    {
        return $this->getUrl('manage_wholesaler/account/deletecustomer');
    }

     /**
     * @return customer export url
     */
    public function getCustomerExportUrl()
    {
        return $this->getUrl('manage_wholesaler/customer/export');
    }

    
    public function getPagenum()
    {
        return $this->request->getParam('p');
    }
    public function getLimit()
    {
        return $this->request->getParam('limit');
    }

    public function getApplyedFilters() {
        return $this->_requestedParameters;
    }
    public function getNotAvailable()
    {
        return $this->messageManager->addWarning(__("No record found for given search"));
    }
    public function getNotMentioned()
    {
        return $this->messageManager->addWarning(__("Mention Customer Name or Number to Search"));
    }


}
