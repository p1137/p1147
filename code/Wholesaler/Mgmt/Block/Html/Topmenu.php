<?php

/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Wholesaler\Mgmt\Block\Html;

use Magento\Framework\Data\Tree\Node;
use Magento\Framework\Data\Tree\NodeFactory;
use Magento\Framework\Data\TreeFactory;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\View\Element\Template;

class Topmenu extends \Magento\Theme\Block\Html\Topmenu {

    /**
     * @var NodeFactory
     */
    private $nodeFactory;

    /**
     * @var TreeFactory
     */
    private $treeFactory;
    private $categoryFactory;
    private $_storeManagerInterface;

    /**
     * @param Template\Context $context
     * @param NodeFactory $nodeFactory
     * @param TreeFactory $treeFactory
     * @param array $data
     */
    public function __construct(
    \Magento\Store\Model\StoreManagerInterface $StoreManagerInterface, \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryFactory, Template\Context $context, NodeFactory $nodeFactory, TreeFactory $treeFactory, array $data = []
    ) {
        $this->_storeManagerInterface = $StoreManagerInterface;
        $this->categoryFactory = $categoryFactory;
        $this->nodeFactory = $nodeFactory;
        $this->treeFactory = $treeFactory;
        parent::__construct($context, $nodeFactory, $treeFactory, $data);
    }

    protected function _getHtml(
    \Magento\Framework\Data\Tree\Node $menuTree, $childrenWrapClass, $limit, $colBrakes = []
    ) {
        $html = '';

        $children = $menuTree->getChildren();
        $parentLevel = $menuTree->getLevel();
        $childLevel = $parentLevel === null ? 0 : $parentLevel + 1;

        $counter = 1;
        $itemPosition = 1;
        $childrenCount = $children->count();

        $parentPositionClass = $menuTree->getPositionClass();
        $itemPositionClassPrefix = $parentPositionClass ? $parentPositionClass . '-' : 'nav-';

        /** @var \Magento\Framework\Data\Tree\Node $child */
        foreach ($children as $child) {
            if ($childLevel === 0 && $child->getData('is_parent_active') === false) {
                continue;
            }
            $child->setLevel($childLevel);
            $child->setIsFirst($counter == 1);
            $child->setIsLast($counter == $childrenCount);
            $child->setPositionClass($itemPositionClassPrefix . $counter);

            $outermostClassCode = '';
            $outermostClass = $menuTree->getOutermostClass();

            if ($childLevel == 0 && $outermostClass) {
                $outermostClassCode = ' class="' . $outermostClass . '" ';
                $currentClass = $child->getClass();

                if (empty($currentClass)) {
                    $child->setClass($outermostClass);
                } else {
                    $child->setClass($currentClass . ' ' . $outermostClass);
                }
            }

            if (count($colBrakes) && $colBrakes[$counter]['colbrake']) {
                $html .= '</ul></li><li class="column"><ul>';
            }

            $html .= '<li ' . $this->_getRenderedMenuItemAttributes($child) . '>';
            $html .= '<a href="' . $this->getCategoryURL($child) . '" ' . $outermostClassCode . '><span>' . $this->escapeHtml(
                            $child->getName()
                    ) . '</span></a>' . $this->_addSubMenu(
                            $child, $childLevel, $childrenWrapClass, $limit
                    ) . '</li>';

            $itemPosition++;
            $counter++;
        }

        if (count($colBrakes) && $limit) {
            $html = '<li class="column"><ul>' . $html . '</ul></li>';
        }
        return $html;
    }

    protected function getCategoryURL($child) {
	$url = "";
	$categoryID = str_replace("category-node-", "", $child->getId());
        $_objectManager = \Magento\Framework\App\ObjectManager::getInstance();	
        $categories = $this->categoryFactory->create()->addAttributeToSelect('*')->addFieldToFilter('disable_category_link', 1);
        foreach ($categories as $category) {
            $categoryIds[] = $category->getId();
        }
        if (isset($categoryIds)) {
            if (in_array($categoryID, $categoryIds)) {
		$_category = "";
		$_category = $_objectManager->create('Magento\Catalog\Model\Category')
                	->load($categoryID);
                if ($_category->getIsProductCustomizable()) {
                    $url = $this->_storeManagerInterface->getStore()->getBaseUrl() . $_category->getCustomizableUrl();
                } else
                    $url = "javascript:void(0)";
            } else {                
                    $url = $child->getUrl();
            }
        } else
            $url = $child->getUrl();

        return $url;
    }

}

