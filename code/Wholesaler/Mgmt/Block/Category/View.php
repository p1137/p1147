<?php

/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Wholesaler\Mgmt\Block\Category;

class View extends \Magento\Catalog\Block\Category\View {

    private $_storeManagerInterface;

    public function __construct(
    \Magento\Store\Model\StoreManagerInterface $StoreManagerInterface, \Magento\Framework\View\Element\Template\Context $context, \Magento\Catalog\Model\Layer\Resolver $layerResolver, \Magento\Framework\Registry $registry, \Magento\Catalog\Helper\Category $categoryHelper, array $data = []
    ) {
        $this->_storeManagerInterface = $StoreManagerInterface;
        $this->_categoryHelper = $categoryHelper;
        $this->_catalogLayer = $layerResolver->get();
        $this->_coreRegistry = $registry;
        parent::__construct($context, $layerResolver, $registry, $categoryHelper, $data);
    }

    public function getBaseUrl() {
        return $this->_storeManagerInterface->getStore()->getBaseUrl();
    }

}
