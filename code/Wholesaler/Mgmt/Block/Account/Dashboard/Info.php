<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Wholesaler\Mgmt\Block\Account\Dashboard;

use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Dashboard Customer Info
 *
 * @api
 * @since 100.0.2
 */
class Info extends \Magento\Framework\View\Element\Template
{

    /**
     * Cached subscription object
     *
     * @var \Magento\Newsletter\Model\Subscriber
     */
    protected $_subscription;

    /**
     * @var \Magento\Newsletter\Model\SubscriberFactory
     */
    protected $_subscriberFactory;

    /**
     * @var \Magento\Customer\Helper\View
     */
    protected $_helperView;

    /**
     * @var \Magento\Customer\Helper\Session\CurrentCustomer
     */
    protected $currentCustomer;

    /**
     * Constructor
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Customer\Helper\Session\CurrentCustomer $currentCustomer
     * @param \Magento\Newsletter\Model\SubscriberFactory $subscriberFactory
     * @param \Magento\Customer\Helper\View $helperView
     * @param array $data
     */
    public function __construct(
        \Magento\Customer\Helper\Session\CurrentCustomer $currentCustomer,
        \Magento\Newsletter\Model\SubscriberFactory $subscriberFactory,
        \Magento\Customer\Model\Customer $customer,
        \Wholesaler\Mgmt\Model\MeasurementFactory $measurementfactory,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Backend\Block\Template\Context $context
    ) {
        $this->_customerSession = $customerSession;
        $this->currentCustomer = $currentCustomer;
        $this->_subscriberFactory = $subscriberFactory;
        $this->_customer = $customer;
        $this->measurementfactory = $measurementfactory;
        parent::__construct($context);

    }

    /**
     * Returns the Magento Customer Model for this block
     *
     * @return \Magento\Customer\Api\Data\CustomerInterface|null
     */
    public function getCurrentCustomer()
    {
        try {
            return $this->currentCustomer->getCustomer();
        } catch (NoSuchEntityException $e) {
            return null;
        }
    }
    
    /**
     * Get the first name of a customer
     *
     * @return string first name
     */
    public function getFirstname()
    {
        $customerObj = $this->_customer->load($this->getCustomerId());
        $fname = $customerObj->getFirstname();
        return $fname;
                 // return $this->_helperView->getCustomerName($this->getCustomer());
    }

    /**
     * Get the last name of a customer
     *
     * @return string last name
     */
    public function getLastname()
    {
        $customerObj = $this->_customer->load($this->getCustomerId());
        $lname = $customerObj->getLastname();
        return $lname;
                // return $this->_helperView->getCustomerName($this->getCustomer());
    }
    /**
     * Get the full name of a customer
     *
     * @return string full name
     */
    public function getName()
    {
        $fullname = $this->getFirstname()." ".$this->getLastname();
        return $fullname;
        // return $this->_helperView->getCustomerName($this->getCustomer());
    }

    /**
     * Get the Id of a customer
     *
     * @return int customer id
     */
    public function getCustomerId()
    {
        $customerId = $this->getRequest()->getParam('id', false);
        return $customerId;
    }

    /**
     * Get the Id of a customer
     *
     * @return int customer id
     */
    public function getCurrentCustomerId()
    {
        $customerId = $this->getCurrentCustomer()->getId();
        return $customerId;
    }

    /**
     * Returns the Magento Customer Model for this block
     *
     * @return \Magento\Customer\Api\Data\CustomerInterface|null
     */
    public function getCustomer()
    {
        $customerObj = $this->_customer->load($this->getCustomerId());
        return $customerObj;
        // return $this->_helperView->getCustomerName($this->getCustomer());
    }


    public function isWholesaler()
    {    
        $customerGroup = $this->_customerSession->getCustomer()->getGroupId();
        if($customerGroup == '2') {

          return "FALSE";
        } 
        else {
        return "TRUE";
        }
    }


     /**
     * @return string
     */
    public function getDeleteMeasurementUrl()
    {
        return $this->getUrl('manage_wholesaler/measurement/deletemeasurement/id/'.$this->getCustomerId().'/');
    }

     /**
     * @return string
     */
    public function getCurrentCustomerDeleteMeasurementUrl()
    {
        return $this->getUrl('manage_wholesaler/measurement/DeleteMeasurementOverride/');
    }

    /**
     * HTML for Billing Address
     *
     * @return \Magento\Framework\Phrase|string
     */
    public function getBodyMeasurements()
    {
        try
        {
            $cust_id = $this->getCustomerId();
            $sample = $this->measurementfactory->create();
            $measurementArray = array();

            $collection = $sample->getCollection()->addFieldToFilter('customer_id', array('eq' => $cust_id));
             foreach($collection as $item)
              {
                $cust_data = $item->getData();
                $measurementArray['measurement_id'] = $cust_data['measurement_id'];
                $measurementArray['json'] = $cust_data['json'];
              } 
              if($measurementArray!= NULL){
                return $measurementArray;
            }
            else {
                $measurementArray['measurement_id'] = '';
                $measurementArray['json'] = '';
                return $measurementArray;
            }
         }
        catch (\Exception $e)
        {
            return __('You have not set your body measurements.');
        }

    }

    /**
     * HTML for Billing Address
     *
     * @return \Magento\Framework\Phrase|string
     */
    public function getCurrentCustomerBodyMeasurements()
    {
        try
        {
            $cust_id = $this->getCurrentCustomer()->getId();
            $sample = $this->measurementfactory->create();
            $measurementArray = array();

            $collection = $sample->getCollection()->addFieldToFilter('customer_id', array('eq' => $cust_id));
             foreach($collection as $item)
              {
                $cust_data = $item->getData();
                $measurementArray['measurement_id'] = $cust_data['measurement_id'];
                $measurementArray['json'] = $cust_data['json'];
              } 
              if($measurementArray!= NULL){
                return $measurementArray;
            }
            else {
                $measurementArray['measurement_id'] = '';
                $measurementArray['json'] = '';
                return $measurementArray;
            }
         }
        catch (\Exception $e)
        {
            return __('You have not set your body measurements.');
        }

    }

    /**
     * Get the Id of a customer
     *
     * @return int customer id
     */
    public function getMeasurementId()
    {
        $dataMeasurement = $this->getBodyMeasurements();
        return $dataMeasurement['measurement_id'];
    }


    /**
     * Get the Id of a customer
     *
     * @return int customer id
     */
    public function getCurrentCustomerMeasurementId()
    {
        $dataMeasurement = $this->getCurrentCustomerBodyMeasurements();
        return $dataMeasurement['measurement_id'];
    }

    /**
     * @return string
     */
    public function getBodyMeasurementsEditUrl()
    {

                $measurementArray = $this->getBodyMeasurements();
                if($measurementArray['json']!='')
                {
                    return $this->_urlBuilder->getUrl(
                    'manage_wholesaler/measurement/index',
                    ['id'=>$this->getCustomerId(),'measurement_id'=>$this->getMeasurementId()]);
                }
                else
                {
                    return $this->_urlBuilder->getUrl(
                    'manage_wholesaler/measurement/index',
                    ['id'=>$this->getCustomerId()]);
                }                
                
    }

    /**
     * @return string
     */
    public function getCurrentCustomerBodyMeasurementsEditUrl()
    {

                $measurementArray = $this->getCurrentCustomerBodyMeasurements();
                if($measurementArray['json']!='')
                {
                    return $this->_urlBuilder->getUrl(
                    'manage_wholesaler/measurement/measurementcurrent',
                    ['id'=>$this->getCurrentCustomerId(),'measurement_id'=>$this->getCurrentCustomerMeasurementId()]);
                }
                else
                {
                    return $this->_urlBuilder->getUrl(
                    'manage_wholesaler/measurement/measurementcurrent',
                    ['id'=>$this->getCurrentCustomerId()]);
                }                
                
    }


    /**
     * @return string
     */
    public function getChangePasswordUrl()
    {
        return $this->_urlBuilder->getUrl('manage_wholesaler/account/edit/id/'.$this->getCustomerId().'/changepass/1');
    }

    /**
     * Get Customer Subscription Object Information
     *
     * @return \Magento\Newsletter\Model\Subscriber
     */
    public function getSubscriptionObject()
    {
        if (!$this->_subscription) {
            $this->_subscription = $this->_createSubscriber();
            $customer = $this->getCustomer();
            if ($customer) {
                $this->_subscription->loadByEmail($customer->getEmail());
            }
        }
        return $this->_subscription;
    }

    /**
     * Gets Customer subscription status
     *
     * @return bool
     *
     * @SuppressWarnings(PHPMD.BooleanGetMethodName)
     */
    public function getIsSubscribed()
    {
        return $this->getSubscriptionObject()->isSubscribed();
    }

    /**
     * Newsletter module availability
     *
     * @return bool
     */
    public function isNewsletterEnabled()
    {
        return $this->getLayout()
            ->getBlockSingleton(\Magento\Customer\Block\Form\Register::class)
            ->isNewsletterEnabled();
    }

    /**
     * @return \Magento\Newsletter\Model\Subscriber
     */
    protected function _createSubscriber()
    {
        return $this->_subscriberFactory->create();
    }

    /**
     * @return string
     */
    // protected function _toHtml()
    // {
    //     return $this->currentCustomer->getCustomerId() ? parent::_toHtml() : '';
    // }


}
