<?php
namespace Wholesaler\Mgmt\Block\Account\Dashboard;
 
use Magento\Backend\Block\Template\Context;
use Magento\Framework\UrlInterface;
use Magento\Customer\Model\SessionFactory;
 
class Profile extends \Magento\Framework\View\Element\Template
{        
    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $urlBuilder;
 
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $customerSession;
 
    /**
    * @var \Magento\Store\Model\StoreManagerInterface $storeManager
    */
    protected $storeManager;
 
    /**
     * @var \Magento\Customer\Model\Customer 
     */
    protected $customerModel;
 
    public function __construct(
        Context $context,
        UrlInterface $urlBuilder,
        SessionFactory $customerSession,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Filesystem\DirectoryList $dir,
        \Magento\Customer\Model\Customer $customerModel,
        \Magento\Store\Model\StoreFactory $storeFactory,
        array $data = []
    )
    {        
        $this->urlBuilder            = $urlBuilder;
        $this->customerSession       = $customerSession->create();
        $this->storeManager          = $storeManager;
        $this->customerModel         = $customerModel;
        $this->storeFactory          = $storeFactory;
        $this->_dir = $dir; 
        
        parent::__construct($context, $data);
 
        $collection = $this->getContracts();
        $this->setCollection($collection);
    }
 
    public function getCurrentCustomer()
    {
        try {
            return $this->customerSession->getCustomer();
        } catch (NoSuchEntityException $e) {
            return null;
        }
    }

    public function getBaseUrl()
    {
        return $this->storeManager->getStore()->getBaseUrl();
    }
 
    public function getMediaUrl()
    {
        return $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
    }
 
    public function getCustomerLogoUrl($logoPath)
    {
        return $this->getMediaUrl() . 'customer' . $logoPath;
    }

    public function getDefaultLogoUrl()
    {
        return $this->getMediaUrl() . 'logo/stores/1/logo.png';
    }

    // public function getCustomerProfileUrl($profilePath)
    // {
    //     return $this->getMediaUrl() . 'customer' . $profilePath;
    // }

    // public function getDefaultProfileUrl()
    // {
    //     return $this->getMediaUrl() . 'logo/stores/1/avtar.png';
    // }

    // public function getProfileUrl()
    // {
    //     $customerData = $this->customerModel->load($this->customerSession->getId());
    //     $profile = $customerData->getData('wholesaler_profile');
    //     if (!empty($profile)) {
    //         // return $this->getCustomerProfileUrl($profile);
    //         return $this->getMediaUrl() . 'logo/stores/1/test-img.png';

    //     }
    //     return $this->getDefaultProfileUrl();
    // }

    public function isWholesaler()
    {    
        $customerData = $this->customerModel->load($this->customerSession->getId());
        $customerGroup = $customerData->getGroupId();
        if($customerGroup == '2') {
              return '1';
        }
        return '';
    }

    public function getParentLogoUrl($parent_id)
    {
        $customerData = $this->customerModel->load($parent_id);
        $logo = $customerData->getData('wholesaler_logo');

        // $file = $this->_dir->getPath('media') . '/logo/stores/1/logo.jpg';
        $file = $this->_dir->getPath('media') .'/customer' . $logo;

        if (!empty($logo) && file_exists($file)) {
            return $this->getCustomerLogoUrl($logo);
            // return $this->getMediaUrl() . 'logo/stores/1/logo.jpg';

        }
        return $this->getDefaultLogoUrl();
    }

    public function getLogoUrl()
    {
        $customerData = $this->customerModel->load($this->customerSession->getId());
        $logo = $customerData->getData('wholesaler_logo');
        // $file = $this->_dir->getPath('media') . '/logo/stores/1/logo.jpg';
        $file = $this->_dir->getPath('media') .'/customer' . $logo;

        if (!empty($logo) && file_exists($file)) {
            return $this->getCustomerLogoUrl($logo);
            // return $this->getMediaUrl() . 'logo/stores/1/logo.jpg';

        }
        return $this->getDefaultLogoUrl();
    }

    public function getStoreName()
    {
        $customerData = $this->customerModel->load($this->customerSession->getId());
        $storename = $customerData->getData('wholesaler_store_name');
        if ($storename!='') {
            return $storename;
        }
        return '';
    }

    public function getStoreId($customer_id = "") {
        if($customer_id == "") {
            $customer_id = $this->customerSession->getId();
        }
        $customerData = $this->customerModel->load($customer_id);
        $storename = $customerData->getData('wholesaler_store_name');
        $store_id = '';
        if ($storename!='') {
            $store_id = $this->storeFactory->create()->load($storename, 'name')->getId();
        }
        return $store_id;
    }

    public function getGstNumber()
    {
        $customerData = $this->customerModel->load($this->customerSession->getId());
        $gst = $customerData->getData('gst_number');
        if ($gst!='') {
            return $gst;
        }
        return '';
    }
    public function getPhoneNumber()
    {
        $customerData = $this->customerModel->load($this->customerSession->getId());
        $mobile = $customerData->getData('mobile');
        if ($mobile!='') {
            return $mobile;
        }
        return '';
    }

}
?>