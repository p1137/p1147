/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

var config = {
    map: {
        '*': {
            customer:'Wholesaler_Mgmt/js/customer',
            design:'Wholesaler_Mgmt/js/design',
            measurement:'Wholesaler_Mgmt/js/measurement'
        }
    }
};
