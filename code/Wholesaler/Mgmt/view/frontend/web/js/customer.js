/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'jquery',
    'Magento_Ui/js/modal/confirm',
    'jquery/ui',
    'mage/translate'
], function ($, confirm) {
    'use strict';

    $.widget('mage.customer', {
        /**
         * Options common to all instances of this widget.
         * @type {Object}
         */
        options: {
            deleteConfirmMessage: $.mage.__('Are you sure you want to delete this customer?')
        },

        /**
         * Bind event handlers for adding and deleting addresses.
         * @private
         */
        _create: function () {
            var options          = this.options,
                addCustomer      = options.addCustomer,
                deleteCustomer   = options.deleteCustomer;

            if (addCustomer) {
                $(document).on('click', addCustomer, this._addCustomer.bind(this));
            }

            if (deleteCustomer) {
                $(document).on('click', deleteCustomer, this._deleteCustomer.bind(this));
            }
        },

        /**
         * Add a new address.
         * @private
         */
        _addCustomer: function () {
            window.location = this.options.addCustomerLocation;
        },

        /**
         * Delete the address whose id is specified in a data attribute after confirmation from the user.
         * @private
         * @param {jQuery.Event} e
         * @return {Boolean}
         */
        _deleteCustomer: function (e) {
            var self = this;

            confirm({
                content: this.options.deleteConfirmMessage,
                actions: {

                    /** @inheritdoc */
                    confirm: function () {
                        if (typeof $(e.target).parent().data('customer') !== 'undefined') {
                            window.location = self.options.deleteUrlPrefix + $(e.target).parent().data('customer') +
                                '/form_key/' + $.mage.cookies.get('form_key');
                        } else {
                            window.location = self.options.deleteUrlPrefix + $(e.target).data('address') +
                                '/form_key/' + $.mage.cookies.get('form_key');
                        }
                    }
                }
            });

            return false;
        }
    });

    return $.mage.customer;
});
