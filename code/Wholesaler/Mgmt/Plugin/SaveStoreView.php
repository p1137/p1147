<?php

namespace Wholesaler\Mgmt\Plugin;
use Magento\Customer\Api\Data\CustomerInterface;


class SaveStoreView
{
    protected $request;

    public function __construct(
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Framework\App\Config\ConfigResource\ConfigInterface $configInterface,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Framework\Filter\FilterManager $filterManager
    ) {
        $this->request = $request;
        $this->configInterface = $configInterface;
        $this->_messageManager = $messageManager;
        $this->filterManager = $filterManager;
    }

    public function getPost()
    {
        return $this->request->getPostValue();
    }

    public function afterExecute(\Magento\Customer\Controller\Account\EditPost $subject, $result)
    {
        $post = $this->getPost();

        if($post['wholesaler_store_name'] == "") {
            return $result;
        }

        $group_id   = 1;
        $name       = $post['wholesaler_store_name'];
        $code       = $post['wholesaler_store_name'];
        $is_active  = 1;
        $sort_order = '';
        $is_default = '';
        $store_id   = (isset($post['wholesaler_store_id']) && $post['wholesaler_store_id'] != '') ? $post['wholesaler_store_id'] : '';

        $store = array(
            'group_id'  => $group_id,
            'name'      => $name,
            'code'      => $code,
            'is_active' => $is_active,
            'sort_order' => $sort_order,
            'is_default' => $is_default,
            'store_id'  => $store_id
        );

        $store_type = "store";
        $store_action = (isset($store_id) && $store_id != '') ? 'edit' : 'add';

        $postData = array(
            'store' =>$store,
            'store_type' => $store_type,
            'store_action' => $store_action
        );

        try {
            $new_store_id = $this->processStoreSave($postData);

            if($new_store_id != '') {
                $this->assignThemeToStore(4, $new_store_id); // 4 = Katalyst Theme Id
            }
        } catch (\Exception $e) {
            $this->_messageManager->addError($e->getMessage()); //For Error Message
        }

        return $result;
    }

    private function assignThemeToStore($theme_id, $store_id)
    {
        $this->configInterface->saveConfig('design/theme/theme_id', $theme_id, 'stores', $store_id);
    }

    /**
     * Process Store model save
     *
     * @param array $postData
     * @throws \Magento\Framework\Exception\LocalizedException
     * @return array
     */
    private function processStoreSave($postData)
    {
        /** @var \Magento\Store\Model\Store $storeModel */
        $_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $storeModel = $_objectManager->create(\Magento\Store\Model\Store::class);
        $postData['store']['name'] = $this->filterManager->removeTags($postData['store']['name']);
        if ($postData['store']['store_id']) {
            $storeModel->load($postData['store']['store_id']);
        }
        $storeModel->setData($postData['store']);
        if ($postData['store']['store_id'] == '') {
            $storeModel->setId(null);
        }
        $groupModel = $_objectManager->create(
            \Magento\Store\Model\Group::class
        )->load(
            $storeModel->getGroupId()
        );
        $storeModel->setWebsiteId($groupModel->getWebsiteId());
        if (!$storeModel->isActive() && $storeModel->isDefault()) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('The default store cannot be disabled')
            );
        }
        try {
            $storeModel->save();
            $this->_messageManager->addSuccess('Store saved successfully'); //For Success Message
        } catch (\Exception $e) {
            $this->_messageManager->addError($e->getMessage());//For Error Message
            return "";
        }

        return $storeModel->getId();
    }
}
