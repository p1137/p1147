<?php

namespace Wholesaler\Mgmt\Plugin;

/**
 * Change website favicon after changin the store
 */
class ChangeFavicon
{
    /**
     * @return string
     */
    public function afterGetDefaultFavicon($return)
    {
        return 'Magento_Theme::logo.png';
    }
}
