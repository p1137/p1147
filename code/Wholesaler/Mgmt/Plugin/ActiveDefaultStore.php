<?php

namespace Wholesaler\Mgmt\Plugin;

/**
 * Active default store view after customer logout.
 */
class ActiveDefaultStore
{
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $customerSession;

    /**
     * @var \Magento\Framework\Controller\Result\RedirectFactory
     */
    protected $redirectFactory;

    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Controller\Result\RedirectFactory $redirectFactory
    ) {
        $this->_storeManager = $storeManager;
        $this->resultRedirectFactory = $redirectFactory;
    }

    public function afterExecute(\Magento\Customer\Controller\Account\Logout $subject, $result)
    {
        $this->_storeManager->setCurrentStore(1); // 1 = default store

        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath($this->_storeManager->getStore()->getBaseUrl().'customer/account/logoutSuccess');

        return $resultRedirect;
    }
}
