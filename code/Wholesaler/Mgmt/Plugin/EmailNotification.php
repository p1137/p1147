<?php

namespace Wholesaler\Mgmt\Plugin;
use Magento\Customer\Api\Data\CustomerInterface;


class EmailNotification extends \Magento\Customer\Model\EmailNotification
{

    public function aroundNewAccount(\Magento\Customer\Model\EmailNotification $subject, \Closure $proceed,CustomerInterface $customer,
        $type = self::NEW_ACCOUNT_EMAIL_REGISTERED,
        $backUrl = '',
        $storeId = 0,
        $sendemailStoreId = null)
    {

        try {
            $old_customer_id = $customer->getCustomAttribute('parent_wholesaler')->getValue();

            if($old_customer_id == 0) {
                $result = $proceed($customer,$type,$backUrl,$storeId,$sendemailStoreId);
            } else{
                $result = $subject->passwordResetConfirmation($customer);
            }

            return $result;
        } catch (\Exception $e) {
            
        }
    }
}
