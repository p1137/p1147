<?php

/**
 * @author Dhirajkumar Deore
 * Copyright © 2018 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Suit\Womenjacket\Controller\Adminhtml\Index;

use Magento\Backend\App\Action\Context;
use Magento\Backend\App\Action;
use Suit\Womenjacket\Model\Womenjacket;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\TestFramework\Inspection\Exception;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\RequestInterface;

//use Magento\Framework\Stdlib\DateTime\DateTime;
//use Magento\Ui\Component\MassAction\Filter;
//use FME\News\Model\ResourceModel\Test\CollectionFactory;

class Save extends \Magento\Backend\App\Action {

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;
    protected $scopeConfig;
    protected $_escaper;
    protected $inlineTranslation;
    protected $_dateFactory;
    protected $_suitstyleFactory;
    protected $_suitsleevesFactory;
    protected $_suitelbowpatchFactory;
    protected $_suitpocketsFactory;
    protected $_suitformallapelsFactory;
    protected $_suitcoatstylesFactory;
    protected $_suitcoatcollarsFactory;
    protected $_suitcoatpocketsFactory;
    protected $_suitcoatsleevesFactory;
    protected $objectManager;
    protected $connection;
    protected $directory;
    protected $rootPath1;
    protected $rootPath;
    protected $mediaPath;

    /**
     * @param Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param DataPersistorInterface $dataPersistor
     */
    public function __construct(
    Context $context, DataPersistorInterface $dataPersistor, \Magento\Framework\Escaper $escaper, \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Magento\Framework\Stdlib\DateTime\DateTimeFactory $dateFactory, \Suit\Womenjacket\Model\SuitstyleFactory $suitstyleFactory, \Suit\Womenjacket\Model\SuitTuxedoTypeFactory $suitTuxedoType,\Suit\Womenjacket\Model\SuitTuxedoColorFactory $suitTuxedoColor,\Suit\Womenjacket\Model\SuitsleevesFactory $suitsleevesFactory, \Suit\Womenjacket\Model\SuitelbowpatchFactory $suitelbowpatchFactory, \Suit\Womenjacket\Model\SuitpocketsFactory $suitpocketsFactory, \Suit\Womenjacket\Model\SuitformallapelsFactory $suitformallapelsFactory, \Suit\Womenjacket\Model\SuitcoatstylesFactory $suitcoatstylesFactory, \Suit\Womenjacket\Model\SuitcoatcollarsFactory $suitcoatcollarsFactory, \Suit\Womenjacket\Model\SuitcoatpocketsFactory $suitcoatpocketsFactory, \Suit\Womenjacket\Model\SuitcoatsleevesFactory $suitcoatsleevesFactory
    ) {
        $this->_suitstyleFactory = $suitstyleFactory;
        $this->_suitsleevesFactory = $suitsleevesFactory;
        $this->_suitelbowpatchFactory = $suitelbowpatchFactory;
        $this->_suitpocketsFactory = $suitpocketsFactory;
        $this->_suitformallapelsFactory = $suitformallapelsFactory;
        $this->_suitcoatstylesFactory = $suitcoatstylesFactory;
        $this->_suitTuxedoType = $suitTuxedoType;
        $this->_suitTuxedoColor = $suitTuxedoColor;
        $this->_suitcoatcollarsFactory = $suitcoatcollarsFactory;
        $this->_suitcoatpocketsFactory = $suitcoatpocketsFactory;
        $this->_suitcoatsleevesFactory = $suitcoatsleevesFactory;
        $this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->connection = $this->objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $this->directory = $this->objectManager->get('\Magento\Framework\Filesystem\DirectoryList');
        $this->rootPath1 = $this->directory->getRoot();
        $fileSystem = $this->objectManager->create('\Magento\Framework\Filesystem');
        $this->mediaPath = $fileSystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA)->getAbsolutePath();
        $this->rootPath = $this->mediaPath . "suit-tool/";
        $this->dataPersistor = $dataPersistor;
        $this->scopeConfig = $scopeConfig;
        $this->_escaper = $escaper;
        $this->_dateFactory = $dateFactory;
        $this->inlineTranslation = $inlineTranslation;
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute() {

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();


        if ($data) {

            $id = $this->getRequest()->getParam('id');

            if (isset($data['status']) && $data['status'] === 'true') {
                $data['status'] = Block::STATUS_ENABLED;
            }
            if (empty($data['id'])) {
                $data['id'] = null;
            }


            /** @var \Magento\Cms\Model\Block $model */
            $model = $this->_objectManager->create('Suit\Womenjacket\Model\Womenjacket')->load($id);
            if (!$model->getId() && $id) {
                $this->messageManager->addError(__('This suit_womenjacket no longer exists.'));
                return $resultRedirect->setPath('*/*/');
            }


            if (isset($data['fabric_thumb'][0]['name']) && isset($data['fabric_thumb'][0]['tmp_name'])) {
                $data['fabric_thumb'] = 'suit-tool/women_fab_suit_images/thumbs/' . $data['fabric_thumb'][0]['name'];
            } elseif (isset($data['fabric_thumb'][0]['name']) && !isset($data['image'][0]['tmp_name'])) {
                $data['fabric_thumb'] = $data['fabric_thumb'][0]['name'];
            } else {
                $data['fabric_thumb'] = null;
            }

            if (isset($data['display_fabric_thumb'][0]['name']) && isset($data['display_fabric_thumb'][0]['tmp_name'])) {
                $data['display_fabric_thumb'] = 'suit-tool/women_fab_suit_images/thumbs/' . $data['display_fabric_thumb'][0]['name'];
            } elseif (isset($data['display_fabric_thumb'][0]['name']) && !isset($data['image'][0]['tmp_name'])) {
                $data['display_fabric_thumb'] = $data['display_fabric_thumb'][0]['name'];
            } else {
                $data['display_fabric_thumb'] = null;
            }

            if (isset($data['fabric_large_image'][0]['name']) && isset($data['fabric_large_image'][0]['tmp_name'])) {
                $data['fabric_large_image'] = 'suit-tool/women_fab_suit_images/thumbs/' . $data['fabric_large_image'][0]['name'];
            } elseif (isset($data['fabric_large_image'][0]['name']) && !isset($data['image'][0]['tmp_name'])) {
                $data['fabric_large_image'] = $data['fabric_large_image'][0]['name'];
            } else {
                $data['fabric_large_image'] = null;
            }
            $data['status'] = 0;
            $model->setData($data);

            $this->inlineTranslation->suspend();
            try {
                $model->save();
                $this->messageManager->addSuccess(__('suit_womenjacket Saved successfully'));
                $this->dataPersistor->clear('suit_womenjacket');


                /* Suit Image Generation Start */
                if ($data['fabric_thumb'] != '') {
                    $tableName = $this->connection->getTableName('generation_cmd');
                    /* color image creation start */
                    $fabric_id = $model->getId();

                    $homepath = $this->rootPath;

                    $fabricThumbImgpath = $this->mediaPath . $data['fabric_thumb'];
                    $fabricName = $fabric_id . "_jacket_fabric_main.png";
                    $fabric_0 = $homepath . 'women_fab_suit_images/' . $fabricName;

//back

                    $fabricName_90 = $fabric_id . "_jacket_fabric_main_90.png";
                    $fabric_90 = $homepath . 'women_fab_suit_images/' . $fabricName_90;

//for zoom view
                    $fabric_zoom = $homepath . "women_fab_suit_images/" . $fabric_id . "_jacket_fabric_zoom.png";
                    $shirtFabImgPath = $homepath . 'women_fab_suit_images/';

                    exec('convert ' . $fabricThumbImgpath . ' -rotate 90 ' . $fabric_90);

                    exec('convert -size 1080x1320 tile:' . $fabric_90 . ' ' . $shirtFabImgPath . $fabric_id . "_jacket_fabric_main_back.png");
                    exec('convert -size 1080x1320 tile:' . $fabric_90 . ' ' . $fabric_90);

                    $fabricName_60 = $fabric_id . "_jacket_fabric_main_60.png";
                    $fabric_60 = $homepath . 'women_fab_suit_images/' . $fabricName_60;



                    exec('convert -size 1080x1320 tile:' . $fabricThumbImgpath . ' ' . $fabric_0);
//created for collar

                    exec('convert ' . $fabric_0 . ' -background "rgba(0,0,0,0.5)" -distort SRT -67 ' . $fabric_60);

                    exec('convert ' . $fabric_0 . ' -background "rgba(0,0,0,0.5)" -distort SRT 80 ' . $shirtFabImgPath . $fabric_id . '_jacket_fabric_cuff_side.png');

//zoom view fabirc
                    exec('convert -size 1200x900 tile:' . $fabricThumbImgpath . ' ' . $fabric_zoom);
//created for collar
                    exec('convert ' . $fabric_zoom . ' -background "rgba(0,0,0,0.5)" -distort SRT 90 ' . $shirtFabImgPath . $fabric_id . '_jacket_fabric_main_back_zoom.png');

                    /* END create fabric image */

                    /* Create plain fabric image */




                    $lapel_fabric_left = $homepath . 'women_fab_suit_images/' . $fabric_id . '_jacket_lapel_fabric_left.png';
                    $lapel_fabric_right = $homepath . 'women_fab_suit_images/' . $fabric_id . '_jacket_lapel_fabric_right.png';
                    exec('convert ' . $fabric_0 . ' -background "rgba(0,0,0,0.5)" -distort SRT 15 ' . $lapel_fabric_left);
                    exec('convert ' . $fabric_0 . ' -background "rgba(0,0,0,0.5)" -distort SRT -15 ' . $lapel_fabric_right);


                    $lapel_fabric_upper_left = $homepath . 'women_fab_suit_images/' . $fabric_id . '_jacket_lapel_fabric_upper_left.png';
                    $lapel_fabric_upper_right = $homepath . 'women_fab_suit_images/' . $fabric_id . '_jacket_lapel_fabric_upper_right.png';
                    exec('convert ' . $fabric_0 . ' -background "rgba(0,0,0,0.5)" -distort SRT -45 ' . $lapel_fabric_upper_left);
                    exec('convert ' . $fabric_0 . ' -background "rgba(0,0,0,0.5)" -distort SRT 45 ' . $lapel_fabric_upper_right);


                    $fabricName_wave_left = $fabric_id . "_jacket_fabric_wave_left.png";
                    $fabricName_wave_right = $fabric_id . "_jacket_fabric_wave_right.png";

                    $wave_left = $homepath . 'women_fab_suit_images/' . $fabricName_wave_left;
                    $wave_right = $homepath . 'women_fab_suit_images/' . $fabricName_wave_right;

                    exec('convert ' . $fabric_0 . ' -background "rgba(0,0,0,0.5)" -distort SRT -5 ' . $wave_left);
                    exec('convert ' . $fabric_0 . ' -background "rgba(0,0,0,0.5)" -distort SRT 5 ' . $wave_right);


// MEN //
//                    Suit Styles
                    $suitstyleFactory = $this->_suitstyleFactory->create()->getCollection();
                    $suitStyle_collection = $suitstyleFactory->getData();

                    $i = 0;
                    foreach ($suitStyle_collection as $suitStyle) {
                        $suitStyleId = $suitStyle['suitstyle_id'];

//view 1
                        $glow_view_1 = $homepath . '' . $suitStyle['glow_image'];
                        $mask_view_1 = $homepath . '' . $suitStyle['mask_image'];
                        $highlight_view_1 = $homepath . '' . $suitStyle['hi_image'];

                        $back_glow_view_1 = $homepath . '' . $suitStyle['back_glow_image'];
                        $back_mask_view_1 = $homepath . '' . $suitStyle['back_mask_image'];
                        $back_highlight_view_1 = $homepath . '' . $suitStyle['back_hi_image'];

                        if ($suitStyle['glow_image'] && $suitStyle['mask_image'] && $suitStyle['hi_image']) {
//mask
                            $cmd = 'composite -compose Dst_In -gravity center ' . $mask_view_1 . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'women_suit_images/jacket_images/style/' . $fabric_id . '_style_' . $suitStyleId . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);

//convert
                            $cmd = 'convert ' . $homepath . 'women_suit_images/jacket_images/style/' . $fabric_id . '_style_' . $suitStyleId . '_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_suit_images/jacket_images/style/' . $fabric_id . '_style_' . $suitStyleId . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//glow
                            $cmd = 'convert ' . $homepath . 'women_suit_images/jacket_images/style/' . $fabric_id . '_style_' . $suitStyleId . '_view_1.png ' . $glow_view_1 . ' -geometry +0+0 -composite ' . $homepath . 'women_suit_images/jacket_images/style/' . $fabric_id . '_style_' . $suitStyleId . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//highlight
                            $cmd = 'composite ' . $highlight_view_1 . ' -compose Overlay  ' . $homepath . 'women_suit_images/jacket_images/style/' . $fabric_id . '_style_' . $suitStyleId . '_view_1.png ' . $homepath . 'women_suit_images/jacket_images/style/' . $fabric_id . '_style_' . $suitStyleId . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }


                        if (!$i) {
                            if ($suitStyle['back_glow_image'] && $suitStyle['back_mask_image'] && $suitStyle['back_hi_image']) {
//mask
                                $cmd = 'composite -compose Dst_In -gravity center ' . $back_mask_view_1 . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'women_suit_images/jacket_images/style/' . $fabric_id . '_style_' . $suitStyleId . '_view_2.png';
                                $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                                $this->connection->query($writeQuery);
//convert
                                $cmd = 'convert ' . $homepath . 'women_suit_images/jacket_images/style/' . $fabric_id . '_style_' . $suitStyleId . '_view_2.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_suit_images/jacket_images/style/' . $fabric_id . '_style_' . $suitStyleId . '_view_2.png';
                                $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                                $this->connection->query($writeQuery);

//glow
                                $cmd = 'convert ' . $homepath . 'women_suit_images/jacket_images/style/' . $fabric_id . '_style_' . $suitStyleId . '_view_2.png ' . $back_glow_view_1 . ' -geometry +0+0 -composite ' . $homepath . 'women_suit_images/jacket_images/style/' . $fabric_id . '_style_' . $suitStyleId . '_view_2.png';
                                $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                                $this->connection->query($writeQuery);

//highlight
                                $cmd = 'composite ' . $back_highlight_view_1 . ' -compose Overlay  ' . $homepath . 'women_suit_images/jacket_images/style/' . $fabric_id . '_style_' . $suitStyleId . '_view_2.png ' . $homepath . 'women_suit_images/jacket_images/style/' . $fabric_id . '_style_' . $suitStyleId . '_view_2.png';
                                $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                                $this->connection->query($writeQuery);
                            }
                        }




                        if ($suitStyleId != 3) {


//Front Collar
                            $glow_collar_view_1 = $homepath . 'woman_glow_mask/suitcollars/commoncollar_shad.png';
                            $mask_collar_view_1 = $homepath . 'woman_glow_mask/suitcollars/commoncollar_mask.png';
                            $highlight_collar_view_1 = $homepath . 'woman_glow_mask/suitcollars/commoncollar_hi.png';

                            if ($glow_collar_view_1 != '' && $mask_collar_view_1 != '' && $highlight_collar_view_1 != '') {
//mask
                                $cmd = 'composite -compose Dst_In -gravity center ' . $mask_collar_view_1 . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'women_suit_images/jacket_images/collar/' . $fabric_id . '_collarstyle_' . $suitStyleId . '_view_3.png';
                                $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                                $this->connection->query($writeQuery);
//convert
                                $cmd = 'convert ' . $homepath . 'women_suit_images/jacket_images/collar/' . $fabric_id . '_collarstyle_' . $suitStyleId . '_view_3.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_suit_images/jacket_images/collar/' . $fabric_id . '_collarstyle_' . $suitStyleId . '_view_3.png';
                                $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                                $this->connection->query($writeQuery);

//glow
                                $cmd = 'convert ' . $homepath . 'women_suit_images/jacket_images/collar/' . $fabric_id . '_collarstyle_' . $suitStyleId . '_view_3.png ' . $glow_collar_view_1 . ' -geometry +0+0 -composite ' . $homepath . 'women_suit_images/jacket_images/collar/' . $fabric_id . '_collarstyle_' . $suitStyleId . '_view_3.png';
                                $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                                $this->connection->query($writeQuery);

//highlight
                                $cmd = 'composite ' . $highlight_collar_view_1 . ' -compose Overlay  ' . $homepath . 'women_suit_images/jacket_images/collar/' . $fabric_id . '_collarstyle_' . $suitStyleId . '_view_3.png ' . $homepath . 'women_suit_images/jacket_images/collar/' . $fabric_id . '_collarstyle_' . $suitStyleId . '_view_3.png';
                                $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                                $this->connection->query($writeQuery);
                            }

// Inner Lining

                            $glow_innerlining_view_2 = $homepath . 'woman_glow_mask/lining/CommonInnerlining_shad.png';
                            $mask_innerlining_view_2 = $homepath . 'woman_glow_mask/lining/CommonInnerlining_mask.png';
                            $highlight_innerlining_view_2 = $homepath . 'woman_glow_mask/lining/CommonInnerlining_hi.png';

                            if ($glow_innerlining_view_2 != '' && $mask_innerlining_view_2 != '' && $highlight_innerlining_view_2 != '') {
//mask
                                $cmd = 'composite -compose Dst_In -gravity center ' . $mask_innerlining_view_2 . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'women_suit_images/jacket_images/lining/' . $fabric_id . '_liningstyle_' . $suitStyleId . '_view_3.png';
                                $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                                $this->connection->query($writeQuery);
//convert
                                $cmd = 'convert ' . $homepath . 'women_suit_images/jacket_images/lining/' . $fabric_id . '_liningstyle_' . $suitStyleId . '_view_3.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_suit_images/jacket_images/lining/' . $fabric_id . '_liningstyle_' . $suitStyleId . '_view_3.png';
                                $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                                $this->connection->query($writeQuery);

//glow
                                $cmd = 'convert ' . $homepath . 'women_suit_images/jacket_images/lining/' . $fabric_id . '_liningstyle_' . $suitStyleId . '_view_3.png ' . $glow_innerlining_view_2 . ' -geometry +0+0 -composite ' . $homepath . 'women_suit_images/jacket_images/lining/' . $fabric_id . '_liningstyle_' . $suitStyleId . '_view_3.png';
                                $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                                $this->connection->query($writeQuery);

//highlight
                                $cmd = 'composite ' . $highlight_innerlining_view_2 . ' -compose Overlay  ' . $homepath . 'women_suit_images/jacket_images/lining/' . $fabric_id . '_liningstyle_' . $suitStyleId . '_view_3.png ' . $homepath . 'women_suit_images/jacket_images/lining/' . $fabric_id . '_liningstyle_' . $suitStyleId . '_view_3.png';
                                $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                                $this->connection->query($writeQuery);
                            }
                        } else {

//Front Collar

                            $glow_collar_view_1 = $homepath . 'woman_glow_mask/suitcollars/MandarinCollar_shad.png';
                            $mask_collar_view_1 = $homepath . 'woman_glow_mask/suitcollars/MandarinCollar_mask.png';
                            $highlight_collar_view_1 = $homepath . 'woman_glow_mask/suitcollars/MandarinCollar_hi.png';

                            if ($glow_collar_view_1 != '' && $mask_collar_view_1 != '' && $highlight_collar_view_1 != '') {
//mask
                                $cmd = 'composite -compose Dst_In -gravity center ' . $mask_collar_view_1 . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'women_suit_images/jacket_images/collar/' . $fabric_id . '_collarstyle_' . $suitStyleId . '_view_3.png';
                                $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                                $this->connection->query($writeQuery);
//convert
                                $cmd = 'convert ' . $homepath . 'women_suit_images/jacket_images/collar/' . $fabric_id . '_collarstyle_' . $suitStyleId . '_view_3.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_suit_images/jacket_images/collar/' . $fabric_id . '_collarstyle_' . $suitStyleId . '_view_3.png';
                                $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                                $this->connection->query($writeQuery);
//glow
                                $cmd = 'convert ' . $homepath . 'women_suit_images/jacket_images/collar/' . $fabric_id . '_collarstyle_' . $suitStyleId . '_view_3.png ' . $glow_collar_view_1 . ' -geometry +0+0 -composite ' . $homepath . 'women_suit_images/jacket_images/collar/' . $fabric_id . '_collarstyle_' . $suitStyleId . '_view_3.png';
                                $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                                $this->connection->query($writeQuery);
//highlight
                                $cmd = 'composite ' . $highlight_collar_view_1 . ' -compose Overlay  ' . $homepath . 'women_suit_images/jacket_images/collar/' . $fabric_id . '_collarstyle_' . $suitStyleId . '_view_3.png ' . $homepath . 'women_suit_images/jacket_images/collar/' . $fabric_id . '_collarstyle_' . $suitStyleId . '_view_3.png';
                                $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                                $this->connection->query($writeQuery);
                            }

// Inner Lining

                            $glow_innerlining_view_2 = $homepath . 'woman_glow_mask/lining/MandarinInnerLining_shad.png';
                            $mask_innerlining_view_2 = $homepath . 'woman_glow_mask/lining/MandarinInnerLining_mask.png';
                            $highlight_innerlining_view_2 = $homepath . 'woman_glow_mask/lining/MandarinInnerLining_hi.png';

                            if ($glow_innerlining_view_2 != '' && $mask_innerlining_view_2 != '' && $highlight_innerlining_view_2 != '') {
//mask
                                $cmd = 'composite -compose Dst_In -gravity center ' . $mask_innerlining_view_2 . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'women_suit_images/jacket_images/lining/' . $fabric_id . '_liningstyle_' . $suitStyleId . '_view_3.png';
                                $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                                $this->connection->query($writeQuery);
//convert
                                $cmd = 'convert ' . $homepath . 'women_suit_images/jacket_images/lining/' . $fabric_id . '_liningstyle_' . $suitStyleId . '_view_3.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_suit_images/jacket_images/lining/' . $fabric_id . '_liningstyle_' . $suitStyleId . '_view_3.png';
                                $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                                $this->connection->query($writeQuery);
//glow
                                $cmd = 'convert ' . $homepath . 'women_suit_images/jacket_images/lining/' . $fabric_id . '_liningstyle_' . $suitStyleId . '_view_3.png ' . $glow_innerlining_view_2 . ' -geometry +0+0 -composite ' . $homepath . 'women_suit_images/jacket_images/lining/' . $fabric_id . '_liningstyle_' . $suitStyleId . '_view_3.png';
                                $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                                $this->connection->query($writeQuery);
//highlight
                                $cmd = 'composite ' . $highlight_innerlining_view_2 . ' -compose Overlay  ' . $homepath . 'women_suit_images/jacket_images/lining/' . $fabric_id . '_liningstyle_' . $suitStyleId . '_view_3.png ' . $homepath . 'women_suit_images/jacket_images/lining/' . $fabric_id . '_liningstyle_' . $suitStyleId . '_view_3.png';
                                $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                                $this->connection->query($writeQuery);
                            }
                        }

                        $i++;
                    }

// Suit Styles
// Suit Sleeves
                    $suitsleevesFactory = $this->_suitsleevesFactory->create()->getCollection();
                    $suitSleeves_collection = $suitsleevesFactory->getData();

                    $i = 0;
                    foreach ($suitSleeves_collection as $suitSleeves) {
                        $suitSleevesId = $suitSleeves['suitsleeves_id'];

//left view 1
                        $left_glow_view_1 = $homepath . '' . $suitSleeves['left_glow_image'];
                        $left_mask_view_1 = $homepath . '' . $suitSleeves['left_mask_image'];
                        $left_highlight_view_1 = $homepath . '' . $suitSleeves['left_hi_image'];

                        if ($suitSleeves['left_glow_image'] && $suitSleeves['left_mask_image'] && $suitSleeves['left_hi_image']) {
//mask
                            $cmd = 'composite -compose Dst_In -gravity center ' . $left_mask_view_1 . ' ' . $wave_left . ' -alpha Set ' . $homepath . 'women_suit_images/jacket_images/sleeves/' . $fabric_id . '_suitsleeves_' . $suitSleevesId . '_left_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//convert
                            $cmd = 'convert ' . $homepath . 'women_suit_images/jacket_images/sleeves/' . $fabric_id . '_suitsleeves_' . $suitSleevesId . '_left_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_suit_images/jacket_images/sleeves/' . $fabric_id . '_suitsleeves_' . $suitSleevesId . '_left_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//glow
                            $cmd = 'convert ' . $homepath . 'women_suit_images/jacket_images/sleeves/' . $fabric_id . '_suitsleeves_' . $suitSleevesId . '_left_view_1.png ' . $left_glow_view_1 . ' -geometry +0+0 -composite ' . $homepath . 'women_suit_images/jacket_images/sleeves/' . $fabric_id . '_suitsleeves_' . $suitSleevesId . '_left_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//highlight
                            $cmd = 'composite ' . $left_highlight_view_1 . ' -compose Overlay  ' . $homepath . 'women_suit_images/jacket_images/sleeves/' . $fabric_id . '_suitsleeves_' . $suitSleevesId . '_left_view_1.png ' . $homepath . 'women_suit_images/jacket_images/sleeves/' . $fabric_id . '_suitsleeves_' . $suitSleevesId . '_left_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }

//right view 1
                        $right_glow_view_1 = $homepath . '' . $suitSleeves['right_glow_image'];
                        $right_mask_view_1 = $homepath . '' . $suitSleeves['right_mask_image'];
                        $right_highlight_view_1 = $homepath . '' . $suitSleeves['right_hi_image'];

                        if ($suitSleeves['right_glow_image'] && $suitSleeves['right_mask_image'] && $suitSleeves['right_hi_image']) {
//mask
                            $cmd = 'composite -compose Dst_In -gravity center ' . $right_mask_view_1 . ' ' . $wave_right . ' -alpha Set ' . $homepath . 'women_suit_images/jacket_images/sleeves/' . $fabric_id . '_suitsleeves_' . $suitSleevesId . '_right_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//convert
                            $cmd = 'convert ' . $homepath . 'women_suit_images/jacket_images/sleeves/' . $fabric_id . '_suitsleeves_' . $suitSleevesId . '_right_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_suit_images/jacket_images/sleeves/' . $fabric_id . '_suitsleeves_' . $suitSleevesId . '_right_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//glow
                            $cmd = 'convert ' . $homepath . 'women_suit_images/jacket_images/sleeves/' . $fabric_id . '_suitsleeves_' . $suitSleevesId . '_right_view_1.png ' . $right_glow_view_1 . ' -geometry +0+0 -composite ' . $homepath . 'women_suit_images/jacket_images/sleeves/' . $fabric_id . '_suitsleeves_' . $suitSleevesId . '_right_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//highlight
                            $cmd = 'composite ' . $right_highlight_view_1 . ' -compose Overlay  ' . $homepath . 'women_suit_images/jacket_images/sleeves/' . $fabric_id . '_suitsleeves_' . $suitSleevesId . '_right_view_1.png ' . $homepath . 'women_suit_images/jacket_images/sleeves/' . $fabric_id . '_suitsleeves_' . $suitSleevesId . '_right_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }

//Compose Left and Right Sleeves View 1

                        $cmd = 'convert ' . $homepath . 'women_suit_images/jacket_images/sleeves/' . $fabric_id . '_suitsleeves_' . $suitSleevesId . '_left_view_1.png ' . $homepath . 'women_suit_images/jacket_images/sleeves/' . $fabric_id . '_suitsleeves_' . $suitSleevesId . '_right_view_1.png -geometry +0+0 -composite ' . $homepath . 'women_suit_images/jacket_images/sleeves/' . $fabric_id . '_suitsleeves_' . $suitSleevesId . '_view_1.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);

                        $cmd = 'convert ' . $homepath . 'women_suit_images/jacket_images/sleeves/' . $fabric_id . '_suitsleeves_' . $suitSleevesId . '_left_view_1.png ' . $homepath . 'women_suit_images/jacket_images/sleeves/' . $fabric_id . '_suitsleeves_' . $suitSleevesId . '_right_view_1.png -geometry +0+0 -composite ' . $homepath . 'women_suit_images/jacket_images/sleeves/' . $fabric_id . '_suitsleeves_' . $suitSleevesId . '_view_3.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);


                        $cmd = "rm " . $homepath . 'women_suit_images/jacket_images/sleeves/' . $fabric_id . '_suitsleeves_' . $suitSleevesId . '_left_view_1.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);

                        $cmd = "rm " . $homepath . 'women_suit_images/jacket_images/sleeves/' . $fabric_id . '_suitsleeves_' . $suitSleevesId . '_right_view_1.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);


//left view 2
                        $left_back_glow_view_2 = $homepath . '' . $suitSleeves['back_left_glow_image'];
                        $left_back_mask_view_2 = $homepath . '' . $suitSleeves['back_left_mask_image'];
                        $left_back_highlight_view_2 = $homepath . '' . $suitSleeves['back_left_hi_image'];

                        if ($suitSleeves['back_left_glow_image'] && $suitSleeves['back_left_mask_image'] && $suitSleeves['back_left_hi_image']) {
//mask
                            $cmd = 'composite -compose Dst_In -gravity center ' . $left_back_mask_view_2 . ' ' . $wave_right . ' -alpha Set ' . $homepath . 'women_suit_images/jacket_images/sleeves/' . $fabric_id . '_suitsleeves_' . $suitSleevesId . '_left_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//convert
                            $cmd = 'convert ' . $homepath . 'women_suit_images/jacket_images/sleeves/' . $fabric_id . '_suitsleeves_' . $suitSleevesId . '_left_view_2.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_suit_images/jacket_images/sleeves/' . $fabric_id . '_suitsleeves_' . $suitSleevesId . '_left_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//glow
                            $cmd = 'convert ' . $homepath . 'women_suit_images/jacket_images/sleeves/' . $fabric_id . '_suitsleeves_' . $suitSleevesId . '_left_view_2.png ' . $left_back_glow_view_2 . ' -geometry +0+0 -composite ' . $homepath . 'women_suit_images/jacket_images/sleeves/' . $fabric_id . '_suitsleeves_' . $suitSleevesId . '_left_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//highlight
                            $cmd = 'composite ' . $left_back_highlight_view_2 . ' -compose Overlay  ' . $homepath . 'women_suit_images/jacket_images/sleeves/' . $fabric_id . '_suitsleeves_' . $suitSleevesId . '_left_view_2.png ' . $homepath . 'women_suit_images/jacket_images/sleeves/' . $fabric_id . '_suitsleeves_' . $suitSleevesId . '_left_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }

//right view 2
                        $right_back_glow_view_2 = $homepath . '' . $suitSleeves['back_right_glow_image'];
                        $right_back_mask_view_2 = $homepath . '' . $suitSleeves['back_right_mask_image'];
                        $right_back_highlight_view_2 = $homepath . '' . $suitSleeves['back_right_hi_image'];

                        if ($suitSleeves['back_right_glow_image'] && $suitSleeves['back_right_mask_image'] && $suitSleeves['back_right_hi_image']) {
//mask
                            $cmd = 'composite -compose Dst_In -gravity center ' . $right_back_mask_view_2 . ' ' . $wave_left . ' -alpha Set ' . $homepath . 'women_suit_images/jacket_images/sleeves/' . $fabric_id . '_suitsleeves_' . $suitSleevesId . '_right_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//convert
                            $cmd = 'convert ' . $homepath . 'women_suit_images/jacket_images/sleeves/' . $fabric_id . '_suitsleeves_' . $suitSleevesId . '_right_view_2.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_suit_images/jacket_images/sleeves/' . $fabric_id . '_suitsleeves_' . $suitSleevesId . '_right_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//glow
                            $cmd = 'convert  ' . $homepath . 'women_suit_images/jacket_images/sleeves/' . $fabric_id . '_suitsleeves_' . $suitSleevesId . '_right_view_2.png ' . $right_back_glow_view_2 . ' -geometry +0+0 -composite ' . $homepath . 'women_suit_images/jacket_images/sleeves/' . $fabric_id . '_suitsleeves_' . $suitSleevesId . '_right_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//highlight
                            $cmd = 'composite ' . $right_back_highlight_view_2 . ' -compose Overlay  ' . $homepath . 'women_suit_images/jacket_images/sleeves/' . $fabric_id . '_suitsleeves_' . $suitSleevesId . '_right_view_2.png ' . $homepath . 'women_suit_images/jacket_images/sleeves/' . $fabric_id . '_suitsleeves_' . $suitSleevesId . '_right_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }

                        $i++;

//Compose Left and Right Sleeves View 2

                        $cmd = 'convert ' . $homepath . 'women_suit_images/jacket_images/sleeves/' . $fabric_id . '_suitsleeves_' . $suitSleevesId . '_left_view_2.png ' . $homepath . 'women_suit_images/jacket_images/sleeves/' . $fabric_id . '_suitsleeves_' . $suitSleevesId . '_right_view_2.png -geometry +0+0 -composite ' . $homepath . 'women_suit_images/jacket_images/sleeves/' . $fabric_id . '_suitsleeves_' . $suitSleevesId . '_view_2.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);

                        $cmd = "rm " . $homepath . 'women_suit_images/jacket_images/sleeves/' . $fabric_id . '_suitsleeves_' . $suitSleevesId . '_left_view_2.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);

                        $cmd = "rm " . $homepath . 'women_suit_images/jacket_images/sleeves/' . $fabric_id . '_suitsleeves_' . $suitSleevesId . '_right_view_2.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);
                    }

// Suit Sleeves
// Suit Elbowpatches
                    $suitelbowpatchFactory = $this->_suitelbowpatchFactory->create()->getCollection();
                    $suitElbowpatch_collection = $suitelbowpatchFactory->getData();

                    foreach ($suitElbowpatch_collection as $suitElbowpatch) {
                        $suitElbowpatchId = $suitElbowpatch['suitelbowpatch_id'];

//left view
                        $left_glow_view_1 = $homepath . '' . $suitElbowpatch['left_glow_image'];
                        $left_mask_view_1 = $homepath . '' . $suitElbowpatch['left_mask_image'];
                        $left_highlight_view_1 = $homepath . '' . $suitElbowpatch['left_hi_image'];

                        if ($suitElbowpatch['left_glow_image'] && $suitElbowpatch['left_mask_image'] && $suitElbowpatch['left_hi_image']) {
//mask
                            $cmd = 'composite -compose Dst_In -gravity center ' . $left_mask_view_1 . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'women_suit_images/jacket_images/elbow_patches/' . $fabric_id . '_elbowpatch_' . $suitElbowpatchId . '_left_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//convert
                            $cmd = 'convert ' . $homepath . 'women_suit_images/jacket_images/elbow_patches/' . $fabric_id . '_elbowpatch_' . $suitElbowpatchId . '_left_view_2.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_suit_images/jacket_images/elbow_patches/' . $fabric_id . '_elbowpatch_' . $suitElbowpatchId . '_left_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//glow
                            $cmd = 'convert ' . $homepath . 'women_suit_images/jacket_images/elbow_patches/' . $fabric_id . '_elbowpatch_' . $suitElbowpatchId . '_left_view_2.png ' . $left_glow_view_1 . ' -geometry +0+0 -composite ' . $homepath . 'women_suit_images/jacket_images/elbow_patches/' . $fabric_id . '_elbowpatch_' . $suitElbowpatchId . '_left_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//highlight
                            $cmd = 'composite ' . $left_highlight_view_1 . ' -compose Overlay  ' . $homepath . 'women_suit_images/jacket_images/elbow_patches/' . $fabric_id . '_elbowpatch_' . $suitElbowpatchId . '_left_view_2.png ' . $homepath . 'women_suit_images/jacket_images/elbow_patches/' . $fabric_id . '_elbowpatch_' . $suitElbowpatchId . '_left_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }

//right view
                        $right_glow_view_1 = $homepath . '' . $suitElbowpatch['right_glow_image'];
                        $right_mask_view_1 = $homepath . '' . $suitElbowpatch['right_mask_image'];
                        $right_highlight_view_1 = $homepath . '' . $suitElbowpatch['right_hi_image'];

                        if ($suitElbowpatch['right_glow_image'] && $suitElbowpatch['right_mask_image'] && $suitElbowpatch['right_hi_image']) {
//mask
                            $cmd = 'composite -compose Dst_In -gravity center ' . $right_mask_view_1 . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'women_suit_images/jacket_images/elbow_patches/' . $fabric_id . '_elbowpatch_' . $suitElbowpatchId . '_right_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//convert
                            $cmd = 'convert ' . $homepath . 'women_suit_images/jacket_images/elbow_patches/' . $fabric_id . '_elbowpatch_' . $suitElbowpatchId . '_right_view_2.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_suit_images/jacket_images/elbow_patches/' . $fabric_id . '_elbowpatch_' . $suitElbowpatchId . '_right_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//glow
                            $cmd = 'convert ' . $homepath . 'women_suit_images/jacket_images/elbow_patches/' . $fabric_id . '_elbowpatch_' . $suitElbowpatchId . '_right_view_2.png ' . $right_glow_view_1 . ' -geometry +0+0 -composite ' . $homepath . 'women_suit_images/jacket_images/elbow_patches/' . $fabric_id . '_elbowpatch_' . $suitElbowpatchId . '_right_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//highlight
                            $cmd = 'composite ' . $right_highlight_view_1 . ' -compose Overlay  ' . $homepath . 'women_suit_images/jacket_images/elbow_patches/' . $fabric_id . '_elbowpatch_' . $suitElbowpatchId . '_right_view_2.png ' . $homepath . 'women_suit_images/jacket_images/elbow_patches/' . $fabric_id . '_elbowpatch_' . $suitElbowpatchId . '_right_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }

                        $cmd = 'convert ' . $homepath . 'women_suit_images/jacket_images/elbow_patches/' . $fabric_id . '_elbowpatch_' . $suitElbowpatchId . '_right_view_2.png ' . $homepath . 'women_suit_images/jacket_images/elbow_patches/' . $fabric_id . '_elbowpatch_' . $suitElbowpatchId . '_left_view_2.png -geometry +0+0 -composite ' . $homepath . 'women_suit_images/jacket_images/elbow_patches/' . $fabric_id . '_elbowpatch_' . $suitElbowpatchId . '_view_2.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);
                    }

// Suit Elbowpatches
// Suit Collars
//back view
                    $glow_collar_view_2 = $homepath . 'woman_glow_mask/suitcollars/backCollar_shad.png';
                    $mask_collar_view_2 = $homepath . 'woman_glow_mask/suitcollars/backCollar_mask.png';
                    $highlight_collar_view_2 = $homepath . 'woman_glow_mask/suitcollars/backCollar_hi.png';

                    if ($glow_collar_view_2 != '' && $mask_collar_view_2 != '' && $highlight_collar_view_2 != '') {
//mask
                        $cmd = 'composite -compose Dst_In -gravity center ' . $mask_collar_view_2 . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'women_suit_images/jacket_images/back_collar/' . $fabric_id . '_backCollar_view_2.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);
//convert
                        $cmd = 'convert ' . $homepath . 'women_suit_images/jacket_images/back_collar/' . $fabric_id . '_backCollar_view_2.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_suit_images/jacket_images/back_collar/' . $fabric_id . '_backCollar_view_2.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);
//glow
                        $cmd = 'convert ' . $homepath . 'women_suit_images/jacket_images/back_collar/' . $fabric_id . '_backCollar_view_2.png ' . $glow_collar_view_2 . ' -geometry +0+0 -composite ' . $homepath . 'women_suit_images/jacket_images/back_collar/' . $fabric_id . '_backCollar_view_2.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);
//highlight
                        $cmd = 'composite ' . $highlight_collar_view_2 . ' -compose Overlay  ' . $homepath . 'women_suit_images/jacket_images/back_collar/' . $fabric_id . '_backCollar_view_2.png ' . $homepath . 'women_suit_images/jacket_images/back_collar/' . $fabric_id . '_backCollar_view_2.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);
                    }

// Suit Collars
// Suit Necklining

                    $glow_necklining_view_2 = $homepath . 'woman_glow_mask/suitnecklining/NeckLining_shad.png';
                    $mask_necklining_view_2 = $homepath . 'woman_glow_mask/suitnecklining/NeckLining_mask.png';
                    $highlight_necklining_view_2 = $homepath . 'woman_glow_mask/suitnecklining/NeckLining_hi.png';

                    if ($glow_necklining_view_2 != '' && $mask_necklining_view_2 != '' && $highlight_necklining_view_2 != '') {
//mask
                        $cmd = 'composite -compose Dst_In -gravity center ' . $mask_necklining_view_2 . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'women_suit_images/jacket_images/suitnecklining/' . $fabric_id . '_suitnecklining_view_2.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);
//convert
                        $cmd = 'convert ' . $homepath . 'women_suit_images/jacket_images/suitnecklining/' . $fabric_id . '_suitnecklining_view_2.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_suit_images/jacket_images/suitnecklining/' . $fabric_id . '_suitnecklining_view_2.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);
//glow
                        $cmd = 'convert ' . $homepath . 'women_suit_images/jacket_images/suitnecklining/' . $fabric_id . '_suitnecklining_view_2.png ' . $glow_necklining_view_2 . '  -geometry +0+0 -composite ' . $homepath . 'women_suit_images/jacket_images/suitnecklining/' . $fabric_id . '_suitnecklining_view_2.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);
//highlight
                        $cmd = 'composite ' . $highlight_necklining_view_2 . ' -compose Overlay  ' . $homepath . 'women_suit_images/jacket_images/suitnecklining/' . $fabric_id . '_suitnecklining_view_2.png ' . $homepath . 'women_suit_images/jacket_images/suitnecklining/' . $fabric_id . '_suitnecklining_view_2.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);
                    }

// Suit Necklining
// Suit Pockets
                    $suitpocketsFactory = $this->_suitpocketsFactory->create()->getCollection();
                    $suitPockets_collection = $suitpocketsFactory->getData();

                    foreach ($suitPockets_collection as $suitPockets) {
                        $suitPocketsId = $suitPockets['suitpocket_id'];

//left view 1
                        $left_glow_view_1 = $homepath . '' . $suitPockets['left_glow_image'];
                        $left_mask_view_1 = $homepath . '' . $suitPockets['left_mask_image'];
                        $left_highlight_view_1 = $homepath . '' . $suitPockets['left_hi_image'];


                        if ($suitPockets['left_glow_image'] && $suitPockets['left_mask_image'] && $suitPockets['left_hi_image']) {
//mask
                            $cmd = 'composite -compose Dst_In -gravity center ' . $left_mask_view_1 . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'women_suit_images/jacket_images/pockets/' . $fabric_id . '_pockets_' . $suitPocketsId . '_left_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//convert
                            $cmd = 'convert ' . $homepath . 'women_suit_images/jacket_images/pockets/' . $fabric_id . '_pockets_' . $suitPocketsId . '_left_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_suit_images/jacket_images/pockets/' . $fabric_id . '_pockets_' . $suitPocketsId . '_left_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//glow
                            $cmd = 'convert ' . $homepath . 'women_suit_images/jacket_images/pockets/' . $fabric_id . '_pockets_' . $suitPocketsId . '_left_view_1.png ' . $left_glow_view_1 . ' -geometry +0+0 -composite ' . $homepath . 'women_suit_images/jacket_images/pockets/' . $fabric_id . '_pockets_' . $suitPocketsId . '_left_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//highlight
                            $cmd = 'composite ' . $left_highlight_view_1 . ' -compose Overlay  ' . $homepath . 'women_suit_images/jacket_images/pockets/' . $fabric_id . '_pockets_' . $suitPocketsId . '_left_view_1.png ' . $homepath . 'women_suit_images/jacket_images/pockets/' . $fabric_id . '_pockets_' . $suitPocketsId . '_left_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }

//right view 1
                        $right_glow_view_1 = $homepath . '' . $suitPockets['right_glow_image'];
                        $right_mask_view_1 = $homepath . '' . $suitPockets['right_mask_image'];
                        $right_highlight_view_1 = $homepath . '' . $suitPockets['right_hi_image'];

                        if ($suitPockets['right_glow_image'] && $suitPockets['right_mask_image'] && $suitPockets['right_hi_image']) {
//mask
                            $cmd = 'composite -compose Dst_In -gravity center ' . $right_mask_view_1 . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'women_suit_images/jacket_images/pockets/' . $fabric_id . '_pockets_' . $suitPocketsId . '_right_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//convert
                            $cmd = 'convert ' . $homepath . 'women_suit_images/jacket_images/pockets/' . $fabric_id . '_pockets_' . $suitPocketsId . '_right_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_suit_images/jacket_images/pockets/' . $fabric_id . '_pockets_' . $suitPocketsId . '_right_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//glow
                            $cmd = 'convert ' . $homepath . 'women_suit_images/jacket_images/pockets/' . $fabric_id . '_pockets_' . $suitPocketsId . '_right_view_1.png ' . $right_glow_view_1 . ' -geometry +0+0 -composite ' . $homepath . 'women_suit_images/jacket_images/pockets/' . $fabric_id . '_pockets_' . $suitPocketsId . '_right_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//highlight
                            $cmd = 'composite ' . $right_highlight_view_1 . ' -compose Overlay  ' . $homepath . 'women_suit_images/jacket_images/pockets/' . $fabric_id . '_pockets_' . $suitPocketsId . '_right_view_1.png ' . $homepath . 'women_suit_images/jacket_images/pockets/' . $fabric_id . '_pockets_' . $suitPocketsId . '_right_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }

//ticket view 1
                        $ticket_glow_view_1 = $homepath . '' . $suitPockets['ticket_glow_image'];
                        $ticket_mask_view_1 = $homepath . '' . $suitPockets['ticket_mask_image'];
                        $ticket_highlight_view_1 = $homepath . '' . $suitPockets['ticket_hi_image'];

                        if ($suitPockets['ticket_glow_image'] && $suitPockets['ticket_mask_image'] && $suitPockets['ticket_hi_image']) {
//mask
                            $cmd = 'composite -compose Dst_In -gravity center ' . $ticket_mask_view_1 . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'women_suit_images/jacket_images/pockets/' . $fabric_id . '_ticketpockets_' . $suitPocketsId . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//convert
                            $cmd = 'convert ' . $homepath . 'women_suit_images/jacket_images/pockets/' . $fabric_id . '_ticketpockets_' . $suitPocketsId . '_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_suit_images/jacket_images/pockets/' . $fabric_id . '_ticketpockets_' . $suitPocketsId . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//glow
                            $cmd = 'convert ' . $homepath . 'women_suit_images/jacket_images/pockets/' . $fabric_id . '_ticketpockets_' . $suitPocketsId . '_view_1.png ' . $ticket_glow_view_1 . ' -geometry +0+0 -composite ' . $homepath . 'women_suit_images/jacket_images/pockets/' . $fabric_id . '_ticketpockets_' . $suitPocketsId . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//highlight
                            $cmd = 'composite ' . $ticket_highlight_view_1 . ' -compose Overlay  ' . $homepath . 'women_suit_images/jacket_images/pockets/' . $fabric_id . '_ticketpockets_' . $suitPocketsId . '_view_1.png ' . $homepath . 'women_suit_images/jacket_images/pockets/' . $fabric_id . '_ticketpockets_' . $suitPocketsId . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }

                        $cmd = 'convert ' . $homepath . 'women_suit_images/jacket_images/pockets/' . $fabric_id . '_pockets_' . $suitPocketsId . '_left_view_1.png ' . $homepath . 'women_suit_images/jacket_images/pockets/' . $fabric_id . '_pockets_' . $suitPocketsId . '_right_view_1.png -geometry +0+0 -composite ' . $homepath . 'women_suit_images/jacket_images/pockets/' . $fabric_id . '_pockets_' . $suitPocketsId . '_view_1.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);

                        $cmd = "rm " . $homepath . 'women_suit_images/jacket_images/pockets/' . $fabric_id . '_pockets_' . $suitPocketsId . '_left_view_1.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);

                        $cmd = "rm " . $homepath . 'women_suit_images/jacket_images/pockets/' . $fabric_id . '_pockets_' . $suitPocketsId . '_right_view_1.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);


                        /* Casual Style Pockets * */

//left view 1
                        $left_glow_view_1 = $homepath . '' . $suitPockets['casual_left_glow'];
                        $left_mask_view_1 = $homepath . '' . $suitPockets['casual_left_mask'];
                        $left_highlight_view_1 = $homepath . '' . $suitPockets['casual_left_hi'];

                        if ($suitPockets['casual_left_glow'] && $suitPockets['casual_left_mask'] && $suitPockets['casual_left_hi']) {
//mask
                            $cmd = 'composite -compose Dst_In -gravity center ' . $left_mask_view_1 . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'women_suit_images/jacket_images/pockets/' . $fabric_id . '_casual_pockets_' . $suitPocketsId . '_left_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//convert
                            $cmd = 'convert ' . $homepath . 'women_suit_images/jacket_images/pockets/' . $fabric_id . '_casual_pockets_' . $suitPocketsId . '_left_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_suit_images/jacket_images/pockets/' . $fabric_id . '_casual_pockets_' . $suitPocketsId . '_left_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//glow
                            $cmd = 'convert ' . $homepath . 'women_suit_images/jacket_images/pockets/' . $fabric_id . '_casual_pockets_' . $suitPocketsId . '_left_view_1.png ' . $left_glow_view_1 . ' -geometry +0+0 -composite ' . $homepath . 'women_suit_images/jacket_images/pockets/' . $fabric_id . '_casual_pockets_' . $suitPocketsId . '_left_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//highlight
                            $cmd = 'composite ' . $left_highlight_view_1 . ' -compose Overlay  ' . $homepath . 'women_suit_images/jacket_images/pockets/' . $fabric_id . '_casual_pockets_' . $suitPocketsId . '_left_view_1.png ' . $homepath . 'women_suit_images/jacket_images/pockets/' . $fabric_id . '_casual_pockets_' . $suitPocketsId . '_left_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }

//right view 1
                        $right_glow_view_1 = $homepath . '' . $suitPockets['casual_right_glow'];
                        $right_mask_view_1 = $homepath . '' . $suitPockets['casual_right_mask'];
                        $right_highlight_view_1 = $homepath . '' . $suitPockets['casual_right_hi'];

                        if ($suitPockets['casual_right_glow'] && $suitPockets['casual_right_mask'] && $suitPockets['casual_right_hi']) {
//mask
                            $cmd = 'composite -compose Dst_In -gravity center ' . $right_mask_view_1 . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'women_suit_images/jacket_images/pockets/' . $fabric_id . '_casual_pockets_' . $suitPocketsId . '_right_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//convert
                            $cmd = 'convert ' . $homepath . 'women_suit_images/jacket_images/pockets/' . $fabric_id . '_casual_pockets_' . $suitPocketsId . '_right_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_suit_images/jacket_images/pockets/' . $fabric_id . '_casual_pockets_' . $suitPocketsId . '_right_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//glow
                            $cmd = 'convert ' . $homepath . 'women_suit_images/jacket_images/pockets/' . $fabric_id . '_casual_pockets_' . $suitPocketsId . '_right_view_1.png ' . $right_glow_view_1 . ' -geometry +0+0 -composite ' . $homepath . 'women_suit_images/jacket_images/pockets/' . $fabric_id . '_casual_pockets_' . $suitPocketsId . '_right_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//highlight
                            $cmd = 'composite ' . $right_highlight_view_1 . ' -compose Overlay  ' . $homepath . 'women_suit_images/jacket_images/pockets/' . $fabric_id . '_casual_pockets_' . $suitPocketsId . '_right_view_1.png ' . $homepath . 'women_suit_images/jacket_images/pockets/' . $fabric_id . '_casual_pockets_' . $suitPocketsId . '_right_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }

//ticket view 1
                        $ticket_glow_view_1 = $homepath . '' . $suitPockets['casual_ticket_glow'];
                        $ticket_mask_view_1 = $homepath . '' . $suitPockets['casual_ticket_mask'];
                        $ticket_highlight_view_1 = $homepath . '' . $suitPockets['casual_ticket_hi'];

                        if ($suitPockets['casual_ticket_glow'] && $suitPockets['casual_ticket_mask'] && $suitPockets['casual_ticket_hi']) {
//mask
                            $cmd = 'composite -compose Dst_In -gravity center ' . $ticket_mask_view_1 . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'women_suit_images/jacket_images/pockets/' . $fabric_id . '_casualticketpockets_' . $suitPocketsId . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//convert
                            $cmd = 'convert ' . $homepath . 'women_suit_images/jacket_images/pockets/' . $fabric_id . '_casualticketpockets_' . $suitPocketsId . '_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_suit_images/jacket_images/pockets/' . $fabric_id . '_casualticketpockets_' . $suitPocketsId . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//glow
                            $cmd = 'convert ' . $homepath . 'women_suit_images/jacket_images/pockets/' . $fabric_id . '_casualticketpockets_' . $suitPocketsId . '_view_1.png ' . $ticket_glow_view_1 . ' -geometry +0+0 -composite ' . $homepath . 'women_suit_images/jacket_images/pockets/' . $fabric_id . '_casualticketpockets_' . $suitPocketsId . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//highlight
                            $cmd = 'composite ' . $ticket_highlight_view_1 . ' -compose Overlay  ' . $homepath . 'women_suit_images/jacket_images/pockets/' . $fabric_id . '_casualticketpockets_' . $suitPocketsId . '_view_1.png ' . $homepath . 'women_suit_images/jacket_images/pockets/' . $fabric_id . '_casualticketpockets_' . $suitPocketsId . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }

                        $cmd = 'convert ' . $homepath . 'women_suit_images/jacket_images/pockets/' . $fabric_id . '_casual_pockets_' . $suitPocketsId . '_left_view_1.png ' . $homepath . 'women_suit_images/jacket_images/pockets/' . $fabric_id . '_casual_pockets_' . $suitPocketsId . '_right_view_1.png -geometry +0+0 -composite ' . $homepath . 'women_suit_images/jacket_images/pockets/' . $fabric_id . '_casual_pockets_' . $suitPocketsId . '_view_1.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);

                        $cmd = "rm " . $homepath . 'women_suit_images/jacket_images/pockets/' . $fabric_id . '_casual_pockets_' . $suitPocketsId . '_left_view_1.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);

                        $cmd = "rm " . $homepath . 'women_suit_images/jacket_images/pockets/' . $fabric_id . '_casual_pockets_' . $suitPocketsId . '_right_view_1.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);
                    }

//breast pocket
                    $glow_view_1 = $homepath . 'woman_glow_mask/suitpockets/Bresat_Pocket_shad.png';
                    $mask_view_1 = $homepath . 'woman_glow_mask/suitpockets/Bresat_Pocket_mask.png';
                    $highlight_view_1 = $homepath . 'woman_glow_mask/suitpockets/Bresat_Pocket_hi.png';

                    if ($glow_view_1 && $mask_view_1 && $highlight_view_1) {
//mask
                        $cmd = 'composite -compose Dst_In -gravity center ' . $mask_view_1 . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'women_suit_images/jacket_images/pockets/' . $fabric_id . '_breastpocket_view_1.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);
//convert
                        $cmd = 'convert ' . $homepath . 'women_suit_images/jacket_images/pockets/' . $fabric_id . '_breastpocket_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_suit_images/jacket_images/pockets/' . $fabric_id . '_breastpocket_view_1.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);
//glow
                        $cmd = 'convert ' . $homepath . 'women_suit_images/jacket_images/pockets/' . $fabric_id . '_breastpocket_view_1.png ' . $glow_view_1 . ' -geometry +0+0 -composite ' . $homepath . 'women_suit_images/jacket_images/pockets/' . $fabric_id . '_breastpocket_view_1.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);
//highlight
                        $cmd = 'composite ' . $highlight_view_1 . ' -compose Overlay  ' . $homepath . 'women_suit_images/jacket_images/pockets/' . $fabric_id . '_breastpocket_view_1.png ' . $homepath . 'women_suit_images/jacket_images/pockets/' . $fabric_id . '_breastpocket_view_1.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);
                    }

// tuxedo breast pocket start 
                    $suittuxedotype = $this->_suitTuxedoType->create()->getCollection();
                    $suittuxedotype_collection = $suittuxedotype->getData();

                    $suittuxedocolor = $this->_suitTuxedoColor->create()->getCollection();
                    $suittuxedocolor_collection = $suittuxedocolor->getData();


                    foreach ($suittuxedocolor_collection as $tuxedoColor) {
                    $suitTuxedoId = $tuxedoColor['suittuxedocolor_id'];
                    $thumb_img = $tuxedoColor['thumb_img'];

                    $fabric_tuxedo = $homepath . $thumb_img;
                    $fabric_0_tuxedo = $homepath . 'women_fab_suit_images/tuxedo'.$suitTuxedoId.'.png';

                    exec('convert -size 1080x1320 tile:' . $fabric_tuxedo . ' ' . $fabric_0_tuxedo);

                    exec('convert -size 1080x1320 tile:' . $fabric_tuxedo);

                    $glow_view_1 = $homepath . 'woman_glow_mask/suitpockets/Bresat_Pocket_shad.png';
                    $mask_view_1 = $homepath . 'woman_glow_mask/suitpockets/Bresat_Pocket_mask.png';
                    $highlight_view_1 = $homepath . 'woman_glow_mask/suitpockets/Bresat_Pocket_hi.png';

                    if ($glow_view_1 && $mask_view_1 && $highlight_view_1) {
//mask
                        $cmd = 'composite -compose Dst_In -gravity center ' . $mask_view_1 . ' ' . $fabric_0_tuxedo . ' -alpha Set ' . $homepath . 'women_suit_images/jacket_images/pockets/tuxedo/tuxedocolor_'.$suitTuxedoId.'_breastpocket_view_1.png';

                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);
//convert
                        $cmd = 'convert ' . $homepath . 'women_suit_images/jacket_images/pockets/tuxedo/tuxedocolor_'.$suitTuxedoId.'_breastpocket_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_suit_images/jacket_images/pockets/tuxedo/tuxedocolor_'.$suitTuxedoId.'_breastpocket_view_1.png';

                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);
//glow
                        $cmd = 'convert ' . $homepath . 'women_suit_images/jacket_images/pockets/tuxedo/tuxedocolor_'.$suitTuxedoId.'_breastpocket_view_1.png ' . $glow_view_1 . ' -geometry +0+0 -composite ' . $homepath . 'women_suit_images/jacket_images/pockets/tuxedo/tuxedocolor_'.$suitTuxedoId.'_breastpocket_view_1.png';

                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);
//highlight
                        $cmd = 'composite ' . $highlight_view_1 . ' -compose Overlay  ' . $homepath . 'women_suit_images/jacket_images/pockets/tuxedo/tuxedocolor_'.$suitTuxedoId.'_breastpocket_view_1.png ' . $homepath . 'women_suit_images/jacket_images/pockets/tuxedo/tuxedocolor_'.$suitTuxedoId.'_breastpocket_view_1.png';

                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);
                    }
                }

//tuxedo breast pocket end 


// Suit Pockets
// Suit Formal lapels
                    $suitformallapelsFactory = $this->_suitformallapelsFactory->create()->getCollection();
                    $suitLapel_collection = $suitformallapelsFactory->getData();

                    foreach ($suitLapel_collection as $suitLapel) {
                        $suitLapelId = $suitLapel['suitformallapels_id'];
                        $styleId = $suitLapel['style'];
                        $lapelId = $suitLapel['lapel'];
                        $sizeId = $suitLapel['size'];

//view UL
                        $glow_view_UL = $homepath . '' . $suitLapel['upper_left_glow_image'];
                        $mask_view_UL = $homepath . '' . $suitLapel['upper_left_mask_image'];
                        $highlight_view_UL = $homepath . '' . $suitLapel['upper_left_hi_image'];

                        if ($suitLapel['upper_left_glow_image'] && $suitLapel['upper_left_mask_image'] && $suitLapel['upper_left_hi_image']) {
//mask
                            $cmd = 'composite -compose Dst_In -gravity center ' . $mask_view_UL . ' ' . $lapel_fabric_upper_left . ' -alpha Set ' . $homepath . 'women_suit_images/jacket_images/lapel/' . $fabric_id . '_lapelstyle_' . $styleId . '_size_' . $sizeId . '_lapeltype_' . $lapelId . '_upper_left_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//convert
                            $cmd = 'convert ' . $homepath . 'women_suit_images/jacket_images/lapel/' . $fabric_id . '_lapelstyle_' . $styleId . '_size_' . $sizeId . '_lapeltype_' . $lapelId . '_upper_left_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_suit_images/jacket_images/lapel/' . $fabric_id . '_lapelstyle_' . $styleId . '_size_' . $sizeId . '_lapeltype_' . $lapelId . '_upper_left_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//glow
                            $cmd = 'convert ' . $homepath . 'women_suit_images/jacket_images/lapel/' . $fabric_id . '_lapelstyle_' . $styleId . '_size_' . $sizeId . '_lapeltype_' . $lapelId . '_upper_left_view_1.png ' . $glow_view_UL . ' -geometry +0+0 -composite ' . $homepath . 'women_suit_images/jacket_images/lapel/' . $fabric_id . '_lapelstyle_' . $styleId . '_size_' . $sizeId . '_lapeltype_' . $lapelId . '_upper_left_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//highlight
                            $cmd = 'composite ' . $highlight_view_UL . ' -compose Overlay  ' . $homepath . 'women_suit_images/jacket_images/lapel/' . $fabric_id . '_lapelstyle_' . $styleId . '_size_' . $sizeId . '_lapeltype_' . $lapelId . '_upper_left_view_1.png ' . $homepath . 'women_suit_images/jacket_images/lapel/' . $fabric_id . '_lapelstyle_' . $styleId . '_size_' . $sizeId . '_lapeltype_' . $lapelId . '_upper_left_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }

//view UR
                        $glow_view_UR = $homepath . '' . $suitLapel['upper_right_glow_image'];
                        $mask_view_UR = $homepath . '' . $suitLapel['upper_right_mask_image'];
                        $highlight_view_UR = $homepath . '' . $suitLapel['upper_right_hi_image'];

                        if ($suitLapel['upper_right_glow_image'] && $suitLapel['upper_right_mask_image'] && $suitLapel['upper_right_hi_image']) {
//mask
                            $cmd = 'composite -compose Dst_In -gravity center ' . $mask_view_UR . ' ' . $lapel_fabric_upper_right . ' -alpha Set ' . $homepath . 'women_suit_images/jacket_images/lapel/' . $fabric_id . '_lapelstyle_' . $styleId . '_size_' . $sizeId . '_lapeltype_' . $lapelId . '_upper_right_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//convert
                            $cmd = 'convert ' . $homepath . 'women_suit_images/jacket_images/lapel/' . $fabric_id . '_lapelstyle_' . $styleId . '_size_' . $sizeId . '_lapeltype_' . $lapelId . '_upper_right_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_suit_images/jacket_images/lapel/' . $fabric_id . '_lapelstyle_' . $styleId . '_size_' . $sizeId . '_lapeltype_' . $lapelId . '_upper_right_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//glow
                            $cmd = 'convert ' . $homepath . 'women_suit_images/jacket_images/lapel/' . $fabric_id . '_lapelstyle_' . $styleId . '_size_' . $sizeId . '_lapeltype_' . $lapelId . '_upper_right_view_1.png ' . $glow_view_UR . ' -geometry +0+0 -composite ' . $homepath . 'women_suit_images/jacket_images/lapel/' . $fabric_id . '_lapelstyle_' . $styleId . '_size_' . $sizeId . '_lapeltype_' . $lapelId . '_upper_right_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//highlight
                            $cmd = 'composite ' . $highlight_view_UR . ' -compose Overlay  ' . $homepath . 'women_suit_images/jacket_images/lapel/' . $fabric_id . '_lapelstyle_' . $styleId . '_size_' . $sizeId . '_lapeltype_' . $lapelId . '_upper_right_view_1.png ' . $homepath . 'women_suit_images/jacket_images/lapel/' . $fabric_id . '_lapelstyle_' . $styleId . '_size_' . $sizeId . '_lapeltype_' . $lapelId . '_upper_right_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }

//view LL
                        $glow_view_LL = $homepath . '' . $suitLapel['lower_left_glow_image'];
                        $mask_view_LL = $homepath . '' . $suitLapel['lower_left_mask_image'];
                        $highlight_view_LL = $homepath . '' . $suitLapel['lower_left_hi_image'];

                        if ($suitLapel['lower_left_glow_image'] && $suitLapel['lower_left_mask_image'] && $suitLapel['lower_left_hi_image']) {
//mask
                            $cmd = 'composite -compose Dst_In -gravity center ' . $mask_view_LL . ' ' . $lapel_fabric_left . ' -alpha Set ' . $homepath . 'women_suit_images/jacket_images/lapel/' . $fabric_id . '_lapelstyle_' . $styleId . '_size_' . $sizeId . '_lapeltype_' . $lapelId . '_lower_left_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//convert
                            $cmd = 'convert ' . $homepath . 'women_suit_images/jacket_images/lapel/' . $fabric_id . '_lapelstyle_' . $styleId . '_size_' . $sizeId . '_lapeltype_' . $lapelId . '_lower_left_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_suit_images/jacket_images/lapel/' . $fabric_id . '_lapelstyle_' . $styleId . '_size_' . $sizeId . '_lapeltype_' . $lapelId . '_lower_left_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//glow
                            $cmd = 'convert ' . $homepath . 'women_suit_images/jacket_images/lapel/' . $fabric_id . '_lapelstyle_' . $styleId . '_size_' . $sizeId . '_lapeltype_' . $lapelId . '_lower_left_view_1.png ' . $glow_view_LL . ' -geometry +0+0 -composite ' . $homepath . 'women_suit_images/jacket_images/lapel/' . $fabric_id . '_lapelstyle_' . $styleId . '_size_' . $sizeId . '_lapeltype_' . $lapelId . '_lower_left_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//highlight
                            $cmd = 'composite ' . $highlight_view_LL . ' -compose Overlay  ' . $homepath . 'women_suit_images/jacket_images/lapel/' . $fabric_id . '_lapelstyle_' . $styleId . '_size_' . $sizeId . '_lapeltype_' . $lapelId . '_lower_left_view_1.png ' . $homepath . 'women_suit_images/jacket_images/lapel/' . $fabric_id . '_lapelstyle_' . $styleId . '_size_' . $sizeId . '_lapeltype_' . $lapelId . '_lower_left_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }

//view LR
                        $glow_view_LR = $homepath . '' . $suitLapel['lower_right_glow_image'];
                        $mask_view_LR = $homepath . '' . $suitLapel['lower_right_mask_image'];
                        $highlight_view_LR = $homepath . '' . $suitLapel['lower_right_hi_image'];

                        if ($suitLapel['lower_right_glow_image'] && $suitLapel['lower_right_mask_image'] && $suitLapel['lower_right_hi_image']) {
//mask
                            $cmd = 'composite -compose Dst_In -gravity center ' . $mask_view_LR . ' ' . $lapel_fabric_right . ' -alpha Set ' . $homepath . 'women_suit_images/jacket_images/lapel/' . $fabric_id . '_lapelstyle_' . $styleId . '_size_' . $sizeId . '_lapeltype_' . $lapelId . '_lower_right_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//convert
                            $cmd = 'convert ' . $homepath . 'women_suit_images/jacket_images/lapel/' . $fabric_id . '_lapelstyle_' . $styleId . '_size_' . $sizeId . '_lapeltype_' . $lapelId . '_lower_right_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_suit_images/jacket_images/lapel/' . $fabric_id . '_lapelstyle_' . $styleId . '_size_' . $sizeId . '_lapeltype_' . $lapelId . '_lower_right_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//glow
                            $cmd = 'convert ' . $homepath . 'women_suit_images/jacket_images/lapel/' . $fabric_id . '_lapelstyle_' . $styleId . '_size_' . $sizeId . '_lapeltype_' . $lapelId . '_lower_right_view_1.png ' . $glow_view_LR . ' -geometry +0+0 -composite ' . $homepath . 'women_suit_images/jacket_images/lapel/' . $fabric_id . '_lapelstyle_' . $styleId . '_size_' . $sizeId . '_lapeltype_' . $lapelId . '_lower_right_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//highlight
                            $cmd = 'composite ' . $highlight_view_LR . ' -compose Overlay  ' . $homepath . 'women_suit_images/jacket_images/lapel/' . $fabric_id . '_lapelstyle_' . $styleId . '_size_' . $sizeId . '_lapeltype_' . $lapelId . '_lower_right_view_1.png ' . $homepath . 'women_suit_images/jacket_images/lapel/' . $fabric_id . '_lapelstyle_' . $styleId . '_size_' . $sizeId . '_lapeltype_' . $lapelId . '_lower_right_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }


                        $cmd = 'convert ' . $homepath . 'women_suit_images/jacket_images/lapel/' . $fabric_id . '_lapelstyle_' . $styleId . '_size_' . $sizeId . '_lapeltype_' . $lapelId . '_upper_left_view_1.png ' . $homepath . 'women_suit_images/jacket_images/lapel/' . $fabric_id . '_lapelstyle_' . $styleId . '_size_' . $sizeId . '_lapeltype_' . $lapelId . '_upper_right_view_1.png -geometry +0+0 -composite ' . $homepath . 'women_suit_images/jacket_images/lapel/' . $fabric_id . '_lapelstyle_' . $styleId . '_size_' . $sizeId . '_lapeltype_' . $lapelId . '_view_1.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);

//                        if (file_exists($homepath . 'women_suit_images/jacket_images/lapel/' . $fabric_id . '_lapelstyle_' . $styleId . '_size_' . $sizeId . '_lapeltype_' . $lapelId . '_lower_left_view_1.png')) {
                        $cmd = 'convert ' . $homepath . 'women_suit_images/jacket_images/lapel/' . $fabric_id . '_lapelstyle_' . $styleId . '_size_' . $sizeId . '_lapeltype_' . $lapelId . '_lower_left_view_1.png ' . $homepath . 'women_suit_images/jacket_images/lapel/' . $fabric_id . '_lapelstyle_' . $styleId . '_size_' . $sizeId . '_lapeltype_' . $lapelId . '_view_1.png -geometry +0+0 -composite ' . $homepath . 'women_suit_images/jacket_images/lapel/' . $fabric_id . '_lapelstyle_' . $styleId . '_size_' . $sizeId . '_lapeltype_' . $lapelId . '_view_1.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);
//                        }
//                        if (file_exists($homepath . 'women_suit_images/jacket_images/lapel/' . $fabric_id . '_lapelstyle_' . $styleId . '_size_' . $sizeId . '_lapeltype_' . $lapelId . '_lower_right_view_1.png')) {
                        $cmd = 'convert ' . $homepath . 'women_suit_images/jacket_images/lapel/' . $fabric_id . '_lapelstyle_' . $styleId . '_size_' . $sizeId . '_lapeltype_' . $lapelId . '_lower_right_view_1.png ' . $homepath . 'women_suit_images/jacket_images/lapel/' . $fabric_id . '_lapelstyle_' . $styleId . '_size_' . $sizeId . '_lapeltype_' . $lapelId . '_view_1.png -geometry +0+0 -composite ' . $homepath . 'women_suit_images/jacket_images/lapel/' . $fabric_id . '_lapelstyle_' . $styleId . '_size_' . $sizeId . '_lapeltype_' . $lapelId . '_view_1.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);
//                        }

                        $cmd = "rm " . $homepath . 'women_suit_images/jacket_images/lapel/' . $fabric_id . '_lapelstyle_' . $styleId . '_size_' . $sizeId . '_lapeltype_' . $lapelId . '_upper_left_view_1.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);

                        $cmd = "rm " . $homepath . 'women_suit_images/jacket_images/lapel/' . $fabric_id . '_lapelstyle_' . $styleId . '_size_' . $sizeId . '_lapeltype_' . $lapelId . '_upper_right_view_1.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);

                        $cmd = "rm " . $homepath . 'women_suit_images/jacket_images/lapel/' . $fabric_id . '_lapelstyle_' . $styleId . '_size_' . $sizeId . '_lapeltype_' . $lapelId . '_lower_left_view_1.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);

                        $cmd = "rm " . $homepath . 'women_suit_images/jacket_images/lapel/' . $fabric_id . '_lapelstyle_' . $styleId . '_size_' . $sizeId . '_lapeltype_' . $lapelId . '_lower_right_view_1.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);
                    }

// Suit Formal lapels



// Suit black tuxedo start
//tuxedo black and white fabric

                    $suittuxedotype = $this->_suitTuxedoType->create()->getCollection();
                    $suittuxedotype_collection = $suittuxedotype->getData();

                    $suittuxedocolor = $this->_suitTuxedoColor->create()->getCollection();
                    $suittuxedocolor_collection = $suittuxedocolor->getData();


                    foreach ($suittuxedocolor_collection as $tuxedoColor) {
                    $suitTuxedoId = $tuxedoColor['suittuxedocolor_id'];
                    $thumb_img = $tuxedoColor['thumb_img'];

                    $fabric_tuxedo = $homepath . $thumb_img;
                    $fabric_0_tuxedo = $homepath . 'women_fab_suit_images/tuxedo'.$suitTuxedoId.'.png';

                    exec('convert -size 1080x1320 tile:' . $fabric_tuxedo . ' ' . $fabric_0_tuxedo);

                    exec('convert -size 1080x1320 tile:' . $fabric_tuxedo);


                    $lapel_tuxedo_fabric_left = $homepath . 'women_fab_suit_images/jacket_lapel_tuxedo_fabric_left'.$suitTuxedoId.'.png';
                    $lapel_tuxedo_fabric_right = $homepath . 'women_fab_suit_images/jacket_lapel_tuxedo_fabric_right'.$suitTuxedoId.'.png';
                    exec('convert ' . $fabric_0_tuxedo . ' -background "rgba(0,0,0,0.5)" -distort SRT 15 ' . $lapel_tuxedo_fabric_left);
                    exec('convert ' . $fabric_0_tuxedo . ' -background "rgba(0,0,0,0.5)" -distort SRT -15 ' . $lapel_tuxedo_fabric_right);


                    $lapel_tuxedo_fabric_upper_left = $homepath . 'women_fab_suit_images/jacket_lapel_tuxedo_fabric_upper_left'.$suitTuxedoId.'.png';
                    $lapel_tuxedo_fabric_upper_right = $homepath . 'women_fab_suit_images/jacket_lapel_tuxedo_fabric_upper_right'.$suitTuxedoId.'.png';
                    exec('convert ' . $fabric_0_tuxedo . ' -background "rgba(0,0,0,0.5)" -distort SRT -45 ' . $lapel_tuxedo_fabric_upper_left);
                    exec('convert ' . $fabric_0_tuxedo . ' -background "rgba(0,0,0,0.5)" -distort SRT 45 ' . $lapel_tuxedo_fabric_upper_right);

                    $suitformallapelsFactory = $this->_suitformallapelsFactory->create()->getCollection();
                    $suitLapel_collection = $suitformallapelsFactory->getData();


                    foreach ($suitLapel_collection as $suitLapel) {
                        $suitLapelId = $suitLapel['suitformallapels_id'];
                        $styleId = $suitLapel['style'];
                        $lapelId = $suitLapel['lapel'];
                        $sizeId = $suitLapel['size'];

//Tuxedo lapel view UL start
                        $glow_view_UL = $homepath . '' . $suitLapel['upper_left_glow_image'];
                        $mask_view_UL = $homepath . '' . $suitLapel['upper_left_mask_image'];
                        $highlight_view_UL = $homepath . '' . $suitLapel['upper_left_hi_image'];


                        if ($suitLapel['upper_left_glow_image'] && $suitLapel['upper_left_mask_image'] && $suitLapel['upper_left_hi_image']) {
//mask
                            $cmd = 'composite -compose Dst_In -gravity center ' . $mask_view_UL . ' ' . $lapel_tuxedo_fabric_upper_left . ' -alpha Set ' . $homepath . 'women_suit_images/jacket_images/lapel/tuxedo/tuxedo_lapelstyle_' . $styleId . '_size_' . $sizeId .'_lapeltype_' . $lapelId . '_tuxedocolor_' . $suitTuxedoId . '_upper_left_view_1.png';

                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//convert
                            $cmd = 'convert ' . $homepath . 'women_suit_images/jacket_images/lapel/tuxedo/tuxedo_lapelstyle_' . $styleId . '_size_' . $sizeId .'_lapeltype_' . $lapelId . '_tuxedocolor_' . $suitTuxedoId .'_upper_left_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_suit_images/jacket_images/lapel/tuxedo/tuxedo_lapelstyle_' . $styleId . '_size_' . $sizeId .'_lapeltype_' . $lapelId .'_tuxedocolor_' . $suitTuxedoId . '_upper_left_view_1.png';

                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);


//glow
                            $cmd = 'convert ' . $homepath . 'women_suit_images/jacket_images/lapel/tuxedo/tuxedo_lapelstyle_' . $styleId . '_size_' . $sizeId .'_lapeltype_' . $lapelId . '_tuxedocolor_' . $suitTuxedoId .'_upper_left_view_1.png ' . $glow_view_UL . ' -geometry +0+0 -composite ' . $homepath . 'women_suit_images/jacket_images/lapel/tuxedo/tuxedo_lapelstyle_' . $styleId . '_size_' . $sizeId .'_lapeltype_' . $lapelId .'_tuxedocolor_' . $suitTuxedoId . '_upper_left_view_1.png';

                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);

//highlight
                            $cmd = 'composite ' . $highlight_view_UL . ' -compose Overlay  ' . $homepath . 'women_suit_images/jacket_images/lapel/tuxedo/tuxedo_lapelstyle_' . $styleId . '_size_' . $sizeId .'_lapeltype_' . $lapelId . '_tuxedocolor_' . $suitTuxedoId .'_upper_left_view_1.png ' . $homepath . 'women_suit_images/jacket_images/lapel/tuxedo/tuxedo_lapelstyle_' . $styleId . '_size_' . $sizeId .'_lapeltype_' . $lapelId .'_tuxedocolor_' . $suitTuxedoId . '_upper_left_view_1.png';

                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);

                        }

//Tuxedo lapel view UL end

//Tuxedo lapel view UR start
                        $glow_view_UR = $homepath . '' . $suitLapel['upper_right_glow_image'];
                        $mask_view_UR = $homepath . '' . $suitLapel['upper_right_mask_image'];
                        $highlight_view_UR = $homepath . '' . $suitLapel['upper_right_hi_image'];


                        if ($suitLapel['upper_right_glow_image'] && $suitLapel['upper_right_mask_image'] && $suitLapel['upper_right_hi_image']) {
//mask
                            $cmd = 'composite -compose Dst_In -gravity center ' . $mask_view_UR . ' ' . $lapel_tuxedo_fabric_upper_right . ' -alpha Set ' . $homepath . 'women_suit_images/jacket_images/lapel/tuxedo/tuxedo_lapelstyle_' . $styleId . '_size_' . $sizeId .'_lapeltype_' . $lapelId .'_tuxedocolor_' . $suitTuxedoId . '_upper_right_view_1.png';

                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);

//convert
                            $cmd = 'convert ' . $homepath . 'women_suit_images/jacket_images/lapel/tuxedo/tuxedo_lapelstyle_' . $styleId . '_size_' . $sizeId .'_lapeltype_' . $lapelId .'_tuxedocolor_' . $suitTuxedoId . '_upper_right_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_suit_images/jacket_images/lapel/tuxedo/tuxedo_lapelstyle_' . $styleId . '_size_' . $sizeId .'_lapeltype_' . $lapelId .'_tuxedocolor_' . $suitTuxedoId . '_upper_right_view_1.png';

                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);


//glow
                            $cmd = 'convert ' . $homepath . 'women_suit_images/jacket_images/lapel/tuxedo/tuxedo_lapelstyle_' . $styleId . '_size_' . $sizeId .'_lapeltype_' . $lapelId . '_tuxedocolor_' . $suitTuxedoId .'_upper_right_view_1.png ' . $glow_view_UR . ' -geometry +0+0 -composite ' . $homepath . 'women_suit_images/jacket_images/lapel/tuxedo/tuxedo_lapelstyle_' . $styleId . '_size_' . $sizeId .'_lapeltype_' . $lapelId .'_tuxedocolor_' . $suitTuxedoId . '_upper_right_view_1.png';

                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);


//highlight
                            $cmd = 'composite ' . $highlight_view_UR . ' -compose Overlay  ' . $homepath . 'women_suit_images/jacket_images/lapel/tuxedo/tuxedo_lapelstyle_' . $styleId . '_size_' . $sizeId . '_lapeltype_' . $lapelId .'_tuxedocolor_' . $suitTuxedoId . '_upper_right_view_1.png ' . $homepath . 'women_suit_images/jacket_images/lapel/tuxedo/tuxedo_lapelstyle_' . $styleId . '_size_' . $sizeId .'_lapeltype_' . $lapelId .'_tuxedocolor_' . $suitTuxedoId . '_upper_right_view_1.png';

                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }


//Tuxedo lapel view UR end

//Tuxedo lapel view LL start
                        $glow_view_LL = $homepath . '' . $suitLapel['lower_left_glow_image'];
                        $mask_view_LL = $homepath . '' . $suitLapel['lower_left_mask_image'];
                        $highlight_view_LL = $homepath . '' . $suitLapel['lower_left_hi_image'];

                        if ($suitLapel['lower_left_glow_image'] && $suitLapel['lower_left_mask_image'] && $suitLapel['lower_left_hi_image']) {
 
//mask
                            $cmd = 'composite -compose Dst_In -gravity center ' . $mask_view_LL . ' ' . $lapel_tuxedo_fabric_left . ' -alpha Set ' . $homepath . 'women_suit_images/jacket_images/lapel/tuxedo/tuxedo_lapelstyle_' . $styleId . '_size_' . $sizeId .'_lapeltype_' . $lapelId . '_tuxedocolor_' . $suitTuxedoId .'_lower_left_view_1.png';

                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);

//convert
                            $cmd = 'convert ' . $homepath . 'women_suit_images/jacket_images/lapel/tuxedo/tuxedo_lapelstyle_' . $styleId . '_size_' . $sizeId .'_lapeltype_' . $lapelId . '_tuxedocolor_' . $suitTuxedoId .'_lower_left_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_suit_images/jacket_images/lapel/tuxedo/tuxedo_lapelstyle_' . $styleId . '_size_' . $sizeId .'_lapeltype_' . $lapelId .'_tuxedocolor_' . $suitTuxedoId . '_lower_left_view_1.png';

                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);


//glow
                            $cmd = 'convert ' . $homepath . 'women_suit_images/jacket_images/lapel/tuxedo/tuxedo_lapelstyle_' . $styleId . '_size_' . $sizeId .'_lapeltype_' . $lapelId . '_tuxedocolor_' . $suitTuxedoId .'_lower_left_view_1.png ' . $glow_view_LL . ' -geometry +0+0 -composite ' . $homepath . 'women_suit_images/jacket_images/lapel/tuxedo/tuxedo_lapelstyle_' . $styleId . '_size_' . $sizeId .'_lapeltype_' . $lapelId .'_tuxedocolor_' . $suitTuxedoId . '_lower_left_view_1.png';

                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);


//highlight
                            $cmd = 'composite ' . $highlight_view_LL . ' -compose Overlay  ' . $homepath . 'women_suit_images/jacket_images/lapel/tuxedo/tuxedo_lapelstyle_' . $styleId . '_size_' . $sizeId .'_lapeltype_' . $lapelId . '_tuxedocolor_' . $suitTuxedoId .'_lower_left_view_1.png ' . $homepath . 'women_suit_images/jacket_images/lapel/tuxedo/tuxedo_lapelstyle_' . $styleId . '_size_' . $sizeId .'_lapeltype_' . $lapelId . '_tuxedocolor_' . $suitTuxedoId .'_lower_left_view_1.png';

                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }


//Tuxedo lapel view LL end

//Tuxedo lapel view LR start
                        $glow_view_LR = $homepath . '' . $suitLapel['lower_right_glow_image'];
                        $mask_view_LR = $homepath . '' . $suitLapel['lower_right_mask_image'];
                        $highlight_view_LR = $homepath . '' . $suitLapel['lower_right_hi_image'];


                        if ($suitLapel['lower_right_glow_image'] && $suitLapel['lower_right_mask_image'] && $suitLapel['lower_right_hi_image']) {
//mask
                            $cmd = 'composite -compose Dst_In -gravity center ' . $mask_view_LR . ' ' . $lapel_tuxedo_fabric_right . ' -alpha Set ' . $homepath . 'women_suit_images/jacket_images/lapel/tuxedo/tuxedo_lapelstyle_' . $styleId . '_size_' . $sizeId .'_lapeltype_' . $lapelId .'_tuxedocolor_' . $suitTuxedoId . '_lower_right_view_1.png';

                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);


//convert
                            $cmd = 'convert ' . $homepath . 'women_suit_images/jacket_images/lapel/tuxedo/tuxedo_lapelstyle_' . $styleId . '_size_' . $sizeId .'_lapeltype_' . $lapelId . '_tuxedocolor_' . $suitTuxedoId .'_lower_right_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_suit_images/jacket_images/lapel/tuxedo/tuxedo_lapelstyle_' . $styleId . '_size_' . $sizeId .'_lapeltype_' . $lapelId .'_tuxedocolor_' . $suitTuxedoId . '_lower_right_view_1.png';

                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);

//glow
                            $cmd = 'convert ' . $homepath . 'women_suit_images/jacket_images/lapel/tuxedo/tuxedo_lapelstyle_' . $styleId . '_size_' . $sizeId . '_lapeltype_' . $lapelId .'_tuxedocolor_' . $suitTuxedoId . '_lower_right_view_1.png ' . $glow_view_LR . ' -geometry +0+0 -composite ' . $homepath . 'women_suit_images/jacket_images/lapel/tuxedo/tuxedo_lapelstyle_' . $styleId . '_size_' . $sizeId .'_lapeltype_' . $lapelId . '_tuxedocolor_' . $suitTuxedoId .'_lower_right_view_1.png';

                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);

//highlight
                            $cmd = 'composite ' . $highlight_view_LR . ' -compose Overlay  ' . $homepath . 'women_suit_images/jacket_images/lapel/tuxedo/tuxedo_lapelstyle_' . $styleId . '_size_' . $sizeId .'_lapeltype_' . $lapelId .'_tuxedocolor_' . $suitTuxedoId . '_lower_right_view_1.png ' . $homepath . 'women_suit_images/jacket_images/lapel/tuxedo/tuxedo_lapelstyle_' . $styleId . '_size_' . $sizeId .'_lapeltype_' . $lapelId .'_tuxedocolor_' . $suitTuxedoId . '_lower_right_view_1.png';

                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }


//Tuxedo lapel view LR end

// combine tuxedo upper left-upper right

                        $cmd = 'convert ' . $homepath . 'women_suit_images/jacket_images/lapel/tuxedo/tuxedo_lapelstyle_' . $styleId . '_size_' . $sizeId .'_lapeltype_' . $lapelId . '_tuxedocolor_' . $suitTuxedoId .'_upper_left_view_1.png ' . $homepath . 'women_suit_images/jacket_images/lapel/tuxedo/tuxedo_lapelstyle_' . $styleId . '_size_' . $sizeId .'_lapeltype_' . $lapelId .'_tuxedocolor_' . $suitTuxedoId . '_upper_right_view_1.png -geometry +0+0 -composite ' . $homepath . 'women_suit_images/jacket_images/lapel/tuxedo/tuxedo_lapelstyle_' . $styleId . '_size_' . $sizeId .'_lapeltype_' . $lapelId .'_tuxedocolor_' . $suitTuxedoId . '_upper_view_1.png';

                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);

// combine tuxedo lower left-lower right

                        $cmd = 'convert ' . $homepath . 'women_suit_images/jacket_images/lapel/tuxedo/tuxedo_lapelstyle_' . $styleId . '_size_' . $sizeId .'_lapeltype_' . $lapelId .'_tuxedocolor_' . $suitTuxedoId . '_lower_left_view_1.png ' . $homepath . 'women_suit_images/jacket_images/lapel/tuxedo/tuxedo_lapelstyle_' . $styleId . '_size_' . $sizeId .'_lapeltype_' . $lapelId .'_tuxedocolor_' . $suitTuxedoId . '_lower_right_view_1.png -geometry +0+0 -composite ' . $homepath . 'women_suit_images/jacket_images/lapel/tuxedo/tuxedo_lapelstyle_' . $styleId . '_size_' . $sizeId .'_lapeltype_' . $lapelId .'_tuxedocolor_' . $suitTuxedoId . '_lower_view_1.png';

                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);


// remove tuxedo upper/lower left/right

                        $cmd = "rm " . $homepath . 'women_suit_images/jacket_images/lapel/tuxedo/tuxedo_lapelstyle_' . $styleId . '_size_' . $sizeId .'_lapeltype_' . $lapelId .'_tuxedocolor_' . $suitTuxedoId . '_upper_left_view_1.png';

                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);
                        
                        $cmd = "rm " . $homepath . 'women_suit_images/jacket_images/lapel/tuxedo/tuxedo_lapelstyle_' . $styleId . '_size_' . $sizeId .'_lapeltype_' . $lapelId . '_tuxedocolor_' . $suitTuxedoId .'_upper_right_view_1.png';

                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);

                        $cmd = "rm " . $homepath . 'women_suit_images/jacket_images/lapel/tuxedo/tuxedo_lapelstyle_' . $styleId . '_size_' . $sizeId .'_lapeltype_' . $lapelId . '_tuxedocolor_' . $suitTuxedoId .'_lower_left_view_1.png';

                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);

                        $cmd = "rm " . $homepath . 'women_suit_images/jacket_images/lapel/tuxedo/tuxedo_lapelstyle_' . $styleId . '_size_' . $sizeId .'_lapeltype_' . $lapelId .'_tuxedocolor_' . $suitTuxedoId . '_lower_right_view_1.png';

                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);

                    }
                }

// Suit black tuxedo end






// Top Coat
// Top Coat Styles
                    $suitcoatstylesFactory = $this->_suitcoatstylesFactory->create()->getCollection();
                    $rows_coatstyle = $suitcoatstylesFactory->getData();

                    foreach ($rows_coatstyle as $coatstyle) {

                        $topcoat_id = $coatstyle['suitcoatstyles_id'];

                        $coatstyle_glow = $homepath . '' . $coatstyle['glow_image'];
                        $coatstyle_mask = $homepath . '' . $coatstyle['mask_image'];
                        $coatstyle_highlighted = $homepath . '' . $coatstyle['hi_image'];

                        if ($coatstyle['glow_image'] != '' && $coatstyle['mask_image'] != '' && $coatstyle['hi_image'] != '') {
//mask changed
                            $cmd = 'composite -compose Dst_In  -gravity center ' . $coatstyle_mask . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'topcoat_images/styles/' . $fabric_id . '_coatstyles_' . $topcoat_id . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//crop
                            $cmd = 'convert ' . $homepath . 'topcoat_images/styles/' . $fabric_id . '_coatstyles_' . $topcoat_id . '_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'topcoat_images/styles/' . $fabric_id . '_coatstyles_' . $topcoat_id . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//glow
                            $cmd = 'convert ' . $homepath . 'topcoat_images/styles/' . $fabric_id . '_coatstyles_' . $topcoat_id . '_view_1.png ' . $coatstyle_glow . ' -geometry +0+0 -composite ' . $homepath . 'topcoat_images/styles/' . $fabric_id . '_coatstyles_' . $topcoat_id . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//highlighted
                            $cmd = 'composite ' . $coatstyle_highlighted . ' -compose Overlay  ' . $homepath . 'topcoat_images/styles/' . $fabric_id . '_coatstyles_' . $topcoat_id . '_view_1.png ' . $homepath . 'topcoat_images/styles/' . $fabric_id . '_coatstyles_' . $topcoat_id . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }


                        $coatstyle_back_glow = $homepath . '' . $coatstyle['back_glow_image'];
                        $coatstyle_back_mask = $homepath . '' . $coatstyle['back_mask_image'];
                        $coatstyle_back_hi = $homepath . '' . $coatstyle['back_hi_image'];

                        if ($coatstyle['back_glow_image'] != '' && $coatstyle['back_mask_image'] != '' && $coatstyle['back_hi_image'] != '') {
//mask changed

                            $cmd = 'composite -compose Dst_In  -gravity center ' . $coatstyle_back_mask . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'topcoat_images/styles/' . $fabric_id . '_coatstyles_' . $topcoat_id . '_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//crop
                            $cmd = 'convert ' . $homepath . 'topcoat_images/styles/' . $fabric_id . '_coatstyles_' . $topcoat_id . '_view_2.png -crop 500x1320+290+0  +repage ' . $homepath . 'topcoat_images/styles/' . $fabric_id . '_coatstyles_' . $topcoat_id . '_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//glow
                            $cmd = 'convert ' . $homepath . 'topcoat_images/styles/' . $fabric_id . '_coatstyles_' . $topcoat_id . '_view_2.png ' . $coatstyle_back_glow . ' -geometry +0+0 -composite ' . $homepath . 'topcoat_images/styles/' . $fabric_id . '_coatstyles_' . $topcoat_id . '_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//highlighted
                            $cmd = 'composite ' . $coatstyle_back_hi . ' -compose Overlay  ' . $homepath . 'topcoat_images/styles/' . $fabric_id . '_coatstyles_' . $topcoat_id . '_view_2.png ' . $homepath . 'topcoat_images/styles/' . $fabric_id . '_coatstyles_' . $topcoat_id . '_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }


                        $coatstyle_breast_glow = $homepath . '' . $coatstyle['breast_glow_image'];
                        $coatstyle_breast_mask = $homepath . '' . $coatstyle['breast_mask_image'];
                        $coatstyle_breast_highlighted = $homepath . '' . $coatstyle['breast_hi_image'];

                        if ($coatstyle['breast_glow_image'] != '' && $coatstyle['breast_mask_image'] != '' && $coatstyle['breast_hi_image'] != '') {
//mask changed
                            $cmd = 'composite -compose Dst_In  -gravity center ' . $coatstyle_breast_mask . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'topcoat_images/styles/' . $fabric_id . '_coatbreast_' . $topcoat_id . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//crop
                            $cmd = 'convert ' . $homepath . 'topcoat_images/styles/' . $fabric_id . '_coatbreast_' . $topcoat_id . '_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'topcoat_images/styles/' . $fabric_id . '_coatbreast_' . $topcoat_id . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//glow
                            $cmd = 'convert ' . $homepath . 'topcoat_images/styles/' . $fabric_id . '_coatbreast_' . $topcoat_id . '_view_1.png ' . $coatstyle_breast_glow . ' -geometry +0+0 -composite ' . $homepath . 'topcoat_images/styles/' . $fabric_id . '_coatbreast_' . $topcoat_id . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//highlighted
                            $cmd = 'composite ' . $coatstyle_breast_highlighted . ' -compose Overlay  ' . $homepath . 'topcoat_images/styles/' . $fabric_id . '_coatbreast_' . $topcoat_id . '_view_1.png ' . $homepath . 'topcoat_images/styles/' . $fabric_id . '_coatbreast_' . $topcoat_id . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }
                    }
//                    Top Coat Styles
//                    Coat Collars
                    $suitcoatcollarsFactory = $this->_suitcoatcollarsFactory->create()->getCollection();
                    $coatcollar_collection = $suitcoatcollarsFactory->getData();


                    foreach ($coatcollar_collection as $coatCollar) {
                        $coatCollarId = $coatCollar['suitcoatcollars_id'];

//view UL
                        $glow_view_UL = $coatCollar['upper_left_glow_image'] != "" ? $homepath . '' . $coatCollar['upper_left_glow_image'] : "";
                        $mask_view_UL = $coatCollar['upper_left_mask_image'] != "" ? $homepath . '' . $coatCollar['upper_left_mask_image'] : "";
                        $highlight_view_UL = $coatCollar['upper_left_hi_image'] != "" ? $homepath . '' . $coatCollar['upper_left_hi_image'] : "";

                        if ($coatCollar['upper_left_glow_image'] && $coatCollar['upper_left_mask_image'] && $coatCollar['upper_left_hi_image']) {
//mask
                            $cmd = 'composite -compose Dst_In -gravity center ' . $mask_view_UL . ' ' . $lapel_fabric_upper_left . ' -alpha Set ' . $homepath . 'topcoat_images/collars/' . $fabric_id . '_coatcollar_' . $coatCollarId . '_upper_left_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//convert
                            $cmd = 'convert ' . $homepath . 'topcoat_images/collars/' . $fabric_id . '_coatcollar_' . $coatCollarId . '_upper_left_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'topcoat_images/collars/' . $fabric_id . '_coatcollar_' . $coatCollarId . '_upper_left_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//glow
                            $cmd = 'convert ' . $homepath . 'topcoat_images/collars/' . $fabric_id . '_coatcollar_' . $coatCollarId . '_upper_left_view_1.png ' . $glow_view_UL . ' -geometry +0+0 -composite ' . $homepath . 'topcoat_images/collars/' . $fabric_id . '_coatcollar_' . $coatCollarId . '_upper_left_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//highlight
                            $cmd = 'composite ' . $highlight_view_UL . ' -compose Overlay  ' . $homepath . 'topcoat_images/collars/' . $fabric_id . '_coatcollar_' . $coatCollarId . '_upper_left_view_1.png ' . $homepath . 'topcoat_images/collars/' . $fabric_id . '_coatcollar_' . $coatCollarId . '_upper_left_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }

//view UR
                        $glow_view_UR = $homepath . '' . $coatCollar['upper_right_glow_image'];
                        $mask_view_UR = $homepath . '' . $coatCollar['upper_right_mask_image'];
                        $highlight_view_UR = $homepath . '' . $coatCollar['upper_right_hi_image'];

                        if ($coatCollar['upper_right_glow_image'] && $coatCollar['upper_right_mask_image'] && $coatCollar['upper_right_hi_image']) {
//mask
                            $cmd = 'composite -compose Dst_In -gravity center ' . $mask_view_UR . ' ' . $lapel_fabric_upper_right . ' -alpha Set ' . $homepath . 'topcoat_images/collars/' . $fabric_id . '_coatcollar_' . $coatCollarId . '_upper_right_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//convert
                            $cmd = 'convert ' . $homepath . 'topcoat_images/collars/' . $fabric_id . '_coatcollar_' . $coatCollarId . '_upper_right_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'topcoat_images/collars/' . $fabric_id . '_coatcollar_' . $coatCollarId . '_upper_right_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//glow
                            $cmd = 'convert ' . $homepath . 'topcoat_images/collars/' . $fabric_id . '_coatcollar_' . $coatCollarId . '_upper_right_view_1.png ' . $glow_view_UR . ' -geometry +0+0 -composite ' . $homepath . 'topcoat_images/collars/' . $fabric_id . '_coatcollar_' . $coatCollarId . '_upper_right_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//highlight
                            $cmd = 'composite ' . $highlight_view_UR . ' -compose Overlay  ' . $homepath . 'topcoat_images/collars/' . $fabric_id . '_coatcollar_' . $coatCollarId . '_upper_right_view_1.png ' . $homepath . 'topcoat_images/collars/' . $fabric_id . '_coatcollar_' . $coatCollarId . '_upper_right_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }

//view LL
                        $glow_view_LL = $homepath . '' . $coatCollar['lower_left_glow_image'];
                        $mask_view_LL = $homepath . '' . $coatCollar['lower_left_mask_image'];
                        $highlight_view_LL = $homepath . '' . $coatCollar['lower_left_hi_image'];

                        if ($coatCollar['lower_left_glow_image'] && $coatCollar['lower_left_mask_image'] && $coatCollar['lower_left_hi_image']) {
//mask
                            $cmd = 'composite -compose Dst_In -gravity center ' . $mask_view_LL . ' ' . $lapel_fabric_left . ' -alpha Set ' . $homepath . 'topcoat_images/collars/' . $fabric_id . '_coatcollar_' . $coatCollarId . '_lower_left_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//convert
                            $cmd = 'convert ' . $homepath . 'topcoat_images/collars/' . $fabric_id . '_coatcollar_' . $coatCollarId . '_lower_left_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'topcoat_images/collars/' . $fabric_id . '_coatcollar_' . $coatCollarId . '_lower_left_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//glow
                            $cmd = 'convert ' . $homepath . 'topcoat_images/collars/' . $fabric_id . '_coatcollar_' . $coatCollarId . '_lower_left_view_1.png ' . $glow_view_LL . ' -geometry +0+0 -composite ' . $homepath . 'topcoat_images/collars/' . $fabric_id . '_coatcollar_' . $coatCollarId . '_lower_left_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//highlight
                            $cmd = 'composite ' . $highlight_view_LL . ' -compose Overlay  ' . $homepath . 'topcoat_images/collars/' . $fabric_id . '_coatcollar_' . $coatCollarId . '_lower_left_view_1.png ' . $homepath . 'topcoat_images/collars/' . $fabric_id . '_coatcollar_' . $coatCollarId . '_lower_left_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }

//view LR
                        $glow_view_LR = $homepath . '' . $coatCollar['lower_right_glow_image'];
                        $mask_view_LR = $homepath . '' . $coatCollar['lower_right_mask_image'];
                        $highlight_view_LR = $homepath . '' . $coatCollar['lower_right_hi_image'];

                        if ($coatCollar['lower_right_glow_image'] && $coatCollar['lower_right_mask_image'] && $coatCollar['lower_right_hi_image']) {
//mask
                            $cmd = 'composite -compose Dst_In -gravity center ' . $mask_view_LR . ' ' . $lapel_fabric_right . ' -alpha Set ' . $homepath . 'topcoat_images/collars/' . $fabric_id . '_coatcollar_' . $coatCollarId . '_lower_right_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//convert
                            $cmd = 'convert ' . $homepath . 'topcoat_images/collars/' . $fabric_id . '_coatcollar_' . $coatCollarId . '_lower_right_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'topcoat_images/collars/' . $fabric_id . '_coatcollar_' . $coatCollarId . '_lower_right_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//glow
                            $cmd = 'convert ' . $homepath . 'topcoat_images/collars/' . $fabric_id . '_coatcollar_' . $coatCollarId . '_lower_right_view_1.png ' . $glow_view_LR . ' -geometry +0+0 -composite ' . $homepath . 'topcoat_images/collars/' . $fabric_id . '_coatcollar_' . $coatCollarId . '_lower_right_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//highlight
                            $cmd = 'composite ' . $highlight_view_LR . ' -compose Overlay  ' . $homepath . 'topcoat_images/collars/' . $fabric_id . '_coatcollar_' . $coatCollarId . '_lower_right_view_1.png ' . $homepath . 'topcoat_images/collars/' . $fabric_id . '_coatcollar_' . $coatCollarId . '_lower_right_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }

                        $cmd = 'convert ' . $homepath . 'topcoat_images/collars/' . $fabric_id . '_coatcollar_' . $coatCollarId . '_lower_left_view_1.png ' . $homepath . 'topcoat_images/collars/' . $fabric_id . '_coatcollar_' . $coatCollarId . '_lower_right_view_1.png -geometry +0+0 -composite ' . $homepath . 'topcoat_images/collars/' . $fabric_id . '_coatcollar_' . $coatCollarId . '_view_1.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);

                        if (file_exists($homepath . 'topcoat_images/collars/' . $fabric_id . '_coatcollar_' . $coatCollarId . '_upper_left_view_1.png')) {
                            $cmd = 'convert ' . $homepath . 'topcoat_images/collars/' . $fabric_id . '_coatcollar_' . $coatCollarId . '_upper_left_view_1.png ' . $homepath . 'topcoat_images/collars/' . $fabric_id . '_coatcollar_' . $coatCollarId . '_view_1.png -geometry +0+0 -composite ' . $homepath . 'topcoat_images/collars/' . $fabric_id . '_coatcollar_' . $coatCollarId . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }

                        if (file_exists($homepath . 'topcoat_images/collars/' . $fabric_id . '_coatcollar_' . $coatCollarId . '_lower_right_view_1.png')) {
                            $cmd = 'convert ' . $homepath . 'topcoat_images/collars/' . $fabric_id . '_coatcollar_' . $coatCollarId . '_upper_right_view_1.png ' . $homepath . 'topcoat_images/collars/' . $fabric_id . '_coatcollar_' . $coatCollarId . '_view_1.png -geometry +0+0 -composite ' . $homepath . 'topcoat_images/collars/' . $fabric_id . '_coatcollar_' . $coatCollarId . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }

//                        $cmd = "rm ".$homepath . 'topcoat_images/collars/' . $fabric_id . '_coatcollar_' . $coatCollarId . '_upper_left_view_1.png';
//                        $cmd = "rm ".$homepath . 'topcoat_images/collars/' . $fabric_id . '_coatcollar_' . $coatCollarId . '_upper_right_view_1.png';
//                        $cmd = "rm ".$homepath . 'topcoat_images/collars/' . $fabric_id . '_coatcollar_' . $coatCollarId . '_lower_left_view_1.png';
//                        $cmd = "rm ".$homepath . 'topcoat_images/collars/' . $fabric_id . '_coatcollar_' . $coatCollarId . '_lower_right_view_1.png';
                    }
// Coat Collars
// Top Coat Pockets
                    $suitcoatpocketsFactory = $this->_suitcoatpocketsFactory->create()->getCollection();
                    $rows_coatpockets = $suitcoatpocketsFactory->getData();
//                    echo "<pre>";print_r($rows_coatpockets);die;

                    foreach ($rows_coatpockets as $coatpockets) {

                        $topcoat_id = $coatpockets['suitcoatpockets_id'];

                        $coatpockets_glow = $coatpockets['left_glow_image'] != "" ? $homepath . '' . $coatpockets['left_glow_image'] : "";
                        $coatpockets_mask = $coatpockets['left_mask_image'] != "" ? $homepath . '' . $coatpockets['left_mask_image'] : "";
                        $coatpockets_highlighted = $coatpockets['left_hi_image'] != "" ? $homepath . '' . $coatpockets['left_hi_image'] : "";

                        if ($coatpockets['left_glow_image'] != '' && $coatpockets['left_mask_image'] != '' && $coatpockets['left_hi_image'] != '') {
//mask changed
                            $cmd = 'composite -compose Dst_In  -gravity center ' . $coatpockets_mask . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'topcoat_images/pockets/' . $fabric_id . '_coatpockets_' . $topcoat_id . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//crop
                            $cmd = 'convert ' . $homepath . 'topcoat_images/pockets/' . $fabric_id . '_coatpockets_' . $topcoat_id . '_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'topcoat_images/pockets/' . $fabric_id . '_coatpockets_' . $topcoat_id . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//glow
                            $cmd = 'convert ' . $homepath . 'topcoat_images/pockets/' . $fabric_id . '_coatpockets_' . $topcoat_id . '_view_1.png ' . $coatpockets_glow . ' -geometry +0+0 -composite ' . $homepath . 'topcoat_images/pockets/' . $fabric_id . '_coatpockets_' . $topcoat_id . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//highlighted
                            $cmd = 'composite ' . $coatpockets_highlighted . ' -compose Overlay  ' . $homepath . 'topcoat_images/pockets/' . $fabric_id . '_coatpockets_' . $topcoat_id . '_view_1.png ' . $homepath . 'topcoat_images/pockets/' . $fabric_id . '_coatpockets_' . $topcoat_id . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }
//
//
                        $coatpockets_back_glow = $coatpockets['right_glow_image'] != "" ? $homepath . '' . $coatpockets['right_glow_image'] : "";
                        $coatpockets_back_mask = $coatpockets['right_mask_image'] != "" ? $homepath . '' . $coatpockets['right_mask_image'] : "";
                        $coatpockets_back_hi = $coatpockets['right_hi_image'] != "" ? $homepath . '' . $coatpockets['right_hi_image'] : "";
//
                        if ($coatpockets['right_glow_image'] != '' && $coatpockets['right_mask_image'] != '' && $coatpockets['right_hi_image'] != '') {
//mask changed

                            $cmd = 'composite -compose Dst_In  -gravity center ' . $coatpockets_back_mask . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'topcoat_images/pockets/' . $fabric_id . '_coatpockets_' . $topcoat_id . '_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//crop
                            $cmd = 'convert ' . $homepath . 'topcoat_images/pockets/' . $fabric_id . '_coatpockets_' . $topcoat_id . '_view_2.png -crop 500x1320+290+0  +repage ' . $homepath . 'topcoat_images/pockets/' . $fabric_id . '_coatpockets_' . $topcoat_id . '_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//glow
                            $cmd = 'convert ' . $homepath . 'topcoat_images/pockets/' . $fabric_id . '_coatpockets_' . $topcoat_id . '_view_2.png ' . $coatpockets_back_glow . ' -geometry +0+0 -composite ' . $homepath . 'topcoat_images/pockets/' . $fabric_id . '_coatpockets_' . $topcoat_id . '_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//highlighted
                            $cmd = 'composite ' . $coatpockets_back_hi . ' -compose Overlay  ' . $homepath . 'topcoat_images/pockets/' . $fabric_id . '_coatpockets_' . $topcoat_id . '_view_2.png ' . $homepath . 'topcoat_images/pockets/' . $fabric_id . '_coatpockets_' . $topcoat_id . '_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }
//
//
                        $cmd = 'convert ' . $homepath . 'topcoat_images/pockets/' . $fabric_id . '_coatpockets_' . $topcoat_id . '_view_1.png ' . $homepath . 'topcoat_images/pockets/' . $fabric_id . '_coatpockets_' . $topcoat_id . '_view_2.png -geometry +0+0 -composite ' . $homepath . 'topcoat_images/pockets/' . $fabric_id . '_coatpockets_' . $topcoat_id . '_view_1.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);
//                        $cmd = "rm ".$homepath . 'topcoat_images/pockets/' . $fabric_id . '_coatpockets_' . $topcoat_id . '_view_2.png';
                    }

// Top Coat Pockets
// Top Coat Sleeves
                    $suitcoatsleevesFactory = $this->_suitcoatsleevesFactory->create()->getCollection();
                    $rows_coatsleeves = $suitcoatsleevesFactory->getData();

                    foreach ($rows_coatsleeves as $coatsleeves) {

                        $topcoat_id = $coatsleeves['suitcoatsleeves_id'];

                        $coatsleeves_glow = $homepath . '' . $coatsleeves['left_glow_image'];
                        $coatsleeves_mask = $homepath . '' . $coatsleeves['left_mask_image'];
                        $coatsleeves_highlighted = $homepath . '' . $coatsleeves['left_hi_image'];

                        if ($coatsleeves['left_glow_image'] != '' && $coatsleeves['left_mask_image'] != '' && $coatsleeves['left_hi_image'] != '') {
//mask changed
                            $cmd = 'composite -compose Dst_In  -gravity center ' . $coatsleeves_mask . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'topcoat_images/sleeves/' . $fabric_id . '_coatsleeves_' . $topcoat_id . '_left_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//crop
                            $cmd = 'convert ' . $homepath . 'topcoat_images/sleeves/' . $fabric_id . '_coatsleeves_' . $topcoat_id . '_left_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'topcoat_images/sleeves/' . $fabric_id . '_coatsleeves_' . $topcoat_id . '_left_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//glow
                            $cmd = 'convert ' . $homepath . 'topcoat_images/sleeves/' . $fabric_id . '_coatsleeves_' . $topcoat_id . '_left_view_1.png ' . $coatsleeves_glow . ' -geometry +0+0 -composite ' . $homepath . 'topcoat_images/sleeves/' . $fabric_id . '_coatsleeves_' . $topcoat_id . '_left_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//highlighted
                            $cmd = 'composite ' . $coatsleeves_highlighted . ' -compose Overlay  ' . $homepath . 'topcoat_images/sleeves/' . $fabric_id . '_coatsleeves_' . $topcoat_id . '_left_view_1.png ' . $homepath . 'topcoat_images/sleeves/' . $fabric_id . '_coatsleeves_' . $topcoat_id . '_left_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }


                        $coatsleeves_back_glow = $homepath . '' . $coatsleeves['right_glow_image'];
                        $coatsleeves_back_mask = $homepath . '' . $coatsleeves['right_mask_image'];
                        $coatsleeves_back_hi = $homepath . '' . $coatsleeves['right_hi_image'];

                        if ($coatsleeves['right_glow_image'] != '' && $coatsleeves['right_mask_image'] != '' && $coatsleeves['right_hi_image'] != '') {
//mask changed

                            $cmd = 'composite -compose Dst_In  -gravity center ' . $coatsleeves_back_mask . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'topcoat_images/sleeves/' . $fabric_id . '_coatsleeves_' . $topcoat_id . '_right_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//crop
                            $cmd = 'convert ' . $homepath . 'topcoat_images/sleeves/' . $fabric_id . '_coatsleeves_' . $topcoat_id . '_right_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'topcoat_images/sleeves/' . $fabric_id . '_coatsleeves_' . $topcoat_id . '_right_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//glow
                            $cmd = 'convert ' . $homepath . 'topcoat_images/sleeves/' . $fabric_id . '_coatsleeves_' . $topcoat_id . '_right_view_1.png ' . $coatsleeves_back_glow . ' -geometry +0+0 -composite ' . $homepath . 'topcoat_images/sleeves/' . $fabric_id . '_coatsleeves_' . $topcoat_id . '_right_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//highlighted
                            $cmd = 'composite ' . $coatsleeves_back_hi . ' -compose Overlay  ' . $homepath . 'topcoat_images/sleeves/' . $fabric_id . '_coatsleeves_' . $topcoat_id . '_right_view_1.png ' . $homepath . 'topcoat_images/sleeves/' . $fabric_id . '_coatsleeves_' . $topcoat_id . '_right_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }


                        $cmd = 'convert ' . $homepath . 'topcoat_images/sleeves/' . $fabric_id . '_coatsleeves_' . $topcoat_id . '_left_view_1.png ' . $homepath . 'topcoat_images/sleeves/' . $fabric_id . '_coatsleeves_' . $topcoat_id . '_right_view_1.png -geometry +0+0 -composite ' . $homepath . 'topcoat_images/sleeves/' . $fabric_id . '_coatsleeves_' . $topcoat_id . '_view_1.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);

                        $cmd = "rm " . $homepath . 'topcoat_images/sleeves/' . $fabric_id . '_coatsleeves_' . $topcoat_id . '_left_view_1.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);

                        $cmd = "rm " . $homepath . 'topcoat_images/sleeves/' . $fabric_id . '_coatsleeves_' . $topcoat_id . '_right_view_1.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);

                        $coatsleeves_glow = $homepath . '' . $coatsleeves['back_left_glow_image'];
                        $coatsleeves_mask = $homepath . '' . $coatsleeves['back_left_mask_image'];
                        $coatsleeves_highlighted = $homepath . '' . $coatsleeves['back_left_hi_image'];

                        if ($coatsleeves['back_left_glow_image'] != '' && $coatsleeves['back_left_mask_image'] != '' && $coatsleeves['back_left_hi_image'] != '') {
//mask changed
                            $cmd = 'composite -compose Dst_In  -gravity center ' . $coatsleeves_mask . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'topcoat_images/sleeves/' . $fabric_id . '_coatsleeves_' . $topcoat_id . '_left_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//crop
                            $cmd = 'convert ' . $homepath . 'topcoat_images/sleeves/' . $fabric_id . '_coatsleeves_' . $topcoat_id . '_left_view_2.png -crop 500x1320+290+0  +repage ' . $homepath . 'topcoat_images/sleeves/' . $fabric_id . '_coatsleeves_' . $topcoat_id . '_left_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//glow
                            $cmd = 'convert ' . $homepath . 'topcoat_images/sleeves/' . $fabric_id . '_coatsleeves_' . $topcoat_id . '_left_view_2.png ' . $coatsleeves_glow . ' -geometry +0+0 -composite ' . $homepath . 'topcoat_images/sleeves/' . $fabric_id . '_coatsleeves_' . $topcoat_id . '_left_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//highlighted
                            $cmd = 'composite ' . $coatsleeves_highlighted . ' -compose Overlay  ' . $homepath . 'topcoat_images/sleeves/' . $fabric_id . '_coatsleeves_' . $topcoat_id . '_left_view_2.png ' . $homepath . 'topcoat_images/sleeves/' . $fabric_id . '_coatsleeves_' . $topcoat_id . '_left_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }


                        $coatsleeves_back_glow = $homepath . '' . $coatsleeves['back_right_glow_image'];
                        $coatsleeves_back_mask = $homepath . '' . $coatsleeves['back_right_mask_image'];
                        $coatsleeves_back_hi = $homepath . '' . $coatsleeves['back_right_hi_image'];

                        if ($coatsleeves['back_right_glow_image'] != '' && $coatsleeves['back_right_mask_image'] != '' && $coatsleeves['back_right_hi_image'] != '') {
//mask changed

                            $cmd = 'composite -compose Dst_In  -gravity center ' . $coatsleeves_back_mask . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'topcoat_images/sleeves/' . $fabric_id . '_coatsleeves_' . $topcoat_id . '_right_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//crop
                            $cmd = 'convert ' . $homepath . 'topcoat_images/sleeves/' . $fabric_id . '_coatsleeves_' . $topcoat_id . '_right_view_2.png -crop 500x1320+290+0  +repage ' . $homepath . 'topcoat_images/sleeves/' . $fabric_id . '_coatsleeves_' . $topcoat_id . '_right_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//glow
                            $cmd = 'convert ' . $homepath . 'topcoat_images/sleeves/' . $fabric_id . '_coatsleeves_' . $topcoat_id . '_right_view_2.png ' . $coatsleeves_back_glow . ' -geometry +0+0 -composite ' . $homepath . 'topcoat_images/sleeves/' . $fabric_id . '_coatsleeves_' . $topcoat_id . '_right_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//highlighted
                            $cmd = 'composite ' . $coatsleeves_back_hi . ' -compose Overlay  ' . $homepath . 'topcoat_images/sleeves/' . $fabric_id . '_coatsleeves_' . $topcoat_id . '_right_view_2.png ' . $homepath . 'topcoat_images/sleeves/' . $fabric_id . '_coatsleeves_' . $topcoat_id . '_right_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }

                        $cmd = 'convert ' . $homepath . 'topcoat_images/sleeves/' . $fabric_id . '_coatsleeves_' . $topcoat_id . '_left_view_2.png ' . $homepath . 'topcoat_images/sleeves/' . $fabric_id . '_coatsleeves_' . $topcoat_id . '_right_view_2.png -geometry +0+0 -composite ' . $homepath . 'topcoat_images/sleeves/' . $fabric_id . '_coatsleeves_' . $topcoat_id . '_view_2.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);

                        $cmd = "rm " . $homepath . 'topcoat_images/sleeves/' . $fabric_id . '_coatsleeves_' . $topcoat_id . '_left_view_2.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);

                        $cmd = "rm " . $homepath . 'topcoat_images/sleeves/' . $fabric_id . '_coatsleeves_' . $topcoat_id . '_right_view_2.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);
                    }

//////////////////// Top Coat Sleeves  ////////////////////////////////
/////////////////////// backcollar  ////////////////////////////////

                    $coatbackcollar_glow = $homepath . 'woman_glow_mask/suitcoatbackcollar/back/CommonBackCollar_shad.png';
                    $coatbackcollar_mask = $homepath . 'woman_glow_mask/suitcoatbackcollar/back/CommonBackCollar_mask.png';
                    $coatbackcollar_highlighted = $homepath . 'woman_glow_mask/suitcoatbackcollar/back/CommonBackCollar_hi.png';

                    if ($coatbackcollar_glow != '' && $coatbackcollar_mask != '' && $coatbackcollar_highlighted != '') {
//mask changed
                        $cmd = 'composite -compose Dst_In  -gravity center ' . $coatbackcollar_mask . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'topcoat_images/collars/' . $fabric_id . '_coatbackcollar_view_2.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);
//crop
                        $cmd = 'convert ' . $homepath . 'topcoat_images/collars/' . $fabric_id . '_coatbackcollar_view_2.png -crop 500x1320+290+0  +repage ' . $homepath . 'topcoat_images/collars/' . $fabric_id . '_coatbackcollar_view_2.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);
//glow
                        $cmd = 'convert ' . $homepath . 'topcoat_images/collars/' . $fabric_id . '_coatbackcollar_view_2.png ' . $coatbackcollar_glow . ' -geometry +0+0 -composite ' . $homepath . 'topcoat_images/collars/' . $fabric_id . '_coatbackcollar_view_2.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);
//highlighted
                        $cmd = 'composite ' . $coatbackcollar_highlighted . ' -compose Overlay  ' . $homepath . 'topcoat_images/collars/' . $fabric_id . '_coatbackcollar_view_2.png ' . $homepath . 'topcoat_images/collars/' . $fabric_id . '_coatbackcollar_view_2.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);
                    }


                    $coatbackcollar_glow = $homepath . 'woman_glow_mask/suitcoatbackcollar/folded/Common_back_Collar_shad.png';
                    $coatbackcollar_mask = $homepath . 'woman_glow_mask/suitcoatbackcollar/folded/Common_back_Collar_mask.png';
                    $coatbackcollar_highlighted = $homepath . 'woman_glow_mask/suitcoatbackcollar/folded/Common_back_Collar_hi.png';

                    if ($coatbackcollar_glow != '' && $coatbackcollar_mask != '' && $coatbackcollar_highlighted != '') {
//mask changed
                        $cmd = 'composite -compose Dst_In  -gravity center ' . $coatbackcollar_mask . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'topcoat_images/collars/' . $fabric_id . '_coatbackcollar_view_3.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);
//crop
                        $cmd = 'convert ' . $homepath . 'topcoat_images/collars/' . $fabric_id . '_coatbackcollar_view_3.png -crop 500x1320+290+0  +repage ' . $homepath . 'topcoat_images/collars/' . $fabric_id . '_coatbackcollar_view_3.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);
//glow
                        $cmd = 'convert ' . $homepath . 'topcoat_images/collars/' . $fabric_id . '_coatbackcollar_view_3.png ' . $coatbackcollar_glow . ' -geometry +0+0 -composite ' . $homepath . 'topcoat_images/collars/' . $fabric_id . '_coatbackcollar_view_3.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);
//highlighted
                        $cmd = 'composite ' . $coatbackcollar_highlighted . ' -compose Overlay  ' . $homepath . 'topcoat_images/collars/' . $fabric_id . '_coatbackcollar_view_3.png ' . $homepath . 'topcoat_images/collars/' . $fabric_id . '_coatbackcollar_view_3.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);
                    }

/////////////////////// backcollar  ////////////////////////////////
///////////////////// Innerlining ////////////////////////

                    $coatlining_glow = $homepath . 'woman_glow_mask/suitcoatlining/Common_Inner_Lining_shad.png';
                    $coatlining_mask = $homepath . 'woman_glow_mask/suitcoatlining/Common_Inner_Lining_mask.png';
                    $coatlining_highlighted = $homepath . 'woman_glow_mask/suitcoatlining/Common_Inner_Lining_hi.png';

                    if ($coatlining_glow != '' && $coatlining_mask != '' && $coatlining_highlighted != '') {
//mask changed
                        $cmd = 'composite -compose Dst_In  -gravity center ' . $coatlining_mask . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'topcoat_images/lining/' . $fabric_id . '_coatlining_view_3.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);
//crop
                        $cmd = 'convert ' . $homepath . 'topcoat_images/lining/' . $fabric_id . '_coatlining_view_3.png -crop 500x1320+290+0  +repage ' . $homepath . 'topcoat_images/lining/' . $fabric_id . '_coatlining_view_3.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);
//glow
                        $cmd = 'convert ' . $homepath . 'topcoat_images/lining/' . $fabric_id . '_coatlining_view_3.png ' . $coatlining_glow . ' -geometry +0+0 -composite ' . $homepath . 'topcoat_images/lining/' . $fabric_id . '_coatlining_view_3.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);
//highlighted
                        $cmd = 'composite ' . $coatlining_highlighted . ' -compose Overlay  ' . $homepath . 'topcoat_images/lining/' . $fabric_id . '_coatlining_view_3.png ' . $homepath . 'topcoat_images/lining/' . $fabric_id . '_coatlining_view_3.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);
                    }

///////////////// Inner lining ///////////////////////
// Top Coat
                    $cmd = "rm " . $homepath . 'women_fab_suit_images/' . $fabric_id . "_jacket_fabric_cuff_side.png";
                    $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($writeQuery);

                    $cmd = "rm " . $homepath . 'women_fab_suit_images/' . $fabric_id . "_jacket_fabric_main.png";
                    $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($writeQuery);

                    $cmd = "rm " . $homepath . 'women_fab_suit_images/' . $fabric_id . "_jacket_fabric_main_60.png";
                    $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($writeQuery);

                    $cmd = "rm " . $homepath . 'women_fab_suit_images/' . $fabric_id . "_jacket_fabric_main_90.png";
                    $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($writeQuery);

                    $cmd = "rm " . $homepath . 'women_fab_suit_images/' . $fabric_id . "_jacket_fabric_main_back.png";
                    $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($writeQuery);

                    $cmd = "rm " . $homepath . 'women_fab_suit_images/' . $fabric_id . "_jacket_fabric_main_back_zoom.png";
                    $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($writeQuery);

                    $cmd = "rm " . $homepath . 'women_fab_suit_images/' . $fabric_id . "_jacket_fabric_wave_left.png";
                    $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($writeQuery);

                    $cmd = "rm " . $homepath . 'women_fab_suit_images/' . $fabric_id . "_jacket_fabric_wave_right.png";
                    $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($writeQuery);

                    $cmd = "rm " . $homepath . 'women_fab_suit_images/' . $fabric_id . "_jacket_fabric_zoom.png";
                    $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($writeQuery);

                    $cmd = "rm " . $homepath . 'women_fab_suit_images/' . $fabric_id . "_jacket_lapel_fabric_left.png";
                    $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($writeQuery);

                    $cmd = "rm " . $homepath . 'women_fab_suit_images/' . $fabric_id . "_jacket_lapel_fabric_right.png";
                    $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($writeQuery);

                    $cmd = "rm " . $homepath . 'women_fab_suit_images/' . $fabric_id . "_jacket_lapel_fabric_upper_left.png";
                    $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($writeQuery);

                    $cmd = "rm " . $homepath . 'women_fab_suit_images/' . $fabric_id . "_jacket_lapel_fabric_upper_right.png";
                    $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($writeQuery);

                    $updateQuery = "UPDATE `womensuit_fabrics` SET `status`=1 WHERE `id`=" . $fabric_id;
                    $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$updateQuery}')";
                    $this->connection->query($writeQuery);
                }
                /* Suit Image Generation End */

                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['id' => $model->getId()]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the suit_womenjacket.'));
            }
            $this->dataPersistor->set('suit_womenjacket', $data);
            return $resultRedirect->setPath('*/*/edit', ['id' => $this->getRequest()->getParam('id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }

}
