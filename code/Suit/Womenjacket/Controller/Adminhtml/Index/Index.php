<?php

/**
 * @author Dhirajkumar Deore
 * Copyright © 2018 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Suit\Womenjacket\Controller\Adminhtml\Index;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Index extends \Magento\Backend\App\Action {

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
    Context $context, PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * Check the permission to run it
     *
     * @return bool
     */
    protected function _isAllowed() {
        return $this->_authorization->isAllowed('Suit_Womenjacket::suit_womenjacket');
    }

    /**
     * Index action
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute() {

        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Suit_Womenjacket::suit_womenjacket');
        $resultPage->addBreadcrumb(__('Womenjacket'), __('Womenjacket'));
        $resultPage->addBreadcrumb(__('Suit'), __('Manage Suit Fabrics'));
        $resultPage->getConfig()->getTitle()->prepend(__('Manage Suit Fabrics'));

        return $resultPage;
    }

}
