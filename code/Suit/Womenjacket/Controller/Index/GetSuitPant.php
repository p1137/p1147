<?php

namespace Suit\Womenjacket\Controller\Index;

class GetSuitPant extends \Magento\Framework\App\Action\Action {

    protected $_pageFactory;
    protected $_resultJsonFactory;

    public function __construct(
    \Suit\Pant\Model\PantcuffFactory $suitPantCuff, 
    \Suit\Womenpant\Model\WomenpantsidepocketFactory $suitPantSidePocket,
    \Suit\Womenpant\Model\WomenpantbackpocketFactory $suitPantBackPocket, 
    \Suit\Pant\Model\PantfasteningFactory $suitPantFastening, 
    \Suit\Pant\Model\PantpleatsFactory $suitPantPleats, 
    \Suit\Pant\Model\PantfitFactory $suitPantFit, 
    \Suit\Pant\Model\WaistbandstyleFactory $suitPantWaistbandstyle, 
    \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory, 
    \Magento\Framework\App\Action\Context $context, 
    \Magento\Framework\View\Result\PageFactory $pageFactory
    ) {
        $this->_pageFactory = $pageFactory;
        $this->_resultJsonFactory = $resultJsonFactory;
        $this->_suitPantFit = $suitPantFit;
        $this->_suitPantPleats = $suitPantPleats;
        $this->_suitPantFastening = $suitPantFastening;
        $this->_suitPantSidePocket = $suitPantSidePocket;
        $this->_suitPantBackPocket = $suitPantBackPocket;
        $this->_suitPantWaistbandstyle = $suitPantWaistbandstyle;
        $this->_suitPantCuff = $suitPantCuff;
        return parent::__construct($context);
    }

    public function execute() {
        $storeManager = \Magento\Framework\App\ObjectManager::getInstance()->get('\Magento\Store\Model\StoreManagerInterface');
        $basePath = $storeManager->getStore()->getBaseUrl();

        $fit_collection = $this->_suitPantFit->create()->getCollection();
        $fit_collection->addFieldToFilter('status', 1);
        $fit_collection = $fit_collection->getData();
        $fits = array();
        foreach ($fit_collection as $fit) {
            $id = $fit['pantfit_id'];
            $name = $fit['title'];
            $class = $fit['class'];
            $thumb = $fit['thumb'];
            $price = $fit['price'];
            $status = $fit['status'];

            if ($status) {
                $fits[] = array('id' => $id, 'parent' => 'pantfit', 'price' => $price, 'class' => $class, "designType" => "pant", 'name' => $name);
            }
        }

        $fits_data = array('id' => 1, 'name' => 'Fit', "designType" => "pant", 'class' => "icon-v2-Pant_Fit_Main_Icon", "style" => $fits);


        $pleates_collection = $this->_suitPantPleats->create()->getCollection();
        $pleates_collection->addFieldToFilter('status', 1);
        $pleates_collection = $pleates_collection->getData();
        $pleates = array();

        foreach ($pleates_collection as $pleate) {
            $id = $pleate['pantpleats_id'];
            $name = $pleate['title'];
            $class = $pleate['class'];
            $price = $pleate['price'];
            $status = $pleate['status'];

            if ($status) {
                $pleates[] = array('id' => $id, 'parent' => 'pantpleats', 'price' => $price, 'class' => $class, "designType" => "pant", 'name' => $name);
            }
        }

        $pleates_data = array('id' => 2, 'name' => 'Pleats', "designType" => "pant", 'class' => "icon-v2-Pant_Pleats_Main_Icon", "style" => $pleates);

        $fastening_collection = $this->_suitPantFastening->create()->getCollection();
        $fastening_collection->addFieldToFilter('status', 1);
        $fastening_collection = $fastening_collection->getData();
        $fastenings = array();
        foreach ($fastening_collection as $fastening) {
            $id = $fastening['pantfastening_id'];
            $name = $fastening['title'];
            $class = $fastening['class'];
            $thumb = $fastening['thumb'];
            $price = $fastening['price'];
            $main_img = $fastening['glow_image'];
            $status = $fastening['status'];

            if ($status) {
                $fastenings[] = array('id' => $id, 'parent' => 'fastening', 'price' => $price, 'class' => $class, "designType" => "pant", 'name' => $name, "main_img" => $basePath . 'media/' . $main_img);
            }
        }

        $fastening_data = array('id' => 3, 'name' => 'Fastening', "designType" => "pant", 'class' => "icon-v2-Pant_Fastening_Main_Icon", "style" => $fastenings);


        $pocket_collection = $this->_suitPantSidePocket->create()->getCollection();
        $pocket_collection->addFieldToFilter('status', 1);
        $pocket_collection = $pocket_collection->getData();
        $pockets = array();
        foreach ($pocket_collection as $pocket) {
            $id = $pocket['pantsidepocket_id'];
            $name = $pocket['title'];
            $class = $pocket['class'];
            $price = $pocket['price'];
            $status = $pocket['status'];

            if ($status) {
                $pockets[] = array('id' => $id, 'parent' => 'pantpockets', 'price' => $price, 'class' => $class, "designType" => "pant", 'name' => $name);
            }
        }

        $pockets_data = array('id' => 4, 'name' => 'Pockets', "designType" => "pant", 'class' => "icon-v2-Pant_Pockets_Main_Icon", "style" => $pockets);


        $backpocket_collection = $this->_suitPantBackPocket->create()->getCollection();
        $backpocket_collection->addFieldToFilter('status', 1);
        $backpocket_collection = $backpocket_collection->getData();
        $backpockets = array();
        foreach ($backpocket_collection as $backpocket) {
            $id = $backpocket['pantbackpocket_id'];
            $name = $backpocket['title'];
            $class = $backpocket['class'];
            $price = $backpocket['price'];
            $status = $backpocket['status'];

            if ($status) {
                $backpockets[] = array('id' => $id, 'parent' => 'pantbackpockets', 'price' => $price, 'class' => $class, "designType" => "pant", 'name' => $name);
            }
        }

        $backpockets_data = array('id' => 5, 'name' => 'Back Pockets', "designType" => "pant", 'class' => "icon-v2-Pant_Double_Welted_Pocket_With_Button_x2", "style" => $backpockets);




        $cuff_collection = $this->_suitPantCuff->create()->getCollection();
        $cuff_collection->addFieldToFilter('status', 1);
        $cuff_collection = $cuff_collection->getData();
        $cuffs = array();
        foreach ($cuff_collection as $cuff) {
            $id = $cuff['pantcuff_id'];
            $name = $cuff['title'];
            $class = $cuff['class'];
            $price = $cuff['price'];
            $thumb = $cuff['thumb'];
            $status = $cuff['status'];

            if ($status) {
                $cuffs[] = array('id' => $id, 'parent' => 'pantcuff', 'price' => $price, 'class' => $class, "designType" => "pant", 'name' => $name);
            }
        }

        $cuffs_data = array('id' => 6, 'name' => 'Cuffs', "designType" => "pant", 'class' => "icon-v2-With_Pant_Cuffs", "style" => $cuffs);


        // $waistbandstyle_collection = $this->_suitPantWaistbandstyle->create()->getCollection();
        // $waistbandstyle_collection->addFieldToFilter('status', 1);
        // $waistbandstyle_collection = $waistbandstyle_collection->getData();
        // $waistbands = array();
        // foreach ($waistbandstyle_collection as $waistbandstyle) {
        //     $id = $waistbandstyle['waistband_id'];
        //     $name = $waistbandstyle['title'];
        //     $class = $waistbandstyle['class'];
        //     $price = $waistbandstyle['price'];
        //     $status = $waistbandstyle['status'];

        //     if ($status) {
        //         $waistbands[] = array('id' => $id, 'parent' => 'waistband', 'price' => $price, 'class' => $class, "designType" => "pant", 'name' => $name);
        //     }
        // }

        // $waistband_data = array('id' => 7, 'name' => 'Waistband', "designType" => "pant", 'class' => "icon-v2-side_adjuster_loop-01", "style" => $waistbands);



        $design_data = array($fits_data, $pleates_data, $fastening_data, $pockets_data, $backpockets_data, $cuffs_data);
        return $this->_resultJsonFactory->create()->setData($design_data);
    }

}
