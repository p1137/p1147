<?php

namespace Suit\Womenjacket\Controller\Index;

class GetSuitDesign extends \Magento\Framework\App\Action\Action {

    protected $_pageFactory;
    protected $_resultJsonFactory;

    public function __construct(
    \Suit\Womenjacket\Model\SuitButtonFactory $suitButton, \Suit\Womenjacket\Model\SuitVentFactory $suitVent, \Suit\Womenjacket\Model\SuitSleeveFactory $suitSleeve, \Suit\Womenjacket\Model\SuitpocketsFactory $suitPocket, \Suit\Womenjacket\Model\SuitLapelSizeFactory $suitLapelSize, \Suit\Womenjacket\Model\SuitLapelTypeFactory $suitLapelType,\Suit\Womenjacket\Model\SuitTuxedoTypeFactory $suitTuxedoType,\Suit\Womenjacket\Model\SuitTuxedoColorFactory $suitTuxedoColor, \Suit\Womenjacket\Model\SuitstyleFactory $suitStyle, \Suit\Womenjacket\Model\SuitFitFactory $suitFit, \Magento\Framework\App\Action\Context $context, \Magento\Framework\View\Result\PageFactory $pageFactory, \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory) {
        $this->_pageFactory = $pageFactory;
        $this->_resultJsonFactory = $resultJsonFactory;
        $this->_suitFit = $suitFit;
        $this->_suitStyle = $suitStyle;
        $this->_suitLapelType = $suitLapelType;
        $this->_suitLapelSize = $suitLapelSize;
        $this->_suitTuxedoType = $suitTuxedoType;
        $this->_suitTuxedoColor = $suitTuxedoColor;
        $this->_suitPocket = $suitPocket;
        $this->_suitSleeve = $suitSleeve;
        $this->_suitVent = $suitVent;
        $this->_suitButton = $suitButton;
        return parent::__construct($context);
    }

    public function execute() {
        $storeManager = \Magento\Framework\App\ObjectManager::getInstance()->get('\Magento\Store\Model\StoreManagerInterface');
        $basePath = $storeManager->getStore()->getBaseUrl();
        $mediaPath = $storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        $fit_collection = $this->_suitFit->create()->getCollection();
        $fit_collection->addFieldToFilter('status', 1);
        $fit_collection = $fit_collection->getData();
        $fits = array();
        foreach ($fit_collection as $fit) {
            $id = $fit['suitfit_id'];
            $name = $fit['title'];
            $class = $fit['class'];
            $status = $fit['status'];
            $thumb = $fit['thumb'];
            $price = $fit['price'];

            if ($status) {
                $fits[] = array('id' => $id, 'class' => $class, 'parent' => 'Fit', "designType" => "jacket", 'name' => $name, "price" => $price);
            }
        }

        $fit_data = array('id' => 1, 'name' => 'Fit', "designType" => "jacket", 'class' => "icon-v2-Fit_Main_Icon", "style" => $fits);


        $style_collection = $this->_suitStyle->create()->getCollection();
        $style_collection->addFieldToFilter('status', 1);
        $style_collection = $style_collection->getData();
        $styles = array();

        foreach ($style_collection as $style) {
            $id = $style['suitstyle_id'];
            $name = $style['title'];
            $class = $style['class'];
//            $type = $style['type'];
            $status = $style['status'];
            $thumb = $style['thumb'];
            $price = $style['price'];

            if ($status) {
                $styles[] = array('id' => $id, 'class' => $class, 'parent' => 'Style', "designType" => "jacket", 'name' => $name, "price" => $price, 'type' => $id);
            }
        }

        $style_data = array('id' => 2, "designType" => "jacket", 'name' => 'Style', 'class' => "icon-v2-Main_Style_Icon", "style" => $styles);



        $lapels = array();

        $lapel_type = array('Notch', 'Peak', 'Shawl');

        $lapel_type_class = array('icon-notch', 'icon-peak', 'icon-shwal');

        $lapel_type_thumb = array('Lapel_Notch.png', 'Lapel_Peak.png', 'Lapel_Shawl.png');

        $lapel_size = array('Slim', 'Regular', 'Wide');

        $lapel_size_class = array('icon-slim-lapel', 'icon-standard', 'icon-wide');

        $lapel_size_thumb = array('Width_Slim.png', 'Width_Standard.png', 'Width_Wide.png');

        $lapeltype_collection = $this->_suitLapelType->create()->getCollection();
        $lapeltype_collection->addFieldToFilter('status', 1);
        $lapeltype_collection = $lapeltype_collection->getData();
        $i = 0;
        foreach ($lapeltype_collection as $ltc) {
            $lapels[] = array(
                'id' => $i + 1,
                'parent' => 'Lapel',
                'class' => $ltc['class'],
                "designType" => "jacket",
                'name' => $ltc['title'],
                'price' => $ltc['price']
            );
            $i++;
        }

        $lapelsize_collection = $this->_suitLapelSize->create()->getCollection();
        $lapelsize_collection->addFieldToFilter('status', 1);
        $lapelsize_collection = $lapelsize_collection->getData();
        $j = 0;
        foreach ($lapelsize_collection as $lsc) {
            $lapel_size1[] = array(
                'id' => $j + 1,
                'parent' => 'Lapel',
                'class' => $lsc['class'],
                "designType" => "jacket",
                'name' => $lsc['title'],
                'price' => $lsc['price']);
            $j++;
        }



        $lapels_data = array('id' => 3, 'name' => 'Lapel Style', "designType" => "jacket", 'class' => "icon-v2-Lapel_Main_Icon", "style" => $lapels, "size" => $lapel_size1);

        $pocket_collection = $this->_suitPocket->create()->getCollection();
        $pocket_collection->addFieldToFilter('status', 1);
        $pocket_collection = $pocket_collection->getData();
        $pockets = array();
        foreach ($pocket_collection as $pocket) {
            $id = $pocket['suitpocket_id'];
            $name = $pocket['title'];
            $class = $pocket['class'];
//            $type = $pocket['type'];
            $thumb = $pocket['thumb'];
            $price = $pocket['price'];
            $status = $pocket['status'];

            if ($status) {
                $pockets[] = array('id' => $id, 'class' => $class, 'name' => $name, 'price' => $price, 'parent' => 'Pockets', "designType" => "jacket");
            }
        }

        $pockets_data = array('id' => 4, 'name' => 'Pockets', "designType" => "jacket", 'class' => "icon-v2-Pockets_Main_Icon", "style" => $pockets);


        $sleeve_collection = $this->_suitSleeve->create()->getCollection();
        $sleeve_collection->addFieldToFilter('status', 1);
        $sleeve_collection = $sleeve_collection->getData();
        $j = 0;
        $sleeve_btns = array();
        foreach ($sleeve_collection as $sleeve) {
            $id = $sleeve['suitsleeve_id'];
            $name = $sleeve['title'];
            $class = $sleeve['class'];
            $thumb = $sleeve['thumb'];
            $btn_count = $sleeve['btn_count'];
            $status = $sleeve['status'];
            $price = $sleeve['price'];

            if ($status) {
                $sleeve_btns[] = array('id' => $id, 'class' => $class, 'name' => $name, 'parent' => 'sleeve_buttons', "designType" => "jacket", 'btn_count' => $btn_count, 'price' => $price);
            }
        }

        $sleeve_btn_data = array('id' => 5, 'name' => 'sleeve buttons', "designType" => "jacket", 'class' => "icon-v2-Sleeve_Main_Icon", "style" => $sleeve_btns);

        $vent_collection = $this->_suitVent->create()->getCollection();
        $vent_collection->addFieldToFilter('status', 1);
        $vent_collection = $vent_collection->getData();
        $vents = array();
        foreach ($vent_collection as $vent) {
            $id = $vent['suitvent_id'];
            $name = $vent['title'];
            $class = $vent['class'];
            $thumb = $vent['thumb'];
            $price = $vent['price'];
            $main_image = $mediaPath . $vent['main_image'];
            if (!$vent['main_image']) {
                $main_image = $mediaPath . 'suit-womenjacket/blank.png';
            }
            $status = $vent['status'];

            if ($status) {
                $vents[] = array('id' => $id, 'name' => $name, 'price' => $price, 'class' => $class, 'parent' => 'Vents', "designType" => "jacket", "main_img" => $main_image,);
            }
        }

        $vents_data = array('id' => 6, 'name' => 'Vents', "designType" => "jacket", 'class' => "icon-v2-Center_Vents", "style" => $vents);

        $buttons_collection = $this->_suitButton->create()->getCollection();
        $buttons_collection->addFieldToFilter('status', 1);
        $buttons_collection = $buttons_collection->getData();
        $buttons = array();
        foreach ($buttons_collection as $button) {
            $id = $button['suitbutton_id'];
            $type = $button['style_type'];
            $class = $button['class'];
            $name = $button['btn_count'] . ' Button';
            $thumb = $button['thumb'];
            $price = $button['price'];
            $btn_count = $button['btn_count'];
            $status = $button['status'];

            if ($status) {
                $buttons[] = array('id' => $id, 'style_id' => $type, 'price' => $price, 'class' => $class, 'name' => $name, 'parent' => 'buttons', "designType" => "jacket", 'btn_count' => $btn_count,);
            }
        }
        $buttons_data = array('id' => 7, 'name' => 'buttons', "designType" => "jacket", 'class' => "icon-v2-Single_Breasted_2_Button", "style" => $buttons);


        $tuxedo_type = array();
        $tuxedotype_collection = $this->_suitTuxedoType->create()->getCollection();
        $tuxedotype_collection->addFieldToFilter('status', 1);
        $tuxedotype_collection = $tuxedotype_collection->getData();
        $i = 0;
        foreach ($tuxedotype_collection as $ttc) {
            $tuxedo_type[] = array(
                'id' => $i + 1,
                'parent' => 'TuxedoStyle',
                'class' => $ttc['class'],
                "designType" => "jacket",
                'name' => $ttc['title'],
                'price' => $ttc['price']
            );
            $i++;
        }

        $tuxedo_color = array();
        $tuxedocolor_collection = $this->_suitTuxedoColor->create()->getCollection();
        $tuxedocolor_collection->addFieldToFilter('status', 1);
        $tuxedocolor_collection = $tuxedocolor_collection->getData();
        $j = 0;
        foreach ($tuxedocolor_collection as $tcc) {
            $tuxedo_color[] = array(
                'id' => $j + 1,
                'parent' => 'Tuxedo',
                'class' => $tcc['class'],
                "designType" => "jacket",
                'name' => $tcc['title'],
                'price' => $tcc['price']);
            $j++;
        }



        $tuxedo_data = array('id' => 8, 'name' => 'Tuxedo', "designType" => "jacket", 'class' => "icon-v2-Lapel_Main_Icon", "style" => $tuxedo_type, "color" => $tuxedo_color);


        
        $design_data = array($fit_data, $style_data, $lapels_data, $pockets_data, $sleeve_btn_data, $vents_data, $buttons_data, $tuxedo_data);        
        return $this->_resultJsonFactory->create()->setData($design_data);
    }

}
