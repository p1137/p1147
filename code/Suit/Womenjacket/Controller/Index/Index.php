<?php

namespace Suit\Womenjacket\Controller\Index;

class Index extends \Magento\Framework\App\Action\Action {

    protected $_pageFactory;
    private $quoteItemFactory;

    public function __construct(
    \Magento\Framework\App\Action\Context $context, 
    \Magento\Quote\Model\Quote\ItemFactory $quoteItemFactory, 
    \Magento\Framework\View\Result\PageFactory $pageFactory,
    \Magento\Wishlist\Model\ItemFactory $itemFactory
    ) {
        $this->_pageFactory = $pageFactory;
        $this->quoteItemFactory = $quoteItemFactory;
        $this->itemFactory = $itemFactory;
        return parent::__construct($context);
    }

    public function execute() {
        $quote_item_id = $this->getRequest()->getParam('item_id', false);
        $wishlist_item_id = $this->getRequest()->getParam('wishlist_item_id', false);
        $data = $this->getRequest()->getPostValue();

        // after editing in cart
        if ($quote_item_id) {
            $quoteItem = $this->quoteItemFactory->create();
            $collection = $quoteItem->getCollection()->addFieldToFilter('item_id', array('eq' => $quote_item_id));
            $item = $collection->getFirstItem();
            $json = json_decode($item->getJson(), true);
            $qty = $item->getQty();
            $json['qty'] = $qty;
            $json['quote_Item_id']= $quote_item_id;
            $json_data = json_encode($json);
        }

        // after editing in wishlist

        else if ($wishlist_item_id) {
            $wishlistitem = $this->itemFactory->create()->load($wishlist_item_id);
            $json = json_decode($wishlistitem->getJson(),true);
            $json['qty'] = (int)$wishlistitem->getQty();
            $json['wishlist_item_id']= $wishlist_item_id;
            $json_data = json_encode($json);
        }


        // after editing in design list
        else if ($data != NULL) {
            $jsonOriginal = base64_decode($data['json_data']);
            $json = json_decode($jsonOriginal, true);
            $json['qty'] = (float)1;
            $json_data = json_encode($json);

        } else {
            $json_data = "";
        }

        $resultPage = $this->_pageFactory->create();
        $block = $resultPage->getLayout()->getBlock('womenjacket_index_index');
        if ($block) {            
            $block->setData('json_data', $json_data);
        }
        $resultPage->getConfig()->getTitle()->set(__(''));
        return $resultPage;

    }

}
