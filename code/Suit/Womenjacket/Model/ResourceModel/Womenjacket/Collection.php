<?php
/**
* @author Dhirajkumar Deore
* Copyright © 2018 Magento. All rights reserved.
* See COPYING.txt for license details.
*/
namespace Suit\Womenjacket\Model\ResourceModel\Womenjacket;

use \Suit\Womenjacket\Model\ResourceModel\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'id';

    /**
     * Load data for preview flag
     *
     * @var bool
     */
    protected $_previewFlag;

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Suit\Womenjacket\Model\Womenjacket', 'Suit\Womenjacket\Model\ResourceModel\Womenjacket');
        $this->_map['fields']['id'] = 'main_table.id';
    }
}
