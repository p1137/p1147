<?php

namespace Suit\Womenjacket\Model\ResourceModel\SuitTuxedoType;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection {

    protected function _construct() {
        $this->_init('Suit\Womenjacket\Model\SuitTuxedoType', 'Suit\Womenjacket\Model\ResourceModel\SuitTuxedoType');
    }

}
