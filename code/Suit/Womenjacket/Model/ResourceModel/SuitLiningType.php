<?php

namespace Suit\Womenjacket\Model\ResourceModel;

class SuitLiningType extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb {

    protected function _construct() {
        $this->_init('suitliningtype', 'suitliningtype_id');
    }

}
