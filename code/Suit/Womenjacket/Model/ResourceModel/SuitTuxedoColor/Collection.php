<?php

namespace Suit\Womenjacket\Model\ResourceModel\SuitTuxedoColor;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection {

    protected function _construct() {
        $this->_init('Suit\Womenjacket\Model\SuitTuxedoColor', 'Suit\Womenjacket\Model\ResourceModel\SuitTuxedoColor');
    }

}
	