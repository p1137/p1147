<?php

namespace Suit\Womenjacket\Model\ResourceModel;

class SuitTuxedoColor extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb {

    protected function _construct() {
        $this->_init('suittuxedocolor', 'suittuxedocolor_id');
    }

}
