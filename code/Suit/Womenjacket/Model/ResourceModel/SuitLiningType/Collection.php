<?php

namespace Suit\Womenjacket\Model\ResourceModel\SuitLiningType;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection {

    protected function _construct() {
        $this->_init('Suit\Womenjacket\Model\SuitLiningType', 'Suit\Womenjacket\Model\ResourceModel\SuitLiningType');
    }

}
