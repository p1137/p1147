<?php

/**
 * @author Akshay Shelke    
 * Copyright © 2018 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Suit\Womenjacket\Model\ResourceModel;

class Suitcoatstyles extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb {

    protected function _construct() {
        $this->_init('woman_suitcoatstyles', 'suitcoatstyles_id');
    }

}
