<?php

namespace Suit\Womenjacket\Model;

class SuitTuxedoType extends \Magento\Framework\Model\AbstractModel {

    protected function _construct() {
        $this->_init('Suit\Womenjacket\Model\ResourceModel\SuitTuxedoType');
    }

}
