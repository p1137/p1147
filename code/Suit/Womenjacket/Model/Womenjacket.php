<?php

/**
 * @author Dhirajkumar Deore
 * Copyright © 2018 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Suit\Womenjacket\Model;

class Womenjacket extends \Magento\Framework\Model\AbstractModel {

    protected function _construct() {
        $this->_init('Suit\Womenjacket\Model\ResourceModel\Womenjacket');
    }

    public function getAvailableStatuses() {


        $availableOptions = ['1' => 'Enable',
            '0' => 'Disable'];

        return $availableOptions;
    }

}
