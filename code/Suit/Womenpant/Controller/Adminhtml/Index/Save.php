<?php

/**
 * @author Dhirajkumar Deore
 * Copyright © 2018 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Suit\Womenpant\Controller\Adminhtml\Index;

use Magento\Backend\App\Action\Context;
use Magento\Backend\App\Action;
use Suit\Womenpant\Model\Womenpant;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\TestFramework\Inspection\Exception;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\RequestInterface;

//use Magento\Framework\Stdlib\DateTime\DateTime;
//use Magento\Ui\Component\MassAction\Filter;
//use FME\News\Model\ResourceModel\Test\CollectionFactory;

class Save extends \Magento\Backend\App\Action {

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;
    protected $scopeConfig;
    protected $_escaper;
    protected $inlineTranslation;
    protected $_dateFactory;
    protected $_womenpantfitFactory;
    protected $_womenpantcuffFactory;
    protected $_womenpantsidepocketFactory;
    protected $_womenpantpleatsFactory;
    protected $_waistbandFactory;
    protected $_womenpantcoinpocketFactory;
    protected $_womenpantbeltloopsFactory;
    protected $objectManager;
    protected $connection;
    protected $directory;
    protected $rootPath1;
    protected $rootPath;

    /**
     * @param Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param DataPersistorInterface $dataPersistor
     */
    public function __construct(
    Context $context, DataPersistorInterface $dataPersistor, \Magento\Framework\Escaper $escaper, \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Magento\Framework\Stdlib\DateTime\DateTimeFactory $dateFactory, \Suit\Womenpant\Model\WomenpantfitFactory $womenpantfitFactory, \Suit\Womenpant\Model\WomenpantcuffFactory $womenpantcuffFactory, \Suit\Womenpant\Model\WomenpantsidepocketFactory $womenpantsidepocketFactory, \Suit\Womenpant\Model\WomenpantbackpocketFactory $womenpantbackpocketFactory, \Suit\Womenpant\Model\WomenpantpleatsFactory $womenpantpleatsFactory, \Suit\Womenpant\Model\WaistbandFactory $waistbandFactory, \Suit\Womenpant\Model\WomenpantcoinpocketFactory $womenpantcoinpocketFactory, \Suit\Womenpant\Model\WomenpantbeltloopsFactory $womenpantbeltloopsFactory
    ) {
        $this->_womenpantfitFactory = $womenpantfitFactory;
        $this->_womenpantcuffFactory = $womenpantcuffFactory;
        $this->_womenpantsidepocketFactory = $womenpantsidepocketFactory;
        $this->_womenpantbackpocketFactory = $womenpantbackpocketFactory;
        $this->_womenpantpleatsFactory = $womenpantpleatsFactory;
        $this->_waistbandFactory = $waistbandFactory;
        $this->_womenpantcoinpocketFactory = $womenpantcoinpocketFactory;
        $this->_womenpantbeltloopsFactory = $womenpantbeltloopsFactory;
        $this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->connection = $this->objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $this->directory = $this->objectManager->get('\Magento\Framework\Filesystem\DirectoryList');
        $this->rootPath1 = $this->directory->getRoot();
        $fileSystem = $this->objectManager->create('\Magento\Framework\Filesystem');
        $this->mediaPath = $fileSystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA)->getAbsolutePath();
        $this->rootPath = $this->mediaPath . "suit-tool/";
        $this->dataPersistor = $dataPersistor;
        $this->scopeConfig = $scopeConfig;
        $this->_escaper = $escaper;
        $this->_dateFactory = $dateFactory;
        $this->inlineTranslation = $inlineTranslation;
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute() {

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();


        if ($data) {

            $id = $this->getRequest()->getParam('id');

            if (isset($data['status']) && $data['status'] === 'true') {
                $data['status'] = Block::STATUS_ENABLED;
            }
            if (empty($data['id'])) {
                $data['id'] = null;
            }


            /** @var \Magento\Cms\Model\Block $model */
            $model = $this->_objectManager->create('Suit\Womenpant\Model\Womenpant')->load($id);
            if (!$model->getId() && $id) {
                $this->messageManager->addError(__('This suit_womenpant no longer exists.'));
                return $resultRedirect->setPath('*/*/');
            }


            if (isset($data['fabric_thumb'][0]['name']) && isset($data['fabric_thumb'][0]['tmp_name'])) {
                $data['fabric_thumb'] = 'suit-pant/women_fab_suit_images/thumbs/' . $data['fabric_thumb'][0]['name'];
            } elseif (isset($data['fabric_thumb'][0]['name']) && !isset($data['image'][0]['tmp_name'])) {
                $data['fabric_thumb'] = $data['fabric_thumb'][0]['name'];
            } else {
                $data['fabric_thumb'] = null;
            }

            if (isset($data['display_fabric_thumb'][0]['name']) && isset($data['display_fabric_thumb'][0]['tmp_name'])) {
                $data['display_fabric_thumb'] = 'suit-pant/women_fab_suit_images/thumbs/' . $data['display_fabric_thumb'][0]['name'];
            } elseif (isset($data['display_fabric_thumb'][0]['name']) && !isset($data['image'][0]['tmp_name'])) {
                $data['display_fabric_thumb'] = $data['display_fabric_thumb'][0]['name'];
            } else {
                $data['display_fabric_thumb'] = null;
            }

            if (isset($data['fabric_large_image'][0]['name']) && isset($data['fabric_large_image'][0]['tmp_name'])) {
                $data['fabric_large_image'] = 'suit-pant/women_fab_suit_images/thumbs/' . $data['fabric_large_image'][0]['name'];
            } elseif (isset($data['fabric_large_image'][0]['name']) && !isset($data['image'][0]['tmp_name'])) {
                $data['fabric_large_image'] = $data['fabric_large_image'][0]['name'];
            } else {
                $data['fabric_large_image'] = null;
            }
            $data['status'] = 0;
            $model->setData($data);

            $this->inlineTranslation->suspend();
            try {
                $model->save();
                $this->messageManager->addSuccess(__('suit_womenpant Saved successfully'));
                $this->dataPersistor->clear('suit_womenpant');


                /* Suit Image Generation Start */
                if ($data['fabric_thumb'] != '') {

                    /* color image creation start */
                    $fabric_id = $model->getId();
                    $tableName = $this->connection->getTableName('generation_cmd');
                    $homepath = $this->rootPath;

                    $fabricThumbImgpath = $this->mediaPath . $data['fabric_thumb'];
                    $fabricName = $fabric_id . "_womenpant_fabric_main.png";
                    $fabric_0 = $homepath . 'women_fab_suit_images/' . $fabricName;

//back

                    $fabricName_90 = $fabric_id . "_womenpant_fabric_main_90.png";
                    $fabric_90 = $homepath . 'women_fab_suit_images/' . $fabricName_90;

//for zoom view
                    $fabric_zoom = $homepath . "fab_suit_images/" . $fabric_id . "_womenpant_fabric_zoom.png";
                    $shirtFabImgPath = $homepath . 'women_fab_suit_images/';

                    exec('convert ' . $fabricThumbImgpath . ' -rotate 90 ' . $fabric_90);

                    exec('convert -size 1080x1320 tile:' . $fabric_90 . ' ' . $shirtFabImgPath . $fabric_id . '_womenpant_fabric_main_back.png');
                    exec('convert -size 1080x1320 tile:' . $fabric_90 . ' ' . $fabric_90);

                    $fabricName_60 = $fabric_id . "_womenpant_fabric_main_60.png";
                    $fabric_60 = $homepath . 'women_fab_suit_images/' . $fabricName_60;



                    exec('convert -size 1080x1320 tile:' . $fabricThumbImgpath . ' ' . $fabric_0);
//created for collar

                    exec('convert ' . $fabric_0 . ' -background "rgba(0,0,0,0.5)" -distort SRT -67 ' . $fabric_60);

                    exec('convert ' . $fabric_0 . ' -background "rgba(0,0,0,0.5)" -distort SRT 80 ' . $shirtFabImgPath . $fabric_id . '_womenpant_fabric_cuff_side.png');

//zoom view fabirc
                    exec('convert -size 1200x900 tile:' . $fabricThumbImgpath . ' ' . $fabric_zoom);
//created for collar
                    exec('convert ' . $fabric_zoom . ' -background "rgba(0,0,0,0.5)" -distort SRT 90 ' . $shirtFabImgPath . $fabric_id . '_womenpant_fabric_main_back_zoom.png');

                    /* END create fabric image */

                    /* Create plain fabric image */




                    $lapel_fabric_left = $homepath . 'women_fab_suit_images/' . $fabric_id . '_womenpant_lapel_fabric_left.png';
                    $lapel_fabric_right = $homepath . 'women_fab_suit_images/' . $fabric_id . '_womenpant_lapel_fabric_right.png';
                    exec('convert ' . $fabric_0 . ' -background "rgba(0,0,0,0.5)" -distort SRT 15 ' . $lapel_fabric_left);
                    exec('convert ' . $fabric_0 . ' -background "rgba(0,0,0,0.5)" -distort SRT -15 ' . $lapel_fabric_right);


                    $lapel_fabric_upper_left = $homepath . 'women_fab_suit_images/' . $fabric_id . '_womenpant_lapel_fabric_upper_left.png';
                    $lapel_fabric_upper_right = $homepath . 'women_fab_suit_images/' . $fabric_id . '_womenpant_lapel_fabric_upper_right.png';
                    exec('convert ' . $fabric_0 . ' -background "rgba(0,0,0,0.5)" -distort SRT -45 ' . $lapel_fabric_upper_left);
                    exec('convert ' . $fabric_0 . ' -background "rgba(0,0,0,0.5)" -distort SRT 45 ' . $lapel_fabric_upper_right);


                    $fabricName_wave_left = $fabric_id . "_womenpant_fabric_wave_left.png";
                    $fabricName_wave_right = $fabric_id . "_womenpant_fabric_wave_right.png";

                    $wave_left = $homepath . 'women_fab_suit_images/' . $fabricName_wave_left;
                    $wave_right = $homepath . 'women_fab_suit_images/' . $fabricName_wave_right;

                    exec('convert ' . $fabric_0 . ' -background "rgba(0,0,0,0.5)" -distort SRT -5 ' . $wave_left);
                    exec('convert ' . $fabric_0 . ' -background "rgba(0,0,0,0.5)" -distort SRT 5 ' . $wave_right);


// MEN //
//                    womenpant fit start
                    $womenpantfitFactory = $this->_womenpantfitFactory->create()->getCollection();
                    $rows_fit = $womenpantfitFactory->getData();
                    foreach ($rows_fit as $fit) {

                        $womenpantfit_id = $fit['pantfit_id'];

                        $fit_glow = $homepath . $fit['glow_image'];
                        $fit_mask = $homepath . $fit['mask_image'];
                        $fit_highlighted = $homepath . $fit['hi_image'];

                        if ($fit['glow_image'] != '' && $fit['mask_image'] != '' && $fit['hi_image'] != '') {
//mask changed
                            $cmd = 'composite -compose Dst_In  -gravity center ' . $fit_mask . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'women_suit_images/pant_images/pantfit/' . $fabric_id . '_pantfit_' . $womenpantfit_id . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//crop
                            $cmd = 'convert ' . $homepath . 'women_suit_images/pant_images/pantfit/' . $fabric_id . '_pantfit_' . $womenpantfit_id . '_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_suit_images/pant_images/pantfit/' . $fabric_id . '_pantfit_' . $womenpantfit_id . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//glow
                            $cmd = 'convert ' . $homepath . 'women_suit_images/pant_images/pantfit/' . $fabric_id . '_pantfit_' . $womenpantfit_id . '_view_1.png ' . $fit_glow . ' -geometry +0+0 -composite ' . $homepath . 'women_suit_images/pant_images/pantfit/' . $fabric_id . '_pantfit_' . $womenpantfit_id . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//highlighted
                            $cmd = 'composite ' . $fit_highlighted . ' -compose Overlay  ' . $homepath . 'women_suit_images/pant_images/pantfit/' . $fabric_id . '_pantfit_' . $womenpantfit_id . '_view_1.png ' . $homepath . 'women_suit_images/pant_images/pantfit/' . $fabric_id . '_pantfit_' . $womenpantfit_id . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }


                        $fit_back_glow = $homepath . '' . $fit['back_glow_image'];
                        $fit_back_mask = $homepath . '' . $fit['back_mask_image'];
                        $fit_back_hi = $homepath . '' . $fit['back_hi_image'];

                        if ($fit['back_glow_image'] != '' && $fit['back_mask_image'] != '' && $fit['back_hi_image'] != '') {
//mask changed

                            $cmd = 'composite -compose Dst_In  -gravity center ' . $fit_back_mask . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'women_suit_images/pant_images/pantfit/' . $fabric_id . '_pantfit_' . $womenpantfit_id . '_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//crop
                            $cmd = 'convert ' . $homepath . 'women_suit_images/pant_images/pantfit/' . $fabric_id . '_pantfit_' . $womenpantfit_id . '_view_2.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_suit_images/pant_images/pantfit/' . $fabric_id . '_pantfit_' . $womenpantfit_id . '_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//glow
                            $cmd = 'convert ' . $homepath . 'women_suit_images/pant_images/pantfit/' . $fabric_id . '_pantfit_' . $womenpantfit_id . '_view_2.png ' . $fit_back_glow . ' -geometry +0+0 -composite ' . $homepath . 'women_suit_images/pant_images/pantfit/' . $fabric_id . '_pantfit_' . $womenpantfit_id . '_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//highlighted
                            $cmd = 'composite ' . $fit_back_hi . ' -compose Overlay  ' . $homepath . 'women_suit_images/pant_images/pantfit/' . $fabric_id . '_pantfit_' . $womenpantfit_id . '_view_2.png ' . $homepath . 'women_suit_images/pant_images/pantfit/' . $fabric_id . '_pantfit_' . $womenpantfit_id . '_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }

                        $fit_folded_straight_glow = $homepath . '' . $fit['folded_straight_glow_image'];
                        $fit_folded_straight_mask = $homepath . '' . $fit['folded_straight_mask_image'];
                        $fit_folded_straight_hi = $homepath . '' . $fit['folded_straight_hi_image'];

                        if ($fit['folded_straight_glow_image'] != '' && $fit['folded_straight_mask_image'] != '' && $fit['folded_straight_hi_image'] != '') {
//mask changed

                            $cmd = 'composite -compose Dst_In  -gravity center ' . $fit_folded_straight_mask . ' ' . $fabric_90 . ' -alpha Set ' . $homepath . 'women_suit_images/pant_images/pantfit/' . $fabric_id . '_womenpantfit_straight_' . $womenpantfit_id . '_view_3.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//crop
                            $cmd = 'convert ' . $homepath . 'women_suit_images/pant_images/pantfit/' . $fabric_id . '_womenpantfit_straight_' . $womenpantfit_id . '_view_3.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_suit_images/pant_images/pantfit/' . $fabric_id . '_womenpantfit_straight_' . $womenpantfit_id . '_view_3.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//glow
                            $cmd = 'convert ' . $homepath . 'women_suit_images/pant_images/pantfit/' . $fabric_id . '_womenpantfit_straight_' . $womenpantfit_id . '_view_3.png ' . $fit_folded_straight_glow . ' -geometry +0+0 -composite ' . $homepath . 'women_suit_images/pant_images/pantfit/' . $fabric_id . '_womenpantfit_straight_' . $womenpantfit_id . '_view_3.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//highlighted
                            $cmd = 'composite ' . $fit_folded_straight_hi . ' -compose Overlay  ' . $homepath . 'women_suit_images/pant_images/pantfit/' . $fabric_id . '_womenpantfit_straight_' . $womenpantfit_id . '_view_3.png ' . $homepath . 'women_suit_images/pant_images/pantfit/' . $fabric_id . '_womenpantfit_straight_' . $womenpantfit_id . '_view_3.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }

                        $fit_folded_tilted_glow = $homepath . '' . $fit['folded_tilted_glow_image'];
                        $fit_folded_tilted_mask = $homepath . '' . $fit['folded_tilted_mask_image'];
                        $fit_folded_tilted_hi = $homepath . '' . $fit['folded_tilted_hi_image'];

                        if ($fit['folded_tilted_glow_image'] != '' && $fit['folded_tilted_mask_image'] != '' && $fit['folded_tilted_hi_image'] != '') {
//mask changed

                            $cmd = 'composite -compose Dst_In  -gravity center ' . $fit_folded_tilted_mask . ' ' . $fabric_60 . ' -alpha Set ' . $homepath . 'women_suit_images/pant_images/pantfit/' . $fabric_id . '_womenpantfit_tilt_' . $womenpantfit_id . '_view_3.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//crop
                            $cmd = 'convert ' . $homepath . 'women_suit_images/pant_images/pantfit/' . $fabric_id . '_womenpantfit_tilt_' . $womenpantfit_id . '_view_3.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_suit_images/pant_images/pantfit/' . $fabric_id . '_womenpantfit_tilt_' . $womenpantfit_id . '_view_3.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//glow
                            $cmd = 'convert ' . $homepath . 'women_suit_images/pant_images/pantfit/' . $fabric_id . '_womenpantfit_tilt_' . $womenpantfit_id . '_view_3.png ' . $fit_folded_tilted_glow . ' -geometry +0+0 -composite ' . $homepath . 'women_suit_images/pant_images/pantfit/' . $fabric_id . '_womenpantfit_tilt_' . $womenpantfit_id . '_view_3.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//highlighted
                            $cmd = 'composite ' . $fit_folded_tilted_hi . ' -compose Overlay  ' . $homepath . 'women_suit_images/pant_images/pantfit/' . $fabric_id . '_womenpantfit_tilt_' . $womenpantfit_id . '_view_3.png ' . $homepath . 'women_suit_images/pant_images/pantfit/' . $fabric_id . '_womenpantfit_tilt_' . $womenpantfit_id . '_view_3.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }


                        $cmd = 'convert ' . $homepath . 'women_suit_images/pant_images/pantfit/' . $fabric_id . '_womenpantfit_straight_' . $womenpantfit_id . '_view_3.png ' . $homepath . 'women_suit_images/pant_images/pantfit/' . $fabric_id . '_womenpantfit_tilt_' . $womenpantfit_id . '_view_3.png -geometry +0+0 -composite ' . $homepath . 'women_suit_images/pant_images/pantfit/' . $fabric_id . '_pantfit_' . $womenpantfit_id . '_view_3.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);

                        $cmd = "rm " . $homepath . 'women_suit_images/pant_images/pantfit/' . $fabric_id . '_womenpantfit_tilt_' . $womenpantfit_id . '_view_3.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);

                        $cmd = "rm " . $homepath . 'women_suit_images/pant_images/pantfit/' . $fabric_id . '_womenpantfit_straight_' . $womenpantfit_id . '_view_3.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);
                    }


//Womenpant fit end
// Womenpant cuff start
                    $womenpantcuffFactory = $this->_womenpantcuffFactory->create()->getCollection();
                    $rows_cuff = $womenpantcuffFactory->getData();

                    foreach ($rows_cuff as $cuff) {

                        $womenpantcuff_id = $cuff['pantcuff_id'];

                        $cuff_glow = $homepath . '' . $cuff['glow_image'];
                        $cuff_mask = $homepath . '' . $cuff['mask_image'];
                        $cuff_highlighted = $homepath . '' . $cuff['hi_image'];

                        if ($cuff['glow_image'] != '' && $cuff['mask_image'] != '' && $cuff['hi_image'] != '') {
//mask changed
                            $cmd = 'composite -compose Dst_In  -gravity center ' . $cuff_mask . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'women_suit_images/pant_images/pantcuff/' . $fabric_id . '_pantcuff_' . $womenpantcuff_id . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//crop
                            $cmd = 'convert ' . $homepath . 'women_suit_images/pant_images/pantcuff/' . $fabric_id . '_pantcuff_' . $womenpantcuff_id . '_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_suit_images/pant_images/pantcuff/' . $fabric_id . '_pantcuff_' . $womenpantcuff_id . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//glow
                            $cmd = 'convert ' . $homepath . 'women_suit_images/pant_images/pantcuff/' . $fabric_id . '_pantcuff_' . $womenpantcuff_id . '_view_1.png ' . $cuff_glow . ' -geometry +0+0 -composite ' . $homepath . 'women_suit_images/pant_images/pantcuff/' . $fabric_id . '_pantcuff_' . $womenpantcuff_id . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//highlighted
                            $cmd = 'composite ' . $cuff_highlighted . ' -compose Overlay  ' . $homepath . 'women_suit_images/pant_images/pantcuff/' . $fabric_id . '_pantcuff_' . $womenpantcuff_id . '_view_1.png ' . $homepath . 'women_suit_images/pant_images/pantcuff/' . $fabric_id . '_pantcuff_' . $womenpantcuff_id . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }


                        $cuff_back_glow = $homepath . '' . $cuff['back_glow_image'];
                        $cuff_back_mask = $homepath . '' . $cuff['back_mask_image'];
                        $cuff_back_hi = $homepath . '' . $cuff['back_hi_image'];

                        if ($cuff['back_glow_image'] != '' && $cuff['back_mask_image'] != '' && $cuff['back_hi_image'] != '') {
//mask changed

                            $cmd = 'composite -compose Dst_In  -gravity center ' . $cuff_back_mask . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'women_suit_images/pant_images/pantcuff/' . $fabric_id . '_pantcuff_' . $womenpantcuff_id . '_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//crop
                            $cmd = 'convert ' . $homepath . 'women_suit_images/pant_images/pantcuff/' . $fabric_id . '_pantcuff_' . $womenpantcuff_id . '_view_2.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_suit_images/pant_images/pantcuff/' . $fabric_id . '_pantcuff_' . $womenpantcuff_id . '_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//glow
                            $cmd = 'convert ' . $homepath . 'women_suit_images/pant_images/pantcuff/' . $fabric_id . '_pantcuff_' . $womenpantcuff_id . '_view_2.png ' . $cuff_back_glow . ' -geometry +0+0 -composite ' . $homepath . 'women_suit_images/pant_images/pantcuff/' . $fabric_id . '_pantcuff_' . $womenpantcuff_id . '_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//highlighted
                            $cmd = 'composite ' . $cuff_back_hi . ' -compose Overlay  ' . $homepath . 'women_suit_images/pant_images/pantcuff/' . $fabric_id . '_pantcuff_' . $womenpantcuff_id . '_view_2.png ' . $homepath . 'women_suit_images/pant_images/pantcuff/' . $fabric_id . '_pantcuff_' . $womenpantcuff_id . '_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }

                        $cuff_folded_straight_glow = $homepath . '' . $cuff['folded_glow_image'];
                        $cuff_folded_straight_mask = $homepath . '' . $cuff['folded_mask_image'];
                        $cuff_folded_straight_hi = $homepath . '' . $cuff['folded_hi_image'];

                        if ($cuff['folded_glow_image'] != '' && $cuff['folded_mask_image'] != '' && $cuff['folded_hi_image'] != '') {
//mask changed

                            $cmd = 'composite -compose Dst_In  -gravity center ' . $cuff_folded_straight_mask . ' ' . $fabric_60 . ' -alpha Set ' . $homepath . 'women_suit_images/pant_images/pantcuff/' . $fabric_id . '_pantcuff_' . $womenpantcuff_id . '_view_3.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//crop
                            $cmd = 'convert ' . $homepath . 'women_suit_images/pant_images/pantcuff/' . $fabric_id . '_pantcuff_' . $womenpantcuff_id . '_view_3.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_suit_images/pant_images/pantcuff/' . $fabric_id . '_pantcuff_' . $womenpantcuff_id . '_view_3.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//glow
                            $cmd = 'convert ' . $homepath . 'women_suit_images/pant_images/pantcuff/' . $fabric_id . '_pantcuff_' . $womenpantcuff_id . '_view_3.png ' . $cuff_folded_straight_glow . ' -geometry +0+0 -composite ' . $homepath . 'women_suit_images/pant_images/pantcuff/' . $fabric_id . '_pantcuff_' . $womenpantcuff_id . '_view_3.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//highlighted
                            $cmd = 'composite ' . $cuff_folded_straight_hi . ' -compose Overlay  ' . $homepath . 'women_suit_images/pant_images/pantcuff/' . $fabric_id . '_pantcuff_' . $womenpantcuff_id . '_view_3.png ' . $homepath . 'women_suit_images/pant_images/pantcuff/' . $fabric_id . '_pantcuff_' . $womenpantcuff_id . '_view_3.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }
                    }
//Womenpant cuff end
// Womenpant Side Pockets
                    $womenpantsidepocketFactory = $this->_womenpantsidepocketFactory->create()->getCollection();
                    $womenpantsidepocket_collection = $womenpantsidepocketFactory->getData();

                    foreach ($womenpantsidepocket_collection as $back) {

                        $womenpantsidepocketId = $back['pantsidepocket_id'];
                        $sidepocket_static_id = "1";

//view 1
                        $glow_view_1 = $homepath . '' . $back['glow_image_view_1'];
                        $mask_view_1 = $homepath . '' . $back['mask_image_view_1'];
                        $highlight_view_1 = $homepath . '' . $back['highlighted_image_view_1'];


                        if ($back['glow_image_view_1'] && $back['mask_image_view_1'] && $back['highlighted_image_view_1']) {
//mask
                            $cmd = 'composite -compose Dst_In -gravity center ' . $mask_view_1 . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'women_suit_images/pant_images/pantpockets/' . $fabric_id . '_pantpockets_' . $womenpantsidepocketId . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//convert
                            $cmd = 'convert ' . $homepath . 'women_suit_images/pant_images/pantpockets/' . $fabric_id . '_pantpockets_' . $womenpantsidepocketId . '_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_suit_images/pant_images/pantpockets/' . $fabric_id . '_pantpockets_' . $womenpantsidepocketId . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);

//glow
                            $cmd = 'convert ' . $homepath . 'women_suit_images/pant_images/pantpockets/' . $fabric_id . '_pantpockets_' . $womenpantsidepocketId . '_view_1.png ' . $glow_view_1 . ' -geometry +0+0 -composite ' . $homepath . 'women_suit_images/pant_images/pantpockets/' . $fabric_id . '_pantpockets_' . $womenpantsidepocketId . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);

//highlight
                            $cmd = 'composite ' . $highlight_view_1 . ' -compose Overlay  ' . $homepath . 'women_suit_images/pant_images/pantpockets/' . $fabric_id . '_pantpockets_' . $womenpantsidepocketId . '_view_1.png ' . $homepath . 'women_suit_images/pant_images/pantpockets/' . $fabric_id . '_pantpockets_' . $womenpantsidepocketId . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }

//view 3

                        $glow_view_3 = $homepath . '' . $back['glow_image_view_3'];
                        $mask_view_3 = $homepath . '' . $back['mask_image_view_3'];
                        $highlight_view_3 = $homepath . '' . $back['highlighted_image_view_3'];

                        if ($back['glow_image_view_3'] && $back['mask_image_view_3'] && $back['highlighted_image_view_3']) {
//mask
                            $cmd = 'composite -compose Dst_In -gravity center ' . $mask_view_3 . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'women_suit_images/pant_images/pantpockets/' . $fabric_id . '_pantpockets_' . $womenpantsidepocketId . '_view_3.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//crop
                            $cmd = 'convert ' . $homepath . 'women_suit_images/pant_images/pantpockets/' . $fabric_id . '_pantpockets_' . $womenpantsidepocketId . '_view_3.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_suit_images/pant_images/pantpockets/' . $fabric_id . '_pantpockets_' . $womenpantsidepocketId . '_view_3.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);

//glow
                            $cmd = 'convert ' . $homepath . 'women_suit_images/pant_images/pantpockets/' . $fabric_id . '_pantpockets_' . $womenpantsidepocketId . '_view_3.png ' . $glow_view_3 . ' -geometry +0+0 -composite ' . $homepath . 'women_suit_images/pant_images/pantpockets/' . $fabric_id . '_pantpockets_' . $womenpantsidepocketId . '_view_3.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);

//highlight
                            $cmd = 'composite ' . $highlight_view_3 . ' -compose Overlay  ' . $homepath . 'women_suit_images/pant_images/pantpockets/' . $fabric_id . '_pantpockets_' . $womenpantsidepocketId . '_view_3.png ' . $homepath . 'women_suit_images/pant_images/pantpockets/' . $fabric_id . '_pantpockets_' . $womenpantsidepocketId . '_view_3.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }
                    }

// Womenpant Side Pockets

// Womenpant back Pockets 
                    $pantbackpocketFactory = $this->_womenpantbackpocketFactory->create()->getCollection();
                    $pantbackpocket_collection = $pantbackpocketFactory->getData();

                    foreach ($pantbackpocket_collection as $back) {

                        $pantbackpocketId = $back['pantbackpocket_id'];
                        $backpocket_static_id = "1";



//back right view 2
                        $glow_view_2_right = $homepath . '' . $back['glow_image_view_2_right'];
                        $mask_view_2_right = $homepath . '' . $back['mask_image_view_2_right'];
                        $highlight_view_2_right = $homepath . '' . $back['highlighted_image_view_2_right'];


                        if ($back['glow_image_view_2_right'] && $back['mask_image_view_2_right'] && $back['highlighted_image_view_2_right']) {
//mask
                            $cmd = 'composite -compose Dst_In -gravity center ' . $mask_view_2_right . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'women_suit_images/pant_images/pantpockets/back/' . $fabric_id . '_pantpockets_' . $pantbackpocketId . '_right_view_2.png';

                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//convert
                            $cmd = 'convert ' . $homepath . 'women_suit_images/pant_images/pantpockets/back/' . $fabric_id . '_pantpockets_' . $pantbackpocketId . '_right_view_2.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_suit_images/pant_images/pantpockets/back/' . $fabric_id . '_pantpockets_' . $pantbackpocketId . '_right_view_2.png';

                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);

//glow
                            $cmd = 'convert ' . $homepath . 'women_suit_images/pant_images/pantpockets/back/' . $fabric_id . '_pantpockets_' . $pantbackpocketId . '_right_view_2.png ' . $glow_view_2_right . ' -geometry +0+0 -composite ' . $homepath . 'women_suit_images/pant_images/pantpockets/back/' . $fabric_id . '_pantpockets_' . $pantbackpocketId . '_right_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);

//highlight
                            $cmd = 'composite ' . $highlight_view_2_right . ' -compose Overlay  ' . $homepath . 'women_suit_images/pant_images/pantpockets/back/' . $fabric_id . '_pantpockets_' . $pantbackpocketId . '_right_view_2.png ' . $homepath . 'women_suit_images/pant_images/pantpockets/back/' . $fabric_id . '_pantpockets_' . $pantbackpocketId . '_right_view_2.png';

                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);

                        }

//back left view 2
                        $glow_view_2_left = $homepath . '' . $back['glow_image_view_2_left'];
                        $mask_view_2_left = $homepath . '' . $back['mask_image_view_2_left'];
                        $highlight_view_2_left = $homepath . '' . $back['highlighted_image_view_2_left'];


                        if ($back['glow_image_view_2_left'] && $back['mask_image_view_2_left'] && $back['highlighted_image_view_2_left']) {
//mask
                            $cmd = 'composite -compose Dst_In -gravity center ' . $mask_view_2_left . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'women_suit_images/pant_images/pantpockets/back/' . $fabric_id . '_pantpockets_' . $pantbackpocketId . '_left_view_2.png';

                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//convert
                            $cmd = 'convert ' . $homepath . 'women_suit_images/pant_images/pantpockets/back/'. $fabric_id . '_pantpockets_' . $pantbackpocketId . '_left_view_2.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_suit_images/pant_images/pantpockets/back/' . $fabric_id . '_pantpockets_' . $pantbackpocketId . '_left_view_2.png';

                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);

//glow
                            $cmd = 'convert ' . $homepath . 'women_suit_images/pant_images/pantpockets/back/' . $fabric_id . '_pantpockets_' . $pantbackpocketId . '_left_view_2.png ' . $glow_view_2_left . ' -geometry +0+0 -composite ' . $homepath . 'women_suit_images/pant_images/pantpockets/back/' . $fabric_id . '_pantpockets_' . $pantbackpocketId . '_left_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);

//highlight
                            $cmd = 'composite ' . $highlight_view_2_left . ' -compose Overlay  ' . $homepath . 'women_suit_images/pant_images/pantpockets/back/' . $fabric_id . '_pantpockets_' . $pantbackpocketId . '_left_view_2.png ' . $homepath . 'women_suit_images/pant_images/pantpockets/back/' . $fabric_id . '_pantpockets_' . $pantbackpocketId . '_left_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);


//combine left right back pocket                            

                        if ($back['mask_image_view_2_right'] != '' && $back['mask_image_view_2_left'] != '') {
                            $cmd = 'convert ' . $homepath . 'women_suit_images/pant_images/pantpockets/back/' . $fabric_id . '_pantpockets_' . $pantbackpocketId . '_right_view_2.png ' . $homepath . 'women_suit_images/pant_images/pantpockets/back/' . $fabric_id . '_pantpockets_' . $pantbackpocketId . '_left_view_2.png -geometry +0+0 -composite ' . $homepath . 'women_suit_images/pant_images/pantpockets/back/' . $fabric_id . '_pantpockets_' . $pantbackpocketId . '_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);

                            $cmd = 'rm ' . $homepath . 'women_suit_images/pant_images/pantpockets/back/' . $fabric_id . '_pantpockets_' . $pantbackpocketId . '_right_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);

                            $cmd = 'rm ' . $homepath . 'women_suit_images/pant_images/pantpockets/back/' . $fabric_id . '_pantpockets_' . $pantbackpocketId . '_left_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);


                        } else if ($back['mask_image_view_2_right'] == '' && $back['mask_image_view_2_left'] != '') {
                            $cmd = 'convert ' . $homepath . 'woman_glow_mask/pantsidepocket/blank.png' . $homepath . 'women_suit_images/pant_images/pantpockets/back/' . $fabric_id . '_pantpockets_' . $pantbackpocketId . '_left_view_2.png -geometry +0+0 -composite ' . $homepath . 'women_suit_images/pant_images/pantpockets/back/' . $fabric_id . '_pantpockets_' . $pantbackpocketId . '_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);

                            $cmd = 'rm ' . $homepath . 'women_suit_images/pant_images/pantpockets/back/' . $fabric_id . '_pantpockets_' . $pantbackpocketId . '_left_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        } else if ($back['mask_image_view_2_right'] != '' && $back['mask_image_view_2_left'] == '') {
                            $cmd = 'convert ' . $homepath . 'women_suit_images/pant_images/pantpockets/back/' . $fabric_id . '_pantpockets_' . $pantbackpocketId . '_right_view_2.png ' . $homepath . 'woman_glow_mask/pantsidepocket/blank.png  -geometry +0+0 -composite ' . $homepath . 'women_suit_images/pant_images/pantpockets/back/' . $fabric_id . '_pantpockets_' . $pantbackpocketId . '_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);

                            $cmd = 'rm ' . $homepath . 'women_suit_images/pant_images/pantpockets/back/' . $fabric_id . '_pantpockets_' . $pantbackpocketId . '_right_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }

              }

//view 3

                        $glow_view_3 = $homepath . '' . $back['glow_image_view_3'];
                        $mask_view_3 = $homepath . '' . $back['mask_image_view_3'];
                        $highlight_view_3 = $homepath . '' . $back['highlighted_image_view_3'];

                        if ($back['glow_image_view_3'] && $back['mask_image_view_3'] && $back['highlighted_image_view_3']) {
//mask
                            $cmd = 'composite -compose Dst_In -gravity center ' . $mask_view_3 . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'women_suit_images/pant_images/pantpockets/back/' . $fabric_id . '_pantpockets_' . $pantbackpocketId . '_view_3.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//crop
                            $cmd = 'convert ' . $homepath . 'women_suit_images/pant_images/pantpockets/back/' . $fabric_id . '_pantpockets_' . $pantbackpocketId . '_view_3.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_suit_images/pant_images/pantpockets/back/' . $fabric_id . '_pantpockets_' . $pantbackpocketId . '_view_3.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);

//glow
                            $cmd = 'convert ' . $homepath . 'women_suit_images/pant_images/pantpockets/back/' . $fabric_id . '_pantpockets_' . $pantbackpocketId . '_view_3.png ' . $glow_view_3 . ' -geometry +0+0 -composite ' . $homepath . 'women_suit_images/pant_images/pantpockets/back/' . $fabric_id . '_pantpockets_' . $pantbackpocketId . '_view_3.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);

//highlight
                            $cmd = 'composite ' . $highlight_view_3 . ' -compose Overlay  ' . $homepath . 'women_suit_images/pant_images/pantpockets/back/' . $fabric_id . '_pantpockets_' . $pantbackpocketId . '_view_3.png ' . $homepath . 'women_suit_images/pant_images/pantpockets/back/' . $fabric_id . '_pantpockets_' . $pantbackpocketId . '_view_3.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }
                    }

// Womenpant back Pockets 



// Womenpant pleats
                    $womenpantpleatsFactory = $this->_womenpantpleatsFactory->create()->getCollection();
                    $womenpantpleats_collection = $womenpantpleatsFactory->getData();

                    foreach ($womenpantpleats_collection as $womenpantpleats) {

                        $womenpantpleatsId = $womenpantpleats['pantpleats_id'];
                        $womenpantpleats_static_id = "1";

//view 1
                        $glow_view_1 = $homepath . '' . $womenpantpleats['glow_image_view_1'];
                        $mask_view_1 = $homepath . '' . $womenpantpleats['mask_image_view_1'];
                        $highlight_view_1 = $homepath . '' . $womenpantpleats['highlighted_image_view_1'];


                        if ($womenpantpleats['glow_image_view_1'] && $womenpantpleats['mask_image_view_1'] && $womenpantpleats['highlighted_image_view_1']) {
//mask
                            $cmd = 'composite -compose Dst_In -gravity center ' . $mask_view_1 . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'women_suit_images/pant_images/pantpleats/' . $womenpantpleats_static_id . '_pantpleats_' . $womenpantpleatsId . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//convert
                            $cmd = 'convert ' . $homepath . 'women_suit_images/pant_images/pantpleats/' . $womenpantpleats_static_id . '_pantpleats_' . $womenpantpleatsId . '_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_suit_images/pant_images/pantpleats/' . $womenpantpleats_static_id . '_pantpleats_' . $womenpantpleatsId . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);

//glow
                            $cmd = 'convert ' . $homepath . 'women_suit_images/pant_images/pantpleats/' . $womenpantpleats_static_id . '_pantpleats_' . $womenpantpleatsId . '_view_1.png ' . $glow_view_1 . ' -geometry +0+0 -composite ' . $homepath . 'women_suit_images/pant_images/pantpleats/' . $womenpantpleats_static_id . '_pantpleats_' . $womenpantpleatsId . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);

//highlight
                            $cmd = 'composite ' . $highlight_view_1 . ' -compose Overlay  ' . $homepath . 'women_suit_images/pant_images/pantpleats/' . $womenpantpleats_static_id . '_pantpleats_' . $womenpantpleatsId . '_view_1.png ' . $homepath . 'women_suit_images/pant_images/pantpleats/' . $womenpantpleats_static_id . '_pantpleats_' . $womenpantpleatsId . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }

//view 3

                        $glow_view_3 = $homepath . '' . $womenpantpleats['glow_image_view_3'];
                        $mask_view_3 = $homepath . '' . $womenpantpleats['mask_image_view_3'];
                        $highlight_view_3 = $homepath . '' . $womenpantpleats['highlighted_image_view_3'];

                        if ($womenpantpleats['glow_image_view_3'] && $womenpantpleats['mask_image_view_3'] && $womenpantpleats['highlighted_image_view_3']) {
//mask
                            $cmd = 'composite -compose Dst_In -gravity center ' . $mask_view_3 . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'women_suit_images/pant_images/pantpleats/' . $womenpantpleats_static_id . '_pantpleats_' . $womenpantpleatsId . '_view_3.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//crop
                            $cmd = 'convert ' . $homepath . 'women_suit_images/pant_images/pantpleats/' . $womenpantpleats_static_id . '_pantpleats_' . $womenpantpleatsId . '_view_3.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_suit_images/pant_images/pantpleats/' . $womenpantpleats_static_id . '_pantpleats_' . $womenpantpleatsId . '_view_3.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);

//glow
                            $cmd = 'convert ' . $homepath . 'women_suit_images/pant_images/pantpleats/' . $womenpantpleats_static_id . '_pantpleats_' . $womenpantpleatsId . '_view_3.png ' . $glow_view_3 . ' -geometry +0+0 -composite ' . $homepath . 'women_suit_images/pant_images/pantpleats/' . $womenpantpleats_static_id . '_pantpleats_' . $womenpantpleatsId . '_view_3.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);

//highlight
                            $cmd = 'composite ' . $highlight_view_3 . ' -compose Overlay  ' . $homepath . 'women_suit_images/pant_images/pantpleats/' . $womenpantpleats_static_id . '_pantpleats_' . $womenpantpleatsId . '_view_3.png ' . $homepath . 'women_suit_images/pant_images/pantpleats/' . $womenpantpleats_static_id . '_pantpleats_' . $womenpantpleatsId . '_view_3.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }
                    }

// Womenpant pleats
// Womenpant Waistband
                    $waistbandFactory = $this->_waistbandFactory->create()->getCollection();
                    $waistband_collection = $waistbandFactory->getData();

                    foreach ($waistband_collection as $waistband) {

                        $waistbandId = $waistband['waistband_id'];
                        $waistband_static_id = "1";

//view 1
                        $glow_view_1 = $homepath . '' . $waistband['glow_image_view_1'];
                        $mask_view_1 = $homepath . '' . $waistband['mask_image_view_1'];
                        $highlight_view_1 = $homepath . '' . $waistband['highlighted_image_view_1'];

                        if ($waistband['glow_image_view_1'] && $waistband['mask_image_view_1'] && $waistband['highlighted_image_view_1']) {
//mask
                            $cmd = 'composite -compose Dst_In -gravity center ' . $mask_view_1 . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'women_suit_images/pant_images/waistband/' . $fabric_id . '_waistband_' . $waistbandId . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//convert
                            $cmd = 'convert ' . $homepath . 'women_suit_images/pant_images/waistband/' . $fabric_id . '_waistband_' . $waistbandId . '_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_suit_images/pant_images/waistband/' . $fabric_id . '_waistband_' . $waistbandId . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);

//glow
                            $cmd = 'convert ' . $homepath . 'women_suit_images/pant_images/waistband' . $fabric_id . '_waistband_' . $waistbandId . '_view_1.png ' . $glow_view_1 . ' -geometry +0+0 -composite ' . $homepath . 'women_suit_images/pant_images/waistband' . $fabric_id . '_waistband_' . $waistbandId . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);

//highlight
                            $cmd = 'composite ' . $highlight_view_1 . ' -compose Overlay  ' . $homepath . 'women_suit_images/pant_images/waistband/' . $fabric_id . '_waistband_' . $waistbandId . '_view_1.png ' . $homepath . 'women_suit_images/pant_images/waistband/' . $fabric_id . '_waistband_' . $waistbandId . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }
                    }
// Womenpant Waistband
// Womenpant Coin pockets
                    $womenpantcoinpocketFactory = $this->_womenpantcoinpocketFactory->create()->getCollection();
                    $womenpantCoin_collection = $womenpantcoinpocketFactory->getData();

                    foreach ($womenpantCoin_collection as $womenpantCoin) {

                        $vestPocketId = $womenpantCoin['coinpocket_id'];
                        $waistband_static_id = "1";

//view 1
                        $glow_view_1 = $homepath . '' . $womenpantCoin['glow_image_view_1'];
                        $mask_view_1 = $homepath . '' . $womenpantCoin['mask_image_view_1'];
                        $highlight_view_1 = $homepath . '' . $womenpantCoin['highlighted_image_view_1'];

                        if ($womenpantCoin['glow_image_view_1'] && $womenpantCoin['mask_image_view_1'] && $womenpantCoin['highlighted_image_view_1']) {
//mask
                            $cmd = 'composite -compose Dst_In -gravity center ' . $mask_view_1 . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'women_suit_images/pant_images/pantcoin/' . $fabric_id . '_pantcoin_' . $vestPocketId . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//convert
                            $cmd = 'convert ' . $homepath . 'women_suit_images/pant_images/pantcoin/' . $fabric_id . '_pantcoin_' . $vestPocketId . '_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_suit_images/pant_images/pantcoin/' . $fabric_id . '_pantcoin_' . $vestPocketId . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);

//glow
                            $cmd = 'convert ' . $homepath . 'women_suit_images/pant_images/pantcoin' . $fabric_id . '_pantcoin_' . $vestPocketId . '_view_1.png ' . $glow_view_1 . ' -geometry +0+0 -composite ' . $homepath . 'women_suit_images/pant_images/pantcoin' . $fabric_id . '_pantcoin_' . $vestPocketId . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);

//highlight
                            $cmd = 'composite ' . $highlight_view_1 . ' -compose Overlay  ' . $homepath . 'women_suit_images/pant_images/pantcoin/' . $fabric_id . '_pantcoin_' . $vestPocketId . '_view_1.png ' . $homepath . 'women_suit_images/pant_images/pantcoin/' . $fabric_id . '_pantcoin_' . $vestPocketId . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }
                    }

// Womenpant Coin pockets
// Womenpant Belt loops
                    $womenpantbeltloopsFactory = $this->_womenpantbeltloopsFactory->create()->getCollection();
                    $womenpantBeltLoops_collection = $womenpantbeltloopsFactory->getData();

                    foreach ($womenpantBeltLoops_collection as $womenpantBeltLoops) {
                        $womenpantbeltloopid = $womenpantBeltLoops['pantbeltloops_id'];

//view 1
                        $glow_view_1 = $homepath . '' . $womenpantBeltLoops['glow_image'];
                        $mask_view_1 = $homepath . '' . $womenpantBeltLoops['mask_image'];
                        $highlight_view_1 = $homepath . '' . $womenpantBeltLoops['hi_image'];

                        if ($womenpantBeltLoops['glow_image'] && $womenpantBeltLoops['mask_image'] && $womenpantBeltLoops['hi_image']) {
//mask
                            $cmd = 'composite -compose Dst_In -gravity center ' . $mask_view_1 . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'women_suit_images/pant_images/pantbeltloops/' . $fabric_id . '_pantbeltloops_' . $womenpantbeltloopid . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//convert
                            $cmd = 'convert ' . $homepath . 'women_suit_images/pant_images/pantbeltloops/' . $fabric_id . '_pantbeltloops_' . $womenpantbeltloopid . '_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_suit_images/pant_images/pantbeltloops/' . $fabric_id . '_pantbeltloops_' . $womenpantbeltloopid . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);

//glow
                            $cmd = 'convert ' . $homepath . 'women_suit_images/pant_images/pantbeltloops' . $fabric_id . '_pantbeltloops_' . $womenpantbeltloopid . '_view_1.png ' . $glow_view_1 . ' -geometry +0+0 -composite ' . $homepath . 'women_suit_images/pant_images/pantbeltloops' . $fabric_id . '_pantbeltloops_' . $womenpantbeltloopid . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);

//highlight
                            $cmd = 'composite ' . $highlight_view_1 . ' -compose Overlay  ' . $homepath . 'women_suit_images/pant_images/pantbeltloops/' . $fabric_id . '_pantbeltloops_' . $womenpantbeltloopid . '_view_1.png ' . $homepath . 'women_suit_images/pant_images/pantbeltloops/' . $fabric_id . '_pantbeltloops_' . $womenpantbeltloopid . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }


//view 2
                        $glow_view_2 = $homepath . '' . $womenpantBeltLoops['back_glow_image'];
                        $mask_view_2 = $homepath . '' . $womenpantBeltLoops['back_mask_image'];
                        $highlight_view_2 = $homepath . '' . $womenpantBeltLoops['back_hi_image'];

                        if ($womenpantBeltLoops['back_glow_image'] && $womenpantBeltLoops['back_mask_image'] && $womenpantBeltLoops['back_hi_image']) {
//mask
                            $cmd = 'composite -compose Dst_In -gravity center ' . $mask_view_2 . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'women_suit_images/pant_images/pantbeltloops/' . $fabric_id . '_pantbeltloops_' . $womenpantbeltloopid . '_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//convert
                            $cmd = 'convert ' . $homepath . 'women_suit_images/pant_images/pantbeltloops/' . $fabric_id . '_pantbeltloops_' . $womenpantbeltloopid . '_view_2.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_suit_images/pant_images/pantbeltloops/' . $fabric_id . '_pantbeltloops_' . $womenpantbeltloopid . '_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);

//glow
                            $cmd = 'convert ' . $homepath . 'women_suit_images/pant_images/pantbeltloops' . $fabric_id . '_pantbeltloops_' . $womenpantbeltloopid . '_view_2.png ' . $glow_view_2 . ' -geometry +0+0 -composite ' . $homepath . 'women_suit_images/pant_images/pantbeltloops' . $fabric_id . '_pantbeltloops_' . $womenpantbeltloopid . '_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);

//highlight
                            $cmd = 'composite ' . $highlight_view_2 . ' -compose Overlay  ' . $homepath . 'women_suit_images/pant_images/pantbeltloops/' . $fabric_id . '_pantbeltloops_' . $womenpantbeltloopid . '_view_2.png ' . $homepath . 'women_suit_images/pant_images/pantbeltloops/' . $fabric_id . '_pantbeltloops_' . $womenpantbeltloopid . '_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }

//view 3
                        $glow_view_3 = $homepath . '' . $womenpantBeltLoops['folded_glow_image'];
                        $mask_view_3 = $homepath . '' . $womenpantBeltLoops['folded_mask_image'];
                        $highlight_view_3 = $homepath . '' . $womenpantBeltLoops['folded_hi_image'];

                        if ($womenpantBeltLoops['back_glow_image'] && $womenpantBeltLoops['back_mask_image'] && $womenpantBeltLoops['back_hi_image']) {
//mask
                            $cmd = 'composite -compose Dst_In -gravity center ' . $mask_view_3 . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'women_suit_images/pant_images/pantbeltloops/' . $fabric_id . '_pantbeltloops_' . $womenpantbeltloopid . '_view_3.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//convert
                            $cmd = 'convert ' . $homepath . 'women_suit_images/pant_images/pantbeltloops/' . $fabric_id . '_pantbeltloops_' . $womenpantbeltloopid . '_view_3.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_suit_images/pant_images/pantbeltloops/' . $fabric_id . '_pantbeltloops_' . $womenpantbeltloopid . '_view_3.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//glow
                            $cmd = 'convert ' . $homepath . 'women_suit_images/pant_images/pantbeltloops' . $fabric_id . '_pantbeltloops_' . $womenpantbeltloopid . '_view_3.png ' . $glow_view_3 . ' -geometry +0+0 -composite ' . $homepath . 'women_suit_images/pant_images/pantbeltloops' . $fabric_id . '_pantbeltloops_' . $womenpantbeltloopid . '_view_3.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);


//highlight
                            $cmd = 'composite ' . $highlight_view_3 . ' -compose Overlay  ' . $homepath . 'women_suit_images/pant_images/pantbeltloops/' . $fabric_id . '_pantbeltloops_' . $womenpantbeltloopid . '_view_3.png ' . $homepath . 'women_suit_images/pant_images/pantbeltloops/' . $fabric_id . '_pantbeltloops_' . $womenpantbeltloopid . '_view_3.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }
                    }

                    $cmd = "rm " . $homepath . 'women_fab_suit_images/' . $fabric_id . "_womenpant_fabric_cuff_side.png";
                    $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($writeQuery);

                    $cmd = "rm " . $homepath . 'women_fab_suit_images/' . $fabric_id . "_womenpant_fabric_main.png";
                    $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($writeQuery);

                    $cmd = "rm " . $homepath . 'women_fab_suit_images/' . $fabric_id . "_womenpant_fabric_main_60.png";
                    $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($writeQuery);

                    $cmd = "rm " . $homepath . 'women_fab_suit_images/' . $fabric_id . "_womenpant_fabric_main_90.png";
                    $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($writeQuery);

                    $cmd = "rm " . $homepath . 'women_fab_suit_images/' . $fabric_id . "_womenpant_fabric_main_back.png";
                    $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($writeQuery);

                    $cmd = "rm " . $homepath . 'women_fab_suit_images/' . $fabric_id . "_womenpant_fabric_main_back_zoom.png";
                    $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($writeQuery);

                    $cmd = "rm " . $homepath . 'women_fab_suit_images/' . $fabric_id . "_womenpant_fabric_wave_left.png";
                    $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($writeQuery);

                    $cmd = "rm " . $homepath . 'women_fab_suit_images/' . $fabric_id . "_womenpant_fabric_wave_right.png";
                    $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($writeQuery);

                    $cmd = "rm " . $homepath . 'women_fab_suit_images/' . $fabric_id . "_womenpant_fabric_zoom.png";
                    $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($writeQuery);

                    $cmd = "rm " . $homepath . 'women_fab_suit_images/' . $fabric_id . "_womenpant_lapel_fabric_left.png";
                    $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($writeQuery);

                    $cmd = "rm " . $homepath . 'women_fab_suit_images/' . $fabric_id . "_womenpant_lapel_fabric_right.png";
                    $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($writeQuery);

                    $cmd = "rm " . $homepath . 'women_fab_suit_images/' . $fabric_id . "_womenpant_lapel_fabric_upper_left.png";
                    $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($writeQuery);

                    $cmd = "rm " . $homepath . 'women_fab_suit_images/' . $fabric_id . "_womenpant_lapel_fabric_upper_right.png";
                    $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($writeQuery);


                    $updateQuery = "UPDATE `suit_womenpant_fabrics` SET `status`=1 WHERE `id`=" . $fabric_id;
                    $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$updateQuery}')";
                    $this->connection->query($writeQuery);
// Womenpant Belt loops
                }
                /* Suit Image Generation End */

                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['id' => $model->getId()]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the suit_womenpant.'));
            }
            $this->dataPersistor->set('suit_womenpant', $data);
            return $resultRedirect->setPath('*/*/edit', ['id' => $this->getRequest()->getParam('id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }

}
