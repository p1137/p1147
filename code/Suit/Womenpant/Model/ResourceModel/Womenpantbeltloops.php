<?php

namespace Suit\Womenpant\Model\ResourceModel;

class Womenpantbeltloops extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb {

    protected function _construct() {
        $this->_init('woman_pantbeltloops', 'pantbeltloops_id');
    }

}
