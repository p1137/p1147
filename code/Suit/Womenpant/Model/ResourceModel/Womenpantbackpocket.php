<?php


namespace Suit\Womenpant\Model\ResourceModel;

class Womenpantbackpocket extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb {

    protected function _construct() {
        $this->_init('woman_pantbackpocket', 'pantbackpocket_id');
    }

}
