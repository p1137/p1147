<?php
/**
* @author Dhirajkumar Deore
* Copyright © 2018 Magento. All rights reserved.
* See COPYING.txt for license details.
*/
namespace Suit\Womenpant\Model\ResourceModel\Womenpant;

use \Suit\Womenpant\Model\ResourceModel\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'id';

    /**
     * Load data for preview flag
     *
     * @var bool
     */
    protected $_previewFlag;

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Suit\Womenpant\Model\Womenpant', 'Suit\Womenpant\Model\ResourceModel\Womenpant');
        $this->_map['fields']['id'] = 'main_table.id';
    }
}
