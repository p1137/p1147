<?php

/**
 * @author Akshay Shelke    
 * Copyright © 2018 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Suit\Womenpant\Model\ResourceModel\Womenpantcuff;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection {

    protected function _construct() {
        $this->_init('Suit\Womenpant\Model\Womenpantcuff', 'Suit\Womenpant\Model\ResourceModel\Womenpantcuff');
    }

}
