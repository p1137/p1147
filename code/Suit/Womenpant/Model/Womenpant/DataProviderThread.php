<?php

/**
 * @author Dhirajkumar Deore
 * Copyright © 2018 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Suit\Womenpant\Model\Womenpant;

use Suit\Womenpant\Model\ResourceModel\Womenpant\CollectionFactory;
use Magento\Framework\App\Request\DataPersistorInterface;

/**
 * Class DataProvider
 */
class DataProviderThread extends \Magento\Ui\DataProvider\AbstractDataProvider {

    /**
     * @var \Magento\Cms\Model\ResourceModel\Block\Collection
     */
    protected $collection;

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;
    public $_storeManager;

    /**
     * @var array
     */
    protected $loadedData;

    /**
     * Constructor
     *
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $blockCollectionFactory
     * @param DataPersistorInterface $dataPersistor
     * @param array $meta
     * @param array $data
     */
    public function __construct(
    $name, $primaryFieldName, $requestFieldName, CollectionFactory $blockCollectionFactory, DataPersistorInterface $dataPersistor, \Magento\Store\Model\StoreManagerInterface $storeManager, array $meta = [], array $data = []
    ) {
        $this->collection = $blockCollectionFactory->create();
        $this->_storeManager = $storeManager;
        $this->dataPersistor = $dataPersistor;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData() {
        $baseurl = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        $items = $this->collection->getItems();
        /** @var \Magento\Cms\Model\Block $block */
        foreach ($items as $block) {
            $this->loadedData[$block->getId()] = $block->getData();

            $temp = $block->getData();
            $img = [];
            $img[0]['name'] = $temp['suit_womenpantimage'];
            $img[0]['url'] = $baseurl . $temp['suit_womenpantimage'];
            $temp['suit_womenpantimage'] = $img;

            $img2 = [];
            $img2[0]['name'] = $temp['suit_womenpantmtl'];
            $img2[0]['url'] = $baseurl . $temp['suit_womenpantmtl'];
            $temp['suit_womenpantmtl'] = $img2;

            $imgthread = [];
            $imgthread[0]['name'] = $temp['suit_womenpantobj'];
            $imgthread[0]['url'] = $baseurl . $temp['suit_womenpantobj'];
            $temp['suit_womenpantobj'] = $imgthread;
        }






        $data = $this->dataPersistor->get('suit_womenpant');

        if (!empty($data)) {
            $block = $this->collection->getNewEmptyItem();
            $block->setData($data);



            $this->loadedData[$block->getId()] = $block->getData();

            $this->dataPersistor->clear('suit_womenpant');
        }

        if (empty($this->loadedData)) {
            return $this->loadedData;
        } else {
            if ($block->getData('suit_womenpantobj') != null && $block->getData('suit_womenpantimage') != null) {
                $t2[$block->getId()] = $temp;
                return $t2;
            } else {
                return $this->loadedData;
            }
        }
    }

}
