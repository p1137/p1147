<?php

namespace Suit\Pant\Model;

class Waistbandstyle extends \Magento\Framework\Model\AbstractModel {

    protected function _construct() {
        $this->_init('Suit\Pant\Model\ResourceModel\Waistbandstyle');
    }

}
