<?php


namespace Suit\Pant\Model\ResourceModel;

class Pantbackpocket extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb {

    protected function _construct() {
        $this->_init('pantbackpocket', 'pantbackpocket_id');
    }

}
