<?php
/**
* @author Dhirajkumar Deore
* Copyright © 2018 Magento. All rights reserved.
* See COPYING.txt for license details.
*/
namespace Suit\Pant\Model\ResourceModel\Pant;

use \Suit\Pant\Model\ResourceModel\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'id';

    /**
     * Load data for preview flag
     *
     * @var bool
     */
    protected $_previewFlag;

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Suit\Pant\Model\Pant', 'Suit\Pant\Model\ResourceModel\Pant');
        $this->_map['fields']['id'] = 'main_table.id';
    }
}
