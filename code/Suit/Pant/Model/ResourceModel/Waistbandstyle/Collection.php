<?php

namespace Suit\Pant\Model\ResourceModel\Waistbandstyle;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection {

    protected function _construct() {
        $this->_init('Suit\Pant\Model\Waistbandstyle', 'Suit\Pant\Model\ResourceModel\Waistbandstyle');
    }

}
