<?php


namespace Suit\Pant\Model\ResourceModel\Pantbackpocket;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection {

    protected function _construct() {
        $this->_init('Suit\Pant\Model\Pantbackpocket', 'Suit\Pant\Model\ResourceModel\Pantbackpocket');
    }

}
