<?php


namespace Suit\Pant\Model\ResourceModel;

class Waistbandstyle extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb {

    protected function _construct() {
        $this->_init('waistband_style', 'waistband_id');
    }

}
