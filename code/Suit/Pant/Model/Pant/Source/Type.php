<?php
/**
* @author Dhirajkumar Deore
* Copyright © 2018 Magento. All rights reserved.
* See COPYING.txt for license details.
*/
namespace Suit\Pant\Model\Pant\Source;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class Status
 */
class Type implements OptionSourceInterface
{

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {

      return [
          ['value' => '1', 'label' => __('wash/fade')],
          ['value' => '2', 'label' => __('riped/details')],
          ['value' => '2', 'label' => __('Pants/Art')]
      ];
    }
}
