<?php

/**
 * @author Akshay Shelke 
 * Copyright © 2018 Magento. All rights reserved. 
 * See COPYING.txt for license details. 
 */

namespace Suit\Pant\Model;

class Waistband extends \Magento\Framework\Model\AbstractModel {

    protected function _construct() {
        $this->_init('Suit\Pant\Model\ResourceModel\Waistband');
    }

}
