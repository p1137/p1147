<?php

/**
 * @author Dhirajkumar Deore
 * Copyright © 2018 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Suit\Pant\Controller\Adminhtml\Index;

use Magento\Backend\App\Action\Context;
use Magento\Backend\App\Action;
use Suit\Pant\Model\Pant;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\TestFramework\Inspection\Exception;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\RequestInterface;

//use Magento\Framework\Stdlib\DateTime\DateTime;
//use Magento\Ui\Component\MassAction\Filter;
//use FME\News\Model\ResourceModel\Test\CollectionFactory;

class Save extends \Magento\Backend\App\Action {

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;
    protected $scopeConfig;
    protected $_escaper;
    protected $inlineTranslation;
    protected $_dateFactory;
    protected $_pantfitFactory;
    protected $_pantcuffFactory;
    protected $_pantsidepocketFactory;
    protected $_pantpleatsFactory;
    protected $_waistbandFactory;
    protected $_pantcoinpocketFactory;
    protected $_pantbeltloopsFactory;
    protected $objectManager;
    protected $connection;
    protected $directory;
    protected $rootPath1;
    protected $rootPath;

    /**
     * @param Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param DataPersistorInterface $dataPersistor
     */
    public function __construct(
    Context $context, DataPersistorInterface $dataPersistor, \Magento\Framework\Escaper $escaper, \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Magento\Framework\Stdlib\DateTime\DateTimeFactory $dateFactory, \Suit\Pant\Model\PantfitFactory $pantfitFactory, \Suit\Pant\Model\PantcuffFactory $pantcuffFactory, \Suit\Pant\Model\PantsidepocketFactory $pantsidepocketFactory,\Suit\Pant\Model\PantbackpocketFactory $pantbackpocketFactory, \Suit\Pant\Model\PantpleatsFactory $pantpleatsFactory, \Suit\Pant\Model\WaistbandFactory $waistbandFactory, \Suit\Pant\Model\PantcoinpocketFactory $pantcoinpocketFactory, \Suit\Pant\Model\PantbeltloopsFactory $pantbeltloopsFactory
    ) {
        $this->_pantfitFactory = $pantfitFactory;
        $this->_pantcuffFactory = $pantcuffFactory;
        $this->_pantsidepocketFactory = $pantsidepocketFactory;
        $this->_pantbackpocketFactory = $pantbackpocketFactory;
        $this->_pantpleatsFactory = $pantpleatsFactory;
        $this->_waistbandFactory = $waistbandFactory;
        $this->_pantcoinpocketFactory = $pantcoinpocketFactory;
        $this->_pantbeltloopsFactory = $pantbeltloopsFactory;
        $this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->connection = $this->objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $this->directory = $this->objectManager->get('\Magento\Framework\Filesystem\DirectoryList');
        $this->rootPath1 = $this->directory->getRoot();
        $this->rootPath = $this->rootPath1 . "/pub/media/suit-tool/";
        $this->dataPersistor = $dataPersistor;
        $this->scopeConfig = $scopeConfig;
        $this->_escaper = $escaper;
        $this->_dateFactory = $dateFactory;
        $this->inlineTranslation = $inlineTranslation;
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute() {

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();


        if ($data) {

            $id = $this->getRequest()->getParam('id');

            if (isset($data['status']) && $data['status'] === 'true') {
                $data['status'] = Block::STATUS_ENABLED;
            }
            if (empty($data['id'])) {
                $data['id'] = null;
            }


            /** @var \Magento\Cms\Model\Block $model */
            $model = $this->_objectManager->create('Suit\Pant\Model\Pant')->load($id);
            if (!$model->getId() && $id) {
                $this->messageManager->addError(__('This suit_pant no longer exists.'));
                return $resultRedirect->setPath('*/*/');
            }


            if (isset($data['fabric_thumb'][0]['name']) && isset($data['fabric_thumb'][0]['tmp_name'])) {
                $data['fabric_thumb'] = 'suit-pant/fab_suit_images/thumbs/' . $data['fabric_thumb'][0]['name'];
            } elseif (isset($data['fabric_thumb'][0]['name']) && !isset($data['image'][0]['tmp_name'])) {
                $data['fabric_thumb'] = $data['fabric_thumb'][0]['name'];
            } else {
                $data['fabric_thumb'] = null;
            }

            if (isset($data['display_fabric_thumb'][0]['name']) && isset($data['display_fabric_thumb'][0]['tmp_name'])) {
                $data['display_fabric_thumb'] = 'suit-pant/fab_suit_images/thumbs/' . $data['display_fabric_thumb'][0]['name'];
            } elseif (isset($data['display_fabric_thumb'][0]['name']) && !isset($data['image'][0]['tmp_name'])) {
                $data['display_fabric_thumb'] = $data['display_fabric_thumb'][0]['name'];
            } else {
                $data['display_fabric_thumb'] = null;
            }

            if (isset($data['fabric_large_image'][0]['name']) && isset($data['fabric_large_image'][0]['tmp_name'])) {
                $data['fabric_large_image'] = 'suit-pant/fab_suit_images/thumbs/' . $data['fabric_large_image'][0]['name'];
            } elseif (isset($data['fabric_large_image'][0]['name']) && !isset($data['image'][0]['tmp_name'])) {
                $data['fabric_large_image'] = $data['fabric_large_image'][0]['name'];
            } else {
                $data['fabric_large_image'] = null;
            }
            $data['status'] = 0;
            $model->setData($data);

            $this->inlineTranslation->suspend();
            try {
                $model->save();
                $this->messageManager->addSuccess(__('suit_pant Saved successfully'));
                $this->dataPersistor->clear('suit_pant');

                /* Suit Image Generation Start */
                if ($data['fabric_thumb'] != '') {

                    /* color image creation start */
                    $fabric_id = $model->getId();
                    $tableName = $this->connection->getTableName('generation_cmd');
                    $homepath = $this->rootPath;

                    $fabricThumbImgpath = $this->rootPath1 . '/pub/media/' . $data['fabric_thumb'];
                    $fabricName = $fabric_id . "_pant_fabric_main.png";
                    $fabric_0 = $homepath . 'fab_suit_images/' . $fabricName;

//back

                    $fabricName_90 = $fabric_id . "_pant_fabric_main_90.png";
                    $fabric_90 = $homepath . 'fab_suit_images/' . $fabricName_90;

//for zoom view
                    $fabric_zoom = $homepath . "fab_suit_images/" . $fabric_id . "_pant_fabric_zoom.png";
                    $shirtFabImgPath = $homepath . 'fab_suit_images/';

                    exec('convert ' . $fabricThumbImgpath . ' -rotate 90 ' . $fabric_90);

                    exec('convert -size 1080x1320 tile:' . $fabric_90 . ' ' . $shirtFabImgPath . $fabric_id . '_pant_fabric_main_back.png');
                    exec('convert -size 1080x1320 tile:' . $fabric_90 . ' ' . $fabric_90);

                    $fabricName_60 = $fabric_id . "_pant_fabric_main_60.png";
                    $fabric_60 = $homepath . 'fab_suit_images/' . $fabricName_60;



                    exec('convert -size 1080x1320 tile:' . $fabricThumbImgpath . ' ' . $fabric_0);
//created for collar

                    exec('convert ' . $fabric_0 . ' -background "rgba(0,0,0,0.5)" -distort SRT -67 ' . $fabric_60);

                    exec('convert ' . $fabric_0 . ' -background "rgba(0,0,0,0.5)" -distort SRT 80 ' . $shirtFabImgPath . $fabric_id . '_pant_fabric_cuff_side.png');

//zoom view fabirc
                    exec('convert -size 1200x900 tile:' . $fabricThumbImgpath . ' ' . $fabric_zoom);
//created for collar
                    exec('convert ' . $fabric_zoom . ' -background "rgba(0,0,0,0.5)" -distort SRT 90 ' . $shirtFabImgPath . $fabric_id . '_pant_fabric_main_back_zoom.png');

                    /* END create fabric image */

                    /* Create plain fabric image */




                    $lapel_fabric_left = $homepath . 'fab_suit_images/' . $fabric_id . '_pant_lapel_fabric_left.png';
                    $lapel_fabric_right = $homepath . 'fab_suit_images/' . $fabric_id . '_pant_lapel_fabric_right.png';
                    exec('convert ' . $fabric_0 . ' -background "rgba(0,0,0,0.5)" -distort SRT 15 ' . $lapel_fabric_left);
                    exec('convert ' . $fabric_0 . ' -background "rgba(0,0,0,0.5)" -distort SRT -15 ' . $lapel_fabric_right);


                    $lapel_fabric_upper_left = $homepath . 'fab_suit_images/' . $fabric_id . '_pant_lapel_fabric_upper_left.png';
                    $lapel_fabric_upper_right = $homepath . 'fab_suit_images/' . $fabric_id . '_pant_lapel_fabric_upper_right.png';
                    exec('convert ' . $fabric_0 . ' -background "rgba(0,0,0,0.5)" -distort SRT -45 ' . $lapel_fabric_upper_left);
                    exec('convert ' . $fabric_0 . ' -background "rgba(0,0,0,0.5)" -distort SRT 45 ' . $lapel_fabric_upper_right);


                    $fabricName_wave_left = $fabric_id . "_pant_fabric_wave_left.png";
                    $fabricName_wave_right = $fabric_id . "_pant_fabric_wave_right.png";

                    $wave_left = $homepath . 'fab_suit_images/' . $fabricName_wave_left;
                    $wave_right = $homepath . 'fab_suit_images/' . $fabricName_wave_right;

                    exec('convert ' . $fabric_0 . ' -background "rgba(0,0,0,0.5)" -distort SRT -5 ' . $wave_left);
                    exec('convert ' . $fabric_0 . ' -background "rgba(0,0,0,0.5)" -distort SRT 5 ' . $wave_right);

// MEN //
//                    pant fit start                    
                    $pantfitFactory = $this->_pantfitFactory->create()->getCollection();
                    $rows_fit = $pantfitFactory->getData();

                    foreach ($rows_fit as $fit) {

                        $pantfit_id = $fit['pantfit_id'];

                        $fit_glow = $homepath . $fit['glow_image'];
                        $fit_mask = $homepath . $fit['mask_image'];
                        $fit_highlighted = $homepath . $fit['hi_image'];

                        if ($fit['glow_image'] != '' && $fit['mask_image'] != '' && $fit['hi_image'] != '') {
//mask changed
                            $cmd = 'composite -compose Dst_In  -gravity center ' . $fit_mask . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'pant_images/pantfit/' . $fabric_id . '_pantfit_' . $pantfit_id . '_view_1.png';

                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//crop
                            $cmd = 'convert ' . $homepath . 'pant_images/pantfit/' . $fabric_id . '_pantfit_' . $pantfit_id . '_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'pant_images/pantfit/' . $fabric_id . '_pantfit_' . $pantfit_id . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//glow
                            $cmd = 'convert ' . $homepath . 'pant_images/pantfit/' . $fabric_id . '_pantfit_' . $pantfit_id . '_view_1.png ' . $fit_glow . ' -geometry +0+0 -composite ' . $homepath . 'pant_images/pantfit/' . $fabric_id . '_pantfit_' . $pantfit_id . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//highlighted
                            $cmd = 'composite ' . $fit_highlighted . ' -compose Overlay  ' . $homepath . 'pant_images/pantfit/' . $fabric_id . '_pantfit_' . $pantfit_id . '_view_1.png ' . $homepath . 'pant_images/pantfit/' . $fabric_id . '_pantfit_' . $pantfit_id . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }


                        $fit_back_glow = $homepath . '' . $fit['back_glow_image'];
                        $fit_back_mask = $homepath . '' . $fit['back_mask_image'];
                        $fit_back_hi = $homepath . '' . $fit['back_hi_image'];

                        if ($fit['back_glow_image'] != '' && $fit['back_mask_image'] != '' && $fit['back_hi_image'] != '') {
//mask changed

                            $cmd = 'composite -compose Dst_In  -gravity center ' . $fit_back_mask . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'pant_images/pantfit/' . $fabric_id . '_pantfit_' . $pantfit_id . '_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//crop
                            $cmd = 'convert ' . $homepath . 'pant_images/pantfit/' . $fabric_id . '_pantfit_' . $pantfit_id . '_view_2.png -crop 500x1320+290+0  +repage ' . $homepath . 'pant_images/pantfit/' . $fabric_id . '_pantfit_' . $pantfit_id . '_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//glow
                            $cmd = 'convert ' . $homepath . 'pant_images/pantfit/' . $fabric_id . '_pantfit_' . $pantfit_id . '_view_2.png ' . $fit_back_glow . ' -geometry +0+0 -composite ' . $homepath . 'pant_images/pantfit/' . $fabric_id . '_pantfit_' . $pantfit_id . '_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//highlighted
                            $cmd = 'composite ' . $fit_back_hi . ' -compose Overlay  ' . $homepath . 'pant_images/pantfit/' . $fabric_id . '_pantfit_' . $pantfit_id . '_view_2.png ' . $homepath . 'pant_images/pantfit/' . $fabric_id . '_pantfit_' . $pantfit_id . '_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }

                        $fit_folded_straight_glow = $homepath . '' . $fit['folded_straight_glow_image'];
                        $fit_folded_straight_mask = $homepath . '' . $fit['folded_straight_mask_image'];
                        $fit_folded_straight_hi = $homepath . '' . $fit['folded_straight_hi_image'];

                        if ($fit['folded_straight_glow_image'] != '' && $fit['folded_straight_mask_image'] != '' && $fit['folded_straight_hi_image'] != '') {
//mask changed

                            $cmd = 'composite -compose Dst_In  -gravity center ' . $fit_folded_straight_mask . ' ' . $fabric_90 . ' -alpha Set ' . $homepath . 'pant_images/pantfit/' . $fabric_id . '_pantfit_straight_' . $pantfit_id . '_view_3.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//crop
                            $cmd = 'convert ' . $homepath . 'pant_images/pantfit/' . $fabric_id . '_pantfit_straight_' . $pantfit_id . '_view_3.png -crop 500x1320+290+0  +repage ' . $homepath . 'pant_images/pantfit/' . $fabric_id . '_pantfit_straight_' . $pantfit_id . '_view_3.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//glow
                            $cmd = 'convert ' . $homepath . 'pant_images/pantfit/' . $fabric_id . '_pantfit_straight_' . $pantfit_id . '_view_3.png ' . $fit_folded_straight_glow . ' -geometry +0+0 -composite ' . $homepath . 'pant_images/pantfit/' . $fabric_id . '_pantfit_straight_' . $pantfit_id . '_view_3.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//highlighted
                            $cmd = 'composite ' . $fit_folded_straight_hi . ' -compose Overlay  ' . $homepath . 'pant_images/pantfit/' . $fabric_id . '_pantfit_straight_' . $pantfit_id . '_view_3.png ' . $homepath . 'pant_images/pantfit/' . $fabric_id . '_pantfit_straight_' . $pantfit_id . '_view_3.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }

                        $fit_folded_tilted_glow = $homepath . '' . $fit['folded_tilted_glow_image'];
                        $fit_folded_tilted_mask = $homepath . '' . $fit['folded_tilted_mask_image'];
                        $fit_folded_tilted_hi = $homepath . '' . $fit['folded_tilted_hi_image'];

                        if ($fit['folded_tilted_glow_image'] != '' && $fit['folded_tilted_mask_image'] != '' && $fit['folded_tilted_hi_image'] != '') {
//mask changed

                            $cmd = 'composite -compose Dst_In  -gravity center ' . $fit_folded_tilted_mask . ' ' . $fabric_60 . ' -alpha Set ' . $homepath . 'pant_images/pantfit/' . $fabric_id . '_pantfit_tilt_' . $pantfit_id . '_view_3.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//crop
                            $cmd = 'convert ' . $homepath . 'pant_images/pantfit/' . $fabric_id . '_pantfit_tilt_' . $pantfit_id . '_view_3.png -crop 500x1320+290+0  +repage ' . $homepath . 'pant_images/pantfit/' . $fabric_id . '_pantfit_tilt_' . $pantfit_id . '_view_3.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//glow
                            $cmd = 'convert ' . $homepath . 'pant_images/pantfit/' . $fabric_id . '_pantfit_tilt_' . $pantfit_id . '_view_3.png ' . $fit_folded_tilted_glow . ' -geometry +0+0 -composite ' . $homepath . 'pant_images/pantfit/' . $fabric_id . '_pantfit_tilt_' . $pantfit_id . '_view_3.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//highlighted
                            $cmd = 'composite ' . $fit_folded_tilted_hi . ' -compose Overlay  ' . $homepath . 'pant_images/pantfit/' . $fabric_id . '_pantfit_tilt_' . $pantfit_id . '_view_3.png ' . $homepath . 'pant_images/pantfit/' . $fabric_id . '_pantfit_tilt_' . $pantfit_id . '_view_3.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }


                        $cmd = 'convert ' . $homepath . 'pant_images/pantfit/' . $fabric_id . '_pantfit_straight_' . $pantfit_id . '_view_3.png ' . $homepath . 'pant_images/pantfit/' . $fabric_id . '_pantfit_tilt_' . $pantfit_id . '_view_3.png -geometry +0+0 -composite ' . $homepath . 'pant_images/pantfit/' . $fabric_id . '_pantfit_' . $pantfit_id . '_view_3.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);

                        $cmd = "rm " . $homepath . 'pant_images/pantfit/' . $fabric_id . '_pantfit_tilt_' . $pantfit_id . '_view_3.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);

                        $cmd = "rm " . $homepath . 'pant_images/pantfit/' . $fabric_id . '_pantfit_straight_' . $pantfit_id . '_view_3.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);
                    }


//Pant fit end
// Pant cuff start 
                    $pantcuffFactory = $this->_pantcuffFactory->create()->getCollection();
                    $rows_cuff = $pantcuffFactory->getData();

                    foreach ($rows_cuff as $cuff) {

                        $pantcuff_id = $cuff['pantcuff_id'];

                        $cuff_glow = $homepath . '' . $cuff['glow_image'];
                        $cuff_mask = $homepath . '' . $cuff['mask_image'];
                        $cuff_highlighted = $homepath . '' . $cuff['hi_image'];

                        if ($cuff['glow_image'] != '' && $cuff['mask_image'] != '' && $cuff['hi_image'] != '') {
//mask changed
                            $cmd = 'composite -compose Dst_In  -gravity center ' . $cuff_mask . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'pant_images/pantcuff/' . $fabric_id . '_pantcuff_' . $pantcuff_id . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//crop
                            $cmd = 'convert ' . $homepath . 'pant_images/pantcuff/' . $fabric_id . '_pantcuff_' . $pantcuff_id . '_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'pant_images/pantcuff/' . $fabric_id . '_pantcuff_' . $pantcuff_id . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//glow
                            $cmd = 'convert ' . $homepath . 'pant_images/pantcuff/' . $fabric_id . '_pantcuff_' . $pantcuff_id . '_view_1.png ' . $cuff_glow . ' -geometry +0+0 -composite ' . $homepath . 'pant_images/pantcuff/' . $fabric_id . '_pantcuff_' . $pantcuff_id . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//highlighted
                            $cmd = 'composite ' . $cuff_highlighted . ' -compose Overlay  ' . $homepath . 'pant_images/pantcuff/' . $fabric_id . '_pantcuff_' . $pantcuff_id . '_view_1.png ' . $homepath . 'pant_images/pantcuff/' . $fabric_id . '_pantcuff_' . $pantcuff_id . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }


                        $cuff_back_glow = $homepath . '' . $cuff['back_glow_image'];
                        $cuff_back_mask = $homepath . '' . $cuff['back_mask_image'];
                        $cuff_back_hi = $homepath . '' . $cuff['back_hi_image'];

                        if ($cuff['back_glow_image'] != '' && $cuff['back_mask_image'] != '' && $cuff['back_hi_image'] != '') {
//mask changed

                            $cmd = 'composite -compose Dst_In  -gravity center ' . $cuff_back_mask . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'pant_images/pantcuff/' . $fabric_id . '_pantcuff_' . $pantcuff_id . '_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//crop
                            $cmd = 'convert ' . $homepath . 'pant_images/pantcuff/' . $fabric_id . '_pantcuff_' . $pantcuff_id . '_view_2.png -crop 500x1320+290+0  +repage ' . $homepath . 'pant_images/pantcuff/' . $fabric_id . '_pantcuff_' . $pantcuff_id . '_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//glow
                            $cmd = 'convert ' . $homepath . 'pant_images/pantcuff/' . $fabric_id . '_pantcuff_' . $pantcuff_id . '_view_2.png ' . $cuff_back_glow . ' -geometry +0+0 -composite ' . $homepath . 'pant_images/pantcuff/' . $fabric_id . '_pantcuff_' . $pantcuff_id . '_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//highlighted
                            $cmd = 'composite ' . $cuff_back_hi . ' -compose Overlay  ' . $homepath . 'pant_images/pantcuff/' . $fabric_id . '_pantcuff_' . $pantcuff_id . '_view_2.png ' . $homepath . 'pant_images/pantcuff/' . $fabric_id . '_pantcuff_' . $pantcuff_id . '_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }

                        $cuff_folded_straight_glow = $homepath . '' . $cuff['folded_glow_image'];
                        $cuff_folded_straight_mask = $homepath . '' . $cuff['folded_mask_image'];
                        $cuff_folded_straight_hi = $homepath . '' . $cuff['folded_hi_image'];

                        if ($cuff['folded_glow_image'] != '' && $cuff['folded_mask_image'] != '' && $cuff['folded_hi_image'] != '') {
//mask changed

                            $cmd = 'composite -compose Dst_In  -gravity center ' . $cuff_folded_straight_mask . ' ' . $fabric_60 . ' -alpha Set ' . $homepath . 'pant_images/pantcuff/' . $fabric_id . '_pantcuff_' . $pantcuff_id . '_view_3.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//crop
                            $cmd = 'convert ' . $homepath . 'pant_images/pantcuff/' . $fabric_id . '_pantcuff_' . $pantcuff_id . '_view_3.png -crop 500x1320+290+0  +repage ' . $homepath . 'pant_images/pantcuff/' . $fabric_id . '_pantcuff_' . $pantcuff_id . '_view_3.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//glow
                            $cmd = 'convert ' . $homepath . 'pant_images/pantcuff/' . $fabric_id . '_pantcuff_' . $pantcuff_id . '_view_3.png ' . $cuff_folded_straight_glow . ' -geometry +0+0 -composite ' . $homepath . 'pant_images/pantcuff/' . $fabric_id . '_pantcuff_' . $pantcuff_id . '_view_3.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//highlighted
                            $cmd = 'composite ' . $cuff_folded_straight_hi . ' -compose Overlay  ' . $homepath . 'pant_images/pantcuff/' . $fabric_id . '_pantcuff_' . $pantcuff_id . '_view_3.png ' . $homepath . 'pant_images/pantcuff/' . $fabric_id . '_pantcuff_' . $pantcuff_id . '_view_3.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }
                    }
//Pant cuff end
// Pant Side Pockets 
                    $pantsidepocketFactory = $this->_pantsidepocketFactory->create()->getCollection();
                    $pantsidepocket_collection = $pantsidepocketFactory->getData();

                    foreach ($pantsidepocket_collection as $back) {

                        $pantsidepocketId = $back['pantsidepocket_id'];
                        $sidepocket_static_id = "1";

//view 1
                        $glow_view_1 = $homepath . '' . $back['glow_image_view_1'];
                        $mask_view_1 = $homepath . '' . $back['mask_image_view_1'];
                        $highlight_view_1 = $homepath . '' . $back['highlighted_image_view_1'];


                        if ($back['glow_image_view_1'] && $back['mask_image_view_1'] && $back['highlighted_image_view_1']) {
//mask
                            $cmd = 'composite -compose Dst_In -gravity center ' . $mask_view_1 . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'pant_images/pantpockets/' . $fabric_id . '_pantpockets_' . $pantsidepocketId . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//convert
                            $cmd = 'convert ' . $homepath . 'pant_images/pantpockets/' . $fabric_id . '_pantpockets_' . $pantsidepocketId . '_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'pant_images/pantpockets/' . $fabric_id . '_pantpockets_' . $pantsidepocketId . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);


//glow
                            $cmd = 'convert ' . $homepath . 'pant_images/pantpockets/' . $fabric_id . '_pantpockets_' . $pantsidepocketId . '_view_1.png ' . $glow_view_1 . ' -geometry +0+0 -composite ' . $homepath . 'pant_images/pantpockets/' . $fabric_id . '_pantpockets_' . $pantsidepocketId . '_view_1.png';


// //glow
//                             $cmd = 'composite ' . $glow_view_1 . ' -compose Multiply  ' . $homepath . 'pant_images/pantpockets/' . $fabric_id . '_pantpockets_' . $pantsidepocketId . '_view_1.png ' . $homepath . 'pant_images/pantpockets/' . $fabric_id . '_pantpockets_' . $pantsidepocketId . '_view_1.png';


                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//highlight
                            $cmd = 'composite ' . $highlight_view_1 . ' -compose Overlay  ' . $homepath . 'pant_images/pantpockets/' . $fabric_id . '_pantpockets_' . $pantsidepocketId . '_view_1.png ' . $homepath . 'pant_images/pantpockets/' . $fabric_id . '_pantpockets_' . $pantsidepocketId . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }

//view 3

                        $glow_view_3 = $homepath . '' . $back['glow_image_view_3'];
                        $mask_view_3 = $homepath . '' . $back['mask_image_view_3'];
                        $highlight_view_3 = $homepath . '' . $back['highlighted_image_view_3'];

                        if ($back['glow_image_view_3'] && $back['mask_image_view_3'] && $back['highlighted_image_view_3']) {
//mask
                            $cmd = 'composite -compose Dst_In -gravity center ' . $mask_view_3 . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'pant_images/pantpockets/' . $fabric_id . '_pantpockets_' . $pantsidepocketId . '_view_3.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//crop
                            $cmd = 'convert ' . $homepath . 'pant_images/pantpockets/' . $fabric_id . '_pantpockets_' . $pantsidepocketId . '_view_3.png -crop 500x1320+290+0  +repage ' . $homepath . 'pant_images/pantpockets/' . $fabric_id . '_pantpockets_' . $pantsidepocketId . '_view_3.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);


//glow
                            $cmd = 'convert ' . $homepath . 'pant_images/pantpockets/' . $fabric_id . '_pantpockets_' . $pantsidepocketId . '_view_3.png ' . $glow_view_3 . ' -geometry +0+0 -composite ' . $homepath . 'pant_images/pantpockets/' . $fabric_id . '_pantpockets_' . $pantsidepocketId . '_view_3.png';

// //glow
//                             $cmd = 'composite ' . $glow_view_3 . ' -compose Multiply  ' . $homepath . 'pant_images/pantpockets/' . $fabric_id . '_pantpockets_' . $pantsidepocketId . '_view_3.png ' . $homepath . 'pant_images/pantpockets/' . $fabric_id . '_pantpockets_' . $pantsidepocketId . '_view_3.png';

                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//highlight
                            $cmd = 'composite ' . $highlight_view_3 . ' -compose Overlay  ' . $homepath . 'pant_images/pantpockets/' . $fabric_id . '_pantpockets_' . $pantsidepocketId . '_view_3.png ' . $homepath . 'pant_images/pantpockets/' . $fabric_id . '_pantpockets_' . $pantsidepocketId . '_view_3.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }
                    }

// Pant Side Pockets 

// Pant back Pockets 
                    $pantbackpocketFactory = $this->_pantbackpocketFactory->create()->getCollection();
                    $pantbackpocket_collection = $pantbackpocketFactory->getData();

                    foreach ($pantbackpocket_collection as $back) {

                        $pantbackpocketId = $back['pantbackpocket_id'];
                        $backpocket_static_id = "1";



//back right view 2
                        $glow_view_2_right = $homepath . '' . $back['glow_image_view_2_right'];
                        $mask_view_2_right = $homepath . '' . $back['mask_image_view_2_right'];
                        $highlight_view_2_right = $homepath . '' . $back['highlighted_image_view_2_right'];


                        if ($back['glow_image_view_2_right'] && $back['mask_image_view_2_right'] && $back['highlighted_image_view_2_right']) {
//mask
                            $cmd = 'composite -compose Dst_In -gravity center ' . $mask_view_2_right . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'pant_images/pantpockets/back/' . $fabric_id . '_pantpockets_' . $pantbackpocketId . '_right_view_2.png';

                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//convert
                            $cmd = 'convert ' . $homepath . 'pant_images/pantpockets/back/' . $fabric_id . '_pantpockets_' . $pantbackpocketId . '_right_view_2.png -crop 500x1320+290+0  +repage ' . $homepath . 'pant_images/pantpockets/back/' . $fabric_id . '_pantpockets_' . $pantbackpocketId . '_right_view_2.png';

                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);


//glow
                            $cmd = 'convert ' . $homepath . 'pant_images/pantpockets/back/' . $fabric_id . '_pantpockets_' . $pantbackpocketId . '_right_view_2.png ' . $glow_view_2_right . ' -geometry +0+0 -composite ' . $homepath . 'pant_images/pantpockets/back/' . $fabric_id . '_pantpockets_' . $pantbackpocketId . '_right_view_2.png';

// //glow
//                             $cmd = 'composite ' . $glow_view_2_right . ' -compose Multiply  ' . $homepath . 'pant_images/pantpockets/back/' . $fabric_id . '_pantpockets_' . $pantbackpocketId . '_right_view_2.png ' . $homepath . 'pant_images/pantpockets/back/' . $fabric_id . '_pantpockets_' . $pantbackpocketId . '_right_view_2.png';


                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//highlight
                            $cmd = 'composite ' . $highlight_view_2_right . ' -compose Overlay  ' . $homepath . 'pant_images/pantpockets/back/' . $fabric_id . '_pantpockets_' . $pantbackpocketId . '_right_view_2.png ' . $homepath . 'pant_images/pantpockets/back/' . $fabric_id . '_pantpockets_' . $pantbackpocketId . '_right_view_2.png';

                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);

                        }

//back left view 2
                        $glow_view_2_left = $homepath . '' . $back['glow_image_view_2_left'];
                        $mask_view_2_left = $homepath . '' . $back['mask_image_view_2_left'];
                        $highlight_view_2_left = $homepath . '' . $back['highlighted_image_view_2_left'];


                        if ($back['glow_image_view_2_left'] && $back['mask_image_view_2_left'] && $back['highlighted_image_view_2_left']) {
//mask
                            $cmd = 'composite -compose Dst_In -gravity center ' . $mask_view_2_left . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'pant_images/pantpockets/back/' . $fabric_id . '_pantpockets_' . $pantbackpocketId . '_left_view_2.png';

                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//convert
                            $cmd = 'convert ' . $homepath . 'pant_images/pantpockets/back/'. $fabric_id . '_pantpockets_' . $pantbackpocketId . '_left_view_2.png -crop 500x1320+290+0  +repage ' . $homepath . 'pant_images/pantpockets/back/' . $fabric_id . '_pantpockets_' . $pantbackpocketId . '_left_view_2.png';

                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);

//glow
                            $cmd = 'convert ' . $homepath . 'pant_images/pantpockets/back/' . $fabric_id . '_pantpockets_' . $pantbackpocketId . '_left_view_2.png ' . $glow_view_2_left . ' -geometry +0+0 -composite ' . $homepath . 'pant_images/pantpockets/back/' . $fabric_id . '_pantpockets_' . $pantbackpocketId . '_left_view_2.png';

// //glow
//                             $cmd = 'composite ' . $glow_view_2_left . ' -compose Multiply  ' . $homepath . 'pant_images/pantpockets/back/' . $fabric_id . '_pantpockets_' . $pantbackpocketId . '_left_view_2.png ' . $homepath . 'pant_images/pantpockets/back/' . $fabric_id . '_pantpockets_' . $pantbackpocketId . '_left_view_2.png';

                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//highlight
                            $cmd = 'composite ' . $highlight_view_2_left . ' -compose Overlay  ' . $homepath . 'pant_images/pantpockets/back/' . $fabric_id . '_pantpockets_' . $pantbackpocketId . '_left_view_2.png ' . $homepath . 'pant_images/pantpockets/back/' . $fabric_id . '_pantpockets_' . $pantbackpocketId . '_left_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);


//combine left right back pocket                            

                        if ($back['mask_image_view_2_right'] != '' && $back['mask_image_view_2_left'] != '') {
                            $cmd = 'convert ' . $homepath . 'pant_images/pantpockets/back/' . $fabric_id . '_pantpockets_' . $pantbackpocketId . '_right_view_2.png ' . $homepath . 'pant_images/pantpockets/back/' . $fabric_id . '_pantpockets_' . $pantbackpocketId . '_left_view_2.png -geometry +0+0 -composite ' . $homepath . 'pant_images/pantpockets/back/' . $fabric_id . '_pantpockets_' . $pantbackpocketId . '_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);

                            $cmd = 'rm ' . $homepath . 'pant_images/pantpockets/back/' . $fabric_id . '_pantpockets_' . $pantbackpocketId . '_right_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);

                            $cmd = 'rm ' . $homepath . 'pant_images/pantpockets/back/' . $fabric_id . '_pantpockets_' . $pantbackpocketId . '_left_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);


                        } else if ($back['mask_image_view_2_right'] == '' && $back['mask_image_view_2_left'] != '') {
                            $cmd = 'convert ' . $homepath . 'glow_mask/pantsidepocket/blank.png' . $homepath . 'pant_images/pantpockets/back/' . $fabric_id . '_pantpockets_' . $pantbackpocketId . '_left_view_2.png -geometry +0+0 -composite ' . $homepath . 'pant_images/pantpockets/back/' . $fabric_id . '_pantpockets_' . $pantbackpocketId . '_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);

                            $cmd = 'rm ' . $homepath . 'pant_images/pantpockets/back/' . $fabric_id . '_pantpockets_' . $pantbackpocketId . '_left_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        } else if ($back['mask_image_view_2_right'] != '' && $back['mask_image_view_2_left'] == '') {
                            $cmd = 'convert ' . $homepath . 'pant_images/pantpockets/back/' . $fabric_id . '_pantpockets_' . $pantbackpocketId . '_right_view_2.png ' . $homepath . 'glow_mask/pantsidepocket/blank.png  -geometry +0+0 -composite ' . $homepath . 'pant_images/pantpockets/back/' . $fabric_id . '_pantpockets_' . $pantbackpocketId . '_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);

                            $cmd = 'rm ' . $homepath . 'pant_images/pantpockets/back/' . $fabric_id . '_pantpockets_' . $pantbackpocketId . '_right_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }

              }

//view 3

                        $glow_view_3 = $homepath . '' . $back['glow_image_view_3'];
                        $mask_view_3 = $homepath . '' . $back['mask_image_view_3'];
                        $highlight_view_3 = $homepath . '' . $back['highlighted_image_view_3'];

                        if ($back['glow_image_view_3'] && $back['mask_image_view_3'] && $back['highlighted_image_view_3']) {
//mask
                            $cmd = 'composite -compose Dst_In -gravity center ' . $mask_view_3 . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'pant_images/pantpockets/back/' . $fabric_id . '_pantpockets_' . $pantbackpocketId . '_view_3.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//crop
                            $cmd = 'convert ' . $homepath . 'pant_images/pantpockets/back/' . $fabric_id . '_pantpockets_' . $pantbackpocketId . '_view_3.png -crop 500x1320+290+0  +repage ' . $homepath . 'pant_images/pantpockets/back/' . $fabric_id . '_pantpockets_' . $pantbackpocketId . '_view_3.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);


//glow
                            $cmd = 'convert ' . $homepath . 'pant_images/pantpockets/back/' . $fabric_id . '_pantpockets_' . $pantbackpocketId . '_view_3.png ' . $glow_view_3 . ' -geometry +0+0 -composite ' . $homepath . 'pant_images/pantpockets/back/' . $fabric_id . '_pantpockets_' . $pantbackpocketId . '_view_3.png';


// //glow
//                             $cmd = 'composite ' . $glow_view_3 . ' -compose Multiply  ' . $homepath . 'pant_images/pantpockets/back/' . $fabric_id . '_pantpockets_' . $pantbackpocketId . '_view_3.png ' . $homepath . 'pant_images/pantpockets/back/' . $fabric_id . '_pantpockets_' . $pantbackpocketId . '_view_3.png';

                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//highlight
                            $cmd = 'composite ' . $highlight_view_3 . ' -compose Overlay  ' . $homepath . 'pant_images/pantpockets/back/' . $fabric_id . '_pantpockets_' . $pantbackpocketId . '_view_3.png ' . $homepath . 'pant_images/pantpockets/back/' . $fabric_id . '_pantpockets_' . $pantbackpocketId . '_view_3.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }
                    }

// Pant back Pockets 


// Pant pleats 
                    $pantpleatsFactory = $this->_pantpleatsFactory->create()->getCollection();
                    $pantpleats_collection = $pantpleatsFactory->getData();

                    foreach ($pantpleats_collection as $pantpleats) {

                        $pantpleatsId = $pantpleats['pantpleats_id'];
                        $pantpleats_static_id = "1";

//view 1
                        $glow_view_1 = $homepath . '' . $pantpleats['glow_image_view_1'];
                        $mask_view_1 = $homepath . '' . $pantpleats['mask_image_view_1'];
                        $highlight_view_1 = $homepath . '' . $pantpleats['highlighted_image_view_1'];


                        if ($pantpleats['glow_image_view_1'] && $pantpleats['mask_image_view_1'] && $pantpleats['highlighted_image_view_1']) {
//mask
                            $cmd = 'composite -compose Dst_In -gravity center ' . $mask_view_1 . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'pant_images/pantpleats/' . $pantpleats_static_id . '_pantpleats_' . $pantpleatsId . '_view_1.png';

                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//convert
                            $cmd = 'convert ' . $homepath . 'pant_images/pantpleats/' . $pantpleats_static_id . '_pantpleats_' . $pantpleatsId . '_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'pant_images/pantpleats/' . $pantpleats_static_id . '_pantpleats_' . $pantpleatsId . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);

//glow
                            $cmd = 'convert ' . $homepath . 'pant_images/pantpleats/' . $pantpleats_static_id . '_pantpleats_' . $pantpleatsId . '_view_1.png ' . $glow_view_1 . ' -geometry +0+0 -composite ' . $homepath . 'pant_images/pantpleats/' . $pantpleats_static_id . '_pantpleats_' . $pantpleatsId . '_view_1.png';

// //glow
//                             $cmd = 'composite ' . $glow_view_1 . ' -compose Multiply  ' . $homepath . 'pant_images/pantpleats/' . $pantpleats_static_id . '_pantpleats_' . $pantpleatsId . '_view_1.png ' . $homepath . 'pant_images/pantpleats/' . $pantpleats_static_id . '_pantpleats_' . $pantpleatsId . '_view_1.png';

                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//highlight
                            $cmd = 'composite ' . $highlight_view_1 . ' -compose Overlay  ' . $homepath . 'pant_images/pantpleats/' . $pantpleats_static_id . '_pantpleats_' . $pantpleatsId . '_view_1.png ' . $homepath . 'pant_images/pantpleats/' . $pantpleats_static_id . '_pantpleats_' . $pantpleatsId . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }

//view 3

                        $glow_view_3 = $homepath . '' . $pantpleats['glow_image_view_3'];
                        $mask_view_3 = $homepath . '' . $pantpleats['mask_image_view_3'];
                        $highlight_view_3 = $homepath . '' . $pantpleats['highlighted_image_view_3'];

                        if ($pantpleats['glow_image_view_3'] && $pantpleats['mask_image_view_3'] && $pantpleats['highlighted_image_view_3']) {
//mask
                            $cmd = 'composite -compose Dst_In -gravity center ' . $mask_view_3 . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'pant_images/pantpleats/' . $pantpleats_static_id . '_pantpleats_' . $pantpleatsId . '_view_3.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//crop
                            $cmd = 'convert ' . $homepath . 'pant_images/pantpleats/' . $pantpleats_static_id . '_pantpleats_' . $pantpleatsId . '_view_3.png -crop 500x1320+290+0  +repage ' . $homepath . 'pant_images/pantpleats/' . $pantpleats_static_id . '_pantpleats_' . $pantpleatsId . '_view_3.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);

//glow
                            $cmd = 'convert ' . $homepath . 'pant_images/pantpleats/' . $pantpleats_static_id . '_pantpleats_' . $pantpleatsId . '_view_3.png ' . $glow_view_3 . ' -geometry +0+0 -composite ' . $homepath . 'pant_images/pantpleats/' . $pantpleats_static_id . '_pantpleats_' . $pantpleatsId . '_view_3.png';


// //glow
//                             $cmd = 'composite ' . $glow_view_3 . ' -compose Multiply  ' . $homepath . 'pant_images/pantpleats/' . $pantpleats_static_id . '_pantpleats_' . $pantpleatsId . '_view_3.png ' . $homepath . 'pant_images/pantpleats/' . $pantpleats_static_id . '_pantpleats_' . $pantpleatsId . '_view_3.png';

                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//highlight
                            $cmd = 'composite ' . $highlight_view_3 . ' -compose Overlay  ' . $homepath . 'pant_images/pantpleats/' . $pantpleats_static_id . '_pantpleats_' . $pantpleatsId . '_view_3.png ' . $homepath . 'pant_images/pantpleats/' . $pantpleats_static_id . '_pantpleats_' . $pantpleatsId . '_view_3.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }
                    }

// Pant pleats 
// Pant Waistband 
                    $waistbandFactory = $this->_waistbandFactory->create()->getCollection();
                    $waistband_collection = $waistbandFactory->getData();

                    foreach ($waistband_collection as $waistband) {

                        $waistbandId = $waistband['waistband_id'];
                        $waistband_static_id = "1";

//view 1
                        $glow_view_1 = $homepath . '' . $waistband['glow_image_view_1'];
                        $mask_view_1 = $homepath . '' . $waistband['mask_image_view_1'];
                        $highlight_view_1 = $homepath . '' . $waistband['highlighted_image_view_1'];

                        if ($waistband['glow_image_view_1'] && $waistband['mask_image_view_1'] && $waistband['highlighted_image_view_1']) {
//mask
                            $cmd = 'composite -compose Dst_In -gravity center ' . $mask_view_1 . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'pant_images/waistband/' . $fabric_id . '_waistband_' . $waistbandId . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//convert
                            $cmd = 'convert ' . $homepath . 'pant_images/waistband/' . $fabric_id . '_waistband_' . $waistbandId . '_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'pant_images/waistband/' . $fabric_id . '_waistband_' . $waistbandId . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);

//glow
                            $cmd = 'convert ' . $homepath . 'pant_images/waistband/' . $fabric_id . '_waistband_' . $waistbandId . '_view_1.png ' . $glow_view_1 . ' -geometry +0+0 -composite ' . $homepath . 'pant_images/waistband/' . $fabric_id . '_waistband_' . $waistbandId . '_view_1.png';

// //glow
//                             $cmd = 'composite ' . $glow_view_1 . ' -compose Multiply  ' . $homepath . 'pant_images/waistband/' . $fabric_id . '_waistband_' . $waistbandId . '_view_1.png ' . $homepath . 'pant_images/waistband/' . $fabric_id . '_waistband_' . $waistbandId . '_view_1.png';

                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//highlight
                            $cmd = 'composite ' . $highlight_view_1 . ' -compose Overlay  ' . $homepath . 'pant_images/waistband/' . $fabric_id . '_waistband_' . $waistbandId . '_view_1.png ' . $homepath . 'pant_images/waistband/' . $fabric_id . '_waistband_' . $waistbandId . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }
                    }
// Pant Waistband 
// Pant Coin pockets 
                    $pantcoinpocketFactory = $this->_pantcoinpocketFactory->create()->getCollection();
                    $pantCoin_collection = $pantcoinpocketFactory->getData();

                    foreach ($pantCoin_collection as $pantCoin) {

                        $vestPocketId = $pantCoin['coinpocket_id'];
                        $waistband_static_id = "1";

//view 1
                        $glow_view_1 = $homepath . '' . $pantCoin['glow_image_view_1'];
                        $mask_view_1 = $homepath . '' . $pantCoin['mask_image_view_1'];
                        $highlight_view_1 = $homepath . '' . $pantCoin['highlighted_image_view_1'];

                        if ($pantCoin['glow_image_view_1'] && $pantCoin['mask_image_view_1'] && $pantCoin['highlighted_image_view_1']) {
//mask
                            $cmd = 'composite -compose Dst_In -gravity center ' . $mask_view_1 . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'pant_images/pantcoin/' . $fabric_id . '_pantcoin_' . $vestPocketId . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//convert
                            $cmd = 'convert ' . $homepath . 'pant_images/pantcoin/' . $fabric_id . '_pantcoin_' . $vestPocketId . '_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'pant_images/pantcoin/' . $fabric_id . '_pantcoin_' . $vestPocketId . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);

//glow
                            $cmd = 'convert ' . $homepath . 'pant_images/pantcoin/' . $fabric_id . '_pantcoin_' . $vestPocketId . '_view_1.png ' . $glow_view_1 . ' -geometry +0+0 -composite ' . $homepath . 'pant_images/pantcoin/' . $fabric_id . '_pantcoin_' . $vestPocketId . '_view_1.png';

// //glow
//                             $cmd = 'composite ' . $glow_view_1 . ' -compose Multiply  ' . $homepath . 'pant_images/pantcoin/' . $fabric_id . '_pantcoin_' . $vestPocketId . '_view_1.png ' . $homepath . 'pant_images/pantcoin/' . $fabric_id . '_pantcoin_' . $vestPocketId . '_view_1.png';


                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//highlight
                            $cmd = 'composite ' . $highlight_view_1 . ' -compose Overlay  ' . $homepath . 'pant_images/pantcoin/' . $fabric_id . '_pantcoin_' . $vestPocketId . '_view_1.png ' . $homepath . 'pant_images/pantcoin/' . $fabric_id . '_pantcoin_' . $vestPocketId . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }
                    }

// Pant Coin pockets 
// Pant Belt loops 
                    $pantbeltloopsFactory = $this->_pantbeltloopsFactory->create()->getCollection();
                    $pantBeltLoops_collection = $pantbeltloopsFactory->getData();

                    foreach ($pantBeltLoops_collection as $pantBeltLoops) {
                        $pantbeltloopid = $pantBeltLoops['pantbeltloops_id'];

//view 1
                        $glow_view_1 = $homepath . '' . $pantBeltLoops['glow_image'];
                        $mask_view_1 = $homepath . '' . $pantBeltLoops['mask_image'];
                        $highlight_view_1 = $homepath . '' . $pantBeltLoops['hi_image'];

                        if ($pantBeltLoops['glow_image'] && $pantBeltLoops['mask_image'] && $pantBeltLoops['hi_image']) {
//mask
                            $cmd = 'composite -compose Dst_In -gravity center ' . $mask_view_1 . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'pant_images/pantbeltloops/' . $fabric_id . '_pantbeltloops_' . $pantbeltloopid . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//convert
                            $cmd = 'convert ' . $homepath . 'pant_images/pantbeltloops/' . $fabric_id . '_pantbeltloops_' . $pantbeltloopid . '_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'pant_images/pantbeltloops/' . $fabric_id . '_pantbeltloops_' . $pantbeltloopid . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);

//glow
                            $cmd = 'convert ' . $homepath . 'pant_images/pantbeltloops/' . $fabric_id . '_pantbeltloops_' . $pantbeltloopid . '_view_1.png ' . $glow_view_1 . ' -geometry +0+0 -composite ' . $homepath . 'pant_images/pantbeltloops/' . $fabric_id . '_pantbeltloops_' . $pantbeltloopid . '_view_1.png';

// //glow
//                             $cmd = 'composite ' . $glow_view_1 . ' -compose Multiply  ' . $homepath . 'pant_images/pantbeltloops/' . $fabric_id . '_pantbeltloops_' . $pantbeltloopid . '_view_1.png ' . $homepath . 'pant_images/pantbeltloops/' . $fabric_id . '_pantbeltloops_' . $pantbeltloopid . '_view_1.png';

                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//highlight
                            $cmd = 'composite ' . $highlight_view_1 . ' -compose Overlay  ' . $homepath . 'pant_images/pantbeltloops/' . $fabric_id . '_pantbeltloops_' . $pantbeltloopid . '_view_1.png ' . $homepath . 'pant_images/pantbeltloops/' . $fabric_id . '_pantbeltloops_' . $pantbeltloopid . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }


//view 2
                        $glow_view_2 = $homepath . '' . $pantBeltLoops['back_glow_image'];
                        $mask_view_2 = $homepath . '' . $pantBeltLoops['back_mask_image'];
                        $highlight_view_2 = $homepath . '' . $pantBeltLoops['back_hi_image'];

                        if ($pantBeltLoops['back_glow_image'] && $pantBeltLoops['back_mask_image'] && $pantBeltLoops['back_hi_image']) {
//mask
                            $cmd = 'composite -compose Dst_In -gravity center ' . $mask_view_2 . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'pant_images/pantbeltloops/' . $fabric_id . '_pantbeltloops_' . $pantbeltloopid . '_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//convert
                            $cmd = 'convert ' . $homepath . 'pant_images/pantbeltloops/' . $fabric_id . '_pantbeltloops_' . $pantbeltloopid . '_view_2.png -crop 500x1320+290+0  +repage ' . $homepath . 'pant_images/pantbeltloops/' . $fabric_id . '_pantbeltloops_' . $pantbeltloopid . '_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);

//glow
                            $cmd = 'convert ' . $homepath . 'pant_images/pantbeltloops/' . $fabric_id . '_pantbeltloops_' . $pantbeltloopid . '_view_2.png ' . $glow_view_2 . ' -geometry +0+0 -composite ' . $homepath . 'pant_images/pantbeltloops/' . $fabric_id . '_pantbeltloops_' . $pantbeltloopid . '_view_2.png';

// //glow
//                             $cmd = 'composite ' . $glow_view_2 . ' -compose Multiply  ' . $homepath . 'pant_images/pantbeltloops/' . $fabric_id . '_pantbeltloops_' . $pantbeltloopid . '_view_2.png ' . $homepath . 'pant_images/pantbeltloops/' . $fabric_id . '_pantbeltloops_' . $pantbeltloopid . '_view_2.png';


                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//highlight
                            $cmd = 'composite ' . $highlight_view_2 . ' -compose Overlay  ' . $homepath . 'pant_images/pantbeltloops/' . $fabric_id . '_pantbeltloops_' . $pantbeltloopid . '_view_2.png ' . $homepath . 'pant_images/pantbeltloops/' . $fabric_id . '_pantbeltloops_' . $pantbeltloopid . '_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }

//view 3
                        $glow_view_3 = $homepath . '' . $pantBeltLoops['folded_glow_image'];
                        $mask_view_3 = $homepath . '' . $pantBeltLoops['folded_mask_image'];
                        $highlight_view_3 = $homepath . '' . $pantBeltLoops['folded_hi_image'];

                        if ($pantBeltLoops['back_glow_image'] && $pantBeltLoops['back_mask_image'] && $pantBeltLoops['back_hi_image']) {
//mask
                            $cmd = 'composite -compose Dst_In -gravity center ' . $mask_view_3 . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'pant_images/pantbeltloops/' . $fabric_id . '_pantbeltloops_' . $pantbeltloopid . '_view_3.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//convert
                            $cmd = 'convert ' . $homepath . 'pant_images/pantbeltloops/' . $fabric_id . '_pantbeltloops_' . $pantbeltloopid . '_view_3.png -crop 500x1320+290+0  +repage ' . $homepath . 'pant_images/pantbeltloops/' . $fabric_id . '_pantbeltloops_' . $pantbeltloopid . '_view_3.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//glow
                            $cmd = 'convert ' . $homepath . 'pant_images/pantbeltloops/' . $fabric_id . '_pantbeltloops_' . $pantbeltloopid . '_view_3.png ' . $glow_view_3 . ' -geometry +0+0 -composite ' . $homepath . 'pant_images/pantbeltloops/' . $fabric_id . '_pantbeltloops_' . $pantbeltloopid . '_view_3.png';

// //glow
//                             $cmd = 'composite ' . $glow_view_3 . ' -compose Multiply  ' . $homepath . 'pant_images/pantbeltloops/' . $fabric_id . '_pantbeltloops_' . $pantbeltloopid . '_view_3.png ' . $homepath . 'pant_images/pantbeltloops/' . $fabric_id . '_pantbeltloops_' . $pantbeltloopid . '_view_3.png';


                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//highlight
                            $cmd = 'composite ' . $highlight_view_3 . ' -compose Overlay  ' . $homepath . 'pant_images/pantbeltloops/' . $fabric_id . '_pantbeltloops_' . $pantbeltloopid . '_view_3.png ' . $homepath . 'pant_images/pantbeltloops/' . $fabric_id . '_pantbeltloops_' . $pantbeltloopid . '_view_3.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }
                    }

                    $cmd = "rm " . $homepath . 'fab_suit_images/' . $fabric_id . "_pant_fabric_cuff_side.png";
                    $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($writeQuery);

                    $cmd = "rm " . $homepath . 'fab_suit_images/' . $fabric_id . "_pant_fabric_main.png";
                    $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($writeQuery);

                    $cmd = "rm " . $homepath . 'fab_suit_images/' . $fabric_id . "_pant_fabric_main_60.png";
                    $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($writeQuery);

                    $cmd = "rm " . $homepath . 'fab_suit_images/' . $fabric_id . "_pant_fabric_main_90.png";
                    $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($writeQuery);

                    $cmd = "rm " . $homepath . 'fab_suit_images/' . $fabric_id . "_pant_fabric_main_back.png";
                    $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($writeQuery);

                    $cmd = "rm " . $homepath . 'fab_suit_images/' . $fabric_id . "_pant_fabric_main_back_zoom.png";
                    $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($writeQuery);

                    $cmd = "rm " . $homepath . 'fab_suit_images/' . $fabric_id . "_pant_fabric_wave_left.png";
                    $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($writeQuery);

                    $cmd = "rm " . $homepath . 'fab_suit_images/' . $fabric_id . "_pant_fabric_wave_right.png";
                    $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($writeQuery);

                    $cmd = "rm " . $homepath . 'fab_suit_images/' . $fabric_id . "_pant_fabric_zoom.png";
                    $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($writeQuery);

                    $cmd = "rm " . $homepath . 'fab_suit_images/' . $fabric_id . "_pant_lapel_fabric_left.png";
                    $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($writeQuery);

                    $cmd = "rm " . $homepath . 'fab_suit_images/' . $fabric_id . "_pant_lapel_fabric_right.png";
                    $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($writeQuery);

                    $cmd = "rm " . $homepath . 'fab_suit_images/' . $fabric_id . "_pant_lapel_fabric_upper_left.png";
                    $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($writeQuery);

                    $cmd = "rm " . $homepath . 'fab_suit_images/' . $fabric_id . "_pant_lapel_fabric_upper_right.png";
                    $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($writeQuery);


                    $updateQuery = "UPDATE `suit_pant_fabrics` SET `status`=1 WHERE `id`=" . $fabric_id;
                    $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$updateQuery}')";
                    $this->connection->query($writeQuery);
                                       
// Pant Belt loops 
                }
                /* Suit Image Generation End */

                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['id' => $model->getId()]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the suit_pant.'));
            }
            $this->dataPersistor->set('suit_pant', $data);
            return $resultRedirect->setPath('*/*/edit', ['id' => $this->getRequest()->getParam('id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }

}
