<?php

namespace Suit\Womensuitlining\Model\ResourceModel;

class Womensuitlining extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb {

    protected function _construct() {
        $this->_init('suit_womenlining', 'id');
    }

}
