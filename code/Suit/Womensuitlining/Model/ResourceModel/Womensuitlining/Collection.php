<?php

namespace Suit\Womensuitlining\Model\ResourceModel\Womensuitlining;

use \Suit\Womensuitlining\Model\ResourceModel\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'id';

    /**
     * Load data for preview flag
     *
     * @var bool
     */
    protected $_previewFlag;

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Suit\Womensuitlining\Model\Womensuitlining', 'Suit\Womensuitlining\Model\ResourceModel\Womensuitlining');
        $this->_map['fields']['id'] = 'main_table.id';
    }
}
