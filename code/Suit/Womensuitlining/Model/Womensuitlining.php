<?php

namespace Suit\Womensuitlining\Model;

class Womensuitlining extends \Magento\Framework\Model\AbstractModel {

    protected function _construct() {
        $this->_init('Suit\Womensuitlining\Model\ResourceModel\Womensuitlining');
    }

    public function getAvailableStatuses() {


        $availableOptions = ['1' => 'Enable',
            '0' => 'Disable'];

        return $availableOptions;
    }

}
