<?php

namespace Suit\Womensuitlining\Controller\Adminhtml\Index;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Index extends \Magento\Backend\App\Action {

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
    Context $context, PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * Check the permission to run it
     *
     * @return bool
     */
    protected function _isAllowed() {
        return $this->_authorization->isAllowed('Suit_Womensuitlining::suit_womensuitlining');
    }

    /**
     * Index action
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute() {

        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Suit_Womensuitlining::suit_womensuitlining');
        $resultPage->addBreadcrumb(__('Womensuitlining'), __('Womensuitlining'));
        $resultPage->addBreadcrumb(__('Suit Lining fabric'), __('Manage Suit Lining Fabrics'));
        $resultPage->getConfig()->getTitle()->prepend(__('Manage Suit lining Fabrics'));

        return $resultPage;
    }

}
