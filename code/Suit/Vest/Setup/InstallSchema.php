<?php

/*
 * Suit_Vest

 * @category   Suit
 * @package    Suit_Vest
 * @copyright  Copyright (c) 2018 Scott Parsons
 * @license    https://github.com/ScottParsons/module-vestuicomponent/blob/master/LICENSE.md
 * @version    1.0.0
 */

namespace Suit\Vest\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class InstallSchema implements InstallSchemaInterface {

    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context) {
        $installer = $setup;
        $installer->startSetup();
        $tableName = $installer->getTable('suit_vest_fabrics');

        if (!$installer->tableExists('suit_vest_fabrics')) {
            $table = $installer->getConnection()
                    ->newTable($tableName)
                    ->addColumn(
                            'id', Table::TYPE_INTEGER, null, [
                        'identity' => true,
                        'unsigned' => true,
                        'nullable' => false,
                        'auto_increment' => true,
                        'primary' => true
                            ], 'ID'
                    )
                    ->addColumn(
                            'name', Table::TYPE_TEXT, 255, ['nullable' => false, 'default' => ''], 'fabric name'
                    )
                    ->addColumn(
                            'fabric_thumb', Table::TYPE_TEXT, 255, ['nullable' => false, 'default' => ''], 'generating fabric images'
                    )
                    ->addColumn(
                            'display_fabric_thumb', Table::TYPE_TEXT, 255, ['nullable' => false, 'default' => ''], 'Display fabric thumbnail'
                    )
                    ->addColumn(
                            'fabric_large_image', Table::TYPE_TEXT, 255, ['nullable' => false, 'default' => ''], 'Display fabric on vest'
                    )
                    ->addColumn(
                            'price', Table::TYPE_FLOAT, null, ['unsigned' => true, 'nullable' => false, 'default' => '0'], 'fabric price'
                    )
                    ->addColumn(
                            'material_id', Table::TYPE_SMALLINT, null, ['unsigned' => true, 'nullable' => false, 'default' => '0'], 'material_id'
                    )
                    ->addColumn(
                            'pattern_id', Table::TYPE_SMALLINT, null, ['unsigned' => true, 'nullable' => false, 'default' => '0'], 'pattern_id'
                    )
                    ->addColumn(
                            'season_id', Table::TYPE_SMALLINT, null, ['unsigned' => true, 'nullable' => false, 'default' => '0'], 'season_id'
                    )
                    ->addColumn(
                            'color_id', Table::TYPE_SMALLINT, null, ['unsigned' => true, 'nullable' => false, 'default' => '0'], 'color_id'
                    )
                    ->addColumn(
                            'category_id', Table::TYPE_SMALLINT, null, ['unsigned' => true, 'nullable' => false, 'default' => '0'], 'category_id'
                    )
                    ->addColumn(
                            'status', Table::TYPE_SMALLINT, null, ['unsigned' => true, 'nullable' => false, 'default' => '0'], 'Status'
                    )
                    ->addColumn(
                            'created_at', Table::TYPE_TIMESTAMP, null, ['nullable' => false, 'default' => Table::TIMESTAMP_INIT], 'Created At'
                    )
                    ->addColumn(
                    'updated_at', Table::TYPE_TIMESTAMP, null, ['nullable' => false, 'default' => Table::TIMESTAMP_INIT], 'Updated At'
            );
            $installer->getConnection()->createTable($table);
        }
        $installer->endSetup();
    }

}
