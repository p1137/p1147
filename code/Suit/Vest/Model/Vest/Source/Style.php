<?php
/**
* @author Dhirajkumar Deore
* Copyright © 2018 Magento. All rights reserved.
* See COPYING.txt for license details.
*/
namespace Suit\vest\Model\vest\Source;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class Status
 */
class Style implements OptionSourceInterface
{

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {

        return [
            ['value' => '1', 'label' => __('Single Breasted')],
            ['value' => '2', 'label' => __('Double breasted')],
            ['value' => '3', 'label' => __('StitMandarinch')]
        ];
    }
}
