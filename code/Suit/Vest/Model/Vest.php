<?php

/**
 * @author Dhirajkumar Deore
 * Copyright © 2018 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Suit\Vest\Model;

class Vest extends \Magento\Framework\Model\AbstractModel {

    protected function _construct() {
        $this->_init('Suit\Vest\Model\ResourceModel\Vest');
    }

    public function getAvailableStatuses() {


        $availableOptions = ['1' => 'Enable',
            '0' => 'Disable'];

        return $availableOptions;
    }

}
