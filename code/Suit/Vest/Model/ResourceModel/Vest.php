<?php

/**
 * @author Dhirajkumar Deore
 * Copyright © 2018 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Suit\Vest\Model\ResourceModel;

class Vest extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb {

    protected function _construct() {
        $this->_init('suit_vest_fabrics', 'id');
    }

}
