<?php

/**
 * @author Akshay Shelke    
 * Copyright © 2018 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Suit\Vest\Model\ResourceModel\Veststyleedge;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection {

    protected function _construct() {
        $this->_init('Suit\Vest\Model\Veststyleedge', 'Suit\Vest\Model\ResourceModel\Veststyleedge');
    }

}
