<?php

/**
 * @author Dhirajkumar Deore
 * Copyright © 2018 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Suit\Vest\Controller\Adminhtml\Index;

use Magento\Backend\App\Action\Context;
use Magento\Backend\App\Action;
use Suit\Vest\Model\Vest;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\TestFramework\Inspection\Exception;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\RequestInterface;

//use Magento\Framework\Stdlib\DateTime\DateTime;
//use Magento\Ui\Component\MassAction\Filter;
//use FME\News\Model\ResourceModel\Test\CollectionFactory;

class Save extends \Magento\Backend\App\Action {

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;
    protected $scopeConfig;
    protected $_escaper;
    protected $inlineTranslation;
    protected $_dateFactory;
    protected $_vestpocketFactory;
    protected $_veststyleedgeFactory;
    protected $_vestlapelsFactory;
    protected $objectManager;
    protected $connection;
    protected $directory;
    protected $rootPath1;
    protected $rootPath;

    /**
     * @param Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param DataPersistorInterface $dataPersistor
     */
    public function __construct(
    Context $context, DataPersistorInterface $dataPersistor, \Magento\Framework\Escaper $escaper, \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Magento\Framework\Stdlib\DateTime\DateTimeFactory $dateFactory, \Suit\Vest\Model\VestpocketFactory $vestpocketFactory, \Suit\Vest\Model\VeststyleedgeFactory $veststyleedgeFactory, \Suit\Vest\Model\VestlapelsFactory $vestlapelsFactory
    ) {

        $this->_vestpocketFactory = $vestpocketFactory;
        $this->_veststyleedgeFactory = $veststyleedgeFactory;
        $this->_vestlapelsFactory = $vestlapelsFactory;
        $this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->connection = $this->objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $this->directory = $this->objectManager->get('\Magento\Framework\Filesystem\DirectoryList');
        $this->rootPath1 = $this->directory->getRoot();
        $this->rootPath = $this->rootPath1 . "/pub/media/suit-tool/";
        $this->dataPersistor = $dataPersistor;
        $this->scopeConfig = $scopeConfig;
        $this->_escaper = $escaper;
        $this->_dateFactory = $dateFactory;
        $this->inlineTranslation = $inlineTranslation;
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute() {

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();


        if ($data) {

            $id = $this->getRequest()->getParam('id');

            if (isset($data['status']) && $data['status'] === 'true') {
                $data['status'] = Block::STATUS_ENABLED;
            }
            if (empty($data['id'])) {
                $data['id'] = null;
            }


            /** @var \Magento\Cms\Model\Block $model */
            $model = $this->_objectManager->create('Suit\Vest\Model\Vest')->load($id);
            if (!$model->getId() && $id) {
                $this->messageManager->addError(__('This suit_vest no longer exists.'));
                return $resultRedirect->setPath('*/*/');
            }


            if (isset($data['fabric_thumb'][0]['name']) && isset($data['fabric_thumb'][0]['tmp_name'])) {
                $data['fabric_thumb'] = 'suit-vest/fab_suit_images/thumbs/' . $data['fabric_thumb'][0]['name'];
            } elseif (isset($data['fabric_thumb'][0]['name']) && !isset($data['image'][0]['tmp_name'])) {
                $data['fabric_thumb'] = $data['fabric_thumb'][0]['name'];
            } else {
                $data['fabric_thumb'] = null;
            }

            if (isset($data['display_fabric_thumb'][0]['name']) && isset($data['display_fabric_thumb'][0]['tmp_name'])) {
                $data['display_fabric_thumb'] = 'suit-vest/fab_suit_images/thumbs/' . $data['display_fabric_thumb'][0]['name'];
            } elseif (isset($data['display_fabric_thumb'][0]['name']) && !isset($data['image'][0]['tmp_name'])) {
                $data['display_fabric_thumb'] = $data['display_fabric_thumb'][0]['name'];
            } else {
                $data['display_fabric_thumb'] = null;
            }

            if (isset($data['fabric_large_image'][0]['name']) && isset($data['fabric_large_image'][0]['tmp_name'])) {
                $data['fabric_large_image'] = 'suit-vest/fab_suit_images/thumbs/' . $data['fabric_large_image'][0]['name'];
            } elseif (isset($data['fabric_large_image'][0]['name']) && !isset($data['image'][0]['tmp_name'])) {
                $data['fabric_large_image'] = $data['fabric_large_image'][0]['name'];
            } else {
                $data['fabric_large_image'] = null;
            }
            $data['status'] = 0;
            $model->setData($data);

            $this->inlineTranslation->suspend();
            try {
                $model->save();
                $this->messageManager->addSuccess(__('suit_vest Saved successfully'));
                $this->dataPersistor->clear('suit_vest');


                /* Suit Image Generation Start */
                if ($data['fabric_thumb'] != '') {
                    $tableName = $this->connection->getTableName('generation_cmd');
                    /* color image creation start */
                    $fabric_id = $model->getId();

                    $homepath = $this->rootPath;

                    $fabricThumbImgpath = $this->rootPath1 . '/pub/media/' . $data['fabric_thumb'];
                    $fabricName = $fabric_id . "_vest_fabric_main.png";
                    $fabric_0 = $homepath . 'fab_suit_images/' . $fabricName;

//back

                    $fabricName_90 = $fabric_id . "_vest_fabric_main_90.png";
                    $fabric_90 = $homepath . 'fab_suit_images/' . $fabricName_90;

//for zoom view
                    $fabric_zoom = $homepath . "fab_suit_images/" . $fabric_id . "_vest_fabric_zoom.png";
                    $shirtFabImgPath = $homepath . 'fab_suit_images/';

                    exec('convert ' . $fabricThumbImgpath . ' -rotate 90 ' . $fabric_90);

                    exec('convert -size 1080x1320 tile:' . $fabric_90 . ' ' . $shirtFabImgPath . $fabric_id . '_vest_fabric_main_back.png');
                    exec('convert -size 1080x1320 tile:' . $fabric_90 . ' ' . $fabric_90);

                    $fabricName_60 = $fabric_id . "_vest_fabric_main_60.png";
                    $fabric_60 = $homepath . 'fab_suit_images/' . $fabricName_60;



                    exec('convert -size 1080x1320 tile:' . $fabricThumbImgpath . ' ' . $fabric_0);
//created for collar

                    exec('convert ' . $fabric_0 . ' -background "rgba(0,0,0,0.5)" -distort SRT -67 ' . $fabric_60);

                    exec('convert ' . $fabric_0 . ' -background "rgba(0,0,0,0.5)" -distort SRT 80 ' . $shirtFabImgPath . $fabric_id . '_vest_fabric_cuff_side.png');

//zoom view fabirc
                    exec('convert -size 1200x900 tile:' . $fabricThumbImgpath . ' ' . $fabric_zoom);
//created for collar
                    exec('convert ' . $fabric_zoom . ' -background "rgba(0,0,0,0.5)" -distort SRT 90 ' . $shirtFabImgPath . $fabric_id . '_vest_fabric_main_back_zoom.png');

                    /* END create fabric image */

                    /* Create plain fabric image */




                    $lapel_fabric_left = $homepath . 'fab_suit_images/' . $fabric_id . '_vest_lapel_fabric_left.png';
                    $lapel_fabric_right = $homepath . 'fab_suit_images/' . $fabric_id . '_vest_lapel_fabric_right.png';
                    exec('convert ' . $fabric_0 . ' -background "rgba(0,0,0,0.5)" -distort SRT 15 ' . $lapel_fabric_left);
                    exec('convert ' . $fabric_0 . ' -background "rgba(0,0,0,0.5)" -distort SRT -15 ' . $lapel_fabric_right);


                    $lapel_fabric_upper_left = $homepath . 'fab_suit_images/' . $fabric_id . '_vest_lapel_fabric_upper_left.png';
                    $lapel_fabric_upper_right = $homepath . 'fab_suit_images/' . $fabric_id . '_vest_lapel_fabric_upper_right.png';
                    exec('convert ' . $fabric_0 . ' -background "rgba(0,0,0,0.5)" -distort SRT -45 ' . $lapel_fabric_upper_left);
                    exec('convert ' . $fabric_0 . ' -background "rgba(0,0,0,0.5)" -distort SRT 45 ' . $lapel_fabric_upper_right);


                    $fabricName_wave_left = $fabric_id . "_vest_fabric_wave_left.png";
                    $fabricName_wave_right = $fabric_id . "_vest_fabric_wave_right.png";

                    $wave_left = $homepath . 'fab_suit_images/' . $fabricName_wave_left;
                    $wave_right = $homepath . 'fab_suit_images/' . $fabricName_wave_right;

                    exec('convert ' . $fabric_0 . ' -background "rgba(0,0,0,0.5)" -distort SRT -5 ' . $wave_left);
                    exec('convert ' . $fabric_0 . ' -background "rgba(0,0,0,0.5)" -distort SRT 5 ' . $wave_right);


// MEN //
// Vest pockets 
                    $vestpocketFactory = $this->_vestpocketFactory->create()->getCollection();
                    $vestPocket_collection = $vestpocketFactory->getData();

                    foreach ($vestPocket_collection as $vestPocket) {
                        $vestPocketId = $vestPocket['vestpocket_id'];

//view 1
                        $glow_view_1 = $homepath . '' . $vestPocket['glow_image'];
                        $mask_view_1 = $homepath . '' . $vestPocket['mask_image'];
                        $highlight_view_1 = $homepath . '' . $vestPocket['hi_image'];



                        if ($vestPocket['glow_image'] && $vestPocket['mask_image'] && $vestPocket['hi_image']) {
//mask
                            $cmd = 'composite -compose Dst_In -gravity center ' . $mask_view_1 . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'vest_images/vestpocket/' . $fabric_id . '_vestpocket_' . $vestPocketId . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//convert
                            $cmd = 'convert ' . $homepath . 'vest_images/vestpocket/' . $fabric_id . '_vestpocket_' . $vestPocketId . '_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'vest_images/vestpocket/' . $fabric_id . '_vestpocket_' . $vestPocketId . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//glow
                            $cmd = 'convert ' . $homepath . 'vest_images/vestpocket/' . $fabric_id . '_vestpocket_' . $vestPocketId . '_view_1.png ' . $glow_view_1 . ' -geometry +0+0 -composite ' . $homepath . 'vest_images/vestpocket/' . $fabric_id . '_vestpocket_' . $vestPocketId . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//highlight
                            $cmd = 'composite ' . $highlight_view_1 . ' -compose Overlay  ' . $homepath . 'vest_images/vestpocket/' . $fabric_id . '_vestpocket_' . $vestPocketId . '_view_1.png ' . $homepath . 'vest_images/vestpocket/' . $fabric_id . '_vestpocket_' . $vestPocketId . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }
                    }

//breast pocket
                    $glow_view_1 = $homepath . 'glow_mask/vestpocket/Vest_Breast-Pocket_shad.png';
                    $mask_view_1 = $homepath . 'glow_mask/vestpocket/Vest_Breast-Pocket_mask.png';
                    $highlight_view_1 = $homepath . 'glow_mask/vestpocket/Vest_Breast-Pocket_hi.png';

                    if ($glow_view_1 && $mask_view_1 && $highlight_view_1) {
//mask
                        $cmd = 'composite -compose Dst_In -gravity center ' . $mask_view_1 . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'vest_images/vestpocket/' . $fabric_id . '_breastpocket_view_1.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);
//convert
                        $cmd = 'convert ' . $homepath . 'vest_images/vestpocket/' . $fabric_id . '_breastpocket_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'vest_images/vestpocket/' . $fabric_id . '_breastpocket_view_1.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);
//glow
                        $cmd = 'convert ' . $homepath . 'vest_images/vestpocket/' . $fabric_id . '_breastpocket_view_1.png ' . $glow_view_1 . ' -geometry +0+0 -composite ' . $homepath . 'vest_images/vestpocket/' . $fabric_id . '_breastpocket_view_1.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);
//highlight
                        $cmd = 'composite ' . $highlight_view_1 . ' -compose Overlay  ' . $homepath . 'vest_images/vestpocket/' . $fabric_id . '_breastpocket_view_1.png ' . $homepath . 'vest_images/vestpocket/' . $fabric_id . '_breastpocket_view_1.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);
                    }


// Vest pockets  
// Vest Styles 
                    $veststyleedgeFactory = $this->_veststyleedgeFactory->create()->getCollection();
                    $vestStyle_collection = $veststyleedgeFactory->getData();

                    $back_glow_view_1 = $homepath . 'glow_mask/vestedge/Back_Vest_shad.png';
                    $back_mask_view_1 = $homepath . 'glow_mask/vestedge/Back_Vest_mask.png';
                    $back_highlight_view_1 = $homepath . 'glow_mask/vestedge/Back_Vest_hi.png';

                    foreach ($vestStyle_collection as $vestStyle) {
                        $vestStyleId = $vestStyle['veststyleedge_id'];
                        $styleId = $vestStyle['style'];
                        $edgeId = $vestStyle['edge'];

//view 1
                        $glow_view_1 = $homepath . '' . $vestStyle['glow_image'];
                        $mask_view_1 = $homepath . '' . $vestStyle['mask_image'];
                        $highlight_view_1 = $homepath . '' . $vestStyle['hi_image'];

                        if ($vestStyle['glow_image'] && $vestStyle['mask_image'] && $vestStyle['hi_image']) {
//mask
                            $cmd = 'composite -compose Dst_In -gravity center ' . $mask_view_1 . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'vest_images/veststyle/' . $fabric_id . '_style_' . $styleId . '_bottom_' . $edgeId . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//convert
                            $cmd = 'convert ' . $homepath . 'vest_images/veststyle/' . $fabric_id . '_style_' . $styleId . '_bottom_' . $edgeId . '_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'vest_images/veststyle/' . $fabric_id . '_style_' . $styleId . '_bottom_' . $edgeId . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//glow
                            $cmd = 'convert ' . $homepath . 'vest_images/veststyle/' . $fabric_id . '_style_' . $styleId . '_bottom_' . $edgeId . '_view_1.png ' . $glow_view_1 . ' -geometry +0+0 -composite ' . $homepath . 'vest_images/veststyle/' . $fabric_id . '_style_' . $styleId . '_bottom_' . $edgeId . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//highlight
                            $cmd = 'composite ' . $highlight_view_1 . ' -compose Overlay  ' . $homepath . 'vest_images/veststyle/' . $fabric_id . '_style_' . $styleId . '_bottom_' . $edgeId . '_view_1.png ' . $homepath . 'vest_images/veststyle/' . $fabric_id . '_style_' . $styleId . '_bottom_' . $edgeId . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);


//mask
                            $cmd = 'composite -compose Dst_In -gravity center ' . $back_mask_view_1 . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'vest_images/veststyle/' . $fabric_id . '_style_' . $styleId . '_bottom_' . $edgeId . '_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//convert
                            $cmd = 'convert ' . $homepath . 'vest_images/veststyle/' . $fabric_id . '_style_' . $styleId . '_bottom_' . $edgeId . '_view_2.png -crop 500x1320+290+0  +repage ' . $homepath . 'vest_images/veststyle/' . $fabric_id . '_style_' . $styleId . '_bottom_' . $edgeId . '_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//glow
                            $cmd = 'convert ' . $homepath . 'vest_images/veststyle/' . $fabric_id . '_style_' . $styleId . '_bottom_' . $edgeId . '_view_2.png ' . $back_glow_view_1 . ' -geometry +0+0 -composite ' . $homepath . 'vest_images/veststyle/' . $fabric_id . '_style_' . $styleId . '_bottom_' . $edgeId . '_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//highlight
                            $cmd = 'composite ' . $back_highlight_view_1 . ' -compose Overlay  ' . $homepath . 'vest_images/veststyle/' . $fabric_id . '_style_' . $styleId . '_bottom_' . $edgeId . '_view_2.png ' . $homepath . 'vest_images/veststyle/' . $fabric_id . '_style_' . $styleId . '_bottom_' . $edgeId . '_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);


// Inner Lining

                            $glow_innerlining_view_2 = $homepath . 'glow_mask/lining/VestLining_shad.png';
                            $mask_innerlining_view_2 = $homepath . 'glow_mask/lining/VestLining_mask.png';
                            $highlight_innerlining_view_2 = $homepath . 'glow_mask/lining/VestLining_hi.png';

                            if ($glow_innerlining_view_2 != '' && $mask_innerlining_view_2 != '' && $highlight_innerlining_view_2 != '') {
//mask
                                $cmd = 'composite -compose Dst_In -gravity center ' . $mask_innerlining_view_2 . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'vest_images/vestlining/' . $fabric_id . '_vestlining_view_3.png';
                                $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                                $this->connection->query($writeQuery);
//convert
                                $cmd = 'convert ' . $homepath . 'vest_images/vestlining/' . $fabric_id . '_vestlining_view_3.png -crop 500x1320+290+0  +repage ' . $homepath . 'vest_images/vestlining/' . $fabric_id . '_vestlining_view_3.png';
                                $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                                $this->connection->query($writeQuery);
//glow
                                $cmd = 'convert ' . $homepath . 'vest_images/vestlining/' . $fabric_id . '_vestlining_view_3.png ' . $glow_innerlining_view_2 . ' -geometry +0+0 -composite ' . $homepath . 'vest_images/vestlining/' . $fabric_id . '_vestlining_view_3.png';
                                $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                                $this->connection->query($writeQuery);
//highlight
                                $cmd = 'composite ' . $highlight_innerlining_view_2 . ' -compose Overlay  ' . $homepath . 'vest_images/vestlining/' . $fabric_id . '_vestlining_view_3.png ' . $homepath . 'vest_images/vestlining/' . $fabric_id . '_vestlining_view_3.png';
                                $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                                $this->connection->query($writeQuery);
                            }


// Inner Collar

                            $glow_innerlining_view_2 = $homepath . 'glow_mask/lining/VestCollar_shad.png';
                            $mask_innerlining_view_2 = $homepath . 'glow_mask/lining/VestCollar_mask.png';
                            $highlight_innerlining_view_2 = $homepath . 'glow_mask/lining/VestCollar_hi.png';

                            if ($glow_innerlining_view_2 != '' && $mask_innerlining_view_2 != '' && $highlight_innerlining_view_2 != '') {
//mask
                                $cmd = 'composite -compose Dst_In -gravity center ' . $mask_innerlining_view_2 . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'vest_images/vestlining/' . $fabric_id . '_vestcollar_view_3.png';
                                $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                                $this->connection->query($writeQuery);
//convert
                                $cmd = 'convert ' . $homepath . 'vest_images/vestlining/' . $fabric_id . '_vestcollar_view_3.png -crop 500x1320+290+0  +repage ' . $homepath . 'vest_images/vestlining/' . $fabric_id . '_vestcollar_view_3.png';
                                $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                                $this->connection->query($writeQuery);
//glow
                                $cmd = 'convert ' . $homepath . 'vest_images/vestlining/' . $fabric_id . '_vestcollar_view_3.png ' . $glow_innerlining_view_2 . ' -geometry +0+0 -composite ' . $homepath . 'vest_images/vestlining/' . $fabric_id . '_vestcollar_view_3.png';
                                $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                                $this->connection->query($writeQuery);
//highlight
                                $cmd = 'composite ' . $highlight_innerlining_view_2 . ' -compose Overlay  ' . $homepath . 'vest_images/vestlining/' . $fabric_id . '_vestcollar_view_3.png ' . $homepath . 'vest_images/vestlining/' . $fabric_id . '_vestcollar_view_3.png';
                                $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                                $this->connection->query($writeQuery);
                            }
                        }
                    }

// Vest Styles 
// Vest lapels 
                    $vestlapelsFactory = $this->_vestlapelsFactory->create()->getCollection();
                    $vestLapel_collection = $vestlapelsFactory->getData();

                    foreach ($vestLapel_collection as $vestLapel) {
                        $vestLapelId = $vestLapel['vestlapels_id'];
                        $styleId = $vestLapel['style'];
                        $lapelId = $vestLapel['lapel'];

//view UL
                        $glow_view_UL = $homepath . '' . $vestLapel['upper_left_glow_image'];
                        $mask_view_UL = $homepath . '' . $vestLapel['upper_left_mask_image'];
                        $highlight_view_UL = $homepath . '' . $vestLapel['upper_left_hi_image'];

                        if ($vestLapel['upper_left_glow_image'] && $vestLapel['upper_left_mask_image'] && $vestLapel['upper_left_hi_image']) {
//mask
                            $cmd = 'composite -compose Dst_In -gravity center ' . $mask_view_UL . ' ' . $lapel_fabric_upper_left . ' -alpha Set ' . $homepath . 'vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_upper_left_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//convert
                            $cmd = 'convert ' . $homepath . 'vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_upper_left_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_upper_left_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//glow
                            $cmd = 'convert ' . $homepath . 'vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_upper_left_view_1.png ' . $glow_view_UL . ' -geometry +0+0 -composite ' . $homepath . 'vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_upper_left_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//highlight
                            $cmd = 'composite ' . $highlight_view_UL . ' -compose Overlay  ' . $homepath . 'vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_upper_left_view_1.png ' . $homepath . 'vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_upper_left_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }

//view UR
                        $glow_view_UR = $homepath . '' . $vestLapel['upper_right_glow_image'];
                        $mask_view_UR = $homepath . '' . $vestLapel['upper_right_mask_image'];
                        $highlight_view_UR = $homepath . '' . $vestLapel['upper_right_hi_image'];

                        if ($vestLapel['upper_right_glow_image'] && $vestLapel['upper_right_mask_image'] && $vestLapel['upper_right_hi_image']) {
//mask
                            $cmd = 'composite -compose Dst_In -gravity center ' . $mask_view_UR . ' ' . $lapel_fabric_upper_right . ' -alpha Set ' . $homepath . 'vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_upper_right_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//convert
                            $cmd = 'convert ' . $homepath . 'vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_upper_right_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_upper_right_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//glow
                            $cmd = 'convert ' . $homepath . 'vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_upper_right_view_1.png ' . $glow_view_UR . ' -geometry +0+0 -composite ' . $homepath . 'vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_upper_right_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//highlight
                            $cmd = 'composite ' . $highlight_view_UR . ' -compose Overlay  ' . $homepath . 'vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_upper_right_view_1.png ' . $homepath . 'vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_upper_right_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }

//view LL
                        $glow_view_LL = $homepath . '' . $vestLapel['lower_left_glow_image'];
                        $mask_view_LL = $homepath . '' . $vestLapel['lower_left_mask_image'];
                        $highlight_view_LL = $homepath . '' . $vestLapel['lower_left_hi_image'];

                        if ($vestLapel['lower_left_glow_image'] && $vestLapel['lower_left_mask_image'] && $vestLapel['lower_left_hi_image']) {
//mask
                            $cmd = 'composite -compose Dst_In -gravity center ' . $mask_view_LL . ' ' . $lapel_fabric_left . ' -alpha Set ' . $homepath . 'vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_lower_left_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//convert
                            $cmd = 'convert ' . $homepath . 'vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_lower_left_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_lower_left_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//glow
                            $cmd = 'convert ' . $homepath . 'vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_lower_left_view_1.png ' . $glow_view_LL . ' -geometry +0+0 -composite ' . $homepath . 'vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_lower_left_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//highlight
                            $cmd = 'composite ' . $highlight_view_LL . ' -compose Overlay  ' . $homepath . 'vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_lower_left_view_1.png ' . $homepath . 'vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_lower_left_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }

//view LR
                        $glow_view_LR = $homepath . '' . $vestLapel['lower_right_glow_image'];
                        $mask_view_LR = $homepath . '' . $vestLapel['lower_right_mask_image'];
                        $highlight_view_LR = $homepath . '' . $vestLapel['lower_right_hi_image'];

                        if ($vestLapel['lower_right_glow_image'] && $vestLapel['lower_right_mask_image'] && $vestLapel['lower_right_hi_image']) {
//mask
                            $cmd = 'composite -compose Dst_In -gravity center ' . $mask_view_LR . ' ' . $lapel_fabric_right . ' -alpha Set ' . $homepath . 'vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_lower_right_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//convert
                            $cmd = 'convert ' . $homepath . 'vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_lower_right_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_lower_right_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//glow
                            $cmd = 'convert ' . $homepath . 'vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_lower_right_view_1.png ' . $glow_view_LR . ' -geometry +0+0 -composite ' . $homepath . 'vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_lower_right_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//highlight
                            $cmd = 'composite ' . $highlight_view_LR . ' -compose Overlay  ' . $homepath . 'vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_lower_right_view_1.png ' . $homepath . 'vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_lower_right_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }


                        $cmd = 'convert ' . $homepath . 'vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_upper_left_view_1.png ' . $homepath . 'vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_upper_right_view_1.png -geometry +0+0 -composite ' . $homepath . 'vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_view_1.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);

//                        if (file_exists($homepath . 'vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_lower_left_view_1.png')) {
                        $cmd = 'convert ' . $homepath . 'vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_lower_left_view_1.png ' . $homepath . 'vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_view_1.png -geometry +0+0 -composite ' . $homepath . 'vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_view_1.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);
//                        }
//                        if (file_exists($homepath . 'vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_lower_right_view_1.png')) {
                        $cmd = 'convert ' . $homepath . 'vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_lower_right_view_1.png ' . $homepath . 'vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_view_1.png -geometry +0+0 -composite ' . $homepath . 'vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_view_1.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);
//                        }

                        $cmd = "rm " . $homepath . 'vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_upper_left_view_1.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);

                        $cmd = "rm " . $homepath . 'vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_upper_right_view_1.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);

                        $cmd = "rm " . $homepath . 'vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_lower_left_view_1.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);

                        $cmd = "rm " . $homepath . 'vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_lower_right_view_1.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);
                    }
//                    Vest lapels

                    $cmd = "rm " . $homepath . 'fab_suit_images/' . $fabric_id . "_vest_fabric_cuff_side.png";
                    $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($writeQuery);

                    $cmd = "rm " . $homepath . 'fab_suit_images/' . $fabric_id . "_vest_fabric_main.png";
                    $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($writeQuery);

                    $cmd = "rm " . $homepath . 'fab_suit_images/' . $fabric_id . "_vest_fabric_main_60.png";
                    $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($writeQuery);

                    $cmd = "rm " . $homepath . 'fab_suit_images/' . $fabric_id . "_vest_fabric_main_90.png";
                    $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($writeQuery);

                    $cmd = "rm " . $homepath . 'fab_suit_images/' . $fabric_id . "_vest_fabric_main_back.png";
                    $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($writeQuery);

                    $cmd = "rm " . $homepath . 'fab_suit_images/' . $fabric_id . "_vest_fabric_main_back_zoom.png";
                    $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($writeQuery);

                    $cmd = "rm " . $homepath . 'fab_suit_images/' . $fabric_id . "_vest_fabric_wave_left.png";
                    $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($writeQuery);

                    $cmd = "rm " . $homepath . 'fab_suit_images/' . $fabric_id . "_vest_fabric_wave_right.png";
                    $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($writeQuery);

                    $cmd = "rm " . $homepath . 'fab_suit_images/' . $fabric_id . "_vest_fabric_zoom.png";
                    $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($writeQuery);

                    $cmd = "rm " . $homepath . 'fab_suit_images/' . $fabric_id . "_vest_lapel_fabric_left.png";
                    $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($writeQuery);

                    $cmd = "rm " . $homepath . 'fab_suit_images/' . $fabric_id . "_vest_lapel_fabric_right.png";
                    $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($writeQuery);

                    $cmd = "rm " . $homepath . 'fab_suit_images/' . $fabric_id . "_vest_lapel_fabric_upper_left.png";
                    $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($writeQuery);

                    $cmd = "rm " . $homepath . 'fab_suit_images/' . $fabric_id . "_vest_lapel_fabric_upper_right.png";
                    $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($writeQuery);

                    $updateQuery = "UPDATE `suit_vest_fabrics` SET `status`=1 WHERE `id`=" . $fabric_id;
                    $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$updateQuery}')";
                    $this->connection->query($writeQuery);
                }
                /* Suit Image Generation End */

                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['id' => $model->getId()]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the suit_vest.'));
            }
            $this->dataPersistor->set('suit_vest', $data);
            return $resultRedirect->setPath('*/*/edit', ['id' => $this->getRequest()->getParam('id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }

}
