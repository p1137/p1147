<?php

/**
 * @author Dhirajkumar Deore
 * Copyright © 2018 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Suit\Threadfabricwomen\Controller\Adminhtml\Index;

use Magento\Backend\App\Action\Context;
use Magento\Backend\App\Action;
use Suit\Threadfabricwomen\Model\Threadfabricwomen;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\TestFramework\Inspection\Exception;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\RequestInterface;

//use Magento\Framework\Stdlib\DateTime\DateTime;
//use Magento\Ui\Component\MassAction\Filter;
//use FME\News\Model\ResourceModel\Test\CollectionFactory;

class Save extends \Magento\Backend\App\Action {

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;
    protected $scopeConfig;
    protected $_escaper;
    protected $inlineTranslation;
    protected $_dateFactory;
    protected $objectManager;
    protected $connection;
    protected $directory;
    protected $rootPath1;
    protected $rootPath;
    protected $mediaURL;

    /**
     * @param Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param DataPersistorInterface $dataPersistor
     */
    public function __construct(
    Context $context, DataPersistorInterface $dataPersistor, \Magento\Framework\Escaper $escaper, \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Magento\Framework\Stdlib\DateTime\DateTimeFactory $dateFactory
    ) {

        $this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->connection = $this->objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $this->directory = $this->objectManager->get('\Magento\Framework\Filesystem\DirectoryList');
        $this->rootPath1 = $this->directory->getRoot();
        $this->mediaURL = $this->objectManager->get('Magento\Store\Model\StoreManagerInterface')
                ->getStore()
                ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        $this->rootPath = $this->rootPath1 . "/pub/media/suit-tool/";
        $this->dataPersistor = $dataPersistor;
        $this->scopeConfig = $scopeConfig;
        $this->_escaper = $escaper;
        $this->_dateFactory = $dateFactory;
        $this->inlineTranslation = $inlineTranslation;
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute() {

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();


        if ($data) {

            $id = $this->getRequest()->getParam('id');

            if (isset($data['status']) && $data['status'] === 'true') {
                $data['status'] = Block::STATUS_ENABLED;
            }
            if (empty($data['id'])) {
                $data['id'] = null;
            }


            /** @var \Magento\Cms\Model\Block $model */
            $model = $this->_objectManager->create('Suit\Threadfabricwomen\Model\Threadfabricwomen')->load($id);
            if (!$model->getId() && $id) {
                $this->messageManager->addError(__('This suit_threadfabricwomen no longer exists.'));
                return $resultRedirect->setPath('*/*/');
            }


            if (isset($data['image'][0]['name']) && isset($data['image'][0]['tmp_name'])) {
                $data['image'] = 'suit-tool/suit-threadfabricwomen/' . $data['image'][0]['name'];
            } elseif (isset($data['image'][0]['name']) && !isset($data['image'][0]['tmp_name'])) {
                $data['image'] = $data['image'][0]['name'];
            } else {
                $data['image'] = null;
            }
            $fabric_thumb_name = $data['image'];

            $model->setData($data);

            $this->inlineTranslation->suspend();
            try {
                $model->save();
                $this->messageManager->addSuccess(__('Threadfabricwomen Saved successfully'));
                $this->dataPersistor->clear('suit_threadfabricwomen');

                if ($fabric_thumb_name != '') {

                    $fabric_id = $model->getId();

                    $homepath = $this->rootPath;


//================  Suit Image Generator START ================================
                    $fabricThumbImgpath = $this->mediaURL . $fabric_thumb_name;
                    $fabricName = "fabric_main.png";
                    $fabric_0 = $homepath . 'fab_suit_images/' . $fabricName;

                    //for zoom view
                    $fabric_zoom = $homepath . "media/fab_suit_images/fabric_zoom.png";
                    $suitFabImgPath = $homepath . 'fab_suit_images/';
                    // echo "string"; die;

                    exec('convert -size 500x1320 tile:' . $fabricThumbImgpath . ' ' . $fabric_0);

                    //zoom view fabirc
                    exec('convert -size 1200x900 tile:' . $fabricThumbImgpath . ' ' . $fabric_zoom);
                    //created for collar
                    exec('convert ' . $fabric_zoom . ' -background "rgba(0,0,0,0.5)" -distort SRT 90 ' . $suitFabImgPath . 'fabric_main_back_zoom.png');


                    $suit_single_1button_thread = $homepath . 'woman_glow_mask/suitthreads/Thread_Single-Breasted_1Button_Mask.png';
                    $suit_single_2button_thread = $homepath . 'woman_glow_mask/suitthreads/Threads_Single-Breasted_2Button_Mask.png';
                    $suit_single_3button_thread = $homepath . 'woman_glow_mask/suitthreads/Threads_Single-Breasted_3Button_Mask.png';

                    $suit_double_2button_thread = $homepath . 'woman_glow_mask/suitthreads/Threads_Double-Breasted_2Buttons_Mask.png';
                    $suit_double_4button_thread = $homepath . 'woman_glow_mask/suitthreads/Threads_Double-Breasted_4Buttons_Mask.png';
                    $suit_double_6button_thread = $homepath . 'woman_glow_mask/suitthreads/Threads_Double-Breasted_6Buttons_Mask.png';

                    $suit_double_4x1button_thread = $homepath . 'woman_glow_mask/suitthreads/Threads_Double-Breasted_4x1Buttons_Mask.png';
                    $suit_double_6x1button_thread = $homepath . 'woman_glow_mask/suitthreads/Threads_Double-Breasted_6x1Buttons_Mask.png';

                    $suit_mandarin_5button_thread = $homepath . 'woman_glow_mask/suitthreads/Threads_Mandarin_5Buttons_Mask.png';

                    $suit_casual_2button_thread = $homepath . 'woman_glow_mask/suitthreads/Threads_Casual_2Button_Mask.png';

                    $suit_sleeve_2button_thread = $homepath . 'woman_glow_mask/suitthreads/Thread_Sleeve_2Button_Mask.png';
                    $suit_sleeve_3button_thread = $homepath . 'woman_glow_mask/suitthreads/Thread_Sleeve_3Button_Mask.png';
                    $suit_sleeve_4button_thread = $homepath . 'woman_glow_mask/suitthreads/Thread_Sleeve_4Button_Mask.png';


                    // suit_single_1button_thread
                    exec('composite -compose CopyOpacity ' . $suit_single_1button_thread . ' ' . $fabric_0 . ' ' . $homepath . 'women_suit_images/jacket_images/threads/' . $fabric_id . '_threads_1_btn_1_view_1.png');
                    // echo 'composite -compose CopyOpacity ' . $suit_single_1button_thread . ' ' . $fabric_0 . ' ' . $homepath . 'women_suit_images/jacket_images/threads/'. $fabric_id . '_threads_1_btn_1_view_1.png'; die;

                    // suit_single_2button_thread
                    exec('composite -compose CopyOpacity ' . $suit_single_2button_thread . ' ' . $fabric_0 . ' ' . $homepath . 'women_suit_images/jacket_images/threads/'. $fabric_id . '_threads_1_btn_2_view_1.png');

                    // suit_single_3button_thread
                    exec('composite -compose CopyOpacity ' . $suit_single_3button_thread . ' ' . $fabric_0 . ' ' . $homepath . 'women_suit_images/jacket_images/threads/'. $fabric_id . '_threads_1_btn_3_view_1.png');

                    // suit_double_2button_thread
                    exec('composite -compose CopyOpacity ' . $suit_double_2button_thread . ' ' . $fabric_0 . ' ' . $homepath . 'women_suit_images/jacket_images/threads/'. $fabric_id . '_threads_2_btn_2_view_1.png');

                    // suit_double_4button_thread
                    exec('composite -compose CopyOpacity ' . $suit_double_4button_thread . ' ' . $fabric_0 . ' ' . $homepath . 'women_suit_images/jacket_images/threads/'. $fabric_id . '_threads_2_btn_4_view_1.png');

                    // suit_double_6button_thread
                    exec('composite -compose CopyOpacity ' . $suit_double_6button_thread . ' ' . $fabric_0 . ' ' . $homepath . 'women_suit_images/jacket_images/threads/'. $fabric_id . '_threads_2_btn_6x2_view_1.png');

                    // suit_double_4x1button_thread
                    exec('composite -compose CopyOpacity ' . $suit_double_4x1button_thread . ' ' . $fabric_0 . ' ' . $homepath . 'women_suit_images/jacket_images/threads/'. $fabric_id . '_threads_2_btn_4x1_view_1.png');

                    // suit_double_6x1button_thread
                    exec('composite -compose CopyOpacity ' . $suit_double_6x1button_thread . ' ' . $fabric_0 . ' ' . $homepath . 'women_suit_images/jacket_images/threads/'. $fabric_id . '_threads_2_btn_6x1_view_1.png');


                    // suit_mandarin_5button_thread
                    exec('composite -compose CopyOpacity ' . $suit_mandarin_5button_thread . ' ' . $fabric_0 . ' ' . $homepath . 'women_suit_images/jacket_images/threads/'. $fabric_id . '_threads_3_btn_5_view_1.png');

                    //suit_casual_2button_thread
                    exec('composite -compose CopyOpacity ' . $suit_casual_2button_thread . ' ' . $fabric_0 . ' ' . $homepath . 'women_suit_images/jacket_images/threads/'. $fabric_id . '_threads_4_btn_2_view_1.png');




                    // suit_sleeve_2button_thread
                    exec('composite -compose CopyOpacity ' . $suit_sleeve_2button_thread . ' ' . $fabric_0 . ' ' . $homepath . 'women_suit_images/jacket_images/threads/'. $fabric_id . '_btn_2_view_2.png');

                    // suit_sleeve_3button_thread
                    exec('composite -compose CopyOpacity ' . $suit_sleeve_3button_thread . ' ' . $fabric_0 . ' ' . $homepath . 'women_suit_images/jacket_images/threads/'. $fabric_id . '_btn_3_view_2.png');

                    // suit_sleeve_4button_thread
                    exec('composite -compose CopyOpacity ' . $suit_sleeve_4button_thread . ' ' . $fabric_0 . ' ' . $homepath . 'women_suit_images/jacket_images/threads/'. $fabric_id . '_btn_4_view_2.png');
                }

                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['id' => $model->getId()]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the Threadfabricwomen.'));
            }
            $this->dataPersistor->set('suit_threadfabricwomen', $data);
            return $resultRedirect->setPath('*/*/edit', ['id' => $this->getRequest()->getParam('id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }

}
