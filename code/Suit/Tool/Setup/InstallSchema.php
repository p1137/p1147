<?php

/*
 * Suit_Tool

 * @category   Suit
 * @package    Suit_Tool
 * @copyright  Copyright (c) 2018 Scott Parsons
 * @license    https://github.com/ScottParsons/module-tooluicomponent/blob/master/LICENSE.md
 * @version    1.0.0
 */

namespace Suit\Tool\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class InstallSchema implements InstallSchemaInterface {

    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context) {
        $installer = $setup;
        $installer->startSetup();
        $tableName = $installer->getTable('suit_fabrics');

        if (!$installer->tableExists('suit_fabrics')) {
            $table = $installer->getConnection()
                    ->newTable($tableName)
                    ->addColumn(
                            'id', Table::TYPE_INTEGER, null, [
                        'identity' => true,
                        'unsigned' => true,
                        'nullable' => false,
                        'auto_increment' => true,
                        'primary' => true
                            ], 'ID'
                    )
                    ->addColumn(
                            'name', Table::TYPE_TEXT, 255, ['nullable' => false, 'default' => ''], 'fabric name'
                    )
                    ->addColumn(
                            'fabric_thumb', Table::TYPE_TEXT, 255, ['nullable' => false, 'default' => ''], 'generating fabric images'
                    )
                    ->addColumn(
                            'display_fabric_thumb', Table::TYPE_TEXT, 255, ['nullable' => false, 'default' => ''], 'Display fabric thumbnail'
                    )
                    ->addColumn(
                            'fabric_large_image', Table::TYPE_TEXT, 255, ['nullable' => false, 'default' => ''], 'Display fabric on tool'
                    )
                    ->addColumn(
                            'price', Table::TYPE_FLOAT, null, ['unsigned' => true, 'nullable' => false, 'default' => '0'], 'fabric price'
                    )
                    ->addColumn(
                            'material_id', Table::TYPE_SMALLINT, null, ['unsigned' => true, 'nullable' => false, 'default' => '0'], 'material_id'
                    )
                    ->addColumn(
                            'pattern_id', Table::TYPE_SMALLINT, null, ['unsigned' => true, 'nullable' => false, 'default' => '0'], 'pattern_id'
                    )
                    ->addColumn(
                            'season_id', Table::TYPE_SMALLINT, null, ['unsigned' => true, 'nullable' => false, 'default' => '0'], 'season_id'
                    )
                    ->addColumn(
                            'color_id', Table::TYPE_SMALLINT, null, ['unsigned' => true, 'nullable' => false, 'default' => '0'], 'color_id'
                    )
                    ->addColumn(
                            'category_id', Table::TYPE_SMALLINT, null, ['unsigned' => true, 'nullable' => false, 'default' => '0'], 'category_id'
                    )
                    ->addColumn(
                            'status', Table::TYPE_SMALLINT, null, ['unsigned' => true, 'nullable' => false, 'default' => '0'], 'Status'
                    )
                    ->addColumn(
                            'created_at', Table::TYPE_TIMESTAMP, null, ['nullable' => false, 'default' => Table::TIMESTAMP_INIT], 'Created At'
                    )
                    ->addColumn(
                    'updated_at', Table::TYPE_TIMESTAMP, null, ['nullable' => false, 'default' => Table::TIMESTAMP_INIT], 'Updated At'
            );
            $installer->getConnection()->createTable($table);
        }

        unset($table);
        /* suitpockets */
        $table = $installer->getConnection()->newTable($installer->getTable('suitpockets'))
                ->addColumn(
                        'suitpocket_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true], 'id'
                )
                ->addColumn(
                        'title', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'title'
                )
                ->addColumn(
                        'class', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'class'
                )
                ->addColumn(
                        'price', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'price'
                )
                ->addColumn(
                        'thumb', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'thumb'
                )
                ->addColumn(
                        'left_glow_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'Image'
                )
                ->addColumn(
                        'left_mask_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'Image'
                )
                ->addColumn(
                        'left_hi_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'Image'
                )
                ->addColumn(
                        'right_glow_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'Image'
                )
                ->addColumn(
                        'right_mask_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'Image'
                )
                ->addColumn(
                        'right_hi_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'Image'
                )
                ->addColumn(
                        'ticket_glow_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'Image'
                )
                ->addColumn(
                        'ticket_mask_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'Image'
                )
                ->addColumn(
                        'ticket_hi_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'Image'
                )
                ->addColumn(
                        'casual_left_glow', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'Image'
                )
                ->addColumn(
                        'casual_left_mask', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'Image'
                )
                ->addColumn(
                        'casual_left_hi', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'Image'
                )
                ->addColumn(
                        'casual_right_glow', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'Image'
                )
                ->addColumn(
                        'casual_right_mask', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'Image'
                )
                ->addColumn(
                        'casual_right_hi', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'Image'
                )
                ->addColumn(
                        'casual_ticket_glow', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'Image'
                )
                ->addColumn(
                        'casual_ticket_mask', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'Image'
                )
                ->addColumn(
                        'casual_ticket_hi', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'Image'
                )
                ->addColumn(
                'status', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, 6, [], 'Status'
                )
        ;
        $installer->getConnection()->createTable($table);

        unset($table);
        /* suitpockets */
        $table = $installer->getConnection()->newTable($installer->getTable('suitformallapels'))
                ->addColumn(
                        'suitformallapels_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true], 'id'
                )
                ->addColumn(
                        'title', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'Title'
                )
                ->addColumn(
                        'class', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'Class'
                )
                ->addColumn(
                        'price', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'Price'
                )
                ->addColumn(
                        'thumb', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'Thumb'
                )
                ->addColumn(
                        'upper_left_glow_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'Image'
                )
                ->addColumn(
                        'upper_left_mask_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'Image'
                )
                ->addColumn(
                        'upper_left_hi_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'Image'
                )
                ->addColumn(
                        'upper_right_glow_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'Image'
                )
                ->addColumn(
                        'upper_right_mask_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'Image'
                )
                ->addColumn(
                        'upper_right_hi_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'Image'
                )
                ->addColumn(
                        'lower_left_glow_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'Image'
                )
                ->addColumn(
                        'lower_left_mask_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'Image'
                )
                ->addColumn(
                        'lower_left_hi_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'Image'
                )
                ->addColumn(
                        'lower_right_glow_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'Image'
                )
                ->addColumn(
                        'lower_right_mask_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'Image'
                )
                ->addColumn(
                        'lower_right_hi_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'Image'
                )
                ->addColumn(
                        'style', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, 6, [], 'Image'
                )
                ->addColumn(
                        'lapel', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, 6, [], 'Image'
                )
                ->addColumn(
                        'size', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, 6, [], 'Image'
                )
                ->addColumn(
                'status', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, 6, [], 'Status'
                )
        ;
        $installer->getConnection()->createTable($table);



        $installer->endSetup();
    }

}
