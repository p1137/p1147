<?php

namespace Suit\Tool\Helper;

use Magento\Framework\UrlInterface;

class Url extends \Magento\Framework\App\Helper\AbstractHelper {

    public function getMediaPath() {
        return $this->_urlBuilder->getBaseUrl(['_type' => UrlInterface::URL_TYPE_MEDIA]);
    }

    public function getBasePath() {
        return $this->_urlBuilder->getBaseUrl();
//        $storeManager = \Magento\Framework\App\ObjectManager::getInstance()->get('\Magento\Store\Model\StoreManagerInterface');
//        $basePath = $storeManager->getStore()->getBaseUrl();
    }

}
