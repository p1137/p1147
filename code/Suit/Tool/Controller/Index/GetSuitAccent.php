<?php

namespace Suit\Tool\Controller\Index;

class GetSuitAccent extends \Magento\Framework\App\Action\Action {

    protected $_pageFactory;
    protected $_resultJsonFactory;
    protected $_jacketBrassButton;
    protected $_suitThreadFabric;
    protected $_suitPocketSquareFabric;
    protected $_suitBackCollar;
    protected $_suitelbowpatch;

    public function __construct(
    \Suit\Tool\Model\SuitelbowpatchFactory $suitElbowpatch, \Suit\Tool\Model\SuitBackCollarFactory $suitBackCollar, \Suit\Tool\Model\SuitPocketSquareFabricFactory $suitPocketSquareFabric, \Suit\Threadfabric\Model\ThreadfabricFactory $suitThreadFabric,\Suit\Mensuitlining\Model\MensuitliningFactory $mensuitlining,\Suit\Tool\Model\SuitLiningTypeFactory $suitLiningType, \Suit\Tool\Model\JacketBrassButtonFactory $jacketBrassButton, \Magento\Framework\App\Action\Context $context, \Magento\Framework\View\Result\PageFactory $pageFactory, \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory) {
        $this->_pageFactory = $pageFactory;
        $this->_resultJsonFactory = $resultJsonFactory;
        $this->_jacketBrassButton = $jacketBrassButton;
        $this->_suitThreadFabric = $suitThreadFabric;
        $this->_mensuitlining = $mensuitlining;
        $this->_suitLiningType = $suitLiningType;
        $this->_suitPocketSquareFabric = $suitPocketSquareFabric;
        $this->_suitBackCollar = $suitBackCollar;
        $this->_suitElbowpatch = $suitElbowpatch;
        return parent::__construct($context);
    }

    public function execute() {
        $storeManager = \Magento\Framework\App\ObjectManager::getInstance()->get('\Magento\Store\Model\StoreManagerInterface');
        $basePath = $storeManager->getStore()->getBaseUrl();
        $mediaPath = $storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        $brass_buttons = array();

        $jacketBrassButtonCollection = $this->_jacketBrassButton->create()->getCollection();
        $jacketBrassButtonCollection->addFieldToFilter('status', 1);
        $jackbrass_collection = $jacketBrassButtonCollection->getData();

        $i = 1;
        foreach ($jackbrass_collection as $bc) {
            $brass_buttons[] = array(
                'id' => $i,
                'parent' => 'brassbuttons',
                "designType" => "jacket",
                "designRel" => "buttons",
                'name' => $bc['name'],
                'price' => $bc['price'],
                "class" => $bc['class'],
                "img" => $mediaPath . $bc['th']);

            $i++;
        }


        $brass_data = array('id' => 1, 'name' => 'Buttons', 'class' => "icon-v2-Brass_Buttons", "designType" => "jacket", "style" => $brass_buttons);


        $suitThreadFabricCollection = $this->_suitThreadFabric->create()->getCollection();
        $suitThreadFabricCollection->addFieldToFilter('status', 1);
        $thread_collection = $suitThreadFabricCollection->getData();

        foreach ($thread_collection as $t) {
            $id = $t['id'];
            $name = $t['title'];

            $thumb = $t['image'];
            $price = $t['price'];

            $threads[] = array('id' => $id, 'parent' => 'threads', 'price' => $price, "designType" => "jacket", "designRel" => "buttons", 'name' => $name, "img" => $mediaPath . $thumb, "class" => "");
        }

        $threads_data = array('id' => 2, 'name' => 'Threads', 'class' => "icon-v2-Thread", "designType" => "jacket", "style" => $threads);


        $backcollar_collection = $this->_suitBackCollar->create()->getCollection();
        $backcollar_collection->addFieldToFilter('status', 1)->setOrder('backcollar_id', 'DESC');
        $backcollar_collection = $backcollar_collection->getData();

        $backcollar = array();
        $i = 1;
        foreach ($backcollar_collection as $bcoll) {
            $backcollar[] = array(
                'id' => $i,
                'parent' => 'backcollaraccent',
                "designType" => "jacket",
                "designRel" => "buttons",
                'name' => $bcoll['title'],
                'price' => $bcoll['price'],
                'class' => $bcoll['class'],
            );
            $i++;
        }

        $backcollar_data = array('id' => 3, 'name' => 'Back Collar', 'class' => "icon-v2-Custom_Colour", "designType" => "jacket", "style" => $backcollar);


       $elbowpatch_collection = $this->_suitElbowpatch->create()->getCollection();
       $elbowpatch_collection->addFieldToFilter('status', 1);
       $elbowpatch_collection = $elbowpatch_collection->getData();
       $elbowpatch = array();
       $i = 1;
       foreach ($elbowpatch_collection as $elbpatch) {
           $elbowpatch[] = array(
               'id' => $i,
               'parent' => 'elbowpatch',
               "designType" => "jacket",
               "designRel" => "buttons",
               'name' => $elbpatch['title'],
               'price' => $elbpatch['price'],
               'class' => $elbpatch['class'],
           );
           $i++;
       }
        $elbowpatches_data = array('id' => 4, 'name' => 'Elbow Patch', 'class' => "icon-v2-With_Elbow_Patch", "designType" => "jacket", "style" => $elbowpatch);


        $pocketsquare_collection = $this->_suitPocketSquareFabric->create()->getCollection();
        $pocketsquare_collection->addFieldToFilter('status', 1);
        $pocketsquare_collection = $pocketsquare_collection->getData();

        $pocketsquares = array();

        foreach ($pocketsquare_collection as $p) {
            $id = $p['fabric_id'];
            $name = $p['title'];
            $thumb = $p['fabric_thumb'];
            $price = $p['price'];
            $class = $p['class'];
            $pocketsquares[] = array('id' => $id, 'parent' => 'pocketsquare', 'price' => $price, "designType" => "jacket", "designRel" => "buttons", 'name' => $name, "img" => $mediaPath . $thumb, "class" => $class);
        }

        $pocketsquare_data = array('id' => 5, 'name' => 'Pocket square', 'class' => "icon-v2-Folded_Pocket_Square", "designType" => "jacket", "style" => $pocketsquares);



        $connection = $this->getObjConnection()->getConnection();
        $bows_arr = array();
        $sql = "Select * FROM suit_bows";
        $result = $connection->fetchAll($sql);
        $i = 1;
        foreach ($result as $value) {
          $bows_arr[] = array(
              'id' => $i,
              'parent' => 'manbow',
              "designType" => "suit bow",
              "designRel" => "tie",
              'name' => $value['name'],
              'price' => $value['price'],
              "class" => $value['class'],
              "img" => $mediaPath . $value['img']);
          $i++;
        }
        $suit_bows_data = array('id' => 6, 'name' => 'Bow Tie', 'class' => "icon-Show_Bow-icon", "designType" => "Bow", "style" => $bows_arr);

        $tie_arr = array();
        $sqls = "Select * FROM suit_ties";
        $results = $connection->fetchAll($sqls);
        $i = 1;
        foreach ($results as $value) {
          $tie_arr[] = array(
              'id' => $i,
              'parent' => 'tie',
              "designType" => "suit tie",
              "designRel" => "tie",
              'name' => $value['name'],
              'price' => $value['price'],
              "class" => $value['class'],
              "img" => $mediaPath . $value['img']);
          $i++;
        }
        $suit_tie_data = array('id' => 7, 'name' => 'Neck Tie', 'class' => "icon-Show_Tie-icon", "designType" => "Tie", "style" => $tie_arr);


        $lining_type = array();
        $liningtype_collection = $this->_suitLiningType->create()->getCollection();
        $liningtype_collection->addFieldToFilter('status', 1);
        $liningtype_collection = $liningtype_collection->getData();
        $i = 0;
        foreach ($liningtype_collection as $ltc) {
            $lining_type[] = array(
                'id' => $i + 1,
                'parent' => 'Lining',
                'class' => $ltc['class'],
                "designType" => "jacket",
                'name' => $ltc['title'],
                'price' => $ltc['price']
            );
            $i++;
        }


        $mensuitliningCollection = $this->_mensuitlining->create()->getCollection();
        $mensuitliningCollection->addFieldToFilter('status', 1);
        $lining_collection = $mensuitliningCollection->getData();
        $linings_data=array();

        foreach ($lining_collection as $t) {
            $id = $t['id'];
            $name = $t['title'];

            $thumb = $t['image'];
            $price = $t['price'];

            $linings[] = array('id' => $id, 'parent' => 'lining', 'price' => $price, "designType" => "jacket", "designRel" => "buttons", 'name' => $name, "fabric_thumb" => $mediaPath . $thumb, "class" => "");
        }

        $linings_data = array('id' => 8, 'name' => 'Lining', 'class' => "icon-v2-linning-none-01", "designType" => "jacket", "style" => $lining_type, "fabric" => $linings);




        $design_data = array($brass_data, $threads_data,$backcollar_data,$elbowpatches_data, $pocketsquare_data,$suit_bows_data,$suit_tie_data,$linings_data);
        return $this->_resultJsonFactory->create()->setData($design_data);
    }

    public function getObjConnection(){
      $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
      $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
      return $resource;
    }

}
