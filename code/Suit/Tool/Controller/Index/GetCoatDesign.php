<?php

namespace Suit\Tool\Controller\Index;

class GetCoatDesign extends \Magento\Framework\App\Action\Action {

    protected $_pageFactory;
    protected $_resultJsonFactory;

    public function __construct(
    \Suit\Tool\Model\SuitcoatstylesFactory $suitCoatStyle, \Magento\Framework\App\Action\Context $context, \Magento\Framework\View\Result\PageFactory $pageFactory, \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory) {
        $this->_pageFactory = $pageFactory;
        $this->_resultJsonFactory = $resultJsonFactory;
        $this->_suitCoatStyle = $suitCoatStyle;
        return parent::__construct($context);
    }

    public function execute() {
        $storeManager = \Magento\Framework\App\ObjectManager::getInstance()->get('\Magento\Store\Model\StoreManagerInterface');
        $basePath = $storeManager->getStore()->getBaseUrl();

        $style_collection = $this->_suitCoatStyle->create()->getCollection();
        $style_collection->addFieldToFilter('status', 1);
        $style_collection = $style_collection->getData();
        $styles = array();
        foreach ($style_collection as $style) {
            $id = $style['suitcoatstyles_id'];
            $name = $style['title'];
            $class = $style['class'];
            $status = $style['status'];
            $thumb = $style['thumb'];
            $price = $style['price'];

            if ($status) {
                $styles[] = array('id' => $id, 'class' => $class, 'parent' => 'topcoatstyle', "designType" => "topcoat", 'name' => $name, "price" => $price, 'type' => $id);
            }
        }

        $style_data = array('id' => 1, "designType" => "topcoat", 'name' => 'Style', 'class' => "icon-v2-Style_01-01","style" => $styles);

        $design_data = array($style_data);
        return $this->_resultJsonFactory->create()->setData($design_data);
    }

}
