<?php

namespace Suit\Tool\Controller\Index;

class GetSuitVest extends \Magento\Framework\App\Action\Action {

    protected $_pageFactory;
    protected $_resultJsonFactory;

    public function __construct(
    \Suit\Vest\Model\VestButtonFactory $suitVestButton, \Suit\Vest\Model\VestBreastPocketFactory $suitVestBreastPocket, \Suit\Vest\Model\VestpocketFactory $suitVestPocket, \Suit\Vest\Model\VestedgeFactory $suitVestEdge, \Suit\Vest\Model\VestlapelsFactory $suitVestLapels, \Suit\Vest\Model\VestStyleFactory $suitVestStyle, \Magento\Framework\App\Action\Context $context, \Magento\Framework\View\Result\PageFactory $pageFactory, \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory) {
        $this->_pageFactory = $pageFactory;
        $this->_resultJsonFactory = $resultJsonFactory;
        $this->_suitVestButton = $suitVestButton;
        $this->_suitVestBreastPocket = $suitVestBreastPocket;
        $this->_suitVestPocket = $suitVestPocket;
        $this->_suitVestEdge = $suitVestEdge;
        $this->_suitVestLapels = $suitVestLapels;
        $this->_suitVestStyle = $suitVestStyle;
        return parent::__construct($context);
    }

    public function execute() {
        $storeManager = \Magento\Framework\App\ObjectManager::getInstance()->get('\Magento\Store\Model\StoreManagerInterface');
        $basePath = $storeManager->getStore()->getBaseUrl();

        $lapels = array();
        $styles = array();

        $style_data = $this->_suitVestStyle->create()->getCollection();
        $style_data->addFieldToFilter('status', 1);
        $style_data = $style_data->getData();
        $i = 1;
        foreach ($style_data as $sd) {
            $styles[] = array(
                'id' => $i,
                'name' => $sd['title'],
                'price' => $sd['price'],
                "parent" => "veststyle",
                'class' => $sd['class'],
                "designType" => "vest"
            );
            $i++;
        }

        $lapel_data = $this->_suitVestLapels->create()->getCollection();
        $lapel_data->addFieldToFilter('status', 1);
        $lapel_data->getSelect()->group('lapel');
        $j = 1;
        foreach ($lapel_data as $lapel) {
            $lapels[] = array(
                'id' => $j,
                'name' => $lapel['title'],
                "parent" => "vestlapel",
                'class' => $lapel['class'],
                'price' => $lapel['price'],
                "designType" => "vest");
            $j++;
        }

        $style_data = array('id' => 1, 'name' => 'Style', 'class' => "icon-v2-Vest_style_main", "style" => $styles);
        $lapels_data = array('id' => 2, 'name' => 'Lapel', 'class' => "icon-v2-Vest_Lapel_Main_Icon", "style" => $lapels);

        $edge_collection = $this->_suitVestEdge->create()->getCollection();
        $edge_collection->addFieldToFilter('status', 1);
        $edge_collection = $edge_collection->getData();
        $edges = array();
        foreach ($edge_collection as $edge) {
            $id = $edge['vestedge_id'];
            $name = $edge['title'];
            $class = $edge['class'];
            $thumb = $edge['thumb'];
            $price = $edge['price'];
            $status = $edge['status'];

            if ($status) {
                $edges[] = array('id' => $id, 'name' => $name, 'price' => $price, "parent" => "vestedge", 'class' => $class, "designType" => "vest");
            }
        }

        $edges_data = array('id' => 3, 'name' => 'Edge', 'class' => "icon-v2-Vest_Edge_Main_Icon", "style" => $edges);

        $pocket_collection = $this->_suitVestPocket->create()->getCollection();
        $pocket_collection->addFieldToFilter('status', 1);
        $pocket_collection = $pocket_collection->getData();
        $pockets = array();
        foreach ($pocket_collection as $pocket) {
            $id = $pocket['vestpocket_id'];
            $name = $pocket['title'];
            $class = $pocket['class'];
            $thumb = $pocket['thumb'];
            $price = $pocket['price'];
            $status = $pocket['status'];

            if ($status) {
                $pockets[] = array('id' => $id, 'name' => $name, 'price' => $price, "parent" => "vestpockets", 'class' => $class, "designType" => "vest");
            }
        }

        $pockets_data = array('id' => 4, 'name' => 'Pockets', 'class' => "icon-v2-Pockets_Main_Icon", "style" => $pockets);

        $breastpocket_collection = $this->_suitVestBreastPocket->create()->getCollection();
        $breastpocket_collection->addFieldToFilter('status', 1);
        $breastpocket_collection = $breastpocket_collection->getData();
        $k = 1;
        foreach ($breastpocket_collection as $bpc) {
            $breast_pockets[] = array(
                'id' => $k,
                'name' => $bpc['title'],
                'price' => $bpc['price'],
                'class' => $bpc['class'],
                "parent" => "breastpocket",
                "designType" => "vest");
            $k++;
        }

        $breast_pockets_data = array('id' => 5, 'name' => 'Breast Pockets', 'class' => 'icon-v2-Vest_Breast_Pocket_Main_Icon', "style" => $breast_pockets);

        $vestbuttons_collection = $this->_suitVestButton->create()->getCollection();
        $vestbuttons_collection->addFieldToFilter('status', 1);
        $vestbuttons_collection = $vestbuttons_collection->getData();
        $vestbuttons = array();
        foreach ($vestbuttons_collection as $vestbutton) {
            $id = $vestbutton['vestbuttons_id'];
            $type = $vestbutton['type'];
            $class = $vestbutton['class'];
            $name = $vestbutton['title'] . ' Button';
            $thumb = $vestbutton['thumb'];
            $price = $vestbutton['price'];
            $btn_count = $vestbutton['btn_count'];
            $status = $vestbutton['status'];

            if ($status) {
                $vestbuttons[] = array('id' => $id, 'style_id' => $type, 'class' => $class, 'name' => $name, 'price' => $price, 'parent' => 'vestbuttons', "designType" => "vest", 'btn_count' => $btn_count);
            }
        }

        $vestbuttons_data = array('id' => 7, 'name' => 'vest buttons', "designType" => "vest", 'class' => "icon-v2-Vest_Single_breasted_4_buttons", "style" => $vestbuttons);

        $design_data = array($style_data, $edges_data, $lapels_data, $pockets_data, $breast_pockets_data, $vestbuttons_data);
        return $this->_resultJsonFactory->create()->setData($design_data);
    }

}
