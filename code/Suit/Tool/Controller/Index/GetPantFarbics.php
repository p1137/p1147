<?php

namespace Suit\Tool\Controller\Index;

class GetPantFarbics extends \Magento\Framework\App\Action\Action {

    protected $_pageFactory;
    protected $_resultJsonFactory;
    protected $_fabricSeason;
    protected $_fabricPattern;
    protected $_fabricMaterial;
    protected $_fabricColor;
    protected $_fabricCatagory;
    protected $_suitPantFarbic;

    public function __construct(
    \Magento\Framework\App\Action\Context $context, \Shirt\Tool\Model\FabricSeasonFactory $fabricSeason, \Magento\Framework\View\Result\PageFactory $pageFactory, \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory, \Shirt\Tool\Model\FabricPatternFactory $fabricPattern, \Shirt\Tool\Model\FabricMaterialFactory $fabricMaterial, \Shirt\Tool\Model\FabricColorFactory $fabricColor, \Shirt\Tool\Model\FabriccataFactory $fabricCatagory, \Suit\Pant\Model\PantFactory $suitPantFarbic) {
        $this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->_pageFactory = $pageFactory;
        $this->_resultJsonFactory = $resultJsonFactory;
        $this->_fabricSeason = $fabricSeason;
        $this->_fabricPattern = $fabricPattern;
        $this->_fabricMaterial = $fabricMaterial;
        $this->_fabricColor = $fabricColor;
        $this->_fabricCatagory = $fabricCatagory;
        $this->_suitPantFarbic = $suitPantFarbic;
        return parent::__construct($context);
    }

    public function execute() {
        $storeManager = $this->objectManager->get('\Magento\Store\Model\StoreManagerInterface');
        $basePath = $storeManager->getStore()->getBaseUrl();
        $mediaPath = $storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        // fabric_season
        $modelFabricSeason = $this->_fabricSeason->create()->getCollection();
        $modelFabricSeason->addFieldToFilter('status', 1);
        // $modelFabricSeason->setOrder('season_id','ASC');
        $season_collection = $modelFabricSeason->getData();

        // gives associated array, table fields as key in array.
        $materials = array();
        foreach ($season_collection as $material) {
            $id = $material['season_id'];
            $name = $material['title'];
            $status = $material['status'];
            if ($status) {
                $materials[] = array('id' => $id, 'name' => $name, 'parent' => 'material');
            }
        }

        //////////////////////////////////////////////////////////////////
        //fabric_pattern
        $modelPattern = $this->_fabricPattern->create()->getCollection();
        $modelPattern->addFieldToFilter('status', 1);
        // $modelPattern->setOrder('pattern_id','ASC');
        $pattern_collection = $modelPattern->getData();


        $patterns = array();
        foreach ($pattern_collection as $pattern) {
            $id = $pattern['pattern_id'];
            $name = $pattern['title'];
            if ($status) {
                $patterns[] = array('id' => $id, 'name' => $name, 'parent' => 'pattern');
            }
        }
        ////////////////////////////////////////////////////////////////////////////////
        // fabric_material

        $modelMaterial = $this->_fabricMaterial->create()->getCollection();
        $modelMaterial->addFieldToFilter('status', 1);
        // $modelMaterial->setOrder('material_id', 'ASC');
        $material_collection = $modelMaterial->getData();


        $seasons = array();
        foreach ($material_collection as $season) {
            $id = $season['material_id'];
            $name = $season['title'];
            if ($status) {
                $seasons[] = array('id' => $id, 'name' => $name, 'parent' => 'seasons');
            }
        }
        //////////////////////////////////////////////////////////////////////////
        // fabric_color
        $modelColor = $this->_fabricColor->create()->getCollection();
        $modelColor->addFieldToFilter('status', 1);
        // $modelColor->setOrder('color_id','ASC');
        $color_collection = $modelColor->getData();

        $colors = array();
        foreach ($color_collection as $color) {
            $id = $color['color_id'];
            $name = $color['title'];
            if ($status) {
                $colors[] = array('id' => $id, 'name' => $name, 'parent' => 'color');
            }
        }

        //////////////////////////////////////////////////////////////////////////
        // fabric_category
        $modelCategory = $this->_fabricCatagory->create()->getCollection();
        $modelCategory->addFieldToFilter('status', 1);
        // $modelCategory->setOrder('category_id','ASC');
        $category_collection = $modelCategory->getData();


        $categorys = array();
        foreach ($category_collection as $category) {
            $id = $category['category_id'];
            $name = $category['title'];
            $class = $category['class'];
            if ($status) {
                $categorys[] = array('id' => $id, 'name' => $name, 'parent' => 'category', "class" => $class);
            }
        }

        $categories = array($materials, $patterns, $seasons, $colors, $categorys);

        //////////////////////////////////////////////////////////////////////////////////
        // shirt_shirtfabric
        $modelshirtShirtFabric = $this->_suitPantFarbic->create()->getCollection();
        $modelshirtShirtFabric->addFieldToFilter('status', 1);
        $modelshirtShirtFabric->setOrder('name', 'ASC');
        $fabrics_collection = $modelshirtShirtFabric->getData();

        $fabrics = array();

        try {
            $i = 0;
            foreach ($fabrics_collection as $fabric) {

                $id = $fabric['id'];
                $name = $fabric['name'];
                $thumb = $fabric['display_fabric_thumb'];
                $price = $fabric['price'];
                $real_img = $fabric['fabric_large_image'];
                $shirt_type_name = "Cotton";
                $status = $fabric['status'];

                // -------------------- //
                $shirt_material_id = $fabric['material_id'];

                $sqlmaterial = $this->_fabricMaterial->create()->getCollection();
                $sqlmaterial->addFieldToFilter('status', 1);
                $sqlmaterial->addFieldToFilter('material_id', $shirt_material_id);

                $material_collection = $sqlmaterial->getData();
                // -------------------- //

                $shirt_pattern_id = $fabric['pattern_id'];

                $sqlpattern = $this->_fabricPattern->create()->getCollection();
                $sqlpattern->addFieldToFilter('status', 1);
                $sqlpattern->addFieldToFilter('pattern_id', $shirt_pattern_id);
                $pattern_collection = $sqlpattern->getData();
                // -------------------- //

                $shirt_season_id = $fabric['season_id'];

                $sqlseason = $this->_fabricSeason->create()->getCollection();
                $sqlseason->addFieldToFilter('status', 1);
                $sqlseason->addFieldToFilter('season_id', $shirt_season_id);
                $season_collection = $sqlseason->getData();
                // -------------------- //
                // -------------------- //
                $shirt_color_id = $fabric['color_id'];

                $sqlcolor = $this->_fabricColor->create()->getCollection();
                $sqlcolor->addFieldToFilter('status', 1);
                $sqlcolor->addFieldToFilter('color_id', $shirt_color_id);

                $color_collection = $sqlcolor->getData();

                // -------------------- //

                $shirt_category_id = $fabric['category_id'];

                $sqlcategory = $this->_fabricCatagory->create()->getCollection();
                $sqlcategory->addFieldToFilter('status', 1);
                $sqlcategory->addFieldToFilter('category_id', $shirt_category_id);

                $category_collection = $sqlcategory->getData();
                // -------------------- //




                if ($status) {
                    $shirt_fab_type_name = $material_collection[0]['title'];
                    $fabrics[] = array(
                        'id' => $id,
                        'name' => $name,
                        'fabric_thumb' => $mediaPath . $thumb,
                        'real_img' => $mediaPath . $real_img,
                        'price' => $price,
                        'material_parent' => $material_collection[0]['title'],
                        'pattern_parent' => $pattern_collection[0]['title'],
                        'season_parent' => $season_collection[0]['title'],
                        'color_parent' => $color_collection[0]['title'],
                        'category_parent' => $category_collection[0]['title']
                    );
                }
            }
        } catch (\Exeption $e) {
            echo $e;
            die;
        }

        $fabricInfo = array('fabric' => $fabrics, 'category' => $categories);
        return $this->_resultJsonFactory->create()->setData($fabricInfo);
    }

}
