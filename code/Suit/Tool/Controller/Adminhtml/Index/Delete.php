<?php
/**
* @author Dhirajkumar Deore
* Copyright © 2018 Magento. All rights reserved.
* See COPYING.txt for license details.
*/
namespace Suit\Tool\Controller\Adminhtml\Index;

use Magento\Framework\UrlInterface;
// use Magento\Backend\App\Action;
// use Magento\Backend\App\Action\Context;
// use Magento\Framework\Filesystem\Driver\File;
// use Magento\Framework\Filesystem;
// use Magento\Framework\App\Filesystem\DirectoryList;

class Delete extends \Magento\Backend\App\Action
{

      //   protected $_filesystem;
      //   protected $_file;
      //
      // public function __construct(
      //     Context $context,
      //     Filesystem $_filesystem,
      //     File $file
      // )
      // {
      //     parent::__construct($context);
      //     $this->_filesystem = $_filesystem;
      //     $this->_file = $file;
      // }


    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Suit_Tool::suit_tool');
    }

    /*
      @mediaStoragePath
    */
    public function getMediaPath() {
      return UrlInterface::URL_TYPE_MEDIA;
    }

    /**
     * Delete action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        // check if we know what should be deleted
        $id = $this->getRequest()->getParam('id');
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($id) {
            $title = "";
            try {
                // init model and delete
                $model = $this->_objectManager->create('Suit\Tool\Model\Tool');
                $model->load($id);
                $title = $model->getTitle();

                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

                $directory = $objectManager->get('\Magento\Framework\Filesystem\DirectoryList');

                $rootPath1  =  $directory->getPath('media').$model['thumbnail_image'];
                $rootPath2  =  $directory->getPath('media').$model['glow_image'];

                //  checking if file exists in folder if exist then delete.
                if(is_file($rootPath1)){

                  unlink($rootPath1);

                }else{ }


                if(is_file($rootPath2)){

                  unlink($rootPath2);

                }else{ }

                      $model->delete();
                      // display success message
                      $this->messageManager->addSuccess(__('The suit has been deleted.'));



                // go to grid
                /*$this->_etoolManager->dispatch(
                    'adminhtml_suit_tool_on_delete',
                    ['title' => $title, 'status' => 'success']
                );*/
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                /*$this->_etoolManager->dispatch(
                    'adminhtml_suit_tool_on_delete',
                    ['title' => $title, 'status' => 'fail']
                );*/
                // display error message
                $this->messageManager->addError($e->getMessage());
                // go back to edit form
                return $resultRedirect->setPath('*/*/edit', ['id' => $id]);
            }
        }
        // display error message
        $this->messageManager->addError(__('We can\'t find a suit tool to delete.'));
        // go to grid
        return $resultRedirect->setPath('*/*/');
    }
}
