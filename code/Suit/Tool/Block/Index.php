<?php

namespace Suit\Tool\Block;

class Index extends \Magento\Framework\View\Element\Template {

    protected $_logo;
 protected $_scopeConfig;

    public function __construct(\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Theme\Block\Html\Header\Logo $logo, 
        \Magento\Customer\Model\Session $customerSession,
        \Wholesaler\Mgmt\Model\MeasurementFactory $measurementfactory,
        \Magento\Catalog\Block\Product\Context $context, 
        array $data = []) {

        $this->_logo = $logo;
        $this->_scopeConfig = $scopeConfig;
        $this->_customerSession = $customerSession;
        $this->measurementfactory = $measurementfactory;
        parent::__construct($context, $data);
    }

    protected function _prepareLayout() {
        return parent::_prepareLayout();
    }

 public function getProductId() {
        $myvalue = $this->_scopeConfig->getValue('toolConfiguration/general/men_suit_tool_product', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        return $myvalue;
    }


    /**
     * Get logo image URL
     *
     * @return string
     */
    public function getLogoSrc() {
        return $this->_logo->getLogoSrc();
    }

    /**
     * Get logo text
     *
     * @return string
     */
    public function getLogoAlt() {
        return $this->_logo->getLogoAlt();
    }

    /**
     * Get logo width
     *
     * @return int
     */
    public function getLogoWidth() {
        return $this->_logo->getLogoWidth();
    }

    /**
     * Get logo height
     *
     * @return int
     */
    public function getLogoHeight() {
        return $this->_logo->getLogoHeight();
    }


}
