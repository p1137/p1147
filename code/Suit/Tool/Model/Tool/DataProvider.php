<?php

/**
 * @author Dhirajkumar Deore
 * Copyright © 2018 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Suit\Tool\Model\Tool;

use Suit\Tool\Model\ResourceModel\Tool\CollectionFactory;
use Magento\Framework\App\Request\DataPersistorInterface;

/**
 * Class DataProvider
 */
class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider {

    /**
     * @var \Magento\Cms\Model\ResourceModel\Block\Collection
     */
    protected $collection;

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;
    public $_storeManager;

    /**
     * @var array
     */
    protected $loadedData;

    /**
     * Constructor
     *
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $blockCollectionFactory
     * @param DataPersistorInterface $dataPersistor
     * @param array $meta
     * @param array $data
     */
    public function __construct(
    $name, $primaryFieldName, $requestFieldName, CollectionFactory $blockCollectionFactory, DataPersistorInterface $dataPersistor, \Magento\Store\Model\StoreManagerInterface $storeManager, array $meta = [], array $data = []
    ) {
        $this->collection = $blockCollectionFactory->create();
        $this->_storeManager = $storeManager;
        $this->dataPersistor = $dataPersistor;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData() {
        $temp = [];
        $tempthread = [];
        $baseurl = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        $items = $this->collection->getItems();
        /** @var \Magento\Cms\Model\Block $block */
        foreach ($items as $block) {
            $this->loadedData[$block->getId()] = $block->getData();
            $temp = $block->getData();

            if ($temp['fabric_thumb'] != '') {
                $img = [];
                $img[0]['name'] = $temp['fabric_thumb'];
                $img[0]['url'] = $baseurl . $temp['fabric_thumb'];
                $temp['fabric_thumb'] = $img;
                $this->loadedData[$block->getId()]['fabric_thumb'] = $img;
            }

            if ($temp['display_fabric_thumb'] != '') {
                $mtl = [];
                $mtl[0]['name'] = $temp['display_fabric_thumb'];
                $mtl[0]['url'] = $baseurl . $temp['display_fabric_thumb'];
                $temp['display_fabric_thumb'] = $mtl;
                $this->loadedData[$block->getId()]['display_fabric_thumb'] = $mtl;
            }

            if ($temp['fabric_large_image'] != '') {
                $mtl = [];
                $mtl[0]['name'] = $temp['fabric_large_image'];
                $mtl[0]['url'] = $baseurl . $temp['fabric_large_image'];
                $temp['fabric_large_image'] = $mtl;
                $this->loadedData[$block->getId()]['fabric_large_image'] = $mtl;
            }
        }

        $data = $this->dataPersistor->get('suit_tool');

        if (!empty($data)) {
            $block = $this->collection->getNewEmptyItem();
            $block->setData($data);

            $this->loadedData[$block->getId()] = $block->getData();

            $this->dataPersistor->clear('suit_tool');
        }

        if (empty($this->loadedData)) {
            return $this->loadedData;
        } else {

            if ($block->getData('fabric_thumb') != null) {
                $t2[$block->getId()] = $temp;
                return $t2;
            } else {
                return $this->loadedData;
            }
        }
    }

}
