<?php

namespace Suit\Tool\Model;

class SuitTuxedoColor extends \Magento\Framework\Model\AbstractModel {

    protected function _construct() {
        $this->_init('Suit\Tool\Model\ResourceModel\SuitTuxedoColor');
    }

}
