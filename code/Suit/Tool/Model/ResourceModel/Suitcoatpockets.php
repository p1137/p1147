<?php

/**
 * @author Akshay Shelke    
 * Copyright © 2018 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Suit\Tool\Model\ResourceModel;

class Suitcoatpockets extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb {

    protected function _construct() {
        $this->_init('suitcoatpockets', 'suitcoatpockets_id');
    }

}
