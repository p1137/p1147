<?php

/**
 * @author Akshay Shelke    
 * Copyright © 2018 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Suit\Tool\Model\ResourceModel\Suitformallapels;

use \Suit\Tool\Model\ResourceModel\AbstractCollection;

class Collection extends AbstractCollection {

    protected function _construct() {
        $this->_init('Suit\Tool\Model\Suitformallapels', 'Suit\Tool\Model\ResourceModel\Suitformallapels');
        $this->_map['fields']['suitformallapels_id'] = 'main_table.suitformallapels_id';
    }

}
