<?php

namespace Suit\Tool\Model\ResourceModel\SuitTuxedoType;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection {

    protected function _construct() {
        $this->_init('Suit\Tool\Model\SuitTuxedoType', 'Suit\Tool\Model\ResourceModel\SuitTuxedoType');
    }

}
