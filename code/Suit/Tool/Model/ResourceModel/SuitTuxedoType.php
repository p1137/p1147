<?php

namespace Suit\Tool\Model\ResourceModel;

class SuitTuxedoType extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb {

    protected function _construct() {
        $this->_init('suittuxedotype', 'suittuxedotype_id');
    }

}
