<?php

namespace Suit\Tool\Model\ResourceModel\SuitTuxedoColor;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection {

    protected function _construct() {
        $this->_init('Suit\Tool\Model\SuitTuxedoColor', 'Suit\Tool\Model\ResourceModel\SuitTuxedoColor');
    }

}
