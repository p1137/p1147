<?php

namespace Suit\Tool\Model;

class SuitLiningType extends \Magento\Framework\Model\AbstractModel {

    protected function _construct() {
        $this->_init('Suit\Tool\Model\ResourceModel\SuitLiningType');
    }

}
