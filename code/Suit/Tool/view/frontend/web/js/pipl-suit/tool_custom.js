var did = "0";
var sizeArr = {};
var selStyleArr = {};
var basePath;
var canvas;
var flg,sizeStdFlg=true;
var stylePriceArr = {};

var currSymbol;

var jacketSizes = [
    {"id": "19", "parent": "Jacket", "product_type": "1", "gender": 1, "size_name": "Slim", "chest": "15.0", "waist": "36"
        , "hip": "38", "shoulder": "38", "sleeve": "38", "length": "38", "size_order": "1"},
    {"id": "20", "parent": "Jacket", "product_type": "1", "gender": 1, "size_name": "Classic", "chest": "15.0", "waist": "36"
        , "hip": "38", "shoulder": "38", "sleeve": "38", "length": "38", "size_order": "2"},
    {"id": "21", "parent": "Jacket", "product_type": "1", "gender": 1, "size_name": "XL", "chest": "15.0", "waist": "36"
        , "hip": "38", "shoulder": "38", "sleeve": "38", "length": "38", "size_order": "3"}
];

var pantSizes = [
    {"id": "19", "parent": "Pant", "product_type": "1", "gender": 1, "size_name": "Slim", "waist": "36"
        , "hip": "38", "crotch": "38", "thigh": "38", "length": "38", "size_order": "1"},
    {"id": "20", "parent": "Pant", "product_type": "1", "gender": 1, "size_name": "Classic", "waist": "36"
        , "hip": "38", "crotch": "38", "thigh": "38", "length": "38", "size_order": "2"},
    {"id": "21", "parent": "Pant", "product_type": "1", "gender": 1, "size_name": "XL", "waist": "36"
        , "hip": "38", "crotch": "38", "thigh": "38", "length": "38", "size_order": "3"}
];



jQuery(document).ready(function (e) {

    basePath = jQuery("#basePath").val();
    currSymbol = jQuery("#currSymbol").val();

    jQuery("#content-5_hor").mCustomScrollbar({
        axis: "x",
        theme: "dark-thin",
        autoExpandScrollbar: true,
        advanced: {autoExpandHorizontalScroll: true}
    });


    jQuery("#validationSubmitButton").on("click", function () {
        myValidation();
    });

    /*fabric popUp hide*/
    jQuery(".navigate-menu").mouseup(function (e)
    {

        var container = jQuery("#fabricSlid");
        if (!container.is(e.target)) // if the target of the click isn't the container...
        {
            console.log("else part");
            jQuery(".fabricInfo a").trigger("click");
        }

    });


    setTimeout(function () {
        jQuery("#div5 ul a").eq(3).find(".option_icon_left").children().css({"display": "block"});
        // jQuery("#div5 ul a").eq(3).find(".option_icon_left").append('<span class="liningIcon"><img width="50" height="50" src=' + basePath + 'media/tool_data/img/Line.png></span>');
    }, 1500);

    setTimeout(function () {
        jQuery(".removemin").on("click", function () {

            jQuery("body").removeClass("LeftpanemOpen");

        });
    }, 1500);


    jQuery("#plength,#thigh,#crotch,#phip,#pwaist,#length,#sleeve,#shoulder,#hip,#waist,#chest").keypress(function (e) {
        //if the letter is not digit then display error and don't type anything
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            //display error message
            //$("#errmsg").html("Digits Only").show().fadeOut("slow");
            return false;
        }
    });

});
function myValidation() {
    var flg = true;

    var measureMents = jQuery("input[name='unitOptions']:checked").val();


    /*jacket*/
    var chestRangeVar = jQuery("#chestRange").next().attr("rangeval");

    var chestRangemin = chestRangeVar.split("-")[0];
    chestRangemin = parseInt(chestRangemin);

    var chestRangemax = chestRangeVar.split("-")[1];
    chestRangemax = parseInt(chestRangemax)

    jQuery("#ChestRangeHolder").css({"display": "none"});
    jQuery("#Chestrequired").css({"display": "none"});
    var ChestRange = jQuery("#chestRange").val();



    if (ChestRange > chestRangemax || ChestRange < chestRangemin) {
        if (ChestRange == "") {

            jQuery("#Chestrequired").css({"display": "block"});

        } else {


            setTimeout(function () {
                jQuery("#chestRange").val("");
                jQuery("#ChestRangeHolder").css({"display": "block"});
            }, 500);

        }

        flg = false;
    }


    var waistRangeVar = jQuery("#waistRange").next().attr("rangeval");
    var waistRangemin = waistRangeVar.split("-")[0];
    waistRangemin = parseInt(waistRangemin);

    var waistRangemax = waistRangeVar.split("-")[1];
    waistRangemax = parseInt(waistRangemax)

    jQuery("#Waistrequired").css({"display": "none"});
    jQuery("#WaistRangeHolder").css({"display": "none"});
    var WaistRange = jQuery("#waistRange").val();

    if (WaistRange > waistRangemax || WaistRange < waistRangemin) {

        if (WaistRange == "") {

            // setTimeout(function() {
            jQuery("#Waistrequired").css({"display": "block"});
            // }, 300);
        } else {

            setTimeout(function () {
                jQuery("#waistRange").val("");
                jQuery("#WaistRangeHolder").css({"display": "block"});
            }, 500);

        }

        flg = false;
    }

    var HipRange = jQuery("#hipRange").val();
    var hipRangeVar = jQuery("#hipRange").next().attr("rangeval");
    var hipRangemin = hipRangeVar.split("-")[0];
    hipRangemin = parseInt(hipRangemin);

    var hipRangemax = hipRangeVar.split("-")[1];
    hipRangemax = parseInt(hipRangemax);
    jQuery("#Hiprequired").css({"display": "none"});
    jQuery("#HipRangeHolder").css({"display": "none"});
    if (HipRange > hipRangemax || HipRange < hipRangemin) {
        if (HipRange == "") {


            //setTimeout(function() {
            jQuery("#Hiprequired").css({"display": "block"});
            //}, 300);
        } else {

            setTimeout(function () {
                jQuery("#hipRange").val("");
                jQuery("#HipRangeHolder").css({"display": "block"});
            }, 500);

        }

        flg = false;
    }

    var ShoulderRange = jQuery("#shoulderRange").val();
    var shoulderRangeVar = jQuery("#shoulderRange").next().attr("rangeval");
    var shoulderRangemin = shoulderRangeVar.split("-")[0];
    shoulderRangemin = parseInt(shoulderRangemin);

    var shoulderRangemax = shoulderRangeVar.split("-")[1];
    shoulderRangemax = parseInt(shoulderRangemax);
    jQuery("#Shoulderrequired").css({"display": "none"});
    jQuery("#ShoulderRangeHolder").css({"display": "none"});
    if (ShoulderRange > shoulderRangemax || ShoulderRange < shoulderRangemin) {
        if (ShoulderRange == "") {
            //setTimeout(function() {
            jQuery("#Shoulderrequired").css({"display": "block"});
            // }, 300);

        } else {

            setTimeout(function () {
                jQuery("#shoulderRange").val("");
                jQuery("#ShoulderRangeHolder").css({"display": "block"});
            }, 500);

        }
        flg = false;
    }

    var SleeveRange = jQuery("#sleeveRange").val();
    var sleeveRangeVar = jQuery("#sleeveRange").next().attr("rangeval");
    var sleeveRangemin = sleeveRangeVar.split("-")[0];
    sleeveRangemin = parseInt(sleeveRangemin);

    var sleeveRangemax = sleeveRangeVar.split("-")[1];
    sleeveRangemax = parseInt(sleeveRangemax)
    jQuery("#Sleeverequired").css({"display": "none"});
    jQuery("#SleeveRangeHolder").css({"display": "none"});
    if (SleeveRange > sleeveRangemax || SleeveRange < sleeveRangemin) {
        if (SleeveRange == "") {
            //setTimeout(function() {
            jQuery("#Sleeverequired").css({"display": "block"});
            //}, 300);


        } else {

            setTimeout(function () {
                jQuery("#sleeveRange").val("");
                jQuery("#SleeveRangeHolder").css({"display": "block"});
            }, 500);

        }
        flg = false;
    }

    var lengthRange = jQuery("#lengthRange").val();
    var lengthRangeVar = jQuery("#lengthRange").next().attr("rangeval");
    var lengthRangemin = lengthRangeVar.split("-")[0];
    lengthRangemin = parseInt(lengthRangemin);

    var lengthRangemax = lengthRangeVar.split("-")[1];
    lengthRangemax = parseInt(lengthRangemax);
    jQuery("#Lengthrequired").css({"display": "none"});
    jQuery("#LengthRangeHolder").css({"display": "none"});
    if (lengthRange > lengthRangemax || lengthRange < lengthRangemin) {
        if (lengthRange == "") {
            //setTimeout(function() {
            jQuery("#Lengthrequired").css({"display": "block"});
            //}, 300);


        } else {

            setTimeout(function () {
                jQuery("#lengthRange").val("");
                jQuery("#LengthRangeHolder").css({"display": "block"});
            }, 500);

        }
        flg = false;
    }

    /*Pant*/
    var PantWaist = jQuery("#PantWaistRange").val();
    var PantWaistRangeVar = jQuery("#PantWaistRange").next().attr("rangeval");
    var PantWaistRangemin = PantWaistRangeVar.split("-")[0];
    PantWaistRangemin = parseInt(PantWaistRangemin);

    var PantWaistRangemax = PantWaistRangeVar.split("-")[1];
    PantWaistRangemax = parseInt(PantWaistRangemax);
    jQuery("#PantWaistrequired").css({"display": "none"});
    jQuery("#PantWaistRangeHolder").css({"display": "none"});
    if (PantWaist > PantWaistRangemax || PantWaist < PantWaistRangemin) {
        if (PantWaist == "") {
            //setTimeout(function() {
            jQuery("#PantWaistrequired").css({"display": "block"});
            // }, 300);


        } else {

            setTimeout(function () {
                jQuery("#PantWaistRange").val("");
                jQuery("#PantWaistRangeHolder").css({"display": "block"});
            }, 500);

        }
        flg = false;
    }

    var PantHip = jQuery("#PantHipRange").val();
    var PantHipRangeVar = jQuery("#PantHipRange").next().attr("rangeval");
    var PantHipRangemin = PantHipRangeVar.split("-")[0];
    PantHipRangemin = parseInt(PantHipRangemin);

    var PantHipRangemax = PantHipRangeVar.split("-")[1];
    PantHipRangemax = parseInt(PantHipRangemax);
    jQuery("#PantHiprequired").css({"display": "none"});
    jQuery("#PantHipRangeHolder").css({"display": "none"});
    if (PantHip > PantHipRangemax || PantHip < PantHipRangemin) {
        if (PantHip == "") {
            // setTimeout(function() {
            jQuery("#PantHiprequired").css({"display": "block"});
            // }, 300);


        } else {

            setTimeout(function () {
                jQuery("#PantHipRange").val("");
                jQuery("#PantHipRangeHolder").css({"display": "block"});
            }, 500);

        }
        flg = false;

    }

    var PantCrotch = jQuery("#PantCrotchRange").val();
    var PantCrotchRangeVar = jQuery("#PantCrotchRange").next().html();
    ;
    var PantCrotchRangemin = PantCrotchRangeVar.split("-")[0];
    PantCrotchRangemin = parseInt(PantCrotchRangemin);

    var PantCrotchRangemax = PantCrotchRangeVar.split("-")[1];
    PantCrotchRangemax = parseInt(PantCrotchRangemax);
    jQuery("#PantCrotchrequired").css({"display": "none"});
    jQuery("#PantCrotchRangeHolder").css({"display": "none"});

    console.log("PantCrotchRangemax==" + PantCrotchRangemax);
    console.log("PantCrotchRangemin==" + PantCrotchRangemin);
    console.log("PantCrotch==" + PantCrotch);

    if (PantCrotch > PantCrotchRangemax || PantCrotch < PantCrotchRangemin) {

        console.log("in croch")
        if (PantCrotch == "") {

            //setTimeout(function() {
            jQuery("#PantCrotchrequired").css({"display": "block"});
            // }, 300);


        } else {

            setTimeout(function () {
                jQuery("#PantCrotchRangeHolder").css({"display": "block"});
                jQuery("#PantCrotchRange").val("");
            }, 500);

        }

        flg = false;

    }

    var PantThigh = jQuery("#PantThighRange").val();
    var PantThighRangeVar = jQuery("#PantThighRange").next().html();
    ;
    var PantThighRangemin = PantThighRangeVar.split("-")[0];
    PantThighRangemin = parseInt(PantThighRangemin);

    var PantThighRangemax = PantThighRangeVar.split("-")[1];
    PantThighRangemax = parseInt(PantThighRangemax);

    jQuery("#PantThighrequired").css({"display": "none"});
    jQuery("#PantThighRangeHolder").css({"display": "none"});
    if (PantThigh > PantThighRangemax || PantThigh < PantThighRangemin) {
        if (PantThigh == "") {
            // setTimeout(function() {
            jQuery("#PantThighrequired").css({"display": "block"});
            // }, 300);


        } else {

            setTimeout(function () {
                jQuery("#PantThighRange").val("");
                jQuery("#PantThighRangeHolder").css({"display": "block"});
            }, 500);

        }
        flg = false;
    }

    var PantLength = jQuery("#PantLengthRang").val();
    var PantLengthRangVar = jQuery("#PantLengthRang").next().attr("rangeval");
    var PantLengthRangmin = PantLengthRangVar.split("-")[0];
    PantLengthRangmin = parseInt(PantLengthRangmin);

    var PantLengthRangmax = PantLengthRangVar.split("-")[1];
    PantLengthRangmax = parseInt(PantLengthRangmax);

    jQuery("#PantLengthrequired").css({"display": "none"});
    jQuery("#PantLengthRangeHolder").css({"display": "none"});
    if (PantLength > PantLengthRangmax || PantLength < PantLengthRangmin) {
        if (PantLength == "") {
            //setTimeout(function() {
            jQuery("#PantLengthrequired").css({"display": "block"});
            // }, 300);


        } else {

            setTimeout(function () {
                jQuery("#PantLengthRang").val("");
                jQuery("#PantLengthRangeHolder").css({"display": "block"});
            }, 500);

        }
        flg = false;
    }





    if (!flg) {
        return false;

    } else {
        return true;

    }
}

function addImage(imgPath)
{
    //  console.log("add image")
    //i = 101;
    //jQuery(".remove-pr").trigger('click');
    //jQuery(".preloader").show();
    //console.info("add function is called = " + imgPath);
    fabric.Image.fromURL(imgPath, function (image) {

        var theScaleFactor = getScaleFactor(image.width, image.height);

        image.set({
            left: theScaleFactor.x,
            top: theScaleFactor.y,
            angle: 0,
            identity: "img" + canvas.getObjects().length,
            centerTransform: true
        }).scale(theScaleFactor.scale).setCoords();

        canvas.add(image);
        canvas.setActiveObject(image);

        canvas.renderAll();

        jQuery(".preloader").hide();

    });

}

function getScaleFactor(w, h)
{
    var newScaleY = 1;
    var newScaleX = 1;
    var isScaled = false;

    var arrW = canvas.getWidth();
    var arrH = canvas.getHeight();


    if (w > arrW || h > arrH)
    {
        if ((w >= arrW) && (h >= arrH))
        {
            if (w > h)
            {
                newScaleX = arrW / w;
                newScaleY = newScaleX;
            }
            else
            {
                newScaleY = arrH / h;
                newScaleX = newScaleY;
            }
            isScaled = true;
        }

        if (!isScaled && (w >= arrW))
        {
            newScaleX = arrW / w;
            newScaleY = newScaleX;
            isScaled = true;
        }

        if (!isScaled && (h >= arrH))
        {
            newScaleY = arrH / h;
            newScaleX = newScaleY;
            isScaled = true;
        }

    }

    var newX = (arrW - (parseFloat(w * newScaleX))) / 2;
    var newY = (arrH - (parseFloat(h * newScaleX))) / 2;

    return {scale: newScaleX, x: newX, y: newY};
}


function addToCart() {

     jQuery("#addMeasurement").modal("show");

   /* jQuery('.modal-backdrop').css("opacity", 0);

    setTimeout(function () {
    jQuery('.modal-backdrop').remove();
    }, 500);


   //saveDesign();
   getProductImage();*/
}

function saveStdSizeData()
{
    var sizeData = {};

    sizeStdFlg = true;

    //saveDesign();
    //getProductImage();
    getAllViewImages();
}


function getStdSizeData()
{



    var cnt = 0;
    var sizeData = {};

    var fit = jQuery("#stdSizeFlgContainer .fit-select-box option:selected").val().toLowerCase();
    var qty = jQuery("#stdSizeFlgContainer .stdQty option:selected").val();


    for (var s in jacketSizes) {

          if (jacketSizes.hasOwnProperty(s))
        {
//            console.log("jacketSizes[s]['size_name'] = " + jacketSizes[s]['size_name'])
            if ((jacketSizes[s]['size_name']).toLowerCase() == fit)
            {
                sizeData[cnt++] = jacketSizes[s];

            }
        }




//        if ((jacketSizes[s.hasOwnProperty()]['size_name']).toLowerCase() == fit)
//        {
//            sizeData[cnt++] = jacketSizes[s];
//
//        }
    }
    //pant
    for (var s in pantSizes) {
        if(pantSizes.hasOwnProperty(s)){
        if ((pantSizes[s]['size_name']).toLowerCase() == fit)
        {
            sizeData[cnt++] = pantSizes[s];
        }
    }
    }


    return {
        sizeData: sizeData,
        qty: qty,
        fit: fit,
        type: "Standard"
    };

}



function getScanSizeData()
{

    var sizeData = {};
    var qty = jQuery("#scanSizeFlgContainer .scanQty option:selected").val();
    var obj = {};
    var cnt = 0;
   // var unit=jQuery("#scanSizeFlgContainer input").val();
    jQuery("#scanSizeFlgContainer input").each(function () {

        var name = jQuery(this).attr("name");
        var value = jQuery(this).val();
        var parent = jQuery(this).attr("parent");

        if (parent)
        {
            //alert(value)
            obj.name = name;
            obj.value = value;
            obj.parent = parent;

            sizeData[cnt++] = obj;
            obj = {};
        }
    });

    return {
        sizeData: sizeData,
        qty: qty,
        type: "Scan",
        //unit:unit
    };

}



function saveScanSizeData() {

    var flg;


    if (jQuery("#chest").val() == "") {
//alert("Valid email address.");
        jQuery('#reqNeck').show();
        jQuery('#reqNeck').html("*This field is required.").css({"color": "red"});
        flg = false;
    }
    else
    {

        jQuery('#reqNeck').hide();
    }


    if (jQuery("#waist").val() == "") {
        jQuery('#reqWaist').show();
        jQuery('#reqWaist').html("*This field is required.").css({"color": "red"});
        flg = false;
    }
    else
    {
        jQuery('#reqWaist').hide();
    }

    if (jQuery("#hip").val() == "") {
//alert("Valid email address.");
        jQuery('#reqHip').show();
        jQuery('#reqHip').html("*This field is required.").css({"color": "red"});
        flg = false;
    }
    else
    {
        jQuery('#reqHip').hide();
    }


    if (jQuery("#shoulder").val() == "") {
        jQuery('#reqShoulder').show();
        jQuery('#reqShoulder').html("*This field is required.").css({"color": "red"});
        flg = false;
    }
    else
    {
        jQuery('#reqShoulder').hide();
    }


    if (jQuery("#sleeve").val() == "") {
        jQuery('#reqSleeve').show();
        jQuery('#reqSleeve').html("*This field is required.").css({"color": "red"});
        flg = false;
    }
    else
    {
        jQuery('#reqSleeve').hide();
    }

    if (jQuery("#length").val() == "") {
        jQuery('#reqLength').show();
        jQuery('#reqLength').html("*This field is required.").css({"color": "red"});
        flg = false;
    }
    else
    {
        jQuery('#reqLength').hide();
    }


    if (jQuery("#pwaist").val() == "") {
        jQuery('#reqPwaist').show();
        jQuery('#reqPwaist').html("*This field is required.").css({"color": "red"});
        flg = false;
    }
    else
    {
        jQuery('#reqPwaist').hide();
    }

    if (jQuery("#phip").val() == "") {
        jQuery('#reqPhip').show();
        jQuery('#reqPhip').html("*This field is required.").css({"color": "red"});
        flg = false;
    }
    else
    {
        jQuery('#reqPhip').hide();
    }

    if (jQuery("#crotch").val() == "") {
        jQuery('#reqCrotch').show();
        jQuery('#reqCrotch').html("*This field is required.").css({"color": "red"});
        flg = false;
    }
    else
    {
        jQuery('#reqCrotch').hide();
    }

    if (jQuery("#thigh").val() == "") {
        jQuery('#reqThigh').show();
        jQuery('#reqThigh').html("*This field is required.").css({"color": "red"});
        flg = false;
    }
    else
    {
        jQuery('#reqThigh').hide();
    }

    if (jQuery("#plength").val() == "") {
        jQuery('#reqPlength').show();
        jQuery('#reqPlength').html("*This field is required.").css({"color": "red"});
        flg = false;
    }
    else
    {
        jQuery('#reqPlength').hide();
    }

    sizeStdFlg = false;

    //return (flg == false) ? false : saveDesign(); 
    //return (flg == false) ? false : getProductImage();
    return (flg == false) ? false : getAllViewImages();

}


function camelToDash(str) {
    return str.replace(/\W+/g, ' ').replace(/([a-z\d])([A-Z])/g, '$1 $2');
}


var sizeDataArr = {};
function saveDesign() {

    jQuery(".loaderText").css({"display": "none"});
    jQuery("#theInitialProgress").css({"display": "block"});

    var user_id = "1";
    var product_id = jQuery("#productId").val();
    var quantity = "1";
    var productType = "Suit";

    var prodImg;
    var finalPrice = jQuery("#basePrice").text();
    var genderId = "Male";


    //get product selected attribute
    jQuery(".custom-part").each(function () {
        var optionVal = jQuery(this).attr('rel');
        var propertyVal = jQuery(this).attr('name') ? jQuery(this).attr('name') : "";
       
        var optionIcon = jQuery(this).attr('icon');

        selStyleArr[optionVal + " " + optionIcon] = camelToDash(propertyVal);


    });

    selStyleArr['vestFabricName icon-Fabric'] = vestFabricName;
    selStyleArr['pantFabricName icon-Fabric'] = pantFabricName;
    selStyleArr['jacketFabricName icon-Fabric'] = jacketFabricName;

    //end

console.log(finalImageArray)
    setTimeout(function () {

        prodImg = jQuery("#prev-holder img").attr("src");

        jQuery.post(basePath + "men-shirt/cart", {
            productType: productType,
            userId: user_id,
            product_id:product_id,
            style_data: selStyleArr,
            size_Data_Arr: sizeDataArr,
            qty: quantity,
            price: finalPrice,
            gender: genderId,
            category_id: productId,
            did: did,
            action: "save",
            prodImg: prodImg,
            final_image_data:finalImageArray
        }).done(function (data) {
          console.log('selStyleArr=');console.log(selStyleArr);

          if(data == "done")
          {
            window.location.href=basePath+'checkout/cart';
          }
          if(data == "fail")
          {
            alert('cannot add to cart, product is out of stock')
            window.location.href=basePath+'checkout/cart';
          }

            if (data)
            {
                // did = data;
                // document.location.href = basePath + 'cart/?add-to-cart=' + productId + '&did=' + did + '&prc=' + finalPrice + '&qty=' + quantity;
                // console.log(did);
        }
        });
    }, 500);
}

function getProductImage()
{

    /*jQuery('.mainToolarea').css({
     'height': '1320px'
     });
     jQuery('.mainToolarea').css({
     'width': '1080px'
     });
     //setTimeout(function () {
     jQuery('#mainImageHolder img').css({
     'width': 'unset'
     });

     html2canvas(document.getElementsByClassName('mainToolarea'), {
     onrendered: function (can) {
     var data = can.toDataURL('image/png');
     var image = new Image();
     image.height = 1320;
     image.width = 1080;
     image.src = data;
     var myEl = angular.element(document.querySelector('#prev-holder'));
     myEl.append(image);
     setTimeout(function () {
     jQuery("#mainImageHolder img").css({"width": "100%"});

     }, 100)
     }
     });*/

    //get size data
    if (sizeStdFlg)
    {
        sizeDataArr = getStdSizeData();
    }
    else {
        sizeDataArr = getScanSizeData();
    }


    console.log("sizeDataArr=");console.log(sizeDataArr);
    document.getElementById("prev-holder").innerHTML = "";

    jQuery(".loaderText").css({"display": "none"});
    jQuery("#theInitialProgress").css({"display": "block"});
    jQuery(".view-thumb").hide();

    html2canvas(jQuery('.rtldf'), {
        onrendered: function (can) {
            var data = can.toDataURL('image/png');
            var image = new Image();
            image.src = data;
            image.height = jQuery('#view1 img:first').css('height').replace(/[^-\d\.]/g, '');
            image.width = jQuery('#view1 img:first').css('width').replace(/[^-\d\.]/g, '');
            var myEl = angular.element(document.querySelector('#prev-holder'));
            myEl.append(image);

            saveDesign();

        }
    });

    /*if (jQuery(window).width() <= 900) {//for respnsive

     jQuery(".loaderText").css({"display": "none"});
     jQuery("#theInitialProgress").css({"display": "block"});

     jQuery('#mainImageHolder').css("height", jQuery("#view1 img:first").css("height").replace(/[^-\d\.]/g, ''));
     jQuery('#mainImageHolder').css("width", jQuery("#view1 img:first").css("width").replace(/[^-\d\.]/g, ''));


     html2canvas(jQuery('#mainImageHolder'), {
     onrendered: function (can) {

     var data = can.toDataURL('image/png');
     var image = new Image();
     image.src = data;
     image.height = jQuery("#view1 img:first").css("height").replace(/[^-\d\.]/g, '');
     image.width = jQuery("#view1 img:first").css("width").replace(/[^-\d\.]/g, '');
     var myEl = angular.element(document.querySelector('#prev-holder'));
     myEl.append(image);

     }
     });
     } else {

     jQuery("#theInitialProgress").css({"display": "block"});
     jQuery(".container-fluid").css({"opacity": "0.3"});

     jQuery("#mainImageHolder").css({"height": "1320px"});
     jQuery("#mainImageHolder").css({"width": "1080px"});
     jQuery(".mainToolarea").css({"height": "1320px"});
     jQuery(".mainToolarea").css({"width": "1080px"});


     setTimeout(function () {


     html2canvas(jQuery('#mainImageHolder'), {
     onrendered: function (can) {

     var data = can.toDataURL('image/png');
     var image = new Image();
     image.height = 1320;
     image.width = 1080;

     image.src = data;
     var myEl = angular.element(document.querySelector('#prev-holder'));
     myEl.append(image);

     }
     });
     }, 100);

     }*/

}

function isNumberKey(evt)
{
    var charCode = (evt.which) ? evt.which : event.keyCode;
    console.log(charCode);
    if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
        return false;

    return true;
}

function getFabricPrice()
{
    var fabPrice ="0.0";
    fabPrice = jQuery("#main-container .fabvie.activeDesign .price").text();
    console.log("fabPrice="+fabPrice)
    return fabPrice;
}



function updatePrice()
{
    //basePrice

    console.log("in price")

    var totPrice = "0.0";

    var basePrice = jQuery("#basePrice").attr("orgBasePrice");


    totPrice = parseFloat(basePrice) + parseFloat(getFabricPrice()) + parseFloat(getStylePrice());

    console.log("Baseprice "+parseFloat(basePrice) +" getFabricPrice "+ parseFloat(getFabricPrice()) +" getStylePrice "+ parseFloat(getStylePrice()) );
    console.log("totPrice="+totPrice);
    jQuery("#basePrice").text(totPrice.toFixed(2));

}

function getStylePrice()
{
    var stylePrice = '0.0';

    for (var key in stylePriceArr) {
        if (stylePriceArr.hasOwnProperty(key)) {
            stylePrice = parseFloat(stylePrice) + parseFloat(stylePriceArr[key]);
          //  console.log(" stylePriceArr[" + key + "]= " + stylePriceArr[key] + " price = " + stylePrice);
        }
    }

    return stylePrice;

// var stylePrice = 0.0;
//   var a=jQuery("#div1 .box_options>div");
//
//    for (var i = 1; i <= a.length; i++) {
//        if (stylePriceArr.hasOwnProperty(i)) {
////            if (key !== "undefined") {
//            stylePrice = parseFloat(stylePrice) + parseFloat(stylePriceArr[i]);
//            //console.log(" stylePriceArr[" + i + "]= " + stylePriceArr[i] + " price = " + stylePrice)
//            //}
//        }
//    }
//    console.log('style  price -== ' + stylePrice)
//    return stylePrice;


}

function getAccentPrice()
{
    var stylePrice = '0.0';


    return stylePrice;
}

var finalImageArray = {};

function getAllViewImages()
{
    generatePngfinal("1");
}

function generatePngfinal(v)
{
    var tempArray = {};

    if (sizeStdFlg)
    {
        sizeDataArr = getStdSizeData();
    } else {
        sizeDataArr = getScanSizeData();
    }

    jQuery(".loaderText").css({"display": "none"});
    jQuery("#theInitialProgress").css({"display": "block"});
    jQuery(".view-thumb").css("display","none");


    if (v <= jQuery(".theView").length)
    {
       
        jQuery("#mainImageHolder .theView").removeClass("active");
        jQuery("#view"+v).addClass("active");

       
            html2canvas(jQuery('.rtldf'), {
                onrendered: function(can) {
                    var data = can.toDataURL('image/png');
    
                    tempArray["dataurl"] = data;
                    finalImageArray[v] = tempArray;
                    tempArray = {};
                    v++;
                    generatePngfinal(v);
                    
        
                }
            });
       
        
    } else {
        jQuery(".view-thumb").css("display","block");
        
        jQuery("#Front").trigger("click");
        jQuery("#view1").addClass("active");
        console.log("generation complete");

        saveDesign();

    }
}
