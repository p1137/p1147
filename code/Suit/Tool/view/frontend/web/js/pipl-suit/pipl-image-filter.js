
jQuery(document).ready(function () {
    bindImageCropButtonEvents();
    bindImgFilterEvents();
});

function bindImageCropButtonEvents()
{
    theCropCanvas = new fabric.Canvas("canvasCrop");

    // image crop
    jQuery("#btnRectCrop, #btnCircleCrop, #btnTriCrop, #btnHeartCrop, #btnStarCrop").bind("click", function () {
        addCropShape(jQuery(this).attr("data-type"));
    });

    jQuery("#crop_image a").click(function () {
        setTimeout(shownCropCanvas, 1000);
    });
}



function addCropShape(theShape)
{
    var objects = theCropCanvas.getObjects();
    var countObj = objects.length;

    for (var i = 0; i < countObj; i++)
    {
        if (objects[i].type != "image")
        {
            theCropCanvas.remove(objects[i]);
        }
    }

    var shape;
    switch (theShape) {
        case "circle":
            shape = new fabric.Ellipse({
                rx: 50,
                ry: 50,
                fill: '#e5e5e5',
                stroke: "#d5d5d5"
            });
            break;
        case "rectangle":
            shape = new fabric.Rect({
                fill: '#e5e5e5',
                stroke: "#d5d5d5",
                width: 80,
                height: 80
            });
            break;
        case "heart":
            shape = new fabric.Path("m 59.668823,31.432556 c 24.123474,-69.195496 118.623717,0 0,88.964664 -118.617737,-88.964664 -24.117126,-158.160159 0,-88.964664 z", {fill: "#e5e5e5", stroke: "#d5d5d5"});
            break;
        case "star":
            shape = new fabric.Path("M4.431608805331052,22.856710265341917 L22.766170657021576,22.856710265341917 L28.431660506670596,5.419932953550699 L34.09715230425354,22.856710265341917 L52.431682989001274,22.856710265341917 C47.48740203931811,26.448837703876507 42.54312108963495,30.040965142411096 37.59884013995179,33.633092580945686 L43.264616335887695,51.06982162864824 L28.431660506670596,40.29318190457161 L13.598715391090082,51.06982162864824 L19.264543207274983,33.633092580945686 L4.431608805331052,22.856710265341917 L4.431608805331052,22.856710265341917 z", {fill: "#e5e5e5", stroke: "#d5d5d5"});
            break;
        case "triangle":
            shape = new fabric.Triangle({
                fill: '#e5e5e5',
                stroke: "#d5d5d5"
            });
            break;
    }
    var theScale = 1.5;
    if (theShape == 'star')
        theScale = 3;

    shape.set({
        left: 150,
        top: 150,
        strokeWidth: 1,
        opacity: 0.8,
        lockRotation: false
    }).scale(theScale).setCoords();

    theCropCanvas.add(shape);
    theCropCanvas.centerObject(shape);
    shape.setCoords();
    theCropCanvas.renderAll();
    theCropCanvas.renderAll.bind(theCropCanvas);
    theCropCanvas.calcOffset();
    theCropCanvas.setActiveObject(shape);
}

function shownCropCanvas()
{
    jQuery("#crop_img_Popup").find(".canvas-container ").css({left: "0px", top: "0px"});

    var srcObject = fabric.util.object.clone(canvas.getActiveObject());
    theCropCanvas.clear();

    srcObject.set("active", false);
    srcObject.hascontrols = false;
    srcObject.selectable = false;

    theCropCanvas.add(srcObject);
    theCropCanvas.renderAll.bind(theCropCanvas);
    theCropCanvas.calcOffset();
    addCropShape("circle");
}


function sendToCrop()
{   // get bg data
    theCropCanvas.getObjects()[1].set("active", false);

    theCropCanvas.getObjects()[1].opacity = 0;
    theCropCanvas.renderAll();
    var srcData = theCropCanvas.toDataURL("png");
    theCropCanvas.getObjects()[0].opacity = 0;
    theCropCanvas.getObjects()[1].opacity = 1;

    theCropCanvas.getObjects()[1].set("fill", "#FFFFFF");
    theCropCanvas.getObjects()[1].set("stroke", "#FFFFFF");
    theCropCanvas.backgroundColor = "#000000";
    theCropCanvas.renderAll();
    var shapeData = theCropCanvas.toDataURL("png");

    theCropCanvas.getObjects()[0].opacity = 1;
    theCropCanvas.getObjects()[1].opacity = 0.8;
    theCropCanvas.getObjects()[1].selectable = true;
    theCropCanvas.backgroundColor = "transparent";
    theCropCanvas.renderAll();
    
    jQuery.post(basePath + "custom_ajax/save-crop-image.php", {src: srcData, dest: shapeData}, function (msg) {
        fabric.Image.fromURL(basePath + msg.croppedImage, function (image) {
            console.log("img = " + basePath + msg.croppedImage)
            var theScaleFactor = getScaleFactor(image.width, image.height);
            var theTempActive = canvas.getActiveObject();

            image.set({
                left: theScaleFactor.x,
                top: theScaleFactor.y,
                angle: 0,
                identity: theTempActive.get('identity'),
                centerTransform: true
            }).setCoords();
            canvas.add(image);

            canvas.setActiveObject(image);

            jQuery("#crop_img_Popup .close-btn").trigger('click');

            canvas.remove(theTempActive);
            canvas.renderAll();
        });
    }, "json");
}

function bindImgFilterEvents() {

    f = fabric.Image.filters;
    jQuery('#imgFiltergrayscale').bind("click", function () {
        applyFilter(0, this.checked && new f.Grayscale());
    });

    jQuery('#imgFilterinvert').bind("click", function () {
        applyFilter(1, this.checked && new f.Invert());
    });

    jQuery('#imgFilterremove-white').bind("click", function () {
        applyFilter(2, this.checked && new f.RemoveWhite({
            threshold: jQuery('remove-white-threshold').value,
            distance: jQuery('remove-white-distance').value
        }));
    });

    jQuery('#imgFilterremove-white-threshold').bind("change", function () {
        applyFilterValue(2, 'threshold', this.value);
    });

    jQuery('#imgFilterremove-white-distance').bind("change", function () {
        applyFilterValue(2, 'distance', this.value);
    });

    jQuery('#imgFiltersepia2').bind("click", function () {
        applyFilter(3, this.checked && new f.Sepia2());
    });

    jQuery('#imgFilterbrightness').bind("click", function () {
        applyFilter(4, this.checked && new f.Brightness({
            brightness: parseInt(jQuery('brightness-value').value, 10)
        }));
    });

    jQuery('#imgFilterbrightness-value').bind("change", function () {
        applyFilterValue(4, 'brightness', parseInt(this.value, 10));
    });

    jQuery('#imgFilternoise').bind("click", function () {
        applyFilter(5, this.checked && new f.Noise({
            noise: parseInt(jQuery('noise-value').value, 10)
        }));
    });

    jQuery('#imgFilternoise-value').bind("change", function () {
        applyFilterValue(5, 'noise', parseInt(this.value, 10));
    });


    jQuery('#imgFilterblur').bind("click", function () {
        applyFilter(6, this.checked && new f.Convolute({
            matrix: [1 / 9, 1 / 9, 1 / 9,
                1 / 9, 1 / 9, 1 / 9,
                1 / 9, 1 / 9, 1 / 9]
        }));
    });

    jQuery('#imgFiltersharpen').bind("click", function () {
        applyFilter(7, this.checked && new f.Convolute({
            matrix: [0, -1, 0,
                -1, 5, -1,
                0, -1, 0]
        }));
    });

    jQuery('#imgFilteremboss').bind("click", function () {
        applyFilter(8, this.checked && new f.Convolute({
            matrix: [1, 1, 1,
                1, 0.7, -1,
                -1, -1, -1]
        }));
    });

    /*var tempTest = (navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia);
     if (tempTest)
     {
     jQuery("#btnLoadWebCam").bind("click", function() {
     
     jQuery("#winWebcam").html('').load("webcam-captures", {}, function() {
     jQuery("#winWebcam").modal("show");
     });
     });
     }
     else
     {
     jQuery("#btnLoadWebCam").hide();
     }
     
     jQuery("#btnLoadCliparts").bind("click", loadCliparts);*/

}

// Image Filters end
function applyFilter(index, filter) {
    var obj = canvas.getActiveObject();
    obj.filters[index] = filter;
    obj.applyFilters(canvas.renderAll.bind(canvas));
}

function applyFilterValue(index, prop, value) {
    var obj = canvas.getActiveObject();
    if (obj.filters[index]) {
        obj.filters[index][prop] = value;
        obj.applyFilters(canvas.renderAll.bind(canvas));
    }
}
