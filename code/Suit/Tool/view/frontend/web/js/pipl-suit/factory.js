angular.module("Suit").factory("viewService", function ($http) {
//var basePath = jQuery("#basePath").val();

    var viewDataSuit = [
        {id: '1', name: 'view1', viewimages: [
                {id: '41', rel: 'TopCoatCollarFold', label: "", type: 'topcoat', name: 'Man', classname: 'shirt_part part', img: basePath + 'media/default_tool_img/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '44', rel: 'TopCoatLining', label: "", type: 'topcoat', name: 'Man', classname: 'shirt_part part', img: basePath + 'media/default_tool_img/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
//                  {id: '0', rel: 'ViewStatic', label: "", type: 'jacket', name: 'Man', classname: 'shirt_part part', img: basePath + 'media/default_tool_img/view_static_1.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
//                {id: '1', rel: 'Man', label: "", type: 'jacket', name: 'Man', classname: 'shirt_part part', img: basePath + 'media/default_tool_img/Man_Front_Static.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '2', rel: 'Shirt', label: "", type: 'jacket', name: 'Man Shirt', classname: 'shirt_part part', img: basePath + 'media/default_tool_img/Shirt01.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
//                {id: '3', rel: 'ManShoes', icon: 'icon-icon-31', label: "", type: 'jacket', name: 'Black', classname: 'shirt_part part custom-part', img: basePath + 'media/default_tool_img/Shoes_Front_Black.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                
                {id: '27', rel: 'ManBow', label: "", type: 'shirt', name: 'Man', classname: 'shirt_part', img: basePath + 'media/default_tool_img/Mens_Suit_bow_tie.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '4', rel: 'Tie',  label: "", type: 'shirt', name: 'Man', classname: 'shirt_part', img: basePath + 'media/default_tool_img/Tie_Black.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '7', rel: 'pantfit', icon: 'icon-icon-87', label: "", type: 'pant', name: 'RegularFit', classname: 'shirt_part custom-part', img: basePath + 'media/default_tool_img/1_pantfit_1_view_1.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '11', rel: 'pantpockets', icon: 'icon-icon-98', label: "", type: 'pant', name: 'DiagonalPocket', classname: 'shirt_part custom-part', img: basePath + 'media/default_tool_img/1_pantpockets_2_view_1.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},

                {id: '9', rel: 'pantpleats', icon: 'icon-icon-90', label: "", type: 'pant', name: 'NoPleates', classname: 'shirt_part custom-part', img: basePath + 'media/default_tool_img/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '10', rel: 'fastening', icon: 'icon-icon-95', label: "", type: 'pant', name: 'Centered', classname: 'shirt_part custom-part', img: basePath + 'media/default_tool_img/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '10', rel: 'fasteningButton', icon: 'icon-icon-95', label: "", type: 'pant', name: 'Centered', classname: 'shirt_part custom-part', img: basePath + 'media/default_tool_img/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '12', rel: 'pantcuff', icon: 'icon-icon-109', label: "", type: 'pant', name: 'NoCuffs', classname: 'shirt_part custom-part', img: basePath + 'media/default_tool_img/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '85', rel: 'beltloop', label: "", btnCount: '1', type: 'pant', name: 'style buttons', classname: 'shirt_part custom-part', img: basePath + 'media/default_tool_img/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '8', rel: 'pantStyle', icon: '', label: "", type: 'pant', name: 'No', classname: 'shirt_part', img: basePath + 'media/default_tool_img/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '13', rel: 'veststyle', icon: 'icon-icon-122', label: "", id: "1", btnCount: '1', type: 'vest', name: 'SingleBreasted', classname: 'shirt_part custom-part', img: basePath + 'media/default_tool_img/1_style_1_bottom_1_view_1.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '14', rel: 'vestlapel', icon: 'icon-icon-131', label: "", id: "1", type: 'vest', name: 'Notch', classname: 'shirt_part custom-part', img: basePath + 'media/default_tool_img/1_style_1_lapel_1_view_1.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '15', rel: 'vestedge', icon: 'icon-icon-135', label: "", id: '1', type: 'vest', name: 'Straight', classname: 'shirt_part custom-part', img: basePath + 'media/default_tool_img/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '16', rel: 'vestpockets', icon: 'icon-icon-145', label: "", id: '1', type: 'vest', name: 'FlapPocket', classname: 'shirt_part custom-part', img: basePath + 'media/default_tool_img/1_vestpocket_1_view_1.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '16', rel: 'breastpocket', icon: 'icon-icon-139', label: "", id: '1', type: 'vest', name: 'BreastPocket', classname: 'shirt_part custom-part', img: basePath + 'media/default_tool_img/1_breastpocket_view_1.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '16', rel: 'ticketpockets', icon: '', label: "", id: '1', type: 'vest', name: 'no', classname: 'shirt_part custom-part', img: basePath + 'media/default_tool_img/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '22', rel: 'vestbuttons', icon: 'icon-icon-123', label: "", type: 'vest', name: 'SingleBreasted3ButtonButton', classname: 'shirt_part custom-part', img: basePath + 'media/default_tool_img/1_buttons_3_color_1.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '17', rel: 'Style', icon: 'icon-icon-40', label: "", btnCount: '1', id: "1", type: 'jacket', name: 'Single Breasted 1 Button', classname: 'shirt_part custom-part', img: basePath + 'media/default_tool_img/1_style_1_view_1.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '58', rel: 'Sleeves', icon: '', label: "", type: 'jacket', name: 'No', classname: 'shirt_part', img: basePath + 'media/default_tool_img/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '20', rel: 'Pockets', icon: 'icon-icon-55', label: "", type: 'jacket', name: 'Regular Flap Pocket ', classname: 'shirt_part custom-part', img: basePath + 'media/default_tool_img/1_pockets_1_view_1.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '18', rel: 'Fit', icon: 'icon-icon-32', label: "", type: 'jacket', name: 'Slim Fit', classname: 'shirt_part custom-part', img: basePath + 'media/default_tool_img/Slim-Fit_Mask.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '23', rel: 'pocketsquare', icon: 'icon-icon-7', label: "", btnCount: '1', type: 'jacket', name: 'Lightblue', classname: 'shirt_part custom-part', img: basePath + 'media/default_tool_img/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '55', rel: 'jacketbreastpocket', label: "", id: '1', type: 'jacket', name: 'Slim Fit', classname: 'shirt_part ', img: basePath + 'media/default_tool_img/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '19', rel: 'Lapel', icon: 'icon-icon-44', label: "", type: 'jacket', name: 'Notch Slim', classname: 'shirt_part custom-part', img: basePath + 'media/default_tool_img/1_lapelstyle_1_size_1_lapeltype_1_view_1.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                
                {id: '21', rel: 'threads', icon: 'icon-icon-31', label: "", btnCount: '1', type: 'jacket', name: 'Yellow', classname: 'shirt_part custom-part', img: basePath + 'media/default_tool_img/1_threads_1_btn_1_view_1.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '22', rel: 'buttons', icon: 'icon-icon-32', label: "", btnCount: '1', type: 'jacket', name: '1 button', classname: 'shirt_part custom-part', img: basePath + 'media/default_tool_img/1_buttons_1_color_4.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '30', rel: 'ButtonsColor', icon: 'icon-icon-31', label: "", btnCount: '1', type: 'jacket', name: 'Black', classname: 'shirt_part custom-part', img: basePath + 'media/default_tool_img/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
//                {id: '0', rel: 'ViewStatic', label: "", type: 'jacket', name: 'Man', classname: 'shirt_part part', img: basePath + 'media/default_tool_img/view_static_1.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},

                {id: '40', rel: 'topcoatstyle', label: "", type: 'topcoat', name: 'Man', classname: 'shirt_part part', img: basePath + 'media/default_tool_img/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '41', rel: 'TopCoatCollar', label: "", type: 'topcoat', name: 'Man', classname: 'shirt_part part', img: basePath + 'media/default_tool_img/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '42', rel: 'TopCoatPocket', label: "", type: 'topcoat', name: 'Man', classname: 'shirt_part part', img: basePath + 'media/default_tool_img/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '42', rel: 'TopCoatBreastPocket', label: "", type: 'topcoat', name: 'Man', classname: 'shirt_part part', img: basePath + 'media/default_tool_img/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '42', rel: 'TopCoatSleeve', label: "", type: 'topcoat', name: 'Man', classname: 'shirt_part part', img: basePath + 'media/default_tool_img/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '46', rel: 'topcoatbuttons', label: "", type: 'topcoat', name: 'Man', classname: 'shirt_part part', img: basePath + 'media/default_tool_img/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0}
                
            ]},
        {id: '2', name: 'view2', viewimages: [
//                {id: '1', rel: 'Man', label: "", type: 'jacket', name: 'Man', classname: 'shirt_part part', img: basePath + 'media/default_tool_img/new_back.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '2', rel: 'Shirt', label: "", type: 'jacket', name: 'Man Shirt', classname: 'shirt_part part', img: basePath + 'media/default_tool_img/Shirt_Back_Formal.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
//                {id: '3', rel: 'ManShoes', label: "", type: 'jacket', name: 'Man Shoe', classname: 'shirt_part part', img: basePath + 'media/default_tool_img/Shoes_Back_Black.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '5', rel: 'backpantfit', label: "", type: 'pant', name: 'Regular Fit', classname: 'shirt_part ', img: basePath + 'media/default_tool_img/1_pantfit_1_view_2.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '6', rel: 'pantBackStyle', label: "", type: 'jacket', name: 'Slim Fit', classname: 'shirt_part ', img: basePath + 'media/default_tool_img/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '85', rel: 'backbeltloop', label: "", btnCount: '1', type: 'pant', name: 'style buttons', classname: 'shirt_part custom-part', img: basePath + 'media/default_tool_img/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '58', rel: 'Sleevesback', icon: '', label: "", type: 'jacket', name: 'No', classname: 'shirt_part', img: basePath + 'media/default_tool_img/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '4', rel: 'backveststyle', label: "", type: 'vest', name: 'Tie', classname: 'shirt_part', img: basePath + 'media/default_tool_img/1_style_1_bottom_1_view_2.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '7', rel: 'BackStyle', label: "", type: 'jacket', name: 'Slim Fit', classname: 'shirt_part', img: basePath + 'media/default_tool_img/1_style_1_view_2.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '18', rel: 'FitBack', icon: 'icon-icon-32', label: "", type: 'jacket', name: 'Slim Fit', classname: 'shirt_part custom-part', img: basePath + 'media/default_tool_img/Slim-Fit_Mask_back.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '8', rel: 'Vents', icon: 'icon-icon-69', label: "", type: 'jacket', name: 'NoVent', classname: 'shirt_part custom-part', img: basePath + 'media/default_tool_img/Back_Jacket_Center_Vent_glow.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},

                {id: '9', rel: 'sleeve_buttonsThread', icon: 'icon-icon-66', label: "", type: 'jacket', name: '2Button', classname: 'shirt_part custom-part', img: basePath + 'media/default_tool_img/2_color_1.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '9', rel: 'sleeve_buttons', icon: 'icon-icon-66', label: "", type: 'jacket', name: '2Button', classname: 'shirt_part custom-part', img: basePath + 'media/default_tool_img/2_color_1.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '10', rel: 'backpantcuff', label: "", type: 'pant', name: '', classname: 'shirt_part', img: basePath + 'media/default_tool_img/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '11', rel: 'backcollar', icon: 'icon-icon-17', label: "", type: 'jacket', name: 'No Back Collar', classname: 'shirt_part custom-part', img: basePath + 'media/default_tool_img/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '51', rel: 'backcollaraccent', icon: 'icon-icon-17', label: "", type: 'jacket', name: 'No Back Collar', classname: 'shirt_part custom-part', img: basePath + 'media/default_tool_img/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '12', rel: 'backelbow', icon: 'icon-icon-80', label: "", type: 'jacket', name: 'No Elbo Patch', classname: 'shirt_part custom-part', img: basePath + 'media/default_tool_img/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '40', rel: 'TopCoatStyleBack', label: "", type: 'topcoat', name: 'Man', classname: 'shirt_part part', img: basePath + 'media/default_tool_img/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '41', rel: 'TopCoatCollarBack', label: "", type: 'topcoat', name: 'Man', classname: 'shirt_part part', img: basePath + 'media/default_tool_img/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '42', rel: 'TopCoatSleeveBack', label: "", type: 'topcoat', name: 'Man', classname: 'shirt_part part', img: basePath + 'media/default_tool_img/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '43', rel: 'TopCoatSleeveButton', label: "", type: 'topcoat', name: 'Man', classname: 'shirt_part part', img: basePath + 'media/default_tool_img/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0}
            ]},
        {id: '3', name: 'view3', viewimages: [
                {id: '18', rel: 'lining', icon: 'icon-icon-1', label: "", btnCount: '1', id: "1", type: 'jacket', name: 'Upper Side', classname: 'shirt_part custom-part', img: basePath + 'media/default_tool_img/1_liningstyle_1_view_3.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
//                {id: '2', rel: 'Shirt', label: "", type: 'jacket', name: 'Man Shirt', classname: 'shirt_part part', img: basePath + 'media/default_tool_img/Shirt_Front_Formal.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '18', rel: 'vestlining', label: "", btnCount: '1', id: "1", type: 'jacket', name: 'Single Breasted 1 Button', classname: 'shirt_part', img: basePath + 'media/default_tool_img/1_vestlining_view_3.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
//                {id: '18', rel: 'lining', label: "", btnCount: '1', id: "1", type: 'jacket', name: 'Single Breasted 1 Button', classname: 'shirt_part', img: basePath + 'media/default_tool_img/1_liningstyle_1_view_3.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '15', rel: 'vestcollar', label: "", btnCount: '1', id: "1", type: 'jacket', name: 'Single Breasted 1 Button', classname: 'shirt_part ', img: basePath + 'media/default_tool_img/1_vestcollar_view_3.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '13', rel: 'veststyle', label: "", id: "1", btnCount: '1', type: 'vest', name: 'Single Breasted 1 Button', classname: 'shirt_part', img: basePath + 'media/default_tool_img/1_style_1_bottom_1_view_1.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '14', rel: 'vestlapel', label: "", id: "1", type: 'vest', name: 'Slim Fit', classname: 'shirt_part ', img: basePath + 'media/default_tool_img/1_style_1_lapel_1_view_1.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '15', rel: 'vestedge', label: "", id: '1', type: 'vest', name: 'Slim Fit', classname: 'shirt_part ', img: basePath + 'media/default_tool_img/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '16', rel: 'vestpockets', label: "", id: '1', type: 'vest', name: 'Slim Fit', classname: 'shirt_part ', img: basePath + 'media/default_tool_img/1_vestpocket_1_view_1.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '16', rel: 'breastpocket', label: "", id: '1', type: 'vest', name: 'Slim Fit', classname: 'shirt_part ', img: basePath + 'media/default_tool_img/1_breastpocket_view_1.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},

                {id: '22', rel: 'vestbuttons', label: "", type: 'vest', name: 'style buttons', classname: 'shirt_part ', img: basePath + 'media/default_tool_img/1_buttons_3_color_1.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '15', rel: 'collarstyle', label: "", btnCount: '1', id: "1", type: 'jacket', name: 'Single Breasted 1 Button', classname: 'shirt_part ', img: basePath + 'media/default_tool_img/1_collarstyle_3_view_3.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},

                {id: '17', rel: 'Style', label: "", btnCount: '1', id: "1", type: 'jacket', name: 'Single Breasted 1 Button', classname: 'shirt_part ', img: basePath + 'media/default_tool_img/1_style_1_view_1.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '58', rel: 'Sleevesfold', icon: '', label: "", type: 'jacket', name: 'No', classname: 'shirt_part', img: basePath + 'media/default_tool_img/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '18', rel: 'Fit', icon: 'icon-icon-32', label: "", type: 'jacket', name: 'Slim Fit', classname: 'shirt_part custom-part', img: basePath + 'media/default_tool_img/Slim-Fit_Mask.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '23', rel: 'pocketsquare', label: "", btnCount: '1', type: 'jacket', name: 'style buttons', classname: 'shirt_part ', img: basePath + 'media/default_tool_img/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '20', rel: 'Pockets', label: "", type: 'jacket', name: 'No Pockets', classname: 'shirt_part ', img: basePath + 'media/default_tool_img/1_pockets_1_view_1.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '55', rel: 'jacketbreastpocket', label: "", id: '1', type: 'jacket', name: 'Slim Fit', classname: 'shirt_part ', img: basePath + 'media/default_tool_img/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '19', rel: 'Lapel', label: "", type: 'jacket', name: 'Notch', classname: 'shirt_part ', img: basePath + 'media/default_tool_img/1_lapelstyle_1_size_1_lapeltype_1_view_1.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '21', rel: 'threads', label: "", btnCount: '1', type: 'jacket', name: 'style buttons', classname: 'shirt_part ', img: basePath + 'media/default_tool_img/1_threads_1_btn_1_view_1.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '22', rel: 'buttons', label: "", btnCount: '1', type: 'jacket', name: 'style buttons', classname: 'shirt_part ', img: basePath + 'media/default_tool_img/1_buttons_1_color_4.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '23', rel: 'pantfit_straight', label: "", btnCount: '1', type: 'pant', name: 'style buttons', classname: 'shirt_part ', img: basePath + 'media/default_tool_img/1_pantfit_1_view_3.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '28', rel: 'pantfit_style', label: "", btnCount: '1', type: 'jacket', name: 'style buttons', classname: 'shirt_part custom-part', img: basePath + 'media/default_tool_img/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '25', rel: 'foldedPantPocket', label: "", btnCount: '1', type: 'pant', name: 'style buttons', classname: 'shirt_part ', img: basePath + 'media/default_tool_img/1_pantpockets_2_view_3.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
//                {id: '24', rel: 'pantfit_tilt', label: "", btnCount: '1', type: 'pant', name: 'style buttons', classname: 'shirt_part ', img: basePath + 'media/default_tool_img/1_pantfit_tilt_1_view_3.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '26', rel: 'pantFoldCuff', label: "", btnCount: '1', type: 'pant', name: 'style buttons', classname: 'shirt_part ', img: basePath + 'media/default_tool_img/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '27', rel: 'foldPantPleats', label: "", btnCount: '1', type: 'pant', name: 'style buttons', classname: 'shirt_part ', img: basePath + 'media/default_tool_img/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '85', rel: 'beltfoldloop', label: "", btnCount: '1', type: 'pant', name: 'style buttons', classname: 'shirt_part custom-part', img: basePath + 'media/default_tool_img/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '41', rel: 'TopCoatCollarFold', label: "", type: 'topcoat', name: 'Man', classname: 'shirt_part part', img: basePath + 'media/default_tool_img/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '44', rel: 'TopCoatLining', label: "", type: 'topcoat', name: 'Man', classname: 'shirt_part part', img: basePath + 'media/default_tool_img/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '40', rel: 'topcoatstyle', label: "", type: 'topcoat', name: 'Man', classname: 'shirt_part part', img: basePath + 'media/default_tool_img/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '41', rel: 'TopCoatCollar', label: "", type: 'topcoat', name: 'Man', classname: 'shirt_part part', img: basePath + 'media/default_tool_img/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '42', rel: 'TopCoatPocket', label: "", type: 'topcoat', name: 'Man', classname: 'shirt_part part', img: basePath + 'media/default_tool_img/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '42', rel: 'TopCoatBreastPocket', label: "", type: 'topcoat', name: 'Man', classname: 'shirt_part part', img: basePath + 'media/default_tool_img/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '43', rel: 'TopCoatSleeve', label: "", type: 'topcoat', name: 'Man', classname: 'shirt_part part', img: basePath + 'media/default_tool_img/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '46', rel: 'topcoatbuttons', label: "", type: 'topcoat', name: 'Man', classname: 'shirt_part part', img: basePath + 'media/default_tool_img/blank.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0}
            ]}
    ];
    return{
        getSuitView: function () {
            return viewDataSuit;
        }
    }
//end


});
angular.module("Suit").factory("designService", function ($http) {

    var designs = [
        {"id": "1", "name": "Fit", "flag": "false", img: basePath + "media/jacket_img/thumbnail/00Fit_Main_Icon.png", "target": "0", "style": [
                {"id": "1", "name": "Slim Fit", "parent": "Fit", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Fit_Slim.png", "prev": "", "live": [
                        {id: '1', name: 'Front', img: basePath + 'media/jacket_img/images/fit_red.png'},
                        {id: '2', name: 'Back', img: basePath + 'media/jacket_img/images/Jacket_slim_back_final.png'},
                        {id: '3', name: 'Inside', img: basePath + 'media/jacket_img/images/suit_1_fabric_13_style_1_view_1.png'}
                    ]},
                {"id": "2", "name": "Classic Fit", "parent": "Fit", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Fit_Classic.png", "prev": "", "live": [
                        {id: '1', name: 'Front', img: basePath + 'media/jacket_img/images/suit_1_fabric_13_style_3_view_1.png'},
                        {id: '2', name: 'Back', img: basePath + 'media/jacket_img/images/Jacket_regular_back_final.png'},
                        {id: '3', name: 'Inside', img: basePath + 'media/jacket_img/images/Jacket_slim_back_final.png'}
                    ]}
            ]},
        {"id": "2", "name": "Style", "flag": "false", img: basePath + "media/jacket_img/thumbnail/00_Style_Main_Icon.png", "target": "0", "style": [
                {"id": "1", "name": "Single-breasted 1 button", "parent": "Style", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Style_1_button_Single_breasted.png", "prev": "", "live": [
                        {id: '1', name: 'Front', img: basePath + 'media/jacket_img/images/suit_1_fabric_13_style_3_view_1.png'},
                        {id: '2', name: 'Back', img: basePath + 'media/jacket_img/images/blank.png'},
                        {id: '3', name: 'Inside', img: basePath + 'media/jacket_img/images/Jacket_slim_back_final.png'}
                    ]},
                {"id": "2", "name": "Single-breasted 2 buttons", "parent": "Style", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Style_2_button_Single_breasted.png", "prev": "media/jacket_img/preview/2_button_single_breasted.png", "live": [
                        {id: '1', name: 'Front', img: basePath + 'media/jacket_img/images/suit_1_fabric_13_style_3_view_1.png'},
                        {id: '2', name: 'Back', img: basePath + 'media/jacket_img/images/blank.png'},
                        {id: '3', name: 'Inside', img: basePath + 'media/jacket_img/images/Jacket_slim_back_final.png'}
                    ]},
                {"id": "3", "name": "Single-breasted 3 buttons", "parent": "Style", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Style_3_button_Single_breasted.png", "prev": "media/jacket_img/preview/3_button_single_breasted.png", "live": [
                        {id: '1', name: 'Front', img: basePath + 'media/jacket_img/images/suit_1_fabric_13_style_3_view_1.png'},
                        {id: '2', name: 'Back', img: basePath + 'media/jacket_img/images/blank.png'},
                        {id: '3', name: 'Inside', img: basePath + 'media/jacket_img/images/Jacket_slim_back_final.png'}
                    ]},
                {"id": "4", "name": "Double-breasted 2 buttons", "parent": "Style", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Style_2_button_Double_breasted.png", "prev": "media/jacket_img/preview/4_button_double_breasted_1_close.png", "live": [
                        {id: '1', name: 'Front', img: basePath + 'media/jacket_img/images/suit_1_fabric_13_style_1_view_1.png'},
                        {id: '2', name: 'Back', img: basePath + 'media/jacket_img/images/blank.png'},
                        {id: '3', name: 'Inside', img: basePath + 'media/jacket_img/images/Jacket_slim_back_final.png'}
                    ]},
                {"id": "5", "name": "Double-breasted 4 buttons", "parent": "Style", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Style_4_button_Double_breasted.png", "prev": "media/jacket_img/preview/4_button_double_breasted_2_close.png", "live": [
                        {id: '1', name: 'Front', img: basePath + 'media/jacket_img/images/suit_1_fabric_13_style_1_view_1.png'},
                        {id: '2', name: 'Back', img: basePath + 'media/jacket_img/images/blank.png'},
                        {id: '3', name: 'Inside', img: basePath + 'media/jacket_img/images/Jacket_slim_back_final.png'}
                    ]},
                {"id": "6", "name": "Double-breasted 6 buttons", "parent": "Style", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Style_6_button_Double_breasted.png", "prev": "media/jacket_img/preview/6_button_double_breasted_2_close.png", "live": [
                        {id: '1', name: 'Front', img: basePath + 'media/jacket_img/images/suit_1_fabric_13_style_1_view_1.png'},
                        {id: '2', name: 'Back', img: basePath + 'media/jacket_img/images/blank.png'},
                        {id: '3', name: 'Inside', img: basePath + 'media/jacket_img/images/Jacket_slim_back_final.png'}
                    ]},
                {"id": "6", "name": "Mandarin", "parent": "Style", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Style_Mandarin.png", "prev": "media/jacket_img/preview/6_button_double_breasted_2_close.png", "live": [
                        {id: '1', name: 'Front', img: basePath + 'media/jacket_img/images/suit_1_fabric_13_style_2_view_11.png'},
                        {id: '2', name: 'Back', img: basePath + 'media/jacket_img/images/blank.png'},
                        {id: '3', name: 'Inside', img: basePath + 'media/jacket_img/images/Jacket_slim_back_final.png'}
                    ]}
            ]},
        {"id": "3", "name": "Lapel", "flag": "false", img: basePath + "media/jacket_img/thumbnail/Width_Wide.png", "target": "0", "style": [
                {"id": "1", "name": "Notch", "parent": "Lapel", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Lapel_Notch.png", "prev": "media/jacket_img/preview/Notch.png", "width": [
                        {id: '1', name: 'Slim', img: basePath + 'media/jacket_img/Width_Slim.png', live: [
                                {id: '1', name: 'Front', img: basePath + 'media/jacket_img/accessories/6_btn_single_breasted.png'},
                                {id: '2', name: 'Back', img: basePath + 'media/jacket_img/images/blank.png'},
                                {id: '3', name: 'Inside', img: basePath + 'media/jacket_img/images/Jacket_slim_back_final.png'}]},
                        {id: '2', name: 'Standard', img: basePath + 'media/jacket_img/Width_Standard.png', live: [
                                {id: '1', name: 'Front', img: basePath + 'media/jacket_img/accessories/6_btn_single_breasted.png'},
                                {id: '2', name: 'Back', img: basePath + 'media/jacket_img/images/blank.png'},
                                {id: '3', name: 'Inside', img: basePath + 'media/jacket_img/images/Jacket_slim_back_final.png'}]},
                        {id: '3', name: 'Wide', img: basePath + 'media/jacket_img/Width_Wide.png', live: [
                                {id: '1', name: 'Front', img: basePath + 'media/jacket_img/accessories/6_btn_single_breasted.png'},
                                {id: '2', name: 'Back', img: basePath + 'media/jacket_img/images/blank.png'},
                                {id: '3', name: 'Inside', img: basePath + 'media/jacket_img/images/Jacket_slim_back_final.png'}]}]},
                {"id": "2", "name": "Peak", "parent": "Lapel", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Lapel_Peak.png", "prev": "", "width": [
                        {id: '1', name: 'Slim', img: basePath + 'media/jacket_img/Width_Slim.png', live: [
                                {id: '1', name: 'Front', img: basePath + 'media/jacket_img/accessories/6_btn_single_breasted.png'},
                                {id: '2', name: 'Back', img: basePath + 'media/jacket_img/images/blank.png'},
                                {id: '3', name: 'Inside', img: basePath + 'media/jacket_img/images/Jacket_slim_back_final.png'}]},
                        {id: '2', name: 'Standard', img: basePath + 'media/jacket_img/Width_Standard.png', live: [
                                {id: '1', name: 'Front', img: basePath + 'media/jacket_img/accessories/6_btn_single_breasted.png'},
                                {id: '2', name: 'Back', img: basePath + 'media/jacket_img/images/blank.png'},
                                {id: '3', name: 'Inside', img: basePath + 'media/jacket_img/images/Jacket_slim_back_final.png'}]},
                        {id: '3', name: 'Wide', img: basePath + 'media/jacket_img/Width_Wide.png', live: [
                                {id: '1', name: 'Front', img: basePath + 'media/jacket_img/accessories/6_btn_single_breasted.png'},
                                {id: '2', name: 'Back', img: basePath + 'media/jacket_img/images/blank.png'},
                                {id: '3', name: 'Inside', img: basePath + 'media/jacket_img/images/Jacket_slim_back_final.png'}]}]},
                {"id": "3", "name": "Shawl", "parent": "Lapel", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Lapel_Shawl.png", "prev": "", "width": [
                        {id: '1', name: 'Slim', img: basePath + 'media/jacket_img/Width_Slim.png', live: [
                                {id: '1', name: 'Front', img: basePath + 'media/jacket_img/accessories/6_btn_single_breasted.png'},
                                {id: '2', name: 'Back', img: basePath + 'media/jacket_img/images/blank.png'},
                                {id: '3', name: 'Inside', img: basePath + 'media/jacket_img/images/Jacket_slim_back_final.png'}]},
                        {id: '2', name: 'Standard', img: basePath + 'media/jacket_img/Width_Standard.png', live: [
                                {id: '1', name: 'Front', img: basePath + 'media/jacket_img/accessories/6_btn_single_breasted.png'},
                                {id: '2', name: 'Back', img: basePath + 'media/jacket_img/images/blank.png'},
                                {id: '3', name: 'Inside', img: basePath + 'media/jacket_img/images/Jacket_slim_back_final.png'}]},
                        {id: '3', name: 'Wide', img: basePath + 'media/jacket_img/Width_Wide.png', live: [
                                {id: '1', name: 'Front', img: basePath + 'media/jacket_img/accessories/6_btn_single_breasted.png'},
                                {id: '2', name: 'Back', img: basePath + 'media/jacket_img/images/blank.png'},
                                {id: '3', name: 'Inside', img: basePath + 'media/jacket_img/images/Jacket_slim_back_final.png'}]}]}
            ]},
        {"id": "4", "name": "Pockets_Regular", img: basePath + "media/jacket_img/thumbnail/00_Pocket_Main_Icon.png", "flag": "false", "target": "0", "style": [
                //{"id": "1", "name": "Regular", "parent": "Pockets", "part": "1", "price": "0", "desc": "description", img: basePath + "", "prev": "", "live": [
                {id: '1', name: 'No Pocket', "parent": "Pockets_Regular", img: basePath + 'media/jacket_img/No_pocket-icon.png',
                    live: [
                        {id: '1', name: 'Front', img: basePath + 'media/jacket_img/accessories/6_btn_single_breasted.png'},
                        {id: '2', name: 'Back', img: basePath + 'media/jacket_img/images/blank.png'},
                        {id: '3', name: 'Inside', img: basePath + 'media/jacket_img/Jacket_slim_back_final.png'}
                    ]},
                {id: '2', name: 'Flap', "parent": "Pockets Regular", img: basePath + 'media/jacket_img/Pocket_Regular_Flap.png',
                    live: [
                        {id: '1', name: 'Front', img: basePath + 'media/jacket_img/accessories/6_btn_single_breasted.png'},
                        {id: '2', name: 'Back', img: basePath + 'media/jacket_img/images/blank.png'},
                        {id: '3', name: 'Inside', img: basePath + 'media/jacket_img/imagesJacket_slim_back_final.png'}
                    ]},
                {id: '3', name: 'Patch', "parent": "Pockets Regular", img: basePath + 'media/jacket_img/Pocket_Regular_Patch.png',
                    live: [
                        {id: '1', name: 'Front', img: basePath + 'media/jacket_img/accessories/6_btn_single_breasted.png'},
                        {id: '2', name: 'Back', img: basePath + 'media/jacket_img/images/blank.png'},
                        {id: '3', name: 'Inside', img: basePath + 'media/jacket_img/images/Jacket_slim_back_final.png'}
                    ]},
                {id: '4', name: 'Welted', "parent": "Pockets Regular", img: basePath + 'media/jacket_img/Pocket_Regular_Welted.png',
                    live: [
                        {id: '1', name: 'Front', img: basePath + 'media/jacket_img/accessories/6_btn_single_breasted.png'},
                        {id: '2', name: 'Back', img: basePath + 'media/jacket_img/images/blank.png'},
                        {id: '3', name: 'Inside', img: basePath + 'media/jacket_img/images/Jacket_slim_back_final.png'}
                    ]}
//]},
            ]},
        {"id": "5", "name": "Pockets_Slanted", img: basePath + "media/jacket_img/thumbnail/Pocket_Slanted_Flap.png", "flag": "false", "target": "0", "style": [
                {id: '1', name: 'No Pocket', "parent": "Pockets_Slanted", img: basePath + 'media/jacket_img/No_pocket-icon.png',
                    live: [
                        {id: '1', name: 'Front', img: basePath + 'media/jacket_img/accessories/6_btn_single_breasted.png'},
                        {id: '2', name: 'Back', img: basePath + 'media/jacket_img/images/blank.png'},
                        {id: '3', name: 'Inside', img: basePath + 'media/jacket_img/images/Jacket_slim_back_final.png'}
                    ]},
                {id: '2', name: 'Flap', "parent": "Pockets Slanted", img: basePath + 'media/jacket_img/Pocket_Slanted_Flap.png',
                    live: [
                        {id: '1', name: 'Front', img: basePath + 'media/jacket_img/accessories/6_btn_single_breasted.png'},
                        {id: '2', name: 'Back', img: basePath + 'media/jacket_img/images/blank.png'},
                        {id: '3', name: 'Inside', img: basePath + 'media/jacket_img/images/Jacket_slim_back_final.png'}
                    ]},
                {id: '3', name: 'Patch', "parent": "Pockets Slanted", img: basePath + 'media/jacket_img/Pocket_Slanted_Patch.png',
                    live: [
                        {id: '1', name: 'Front', img: basePath + 'media/jacket_img/accessories/6_btn_single_breasted.png'},
                        {id: '2', name: 'Back', img: basePath + 'media/jacket_img/images/blank.png'},
                        {id: '3', name: 'Inside', img: basePath + 'media/jacket_img/images/Jacket_slim_back_final.png'}
                    ]},
                {id: '4', name: 'Welted', "parent": "Pockets Slanted", img: basePath + 'media/jacket_img/Pocket_Slanted_Welted.png',
                    live: [
                        {id: '1', name: 'Front', img: basePath + 'media/jacket_img/accessories/6_btn_single_breasted.png'},
                        {id: '2', name: 'Back', img: basePath + 'media/jacket_img/images/blank.png'},
                        {id: '3', name: 'Inside', img: basePath + 'media/jacket_img/images/Jacket_slim_back_final.png'}
                    ]},
            ]},
        {"id": "6", "name": "Sleeve_Button", img: basePath + "media/jacket_img/thumbnail/00_vents_Main_Icon.png", "flag": "false", img: basePath + "media/jacket_img/thumbnail/00_Main_Icon.png", "target": "0", "style": [
                {"id": "1", "name": "0 Standard Button", "parent": "Sleeve_Button", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Sleeve_Button_2.png", "prev": "media/jacket_img/preview/01_Three_Standard_Buttons.png", "live": [
                        {id: '1', name: 'Front', img: basePath + 'media/jacket_img/accessories/3_btn_std_sleeve.png'},
                        {id: '2', name: 'Back', img: basePath + 'media/jacket_img/accessories/blank.png'}
                    ]},
                {"id": "2", "name": "2 Standard Button", "parent": "Sleeve Button", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Sleeve_Button_2.png", "prev": "media/jacket_img/preview/01_Three_Standard_Buttons.png", "live": [
                        {id: '1', name: 'Front', img: basePath + 'media/jacket_img/accessories/3_btn_std_sleeve.png'},
                        {id: '2', name: 'Back', img: basePath + 'media/jacket_img/accessories/blank.png'}
                    ]},
                {"id": "3", "name": "3 Standard Button", "parent": "Sleeve Button", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Sleeve_Button_3.png", "prev": "media/jacket_img/preview/02_Four_Standard_Buttons.png", "live": [
                        {id: '1', name: 'Front', img: basePath + 'media/jacket_img/accessories/4_btn_std_sleeve.png'},
                        {id: '2', name: 'Back', img: basePath + 'media/jacket_img/accessories/blank.png'}
                    ]},
                {"id": "4", "name": "4 Standard Button", "parent": "Sleeve Button", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Sleeve_Button_4.png", "prev": "media/jacket_img/preview/03_Five_Standard_Buttons.png", "live": [
                        {id: '1', name: 'Front', img: basePath + 'media/jacket_img/accessories/5_btn_std_sleeve.png'},
                        {id: '2', name: 'Back', img: basePath + 'media/jacket_img/accessories/blank.png'}
                    ]}
            ]},
        {"id": "7", "name": "Vents", img: basePath + "media/jacket_img/thumbnail/00_vents_Main_Icon.png", "flag": "false", "target": "1", "style": [
                {"id": "1", "name": "ventless", "parent": "Vents", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Vents_Side.png", "prev": "media/jacket_img/Vents_No_Vents.png", "live": [
                        {id: '1', name: 'Front', img: basePath + 'media/jacket_img/images/blank.png'},
                        {id: '2', name: 'Back', img: basePath + 'media/jacket_img/images/blank.png'},
                        {id: '3', name: 'Back', img: basePath + 'media/jacket_img/images/blank.png'}
                    ]},
                {"id": "2", "name": "Center Vent", "parent": "Vents", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Vents_No_Vents.png", "prev": "media/jacket_img/Vents_Center_Vents.png", "live": [
                        {id: '1', name: 'Front', img: basePath + 'media/jacket_img/images/blank.png'},
                        {id: '2', name: 'Back', img: basePath + 'media/jacket_img/images/blank.png'},
                        {id: '3', name: 'Back', img: basePath + 'media/jacket_img/images/blank.png'}
                    ]},
                {"id": "3", "name": "Side Vents", "parent": "Vents", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Vents_Center_Vents.png", "prev": "media/jacket_img/Vents_Side.png", "live": [
                        {id: '1', name: 'Front', img: basePath + 'media/jacket_img/images/blank.png'},
                        {id: '2', name: 'Back', img: basePath + 'media/jacket_img/accessories/jacket_vent.png'},
                        {id: '3', name: 'Back', img: basePath + 'media/jacket_img/images/blank.png'}
                    ]}
            ]}

    ];
    //load data static way
    /* return{
     getDesignData: function(e) {
     return designs;
     }
     }*/

    //load data dynamic way
    return {
        getDesignData: function (fabricId, genderId, productId) {

            return $http.post(basePath + 'men-shirt/men/getSuitDesign', {
                fabricId: fabricId,
                genderId: genderId,
                productId: productId
            });
        }
    };
});
angular.module("Suit").factory("fabricService", function ($http) {

    var fabrics = [
        {"id": "1", "name": "Fabric 1", img: basePath + "media/jacket_img/all_fabric_images/01.png", "target": "", "style": [
                {"id": "1", "name": "Violet", "code": "AA1", "price": "134", img: basePath + "media/jacket_img/all_fabric_images/01.png", "color": "Violet Light", "pattern": "Plain"}

            ]},
        {"id": "2", "name": "Fabric 2", img: basePath + "media/jacket_img/all_fabric_images/02.png", "target": "", "style": [
                {"id": "21", "name": "Blue1", "code": "AA3", "price": "130", img: basePath + "media/jacket_img/all_fabric_images/02.png", "color": "Blue", "pattern": "Plain"}
            ]},
        {"id": "3", "name": "Fabric 3", img: basePath + "media/jacket_img/all_fabric_images/03.png", "target": "", "style": [
                {"id": "31", "name": "Blue2", "code": "AA4", "price": "130", img: basePath + "media/jacket_img/all_fabric_images/03.png", "color": "Blue", "pattern": "Plain"}
            ]},
        {"id": "4", "name": "Fabric 4", img: basePath + "media/jacket_img/all_fabric_images/04.png", "target": "", "style": [
                {"id": "41", "name": "Blue3", "code": "AA6", "price": "130", img: basePath + "media/jacket_img/all_fabric_images/04.png", "color": "Blue", "pattern": "Plain"}
            ]},
        {"id": "5", "name": "Fabric 5", img: basePath + "media/jacket_img/all_fabric_images/05.png", "target": "", "style": [
                {"id": "51", "name": "Blue4", "code": "AA3", "price": "130", img: basePath + "media/jacket_img/all_fabric_images/05.png", "color": "Blue", "pattern": "Plain"}
            ]},
        {"id": "6", "name": "Fabric 6", img: basePath + "media/jacket_img/all_fabric_images/06.png", "target": "", "style": [
                {"id": "61", "name": "Blue5", "code": "AA3", "price": "130", img: basePath + "media/jacket_img/all_fabric_images/06.png", "color": "Blue", "pattern": "Plain"}
            ]},
        {"id": "7", "name": "Fabric 6", img: basePath + "media/jacket_img/all_fabric_images/07.png", "target": "", "style": [
                {"id": "71", "name": "Blue6", "code": "AA3", "price": "130", img: basePath + "media/jacket_img/all_fabric_images/07.png", "color": "Blue", "pattern": "Plain"}
            ]},
        {"id": "8", "name": "Fabric 7", img: basePath + "media/jacket_img/all_fabric_images/08.png", "target": "", "style": [
                {"id": "81", "name": "Blue7", "code": "AA3", "price": "130", img: basePath + "media/jacket_img/all_fabric_images/08.png", "color": "Blue", "pattern": "Plain"}
            ]},
        {"id": "9", "name": "Fabric 8", img: basePath + "media/jacket_img/all_fabric_images/09.png", "target": "", "style": [
                {"id": "91", "name": "Blue8", "code": "AA3", "price": "130", img: basePath + "media/jacket_img/all_fabric_images/09.png", "color": "Blue", "pattern": "Plain"}
            ]},
        {"id": "10", "name": "Fabric 9", img: basePath + "media/jacket_img/all_fabric_images/10.png", "target": "", "style": [
                {"id": "101", "name": "Blue9", "code": "AA3", "price": "130", img: basePath + "media/jacket_img/all_fabric_images/10.png", "color": "Blue", "pattern": "Plain"}
            ]},
        {"id": "11", "name": "Fabric 10", img: basePath + "media/jacket_img/all_fabric_images/11.png", "target": "", "style": [
                {"id": "111", "name": "Blue10", "code": "AA3", "price": "130", img: basePath + "media/jacket_img/all_fabric_images/11.png", "color": "Blue", "pattern": "Plain"}
            ]},
        {"id": "12", "name": "Fabric 11", img: basePath + "media/jacket_img/all_fabric_images/12.png", "target": "", "style": [
                {"id": "121", "name": "Blue611", "code": "AA3", "price": "130", img: basePath + "media/jacket_img/all_fabric_images/12.png", "color": "Blue", "pattern": "Plain"}
            ]},
        {"id": "13", "name": "Fabric 12", img: basePath + "media/jacket_img/all_fabric_images/13.png", "target": "", "style": [
                {"id": "131", "name": "Blue", "code": "AA3", "price": "130", img: basePath + "media/jacket_img/all_fabric_images/13.png", "color": "Blue", "pattern": "Plain"}
            ]},
        {"id": "14", "name": "Fabric 13", img: basePath + "media/jacket_img/all_fabric_images/14.png", "target": "", "style": [
                {"id": "141", "name": "Blue", "code": "AA3", "price": "130", img: basePath + "media/jacket_img/all_fabric_images/14.png", "color": "Blue", "pattern": "Plain"}
            ]},
        {"id": "15", "name": "Fabric 14", img: basePath + "media/jacket_img/all_fabric_images/15.png", "target": "", "style": [
                {"id": "151", "name": "Blue", "code": "AA3", "price": "130", img: basePath + "media/jacket_img/all_fabric_images/15.png", "color": "Blue", "pattern": "Plain"}
            ]},
        {"id": "16", "name": "Fabric 15", img: basePath + "media/jacket_img/all_fabric_images/16.png", "target": "", "style": [
                {"id": "161", "name": "Blue", "code": "AA3", "price": "130", img: basePath + "media/jacket_img/all_fabric_images/16.png", "color": "Blue", "pattern": "Plain"}
            ]},
        {"id": "17", "name": "Fabric 16", img: basePath + "media/jacket_img/all_fabric_images/17.png", "target": "", "style": [
                {"id": "171", "name": "Blue", "code": "AA3", "price": "130", img: basePath + "media/jacket_img/all_fabric_images/17.png", "color": "Blue", "pattern": "Plain"}
            ]},
        {"id": "18", "name": "Fabric 17", img: basePath + "media/jacket_img/all_fabric_images/18.png", "target": "", "style": [
                {"id": "181", "name": "Blue", "code": "AA3", "price": "130", img: basePath + "media/jacket_img/all_fabric_images/18.png", "color": "Blue", "pattern": "Plain"}
            ]},
        {"id": "19", "name": "Fabric 18", img: basePath + "media/jacket_img/all_fabric_images/19.png", "target": "", "style": [
                {"id": "191", "name": "Blue", "code": "AA3", "price": "130", img: basePath + "media/jacket_img/all_fabric_images/19.png", "color": "Blue", "pattern": "Plain"}
            ]},
        {"id": "20", "name": "Fabric 19", img: basePath + "media/jacket_img/all_fabric_images/20.png", "target": "", "style": [
                {"id": "201", "name": "Blue", "code": "AA3", "price": "130", img: basePath + "media/jacket_img/all_fabric_images/20.png", "color": "Blue", "pattern": "Plain"}
            ]}
    ];
    //staic way
    /* return {
     getFabricData: function (e) {
     return fabrics;
     
     }
     };*/
    //dynamic way

    return {
        getFabricData: function (genderId, productId) {

            return $http.post(basePath + 'men-shirt/men/getSuitFabrics', {
                genderId: genderId,
                productId: productId
            });
        }

    };
});
angular.module("Suit").factory("vestService", function ($http) {

    var vestDesign = [
        {"id": "1", "name": "Style", "flag": "false", img: basePath + "media/jacket_img/thumbnail/00_Vest_Style_Main_Icon.png", "target": "0", "style": [
                {"id": "1", "name": "Double Breasted", "parent": "Fit", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Vest_Style_Double_Breasted.png", "prev": "", "live": [
                        {id: '1', name: 'Front', img: basePath + 'media/jacket_img/images/jacket_slim_fit_final.png'},
                        {id: '2', name: 'Back', img: basePath + 'media/jacket_img/images/Jacket_slim_back_final.png'},
                        {id: '3', name: 'Inside', img: basePath + 'media/jacket_img/images/Jacket_slim_back_final.png'}
                    ]},
                {"id": "2", "name": "Single Breasted", "parent": "Fit", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Vest_Style_Single_Breasted.png", "prev": "", "live": [
                        {id: '1', name: 'Front', img: basePath + 'media/jacket_img/images/jacket_std_fit_final.png'},
                        {id: '2', name: 'Back', img: basePath + 'media/jacket_img/images/Jacket_regular_back_final.png'},
                        {id: '3', name: 'Inside', img: basePath + 'media/jacket_img/images/Jacket_slim_back_final.png'}
                    ]}
            ]},
        {"id": "2", "name": "Vest", "flag": "false", img: basePath + "media/jacket_img/thumbnail/00_Style_Main_Icon.png", "target": "0", "style": [
                {"id": "1", "name": "No vest", "parent": "Style", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Style_1_button_Single_breasted.png", "prev": "", "live": [
                        {id: '1', name: 'Front', img: basePath + 'media/jacket_img/accessories/1_btn_single_breasted.png'},
                        {id: '2', name: 'Back', img: basePath + 'media/jacket_img/images/blank.png'},
                        {id: '3', name: 'Inside', img: basePath + 'media/jacket_img/images/Jacket_slim_back_final.png'}
                    ]},
                {"id": "2", "name": "3 Piece Suite", "parent": "Style", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Style_2_button_Single_breasted.png", "prev": "media/jacket_img/preview/2_button_single_breasted.png", "live": [
                        {id: '1', name: 'Front', img: basePath + 'media/jacket_img/accessories/2_btn_single_breasted.png'},
                        {id: '2', name: 'Back', img: basePath + 'media/jacket_img/images/blank.png'},
                        {id: '3', name: 'Inside', img: basePath + 'media/jacket_img/images/Jacket_slim_back_final.png'}
                    ]}
            ]},
        {"id": "3", "name": "Lapel", "flag": "false", img: basePath + "media/jacket_img/thumbnail/00_Vest_Main_icon.png", "target": "0", "style": [
                {"id": "1", "name": "Notch", "parent": "Lapel", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Lapel_Notch.png", "prev": "media/jacket_img/preview/Notch.png", "width": [
                        {
                            id: '1', name: 'Slim', img: basePath + 'media/jacket_img/Lapel_Width_Narrow.png', live: [
                                {id: '1', name: 'Front', img: basePath + 'media/jacket_img/accessories/6_btn_single_breasted.png'},
                                {id: '2', name: 'Back', img: basePath + 'media/jacket_img/images/blank.png'},
                                {id: '3', name: 'Inside', img: basePath + 'media/jacket_img/images/Jacket_slim_back_final.png'}
                            ]
                        },
                        {
                            id: '2', name: 'Standard', img: basePath + 'media/jacket_img/Lapel_Width_Wide.png', live: [
                                {id: '1', name: 'Front', img: basePath + 'media/jacket_img/accessories/6_btn_single_breasted.png'},
                                {id: '2', name: 'Back', img: basePath + 'media/jacket_img/images/blank.png'},
                                {id: '3', name: 'Inside', img: basePath + 'media/jacket_img/images/Jacket_slim_back_final.png'}
                            ]
                        }
                    ]},
                {"id": "2", "name": "Peak", "parent": "Lapel", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Vest_Peak.png", "prev": "", "width": [
                        {
                            id: '1',
                            name: 'Slim',
                            img: basePath + 'media/jacket_img/Lapel_Width_Narrow.png',
                            live: [
                                {id: '1', name: 'Front', img: basePath + 'media/jacket_img/accessories/6_btn_single_breasted.png'},
                                {id: '2', name: 'Back', img: basePath + 'media/jacket_img/images/blank.png'},
                                {id: '3', name: 'Inside', img: basePath + 'media/jacket_img/images/Jacket_slim_back_final.png'}
                            ]
                        },
                        {
                            id: '2',
                            name: 'Standard',
                            img: basePath + 'media/jacket_img/Lapel_Width_Wide.png',
                            live: [
                                {id: '1', name: 'Front', img: basePath + 'media/jacket_img/accessories/6_btn_single_breasted.png'},
                                {id: '2', name: 'Back', img: basePath + 'media/jacket_img/images/blank.png'},
                                {id: '3', name: 'Inside', img: basePath + 'media/jacket_img/images/Jacket_slim_back_final.png'}
                            ]
                        }
                    ]},
                {"id": "3", "name": "Shawl", "parent": "Lapel", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Vest_Shawl.png", "prev": "", "width": [
                        {
                            id: '1',
                            name: 'Slim',
                            img: basePath + 'media/jacket_img/Lapel_Width_Narrow.png',
                            live: [
                                {id: '1', name: 'Front', img: basePath + 'media/jacket_img/accessories/6_btn_single_breasted.png'},
                                {id: '2', name: 'Back', img: basePath + 'media/jacket_img/images/blank.png'},
                                {id: '3', name: 'Inside', img: basePath + 'media/jacket_img/images/Jacket_slim_back_final.png'}
                            ]
                        },
                        {
                            id: '2',
                            name: 'Standard',
                            img: basePath + 'media/jacket_img/Lapel_Width_Wide.png',
                            live: [
                                {id: '1', name: 'Front', img: basePath + 'media/jacket_img/accessories/6_btn_single_breasted.png'},
                                {id: '2', name: 'Back', img: basePath + 'media/jacket_img/images/blank.png'},
                                {id: '3', name: 'Inside', img: basePath + 'media/jacket_img/images/Jacket_slim_back_final.png'}
                            ]
                        }
                    ]}
            ]},
        {"id": "5", "name": "Pocket", img: basePath + "media/jacket_img/thumbnail/Breast_Pocket.png", "flag": "false", "target": "0", "style": [
                {"id": "1", "name": "Without Breast Pocket", "parent": "Pocket", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Without_Breast_Pocket.png", "prev": "", "live": [
                        {id: '1', name: 'Front', img: basePath + 'media/jacket_img/accessories/3_btn_std_sleeve.png'},
                        {id: '2', name: 'Back', img: basePath + 'media/jacket_img/accessories/blank.png'},
                        {id: '3', name: 'Inside', img: basePath + 'media/jacket_img/images/Jacket_slim_back_final.png'}
                    ]},
                {"id": "2", "name": "Breast_Pocket.png", "parent": "Pocket", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Breast_Pocket.png", "prev": "", "live": [
                        {id: '1', name: 'Front', img: basePath + 'media/jacket_img/accessories/3_btn_std_sleeve.png'},
                        {id: '2', name: 'Back', img: basePath + 'media/jacket_img/accessories/blank.png'},
                        {id: '3', name: 'Inside', img: basePath + 'media/jacket_img/images/Jacket_slim_back_final.png'}
                    ]}

            ]},
        {"id": "5", "name": "Bottom", img: basePath + "media/jacket_img/thumbnail/00_Bottom_Main_Icon.png", "flag": "false", "target": "0", "style": [
                {"id": "1", "name": "Diagonal", "parent": "Pocket", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Bottom_Diagonal.png", "prev": "", "live": [
                        {id: '1', name: 'Front', img: basePath + 'media/jacket_img/accessories/3_btn_std_sleeve.png'},
                        {id: '2', name: 'Back', img: basePath + 'media/jacket_img/accessories/blank.png'},
                        {id: '3', name: 'Inside', img: basePath + 'media/jacket_img/images/Jacket_slim_back_final.png'}
                    ]},
                {"id": "2", "name": "Rounded", "parent": "Pocket", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Bottom_Rounded.png", "prev": "", "live": [
                        {id: '1', name: 'Front', img: basePath + 'media/jacket_img/accessories/3_btn_std_sleeve.png'},
                        {id: '2', name: 'Back', img: basePath + 'media/jacket_img/accessories/blank.png'},
                        {id: '3', name: 'Inside', img: basePath + 'media/jacket_img/images/Jacket_slim_back_final.png'}
                    ]},
                {"id": "2", "name": "Straight", "parent": "Pocket", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Bottom_Straight.png", "prev": "", "live": [
                        {id: '1', name: 'Front', img: basePath + 'media/jacket_img/accessories/3_btn_std_sleeve.png'},
                        {id: '2', name: 'Back', img: basePath + 'media/jacket_img/accessories/blank.png'},
                        {id: '3', name: 'Inside', img: basePath + 'media/jacket_img/images/Jacket_slim_back_final.png'}
                    ]}

            ]}
    ];
    /*return{
     getVestData: function (e) {
     return vestDesign;
     }
     }*/
    return {
        getVestData: function (genderId, productId) {

            return $http.post(basePath + 'men-shirt/men/getSuitVest', {
                genderId: genderId,
                productId: productId
            });
        }
    };
});
angular.module("Suit").factory("topCoatService", function ($http) {

    var topCoatDesign = [
        {"id": 1, "name": "Style", "designType": "pant", "class": " icon-icon-87", "img": "media\/jacket_img\/thumbnail\/00Fit_Main_Icon.png", "style": [{"id": "1", "parent": "topcoatstyle", "price": "10.00", "class": "icon - icon - 87", "designType": "pant", "name": "CTOPC TPC3", "img": "media/"}, {"id": "2", "parent": "topcoatstyle", "price": "10.00", "class": "icon-icon-87", "designType": "pant"
                    , "name": "CTOPC TPC6", "img": "http:\/\/192.168.2.41\/p1135\/media\/"}, {"id": "3", "parent": "topcoatstyle", "price"
                            : "10.00", "class": "icon-icon-87", "designType": "pant", "name": "CTOPC_TPCAR1", "img": "media/"}, {"id": "4", "parent": "topcoatstyle", "price"
                            : "10.00", "class": "icon-icon-87", "designType": "pant", "name": "CTOPC_TPCAR2", "img": "media/"}, {"id": "5", "parent": "topcoatstyle", "price"
                            : "10.00", "class": "icon-icon-87", "designType": "pant", "name": "CTOPC_TPCAR3", "img": "media/"}, {"id": "6", "parent": "topcoatstyle", "price"
                            : "10.00", "class": "icon-icon-87", "designType": "pant", "name": "CTOPC_TPCAR4", "img": "media/"}]},
    ]


    /*return {
     gettopCoatData: function (e) {
     return topCoatDesign;
     }
     }*/
    return {
        gettopCoatData: function (genderId, productId) {

            return $http.post(basePath + 'men-shirt/men/getCoatDesign', {
                genderId: genderId,
                productId: productId
            });
        }
    };

});
angular.module("Suit").factory("pantService", function ($http) {

    var pants = [
        {"id": "1", "name": "Fit", "flag": "false", img: basePath + "media/jacket_img/thumbnail/00_Pant_Main_Icon.png", "target": "0", "style": [
                {"id": "1", "name": "Regular", "parent": "Fit", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Pant_Regular_Fit.png", "prev": "", "live": [
                        {id: '1', name: 'Front', img: basePath + ''},
                        {id: '2', name: 'Back', img: basePath + ''},
                        {id: '3', name: 'Inside', img: basePath + ''}
                    ]},
                {"id": "2", "name": "Slim", "parent": "Fit", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Pant_Slim_Fit.png", "prev": "", "live": [
                        {id: '1', name: 'Front', img: basePath + ''},
                        {id: '2', name: 'Back', img: basePath + ''},
                        {id: '3', name: 'Inside', img: basePath + ''}
                    ]}
            ]},
        {"id": "2", "name": "Pleats", "flag": "false", img: basePath + "media/jacket_img/thumbnail/00_Front_Main_Icon.png", "target": "0", "style": [
                {"id": "1", "name": "No Pleats", "parent": "Style", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Front_No_Pleats.png", "prev": "", "live": [
                        {id: '1', name: 'Front', img: basePath + 'media/jacket_img/accessories/1_btn_single_breasted.png'},
                        {id: '2', name: 'Back', img: basePath + 'media/jacket_img/images/blank.png'},
                        {id: '3', name: 'Inside', img: basePath + 'media/jacket_img/images/Jacket_slim_back_final.png'}
                    ]},
                {"id": "2", "name": "Single Pleat", "parent": "Style", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Front_Single_Pleat.png", "prev": "", "live": [
                        {id: '1', name: 'Front', img: basePath + 'media/jacket_img/accessories/2_btn_single_breasted.png'},
                        {id: '2', name: 'Back', img: basePath + 'media/jacket_img/images/blank.png'},
                        {id: '3', name: 'Inside', img: basePath + 'media/jacket_img/images/Jacket_slim_back_final.png'}
                    ]},
                {"id": "3", "name": "Double Pleats", "parent": "Style", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Front_Double_Pleats.png", "prev": "", "live": [
                        {id: '1', name: 'Front', img: basePath + 'media/jacket_img/accessories/3_btn_single_breasted.png'},
                        {id: '2', name: 'Back', img: basePath + 'media/jacket_img/images/blank.png'},
                        {id: '3', name: 'Inside', img: basePath + 'media/jacket_img/images/Jacket_slim_back_final.png'}
                    ]}
            ]},
        {"id": "3", "name": "Fastening", img: basePath + "media/jacket_img/thumbnail/00_Fastening_Main_Icon.png", "flag": "false", "target": "0", "style": [
                {"id": "1", "name": "Centered", "parent": "Sleeve Button", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Fastening_Centered.png", "prev": "media/jacket_img/preview/01_Three_Standard_Buttons.png", "live": [
                        {id: '1', name: 'Front', img: basePath + 'media/jacket_img/accessories/3_btn_std_sleeve.png'},
                        {id: '2', name: 'Back', img: basePath + 'media/jacket_img/accessories/blank.png'}
                    ]},
                {"id": "1", "name": "Off-centered", "parent": "Sleeve Button", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Fastening_Centered_Buttonless.png", "prev": "media/jacket_img/preview/01_Three_Standard_Buttons.png", "live": [
                        {id: '1', name: 'Front', img: basePath + 'media/jacket_img/accessories/3_btn_std_sleeve.png'},
                        {id: '2', name: 'Back', img: basePath + 'media/jacket_img/accessories/blank.png'}
                    ]},
                {"id": "2", "name": "No Button", "parent": "Sleeve Button", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Fastening_Off_Centered.png", "prev": "media/jacket_img/preview/02_Four_Standard_Buttons.png", "live": [
                        {id: '1', name: 'Front', img: basePath + 'media/jacket_img/accessories/4_btn_std_sleeve.png'},
                        {id: '2', name: 'Back', img: basePath + 'media/jacket_img/accessories/blank.png'}
                    ]},
                {"id": "3", "name": "Off-centered: Buttonless", "parent": "Sleeve Button", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Fastening_Off_Centered_Buttonless.png", "prev": "media/jacket_img/preview/03_Five_Standard_Buttons.png", "live": [
                        {id: '1', name: 'Front', img: basePath + 'media/jacket_img/accessories/5_btn_std_sleeve.png'},
                        {id: '2', name: 'Back', img: basePath + 'media/jacket_img/accessories/blank.png'}
                    ]}
            ]},
        {"id": "4", "name": "Pockets Back", img: basePath + "media/jacket_img/thumbnail/Back_Pocket_Flaps.png", "flag": "false", "target": "1", "style": [
                {"id": "1", "name": "Pocket Flaps", "parent": "Vents", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Back_Pocket_Flaps.png", "prev": "media/jacket_img/Vents_No_Vents.png", "live": [
                        {id: '1', name: 'Front', img: basePath + 'media/jacket_img/images/blank.png'},
                        {id: '2', name: 'Back', img: basePath + 'media/jacket_img/images/blank.png'},
                        {id: '2', name: 'Back', img: basePath + 'media/jacket_img/images/blank.png'}
                    ]},
                {"id": "2", "name": "Pocket Patched", "parent": "Vents", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Back_Pocket_Patched.png", "prev": "media/jacket_img/Vents_Center_Vents.png", "live": [
                        {id: '1', name: 'Front', img: basePath + 'media/jacket_img/images/blank.png'},
                        {id: '2', name: 'Back', img: basePath + 'media/jacket_img/images/blank.png'},
                        {id: '2', name: 'Back', img: basePath + 'media/jacket_img/images/blank.png'}
                    ]},
                {"id": "3", "name": "Pocket Welted With Buttons", "parent": "Vents", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Back_Pocket_Welted_With_Buttons.png", "prev": "media/jacket_img/Vents_Side.png", "live": [
                        {id: '1', name: 'Front', img: basePath + 'media/jacket_img/images/blank.png'},
                        {id: '2', name: 'Back', img: basePath + 'media/jacket_img/accessories/jacket_vent.png'},
                        {id: '2', name: 'Back', img: basePath + 'media/jacket_img/images/blank.png'}
                    ]}
            ]},
        {"id": "5", "name": "Pockets Front", img: basePath + "media/jacket_img/thumbnail/00_Pockets_Main_icon.png", "flag": "false", "target": "1", "style": [
                {"id": "1", "name": "Rounded", "parent": "Vents", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Front_Pockets_Rounded.png", "prev": "media/jacket_img/Vents_No_Vents.png", "live": [
                        {id: '1', name: 'Front', img: basePath + 'media/jacket_img/images/blank.png'},
                        {id: '2', name: 'Back', img: basePath + 'media/jacket_img/images/blank.png'},
                        {id: '2', name: 'Back', img: basePath + 'media/jacket_img/images/blank.png'}
                    ]},
                {"id": "2", "name": "Slanted", "parent": "Vents", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Front_Pockets_Slanted.png", "prev": "media/jacket_img/Vents_Center_Vents.png", "live": [
                        {id: '1', name: 'Front', img: basePath + 'media/jacket_img/images/blank.png'},
                        {id: '2', name: 'Back', img: basePath + 'media/jacket_img/images/blank.png'},
                        {id: '2', name: 'Back', img: basePath + 'media/jacket_img/images/blank.png'}
                    ]},
                {"id": "3", "name": "Straignt", "parent": "Vents", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Front_Pockets_Straignt.png", "prev": "media/jacket_img/Vents_Side.png", "live": [
                        {id: '1', name: 'Front', img: basePath + 'media/jacket_img/images/blank.png'},
                        {id: '2', name: 'Back', img: basePath + 'media/jacket_img/accessories/jacket_vent.png'},
                        {id: '2', name: 'Back', img: basePath + 'media/jacket_img/images/blank.png'}
                    ]}
            ]},
        {"id": "6", "name": "Cuffs", img: basePath + "media/jacket_img/thumbnail/00_Cuffs_Main_Icon.png", "flag": "false", "target": "1", "style": [
                {"id": "1", "name": "No Cuffs", "parent": "Vents", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/No_Cuffs.png", "prev": "media/jacket_img/Vents_No_Vents.png", "live": [
                        {id: '1', name: 'Front', img: basePath + 'media/jacket_img/images/blank.png'},
                        {id: '2', name: 'Back', img: basePath + 'media/jacket_img/images/blank.png'},
                        {id: '2', name: 'Back', img: basePath + 'media/jacket_img/images/blank.png'}
                    ]},
                {"id": "2", "name": "With Cuffs", "parent": "Vents", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/With_Cuffs.png", "prev": "media/jacket_img/Vents_Center_Vents.png", "live": [
                        {id: '1', name: 'Front', img: basePath + 'media/jacket_img/images/blank.png'},
                        {id: '2', name: 'Back', img: basePath + 'media/jacket_img/images/blank.png'},
                        {id: '2', name: 'Back', img: basePath + 'media/jacket_img/images/blank.png'}
                    ]}
            ]},
        {"id": "7", "name": "Pleats", img: basePath + "media/jacket_img/thumbnail/Front_Double_Pleats.png", "flag": "false", "target": "1", "style": [
                {"id": "1", "name": "Double Pleats", "parent": "Vents", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Front_Double_Pleats.png", "prev": "media/jacket_img/Vents_No_Vents.png", "live": [
                        {id: '1', name: 'Front', img: basePath + 'media/jacket_img/images/blank.png'},
                        {id: '2', name: 'Back', img: basePath + 'media/jacket_img/images/blank.png'},
                        {id: '2', name: 'Back', img: basePath + 'media/jacket_img/images/blank.png'}
                    ]},
                {"id": "2", "name": "No Pleats", "parent": "Vents", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Front_No_Pleats.png", "prev": "media/jacket_img/Vents_Center_Vents.png", "live": [
                        {id: '1', name: 'Front', img: basePath + 'media/jacket_img/images/blank.png'},
                        {id: '2', name: 'Back', img: basePath + 'media/jacket_img/images/blank.png'},
                        {id: '2', name: 'Back', img: basePath + 'media/jacket_img/images/blank.png'}
                    ]},
                {"id": "3", "name": "Single Pleats", "parent": "Vents", "part": "1", "price": "0", "desc": "description", img: basePath + "media/jacket_img/Front_Single_Pleat.png", "prev": "media/jacket_img/Vents_Side.png", "live": [
                        {id: '1', name: 'Front', img: basePath + 'media/jacket_img/images/blank.png'},
                        {id: '2', name: 'Back', img: basePath + 'media/jacket_img/accessories/jacket_vent.png'},
                        {id: '2', name: 'Back', img: basePath + 'media/jacket_img/images/blank.png'}
                    ]}
            ]},
    ];
//     return{
//     getPantsData: function (e) {
//     return pants;
//     }
//     }

    return {
        getPantsData: function (genderId, productId) {

            return $http.post(basePath + 'men-shirt/men/getSuitPant', {
                genderId: genderId,
                productId: productId
            });
        }
    };
});
angular.module("Suit").factory("fabricAllDataService", function ($http) {

    var fabricAllData = [
        {"id": "1", "name": "Fabric 1", img: basePath + "media/jacket_img/all_fabric_images/01.png", "target": "", "style": [
                {"id": "1", "name": "Violet", "code": "AA1", "price": "134", img: basePath + "media/jacket_img/all_fabric_images/01.png", "color": "Violet Light", "pattern": "Plain"}

            ]},
        {"id": "2", "name": "Fabric 2", img: basePath + "media/jacket_img/all_fabric_images/02.png", "target": "", "style": [
                {"id": "21", "name": "Blue1", "code": "AA3", "price": "130", img: basePath + "media/jacket_img/all_fabric_images/02.png", "color": "Blue", "pattern": "Plain"}
            ]},
        {"id": "3", "name": "Fabric 3", img: basePath + "media/jacket_img/all_fabric_images/03.png", "target": "", "style": [
                {"id": "31", "name": "Blue2", "code": "AA4", "price": "130", img: basePath + "media/jacket_img/all_fabric_images/03.png", "color": "Blue", "pattern": "Plain"}
            ]},
        {"id": "4", "name": "Fabric 4", img: basePath + "media/jacket_img/all_fabric_images/04.png", "target": "", "style": [
                {"id": "41", "name": "Blue3", "code": "AA6", "price": "130", img: basePath + "media/jacket_img/all_fabric_images/04.png", "color": "Blue", "pattern": "Plain"}
            ]},
        {"id": "5", "name": "Fabric 5", img: basePath + "media/jacket_img/all_fabric_images/05.png", "target": "", "style": [
                {"id": "51", "name": "Blue4", "code": "AA3", "price": "130", img: basePath + "media/jacket_img/all_fabric_images/05.png", "color": "Blue", "pattern": "Plain"}
            ]},
        {"id": "6", "name": "Fabric 6", img: basePath + "media/jacket_img/all_fabric_images/06.png", "target": "", "style": [
                {"id": "61", "name": "Blue5", "code": "AA3", "price": "130", img: basePath + "media/jacket_img/all_fabric_images/06.png", "color": "Blue", "pattern": "Plain"}
            ]},
        {"id": "7", "name": "Fabric 6", img: basePath + "media/jacket_img/all_fabric_images/07.png", "target": "", "style": [
                {"id": "71", "name": "Blue6", "code": "AA3", "price": "130", img: basePath + "media/jacket_img/all_fabric_images/07.png", "color": "Blue", "pattern": "Plain"}
            ]},
        {"id": "8", "name": "Fabric 7", img: basePath + "media/jacket_img/all_fabric_images/08.png", "target": "", "style": [
                {"id": "81", "name": "Blue7", "code": "AA3", "price": "130", img: basePath + "media/jacket_img/all_fabric_images/08.png", "color": "Blue", "pattern": "Plain"}
            ]},
        {"id": "9", "name": "Fabric 8", img: basePath + "media/jacket_img/all_fabric_images/09.png", "target": "", "style": [
                {"id": "91", "name": "Blue8", "code": "AA3", "price": "130", img: basePath + "media/jacket_img/all_fabric_images/09.png", "color": "Blue", "pattern": "Plain"}
            ]},
        {"id": "10", "name": "Fabric 9", img: basePath + "media/jacket_img/all_fabric_images/10.png", "target": "", "style": [
                {"id": "101", "name": "Blue9", "code": "AA3", "price": "130", img: basePath + "media/jacket_img/all_fabric_images/10.png", "color": "Blue", "pattern": "Plain"}
            ]},
        {"id": "11", "name": "Fabric 10", img: basePath + "media/jacket_img/all_fabric_images/11.png", "target": "", "style": [
                {"id": "111", "name": "Blue10", "code": "AA3", "price": "130", img: basePath + "media/jacket_img/all_fabric_images/11.png", "color": "Blue", "pattern": "Plain"}
            ]},
        {"id": "12", "name": "Fabric 11", img: basePath + "media/jacket_img/all_fabric_images/12.png", "target": "", "style": [
                {"id": "121", "name": "Blue611", "code": "AA3", "price": "130", img: basePath + "media/jacket_img/all_fabric_images/12.png", "color": "Blue", "pattern": "Plain"}
            ]},
        {"id": "13", "name": "Fabric 12", img: basePath + "media/jacket_img/all_fabric_images/13.png", "target": "", "style": [
                {"id": "131", "name": "Blue", "code": "AA3", "price": "130", img: basePath + "media/jacket_img/all_fabric_images/13.png", "color": "Blue", "pattern": "Plain"}
            ]},
        {"id": "14", "name": "Fabric 13", img: basePath + "media/jacket_img/all_fabric_images/14.png", "target": "", "style": [
                {"id": "141", "name": "Blue", "code": "AA3", "price": "130", img: basePath + "media/jacket_img/all_fabric_images/14.png", "color": "Blue", "pattern": "Plain"}
            ]},
        {"id": "15", "name": "Fabric 14", img: basePath + "media/jacket_img/all_fabric_images/15.png", "target": "", "style": [
                {"id": "151", "name": "Blue", "code": "AA3", "price": "130", img: basePath + "media/jacket_img/all_fabric_images/15.png", "color": "Blue", "pattern": "Plain"}
            ]},
        {"id": "16", "name": "Fabric 15", img: basePath + "media/jacket_img/all_fabric_images/16.png", "target": "", "style": [
                {"id": "161", "name": "Blue", "code": "AA3", "price": "130", img: basePath + "media/jacket_img/all_fabric_images/16.png", "color": "Blue", "pattern": "Plain"}
            ]},
        {"id": "17", "name": "Fabric 16", img: basePath + "media/jacket_img/all_fabric_images/17.png", "target": "", "style": [
                {"id": "171", "name": "Blue", "code": "AA3", "price": "130", img: basePath + "media/jacket_img/all_fabric_images/17.png", "color": "Blue", "pattern": "Plain"}
            ]},
        {"id": "18", "name": "Fabric 17", img: basePath + "media/jacket_img/all_fabric_images/18.png", "target": "", "style": [
                {"id": "181", "name": "Blue", "code": "AA3", "price": "130", img: basePath + "media/jacket_img/all_fabric_images/18.png", "color": "Blue", "pattern": "Plain"}
            ]},
        {"id": "19", "name": "Fabric 18", img: basePath + "media/jacket_img/all_fabric_images/19.png", "target": "", "style": [
                {"id": "191", "name": "Blue", "code": "AA3", "price": "130", img: basePath + "media/jacket_img/all_fabric_images/19.png", "color": "Blue", "pattern": "Plain"}
            ]},
        {"id": "20", "name": "Fabric 19", img: basePath + "media/jacket_img/all_fabric_images/20.png", "target": "", "style": [
                {"id": "201", "name": "Blue", "code": "AA3", "price": "130", img: basePath + "media/jacket_img/all_fabric_images/20.png", "color": "Blue", "pattern": "Plain"}
            ]}
    ];
    //staic way
    /* return {
     getFabricData: function (e) {
     return fabrics;
     
     }
     };*/
    //dynamic way
    return {
        getFabricAllDesignData: function (genderId, productId) {

            return $http.post(basePath + 'custom_ajax/get_fabric_all_data.php', {
                genderId: genderId,
                productId: productId
            });
        }
    };
});
angular.module("Suit").factory("suitAccentService", function ($http) {

//dynamic way
    return {
        getSuitAccentData: function (genderId, productId) {

            return $http.post(basePath + 'men-shirt/men/getSuitAccent', {
                genderId: genderId,
                productId: productId
            });
        }
    };
});
angular.module("Suit").factory("measurementDataService", function ($http) {

    var measurementViewDataSuit = [
        {id: '1', name: 'viewM1', viewimages: [
                {id: '1', rel: 'Body', label: "", type: 'jacket', name: 'Man', classname: 'shirt_part part', img: basePath + 'media/Body_Measurement_images/2/Char_2_1_View_01.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '2', rel: 'Suit', label: "", type: 'jacket', name: 'Man', classname: 'shirt_part part', img: basePath + 'media/Fabric_Measurement_images/Fabric_01_1_View_01.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
            ]},
        {id: '2', name: 'viewM2', viewimages: [
                {id: '1', rel: 'Body', label: "", type: 'jacket', name: 'Man', classname: 'shirt_part part', img: basePath + 'media/Body_Measurement_images/2/Char_2_1_View_02.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
                {id: '2', rel: 'Suit', label: "", type: 'jacket', name: 'Man', classname: 'shirt_part part', img: basePath + 'media/Fabric_Measurement_images/Fabric_01_1_View_02.png', xPos: '0', yPos: '0', activeStyle: '0', activeStyleIndex: '0', price: 0},
            ]}
    ];
    var fabrics = [
        {"id": "1", "name": "Black", img: basePath + "media/tool_data/img/01.png", "price": "0", "target": "", "style": [
                {"id": "1", "name": "Violet", "code": "AA1", "price": "0", img: basePath + "media/tool_data/img/01.png", "color": "Violet Light", "pattern": "Plain"}

            ]},
        {"id": "2", "name": "Gray", img: basePath + "media/tool_data/img/02.png", "price": "0", "target": "", "style": [
                {"id": "21", "name": "Blue1", "code": "AA3", "price": "0", img: basePath + "media/tool_data/img/02.png", "color": "Blue", "pattern": "Plain"}
            ]},
        {"id": "3", "name": "Bourne", img: basePath + "media/tool_data/img/03.png", "price": "0", "target": "", "style": [
                {"id": "31", "name": "Blue2", "code": "AA4", "price": "0", img: basePath + "media/tool_data/img/03.png", "color": "Blue", "pattern": "Plain"}
            ]},
        {"id": "4", "name": "Dark Green", img: basePath + "media/tool_data/img/04.png", "price": "0", "target": "", "style": [
                {"id": "41", "name": "Blue3", "code": "AA6", "price": "0", img: basePath + "media/tool_data/img/04.png", "color": "Blue", "pattern": "Plain"}
            ]},
        {"id": "5", "name": "Aliai Dark Red", img: basePath + "media/tool_data/img/05.png", "price": "0", "target": "", "style": [
                {"id": "51", "name": "Blue4", "code": "AA3", "price": "0", img: basePath + "media/tool_data/img/05.png", "color": "Blue", "pattern": "Plain"}
            ]}
    ];
    var measurementBackgrounds = [
        {"id": "0", "name": "No Background", img: basePath + "media/background_images/0.png", "price": "0", "target": "", "style": [
                {"id": "0", "name": "No Background", "code": "AA1", "price": "0", img: basePath + "media/background_images/0.png", "color": "Violet Light", "pattern": "Plain"}

            ]}, {"id": "1", "name": "Background-1", img: basePath + "media/background_images/1.png", "price": "0", "target": "", "style": [
                {"id": "1", "name": "Violet", "code": "AA1", "price": "134", img: basePath + "media/background_images/1.png", "color": "Violet Light", "pattern": "Plain"}

            ]},
        {"id": "2", "name": "Background-1", img: basePath + "media/background_images/2.png", "price": "0", "target": "", "style": [
                {"id": "2", "name": "Blue1", "code": "AA3", "price": "130", img: basePath + "media/background_images/2.png", "color": "Blue", "pattern": "Plain"}
            ]},
        {"id": "3", "name": "Background-3", img: basePath + "media/background_images/3.png", "price": "0", "target": "", "style": [
                {"id": "3", "name": "Blue2", "code": "AA4", "price": "130", img: basePath + "media/background_images/3.png", "color": "Blue", "pattern": "Plain"}
            ]},
        {"id": "4", "name": "Background-4", img: basePath + "media/background_images/4.png", "price": "0", "target": "", "style": [
                {"id": "4", "name": "Blue3", "code": "AA6", "price": "130", img: basePath + "media/background_images/4.png", "color": "Blue", "pattern": "Plain"}
            ]},
        {"id": "5", "name": "Background-5", img: basePath + "media/background_images/5.png", "price": "0", "target": "", "style": [
                {"id": "5", "name": "Blue4", "code": "AA3", "price": "130", img: basePath + "media/background_images/5.png", "color": "Blue", "pattern": "Plain"}
            ]},
        {"id": "6", "name": "Background-6", img: basePath + "media/background_images/6.png", "price": "0", "target": "", "style": [
                {"id": "7", "name": "Blue4", "code": "AA3", "price": "130", img: basePath + "media/background_images/6.png", "color": "Blue", "pattern": "Plain"}
            ]},
        {"id": "7", "name": "Background-7", img: basePath + "media/background_images/7.png", "price": "0", "target": "", "style": [
                {"id": "7", "name": "Blue4", "code": "AA3", "price": "130", img: basePath + "media/background_images/7.png", "color": "Blue", "pattern": "Plain"}
            ]},
        {"id": "8", "name": "Background-8", img: basePath + "media/background_images/8.png", "price": "0", "target": "", "style": [
                {"id": "8", "name": "Blue4", "code": "AA3", "price": "130", img: basePath + "media/background_images/8.png", "color": "Blue", "pattern": "Plain"}
            ]},
        {"id": "9", "name": "Background-9", img: basePath + "media/background_images/9.png", "price": "0", "target": "", "style": [
                {"id": "9", "name": "Blue4", "code": "AA3", "price": "130", img: basePath + "media/background_images/9.png", "color": "Blue", "pattern": "Plain"}
            ]},
        {"id": "10", "name": "Background-10", img: basePath + "media/background_images/10.png", "price": "0", "target": "", "style": [
                {"id": "10", "name": "Blue4", "code": "AA3", "price": "130", img: basePath + "media/background_images/10.png", "color": "Blue", "pattern": "Plain"}
            ]}
    ];
    var custSizeJacktData = [
        {id: '1', name: 'Slim', chest: '20.00-40.00', waist: '20.00-40.00', hip: "20.00-40.00", shoulder: "20.00-40.00", sleeve: "15.00-30.00", length: "15.00-30.00"},
        {id: '2', name: 'Fit', chest: '40.00-60.00', waist: '40.00-60.00', hip: "40.00-60.00", shoulder: "40.00-60.00", sleeve: "30.00-45.00", length: "30.00-45.00"},
        {id: '3', name: 'XL', chest: '60.00-80.00', waist: '60.00-80.00', hip: "60.00-80.00", shoulder: "60.00-80.00", sleeve: "45.00-60.00", length: "45.00-60.00"},
    ];
    var custSizeJacktDataCm = [
        {id: '1', name: 'Slim', chest: '50.8-101.6', waist: '50.8-101.6', hip: "50.8-101.6", shoulder: "50.8-101.6", sleeve: "38.1-76.2", length: "38.1-76.2"},
        {id: '2', name: 'Fit', chest: '101.6-152.4', waist: '101.6-152.4', hip: "101.6-152.4", shoulder: "101.6-152.4", sleeve: "76.2-114.3", length: "76.2-114.3"},
        {id: '3', name: 'XL', chest: '152.4-203.2', waist: '152.4-203.2', hip: "152.4-203.2", shoulder: "152.4-203.2", sleeve: "114.3-152.4", length: "114.3-152.4"},
    ];
    var custSizePantData = [
        {id: '1', name: 'Slim', waist: '20.00-35.00', hip: "20.00-35.00", crotch: "20.00-25.00", thigh: "15.00-20.00", length: "15.00-25.00"},
        {id: '2', name: 'Fit', waist: '35.00-45.00', hip: "35.00-45.00", crotch: "25.00-30.00", thigh: "20.00-30.00", length: "20.00-30.00"},
        {id: '3', name: 'XL', waist: '45.00-60.00', hip: "45.00-65.00", crotch: "30.00-40.00", thigh: "30.00-40.00", length: "30.00-45.00"},
    ];
    var custSizePantDataCm = [
        {id: '1', name: 'Slim', waist: '50.8-88.9', hip: "50.8-88.9", crotch: "50.8-63.5", thigh: "38.1-50.8", length: "38.1-63.5"},
        {id: '2', name: 'Fit', waist: '88.9-114.3', hip: "88.9-114.3", crotch: "63.5-76.2", thigh: "63.5-76.2", length: "63.5-76.2"},
        {id: '3', name: 'XL', waist: '114.3-152.4', hip: "114.3-165.1", crotch: "76.2-101.6", thigh: "76.2-101.6", length: "76.2-114.3"},
    ];
    var staticBodies = [
        {"id": "2", "name": "Ramesh", img: basePath + "media/default_tool_measurement_img/character_01.png", "target": "", "style": [
                {"id": "2", "name": "Violet", "code": "AA1", "price": "134", img: basePath + "media/default_tool_measurement_img/character_01.png", "color": "Violet Light", "pattern": "Plain"}

            ]},
        {"id": "1", "name": "Dinesh", img: basePath + "media/default_tool_measurement_img/character_02.png", "target": "", "style": [
                {"id": "1", "name": "Blue1", "code": "AA3", "price": "130", img: basePath + "media/default_tool_measurement_img/character_02.png", "color": "Blue", "pattern": "Plain"}
            ]},
        {"id": "3", "name": "Suresh", img: basePath + "media/default_tool_measurement_img/character_03.png", "target": "", "style": [
                {"id": "3", "name": "Blue2", "code": "AA4", "price": "130", img: basePath + "media/default_tool_measurement_img/character_03.png", "color": "Blue", "pattern": "Plain"}
            ]}
    ];
    //measurment sizes

    var jacketSizes = [
        {"id": "19", "product_type": "1", "gender": 1, "size_name": "Slim", "chest": "15.0", "waist": "36"
            , "hip": "38", "shoulder": "38", "sleeve": "38", "length": "38", "size_order": "1"},
        {"id": "20", "product_type": "1", "gender": 1, "size_name": "Classic", "chest": "15.0", "waist": "36"
            , "hip": "38", "shoulder": "38", "sleeve": "38", "length": "38", "size_order": "2"},
        {"id": "21", "product_type": "1", "gender": 1, "size_name": "XL", "chest": "15.0", "waist": "36"
            , "hip": "38", "shoulder": "38", "sleeve": "38", "length": "38", "size_order": "3"}
    ];
    var pantSizes = [
        {"id": "19", "product_type": "1", "gender": 1, "size_name": "Slim", "waist": "36"
            , "hip": "38", "crotch": "38", "thigh": "38", "length": "38", "size_order": "1"},
        {"id": "20", "product_type": "1", "gender": 1, "size_name": "Classic", "waist": "36"
            , "hip": "38", "crotch": "38", "thigh": "38", "length": "38", "size_order": "2"},
        {"id": "21", "product_type": "1", "gender": 1, "size_name": "XL", "waist": "36"
            , "hip": "38", "crotch": "38", "thigh": "38", "length": "38", "size_order": "3"}
    ];
    return {
        getMeasurementBackrounds: function (e) {
            return measurementBackgrounds;
        },
        getMeasurementFabrics: function (e) {
            return fabrics;
        },
        getCustSizeJacktData: function (e) {
            return custSizeJacktData;
        },
        getCustSizePantData: function (e) {
            return custSizePantData;
        },
        getCustSizeJacktDataCm: function (e) {
            return custSizeJacktDataCm;
        },
        getCustSizePantDataCm: function (e) {
            return custSizePantDataCm;
        },
        getMeasurementSuitView: function () {
            return measurementViewDataSuit;
        },
        getMeasurementBodies: function () {
            return staticBodies;
        },
        getJacketSizes: function () {
            return jacketSizes;
        },
        getPantSizes: function () {
            return pantSizes;
        }
    }
});
