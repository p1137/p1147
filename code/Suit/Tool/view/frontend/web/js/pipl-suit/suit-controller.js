var style, currentId;
var removeSpace, clrfilter;
var jacketImage;
var lapelImage;
var basePath;//jQuery("#basePath").val()
var custom_ajax;
var productId;
var genderId;
var fabricId = '1';
var style_ID;
var tempPj;
var currStyleName = "Style";
var linid_active_id = "1";
var active_view_id = "1";

//for fabric change 
var tempJk;
var tempPt;
var tempVst;
var tempAccnt;
var temptopCoat;
var currentThreadsId = "1";
var topcoatStatus = true;
//end
var temp = {};

//
var jacketFabricName = 'Upper Side';
var pantFabricName = 'Upper Side';
var vestFabricName = 'Upper Side';
var jacketFabricId = '1';
var pantFabricId = '1';
var vestFabricId = '1';
//


angular.module("Suit").controller("suitController", function ($scope, $filter, designService, viewService, fabricService, vestService, pantService, topCoatService, fabricAllDataService, suitAccentService, $http, measurementDataService) {//measurementDataService

    $scope.isActive = false;
    $scope.IsBow = false;
    $scope.IsVisible = $scope.IsTie = true;
    $scope.IsJacket = true;
    $scope.IsVest = false;
    $scope.myClassForZoom = [];
    $scope.activeView = 1;
    $scope.IsImage = false;
    $scope.IsZoom = true;
    $scope.jacketView = false;
    $scope.myClassForZoom = [];
    $scope.fabricId = "1";
    $scope.backcollfabId = "1";
    $scope.backelbofabId = "1";
    $scope.partOption = "all";
    $scope.selStyleArr = [];

    $scope.sleeveBtnCnt = "2";
    $scope.styleBtnCnt = "1";
    $scope.styleBtnType = "1";
    $scope.vestStyleBtnType = "1";
    $scope.actCatName = "";

    $scope.actStyleJKArr = {};
    $scope.actStylePantArr = {};
    $scope.actStyleVestArr = {};
    $scope.actStyleData = {};
    $scope.actStyleTopCoat = {};

    productId = angular.element("#productId").val();
    genderId = angular.element("#genderId").val();
    basePath = angular.element("#basePath").val();


    $scope.activeProduct = productId; //suit
    $scope.activeModel = genderId; //male


    //mesuarement data
    $scope.measureFlg = true;
    $scope.scanSizeFlg = false;
    $scope.stdSizeFlg = false;
    $scope.unitVal = 1;
    $scope.unitName = "in";
    $scope.unitScanName = "in";
    $scope.jacketSizes = measurementDataService.getJacketSizes();
    $scope.pantSizes = measurementDataService.getPantSizes();
    //load views
    $scope.views = viewService.getSuitView();
    //add more fit in standard size
    $scope.addMoreFit = function () {
        jQuery("#fit-holder").append('<div class="row offset-top-10"><label class="col-xs-3 control-label" for="inputPassword3"> <i class="fa fa-arrow-circle-right text-success"> </i>  Select Your Fit </label><div class="col-xs-3"><select class="form-control fit-select-box"><option value="-">Select</option><option value="S">S</option><option value="M">M</option><option value="L">L</option><option value="XL">XL</option><option value="XXL">XXL</option><option value="3XL">3XL</option><option value="4XL">4XL</option></select></div><label class="col-xs-2 control-label" for="inputPassword3">  Quantity </label><div class="col-xs-2"><select class="form-control"><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option></select></div><div class="col-xs-2" onclick="removeFit(this);" ><a class="btn btn-primary btn-xs" href="javascript:void(0);" > <span class="glyphicon glyphicon-minus"></span></a></div></div>');
    }

    $scope.showScanSize = function () {
        console.log("in scan..")
        $scope.scanSizeFlg = true;
        $scope.stdSizeFlg = false;
        $scope.measureFlg = false;
    }

    $scope.showStdSize = function () {

        console.log("in std..")
        $scope.scanSizeFlg = false;
        $scope.stdSizeFlg = true;
        $scope.measureFlg = false;
    }
//change Unit
    $scope.changeUnit = function (val) {
        if (val == "in") {
            $scope.unitVal = 1;
        } else {
            $scope.unitVal = 2.54;
        }
    }
// change unit scan
    $scope.changeScanUnit = function (unitVal) {
        $scope.unitScanName = unitVal;
        var convertVal = 0;
        if (unitVal == "cm") {
            convertVal = 2.54;
        } else {
            convertVal = 0.393701;
        }

        jQuery(".scan_number").each(function (e) {
            if (jQuery(this).val() != "") {
                var res = parseFloat(parseFloat(jQuery(this).val()) * convertVal).toFixed(2);
                jQuery(this).val(res);
            }
        });
    }
    //mesuarement data end


    $scope.addClassOfZoom = function () {

        $scope.myClassForZoom.push('zoomOut');
        $scope.IsVisible = false;
    }

    $scope.hideFabricDiv = function () {
        $scope.IsImage = false;
    }

    $scope.showFabricDiv = function () {
        $scope.IsImage = true;
    }

    $scope.jacketVisiblefalse = function (obj) {


        $scope.IsJacket = true;
        hideJacket();
    }


    $scope.jacketVisibletrue = function () {
//angular.element("#jacketHide").css({"margine-left": "11px"});
        $scope.IsJacket = false;
        hideJacket();
    }


    $scope.vestVisiblefalse = function (obj) {
        $scope.IsVest = true;
        hideVest();
    }

    $scope.vestVisibletrue = function () {

//angular.element("#jacketHide").css({"margine-left": "11px"});
        $scope.IsVest = false;
        hideVest();
    }

    $scope.TopCoatVisiblefalse = function (obj) {
        $scope.IsTopCoat = true;
        // jQuery('#linkAccent').parent().show()
        hideTopCoat();
    }

    $scope.TopCoatVisibletrue = function () {

        // jQuery('#linkAccent').parent().hide()
        $scope.IsTopCoat = false;
        hideTopCoat();
    }
    function hideTopCoat()
    {
        if ($scope.IsTopCoat)
        {
            angular.element("#mainImageHolder").find(".shirt_part[rel=topcoatstyle]").hide();
            angular.element("#mainImageHolder").find(".shirt_part[rel=TopCoatCollar]").hide();
            angular.element("#mainImageHolder").find(".shirt_part[rel=TopCoatPocket]").hide();
            angular.element("#mainImageHolder").find(".shirt_part[rel=TopCoatSleeve]").hide();
            angular.element("#mainImageHolder").find(".shirt_part[rel=TopCoatStyleBack]").hide();
            angular.element("#mainImageHolder").find(".shirt_part[rel=TopCoatSleeveBack]").hide();
            angular.element("#mainImageHolder").find(".shirt_part[rel=TopCoatSleeveButton]").hide();
            angular.element("#mainImageHolder").find(".shirt_part[rel=TopCoatCollarFold]").hide();
            angular.element("#mainImageHolder").find(".shirt_part[rel=TopCoatLining]").hide();
            angular.element("#mainImageHolder").find(".shirt_part[rel=pantfit_straight]").show();
            angular.element("#mainImageHolder").find(".shirt_part[rel=pantfit_style]").show();
            angular.element("#mainImageHolder").find(".shirt_part[rel=foldedPantPocket]").show();
            // angular.element('#mainImageHolder').find('[rel=vestcollar]').show();//beltfoldloop
            angular.element('#mainImageHolder').find('[rel=TopCoatCollarBack]').hide();
            angular.element('#mainImageHolder').find('[rel=topcoatbuttons]').hide();
            angular.element('#mainImageHolder').find('[rel=TopCoatBreastPocket]').hide();
            angular.element('#mainImageHolder').find('[rel=TopCoatCollarFold]').hide();
            angular.element('#mainImageHolder').find('[rel=beltfoldloop]').show();
            angular.element("#mainImageHolder").find(".shirt_part[rel=pantFoldCuff]").show();

        } else {
            angular.element("#mainImageHolder").find(".shirt_part[rel=topcoatstyle]").show();
            angular.element("#mainImageHolder").find(".shirt_part[rel=TopCoatCollar]").show();
            angular.element("#mainImageHolder").find(".shirt_part[rel=TopCoatPocket]").show();
            angular.element("#mainImageHolder").find(".shirt_part[rel=TopCoatSleeve]").show();
            angular.element("#mainImageHolder").find(".shirt_part[rel=TopCoatStyleBack]").show();
            angular.element("#mainImageHolder").find(".shirt_part[rel=TopCoatSleeveBack]").show();
            angular.element("#mainImageHolder").find(".shirt_part[rel=TopCoatSleeveButton]").show();
            angular.element('#mainImageHolder').find('[rel=TopCoatCollarBack]').show();
            angular.element("#mainImageHolder").find(".shirt_part[rel=TopCoatCollarFold]").show();
            angular.element("#mainImageHolder").find(".shirt_part[rel=TopCoatLining]").show();
            angular.element('#mainImageHolder').find('[rel=topcoatbuttons]').show();
            if ($scope.activeView == 3) {


                angular.element("#mainImageHolder").find(".shirt_part[rel=pantfit_straight]").hide();
                angular.element("#mainImageHolder").find(".shirt_part[rel=pantfit_style]").hide();
                angular.element("#mainImageHolder").find(".shirt_part[rel=foldedPantPocket]").hide();

            }
            $scope.IsJacket = false;//hide jacket
            hideJacket();
            angular.element('#mainImageHolder').find('[rel=TopCoatBreastPocket]').show();
            angular.element('#mainImageHolder').find('[rel=TopCoatCollarFold]').show();
            angular.element('#mainImageHolder').find('[rel=beltfoldloop]').hide();

        }
    }
    $scope.tieVisiblefalse = function (obj) {

        $scope.IsTie = false;
        hideTie();
    }
    $scope.tieVisibletrue = function () {


        $scope.IsTie = true;
        hideTie();
    }
    function hideTie()
    {

        if ($scope.IsTie) {
            //alert("Tie show")
            angular.element("#mainImageHolder").find('.shirt_part[rel="Tie"]').show();
            $scope.IsBow = false;
            hideBow();
            // angular.element("#mainImageHolder").find('.shirt_part[rel="ManTieSide"]').show();
            //  angular.element("#casual_1").hide();
            // angular.element("#casual_2").hide();
            //jQuery("#mainImageHolder").find("[rel=ManPant]").show();
        } else {
            angular.element("#mainImageHolder").find('.shirt_part[rel="Tie"]').hide();
            //alert("Tie hide")
// angular.element("#mainImageHolder").find('.shirt_part[rel="ManTieSide"]').hide();
            // angular.element("#casual_1").show();
            // angular.element("#casual_2").show();
        }
    }
    $scope.bowVisiblefalse = function (obj) {

        $scope.IsBow = false;
        hideBow();
    }
    $scope.bowVisibletrue = function () {


        $scope.IsBow = true;
        hideBow();
    }
    function hideBow()
    {

        if ($scope.IsBow) {
            //  alert("Bow show")
            angular.element("#mainImageHolder").find('.shirt_part[rel="ManBow"]').show();
            $scope.IsTie = false;
            hideTie();

        } else {
            angular.element("#mainImageHolder").find('.shirt_part[rel="ManBow"]').hide();

        }
    }
    $scope.pantVisiblefalse = function (obj) {

        $scope.IsPant = true;
        hidePant();
    }
    $scope.pantVisibletrue = function () {
        //formal

        $scope.IsPant = false;
        hidePant();
    }
    function hidePant()
    {

        if ($scope.IsPant)
        {   // jeans

            angular.element("#mainImageHolder").find('.shirt_part[rel="pantfit"]').attr("src", basePath + "media/default_tool_img/Jeans_Front.png");
            angular.element("#mainImageHolder").find('.shirt_part[rel="backpantfit"]').attr("src", basePath + "media/default_tool_img/Jeans_Back.png");
            jQuery(".leftMainNav li").eq(1).hide();

            jQuery("#div2").hide();
            // $scope.IsJeans = true;
            jQuery('#view2').find(' .shirt_part[rel="backpantcuff"]').hide();
            jQuery('#view2').find(' .shirt_part[rel="pantBackStyle"]').hide();
            jQuery('#view2').find(' .shirt_part[rel="backbeltloop"]').hide();
            jQuery('#view1').find(' .shirt_part[rel="beltloop"]').hide();
            jQuery('#view1').find(' .shirt_part[rel="fasteningButton"]').hide();
            jQuery('#view1').find(' .shirt_part[rel="fastening"]').hide();
            jQuery('#view1').find(' .shirt_part[rel="pantpleats"]').hide();
            jQuery('#view1').find(' .shirt_part[rel="pantpockets"]').hide();
            jQuery('#view1').find(' .shirt_part[rel="pantStyle"]').hide();
            jQuery('#view1').find(' .shirt_part[rel="pantcuff"]').hide();


        } else {
            console.log("Formal pant")
            //formal pant
            angular.element("#mainImageHolder").find('.shirt_part[rel="pantfit"]').attr("src", basePath + "media/pant_images/pantfit/" + fabricId + "_pantfit_2_view_1.png");
            // $scope.actStylePantArr['pantfit'] = basePath + "media/pant_images/" + parent.toLowerCase() + "/" + fabricId + "_pantfit_2_view_1.png";

            angular.element("#mainImageHolder").find('.shirt_part[rel="backpantfit"]').attr("src", basePath + "media/pant_images/pantfit/" + fabricId + "_pantfit_2_view_2.png");
            //$scope.actStylePantArr['pantfit_straight'] = basePath + "media/pant_images/" + parent.toLowerCase() + "/" + fabricId + "_pantfit_2_view_3.png";
            angular.element("#view3").find('.shirt_part[rel=pantfit_straight]').attr("src", basePath + "media/pant_images/pantfit/" + fabricId + "_pantfit_2_view_3.png")




            jQuery(".leftMainNav li").eq(1).show();
            if (jQuery(".leftMainNav li").eq(1).hasClass('active')) {
                jQuery("#div2").show();
            } else {
                jQuery("#div2").hide();
            }
            //angular.element("#mainImageHolder").find('.shirt_part[rel="pantfit"]').attr("src", basePath + "media/shirt_images/bottom/Shirt_Bottom_1_1.png");

            jQuery('#view2').find(' .shirt_part[rel="backpantcuff"]').show();
            jQuery('#view2').find(' .shirt_part[rel="pantBackStyle"]').show();
            jQuery('#view2').find(' .shirt_part[rel="backbeltloop"]').show();
            jQuery('#view1').find(' .shirt_part[rel="beltloop"]').show();
            jQuery('#view1').find(' .shirt_part[rel="fasteningButton"]').show();
            jQuery('#view1').find(' .shirt_part[rel="fastening"]').show();
            jQuery('#view1').find(' .shirt_part[rel="pantpleats"]').show();
            jQuery('#view1').find(' .shirt_part[rel="pantpockets"]').show();
            jQuery('#view1').find(' .shirt_part[rel="pantStyle"]').show();
            jQuery('#view1').find(' .shirt_part[rel="pantcuff"]').show();


            //$scope.IsJeans = false;

        }
    }
    function hideVest()
    {
        if ($scope.IsVest)
        {
            angular.element("#view1,#view2").find(".shirt_part[rel=veststyle]").hide();
            angular.element("#view1,#view2").find(".shirt_part[rel=vestlapel]").hide();
            angular.element("#view1,#view2").find(".shirt_part[rel=vestbuttons]").hide();
            angular.element("#view1,#view2").find(".shirt_part[rel=breastpocket]").hide();
            angular.element("#view1,#view2").find(".shirt_part[rel=vestpockets]").hide();
            angular.element("#view1,#view2").find(".shirt_part[rel=backveststyle]").hide();
            angular.element("#view1,#view2").find(".shirt_part[rel=vestlining]").hide();
            angular.element("#view1,#view2").find(".shirt_part[rel=vestcollar]").hide();

        } else {
            angular.element("#view1,#view2").find(".shirt_part[rel=veststyle]").show();
            angular.element("#view1,#view2").find(".shirt_part[rel=vestlapel]").show();
            angular.element("#view1,#view2").find(".shirt_part[rel=vestbuttons]").show();
            angular.element("#view1,#view2").find(".shirt_part[rel=breastpocket]").show();
            angular.element("#view1,#view2").find(".shirt_part[rel=vestpockets]").show();
            angular.element("#view1,#view2").find(".shirt_part[rel=backveststyle]").show();
            angular.element("#view1,#view2").find(".shirt_part[rel=vestcollar]").show();
            angular.element("#view1,#view2").find(".shirt_part[rel=vestlining]").show();
        }
    }





    function hideJacket()
    {
        //  console.log("$scope.IsJacket == " + $scope.IsJacket)
        if ($scope.IsJacket)
        {
//            console.log("inside isjacket")
//jacket view
            jQuery("#mainImageHolder").find("[rel=Style]").show();
            jQuery("#mainImageHolder").find("[rel=Lapel]").show();
            jQuery("#mainImageHolder").find("[rel=Pockets]").show();
            jQuery("#mainImageHolder").find("[rel=buttons]").show();
            jQuery("#mainImageHolder").find("[rel=Sleeves]").show();
            //back
            jQuery("#mainImageHolder").find("[rel=Vents]").show();
            jQuery("#mainImageHolder").find("[rel=BackStyle]").show();
            jQuery("#mainImageHolder").find("[rel=sleeve_buttons]").show();
            jQuery("#mainImageHolder").find("[rel=sleeve_buttonsThread]").show();
            jQuery("#mainImageHolder").find("[rel=threads]").show();
            jQuery("#mainImageHolder").find("[rel=pocketsquare]").show();
            jQuery("#mainImageHolder").find("[rel=Sleevesback]").show();
            jQuery("#mainImageHolder").find("[rel=Sleevesfold]").show();
            //show lining vest
            jQuery("#mainImageHolder").find("[rel=collarstyle]").show();
            jQuery("#mainImageHolder").find("[rel=jacketlining]").show();
            jQuery("#mainImageHolder").find("[rel=lining]").show();
            // jQuery("#mainImageHolder").find("[rel=vestlining]").hide();
            angular.element("#mainImageHolder").find(".shirt_part[rel=jacketbreastpocket]").show();
            jQuery("#mainImageHolder").find("[rel=backelbow]").show();
            jQuery("#mainImageHolder").find("[rel=backcollaraccent]").show();

            jQuery("#mainImageHolder").find("[rel=backcollar]").show();


            angular.element("#mainImageHolder").find(".shirt_part[rel=vestcollar]").hide();
            angular.element("#mainImageHolder").find(".shirt_part[rel=vestlining]").hide();

            $scope.IsTopCoat = true;//hide topCoat
            hideTopCoat();
        } else
        {

//front view
            jQuery("#mainImageHolder").find("[rel=Style]").hide();
            jQuery("#mainImageHolder").find("[rel=Lapel]").hide();
            jQuery("#mainImageHolder").find("[rel=Pockets]").hide();
            jQuery("#mainImageHolder").find("[rel=buttons]").hide();
            jQuery("#mainImageHolder").find("[rel=Sleeves]").hide();
            //back view
            jQuery("#mainImageHolder").find("[rel=BackStyle]").hide();
            jQuery("#mainImageHolder").find("[rel=Vents]").hide();
            jQuery("#mainImageHolder").find("[rel=sleeve_buttons]").hide();
            jQuery("#mainImageHolder").find("[rel=sleeve_buttonsThread]").hide();
            jQuery("#mainImageHolder").find("[rel=threads]").hide();
            jQuery("#mainImageHolder").find("[rel=pocketsquare]").hide();
            jQuery("#mainImageHolder").find("[rel=Sleevesback]").hide();
            jQuery("#mainImageHolder").find("[rel=Sleevesfold]").hide();
            //hide lining vest
            jQuery("#mainImageHolder").find("[rel=collarstyle]").hide();
            jQuery("#mainImageHolder").find("[rel=jacketlining]").hide();
            jQuery("#mainImageHolder").find("[rel=lining]").hide();
            //jQuery("#mainImageHolder").find("[rel=vestlining]").show();
            angular.element("#mainImageHolder").find(".shirt_part[rel=jacketbreastpocket]").hide();
            jQuery("#mainImageHolder").find("[rel=backelbow]").hide();
            jQuery("#mainImageHolder").find("[rel=backcollaraccent]").hide();
            jQuery("#mainImageHolder").find("[rel=backcollar]").hide();

            angular.element("#mainImageHolder").find(".shirt_part[rel=vestcollar]").show();
            angular.element("#mainImageHolder").find(".shirt_part[rel=vestlining]").show();

        }

    }


    function zoomOutProdImage()
    {
        $scope.IsVisible = true;
        $scope.isActive = false;
        if ($scope.myClassForZoom[0] == "zoomOut")
        {
            $scope.myClassForZoom.pop('zoomOut');
        } else {



            document.getElementById("prev-holder").innerHTML = "";

            if (jQuery(window).width() <= 900) {//for respnsive 

                jQuery(".loaderText").css({"display": "none"});
                jQuery("#theInitialProgress").css({"display": "block"});
                jQuery('#zoomOfTool').modal('show');
                jQuery('#mainImageHolder').css("height", jQuery("#view1 img:first").css("height").replace(/[^-\d\.]/g, ''));
                jQuery('#mainImageHolder').css("width", jQuery("#view1 img:first").css("width").replace(/[^-\d\.]/g, ''));

                jQuery('.modal-backdrop').css("display", "none");
                html2canvas(jQuery('#mainImageHolder'), {
                    onrendered: function (can) {
                        jQuery('.modal-backdrop').css("display", "none");
                        var data = can.toDataURL('image/png');
                        var image = new Image();
                        image.src = data;

                        image.height = jQuery("#view1 img:first").css("height").replace(/[^-\d\.]/g, '');
                        image.width = jQuery("#view1 img:first").css("width").replace(/[^-\d\.]/g, '');
                        var myEl = angular.element(document.querySelector('#prev-holder'));
                        myEl.append(image);

                        jQuery("#theInitialProgress").css({"display": "none"});
                        jQuery(".loaderText").css({"display": "block"});

                    }
                });
            } else {

                console.log("zoom")
                jQuery("#theInitialProgress").css({"display": "block"});
                jQuery(".container-fluid").css({"opacity": "0.3"});
                jQuery(".mainToolarea").css({"height": "1320px"});
                jQuery(".mainToolarea").css({"width": "1080px"});
                document.getElementById("prev-holder").innerHTML = "";
                jQuery("#zoomOfTool").modal("show");
                jQuery(".modal-content .modal-body").css({"margin-left": "300px"});
                setTimeout(function () {

                    jQuery("#mainImageHolder img").css({"width": "unset"});
                    html2canvas(document.getElementsByClassName("mainToolarea"), {
                        onrendered: function (can) {



                            var data = can.toDataURL('image/png');
                            var image = new Image();
                            image.height = 1320;
                            image.width = 1080;
                            image.src = data;
                            var myEl = angular.element(document.querySelector('#prev-holder'));
                            myEl.append(image);
                            jQuery("#mainImageHolder img").css({"width": "400px"});
                            jQuery("#mainImageHolder img").css({"max-width": "100%"});
                            jQuery("#theInitialProgress").css({"display": "none"});
                        }
                    });
                }, 500);

            }
        }
    }



    $scope.takeScreenShot = function () {

        zoomOutProdImage();
    }


    $scope.leftNavFabCat = function ($event, actCatName)
    {
        jQuery(".leftpanelsection .sidebar-options").hide();
        jQuery("#divOptionFilter").hide();
        jQuery("#div4").show();




        angular.element($event.currentTarget).parent().find(".activeDesign").removeClass("activeDesign")
        angular.element($event.currentTarget).addClass("activeDesign");

        angular.element('#fabircCatName').html(actCatName + " Fabrics");

        $scope.searchFabric = actCatName;



        var fabricFilterData = tempPj.fabric;

        if ($scope.searchFabric == "All" || $scope.searchFabric == "ALL" || $scope.searchFabric == "all")
        {
            var data = fabricFilterData;

        } else {
            var data = $filter('filter')(fabricFilterData, {category_parent: $scope.searchFabric});
        }

        $scope.fabricData.fabric = data;




        updateInItZoom();

    }

    $scope.leftNavFabCatFilterClose = function ($event)
    {

        $scope.backClearFilter();
//        angular.element($event.currentTarget).parent().find(".activeDesign").removeClass("activeDesign")
//        angular.element($event.currentTarget).addClass("activeDesign");
        jQuery("#divOptionFilter").hide();
        nullfabricTxt();
        //leftMainNav_fabricCat
        setTimeout(function () {
            jQuery(".leftpanelsection #div4").show();

            jQuery('#accordian').accordion("refresh");
        }, 50);
    }


    $scope.leftNavFabCatFilter = function ($event)
    {
//        angular.element($event.currentTarget).parent().find(".activeDesign").removeClass("activeDesign")
//        angular.element($event.currentTarget).addClass("activeDesign");

        jQuery(".leftpanelsection .sidebar-options").hide();
        //leftMainNav_fabricCat
        setTimeout(function () {

            jQuery("#divOptionFilter").show();
            jQuery('#accordian').accordion("refresh");
        }, 50);
    }

    $scope.leftNavFabCatMulti = function ($event)
    {
        angular.element($event.currentTarget).parent().find(".activeDesign").removeClass("activeDesign")
        angular.element($event.currentTarget).addClass("activeDesign");
        jQuery(".leftpanelsection .sidebar-options").hide();

        //leftMainNav_fabricCat
        setTimeout(function () {
            jQuery("#divOptionMulti").show();
        }, 50);
    }

    $scope.leftNavFabCatInfo = function ($event)
    {
        angular.element($event.currentTarget).parent().find(".activeDesign").removeClass("activeDesign")
        angular.element($event.currentTarget).addClass("activeDesign");

        jQuery(".leftpanelsection .sidebar-options").hide();
        //leftMainNav_fabricCat
        setTimeout(function () {
            jQuery("#divOptionInfo").show();
        }, 50);
    }

    $scope.changeSameDifferentFabric = function (part)
    {
        jQuery(".leftpanelsection .sidebar-options").hide();
        jQuery("#div4").show();

        //divOptionMulti
        angular.element("#divOptionMulti").find(".activeDesign").removeClass("activeDesign")


        if (part == 'same')
        {
            jQuery("#divOptionDifferentParts").hide();
            $scope.partOption = 'all';
            angular.element("#divOptionMulti .same").addClass("activeDesign");

        } else {
            jQuery("#divOptionDifferentParts").show();
            $scope.partOption = 'jacket';
            angular.element("#divOptionMulti .different").addClass("activeDesign");
        }


    }



//filter start


    $scope.applyFilter = function ()
    {
        jQuery(".leftpanelsection .sidebar-options").hide();
        jQuery("#div4").show();
        jQuery("#clearfilterfab").show();
        clrfilter = true;
        if ($scope.test == "" && $scope.filteredFabrics.length == $scope.fabrics.length) {
            jQuery("#clearfilterfab").hide();
        } else
        if ($scope.filteredFabrics.length == $scope.fabrics.length && $scope.test == undefined) {
            jQuery("#clearfilterfab").hide();
        } else {
            jQuery("#clearfilterfab").show();
        }

//        setTimeout(function () {
//            jQuery('.back-btn').trigger('click');
//        }, 200)

        console.log("In applyFilter")
        jQuery('body').addClass('open-fabric');
        nullfabricTxt();

    }
    function clearFilterActive() {

//        var clrfilter=jQuery("#div4").find(".active");
        if (clrfilter) {

            jQuery("#clearfilterfab").show();
        } else {
            jQuery("#clearfilterfab").hide();
        }
    }

    function nullfabricTxt() {

        console.log($scope.fabricData.fabric)

        if (jQuery(".fabricGene").children().length) {
            jQuery("#nullTxt").hide()
        } else {
            jQuery("#nullTxt").show()
        }

    }
    $scope.search = {};
    $scope.searchMat = {};
    $scope.searchSea = {};
    $scope.searchPat = {};
    $scope.searchCol = {};

    $scope.resetCheckBoxFilter = function ($event) {

        //console.log("keyy" + jQuery('#txt_name').val())

        $scope.searchFabricName = jQuery('#txt_name').val();
        var fabricFilterData = tempPj.fabric;
        var data = $filter('filter')(fabricFilterData, {name: $scope.searchFabricName});
        $scope.fabricData.fabric = data;

        //filter disable on input
        $scope.useMaterials = {};
        $scope.usePatterns = {};
        $scope.useSeasons = {};
        $scope.useColors = {};
        $scope.useCategory = {};
        //filter end

        if ($event.keyCode == 13) {
            $scope.applyFilter();
        }

    };


//old filter end


///new filter fabric code start

///fabric filter code start


    $scope.useMaterials = {};
    $scope.usePatterns = {};
    $scope.useSeasons = {};
    $scope.useColors = {};
    $scope.useCategory = {};
// Watch the pants that are selected

    $scope.$watch(function () {
        console.log("in watch")

        return {
            fabrics: $scope.fabrics,
            useMaterials: $scope.useMaterials,
            usePatterns: $scope.usePatterns,
            useSeasons: $scope.useSeasons,
            useColors: $scope.useColors,
            useCategory: $scope.useCategory
        }
    }, function (value) {
        var selected;

        $scope.count = function (prop, value) {
            return function (el) {
                return el[prop] == value;
            };
        };


        var filterAfterMaterials = [];
        selected = false;
        for (var j in $scope.fabrics) {
            var p = $scope.fabrics[j];
            for (var i in $scope.useMaterials) {
                if ($scope.useMaterials[i]) {
                    selected = true;
                    if (i == p.material_parent) {
                        filterAfterMaterials.push(p);
                        break;
                    }
                }
            }
        }
        if (!selected) {
            filterAfterMaterials = $scope.fabrics;
        }



        var filterAfterPatterns = [];
        selected = false;
        for (var j in filterAfterMaterials) {
            var p = filterAfterMaterials[j];
            for (var i in $scope.usePatterns) {
                console.log("i=" + i)
                if ($scope.usePatterns[i]) {
                    selected = true;
                    console.log("p.pattern_parent=" + p.pattern_parent)
                    if (i == p.pattern_parent) {
                        filterAfterPatterns.push(p);
                        break;
                    }
                }
            }
        }
        if (!selected) {
            filterAfterPatterns = filterAfterMaterials;
        }
        //season
        var filterAfterSeasons = [];
        selected = false;
        for (var j in filterAfterPatterns) {
            var p = filterAfterPatterns[j];
            for (var i in $scope.useSeasons) {
                console.log("i=" + i)
                if ($scope.useSeasons[i]) {
                    selected = true;
                    console.log("p.season_parent=" + p.season_parent)
                    if (i == p.season_parent) {

                        filterAfterSeasons.push(p);
                        break;
                    }
                }
            }
        }
        if (!selected) {
            filterAfterSeasons = filterAfterPatterns;
        }
        //season end

        var filterAfterColors = [];
        selected = false;
        for (var j in filterAfterSeasons) {
            var p = filterAfterSeasons[j];
            for (var i in $scope.useColors) {
                if ($scope.useColors[i]) {
                    selected = true;
                    if (i == p.color_parent) {
                        filterAfterColors.push(p);
                        break;
                    }
                }
            }
        }
        if (!selected) {
            filterAfterColors = filterAfterSeasons;
        }


        var filterAfterCategory = [];
        selected = false;
        for (var j in filterAfterColors) {
            var p = filterAfterColors[j];
            for (var i in $scope.useCategory) {
                if ($scope.useCategory[i]) {
                    selected = true;
                    if (i == p.category_parent) {
                        filterAfterCategory.push(p);
                        break;
                    }
                }
            }
        }
        if (!selected) {
            filterAfterCategory = filterAfterColors;
        }

        $scope.filteredFabrics = filterAfterCategory;
    }, true);


    $scope.$watch('filtered', function (newValue) {
        if (angular.isArray(newValue)) {
            console.log(newValue.length);
        }
    }, true);

//end fabric filter code


//filter fabric code

    angular.module("Suit").filter('count', function () {
        return function (collection, key) {
            var out = "test";
            for (var i = 0; i < collection.length; i++) {
                //console.log(collection[i].pants);
                //var out = myApp.filter('filter')(collection[i].pants, "42", true);
            }
            return out;
        }
    });


    angular.module("Suit").filter('groupBy',
            function () {
                return function (collection, key) {
                    if (collection === null)
                        return;
                    return uniqueItems(collection, key);
                };
            });


    function uniqueItems(data, key) {
        var result = [];

        for (var i = 0; i < data.length; i++) {



            var value = data[i][key];
            console.log("key = " + key + " value=  " + value);
            if (result.indexOf(value) == -1) {
                result.push(value);
            }

        }
        return result;
    }
    ;
    $scope.clearFilter = function () {

        $scope.test = "";
        var fabId = $scope.fabricId;

        $scope.useMaterials = {};
        $scope.usePatterns = {};
        $scope.useSeasons = {};
        $scope.useColors = {};
        $scope.useCategory = {};

        $scope.fabricData.fabric = [];

        for (var data in tempPj.fabric) {
            if (tempPj.fabric.hasOwnProperty(data))
            {
                console.log(data)
                $scope.fabricData.fabric.push(tempPj.fabric[data]);

            }
        }

        jQuery("#clearfilterfab").hide();
        clrfilter = false;
        jQuery("#nullTxt").hide();
        //angular.element("#div4").find(".activeDesign").removeClass('activeDesign');


        //angular.element($event.currentTarget).find(".fabvie").addClass("activeDesign");
        console.log("$scope.fabricId=" + $scope.fabricId);
        setTimeout(function () {
            jQuery("#div4 #" + fabricId).find(".fabvie").addClass("activeDesign");
        }, 200);



    }

    $scope.backClearFilter = function () {

        $scope.test = "";
        var fabId = $scope.fabricId;

        $scope.useMaterials = {};
        $scope.usePatterns = {};
        $scope.useSeasons = {};
        $scope.useColors = {};
        $scope.useCategory = {};

        $scope.fabricData.fabric = [];

        for (var data in tempPj.fabric) {
            if (tempPj.fabric.hasOwnProperty(data))
            {
                console.log(data)
                $scope.fabricData.fabric.push(tempPj.fabric[data]);

            }
        }

    }
//filter fabric code end

//new filter fabric code end

//filter end



    $scope.leftNav = function ($event, obj, ind) {

        currStyleName = '';
        angular.element(".suit-preloader").show();
        angular.element(".mainToolarea").css({"opacity": "0"});
        $scope.actLeftNav = angular.element($event.currentTarget).find("a").attr("target") ? angular.element($event.currentTarget).find("a").attr("target") : "1";
        angular.element($event.currentTarget).parent().find(".active").removeClass("active");
//        console.log($scope.actLeftNav)
        angular.element(".sidebar-options").hide();
        angular.element("#div" + $scope.actLeftNav).show();
        angular.element($event.currentTarget).addClass("active");
        //customize pant n hide jacket//removemin
        // console.log($event.currentTarget);
        var styleType = angular.element($event.currentTarget).attr("type")
        console.log("styleType-" + styleType);
        if (styleType == "pant")
        {


            angular.element("#mainImageHolder").find(".shirt_part[rel=jacketbreastpocket]").hide();
            angular.element(".jhStyle").css("cssText", "display: block !important;");
            jQuery('#linkAccent').parent().hide()//hiding accent section
            if (jQuery(window).width() <= 767) {
                $scope.isActive = false;
            } else {
                if ($scope.activeView == 1 || $scope.activeView == 2)
                {
                    $scope.IsVest = true;
                    jQuery('#mainImageHolder').find('[rel=veststyle]').hide();
                    jQuery('#mainImageHolder').find('[rel=vestlapel]').hide();
                    jQuery('#mainImageHolder').find('[rel=vestpockets]').hide();
                    jQuery('#mainImageHolder').find('[rel=breastpocket]').hide();
                    jQuery('#mainImageHolder').find('[rel=vestbuttons]').hide();
                    //vest back
                    jQuery('#mainImageHolder').find('[rel=backveststyle]').hide();
                    //lining
                    jQuery('#mainImageHolder').find('[rel=collarstyle]').show();
                    jQuery('#mainImageHolder').find('[rel=lining]').show();
                    jQuery('#mainImageHolder').find('[rel=vestcollar]').hide();
                    jQuery('#mainImageHolder').find('[rel=vestlining]').hide();
                    $scope.isActive = true;
                } else {

                    jQuery('#mainImageHolder').find('[rel=veststyle]').show();
                    jQuery('#mainImageHolder').find('[rel=vestlapel]').show();
                    jQuery('#mainImageHolder').find('[rel=vestpockets]').hide();
                    jQuery('#mainImageHolder').find('[rel=breastpocket]').hide();
                    jQuery('#mainImageHolder').find('[rel=vestbuttons]').show();
                    //vest back
                    jQuery('#mainImageHolder').find('[rel=backveststyle]').hide();
                    //lining
                    jQuery('#mainImageHolder').find('[rel=collarstyle]').show();
                    jQuery('#mainImageHolder').find('[rel=lining]').show();
                    jQuery('#mainImageHolder').find('[rel=vestcollar]').hide();
                    jQuery('#mainImageHolder').find('[rel=jacketlining]').show();
                    $scope.isActive = false;
                }
            }


            //check for view 3
            if ($scope.activeView == 3)
            {
                //show jacket
                $scope.IsJacket = true;
            } else {
                //hide jacket
                $scope.IsJacket = false;
            }


            hideJacket();
            //hide vest

            $scope.IsTopCoat = true;//hide TopCoat
            topcoatStatus = $scope.IsTopCoat;
            hideTopCoat();

        } else if (styleType == "vest")
        {

            angular.element(".jhStyle").css("cssText", "display: block !important;");
            angular.element("#mainImageHolder").find(".shirt_part[rel=jacketbreastpocket]").hide();
            jQuery('#linkAccent').parent().hide()//hiding accent section
            //jQuery('#linkAccent').parent().show()//showing accent section
            if (jQuery(window).width() <= 767) {

                $scope.isActive = false;
            } else {
                $scope.isActive = false;
            }

            //hide jacket
            $scope.IsJacket = false;
            //hide vest
            jQuery('#mainImageHolder').find('[rel=veststyle]').show();
            jQuery('#mainImageHolder').find('[rel=vestlapel]').show();
            jQuery('#mainImageHolder').find('[rel=vestpockets]').show();
            jQuery('#mainImageHolder').find('[rel=breastpocket]').show();
            jQuery('#mainImageHolder').find('[rel=vestbuttons]').show();
            //vest back
            jQuery('#mainImageHolder').find('[rel=backveststyle]').show();
            //lining
            jQuery('#mainImageHolder').find('[rel=collarstyle]').hide();
            jQuery('#mainImageHolder').find('[rel=lining]').hide();
            jQuery('#mainImageHolder').find('[rel=vestcollar]').show();
            jQuery('#mainImageHolder').find('[rel=jacketlining]').hide();
            jQuery('#mainImageHolder').find('[rel=vestlining]').show();
            hideJacket();
            $scope.IsTopCoat = true;//hide TopCoat
            topcoatStatus = $scope.IsTopCoat;
            hideTopCoat();
            $scope.IsVest = false//show
        } else if (styleType == "topcoat") {


            $scope.IsTopCoat = false;//show TopCoat
            topcoatStatus = $scope.IsTopCoat;
            hideTopCoat();
            jQuery(".TC").show();
            angular.element(".jhStyle").css("cssText", "display: none !important;");
            jQuery('#linkAccent').parent().hide()//hiding accent section

            angular.element("#mainImageHolder").find(".shirt_part[rel=jacketbreastpocket]").hide();
            $scope.isActive = false;//up down
            //check for view 3
            if ($scope.activeView == 3)
            {
                angular.element("#mainImageHolder").find(".shirt_part[rel=TopCoatCollarFold]").show();
                angular.element("#mainImageHolder").find(".shirt_part[rel=TopCoatLining]").show();
                angular.element("#mainImageHolder").find(".shirt_part[rel=pantfit_straight]").hide();
                angular.element("#mainImageHolder").find(".shirt_part[rel=pantfit_style]").hide();
                angular.element("#mainImageHolder").find(".shirt_part[rel=foldedPantPocket]").hide();
                angular.element("#mainImageHolder").find(".shirt_part[rel=pantFoldCuff]").hide();//
                angular.element("#mainImageHolder").find(".shirt_part[rel=foldPantPleats]").hide();
                //hide jacket
                $scope.IsJacket = false;

            } else {
                //hide Jacket
                $scope.IsJacket = false;//hide jacket
            }
            if (jQuery("#div6 #divvest_1 ul li").hasClass("activeDesign")) {
                jQuery("#showTopCoat,#hideTopCoat").show();

            } else {
                jQuery("#showTopCoat,#hideTopCoat").hide()
            }
            hideJacket();
            //jQuery(".jH").hide();


        } else {

            angular.element("#mainImageHolder").find(".shirt_part[rel=jacketbreastpocket]").show();
            angular.element(".jhStyle").css("cssText", "display: block !important;");
            jQuery('#linkAccent').parent().show()//showing accent section
            $scope.isActive = false;
            //show jacket
            $scope.IsJacket = true;
            //hide vest
            jQuery('#mainImageHolder').find('[rel=veststyle]').show();
            jQuery('#mainImageHolder').find('[rel=vestlapel]').show();
            jQuery('#mainImageHolder').find('[rel=vestpockets]').show();
            jQuery('#mainImageHolder').find('[rel=breastpocket]').show();
            jQuery('#mainImageHolder').find('[rel=vestbuttons]').show();
            //vest back
            jQuery('#mainImageHolder').find('[rel=backveststyle]').show();
            //lining
            jQuery('#mainImageHolder').find('[rel=collarstyle]').show();
            jQuery('#mainImageHolder').find('[rel=lining]').show();
            jQuery('#mainImageHolder').find('[rel=vestcollar]').hide();
            jQuery('#mainImageHolder').find('[rel=vestlining]').hide();
            jQuery('#mainImageHolder').find('[rel=jacketlining]').show();
            hideJacket();
            $scope.IsTopCoat = true;//hide TopCoat
            topcoatStatus = $scope.IsTopCoat;
            hideTopCoat();
            jQuery(".TC").hide();
        }



        imageLoaded();
        //hide name
        if (jQuery(".sidebar-options").hasClass("min"))
        {
            jQuery(".shorttext").show();
        } else {
            jQuery(".shorttext").hide();
            //removeActnMin();
        }

    }

    $scope.categoryClick = function ($event, obj, ind) {
        currStyleName = "";

        var navType = jQuery(".topmenu ul li.active").find("a").attr("target");
        jQuery(".shorttext").show();

        if (navType == "5") {
            $scope.actLeftNav = 5;
            $scope.activeView = 3;
        } else {
            if ($scope.actLeftNav == "5") {
                $scope.actLeftNav = 1;
            }
        }

        var designStyle = obj.name.replace(/\s+|&/g, "");

        console.log("categoryClick = " + designStyle + " ->" + $event.currentTarget);

        $scope.actCatName = designStyle;

        var actCon = angular.element("#div" + $scope.actLeftNav + " .box_option:first").attr("id") ? angular.element("#div" + $scope.actLeftNav + " .box_option:first").attr("id").split("_")[0] : "divs";
        angular.element("#div" + $scope.actLeftNav + " ul li ").removeClass("active");
        angular.element($event.currentTarget).addClass("active");
        angular.element($event.currentTarget).parent().find(".activeDesign").removeClass("activeDesign")
        angular.element($event.currentTarget).addClass("activeDesign");
        angular.element(".sidebar-options").addClass("min");
        angular.element(".box_options .box_option").removeClass("active");
        angular.element(".box_options #" + actCon + "_" + obj.id).addClass("active");
        setTimeout(function () {
            if (navType == "1") {
                jQuery(".leftMainNav").find(".active").trigger("click");
            }
        }, 50);
        $scope.IsBow = $scope.IsTie = false;

        if (designStyle == "Vents" || designStyle == "sleevebuttons")
        {
            $scope.activeView = 2;
        } else if (designStyle == "Lining") {

            $scope.activeView = 3;
        } else if (designStyle == "Style" || designStyle == "Lapel" || designStyle == "Pockets" || designStyle == "buttons")
        {
            $scope.activeView = 1;
        } else if (designStyle == "BackCollar") {
            $scope.activeView = 2;
//            if (!angular.element('#divaccent_5 ul:first').find('.activeDesign').attr('id'))
//            {
//                angular.element('#divaccent_5 ul:first li:first').addClass('activeDesign');
            angular.element('#divaccent_5 ul.BackCollar').css("display", "none");
//            }

        } else if (designStyle == "ElbowPatch") {
            $scope.activeView = 2;
//            if (!angular.element('#divaccent_6 ul:first').find('.activeDesign').attr('id'))
//            {
//                angular.element('#divaccent_6 ul:first li:first').addClass('activeDesign');
            angular.element('#divaccent_6 ul.ElbowPatch').css("display", "none");
//            }
        }


        /*else {
         
         $scope.activeView = 1;
         
         }*/


        setTimeout(function () {
            angular.element(".fabricInfo").find("a").trigger("click");
        }, 500);

        changeView();
    }

    function changeView()
    {
        currStyleName = "";
        angular.element("#main-container .view-thumb button").removeClass("active");
        var actCat = jQuery(".topmenu .active a").html();
        if (actCat == "Fabric") {
            jQuery("#clearfilterfab").hide();
            clearFilterActive();
        }

        active_view_id = $scope.activeView;
        if ($scope.activeView == "1")
        {
            jQuery("#zoomButton,#arrowUP,#arrowDown,#showVest,#vestHide").show();
            if (angular.element(".leftMainNav").children().find(".active").attr("type") == "pant" && actCat == "Style")
            {

                if (jQuery(window).width() <= 767) {

                    $scope.isActive = false;
                } else {
                    $scope.isActive = true;
                }


                $scope.IsJacket = false;
                hideJacket();



//                if (jQuery(window).width() <= 767) {
//
//                    $scope.isActive = false;
//                } else {
//                    $scope.isActive = true;
//                }
//
//                $scope.IsVest = true;//hide
//                $scope.IsJacket = false;
            } else if (angular.element(".leftMainNav").children().find(".active").attr("type") == "vest") {

                $scope.IsJacket = false;
            } else
            {
                if ($scope.IsJacket) {

                    $scope.IsJacket = true;
                } else {

                    $scope.IsJacket = false;
                }
                // $scope.IsVest=false;//show
//                if ($scope.IsJacket) {
//
//                    $scope.IsJacket = true;
//                } else {
//                    $scope.IsJacket = false;
//                }

            }

            angular.element("#Front").addClass("active");
            angular.element(".jH").show();
            if (angular.element(".leftMainNav").children().find(".active").attr("type") == "pant") {


                jQuery('#mainImageHolder').find('[rel=veststyle]').hide();
                jQuery('#mainImageHolder').find('[rel=vestlapel]').hide();
                jQuery('#mainImageHolder').find('[rel=vestpockets]').hide();
                jQuery('#mainImageHolder').find('[rel=breastpocket]').hide();
                jQuery('#mainImageHolder').find('[rel=vestbuttons]').hide();
                //vest back
                jQuery('#mainImageHolder').find('[rel=backveststyle]').hide();
                //lining
                jQuery('#mainImageHolder').find('[rel=collarstyle]').hide();
                jQuery('#mainImageHolder').find('[rel=lining]').hide();
                jQuery('#mainImageHolder').find('[rel=vestcollar]').hide();
                jQuery('#mainImageHolder').find('[rel=vestlining]').hide();
                $scope.IsJacket = false;
                $scope.isActive = true;

            }
            angular.element("#jacketHide").css({"display": "inline-block"});
            //angular.element(".navigate-menu").find("li.jV").show()

        } else if ($scope.activeView == "2")
        {

            jQuery("#zoomButton,#arrowUP,#arrowDown,#showVest,#vestHide").show();
            angular.element("#Back").addClass("active");
            angular.element(".jH").show();
            if (angular.element(".leftMainNav").children().find(".active").attr("type") == "pant" && actCat == "Style")
            {

//                if (pantbeltlop == "8" || pantpokt == "7")
//                {
//                    $scope.IsVest = false;
//                } else
//                {
//                    $scope.IsVest = true;
//                }

                if (jQuery(window).width() <= 767) {

                    $scope.isActive = false;
                } else {
                    $scope.isActive = true;
                }

                $scope.IsJacket = false;

//                if (jQuery(window).width() <= 767) {
//
//                    $scope.isActive = false;
//                } else {
//                    $scope.isActive = true;
//                }
//                $scope.IsVest = true;//hide
//                $scope.IsJacket = false;
            } else if (angular.element(".leftMainNav").children().find(".active").attr("type") == "vest") {
                $scope.IsJacket = false;
                // $scope.IsVest=false;//show
            } else {
                if ($scope.IsJacket) {
                    $scope.IsJacket = true;
                } else {
                    $scope.IsJacket = false;
                }
            }

            if (angular.element(".leftMainNav").children().find(".active").attr("type") == "pant") {


                jQuery('#mainImageHolder').find('[rel=veststyle]').hide();
                jQuery('#mainImageHolder').find('[rel=vestlapel]').hide();
                jQuery('#mainImageHolder').find('[rel=vestpockets]').hide();
                jQuery('#mainImageHolder').find('[rel=breastpocket]').hide();
                jQuery('#mainImageHolder').find('[rel=vestbuttons]').hide();
                //vest back
                jQuery('#mainImageHolder').find('[rel=backveststyle]').hide();
                //lining
                jQuery('#mainImageHolder').find('[rel=collarstyle]').hide();
                jQuery('#mainImageHolder').find('[rel=lining]').hide();
                jQuery('#mainImageHolder').find('[rel=vestcollar]').hide();
                jQuery('#mainImageHolder').find('[rel=vestlining]').hide();
                $scope.IsJacket = false;
                $scope.isActive = true;
            }
            angular.element("#jacketHide").css({"display": "inline-block"});
            angular.element(".navigate-menu").find("li.jV").show()
        } else {

            angular.element("#Inside").addClass("active");
            angular.element(".jH").show();
            angular.element(".navigate-menu").find("li.jV").hide()

            $scope.isActive = false;
            if (angular.element(".leftMainNav").children().find(".active").attr("type") == "vest" && actCat == "Style")
            {

                $scope.IsJacket = false;
                jQuery('#view3').find('[rel=veststyle]').show();
                jQuery('#view3').find('[rel=vestlapel]').show();
                jQuery('#view3').find('[rel=vestpockets]').show();
                jQuery('#view3').find('[rel=breastpocket]').show();
                jQuery('#view3').find('[rel=vestbuttons]').show();
                //vest back
                jQuery('#view3').find('[rel=backveststyle]').show();
                //lining
                jQuery('#view3').find('[rel=collarstyle]').show();
                jQuery('#view3').find('[rel=lining]').show();
                jQuery('#view3').find('[rel=vestcollar]').show();
                jQuery('#view3').find('[rel=vestlining]').show();

            } else if (angular.element(".leftMainNav").children().find(".active").attr("type") == "vest") {
                $scope.IsJacket = false;


            } else if (angular.element(".leftMainNav").children().find(".active").attr("type") == "topcoat") {
                $scope.IsJacket = false;
            } else {
//                jQuery('#view3').find('[rel=veststyle]').show();
//                jQuery('#view3').find('[rel=vestlapel]').show();
//                jQuery('#view3').find('[rel=vestpockets]').show();
//                jQuery('#view3').find('[rel=breastpocket]').show();
//                jQuery('#view3').find('[rel=vestbuttons]').show();
//                //vest back
//                jQuery('#view3').find('[rel=backveststyle]').show();
//                //lining
//                jQuery('#view3').find('[rel=collarstyle]').show();
//                jQuery('#view3').find('[rel=lining]').show();
//                jQuery('#view3').find('[rel=vestcollar]').show();
//                jQuery('#view3').find('[rel=vestlining]').show();
                $scope.IsJacket = true;
            }
            if (angular.element(".leftMainNav").children().find(".active").attr("type") == "pant") {

                if ($scope.IsVest) {
                    jQuery('#view1').find('[rel=veststyle]').show();
                    jQuery('#view2').find('[rel=veststyle]').show();
                    jQuery('#view1').find('[rel=vestbackbuckle]').show();
                    jQuery('#view2').find('[rel=vestbackbuckle]').show();
                }

                jQuery('#view1').find('[rel=vestlapel]').show();
                jQuery('#view2').find('[rel=vestlapel]').show();
                jQuery('#view1').find('[rel=vestpockets]').hide();
                jQuery('#view2').find('[rel=vestpockets]').hide();
                jQuery('#view1').find('[rel=breastpocket]').hide();
                jQuery('#view2').find('[rel=breastpocket]').hide();
                jQuery('#view1').find('[rel=vestbuttons]').show();
                jQuery('#view2').find('[rel=vestbuttons]').show();

                //vest back
                jQuery('#view1').find('[rel=backveststyle]').hide();
                jQuery('#view2').find('[rel=backveststyle]').hide();


                //lining
                jQuery('#mainImageHolder').find('[rel=collarstyle]').show();
                jQuery('#mainImageHolder').find('[rel=lining]').show();
                jQuery('#view1').find('[rel=vestcollar]').hide();
                jQuery('#view2').find('[rel=vestcollar]').hide();
                jQuery('#mainImageHolder').find('[rel=jacketlining]').show();


//                jQuery('#mainImageHolder').find('[rel=veststyle]').show();
//                jQuery('#mainImageHolder').find('[rel=vestlapel]').show();
//                jQuery('#mainImageHolder').find('[rel=vestpockets]').hide();
//                jQuery('#mainImageHolder').find('[rel=breastpocket]').hide();
//                jQuery('#mainImageHolder').find('[rel=vestbuttons]').show();
//                //vest back
//                jQuery('#mainImageHolder').find('[rel=backveststyle]').hide();
//                //lining
//                jQuery('#mainImageHolder').find('[rel=collarstyle]').show();
//                jQuery('#mainImageHolder').find('[rel=lining]').show();
//                jQuery('#mainImageHolder').find('[rel=vestcollar]').hide();
//                jQuery('#mainImageHolder').find('[rel=jacketlining]').show();
            }

            jQuery("#arrowUP,#arrowDown").hide();

            if (angular.element(".leftMainNav").children().find(".active").attr("type") == "topcoat") {

                angular.element("#mainImageHolder").find(".shirt_part[rel=pantfit_straight]").hide();
                angular.element("#mainImageHolder").find(".shirt_part[rel=pantfit_style]").hide();
                angular.element("#mainImageHolder").find(".shirt_part[rel=foldedPantPocket]").hide();

            }
        }

        hideJacket();
        imageLoaded();
        //check for mobile
        if (jQuery(window).width() <= 900) {

            jQuery("#arrowUP,#arrowDown,#zoomButton,#zoomOutButton").hide();
        }
    }


    $scope.removemin = function ($event) {
        removeActnMin();
    }

    function removeActnMin() {


        if (jQuery(window).width() <= 767) {

            angular.element(".sidebar-options").removeClass("min");

            angular.element(".LeftpanemOpen .leftpanelsection").css("left", "0");
        }

        angular.element(".box_option").removeClass("active");
        //hide stle name
        var type = jQuery(".topmenu .active a").attr("target");
        setTimeout(function () {
            if (type == "5")
            {
                angular.element(".topmenu a[target=" + type + "]").trigger("click");
            } else {
                angular.element(".leftMainNav").find(".active a").trigger("click");
            }

            jQuery(".shorttext").hide();
        }, 100);
        //hide stle name end


    }



    //change Style of Jacket 
    $scope.designClick = function ($event, obj, ind, icon) {


        style_ID = jQuery(obj).attr("id");
        angular.element(".suit-preloader").show();
        angular.element(".mainToolarea").css({"opacity": "0"});

        var parent, styleImg, styleType, styleName, styleId;
        angular.element($event.currentTarget).parent().find(".activeDesign").removeClass("activeDesign");
        angular.element($event.currentTarget).addClass("activeDesign");
        if (jQuery(window).width() <= 767) {

            angular.element(".sidebar-options").removeClass("min");
            angular.element(".LeftpanemOpen .leftpanelsection").css("left", "0");
        }

        angular.element("body").removeClass("LeftpanemOpen");
        $scope.activeStyle = obj;
        currStyleName = parent = $scope.activeStyle.parent.replace(/\s+|&/g, "");
        styleName = $scope.activeStyle.name.replace(/\s+|&/g, "");
        styleId = $scope.activeStyle.id;
        //get style type single or double
        styleType = $scope.activeStyle.designType; //angular.element("#view" + $scope.activeView).find('.shirt_part[rel=' + parent + ']').attr("type");

        temp[parent] = $scope.actStyleData[parent] = styleId;

        if (jQuery("#div6 #divvest_1 ul li").hasClass("activeDesign")) {
            if (jQuery("#linkJacket").parent().hasClass("active")) {
                jQuery("#showTopCoat,#hideTopCoat").hide();
            } else {
                jQuery("#showTopCoat,#hideTopCoat").show();
            }

        } else {
            jQuery("#showTopCoat,#hideTopCoat").hide()
        }
        console.log("style type in design click ---" + styleType)
        console.log("parent == " + parent + " style Id=" + styleId)

//  var a = jQuery("#div1 .box_options>div").length;
//
//        for (var i = 1; i <= a; i++) {
//            stylePriceArr[i] = jQuery("#div1  #divs_" + i + " .activeDesign").find(".price").text().trim("");
//            console.log("stylePriceArr[" + i + "]" + stylePriceArr[i])
//        }





        if ($scope.activeStyle.price) {
            stylePriceArr[parent] = $scope.activeStyle.price;
        } else {
            stylePriceArr[parent] = "0.0";
        }



        //check style
        if (styleName == "Mandarin")
        {
            jQuery("#bow,#bow1").hide();
            jQuery("#tie,#tie1").hide();
            angular.element(".theView").find('.shirt_part[rel=Lapel]').hide();

            angular.element(".theView").find('.shirt_part[rel=Tie]').hide();
            angular.element(".theView").find('.shirt_part[rel=ManBow]').hide();

        } else
        {

            jQuery("#bow,#bow1").show();
            jQuery("#tie,#tie1").show();
            angular.element(".theView").find('.shirt_part[rel=Lapel]').show();
            if ($scope.IsTie && (!$scope.IsBow)) {

                angular.element(".theView").find('.shirt_part[rel=Tie]').show();
                angular.element(".theView").find('.shirt_part[rel=ManBow]').hide();


            } else if ($scope.IsBow && (!$scope.IsTie)) {
                angular.element(".theView").find('.shirt_part[rel=Tie]').hide();
                angular.element(".theView").find('.shirt_part[rel=ManBow]').show();


            }
//            $scope.activeView = 1;
//            changeView();
        }
        //$scope.actStylePantArr['beltfoldloop'] = basePath + "media/default_tool_img/blank.png";smb://192.168.2.41/html/p1135/media/pant_images/pantbeltloops/1_pantbeltloops_1_view_1.png


        //chnage View
        if (parent == "Vents" || parent == "sleeve_buttons")
        {
//            $scope.activeView = 2;
//            changeView();
        } else {

            /*$scope.activeView = 1;
             changeView();*/
        }

        angular.element("#view1").find('.shirt_part[rel=Sleeves]').attr("src", basePath + "media/jacket_images/sleeves/" + fabricId + "_suitsleeves_1_view_1.png");
        $scope.actStyleJKArr['Sleeves'] = basePath + "media/jacket_images/sleeves/" + fabricId + "_suitsleeves_1_view_1.png";
        angular.element("#view2").find('.shirt_part[rel=Sleevesback]').attr("src", basePath + "media/jacket_images/sleeves/" + fabricId + "_suitsleeves_1_view_2.png");
        $scope.actStyleJKArr['Sleevesback'] = basePath + "media/jacket_images/sleeves/" + fabricId + "_suitsleeves_1_view_2.png";
        angular.element("#view3").find('.shirt_part[rel=Sleevesfold]').attr("src", basePath + "media/jacket_images/sleeves/" + fabricId + "_suitsleeves_1_view_3.png");
        $scope.actStyleJKArr['Sleevesfold'] = basePath + "media/jacket_images/sleeves/" + fabricId + "_suitsleeves_1_view_3.png";

        angular.element("#view2").find('.shirt_part[rel=backcollar]').attr("src", basePath + "media/jacket_images/back_collar/" + fabricId + "_backCollar_view_2.png");
        $scope.actStyleJKArr['backcollar'] = basePath + "media/jacket_images/back_collar/" + fabricId + "_backCollar_view_2.png";

        angular.element("#view1").find('.shirt_part[rel=jacketbreastpocket]').attr("src", basePath + "media/jacket_images/pockets/" + fabricId + "_breastpocket_view_1.png");
        angular.element("#view3").find('.shirt_part[rel=jacketbreastpocket]').attr("src", basePath + "media/jacket_images/pockets/" + fabricId + "_breastpocket_view_1.png");
        $scope.actStyleJKArr['jacketbreastpocket'] = basePath + "media/jacket_images/pockets/" + fabricId + "_breastpocket_view_1.png";//breat pocket for jacket

        if (parent == "Fit")
        {
            styleImg = basePath + "media/jacket_images/" + parent.toLowerCase() + "/" + parent.toLowerCase() + "/" + parent.toLowerCase() + "_" + styleId + ".png";
            $scope.actStyleJKArr['FitBack'] = basePath + "media/jacket_images/" + parent.toLowerCase() + "/" + parent.toLowerCase() + "_" + styleId + ".png";//

        } else if (parent == "Pockets")
        {

            var styleTypeId = angular.element("#view1").find('.shirt_part[rel=Style]').attr("id") ? angular.element("#view1").find('.shirt_part[rel=Style]').attr("id") : "1";
            if (styleTypeId == "4") {
                $scope.actStyleJKArr['Pockets'] = styleImg = basePath + "media/jacket_images/pockets/" + $scope.fabricId + "_casual_pockets_" + styleId + "_view_1.png";
            } else {
                $scope.actStyleJKArr['Pockets'] = styleImg = basePath + "media/jacket_images/pockets/" + $scope.fabricId + "_pockets_" + styleId + "_view_1.png";
            }
        } else if (parent == "Lapel")
        {
            var styleTypeId = angular.element("#view1").find('.shirt_part[rel=Style]').attr("id") ? angular.element("#view1").find('.shirt_part[rel=Style]').attr("id") : "1";
            var LapelTypeId = angular.element("#lapelSize").prev().prev().find(".activeDesign").attr("id");
            styleImg = basePath + "media/jacket_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_" + parent.toLowerCase() + "style_" + styleTypeId + "_size_" + styleId + "_" + parent.toLowerCase() + "type_" + LapelTypeId + "_view_1.png";

        } else if (parent == "Vents")
        {
            styleImg = $scope.activeStyle.main_img;
        } else if (parent == "sleeve_buttons")
        {
            $scope.sleeveBtnCnt = $scope.activeStyle.btn_count;
            styleImg = basePath + "media/jacket_images/" + parent.toLowerCase() + "/" + $scope.activeStyle.btn_count + "_color_4.png";
        } else if (parent == "buttons")
        {
            var styleTypeId = angular.element("#view1").find('.shirt_part[rel=Style]').attr("id") ? angular.element("#view1").find('.shirt_part[rel=Style]').attr("id") : "1";
            if (styleTypeId == "4") {
                $scope.actStyleJKArr['buttons'] = styleImg = basePath + "media/jacket_images/buttons/casual/1_buttons_2_color_4.png";
            } else {
                $scope.actStyleJKArr['buttons'] = styleImg = basePath + "media/jacket_images/" + parent.toLowerCase() + "/" + styleTypeId + "_" + parent.toLowerCase() + "_" + $scope.activeStyle.btn_count + "_color_4.png";
            }


            //update btn count in style
            angular.element("#view1").find('.shirt_part[rel=Style]').attr("btnCount", $scope.activeStyle.btn_count);
            $scope.actStyleJKArr['threads'] = basePath + "media/jacket_images/threads/" + currentThreadsId + "_threads_" + styleTypeId + "_btn_" + $scope.activeStyle.btn_count + "_view_1.png";

        } else if (parent == "pantfit")
        {
            console.log("styleid- = " + styleId)
            styleImg = basePath + "media/pant_images/" + parent.toLowerCase() + "/" + fabricId + "_pantfit_2_view_1.png";
            $scope.actStylePantArr['pantfit'] = basePath + "media/pant_images/" + parent.toLowerCase() + "/" + fabricId + "_pantfit_2_view_1.png";
            $scope.actStylePantArr['backpantfit'] = basePath + "media/pant_images/" + parent.toLowerCase() + "/" + fabricId + "_pantfit_2_view_2.png";
            $scope.actStylePantArr['pantfit_straight'] = basePath + "media/pant_images/" + parent.toLowerCase() + "/" + fabricId + "_pantfit_2_view_3.png";
            angular.element("#view3").find('.shirt_part[rel=pantfit_straight]').attr("src", basePath + "media/pant_images/" + parent.toLowerCase() + "/" + fabricId + "_pantfit_2_view_3.png")

            $scope.actStylePantArr['beltloop'] = basePath + "media/pant_images/pantbeltloops/" + $scope.fabricId + "_pantbeltloops_1_view_1.png";
            //://192.168.2.41/html/p1135/media/pant_images/pantbeltloops/1_pantbeltloops_1_view_1.png
            $scope.actStylePantArr['backbeltloop'] = basePath + "media/pant_images/pantbeltloops/" + $scope.fabricId + "_pantbeltloops_" + 1 + "_view_2.png";
            $scope.actStylePantArr['beltfoldloop'] = basePath + "media/pant_images/pantbeltloops/" + $scope.fabricId + "_pantbeltloops_" + 1 + "_view_3.png";

            if (styleId == "2") {
                $scope.actStylePantArr['pantBackStyle'] = basePath + "media/pant_images/" + parent.toLowerCase() + "/Slim_static/Slim_mask_back.png";//view2 mask of slim and tailored
                $scope.actStylePantArr['pantStyle'] = basePath + "media/pant_images/" + parent.toLowerCase() + "/Slim_static/Slim_Mask.png";//view1 mask of slim and tailored
                $scope.actStylePantArr['pantfit_style'] = basePath + "media/pant_images/" + parent.toLowerCase() + "/Slim_static/Slim_mask_view_3.png";
                angular.element("#view1").find('.shirt_part[rel=pantStyle]').attr("src", basePath + "media/pant_images/" + parent.toLowerCase() + "/Slim_static/Slim_Mask.png")
                angular.element("#view2").find('.shirt_part[rel=pantBackStyle]').attr("src", basePath + "media/pant_images/" + parent.toLowerCase() + "/Slim_static/Slim_mask_back.png")
                angular.element("#view3").find('.shirt_part[rel=pantfit_style]').attr("src", basePath + "media/pant_images/" + parent.toLowerCase() + "/Slim_static/Slim_mask_view_3.png")
            } else {
                $scope.actStylePantArr['pantBackStyle'] = basePath + "media/default_tool_img/blank.png";//view2 mask of slim and tailored
                $scope.actStylePantArr['pantStyle'] = basePath + "media/default_tool_img/blank.png";//view1 mask of slim and tailored
                $scope.actStylePantArr['pantfit_style'] = basePath + "media/default_tool_img/blank.png";
                angular.element("#view1").find('.shirt_part[rel=pantStyle]').attr("src", basePath + "media/default_tool_img/blank.png")
                angular.element("#view2").find('.shirt_part[rel=pantBackStyle]').attr("src", basePath + "media/default_tool_img/blank.png")
                angular.element("#view3").find('.shirt_part[rel=pantfit_style]').attr("src", basePath + "media/default_tool_img/blank.png")
            }

        } else if (parent == "pantpleats" || parent == "pantcuff")
        {

            if (parent == "pantpleats") {

                styleImg = basePath + "media/pant_images/" + parent.toLowerCase() + "/1_pantpleats_" + styleId + "_view_1.png";
                $scope.actStylePantArr['pantpleats'] = basePath + "media/pant_images/" + parent.toLowerCase() + "/1_pantpleats_" + styleId + "_view_1.png";
                $scope.actStylePantArr['foldPantPleats'] = basePath + "media/pant_images/" + parent.toLowerCase() + "/1_pantpleats_" + styleId + "_view_3.png";
                angular.element("#view3").find('.shirt_part[rel=foldPantPleats]').attr("src", basePath + "media/pant_images/" + parent.toLowerCase() + "/1_pantpleats_" + styleId + "_view_3.png");
                console.log("styleimage for pantpleats --0 " + styleImg)
            }

            if (parent == "pantcuff")
            {

                styleImg = basePath + "media/pant_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_" + parent.toLowerCase() + "_" + styleId + "_view_1.png";
                console.log("styl = " + styleImg)
                $scope.actStylePantArr['pantcuff'] = styleImg;
                $scope.actStylePantArr['backpantcuff'] = basePath + "media/pant_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_" + parent.toLowerCase() + "_" + styleId + "_view_2.png";
                $scope.actStylePantArr['pantFoldCuff'] = basePath + "media/pant_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_" + parent.toLowerCase() + "_" + styleId + "_view_3.png";


            }



        } else if (parent == "fastening" || parent == "pantpockets")
        {

            if (parent == "fastening") {
                if (styleId == 1) {
                    styleImg = basePath + "media/jacket_images/" + parent.toLowerCase() + "/Main-Pant_Fastening_Centered_Glow.png";
                    $scope.actStylePantArr['fasteningButton'] = basePath + "media/jacket_images/" + parent.toLowerCase() + "/Button_centered.png";
                } else {
                    styleImg = basePath + "media/jacket_images/" + parent.toLowerCase() + "/Main-Pant_Fastening_Off-Centered_Glow.png";
                    $scope.actStylePantArr['fasteningButton'] = basePath + "media/default_tool_img/blank.png"
                }


            } else if (parent == "pantpockets") {

                styleImg = basePath + "media/pant_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_pantpockets_" + styleId + "_view_1.png";
                $scope.actStyleVestArr['pantpockets'] = styleImg;
                angular.element("#view3").find('.shirt_part[rel=foldedPantPocket]').attr("src", basePath + "media/pant_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_pantpockets_" + styleId + "_view_3.png")
                $scope.actStyleVestArr['foldedPantPocket'] = basePath + "media/pant_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_pantpockets_" + styleId + "_view_3.png"
            }

        } else if (parent == "veststyle" || parent == "vestedge")
        {
            console.log(styleName)
            if (parent == "vestedge")
            {
                var vestStyleId = angular.element("#view1").find('.shirt_part[rel=veststyle]').attr("id") ? angular.element("#view1").find('.shirt_part[rel=veststyle]').attr("id") : "1";
                var vestEdgeId = styleId; //angular.element("#view1").find('.shirt_part[rel=vestedge]').attr("id") ? angular.element("#view1").find('.shirt_part[rel=vestedge]').attr("id") : "1";

                styleImg = basePath + "media/default_tool_img/blank.png";
                if ($scope.activeView == 3) {
                    $scope.actStyleVestArr['veststyle'] = basePath + "media/vest_images/veststyle/" + $scope.fabricId + "_style_" + vestStyleId + "_bottom_" + vestEdgeId + "_view_1.png";
                } else {
                    if ($scope.activeView == "1") {
                        $scope.actStyleVestArr['veststyle'] = basePath + "media/vest_images/veststyle/" + $scope.fabricId + "_style_" + vestStyleId + "_bottom_" + vestEdgeId + "_view_1.png";
                    } else {
                        $scope.actStyleVestArr['veststyle'] = basePath + "media/vest_images/veststyle/" + $scope.fabricId + "_style_" + vestStyleId + "_bottom_" + vestEdgeId + "_view_1.png";
                    }

                }
            } else {
                var vestStyleId = styleId; // angular.element("#view1").find('.shirt_part[rel=veststyle]').attr("id") ? angular.element("#view1").find('.shirt_part[rel=veststyle]').attr("id") : "1";
                var vestEdgeId = angular.element("#view1").find('.shirt_part[rel=vestedge]').attr("id") ? angular.element("#view1").find('.shirt_part[rel=vestedge]').attr("id") : "1";
                var vestLapelId = angular.element("#view1").find('.shirt_part[rel=vestlapel]').attr("id") ? angular.element("#view1").find('.shirt_part[rel=vestlapel]').attr("id") : "1";
                $scope.vestStyleBtnType = vestStyleId;

                if ($scope.activeView == 3) {
                    styleImg = basePath + "media/vest_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + vestStyleId + "_bottom_" + vestEdgeId + "_view_1.png";
                } else {
                    if ($scope.activeView == "1") {
                        styleImg = basePath + "media/vest_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + vestStyleId + "_bottom_" + vestEdgeId + "_view_1.png";
                    } else {
                        styleImg = basePath + "media/vest_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + vestStyleId + "_bottom_" + vestEdgeId + "_view_1.png";
                    }

                }


                $scope.actStyleVestArr['veststyle'] = styleImg;
                $scope.actStyleVestArr['backveststyle'] = basePath + "media/vest_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + vestStyleId + "_bottom_" + vestEdgeId + "_view_2.png";
                $scope.actStyleVestArr['vestlapel'] = basePath + "media/vest_images/vestlapel/" + $scope.fabricId + "_style_" + vestStyleId + "_lapel_" + vestLapelId + "_view_1.png";
                //update vestbutton image


                var vestStyleBtCnt = angular.element("#view1").find('.shirt_part[rel=veststyle]').attr("btnCount");
                if (vestStyleId == "1")
                {
                    angular.element("#view1").find('.shirt_part[rel=veststyle]').attr("btnCount", "3")
                    $scope.actStyleVestArr['vestbuttons'] = basePath + "media/vest_images/vestbuttons/" + vestStyleId + "_buttons_3_color_4.png";
                } else {
                    angular.element("#view1").find('.shirt_part[rel=veststyle]').attr("btnCount", "4")
                    $scope.actStyleVestArr['vestbuttons'] = basePath + "media/vest_images/vestbuttons/" + vestStyleId + "_buttons_4_color_4.png";
                }



                //end



            }

            dispStyleBtn(styleId, parent);


        } else if (parent == "vestlapel")
        {
            var vestStyleId = angular.element("#view1").find('.shirt_part[rel=veststyle]').attr("id") ? angular.element("#view1").find('.shirt_part[rel=veststyle]').attr("id") : "1";
            var vestLapelId = styleId; //angular.element("#view1").find('.shirt_part[rel=vestlapel]').attr("id") ? angular.element("#view1").find('.shirt_part[rel=vestlapel]').attr("id") : "1";

            if ($scope.activeView == 3) {
                styleImg = basePath + "media/vest_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + vestStyleId + "_lapel_" + vestLapelId + "_view_1.png";
            } else {
                styleImg = basePath + "media/vest_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + vestStyleId + "_lapel_" + vestLapelId + "_view_1.png";
            }

        } else if (parent == "vestbuttons")
        {
            var vestStyleId = angular.element("#view1").find('.shirt_part[rel=veststyle]').attr("id") ? angular.element("#view1").find('.shirt_part[rel=veststyle]').attr("id") : "1";
            angular.element("#view1").find('.shirt_part[rel=veststyle]').attr("btnCount", $scope.activeStyle.btn_count);
            styleImg = basePath + "media/vest_images/" + parent.toLowerCase() + "/" + vestStyleId + "_buttons_" + $scope.activeStyle.btn_count + "_color_4.png";

        } else if (parent == "vestpockets" || parent == "breastpocket")
        {
            if (parent == "breastpocket")
            {

                if (styleId == "2")
                {
                    styleImg = basePath + "media/default_tool_img/blank.png";
                } else
                {
                    if ($scope.activeView == 3) {
                        styleImg = basePath + "media/vest_images/vestpocket/" + $scope.fabricId + "_" + parent.toLowerCase() + "_view_1.png";
                    } else {
                        styleImg = basePath + "media/vest_images/vestpocket/" + $scope.fabricId + "_" + parent.toLowerCase() + "_view_1.png";
                    }
                }
            } else
            {
                if ($scope.activeView == 3) {
                    styleImg = basePath + "media/vest_images/vestpocket/" + $scope.fabricId + "_vestpocket_" + styleId + "_view_1.png";
                } else {
                    styleImg = basePath + "media/vest_images/vestpocket/" + $scope.fabricId + "_vestpocket_" + styleId + "_view_1.png";
                }

            }


        } else if (parent == "topcoatstyle") {

            angular.element("#mainImageHolder").find(".shirt_part[rel=pantfit_straight]").hide();
            angular.element("#mainImageHolder").find(".shirt_part[rel=pantfit_style]").hide();//
            angular.element("#mainImageHolder").find(".shirt_part[rel=pantfit_style]").hide();
            angular.element("#mainImageHolder").find(".shirt_part[rel=pantFoldCuff]").hide();//
            angular.element("#mainImageHolder").find(".shirt_part[rel=foldPantPleats]").hide();
            jQuery('#view3').find('.shirt_part[rel=foldedPantPocket]').hide()
            styleImg = basePath + "media/topcoat_images/styles/" + $scope.fabricId + "_coatstyles_" + styleId + "_view_1.png";
            angular.element("#mainImageHolder").find('.shirt_part[rel=topcoatstyle]').attr("src", basePath + "media/topcoat_images/styles/" + $scope.fabricId + "_coatstyles_" + styleId + "_view_1.png");
            angular.element("#mainImageHolder").find('.shirt_part[rel=TopCoatCollar]').attr("src", basePath + "media/topcoat_images/collars/" + $scope.fabricId + "_coatcollar_" + styleId + "_view_1.png");
            angular.element("#mainImageHolder").find('.shirt_part[rel=TopCoatSleeve]').attr("src", basePath + "media/topcoat_images/sleeves/" + $scope.fabricId + "_coatsleeves_1_view_1.png");
            angular.element("#mainImageHolder").find(".shirt_part[rel=TopCoatSleeveButton]").attr("src", basePath + "media/topcoat_images/sleeves/static/Back_Sleeve_Common_Buttons.png");
            if (styleId == 1) {
                angular.element("#mainImageHolder").find('.shirt_part[rel=TopCoatPocket]').attr("src", basePath + "media/default_tool_img/blank.png");

            } else {
                angular.element("#mainImageHolder").find('.shirt_part[rel=TopCoatPocket]').attr("src", basePath + "media/topcoat_images/pockets/" + $scope.fabricId + "_coatpockets_" + styleId + "_view_1.png");
            }

            angular.element("#mainImageHolder").find('.shirt_part[rel=topcoatbuttons]').attr("src", basePath + "media/topcoat_images/styles/buttons/buttons_" + styleId + ".png");
            angular.element("#mainImageHolder").find('.shirt_part[rel=TopCoatStyleBack]').attr("src", basePath + "media/topcoat_images/styles/" + $scope.fabricId + "_coatstyles_" + styleId + "_view_2.png");
            angular.element("#mainImageHolder").find('.shirt_part[rel=TopCoatCollarBack]').attr("src", basePath + "media/topcoat_images/collars/" + $scope.fabricId + "_coatbackcollar_view_2.png");
            angular.element("#mainImageHolder").find('.shirt_part[rel=TopCoatSleeveBack]').attr("src", basePath + "media/topcoat_images/sleeves/" + $scope.fabricId + "_coatsleeves_1_view_2.png");

            angular.element("#mainImageHolder").find('.shirt_part[rel=TopCoatLining]').attr("src", basePath + "media/topcoat_images/lining/" + $scope.fabricId + "_coatlining_view_3.png");

            angular.element("#mainImageHolder").find('.shirt_part[rel=TopCoatCollarFold]').attr("src", basePath + "media/topcoat_images/collars/" + $scope.fabricId + "_coatbackcollar_view_3.png");

            if (styleId == 3 || styleId == 4) {
                angular.element("#mainImageHolder").find('.shirt_part[rel=TopCoatBreastPocket]').attr("src", basePath + "media/topcoat_images/styles/" + $scope.fabricId + "_coatbreast_" + styleId + "_view_1.png");
            } else {
                angular.element("#mainImageHolder").find('.shirt_part[rel=TopCoatBreastPocket]').attr("src", basePath + "media/default_tool_img/blank.png");
            }

            $scope.IsJacket = false;//hide jacket
            hideJacket();

        } else
        {
            if (parent == "Style")//update Lapel design Style
            {


                var LapelTypeId = angular.element("#lapelSize").prev().prev().find(".activeDesign").attr("id") ? angular.element("#lapelSize").prev().prev().find(".activeDesign").attr("id") : "1";
                var LapelSizeId = angular.element("#lapelSize").find(".activeDesign").attr("id") ? angular.element("#lapelSize").find(".activeDesign").attr("id") : "1";
                ///update style buttons
                var styleTypeId = angular.element("#view1").find('.shirt_part[rel=Style]').attr("id") ? angular.element("#view1").find('.shirt_part[rel=Style]').attr("id") : "1";
                var styleBtnCnt;
                //              console.log($scope.activeStyle)
//                console.log("$scope.activeStyle.type = " + $scope.activeStyle.type)
                var image = new Image();
                if ($scope.activeStyle.type == "1")
                {


                    jQuery('#divs_2').find('ul').find('li[id="2"],li[id="3"]').show()
                    // angular.element(".theView").find('.shirt_part[rel=Tie]').show();
                    styleBtnCnt = $scope.activeStyle.btn_count;//angular.element("#view1").find('.shirt_part[rel=Style]').attr("btnCount") ? angular.element("#view1").find('.shirt_part[rel=Style]').attr("btnCount") : "1";
                    image.src = basePath + "media/jacket_images/buttons/" + $scope.activeStyle.type + "_buttons_" + $scope.activeStyle.btn_count + "_color_4.png";
                    //alert(styleBtnCnt)
                    //console.log("style button cmnt in type 1 = " + styleBtnCnt)

                    if (image.width == 0) {

                        angular.element("#view1").find('.shirt_part[rel=Style]').attr("btnCount", "1");
                        $scope.actStyleJKArr['buttons'] = basePath + "media/jacket_images/buttons/" + $scope.activeStyle.type + "_buttons_1_color_4.png";
                        $scope.actStyleJKArr['threads'] = basePath + "media/jacket_images/threads/" + currentThreadsId + "_threads_" + $scope.activeStyle.type + "_btn_1_view_1.png";
                        $scope.actStyleJKArr['collarstyle'] = basePath + "media/jacket_images/collar/" + $scope.fabricId + "_collarstyle_" + $scope.activeStyle.id + "_view_3.png";
                        $scope.actStyleJKArr['vestcollar'] = basePath + "media/vest_images/vestlining/" + $scope.fabricId + "_vestcollar_view_3.png";
                        $scope.actStyleJKArr['lining'] = basePath + "media/jacket_images/lining/" + fabricId + "_liningstyle_" + $scope.activeStyle.id + "_view_3.png";
                    } else {
                        $scope.actStyleJKArr['buttons'] = basePath + "media/jacket_images/buttons/" + $scope.activeStyle.type + "_buttons_" + $scope.activeStyle.btn_count + "_color_4.png";
                        $scope.actStyleJKArr['threads'] = basePath + "media/jacket_images/threads/" + currentThreadsId + "_threads_" + $scope.activeStyle.type + "_btn_" + styleBtnCnt + "_view_1.png";
                        $scope.actStyleJKArr['collarstyle'] = basePath + "media/jacket_images/collar/" + $scope.fabricId + "_collarstyle_" + $scope.activeStyle.id + "_view_3.png";
                        $scope.actStyleJKArr['vestcollar'] = basePath + "media/vest_images/vestlining/" + $scope.fabricId + "_vestcollar_view_3.png";
                    }

                } else if ($scope.activeStyle.type == "2") {
                    //alert(styleBtnCnt)

                    styleBtnCnt = $scope.activeStyle.btn_count;//angular.element("#view1").find('.shirt_part[rel=Style]').attr("btnCount") ? angular.element("#view1").find('.shirt_part[rel=Style]').attr("btnCount") : "2";
                    //console.log("style button cmnt in type 2 = " + styleBtnCnt)
                    //angular.element(".theView").find('.shirt_part[rel=Tie]').show();
                    image.src = basePath + "media/jacket_images/buttons/" + $scope.activeStyle.type + "_buttons_" + $scope.activeStyle.btn_count + "_color_4.png";
                    //angular.element("#view1").find('.shirt_part[rel=Shirt]').attr("src", basePath + "media/default_tool_img/Woman/Shirt_Front_Formal.png");
                    if (image.width == 0) {

                        angular.element("#view1").find('.shirt_part[rel=Style]').attr("btnCount", "2");
                        $scope.actStyleJKArr['buttons'] = basePath + "media/jacket_images/buttons/2_buttons_2_color_4.png";
                        $scope.actStyleJKArr['threads'] = basePath + "media/jacket_images/threads/" + currentThreadsId + "_threads_" + $scope.activeStyle.type + "_btn_2_view_1.png";
                        $scope.actStyleJKArr['collarstyle'] = basePath + "media/jacket_images/collar/" + $scope.fabricId + "_collarstyle_" + $scope.activeStyle.id + "_view_3.png";
                        $scope.actStyleJKArr['vestcollar'] = basePath + "media/vest_images/vestlining/" + $scope.fabricId + "_vestcollar_view_3.png";
                        $scope.actStyleJKArr['lining'] = basePath + "media/jacket_images/lining/" + fabricId + "_liningstyle_" + $scope.activeStyle.id + "_view_3.png";
                    } else {

                        $scope.actStyleJKArr['buttons'] = basePath + "media/jacket_images/buttons/2_buttons_" + styleBtnCnt + "_color_4.png";
                        $scope.actStyleJKArr['threads'] = basePath + "media/jacket_images/threads/" + currentThreadsId + "_threads_" + $scope.activeStyle.type + "_btn_" + styleBtnCnt + "_view_1.png";
                    }


                } else if ($scope.activeStyle.type == "3") {//Mandarin

                    styleBtnCnt = "5";
//                    alert(styleBtnCnt)
                    //console.log("style button cmnt in type3 = " + styleBtnCnt)

                    angular.element("#view1").find('.shirt_part[rel=Style]').attr("btnCount", "5");
                    $scope.actStyleJKArr['buttons'] = basePath + "media/jacket_images/buttons/3_buttons_" + styleBtnCnt + "_color_4.png";
                    $scope.actStyleJKArr['threads'] = basePath + "media/jacket_images/threads/" + currentThreadsId + "_threads_" + $scope.activeStyle.type + "_btn_" + styleBtnCnt + "_view_1.png";
                    $scope.actStyleJKArr['collarstyle'] = basePath + "media/jacket_images/collar/" + $scope.fabricId + "_collarstyle_" + $scope.activeStyle.id + "_view_3.png";
                    $scope.actStyleJKArr['vestcollar'] = basePath + "media/vest_images/vestlining/" + $scope.fabricId + "_vestcollar_view_3.png";
                    $scope.actStyleJKArr['lining'] = basePath + "media/jacket_images/lining/" + fabricId + "_liningstyle_" + $scope.activeStyle.id + "_view_3.png";
                } else {// casual
                    styleBtnCnt = "2";
//                    alert(styleBtnCnt)

                    angular.element("#view1").find('.shirt_part[rel=Style]').attr("btnCount", "2");
                    angular.element("#view1").find('.shirt_part[rel=buttons]').attr("src", basePath + "media/jacket_images/buttons/casual/1_buttons_2_color_4.png");
                    $scope.actStyleJKArr['buttons'] = basePath + "media/jacket_images/buttons/casual/1_buttons_2_color_4.png";
                    $scope.actStyleJKArr['threads'] = basePath + "media/jacket_images/threads/" + currentThreadsId + "_threads_" + $scope.activeStyle.type + "_btn_" + styleBtnCnt + "_view_1.png";
                    $scope.actStyleJKArr['collarstyle'] = basePath + "media/jacket_images/collar/" + $scope.fabricId + "_collarstyle_" + $scope.activeStyle.id + "_view_3.png";
                    $scope.actStyleJKArr['vestcollar'] = basePath + "media/vest_images/vestlining/" + $scope.fabricId + "_vestcollar_view_3.png";
                    $scope.actStyleJKArr['lining'] = basePath + "media/jacket_images/lining/" + fabricId + "_liningstyle_" + $scope.activeStyle.id + "_view_3.png";

                }
                //pockets
                var PocketstyleTypeId = jQuery("#divs_4 ul ").find(".activeDesign").attr("id") ? jQuery("#divs_4 ul ").find(".activeDesign").attr("id") : "1";
                if ($scope.activeStyle.type == "4") {
                    $scope.actStyleJKArr['Pockets'] = styleImg = basePath + "media/jacket_images/pockets/" + $scope.fabricId + "_casual_pockets_" + PocketstyleTypeId + "_view_1.png";
                } else {
                    $scope.actStyleJKArr['Pockets'] = styleImg = basePath + "media/jacket_images/pockets/" + $scope.fabricId + "_pockets_" + PocketstyleTypeId + "_view_1.png";
                }
                ///update style buttons

//                alert(styleTypeId)
                $scope.styleBtnCnt = styleBtnCnt;
                $scope.styleBtnType = $scope.activeStyle.type;

                //console.log("button cnt == " + $scope.styleBtnCnt + "button type == " + $scope.styleBtnType)


                if ($scope.activeView == 3 || $scope.activeView == 1)
                {
                    //$scope.activeView = 1;
                    if ($scope.activeStyle.type == "3") {
                        $scope.actStyleJKArr['Lapel'] = basePath + "media/default_shirttool_image/blank.png";
                    } else {
                        $scope.actStyleJKArr['Lapel'] = basePath + "media/jacket_images/lapel/" + $scope.fabricId + "_lapelstyle_" + styleId + "_size_" + LapelSizeId + "_lapeltype_" + LapelTypeId + "_view_1.png";
                    }

                } else {


                    $scope.actStyleJKArr['Lapel'] = basePath + "media/jacket_images/lapel/" + $scope.fabricId + "_lapelstyle_" + styleId + "_size_" + LapelSizeId + "_lapeltype_" + LapelTypeId + "_view_1.png";
                }




                //back image
                if (styleName != "Mandarin")
                {

                    jQuery("#mainImageHolder").find("[rel=Lapel]").show();
                    jQuery("#div1").find("[target=3]").parent().show();
                    $scope.actStyleJKArr['BackStyle'] = basePath + "media/jacket_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_" + parent.toLowerCase() + "_1_view_2.png";
                } else {

                    setTimeout(function () {
                        jQuery("#div1").find("[target=3]").parent().hide();
                        jQuery("#mainImageHolder").find("[rel=Lapel]").hide();
                    }, 100);
                }

                dispStyleBtn(styleId, parent);
            }


//            if ($scope.activeView == 3)
//            {

            styleImg = basePath + "media/jacket_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_" + parent.toLowerCase() + "_" + styleId + "_view_1.png";
//            } else {
//
//                styleImg = basePath + "media/jacket_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_" + parent.toLowerCase() + "_" + styleId + "_view_" + $scope.activeView + ".png";
//            }



        }




        if (styleType == "jacket")
        {
//            console.log("inside jacket update of design click")
            if (parent == "Lapel")
            {
                var LapelActSize = angular.element("#lapelSize").find(".activeDesign .ng-binding:first").text().trim() ? angular.element("#lapelSize").find(".activeDesign .ng-binding:first").text().trim() : angular.element("#lapelSize").find("li:first .ng-binding:first").text().trim();
                styleName = angular.element("#lapelSize").parent().find("ul:first").find(".activeDesign .ng-binding:first").text().trim();
                $scope.actStyleJKArr[parent + "_styleName"] = styleName + "_" + LapelActSize;
            } else {
                $scope.actStyleJKArr[parent + '_styleName'] = styleName;
            }

            $scope.actStyleJKArr[parent] = styleImg;
            $scope.actStyleJKArr[parent + "_index"] = ind;
            $scope.actStyleJKArr[parent + '_id'] = styleId;
            $scope.actStyleJKArr[parent + '_btnCount'] = $scope.activeStyle.btn_count;

            $scope.actStyleJKArr[parent + '_icon'] = icon;
            //console.log($scope.actStyleJKArr[parent + '_icon'])

        } else if (styleType == "pant") {

            $scope.actStylePantArr[parent] = styleImg;
            $scope.actStylePantArr[parent + "_index"] = ind;
            $scope.actStylePantArr[parent + '_styleName'] = styleName;
            $scope.actStylePantArr[parent + '_id'] = styleId;
            $scope.actStylePantArr[parent + '_icon'] = icon;
        } else if (styleType == "vest") {

            $scope.actStyleVestArr[parent] = styleImg;
            $scope.actStyleVestArr[parent + "_index"] = ind;
            $scope.actStyleVestArr[parent + '_styleName'] = styleName;
            $scope.actStyleVestArr[parent + '_id'] = styleId;
            $scope.actStyleVestArr[parent + '_btnCount'] = $scope.activeStyle.btn_count;
            $scope.actStyleVestArr[parent + '_icon'] = icon;
        } else if (styleType == "topcoat") {
            console.log("$scope.actStyleJKArr[parent] == " + styleImg)
            console.log("inside topcode update of designclick ")
            $scope.actStyleVestArr[parent] = styleImg;
            $scope.actStyleVestArr[parent + "_index"] = ind;
            $scope.actStyleVestArr[parent + '_styleName'] = styleName;
            $scope.actStyleVestArr[parent + '_id'] = styleId;
            $scope.actStyleVestArr[parent + '_btnCount'] = $scope.activeStyle.btn_count;
            $scope.actStyleVestArr[parent + '_icon'] = icon;
        }




        updatePrice();
        updateProduct();
        hideJacket();
        imageLoaded();
          if ($scope.IsPant) {

            hidePant();
        }
    }

    //change Accent of Suit 
    $scope.accentClick = function ($event, obj, ind) {

        angular.element(".suit-preloader").show();
        angular.element(".mainToolarea").css({"opacity": "0"});
        var parent, styleImg, styleType, styleName, styleId;

        angular.element($event.currentTarget).parent().find(".activeDesign").removeClass("activeDesign");
        angular.element($event.currentTarget).addClass("activeDesign");

        if (jQuery(window).width() <= 767) {
            jQuery("body").removeClass("LeftpanemOpen")
            jQuery(".leftpanelsection").css("left", "0");
        }

        $scope.activeStyle = obj;
        currStyleName = parent = $scope.activeStyle.parent.replace(/\s+|&/g, "");
        styleName = $scope.activeStyle.name.replace(/\s+|&/g, "");
        styleId = $scope.activeStyle.id;

        var liningCurrentId;

        console.log("parent == " + parent + " style Id=" + styleId + " ==>ACCENT")

        if (parent == "lining")
        {
            var liningCurrentId = angular.element($event.currentTarget).attr("id");
            linid_active_id = liningCurrentId;
        }


        if ($scope.activeStyle.price) {
            stylePriceArr[parent] = $scope.activeStyle.price;
        } else {
            stylePriceArr[parent] = "0.0";
        }




        //get style type single or double
        styleType = $scope.activeStyle.designType; //angular.element("#view" + $scope.activeView).find('.shirt_part[rel=' + parent + ']').attr("type");

        var styleTypeId = angular.element("#view1").find('.shirt_part[rel=Style]').attr("id") ? angular.element("#view1").find('.shirt_part[rel=Style]').attr("id") : "1";

        if (parent == "brassbuttons")
        {
            var styleBtnCount = angular.element("#view1").find('.shirt_part[rel=Style]').attr("btnCount") ? angular.element("#view1").find('.shirt_part[rel=Style]').attr("btnCount") : "1";
            //var styleBtnCountVest = angular.element("#view1").find('.shirt_part[rel=veststyle]').attr("btnCount") ? angular.element("#view1").find('.shirt_part[rel=veststyle]').attr("btnCount") : "1";
            styleImg = basePath + "media/jacket_images/" + parent.toLowerCase() + "/" + styleTypeId + "_" + parent.toLowerCase() + "_" + styleBtnCount + "_color_" + styleId + ".png";
            parent = "buttons";
        } else if (parent == "threads")
        {
            var styleBtnCount = angular.element("#view1").find('.shirt_part[rel=Style]').attr("btnCount") ? angular.element("#view1").find('.shirt_part[rel=Style]').attr("btnCount") : "1";
            styleImg = basePath + "media/jacket_images/" + parent.toLowerCase() + "/" + styleId + "_" + parent.toLowerCase() + "_" + styleTypeId + "_btn_" + styleBtnCount + "_view_1.png";
            angular.element("#view2").find('.shirt_part[rel=sleeve_buttonsThread]').attr('src', basePath + "media/jacket_images/threads/" + styleId + "_btn_" + $scope.sleeveBtnCnt + "_view_2.png");
            currentThreadsId = styleId;
            //$scope.actStyleJKArr['threads'] = basePath + "media/jacket_images/threads/" + currentThreadsId + "_threads_" + styleTypeId + "_btn_" + $scope.activeStyle.btn_count + "_view_1.png";

        } else if (parent == "pocketsquare")
        {

            styleImg = basePath + "media/jacket_images/" + parent.toLowerCase() + "/" + styleId + "_suitpocketsquare_2_view_1.png";
            styleImg = basePath + "media/jacket_images/" + parent.toLowerCase() + "/" + styleId + "_suitpocketsquare_1_view_1.png";

        } else if (parent == "lining")
        {


            /* if (styleTypeId == "1") {
             
             styleImg = basePath + "media/jacket_images/lining/" + liningCurrentId + "_liningstyle_1_view_3.png";
             
             } else if (styleTypeId == "4") {
             
             
             styleImg = basePath + "media/jacket_images/lining/" + liningCurrentId + "_liningstyle_4_view_3.png";
             }*/

            styleImg = basePath + "media/jacket_images/lining/" + linid_active_id + "_liningstyle_" + styleTypeId + "_view_3.png";
            console.log("lininf image == " + styleImg)
            $scope.actStyleJKArr['vestlining'] = basePath + "media/vest_images/vestlining/" + liningCurrentId + "_vestlining_view_3.png";
            $scope.actStyleJKArr['jacketlining'] = basePath + "media/vest_images/vestlining/" + liningCurrentId + "_vestlining_view_3.png";
        } else if (parent == "backcollaraccent") {
            styleId = angular.element('#divaccent_5 ul:first').find('.activeDesign').attr('id') ? angular.element('#divaccent_5 ul:first').find('.activeDesign').attr('id') : "1";

            if (styleId == 2)
            {
                angular.element('#divaccent_5 ul.BackCollar').css("display", "block");
                styleImg = $scope.actStyleJKArr['backcollaraccent'] = basePath + "media/jacket_images/suitnecklining/" + $scope.backcollfabId + "_suitnecklining_view_2.png";
            } else
            {
                angular.element('#divaccent_5 ul.BackCollar').css("display", "none");
                styleImg = $scope.actStyleJKArr['backcollaraccent'] = basePath + "media/default_tool_img/blank.png";
            }
        } else if (parent == "elbowpatches") {

            styleId = angular.element('#divaccent_6 ul:first').find('.activeDesign').attr('id') ? angular.element('#divaccent_6 ul:first').find('.activeDesign').attr('id') : "1";
            if (styleId == 2)
            {
                angular.element('#divaccent_6 ul.ElbowPatch').css("display", "block");//2_elbowpatch_1_view_2
                styleImg = $scope.actStyleJKArr['backelbow'] = basePath + "media/jacket_images/elbow_patches/" + $scope.backelbofabId + "_elbowpatch_1_view_2.png";
            } else
            {
                angular.element('#divaccent_6 ul.ElbowPatch').css("display", "none");
                styleImg = $scope.actStyleJKArr['backelbow'] = basePath + "media/default_tool_img/blank.png";
            }
        }







        //check style
        if (styleTypeId == "3")//Mandarin
        {

            angular.element(".theView").find('.shirt_part[rel=Lapel]').hide();
        } else {



            angular.element(".theView").find('.shirt_part[rel=Lapel]').show();
        }


        if (parent == "elbowpatches" || parent == "elbowpatches")
        {

        } else {
            if (styleType == "jacket")
            {

                $scope.actStyleJKArr[parent] = styleImg;
                $scope.actStyleJKArr[parent + "_index"] = ind;
                $scope.actStyleJKArr[parent + '_styleName'] = styleName;
                $scope.actStyleJKArr[parent + '_id'] = styleId;


            } else if (styleType == "pant") {

                $scope.actStylePantArr[parent] = styleImg;
                $scope.actStylePantArr[parent + "_index"] = ind;
                $scope.actStylePantArr[parent + '_styleName'] = styleName;
                $scope.actStylePantArr[parent + '_id'] = styleId;

            } else if (styleType == "vest") {

                $scope.actStyleVestArr[parent] = styleImg;
                $scope.actStyleVestArr[parent + "_index"] = ind;
                $scope.actStyleVestArr[parent + '_styleName'] = styleName;
                $scope.actStyleVestArr[parent + '_id'] = styleId;
            }
        }

        updatePrice();
        updateProduct();
        hideJacket();
        imageLoaded();
          if ($scope.IsPant) {

            hidePant();
        }
    }

    /*code for back collar*/
    $scope.changebackcolfab = function ($event, fabric) {

        var styleImg, styleId;




        currStyleName = "fabric";

        angular.element(".suit-preloader").show();
        angular.element(".mainToolarea").css({"opacity": "0"});

        angular.element($event.currentTarget).parent().find(".activeDesign").removeClass('activeDesign');
        angular.element($event.currentTarget).find(".fabvie").addClass("activeDesign");
        var parent;

        if ($scope.actCatName == "BackCollar") {
            parent = "backcollaraccent";

            $scope.backcollfabId = angular.element($event.currentTarget).attr("id");


            styleId = angular.element('#divaccent_5 ul > li.activeDesign').attr('id') ? angular.element('#divaccent_5 ul > li.activeDesign').attr('id') : "1";

            if (styleId == 2)
            {

                styleImg = $scope.actStyleJKArr['backcollaraccent'] = basePath + "media/jacket_images/suitnecklining/" + $scope.backcollfabId + "_suitnecklining_view_2.png";
            } else
            {

                styleImg = $scope.actStyleJKArr['backcollaraccent'] = basePath + "media/default_tool_img/blank.png";
            }
        } else if ($scope.actCatName == "ElbowPatch") {

            parent = "backelbow";

            $scope.backelbofabId = angular.element($event.currentTarget).attr("id");
            styleId = angular.element('#divaccent_6 ul > li.activeDesign').attr('id') ? angular.element('#divaccent_6 ul > li.activeDesign').attr('id') : "1";


            if (styleId == 2)
            {

                styleImg = $scope.actStyleJKArr['backelbow'] = basePath + "media/jacket_images/elbow_patches/" + $scope.backelbofabId + "_elbowpatch_1_view_2.png";
            } else
            {

                styleImg = $scope.actStyleJKArr['backelbow'] = basePath + "media/default_tool_img/blank.png";
            }
        }



        $scope.actStyleJKArr[parent] = styleImg;
        $scope.actStyleJKArr[parent + '_styleName'] = fabric.name;




        updateProduct();
        updatePrice();

        angular.element(".suit-preloader").hide();
        angular.element(".mainToolarea").css({"opacity": "1"});

    };


    /*code for back collar ends here*/


    function dispStyleBtn(styleId, parent)
    {

        if (parent == "Style") {

            if (jQuery("#linkJacket").parent().hasClass("active")) {

                jQuery("#divs_7 li").each(function () {
                    console.log("dispStyleBtn ---------------" + parent + "-+" + this)
                    if (jQuery(this).attr("style_id") == styleId) {
                        jQuery(this).show();
                    } else {
                        jQuery(this).hide();
                    }
                });
            }
        }
        //console.log("---------------"+parent+"---"+styleId+"+++")
        if (parent == "veststyle") {
            jQuery("#divvest_7 li").each(function () {
                //console.log("dispStyleBtn ---------------"+parent+"---"+styleId+"+++"+this)
                if (jQuery(this).attr("style_id") == styleId) {
                    jQuery(this).show();
                } else {
                    jQuery(this).hide();
                }
            });
        } else {
            //alert("hi")
            if (jQuery("#linkVest").parent().hasClass("active")) {

                jQuery("#divvest_7 li").each(function () {
                    //console.log("dispStyleBtn ---------------"+parent+"---"+styleId+"+++"+this)
                    if (jQuery(this).attr("style_id") == styleId) {
                        jQuery(this).show();
                    } else {
                        jQuery(this).hide();
                    }
                });
            }
        }
    }

    function updateProduct()
    {

        //check this use view id dynamic
        jQuery("#view1 img").each(function () {

            var parent = jQuery(this).attr('rel');
            var type = jQuery(this).attr('type');

            if (type == "jacket")
            {
                if ($scope.actStyleJKArr[parent])
                {

                    jQuery(this).attr('src', $scope.actStyleJKArr[parent]);
                    jQuery(this).attr("index", $scope.actStyleJKArr[parent + "_index"]);
                    jQuery(this).attr('name', $scope.actStyleJKArr[parent + '_styleName']);
                    jQuery(this).attr('id', $scope.actStyleJKArr[parent + '_id']);
                    jQuery(this).attr('btnCount', $scope.actStyleJKArr[parent + '_btnCount']);
                    jQuery(this).attr('label', $scope.actStyleJKArr[parent + '_label']);
                    jQuery(this).attr('icon', $scope.actStyleJKArr[parent + '_icon']);

                }



            } else if (type == "pant") {
                if ($scope.actStylePantArr[parent])
                {
                    jQuery(this).attr('src', $scope.actStylePantArr[parent]);
                    jQuery(this).attr("index", $scope.actStylePantArr[parent + "_index"]);
                    jQuery(this).attr('name', $scope.actStylePantArr[parent + '_styleName']);
                    jQuery(this).attr('id', $scope.actStylePantArr[parent + '_id']);
                    jQuery(this).attr('label', $scope.actStylePantArr[parent + '_label']);
                    jQuery(this).attr('icon', $scope.actStylePantArr[parent + '_icon']);
                }

            } else if (type == "vest") {

                if ($scope.actStyleVestArr[parent])
                {
                    jQuery(this).attr('src', $scope.actStyleVestArr[parent]);
                    jQuery(this).attr("index", $scope.actStyleVestArr[parent + "_index"]);
                    jQuery(this).attr('name', $scope.actStyleVestArr[parent + '_styleName']);
                    jQuery(this).attr('id', $scope.actStyleVestArr[parent + '_id']);
                    jQuery(this).attr('btnCount', $scope.actStyleVestArr[parent + '_btnCount']);
                    jQuery(this).attr('label', $scope.actStyleVestArr[parent + '_label']);
                    jQuery(this).attr('icon', $scope.actStyleVestArr[parent + '_icon']);
                }



            }

        });
        jQuery("#view2 img").each(function () {

            var parent = jQuery(this).attr('rel');
            var type = jQuery(this).attr('type');

            if (type == "jacket")
            {
                if ($scope.actStyleJKArr[parent])
                {

                    jQuery(this).attr('src', $scope.actStyleJKArr[parent]);
                    jQuery(this).attr("index", $scope.actStyleJKArr[parent + "_index"]);
                    jQuery(this).attr('name', $scope.actStyleJKArr[parent + '_styleName']);
                    jQuery(this).attr('id', $scope.actStyleJKArr[parent + '_id']);
                    jQuery(this).attr('label', $scope.actStyleJKArr[parent + '_label']);
                    jQuery(this).attr('icon', $scope.actStyleJKArr[parent + '_icon']);
                }

            } else if (type == "pant") {

                if ($scope.actStylePantArr[parent])
                {
                    jQuery(this).attr('src', $scope.actStylePantArr[parent]);
                    jQuery(this).attr("index", $scope.actStylePantArr[parent + "_index"]);
                    jQuery(this).attr('name', $scope.actStylePantArr[parent + '_styleName']);
                    jQuery(this).attr('id', $scope.actStylePantArr[parent + '_id']);
                    jQuery(this).attr('label', $scope.actStylePantArr[parent + '_label']);
                    jQuery(this).attr('icon', $scope.actStylePantArr[parent + '_icon']);
                }
            } else if (type == "vest") {
                if ($scope.actStyleVestArr[parent])
                {

                    jQuery(this).attr('src', $scope.actStyleVestArr[parent]);
                    jQuery(this).attr("index", $scope.actStyleVestArr[parent + "_index"]);
                    jQuery(this).attr('name', $scope.actStyleVestArr[parent + '_styleName']);
                    jQuery(this).attr('id', $scope.actStyleVestArr[parent + '_id']);
                    jQuery(this).attr('label', $scope.actStyleVestArr[parent + '_label']);
                    jQuery(this).attr('icon', $scope.actStyleVestArr[parent + '_icon']);
                }
            }




        });
        //check this use view id dynamic
        jQuery("#view3 img").each(function () {

            var parent = jQuery(this).attr('rel');
            var type = jQuery(this).attr('type');
            if (type == "jacket")
            {
                if ($scope.actStyleJKArr[parent])
                {

                    jQuery(this).attr('src', $scope.actStyleJKArr[parent]);
                    jQuery(this).attr("index", $scope.actStyleJKArr[parent + "_index"]);
                    jQuery(this).attr('name', $scope.actStyleJKArr[parent + '_styleName']);
                    jQuery(this).attr('id', $scope.actStyleJKArr[parent + '_id']);
                    jQuery(this).attr('btnCount', $scope.actStyleJKArr[parent + '_btnCount']);
                    jQuery(this).attr('label', $scope.actStyleJKArr[parent + '_label']);
                    jQuery(this).attr('icon', $scope.actStyleJKArr[parent + '_icon']);



                }

            } else if (type == "pant") {
                if ($scope.actStylePantArr[parent])
                {
                    jQuery(this).attr('src', $scope.actStylePantArr[parent]);
                    jQuery(this).attr("index", $scope.actStylePantArr[parent + "_index"]);
                    jQuery(this).attr('name', $scope.actStylePantArr[parent + '_styleName']);
                    jQuery(this).attr('id', $scope.actStylePantArr[parent + '_id']);
                    jQuery(this).attr('label', $scope.actStylePantArr[parent + '_label']);
                    jQuery(this).attr('icon', $scope.actStylePantArr[parent + '_icon']);
                }
            } else if (type == "vest") {
                if ($scope.actStyleVestArr[parent])
                {
                    jQuery(this).attr('src', $scope.actStyleVestArr[parent]);
                    jQuery(this).attr("index", $scope.actStyleVestArr[parent + "_index"]);
                    jQuery(this).attr('name', $scope.actStyleVestArr[parent + '_styleName']);
                    jQuery(this).attr('id', $scope.actStyleVestArr[parent + '_id']);
                    jQuery(this).attr('label', $scope.actStyleVestArr[parent + '_label']);
                    jQuery(this).attr('icon', $scope.actStyleVestArr[parent + '_icon']);
                }
            }

        });
    }


//load fabric for suit
    $scope.fabricClick = function ($event, obj) {
        //hide default image
        angular.element("#view1").find('.shirt_part[rel=ViewStatic]').hide();

        currStyleName = "fabric";

        angular.element(".suit-preloader").show();
        angular.element(".mainToolarea").css({"opacity": "0"});

        angular.element("#div4").find(".activeDesign").removeClass('activeDesign');
        angular.element($event.currentTarget).find(".fabvie").addClass("activeDesign");



        fabricId = $scope.fabricId = obj.id;
        $scope.fabricName = obj.name;
        $scope.fabricBtnId = obj.button_color ? obj.button_color : "4";
        $scope.tieColorId = obj.tie_color ? obj.tie_color : "1";
        $scope.shoesColorId = obj.shoes_color ? obj.shoes_color : "1";




        //update fabric preview image
        var fabricVal = angular.element($event.currentTarget).find("img").attr("data-zoom-image");
        angular.element('#slide').attr('src', fabricVal);

        angular.element('.fabricName').html(obj.name);
        angular.element('.fabricType').html(obj.type);
        angular.element('.fabricPrice').html(obj.price);

        //for responsive
        if (jQuery(window).width() <= 767) {
            jQuery("body").removeClass("LeftpanemOpen")
            jQuery(".leftpanelsection").css("left", "0");
        }



        applyFabricOnSuit();
        updatePrice();


    }

    function updateTieButtonShoes()
    {

        angular.element("#view1").find('.shirt_part[rel=Tie]').attr('src', basePath + "media/jacket_images/tie_images/tie_color_" + $scope.tieColorId + ".png");
        var styleTypeId = angular.element("#view1").find('.shirt_part[rel=Style]').attr('id') ? angular.element("#view1").find('.shirt_part[rel=Style]').attr('id') : "1";

//        angular.element("#view1").find('.shirt_part[rel=ManShoes]').attr('src', basePath + "media/jacket_images/shoes_images/shoes_color_" + $scope.shoesColorId + "_view_1.png");
//        angular.element("#view2").find('.shirt_part[rel=ManShoes]').attr('src', basePath + "media/jacket_images/shoes_images/shoes_color_" + $scope.shoesColorId + "_view_2.png");
//
//        angular.element("#view1").find('.shirt_part[rel=ManShoes]').attr("name", $scope.shoesColorId);
        angular.element("#view1").find('.shirt_part[rel=ButtonsColor]').attr("name", $scope.fabricBtnId);
        angular.element("#view1").find('.shirt_part[rel=Tie]').attr("name", $scope.tieColorId);


        //style button

        if (styleTypeId == "4") {
            // alert(styleTypeId)
            angular.element("#view1").find('.shirt_part[rel=Style]').attr('btnCount', "2")
            angular.element("#view1").find('.shirt_part[rel=buttons]').attr("src", basePath + "media/jacket_images/buttons/casual/1_buttons_2_color_4.png");
            angular.element("#view3").find('.shirt_part[rel=buttons]').attr("src", basePath + "media/jacket_images/buttons/casual/1_buttons_2_color_4.png");
            $scope.actStyleJKArr['buttons'] = basePath + "media/jacket_images/buttons/casual/1_buttons_2_color_4.png";
        } else {
            if (styleTypeId == "2" || styleTypeId == "1") {
                //alert("sad")
                // angular.element("#view1").find('.shirt_part[rel=Style]').attr('btnCount', angular.element("#view1").find('.shirt_part[rel=Style]').attr('btnCount'))
                $scope.styleBtnCnt = angular.element("#view1").find('.shirt_part[rel=Style]').attr('btnCount') ? angular.element("#view1").find('.shirt_part[rel=Style]').attr('btnCount') : "1";
            }
            angular.element("#view1").find('.shirt_part[rel=buttons]').attr('src', basePath + "media/jacket_images/buttons/" + $scope.styleBtnType + "_buttons_" + $scope.styleBtnCnt + "_color_" + $scope.fabricBtnId + ".png");
            console.log("update buttons shoes == " + basePath + "media/jacket_images/buttons/" + $scope.styleBtnType + "_buttons_" + $scope.styleBtnCnt + "_color_" + $scope.fabricBtnId + ".png")
        }


        angular.element("#view2").find('.shirt_part[rel=sleeve_buttons]').attr('src', basePath + "media/jacket_images/sleeve_buttons/" + $scope.sleeveBtnCnt + "_color_" + $scope.fabricBtnId + ".png");

        /*jQuery().attr('src', $scope.actStyleJKArr[parent]);
         jQuery(this).attr("index", $scope.actStyleJKArr[parent + "_index"]);
         jQuery(this).attr('name', $scope.actStyleJKArr[parent + '_styleName']);
         jQuery(this).attr('id', $scope.actStyleJKArr[parent + '_id']);
         jQuery(this).attr('label', $scope.actStyleJKArr[parent + '_label']);*/
    }







    $scope.changeView = function ($event, viewId) {

        angular.element(".suit-preloader").show();
        angular.element(".mainToolarea").css({"opacity": "0"});
        /*$scope.activeView = viewId;
         //remove active and add active to current obj
         angular.element(".view-thumb button").removeClass("active");
         angular.element($event.currentTarget).addClass("active");*/

        $scope.activeView = viewId;
        active_view_id = $scope.activeView;
        changeView();
    }

    $scope.changePart = function ($event) {
        console.log($scope.partOption)
    }




    /*
     * load data static way
     $scope.fabricData = fabricService.getFabricData();
     */

    /*
     * load data dynamic way
     $scope.fabricData = fabricService.getFabricData();
     */
    //get fabric data
    var handleSuccessFabric = function (data, status) {

        if (data)
        {
            //for clone data
            var strData = JSON.stringify(data);
            $scope.fabricData = JSON.parse(strData);

            tempPj = JSON.parse(strData);

            //fabric filter start
            $scope.fabrics = $scope.fabricData.fabric;
            $scope.categories = $scope.fabricData.category;

            //Material
            $scope.materialGroup = uniqueItems($scope.fabrics, 'material_parent');
            //Pattern
            $scope.patternGroup = uniqueItems($scope.fabrics, 'pattern_parent');
            //seasons
            $scope.seasonGroup = uniqueItems($scope.fabrics, 'season_parent');
            //colors
            $scope.colorGroup = uniqueItems($scope.fabrics, 'color_parent');
            //collection
            $scope.categoryGroup = uniqueItems($scope.fabrics, 'category_parent');

            //fabric filter end


            $scope.backColElboFabricData = tempPj.fabric;

            //call designData
            designService.getDesignData().success(handleSuccessDesign).error(function (data, status) {
                console.log(status);
            });

        } else {
            console.log(data.error);
        }
    }

//    console.log("in dddd")
    fabricService.getFabricData().success(handleSuccessFabric).error(function (data, status) {
        console.log(status);
    });


    //get design data
    var handleSuccessDesign = function (data, status) {

        if (data)
        {
            $scope.designData = data;
            tempJk = $scope.designData;

            //call vest data
            vestService.getVestData().success(handleSuccessVest).error(function (data, status) {
                console.log(status);
            });
        } else {
            console.log(data.error);
        }
    }


    //load suit vest data
    var handleSuccessVest = function (data, status) {

        if (data)
        {
            $scope.vestData = data;
            tempVst = data;

            //call pant data
            // $scope.pantData = pantService.getPantsData();
            pantService.getPantsData().success(handleSuccessPant).error(function (data, status) {
                console.log(status);
            });

        } else {
            console.log(data.error);
        }
    }


    //load suit vest data
    var handleSuccessPant = function (data, status) {

        if (data)
        {
            $scope.pantData = data;
            tempPt = data;

            //call accent data
            suitAccentService.getSuitAccentData().success(handleSuccessSuitAccent).error(function (data, status) {
                console.log(status);
            });

        } else {
            console.log(data.error);
        }
    }

//    load top coat data
    var handleSuccessTopcoat = function (data, status) {

        if (data)
        {
            $scope.topCoat = data;
            temptopCoat = data;

            //call pant data
            // $scope.pantData = pantService.getPantsData();



        } else {
            console.log(data.error);
        }
    }



//    $scope.topCoat = topCoatService.gettopCoatData()

    //load suit vest data
    var handleSuccessSuitAccent = function (data, status) {

        if (data)
        {
            $scope.suitAccentData = data;
            tempAccnt = data;



            applyFabricOnSuitInit();
            updatePrice();
            topCoatService.gettopCoatData().success(handleSuccessTopcoat).error(function (data, status) {
                console.log(status);
            });

        } else {
            console.log(data.error);
        }
    }


    function applyFabricOnSuit()
    {


        var first = tempJk;
        var second = tempPt;
        var third = tempVst;
        var forth = tempAccnt;
        var fifth = temptopCoat;
        var data = jQuery.merge(jQuery.merge([], first), second);
        var data1 = jQuery.merge(jQuery.merge([], data), third);
        var data2 = jQuery.merge(jQuery.merge([], data1), forth);
        var suitDesignData = jQuery.merge(jQuery.merge([], data2), fifth);
        var parent, styleImg, styleType, styleName, styleId, labelName;

        $scope.allDesignStyles = "";

        var styleCnt = "0";
        for (var design in suitDesignData)
        {
            if (suitDesignData.hasOwnProperty(design))
            {
                $scope.allDesignStyles = suitDesignData[design]["style"][0];
                if ($scope.allDesignStyles)
                {
                    var designType = $scope.allDesignStyles.designType;
                    if ($scope.partOption == designType || $scope.partOption == 'all')
                    {

                        if ($scope.partOption == "jacket")
                        {
                            jacketFabricId = $scope.fabricId;
                            jacketFabricName = $scope.fabricName;
                        } else if ($scope.partOption == "pant")
                        {
                            pantFabricId = $scope.fabricId;
                            pantFabricName = $scope.fabricName;
                        } else if ($scope.partOption == "vest")
                        {
                            vestFabricId = $scope.fabricId;
                            vestFabricName = $scope.fabricName;

                        } else {
                            vestFabricId = $scope.fabricId;
                            pantFabricId = $scope.fabricId;
                            jacketFabricId = $scope.fabricId;

                            jacketFabricName = $scope.fabricName;
                            pantFabricName = $scope.fabricName;
                            vestFabricName = $scope.fabricName;
                        }


                        if (!$scope.allDesignStyles.parent)
                        {
                            continue;
                        }


                        parent = $scope.allDesignStyles.parent.replace(/\s+|&/g, "");
                        styleName = $scope.allDesignStyles.name.replace(/\s+|&/g, "");
                        labelName = $scope.allDesignStyles.parent;
                        styleId = $scope.allDesignStyles.id;

                        //check style
                        if (styleName == "Mandarin" || styleId == "7")
                        {
                            angular.element(".theView").find('.shirt_part[rel=Lapel]').hide();
                        } else {
                            angular.element(".theView").find('.shirt_part[rel=Lapel]').show();
                        }


                        //get style type single or double
                        styleType = $scope.allDesignStyles.designType; //angular.element("#view" + $scope.activeView).find('.shirt_part[rel=' + parent + ']').attr("type");
                        console.log("style type of fab click -- " + styleType)

                        if ($scope.actStyleData[parent]) {
                            styleId = $scope.actStyleData[parent];
                        }

                        angular.element("#view1").find('.shirt_part[rel=Sleeves]').attr("src", basePath + "media/jacket_images/sleeves/" + fabricId + "_suitsleeves_1_view_1.png");
                        $scope.actStyleJKArr['Sleeves'] = basePath + "media/jacket_images/sleeves/" + fabricId + "_suitsleeves_1_view_1.png";

                        angular.element("#view2").find('.shirt_part[rel=Sleevesback]').attr("src", basePath + "media/jacket_images/sleeves/" + fabricId + "_suitsleeves_1_view_2.png");
                        $scope.actStyleJKArr['Sleevesback'] = basePath + "media/jacket_images/sleeves/" + fabricId + "_suitsleeves_1_view_2.png";
                        angular.element("#view3").find('.shirt_part[rel=Sleevesfold]').attr("src", basePath + "media/jacket_images/sleeves/" + fabricId + "_suitsleeves_1_view_3.png");
                        $scope.actStyleJKArr['Sleevesfold'] = basePath + "media/jacket_images/sleeves/" + fabricId + "_suitsleeves_1_view_3.png";

                        angular.element("#view2").find('.shirt_part[rel=backcollar]').attr("src", basePath + "media/jacket_images/back_collar/" + fabricId + "_backCollar_view_2.png");
                        $scope.actStyleJKArr['backcollar'] = basePath + "media/jacket_images/back_collar/" + fabricId + "_backCollar_view_2.png";

                        angular.element("#view1").find('.shirt_part[rel=jacketbreastpocket]').attr("src", basePath + "media/jacket_images/pockets/" + fabricId + "_breastpocket_view_1.png");
                        angular.element("#view3").find('.shirt_part[rel=jacketbreastpocket]').attr("src", basePath + "media/jacket_images/pockets/" + fabricId + "_breastpocket_view_1.png");
                        $scope.actStyleJKArr['jacketbreastpocket'] = basePath + "media/jacket_images/pockets/" + fabricId + "_breastpocket_view_1.png";//breat pocket for jacket


                        if (parent == "Fit")
                        {
                            styleImg = basePath + "media/jacket_images/" + parent.toLowerCase() + "/" + parent.toLowerCase() + "/" + parent.toLowerCase() + "_" + styleId + ".png";
                            $scope.actStyleJKArr['FitBack'] = basePath + "media/jacket_images/" + parent.toLowerCase() + "/" + parent.toLowerCase() + "_" + styleId + ".png";//

                        } else if (parent == "Pockets")
                        {
                            $scope.actStyleJKArr['Pockets'] = styleImg = basePath + "media/jacket_images/pockets/" + $scope.fabricId + "_pockets_" + styleId + "_view_1.png";
//            $scope.actStyleJKArr['Pockets'] = basePath + "media/jacket_images/" + parent.toLowerCase() + "/" + parent.toLowerCase() + "_" + styleId + ".png";//

                        } else if (parent == "Lapel")
                        {
                            var styleTypeId = ($scope.actStyleData["Style"]) ? $scope.actStyleData["Style"] : "1";
                            var LapelTypeId = ($scope.actStyleData["Lapel"]) ? $scope.actStyleData["Lapel"] : "1";
                            styleImg = basePath + "media/jacket_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_" + parent.toLowerCase() + "style_" + styleTypeId + "_size_" + styleId + "_" + parent.toLowerCase() + "type_" + LapelTypeId + "_view_1.png";
                        } else if (parent == "Vents")
                        {
                            $scope.actStylePantArr['Vents'] = styleImg = $scope.allDesignStyles.main_img;
                        } else if (parent == "sleeve_buttons")
                        {

                            styleImg = basePath + "media/jacket_images/" + parent.toLowerCase() + "/" + $scope.sleeveBtnCnt + "_color_4.png";

                        }
//                        else if (parent == "buttons")
//                        {
//                            //console.log($scope.actStyleData)
//                            // console.log("style type id in buttons in fabric section" + $scope.actStyleData["Style"])
//                            var styleTypeId = angular.element("#view1").find('.shirt_part[rel=Style]').attr("id") ? angular.element("#view1").find('.shirt_part[rel=Style]').attr("id") : "1";//($scope.actStyleData["Style"]) ? $scope.actStyleData["Style"] : "1";
//                            if (styleTypeId == "3")
//                            {
//                                styleId = "5";
//                            } else if (styleTypeId == "2" || styleTypeId == "4") {
//                                styleId = "2";
//                            } else {
//                                styleId = "1";
//                            }
//                            //alert("style " + styleId)
//                            if (styleTypeId == "4") {
//                                $scope.actStyleJKArr['buttons'] = styleImg = basePath + "media/jacket_images/" + parent.toLowerCase() + "/casual/1_" + parent.toLowerCase() + "_" + styleId + "_color_4.png";
//                            } else {
//                                $scope.actStyleJKArr['buttons'] = styleImg = basePath + "media/jacket_images/" + parent.toLowerCase() + "/" + styleTypeId + "_" + parent.toLowerCase() + "_" + styleId + "_color_4.png";
//                            }// console.log("style i,mggg in fab for buttons  = " + styleImg)
//                        } 
                        else if (parent == "buttons")
                        {
                            var styleTypeId = angular.element("#view1").find('.shirt_part[rel=Style]').attr("id") ? angular.element("#view1").find('.shirt_part[rel=Style]').attr("id") : "1";
                            var btn_count = angular.element("#view1").find('.shirt_part[rel=buttons]').attr("btncount") ? angular.element("#view1").find('.shirt_part[rel=buttons]').attr("btncount") : "1";
                            if (styleTypeId == "4") {
                                $scope.actStyleJKArr['buttons'] = styleImg = basePath + "media/jacket_images/buttons/casual/1_buttons_2_color_4.png";
                            } else {
                                $scope.actStyleJKArr['buttons'] = styleImg = basePath + "media/jacket_images/" + parent.toLowerCase() + "/" + styleTypeId + "_" + parent.toLowerCase() + "_" + btn_count + "_color_4.png";
                            }


                            //update btn count in style
                            angular.element("#view1").find('.shirt_part[rel=Style]').attr("btnCount", $scope.activeStyle.btn_count);
                            $scope.actStyleJKArr['threads'] = basePath + "media/jacket_images/threads/" + currentThreadsId + "_threads_" + styleTypeId + "_btn_" + $scope.activeStyle.btn_count + "_view_1.png";

                        } else if (parent == "threads")
                        {
                            var styleTypeId = angular.element("#view1").find('.shirt_part[rel=Style]').attr("id") ? angular.element("#view1").find('.shirt_part[rel=Style]').attr("id") : "1";
                            var btn_count = angular.element("#view1").find('.shirt_part[rel=buttons]').attr("btncount") ? angular.element("#view1").find('.shirt_part[rel=buttons]').attr("btncount") : "1";
                            if (styleTypeId == "4") {
                                $scope.actStyleJKArr['threads'] = styleImg = basePath + "media/jacket_images/threads/" + currentThreadsId + "_threads_" + styleTypeId + "_btn_" + $scope.activeStyle.btn_count + "_view_1.png";
                            } else {
                                $scope.actStyleJKArr['threads'] = styleImg = basePath + "media/jacket_images/threads/" + currentThreadsId + "_threads_" + styleTypeId + "_btn_" + btn_count + "_view_1.png";

                            }

                        } else if (parent == "vestbuttons")
                        {
                            var vestStyleId = angular.element("#view1").find('.shirt_part[rel=veststyle]').attr("id") ? angular.element("#view1").find('.shirt_part[rel=veststyle]').attr("id") : "1";
                            var btncnt = angular.element("#view1").find('.shirt_part[rel=veststyle]').attr("btnCount");
                            angular.element("#view1").find('.shirt_part[rel=veststyle]').attr("btnCount", btncnt);
                            styleImg = basePath + "media/vest_images/" + parent.toLowerCase() + "/" + vestStyleId + "_buttons_" + btncnt + "_color_4.png";

                        }/*else if (parent == "vestbuttons")
                         {
                         var btncount = angular.element("#view1").find('.shirt_part[rel=veststyle]').attr("btnCount");
                         styleImg = basePath + "media/vest_images/" + parent.toLowerCase() + "/1_buttons_" + btncount + "_color_4.png";
                         }*/ else if (parent == "pantfit")
                        {

                            console.log("styleid- = " + styleId)
                            styleImg = basePath + "media/pant_images/" + parent.toLowerCase() + "/" + fabricId + "_pantfit_2_view_1.png";
                            $scope.actStylePantArr['pantfit'] = basePath + "media/pant_images/" + parent.toLowerCase() + "/" + fabricId + "_pantfit_2_view_1.png";
                            $scope.actStylePantArr['backpantfit'] = basePath + "media/pant_images/" + parent.toLowerCase() + "/" + fabricId + "_pantfit_2_view_2.png";
                            $scope.actStylePantArr['pantfit_straight'] = basePath + "media/pant_images/" + parent.toLowerCase() + "/" + fabricId + "_pantfit_2_view_3.png";
                            angular.element("#view3").find('.shirt_part[rel=pantfit_straight]').attr("src", basePath + "media/pant_images/" + parent.toLowerCase() + "/" + fabricId + "_pantfit_2_view_3.png")

                            $scope.actStylePantArr['beltloop'] = basePath + "media/pant_images/pantbeltloops/" + $scope.fabricId + "_pantbeltloops_1_view_1.png";
                            //://192.168.2.41/html/p1135/media/pant_images/pantbeltloops/1_pantbeltloops_1_view_1.png
                            $scope.actStylePantArr['backbeltloop'] = basePath + "media/pant_images/pantbeltloops/" + $scope.fabricId + "_pantbeltloops_" + 1 + "_view_2.png";
                            $scope.actStylePantArr['beltfoldloop'] = basePath + "media/pant_images/pantbeltloops/" + $scope.fabricId + "_pantbeltloops_" + 1 + "_view_3.png";

                            if (styleId == "2") {
                                $scope.actStylePantArr['pantBackStyle'] = basePath + "media/pant_images/" + parent.toLowerCase() + "/Slim_static/Slim_mask_back.png";//view2 mask of slim and tailored
                                $scope.actStylePantArr['pantStyle'] = basePath + "media/pant_images/" + parent.toLowerCase() + "/Slim_static/Slim_Mask.png";//view1 mask of slim and tailored
                                $scope.actStylePantArr['pantfit_style'] = basePath + "media/pant_images/" + parent.toLowerCase() + "/Slim_static/Slim_mask_view_3.png";
                                angular.element("#view1").find('.shirt_part[rel=pantStyle]').attr("src", basePath + "media/pant_images/" + parent.toLowerCase() + "/Slim_static/Slim_Mask.png")
                                angular.element("#view2").find('.shirt_part[rel=pantBackStyle]').attr("src", basePath + "media/pant_images/" + parent.toLowerCase() + "/Slim_static/Slim_mask_back.png")
                                angular.element("#view3").find('.shirt_part[rel=pantfit_style]').attr("src", basePath + "media/pant_images/" + parent.toLowerCase() + "/Slim_static/Slim_mask_view_3.png")
                            } else {
                                $scope.actStylePantArr['pantBackStyle'] = basePath + "media/default_tool_img/blank.png";//view2 mask of slim and tailored
                                $scope.actStylePantArr['pantStyle'] = basePath + "media/default_tool_img/blank.png";//view1 mask of slim and tailored
                                $scope.actStylePantArr['pantfit_style'] = basePath + "media/default_tool_img/blank.png";
                                angular.element("#view1").find('.shirt_part[rel=pantStyle]').attr("src", basePath + "media/default_tool_img/blank.png")
                                angular.element("#view2").find('.shirt_part[rel=pantBackStyle]').attr("src", basePath + "media/default_tool_img/blank.png")
                                angular.element("#view3").find('.shirt_part[rel=pantfit_style]').attr("src", basePath + "media/default_tool_img/blank.png")
                            }


                        }/* else if (parent == "pantfit")
                         {
                         
                         $scope.actStylePantArr['pantfit_straight'] = basePath + "media/pant_images/pantfit/" + $scope.fabricId + "_pantfit_straight_1_view_3.png";
                         $scope.actStylePantArr['pantfit_tilt'] = basePath + "media/pant_images/pantfit/" + $scope.fabricId + "_pantfit_tilt_1_view_3.png";
                         if (styleId == "2")
                         {
                         
                         $scope.actStylePantArr['pantStyle'] = basePath + "media/pant_images/" + parent.toLowerCase() + "/pantfit_2_view_1.png";
                         $scope.actStylePantArr['pantBackStyle'] = basePath + "media/pant_images/" + parent.toLowerCase() + "/pantfit_2_view_2.png";
                         } else {
                         
                         styleImg = basePath + "media/pant_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_" + parent.toLowerCase() + "_" + styleId + "_view_1.png";
                         var styleImg1 = basePath + "media/pant_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_" + parent.toLowerCase() + "_" + styleId + "_view_2.png";
                         $scope.actStylePantArr['pantStyle'] = basePath + "media/default_tool_img/blank.png";
                         $scope.actStylePantArr['pantBackStyle'] = basePath + "media/default_tool_img/blank.png";
                         $scope.actStylePantArr['backpantfit'] = styleImg1;
                         }
                         
                         
                         
                         }*/ else if (parent == "pantpleats")
                        {
                            styleImg = basePath + "media/pant_images/" + parent.toLowerCase() + "/1_pantpleats_" + styleId + "_view_1.png";
                            $scope.actStylePantArr['pantpleats'] = basePath + "media/pant_images/" + parent.toLowerCase() + "/1_pantpleats_" + styleId + "_view_1.png";
                            $scope.actStylePantArr['foldPantPleats'] = basePath + "media/pant_images/" + parent.toLowerCase() + "/1_pantpleats_" + styleId + "_view_3.png";
                            angular.element("#view3").find('.shirt_part[rel=foldPantPleats]').attr("src", basePath + "media/pant_images/" + parent.toLowerCase() + "/1_pantpleats_" + styleId + "_view_3.png");

//                            styleImg = basePath + "media/default_tool_img/blank.png";
//                            $scope.actStylePantArr['foldPantPleats'] = styleImg;
//                            $scope.actStylePantArr['pantpleats'] = styleImg;
//                            $scope.actStylePantArr['Pleats'] = styleImg;
                        } else if (parent == "pantcuff")
                        {
                            if (jQuery("#divpant_5").find(".activeDesign").attr("id") == "2") {

                                styleImg = basePath + "media/pant_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_" + parent.toLowerCase() + "_2_view_1.png"; //" + styleId + "
                                angular.element("#view1 .shirt_part[rel='pantcuff']").attr("src", styleImg)
                                $scope.actStylePantArr['pantcuff'] = styleImg;
                                $scope.actStylePantArr['backpantcuff'] = basePath + "media/pant_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_" + parent.toLowerCase() + "_2_view_2.png";
                                $scope.actStylePantArr['pantFoldCuff'] = basePath + "media/pant_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_" + parent.toLowerCase() + "_2_view_3.png";
                            } else
                            {

                                styleImg = basePath + "media/default_tool_img/blank.png";
                                $scope.actStylePantArr['backpantcuff'] = styleImg;
                                $scope.actStylePantArr['pantFoldCuff'] = styleImg;
                                $scope.actStylePantArr['pantcuff'] = styleImg;
                            }



                            //pantFoldCuff

                        } else if (parent == "fastening" || parent == "pantpockets")
                        {


                            if (parent == "fastening") {
                                if (styleId == 1) {
                                    styleImg = basePath + "media/jacket_images/" + parent.toLowerCase() + "/Main-Pant_Fastening_Centered_Glow.png";
                                    $scope.actStylePantArr['fasteningButton'] = basePath + "media/jacket_images/" + parent.toLowerCase() + "/Button_centered.png";
                                } else {
                                    styleImg = basePath + "media/jacket_images/" + parent.toLowerCase() + "/Main-Pant_Fastening_Off-Centered_Glow.png";
                                    $scope.actStylePantArr['fasteningButton'] = basePath + "media/default_tool_img/blank.png"
                                }


                            } else if (parent == "pantpockets") {

                                styleImg = basePath + "media/pant_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_pantpockets_" + styleId + "_view_1.png";
                                $scope.actStyleVestArr['pantpockets'] = styleImg;
                                angular.element("#view3").find('.shirt_part[rel=foldedPantPocket]').attr("src", basePath + "media/pant_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_pantpockets_" + styleId + "_view_3.png")
                                $scope.actStyleVestArr['foldedPantPocket'] = basePath + "media/pant_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_pantpockets_" + styleId + "_view_3.png"
                            }

                        } else if (parent == "veststyle" || parent == "vestedge")
                        {

                            if (parent == "vestedge")
                            {
                                console.log("$scope.activeView == " + $scope.activeView)
                                var vestStyleId = angular.element("#view1").find('.shirt_part[rel=veststyle]').attr("id") ? angular.element("#view1").find('.shirt_part[rel=veststyle]').attr("id") : "1";
                                var vestEdgeId = styleId; //angular.element("#view1").find('.shirt_part[rel=vestedge]').attr("id") ? angular.element("#view1").find('.shirt_part[rel=vestedge]').attr("id") : "1";

                                styleImg = basePath + "media/default_tool_img/blank.png";
                                if ($scope.activeView == 3) {
                                    $scope.actStyleVestArr['veststyle'] = basePath + "media/vest_images/veststyle/" + $scope.fabricId + "_style_" + vestStyleId + "_bottom_" + vestEdgeId + "_view_1.png";
                                } else {
                                    if ($scope.activeView == "1") {
                                        $scope.actStyleVestArr['veststyle'] = basePath + "media/vest_images/veststyle/" + $scope.fabricId + "_style_" + vestStyleId + "_bottom_" + vestEdgeId + "_view_1.png";
                                    } else {
                                        $scope.actStyleVestArr['veststyle'] = basePath + "media/vest_images/veststyle/" + $scope.fabricId + "_style_" + vestStyleId + "_bottom_" + vestEdgeId + "_view_1.png";
                                    }

                                }
                            } else {
                                var vestStyleId = angular.element("#view1").find('.shirt_part[rel=veststyle]').attr("id") ? angular.element("#view1").find('.shirt_part[rel=veststyle]').attr("id") : "1";
                                var vestEdgeId = angular.element("#view1").find('.shirt_part[rel=vestedge]').attr("id") ? angular.element("#view1").find('.shirt_part[rel=vestedge]').attr("id") : "1";

                                $scope.vestStyleBtnType = vestStyleId;

                                if ($scope.activeView == 3) {
                                    styleImg = basePath + "media/vest_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + vestStyleId + "_bottom_" + vestEdgeId + "_view_3.png";
                                    $scope.actStyleVestArr['veststyle'] = basePath + "media/vest_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + vestStyleId + "_bottom_" + vestEdgeId + "_view_3.png";
                                } else {
                                    if ($scope.activeView == "1") {
                                        styleImg = basePath + "media/vest_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + vestStyleId + "_bottom_" + vestEdgeId + "_view_1.png";
                                        $scope.actStyleVestArr['veststyle'] = basePath + "media/vest_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + vestStyleId + "_bottom_" + vestEdgeId + "_view_1.png";
                                    } else {
                                        styleImg = basePath + "media/vest_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + vestStyleId + "_bottom_" + vestEdgeId + "_view_1.png";
                                        $scope.actStyleVestArr['veststyle'] = basePath + "media/vest_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + vestStyleId + "_bottom_" + vestEdgeId + "_view_1.png";
                                    }

                                }


                                $scope.actStyleVestArr['veststyle'] = styleImg;
                                $scope.actStyleVestArr['backveststyle'] = basePath + "media/vest_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + vestStyleId + "_bottom_" + vestEdgeId + "_view_2.png";
                                //update vestbutton image


                                var vestStyleBtCnt;
                                if (vestStyleId == "1")
                                {
                                    vestStyleBtCnt = angular.element("#view1").find('.shirt_part[rel=veststyle]').attr("btnCount") ? angular.element("#view1").find('.shirt_part[rel=veststyle]').attr("btnCount") : "3";
                                    $scope.actStyleVestArr['vestbuttons'] = basePath + "media/vest_images/vestbuttons/" + vestStyleId + "_buttons_" + vestStyleBtCnt + "_color_4.png";
                                } else {

                                    vestStyleBtCnt = angular.element("#view1").find('.shirt_part[rel=veststyle]').attr("btnCount") ? angular.element("#view1").find('.shirt_part[rel=veststyle]').attr("btnCount") : "4";
                                    //alert(vestStyleBtCnt)
                                    angular.element("#view1").find('.shirt_part[rel=vestbuttons]').attr("src", basePath + "media/vest_images/vestbuttons/" + vestStyleId + "_buttons_" + vestStyleBtCnt + "_color_4.png");
                                    angular.element("#view3").find('.shirt_part[rel=vestbuttons]').attr("src", basePath + "media/vest_images/vestbuttons/" + vestStyleId + "_buttons_" + vestStyleBtCnt + "_color_4.png");
                                    $scope.actStyleVestArr['vestbuttons'] = basePath + "media/vest_images/vestbuttons/" + vestStyleId + "_buttons_" + vestStyleBtCnt + "_color_4.png";
                                }



                                //end



                            }

                            dispStyleBtn(styleId, parent);


                        } /*else if (parent == "veststyle" || parent == "vestedge")
                         {
                         
                         if (parent == "vestedge")
                         {
                         var vestStyleId = angular.element("#view1").find('.shirt_part[rel=veststyle]').attr("id") ? angular.element("#view1").find('.shirt_part[rel=veststyle]').attr("id") : "1";
                         var vestEdgeId = styleId; //angular.element("#view1").find('.shirt_part[rel=vestedge]').attr("id") ? angular.element("#view1").find('.shirt_part[rel=vestedge]').attr("id") : "1";
                         
                         
                         
                         styleImg = basePath + "media/default_tool_img/blank.png";
                         $scope.actStyleVestArr['veststyle'] = basePath + "media/vest_images/veststyle/" + $scope.fabricId + "_style_" + vestEdgeId + "_bottom_" + vestStyleId + "_view_1.png";
                         } else {
                         
                         var vestStyleId = angular.element("#view1").find('.shirt_part[rel=veststyle]').attr("id") ? angular.element("#view1").find('.shirt_part[rel=veststyle]').attr("id") : "1";
                         var vestEdgeId = angular.element("#view1").find('.shirt_part[rel=vestedge]').attr("id") ? angular.element("#view1").find('.shirt_part[rel=vestedge]').attr("id") : "1";
                         styleImg = basePath + "media/vest_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + vestStyleId + "_bottom_" + vestEdgeId + "_view_1.png";
                         $scope.actStyleVestArr['veststyle'] = styleImg;
                         $scope.actStyleVestArr['backveststyle'] = basePath + "media/vest_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + vestStyleId + "_bottom_" + vestEdgeId + "_view_2.png";
                         dispStyleBtn(styleId);
                         }
                         
                         
                         
                         
                         }*/ /*else if (parent == "vestlapel")
                          {
                          var vestStyleId = styleId; //angular.element("#view1").find('.shirt_part[rel=veststyle]').attr("id") ? angular.element("#view1").find('.shirt_part[rel=veststyle]').attr("id") : "1";
                          
                          var vestLapelId = angular.element("#view1").find('.shirt_part[rel=vestlapel]').attr("id") ? angular.element("#view1").find('.shirt_part[rel=vestlapel]').attr("id") : "1";
                          styleImg = basePath + "media/vest_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + vestStyleId + "_lapel_" + vestLapelId + "_view_1.png";
                          }*/ else if (parent == "vestlapel")
                        {
                            var vestStyleId = angular.element("#view1").find('.shirt_part[rel=veststyle]').attr("id") ? angular.element("#view1").find('.shirt_part[rel=veststyle]').attr("id") : "1";
                            var vestLapelId = angular.element("#view1").find('.shirt_part[rel=vestlapel]').attr("id") ? angular.element("#view1").find('.shirt_part[rel=vestlapel]').attr("id") : "1";

                            if ($scope.activeView == 3) {
                                styleImg = basePath + "media/vest_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + vestStyleId + "_lapel_" + vestLapelId + "_view_1.png";
                            } else {
                                styleImg = basePath + "media/vest_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_style_" + vestStyleId + "_lapel_" + vestLapelId + "_view_1.png";
                            }

                        } else if (parent == "vestpockets" || parent == "breastpocket")
                        {
                            if (parent == "breastpocket")
                            {

                                if (styleId == "2")
                                {
                                    styleImg = basePath + "media/default_tool_img/blank.png";
                                } else
                                {
                                    if ($scope.activeView == 3) {
                                        styleImg = basePath + "media/vest_images/vestpocket/" + $scope.fabricId + "_" + parent.toLowerCase() + "_view_1.png";
                                    } else {
                                        styleImg = basePath + "media/vest_images/vestpocket/" + $scope.fabricId + "_" + parent.toLowerCase() + "_view_1.png";
                                    }
                                }
                            } else
                            {
                                if ($scope.activeView == 3) {
                                    styleImg = basePath + "media/vest_images/vestpocket/" + $scope.fabricId + "_vestpocket_" + styleId + "_view_1.png";
                                } else {
                                    styleImg = basePath + "media/vest_images/vestpocket/" + $scope.fabricId + "_vestpocket_" + styleId + "_view_1.png";
                                }

                            }



                        } else if (parent == "pocketsquare")
                        {
                            if (jQuery("#div5 #divaccent_3 #2").hasClass("activeDesign")) {
                                styleImg = basePath + "media/jacket_images/" + parent.toLowerCase() + "/" + styleId + "_suitpocketsquare_2_view_1.png";
                                styleImg = basePath + "media/jacket_images/" + parent.toLowerCase() + "/" + styleId + "_suitpocketsquare_1_view_1.png";
                            }
                        } else if (parent == "lining")
                        {

                            var styleTypeId = angular.element("#view1").find('.shirt_part[rel=Style]').attr("id") ? angular.element("#view1").find('.shirt_part[rel=Style]').attr("id") : "1";
                            /* if ($scope.activeView == 3) {
                             
                             if (styleTypeId == "1") {
                             
                             styleImg = basePath + "media/jacket_images/lining/" + linid_active_id + "_liningstyle_" + styleTypeId + "_view_3.png";
                             } else if (styleTypeId == "4") {
                             
                             styleImg = basePath + "media/jacket_images/lining/1_liningstyle_" + styleTypeId + "_view_3.png";
                             }
                             }*/
                            styleImg = basePath + "media/jacket_images/lining/" + fabricId + "_liningstyle_" + styleTypeId + "_view_3.png";

                            var liningCurrentId = fabricId;
                            $scope.actStyleJKArr['vestlining'] = basePath + "media/vest_images/vestlining/" + liningCurrentId + "_vestlining_view_3.png";



                        } else if (parent == "vestlining")
                        {

                            var styleTypeId = angular.element("#view1").find('.shirt_part[rel=Style]').attr("id") ? angular.element("#view1").find('.shirt_part[rel=Style]').attr("id") : "1";
                            if (styleTypeId == "1") {

                                styleImg = basePath + "media/jacket_images/lining/1_liningstyle_1_view_3.png";
                            } else if (styleTypeId == "4") {

                                styleImg = basePath + "media/jacket_images/lining/1_liningstyle_4_view_3.png";
                            }
                        } else if (parent == "backcollaraccent") {
                            styleId = angular.element('#divaccent_5 ul:first').find('.activeDesign').attr('id') ? angular.element('#divaccent_5 ul:first').find('.activeDesign').attr('id') : "1";

                            if (styleId == 2)
                            {
                                angular.element('#divaccent_5 ul.BackCollar').css("display", "block");
                                styleImg = $scope.actStyleJKArr['backcollaraccent'] = basePath + "media/jacket_images/suitnecklining/" + $scope.backcollfabId + "_suitnecklining_view_2.png";
                            } else
                            {
                                angular.element('#divaccent_5 ul.BackCollar').css("display", "none");
                                styleImg = $scope.actStyleJKArr['backcollaraccent'] = basePath + "media/default_tool_img/blank.png";
                            }
                        } else if (parent == "backcollar")
                        {

                            styleId = angular.element('#divaccent_5 ul > li.activeDesign').attr('id') ? angular.element('#divaccent_5 ul > li.activeDesign').attr('id') : "1";

                            if (styleId == 2)
                            {

                                styleImg = $scope.actStyleJKArr['backcollar'] = basePath + "media/jacket_images/back_collar/" + $scope.backcollfabId + "_backCollar_view_2.png";
                            } else
                            {

                                styleImg = $scope.actStyleJKArr['backcollar'] = basePath + "media/default_tool_img/blank.png";
                            }


                        } else if (parent == "elbowpatches") {


                            styleId = angular.element('#divaccent_6 ul > li.activeDesign').attr('id') ? angular.element('#divaccent_6 ul > li.activeDesign').attr('id') : "1";


                            if (styleId == 2)
                            {

                                styleImg = $scope.actStyleJKArr['backelbow'] = basePath + "media/jacket_images/elbow_patches/" + $scope.backelbofabId + "_elbowpatch_1_view_2.png";
                            } else
                            {

                                styleImg = $scope.actStyleJKArr['backelbow'] = basePath + "media/default_tool_img/blank.png";
                            }
                        } else
                        {

                            if (parent == "Style")//update Lapel design Style
                            {
                                var styleTypeId = angular.element("#view1").find('.shirt_part[rel=Style]').attr("id") ? angular.element("#view1").find('.shirt_part[rel=Style]').attr("id") : "1";
                                var LapelTypeId = angular.element("#lapelSize").prev().prev().find(".activeDesign").attr("id") ? angular.element("#lapelSize").prev().prev().find(".activeDesign").attr("id") : "1";
                                var LapelSizeId = angular.element("#lapelSize").find(".activeDesign").attr("id") ? angular.element("#lapelSize").find(".activeDesign").attr("id") : "1";
                                $scope.actStyleJKArr['Lapel'] = basePath + "media/jacket_images/lapel/" + $scope.fabricId + "_lapelstyle_1_size_" + LapelSizeId + "_lapeltype_1_view_1.png";
                                $scope.actStyleJKArr['collarstyle'] = basePath + "media/jacket_images/collar/" + $scope.fabricId + "_collarstyle_" + styleTypeId + "_view_3.png";
                                $scope.actStyleJKArr['vestcollar'] = basePath + "media/vest_images/vestlining/" + $scope.fabricId + "_vestcollar_view_3.png";
                                $scope.actStyleJKArr['pantfit_straight'] = basePath + "media/pant_images/pantfit/" + $scope.fabricId + "_pantfit_straight_1_view_3.png";
                                $scope.actStyleJKArr['pantfit_tilt'] = basePath + "media/pant_images/pantfit/" + $scope.fabricId + "_pantfit_tilt_1_view_3.png";
                                $scope.actStyleJKArr['lining'] = basePath + "media/jacket_images/lining/1_liningstyle_1_view_3.png";
                                //back image

                                $scope.actStyleJKArr['BackStyle'] = basePath + "media/jacket_images/" + parent.toLowerCase() + "/" + $scope.fabricId + "_" + parent.toLowerCase() + "_1_view_2.png";
                                // dispStyleBtn(styleId);



                            }

                            styleImg = basePath + "media/jacket_images/style/" + $scope.fabricId + "_" + parent.toLowerCase() + "_" + styleId + "_view_1.png";
                            dispStyleBtn(styleId, parent);
//alert(styleImg)
                            // console.log(styleImg)
                        }

                        if (topcoatStatus == false) {
                            if (parent == "topcoatstyle") {

                                angular.element("#mainImageHolder").find(".shirt_part[rel=pantfit_straight]").hide();
                                angular.element("#mainImageHolder").find(".shirt_part[rel=pantfit_style]").hide();//
                                angular.element("#mainImageHolder").find(".shirt_part[rel=pantfit_style]").hide();
                                angular.element("#mainImageHolder").find(".shirt_part[rel=pantFoldCuff]").hide();//
                                angular.element("#mainImageHolder").find(".shirt_part[rel=foldPantPleats]").hide();
                                jQuery('#view3').find('.shirt_part[rel=foldedPantPocket]').hide()
                                styleImg = basePath + "media/topcoat_images/styles/" + $scope.fabricId + "_coatstyles_" + styleId + "_view_1.png";
                                angular.element("#mainImageHolder").find('.shirt_part[rel=topcoatstyle]').attr("src", basePath + "media/topcoat_images/styles/" + $scope.fabricId + "_coatstyles_" + styleId + "_view_1.png");
                                angular.element("#mainImageHolder").find('.shirt_part[rel=TopCoatCollar]').attr("src", basePath + "media/topcoat_images/collars/" + $scope.fabricId + "_coatcollar_" + styleId + "_view_1.png");
                                angular.element("#mainImageHolder").find('.shirt_part[rel=TopCoatSleeve]').attr("src", basePath + "media/topcoat_images/sleeves/" + $scope.fabricId + "_coatsleeves_1_view_1.png");
                                angular.element("#mainImageHolder").find(".shirt_part[rel=TopCoatSleeveButton]").attr("src", basePath + "media/topcoat_images/sleeves/static/Back_Sleeve_Common_Buttons.png");
                                if (styleId == 1) {
                                    angular.element("#mainImageHolder").find('.shirt_part[rel=TopCoatPocket]').attr("src", basePath + "media/default_tool_img/blank.png");

                                } else {
                                    angular.element("#mainImageHolder").find('.shirt_part[rel=TopCoatPocket]').attr("src", basePath + "media/topcoat_images/pockets/" + $scope.fabricId + "_coatpockets_" + styleId + "_view_1.png");
                                }

                                angular.element("#mainImageHolder").find('.shirt_part[rel=topcoatbuttons]').attr("src", basePath + "media/topcoat_images/styles/buttons/buttons_" + styleId + ".png");
                                angular.element("#mainImageHolder").find('.shirt_part[rel=TopCoatStyleBack]').attr("src", basePath + "media/topcoat_images/styles/" + $scope.fabricId + "_coatstyles_" + styleId + "_view_2.png");
                                angular.element("#mainImageHolder").find('.shirt_part[rel=TopCoatCollarBack]').attr("src", basePath + "media/topcoat_images/collars/" + $scope.fabricId + "_coatbackcollar_view_2.png");
                                angular.element("#mainImageHolder").find('.shirt_part[rel=TopCoatSleeveBack]').attr("src", basePath + "media/topcoat_images/sleeves/" + $scope.fabricId + "_coatsleeves_1_view_2.png");

                                angular.element("#mainImageHolder").find('.shirt_part[rel=TopCoatLining]').attr("src", basePath + "media/topcoat_images/lining/" + $scope.fabricId + "_coatlining_view_3.png");

                                angular.element("#mainImageHolder").find('.shirt_part[rel=TopCoatCollarFold]').attr("src", basePath + "media/topcoat_images/collars/" + $scope.fabricId + "_coatbackcollar_view_3.png");

                                if (styleId == 3 || styleId == 4) {
                                    angular.element("#mainImageHolder").find('.shirt_part[rel=TopCoatBreastPocket]').attr("src", basePath + "media/topcoat_images/styles/" + $scope.fabricId + "_coatbreast_" + styleId + "_view_1.png");
                                } else {
                                    angular.element("#mainImageHolder").find('.shirt_part[rel=TopCoatBreastPocket]').attr("src", basePath + "media/default_tool_img/blank.png");
                                }

//                                $scope.IsJacket = false;//hide jacket
//                                hideJacket();


                            }
                        }


                        var designStyleId = suitDesignData[design].id;
                        if (styleType == "jacket")
                        {

                            //  console.log("inside JACKET update of fab clck ")
                            var hClass = jQuery("#divs_" + designStyleId + " li").hasClass("activeDesign");
                            if (!hClass)
                            {
                                jQuery("#divs_" + designStyleId + " li:first").addClass("activeDesign");
                            }


                            $scope.actStyleJKArr[parent] = styleImg;

//                        $scope.actStyleJKArr[parent + "_index"] = ind;
                            $scope.actStyleJKArr[parent + '_styleName'] = styleName;
                            $scope.actStyleJKArr[parent + '_id'] = styleId;
                            $scope.actStyleJKArr[parent + '_btnCount'] = $scope.allDesignStyles.btn_count;
                            $scope.actStyleJKArr[parent + '_label'] = labelName;
                            $scope.actStyleJKArr[parent + '_icon'] = $scope.activeStyle.class;
                        } else if (styleType == "pant") {

                            /* if (jQuery("#mCSB_14_container").children().children().hasClass("activeDesign") == true) {
                             } else {
                             jQuery("#divpant_" + designStyleId + " li:first").addClass("activeDesign");
                             }*/




                            var hClass = jQuery("#divpant_" + designStyleId + " li").hasClass("activeDesign");
                            if (!hClass)
                            {
                                jQuery("#divpant_" + designStyleId + " li:first").addClass("activeDesign");
                            }


                            $scope.actStylePantArr[parent] = styleImg;
//                        $scope.actStylePantArr[parent + "_index"] = ind;
                            $scope.actStylePantArr[parent + '_styleName'] = styleName;
                            $scope.actStylePantArr[parent + '_id'] = styleId;
                            $scope.actStylePantArr[parent + '_label'] = labelName;
                            $scope.actStylePantArr[parent + '_icon'] = $scope.activeStyle.class;
                        } else if (styleType == "vest") {


                            var hClass = jQuery("#divvest_" + designStyleId + " li").hasClass("activeDesign");
                            if (!hClass)
                            {
                                jQuery("#divvest_" + designStyleId + " li:first").addClass("activeDesign");
                            }
//                            console.log("$scope.actStyleJKArr[parent] == " + styleImg + "parent == " + parent)

                            $scope.actStyleVestArr[parent] = styleImg;
//                    $scope.actStyleVestArr[parent + "_index"] = ind;
                            $scope.actStyleVestArr[parent + '_styleName'] = styleName;
                            $scope.actStyleVestArr[parent + '_id'] = styleId;
                            $scope.actStyleVestArr[parent + '_btnCount'] = $scope.allDesignStyles.btn_count;
                            $scope.actStyleVestArr[parent + '_label'] = labelName;
                            $scope.actStyleVestArr[parent + '_icon'] = $scope.activeStyle.class;
                        } else if (styleType == "topcoat") {

                          //  console.log("inside topcode update of fab click ")
                            $scope.actStyleVestArr[parent] = styleImg;
//                            $scope.actStyleVestArr[parent + "_index"] = ind;
                            $scope.actStyleVestArr[parent + '_styleName'] = styleName;
                            $scope.actStyleVestArr[parent + '_id'] = styleId;
                            $scope.actStyleVestArr[parent + '_btnCount'] = $scope.activeStyle.btn_count;
                            $scope.actStyleVestArr[parent + '_icon'] = $scope.activeStyle.class;
                        }

                    }
                }
            }
        }

// var cnt=0;
//                    jQuery("#div1 .box_option ul .activeDesign").each(function() {
//                         cnt++;
//                        stylePriceArr[cnt] = jQuery(this).attr("price");
//                       
//                       // console.log("stylePriceArr[" + cnt + "]" + stylePriceArr[cnt])
//                        
//                    })
        updateProduct();
        hideJacket();
        imageLoaded();
        if ($scope.IsPant) {

            hidePant();
        }

        //updateTieButtonShoes();
    }

    function applyFabricOnSuitInit()
    {
        var first = tempJk;
        var second = tempPt;
        var third = tempVst;
        var forth = tempAccnt;
//        var fifth = temptopCoat;

        var data = jQuery.merge(jQuery.merge([], first), second);
        var data1 = jQuery.merge(jQuery.merge([], data), third);
        var suitDesignData = jQuery.merge(jQuery.merge([], data1), forth);
//        var suitDesignData = jQuery.merge(jQuery.merge([], data2), fifth);
        var parent, styleImg, styleType, styleName, styleId, labelName;


        $scope.allDesignStyles = "";

        var styleCnt = "0";

        for (var design in suitDesignData)
        {
            if (suitDesignData.hasOwnProperty(design))
            {
                $scope.allDesignStyles = suitDesignData[design]["style"][0];
                if ($scope.allDesignStyles) {

                    if (!$scope.allDesignStyles.parent)
                    {
                        continue;
                    }


                    parent = $scope.allDesignStyles.parent.replace(/\s+|&/g, "");
                    styleName = $scope.allDesignStyles.name.replace(/\s+|&/g, "");
                    labelName = $scope.allDesignStyles.parent;
                    styleId = $scope.allDesignStyles.id;
                    if ($scope.allDesignStyles.price) {
                        stylePriceArr[parent] = $scope.allDesignStyles.price;
                    } else {
                        stylePriceArr[parent] = "0.0";
                    }


                    //get style type single or double
                    styleType = $scope.allDesignStyles.designType;
                    if ($scope.actStyleData[parent]) {
                        styleId = $scope.actStyleData[parent];
                    }

                    var designStyleId = suitDesignData[design].id;
                    var designStyleId = suitDesignData[design].id;
                    //act fab end
                    var hClass = jQuery("#div4 .fabvie").hasClass("activeDesign");
                    if (!hClass)
                    {

                        jQuery("#div4 .fabvie:first").addClass("activeDesign");
                    }

                    //act fab end



                    if (styleType == "jacket")
                    {

                        var hClass = jQuery("#divs_" + designStyleId + " li").hasClass("activeDesign");
                        if (!hClass)
                        {
                            jQuery("#divs_" + designStyleId + " li:first").addClass("activeDesign");
                        }
                    } else if (styleType == "pant") {


                        var hClass = jQuery("#divpant_" + designStyleId + " li").hasClass("activeDesign");
                        if (!hClass)
                        {
                            jQuery("#divpant_" + designStyleId + " li:first").addClass("activeDesign");
                        }
                    } else if (styleType == "vest") {


                        var hClass = jQuery("#divvest_" + designStyleId + " li").hasClass("activeDesign");
                        if (!hClass)
                        {
                            jQuery("#divvest_" + designStyleId + " li:first").addClass("activeDesign");
                        }
                    }
                }
            }
        }


        currStyleName = "";

        imageLoaded();
        angular.element("#mainImageHolder").find('.shirt_part[rel="ManBow"]').hide();
        angular.element("#mainImageHolder").find('.shirt_part[rel="Tie"]').hide();

        jQuery("#displayLargeImg").html('<img id="slide" src="' + basePath + 'media/fabric_images/01.png">');

        setTimeout(function () {
            jQuery("#divvest_1 li:first").trigger("click");
            jQuery("#divs_2 li:first").trigger("click");

            setTimeout(function () {

//                jQuery("#divvest_1 li:first").trigger("click");

                setTimeout(function () {
                    console.log("in fabric loaded ");

                    jQuery("#divpant_1 li:first").trigger("click");
                    angular.element("#view1").find('.shirt_part[rel=ViewStatic]').hide();
                    angular.element("#div4 fabricview:first").trigger("click");
                    jQuery("#div5 .slde-in ul > li:last").remove();
//                    console.log("in fabric loaded ");
//
//                    angular.element("#view1").find('.shirt_part[rel=ViewStatic]').hide();
//                    angular.element("#div4 fabricview:first").trigger("click");
//                    jQuery("#div5 .slde-in ul > li:last").remove();
                    //updateInItZoom();
                }, 2500);

            }, 200);
        }, 200);
        jQuery("#hideTopCoat").hide()
    }



    function imageLoaded() {
//alert($scope.partOption)
        if (currStyleName == "fabric")
        {
            if ($scope.partOption == "all" || $scope.partOption == "jacket") {
                angular.element('.shirt_part[rel="Style"]').load(function () {
                    angular.element(".mainToolarea").fadeIn("slow", function (e) {
                        setTimeout(function () {
                            angular.element(".suit-preloader").hide();
                            angular.element(".mainToolarea").css({"opacity": "1"});
                            angular.element("#theInitialProgress").hide();
                            currStyleName = "";
                        }, 500);
                    });
                });
            } else if ($scope.partOption == "pant") {
                angular.element('.shirt_part[rel="pantStyle"]').load(function () {

                    angular.element(".mainToolarea").fadeIn("slow", function (e) {
                        setTimeout(function () {
                            angular.element(".suit-preloader").hide();
                            angular.element(".mainToolarea").css({"opacity": "1"});
                            angular.element("#theInitialProgress").hide();
                            currStyleName = "";
                        }, 500);
                    });
                });
            } else if ($scope.partOption == "vest") {
                angular.element('.shirt_part[rel="veststyle"]').load(function () {

                    angular.element(".mainToolarea").fadeIn("slow", function (e) {
                        setTimeout(function () {
                            angular.element(".suit-preloader").hide();
                            angular.element(".mainToolarea").css({"opacity": "1"});
                            angular.element("#theInitialProgress").hide();
                            currStyleName = "";
                        }, 500);
                    });
                });
            } else if ($scope.partOption == "all" || $scope.partOption == "topcaot") {
                angular.element('.shirt_part[rel="Style"]').load(function () {
                    angular.element(".mainToolarea").fadeIn("slow", function (e) {
                        setTimeout(function () {
                            angular.element(".suit-preloader").hide();
                            angular.element(".mainToolarea").css({"opacity": "1"});
                            angular.element("#theInitialProgress").hide();
                            currStyleName = "";
                        }, 500);
                    });
                });
            }



        } else if (currStyleName != "") {


            if (currStyleName == "Style")//threads
            {
                angular.element('.shirt_part[rel="Style"]').load(function () {

                    angular.element(".mainToolarea").fadeIn("slow", function (e) {
                        currStyleName = "";
                        setTimeout(function () {
                            //alert("in style")
                            angular.element(".suit-preloader").hide();
                            angular.element(".mainToolarea").css({"opacity": "1"});
                            angular.element("#theInitialProgress").hide();
                        }, 500);
                    });
                });
            } else if (currStyleName == "brassbuttons" || currStyleName == "buttons" || currStyleName == "threads" || currStyleName == "lining" || currStyleName == "pocketsquare" || currStyleName == "pantfit" || currStyleName == "BackCollar" || currStyleName == "backcollar" || currStyleName == "elbowpatches" || currStyleName == "ElbowPatch")//threads
            {


                setTimeout(function () {
                    currStyleName = "";
                    angular.element(".suit-preloader").hide();
                    angular.element(".mainToolarea").css({"opacity": "1"});
                    angular.element("#theInitialProgress").hide();
                }, 500);

            } else
            {

                angular.element('.shirt_part[rel="' + currStyleName + '"]').load(function () {

                    angular.element(".mainToolarea").fadeIn("slow", function (e) {
                        currStyleName = "";
                        setTimeout(function () {
                            angular.element(".suit-preloader").hide();
                            angular.element(".mainToolarea").css({"opacity": "1"});
                            angular.element("#theInitialProgress").hide();
                        }, 100);
                    });
                });
            }

        } else {
            angular.element(".suit-preloader").hide();
            angular.element(".mainToolarea").css({"opacity": "1"});
            angular.element("#theInitialProgress").hide();
        }


        if (jQuery("#divs_2 li.activeDesign").attr("id") == "7") {
            jQuery("#mainImageHolder").find("[rel=Lapel]").hide();
        }
    }

});

function CurrentActiveClass(obj) {
    var currentAttr;
    jQuery("#toolHeader").find(".active").removeClass("active");
    jQuery(obj).addClass("active");
    currentAttr = jQuery(obj).attr("id");
    if (currentAttr == "stylSec")
    {
        jQuery("#stylePartOfTool").css({"display": "block"});
        jQuery("#fabricPartOfTool").css({"display": "none"});
        jQuery("#accentPartOfTool").css({"display": "blobk"});
    } else if (currentAttr == "febSec")
    {

        //trigger to active fab if not trig first
        if (fabricId != "0")
        {
            //check active if not act then only trigger
            var fabId = angular.element("#fabricInnerPart fabricview").find(".activeDesign").parent().attr("id");
            if (!fabId) {
                angular.element("#fabricInnerPart fabricview[id=" + fabricId + "]").trigger("click");
            }

        } else {
            angular.element("#fabricInnerPart fabricview:first").trigger("click");
        }
        //end

        jQuery("#fabricPartOfTool").css({"display": "block"});
        jQuery("#stylePartOfTool").css({"display": "none"});
        jQuery("#accentPartOfTool").css({"display": "blobk"});
    } else if (currentAttr == "accSec")
    {
        jQuery("#accentPartOfTool").css({"display": "blobk"});
        jQuery("#stylePartOfTool").css({"display": "none"});
        jQuery("#fabricPartOfTool").css({"display": "none"});
        //
    }

}
function closeButton() {

    jQuery(".container-fluid").css({"opacity": "1"});
    jQuery(".mainToolarea").css({"height": "0px"});
    jQuery(".mainToolarea").css({"width": "0px"});
    jQuery(".modal-content .modal-body").css({"margin-left": "0px"});
}
// Get the modal
var modal = document.getElementById('zoomOfTool');

// When the user clicks anywhere outside of the modal, close it
window.onclick = function (event) {

    if (event.target == modal) {
        closeButton();

    }
}
function zoomButtonHide() {

    jQuery("#zoomButton").css({"display": "none"});
}
function zoomButtonShow() {

    jQuery("#zoomButton").css({"display": "block"});
    // jQuery("#zoomButton").css({"margin-left": "10px"});
}



function imageLoadedTemp() {

    angular.element('.shirt_part[rel=Pockets]').load(function () {

        angular.element(".mainToolarea").fadeIn("slow", function (e) {

            setTimeout(function () {
                jQuery(".preloader").hide();
                angular.element(".suit-preloader").hide();
                angular.element(".mainToolarea").css({"opacity": "1"});
                jQuery(".container-fluid").css({"opacity": "1"});
            }, 500);
        });
    });
    setTimeout(function () {
        angular.element(".suit-preloader").hide();
        angular.element(".mainToolarea").css({"opacity": "1"});
    }, 500);
    if (jQuery("#divs_2 li.activeDesign").attr("id") == "7") {
        jQuery("#mainImageHolder").find("[rel=Lapel]").hide();
    }

}

/* call after DOM loaded */
angular.element(window).load(function () {
    return false;
    //trigger to first design ele
    angular.element("#theMainInitialProgress").trigger("click");
    angular.element("#theInitialProgress").hide();
    angular.element(".container-fluid").css({"opacity": "1"});
});




function updateInItZoom()
{
    if (jQuery(window).width() > 767) {

        jQuery('.zoomContainer').remove();
        jQuery('.fabvie img').removeData('elevateZoom');

        inZoomEvalate();
    }
}

function inZoomEvalate()
{
    jQuery('.fabvie img').data('zoom-image', jQuery('.fabvie img').attr("data-image")).elevateZoom({
        responsive: true
    });

}
