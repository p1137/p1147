
angular.module("Suit").controller("measureController", function ($scope, measurementDataService, $http) {

    $scope.activeView = 1;

    $scope.actFit = "1";
    $scope.unitName = "in";
    $scope.fabricId = "1";
    $scope.bodyId = "1";

    $scope.measurementViews = measurementDataService.getMeasurementSuitView();
    $scope.measurementBackgrounds = measurementDataService.getMeasurementBackrounds();
    
    $scope.measurementFabrics = measurementDataService.getMeasurementFabrics();
    $scope.measurementJkSizes = measurementDataService.getCustSizeJacktData();
    $scope.measurementPntSizes = measurementDataService.getCustSizePantData();
    $scope.measurementJkSizesCm = measurementDataService.getCustSizeJacktDataCm();
    $scope.measurementPntSizesCm = measurementDataService.getCustSizePantDataCm();
    $scope.currJkSizes = $scope.measurementJkSizes[0];
    $scope.currPntSizes = $scope.measurementPntSizes[0];
    $scope.measurementBodies = measurementDataService.getMeasurementBodies();






    $scope.changeView = function ($event, viewId) {

        angular.element(".mainToolareaM .suit-preloader").show();

        $scope.activeView = viewId;
        angular.element(".mainToolareaM .view-thumb button").removeClass("active");
        angular.element($event.currentTarget).addClass("active");
        imageLoaded();


    }

    $scope.openMeasureT = function () {
        if (!jQuery("#bodySize option:selected").val()) {
            jQuery("#bodySize option:first").prop("selected", "true");
        }

    }
    $scope.updateModal = function () {

        if (myValidation()) {

            jQuery("body").removeClass("lefrmenuOpen");
            angular.element(".mainToolareaM .suit-preloader").show();
            $scope.actFit = angular.element("#bodySize option:selected").val();

            updateProduct();
        }

    }



    $scope.changeBacgrnd = function ($event, viewId) {

        jQuery("body").removeClass("lefrmenuOpen");
        jQuery(".rightMenuBtn").removeClass("LcrossMenu");
        jQuery("body").removeClass("rightmenuOpen");
        jQuery(".leftMenuBtn").removeClass("RcrossMenu");


        angular.element("#measurementbackground").find(".activeDesign").removeClass('activeDesign');
        angular.element($event.currentTarget).find(".fabvie").addClass("activeDesign");

        //for mobile view
        jQuery("body").removeClass("lefrmenuOpen");

        var BgFlg = angular.element($event.currentTarget).find(".faric-name").text();
        if (BgFlg != "No Background") {
            var bg = angular.element($event.currentTarget).find("img").attr("src");
            jQuery('.mainToolareaM').css({'background-image': 'url(' + bg + ')'});
        } else {
            jQuery('.mainToolareaM').css({'background-image': 'none'});
        }


    }

    $scope.changeBody = function ($event) {

        //angular.element(".mainToolareaM .suit-preloader").show();

        //jQuery("#measurementupld02").find('span')
        angular.element(".mainToolareaM .suit-preloader").show();
        angular.element("#measurementupld02").find(".activeDesign").removeClass('activeDesign');
        angular.element($event.currentTarget).find(".fabvie").addClass("activeDesign");

        $scope.bodyId = angular.element($event.currentTarget).attr("id");


        updateProduct();

    }




    $scope.changeSize = function (Fit) {
        Fit = parseInt(Fit) - 1;


        changeUnitSizeChange($scope.unitName)

    }

    $scope.selectBody = function ($event) {
        //for mobileview
        jQuery("body").removeClass("lefrmenuOpen");

        angular.element(".mainToolareaM .suit-preloader").show();

        angular.element("#content-5_hor").find(".activeDesign").removeClass('activeDesign');
        angular.element($event.currentTarget).addClass("activeDesign");

        $scope.actFit = angular.element($event.currentTarget).attr("fit");


        updateProduct();
    }





    //load fabric for suit
    $scope.fabricClick = function ($event, obj) {



        angular.element(".mainToolareaM .suit-preloader").show();

        angular.element("#measurementFabric").find(".activeDesign").removeClass('activeDesign');
        angular.element($event.currentTarget).find(".fabvie").addClass("activeDesign");

        $scope.fabricId = obj.id;





        updateProduct();
    }


    $scope.changeUnit = function (unitVal) {
        changeUnitSizeChange(unitVal)
    }


    function changeUnitSizeChange(unitVal)
    {
        $scope.unitName = unitVal;
        var convertVal = 0;


        if (unitVal == "cm") {

            $scope.currJkSizes = $scope.measurementJkSizesCm[$scope.actFit - 1];
            $scope.currPntSizes = $scope.measurementPntSizesCm[$scope.actFit - 1];

        } else {

            $scope.currJkSizes = $scope.measurementJkSizes[$scope.actFit - 1];
            $scope.currPntSizes = $scope.measurementPntSizes[$scope.actFit - 1];

        }

    }




    function updateProduct()
    {
        jQuery("body").removeClass("lefrmenuOpen");
        jQuery(".rightMenuBtn").removeClass("LcrossMenu");
        jQuery("body").removeClass("rightmenuOpen");
        jQuery(".leftMenuBtn").removeClass("RcrossMenu");
        //check this use view id dynamic
        jQuery(".theViewM").each(function () {

            var parent = jQuery(this).find("[rel=Body]").attr('rel');
            var type = jQuery(this).find("[rel=Body]").attr('type');
            var viewId = jQuery(this).attr('viewId');

//Bodytype_Fit_View_01
            var suitImgBody = basePath + "media/Body_Measurement_images/" + $scope.bodyId + "/Char_" + $scope.bodyId + "_" + $scope.actFit + "_View_0" + viewId + ".png";

            var suitImgFab = basePath + "media/Fabric_Measurement_images/Fabric_0" + $scope.fabricId + "_" + $scope.actFit + "_View_0" + viewId + ".png";


            jQuery(this).find("[rel=Body]").attr('src', suitImgBody);
            jQuery(this).find("[rel=Suit]").attr('src', suitImgFab);

        });



        imageLoaded();

    }

    function imageLoaded() {

        setTimeout(function () {
            angular.element(".mainToolareaM .suit-preloader").hide();
        }, 500);
    }


    /*setTimeout(function () {
     //setDefault
     angular.element("#measurementFabric fabricview:first").find(".fabvie").addClass("activeDesign");
     angular.element("#measurementbackground fabricview").eq(1).find(".fabvie").trigger("click");
     angular.element("#measurementupld02 fabricview").find(".fabvie").trigger("click");
     angular.element("#measurement01 li").eq(1).trigger("click");
     }, 500);*/

});


/* call after DOM loaded */
angular.element(window).load(function () {

    //setDefault
    angular.element("#measurementFabric fabricview:first").find(".fabvie").addClass("activeDesign");
    angular.element("#measurementbackground fabricview").eq(1).find(".fabvie").trigger("click");
    angular.element("#measurementupld02 fabricview").find(".fabvie").trigger("click");
    angular.element("#measurement01 li").eq(1).trigger("click");
    angular.element("#measurementupld02").find('.currency').hide();

})




