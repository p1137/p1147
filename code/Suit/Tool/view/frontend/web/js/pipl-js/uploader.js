/********************************************************************************
 * This script is brought to you by Vasplus Programming Blog
 * Website: www.vasplus.info
 * Email: info@vasplus.info
 *********************************************************************************/


function inItUploader()
{
    var uploadId, uploadDragId;
    var uploaded_files_location;
    var uploadedImagesHolder;
    var removeBack;
    var basePath;



    uploaded_files_location = "tool-data/img/customized-images/uploaded_files";
    uploadId = jQuery('#browse-image');
    uploadDragId = jQuery('#browse-image-drag');
    uploadedImagesHolder = jQuery("#imageContainer");
    var basePath = jQuery("#site_path").val();

    removeBack = jQuery("#removeBack").prop("checked");



    // 1) Browse upload container start
    new AjaxUpload(uploadId,
            {
                action: basePath + 'custom-ajax/uploader.php',
                name: 'file_to_upload',
                type: "POST",
                data: {
                    removeBack: removeBack
                },
                onSubmit: function (file, file_extensions)
                {
                    removeBack = jQuery("#removeBack").prop("checked");

                    jQuery(".jacket-preloader").show();


                    //Allowed file formats jpg|png|jpeg|gif|pdf|docx|doc|rtf|txt - You can add as more file formats or remove as you wish.
                    if (!(file_extensions && /^(jpg|png|tif|jpeg|gif|ai|eps|tiff|bmp|svg)$/.test(file_extensions)))
                    {
                        //If file format is not allowed then, display an error message to the user
                        alert("Sorry, you can only upload the following file formats:  EPS, AI, SVG, JPG, JPEG, PNG, TIF,TIFF, GIF, BMP.");
                        jQuery(".jacket-preloader").hide();

                        return false;
                    }
                    else
                    {
                        jQuery('#vpb_uploads_error_displayer').html('<div class="uplading_image">Uploading <img src="media/image/loadings.gif" align="absmiddle" /></div>');

                        // jQuery(".jacket-preloader").hide();
                        return true;
                    }
                },
                onComplete: function (file, response)
                {

                    if (response === "ERROR")
                    {
                        alert("Sorry, your file upload was unsuccessful. File size should be less than 20MB.");
                        jQuery(".jacket-preloader").hide();
                    }
                    else
                    {
                        var aifile = response;
                        var aifileExt = aifile.substring(aifile.lastIndexOf('.') + 1);



                        var imgPath = basePath + uploaded_files_location + '/' + aifile;
                        jQuery("#imageContainer").css("display", "block");
                        jQuery("#imageContainer img").css("opacity", "1");
                        jQuery("#imageContainer img").attr('src', imgPath);
//                        jQuery(uploadedImagesHolder).html('<img  src="' + imgPath + '" data-src="' + imgPath + '" class="vpb_image_design"  />');
                        jQuery("#imageContainerNote").css("display", "none");
                        jQuery(".delete-uploaded-img").css("display", "block");
                        jQuery(".image-position-holder").css("display", "block");

                        jQuery("#imageBoxList option:selected").attr("image", imgPath);
                        jQuery("#imageBoxList option:selected").attr("addedImageType", "image");

                        // addImage();
                        setTimeout(function () {
                            updateTextImageData();
                        }, 100);


                        return;
                    }
                }
            });

    //Browse upload container end

    // 2)For drag uploader starta


    new AjaxUpload(uploadDragId,
            {
                action: basePath + 'custom-ajax/uploader.php',
                name: 'file_to_upload',
                type: "POST",
                data: {
                    removeBack: removeBack
                },
                onSubmit: function (file, file_extensions)
                {

                    jQuery(".jacket-preloader").show();
                    //Allowed file formats jpg|png|jpeg|gif|pdf|docx|doc|rtf|txt - You can add as more file formats or remove as you wish.
                    if (!(file_extensions && /^(jpg|png|tif|jpeg|gif|ai|eps|tiff|bmp|svg)$/.test(file_extensions)))
                    {
                        //If file format is not allowed then, display an error message to the user
                        alert("Sorry, you can only upload the following file formats:  EPS, AI, SVG, JPG, JPEG, PNG, TIF,TIFF, GIF, BMP.");
                        jQuery(".jacket-preloader").hide();
                        return false;
                    }
                    else
                    {
                        jQuery('#vpb_uploads_error_displayer').html('<div class="uplading_image">Uploading <img src="media/image/loadings.gif" align="absmiddle" /></div>');
                        //jQuery(".jacket-preloader").hide();
                        return true;
                    }
                },
                onComplete: function (file, response)
                {
                    if (response === "ERROR")
                    {
                        alert("Sorry, your file upload was unsuccessful. File size should be less than 20MB.");

                    }
                    else
                    {
                        var aifile = response;
                        var aifileExt = aifile.substring(aifile.lastIndexOf('.') + 1);


                        var imgPath = basePath + uploaded_files_location + '/' + aifile;
                        jQuery("#imageContainer").show();
                        jQuery("#imageContainer img").css("opacity", "1");

                        jQuery("#imageContainer img").attr('src', imgPath);
//                        jQuery(uploadedImagesHolder + " img").html('<img  src="' + imgPath + '" data-src="' + imgPath + '" class="vpb_image_design"  /><input type="file"  onchange="dragUploadOnchange(this)" name="userprofile_picture"  id="filePhoto" style="opacity: 0;height: 100px"/> ');
                        jQuery("#imageContainerNote").css("display", "none");
                        jQuery(".delete-uploaded-img").css("display", "block");
                        jQuery(".image-position-holder").css("display", "block");


                        jQuery("#imageBoxList option:selected").attr("image", imgPath);
                        jQuery("#imageBoxList option:selected").attr("addedImageType", "image");

                        //addImage();
                        updateTextImageData();
                        setTimeout(function () {
                            updateTextImageData();
                        }, 1000);

                        return;
                    }
                }
            });




    //drag upload container end

}


//This removes all unwanted files
function remove_unwanted_file(id, file)
{
    if (confirm("If you are sure that you really want to remove the file " + file + " then click on OK otherwise, Cancel it."))
    {
        var dataString = "file_to_remove=" + file;
        jQuery.ajax({
            type: "POST",
            url: "remove_unwanted_files.php",
            data: dataString,
            cache: false,
            beforeSend: function ()
            {
                jQuery("#vpb_remove_button" + id).html('<img src="images/loadings.gif" align="absmiddle" />');
            },
            success: function (response)
            {
                jQuery('div#fileID' + id).fadeOut('slow');
            }
        });
    }
    return false;
}