var textureJSON;
var ajaxPath = "";
var basePath = "";
var pid;
var did = "0";
var views = {};
var reloadFlg = false;
var jeansActiveDataThumb = {};

jQuery(document).ready(function(e) {


    basePath = jQuery("#site_path").val();
    ajaxPath = jQuery("#site_path").val() + "custom-ajax/";

    jQuery.ajax({
        url: basePath + "men-shirt/jeans/getJeans",
        success: function(msg) {
            textureJSON = JSON.parse(msg);
            loadTextures();
            loadStyle();
            init();
            animate();
        }
    });


    jQuery("#add-to-cart-btn").click(function() {
        cartFlg = true;
        saveDesign();
        jQuery(".form-group #comment").val("");
        jQuery("#jeans-cart").hide();

    });

    jQuery("#close-modal, .j-close").click(function() {

       jQuery(".form-group #comment").val("");
    });

});



//save design
function saveDesign()
{
    var design;

    jQuery(".theInitialProgress-text").hide();
    jQuery("#theInitialProgress").show();

    design = saveModifiedDesignJSON();

    var pid = 10;


    jQuery.ajax({
        url: basePath + "men-shirt/jeans/saveJeansDesign",
        data: {
            design: design,
            did: did,
            product_id: pid
        },
        type: "POST",
        success: function(msg) {

            if (msg == "done") {
               window.location.href = basePath + 'checkout/cart/';
            }
        }
    });
}



function saveModifiedDesignJSON()
{
    var finalImageArray = {};
    var prodTotalPrice = jQuery(".totalQtyPrice").text();
    var prodPrice = jQuery("#fitPrice").text();
    var qty = "1";//jQuery(".quantity").text();
    var jeansNote = jQuery(".form-group #comment").val();
  

    finalImageArray["dataurl_3d"] = downloadImage();

    views["jeansAllData"] = jeansActiveDataThumb;
    views["actStyle"] = jQuery("#socks-style").find(".activeClass").attr("title");
    views["productPrice"] = prodPrice;
    views["qty"] = qty;
    views["jeansNote"] = jeansNote;


    return {
        views: views,
        imageGenerationData: finalImageArray,
    }
}

//image generation of threed object
function downloadImage() {

    //testing of resize the object
    return getImage();
}

function getImage() {

    return renderer.domElement.toDataURL("png");
}
