/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if (!Detector.webgl)
    Detector.addGetWebGLMessage();
var STATS_ENABLED = false;
var objName;
var container, stats;
var camera, scene, renderer; 
var m, mi;
var obj, fov;
var canvasHt = 600;
var canvasWt = 600;
var targetRotation = 0;
var targetRotationOnMouseDown = 0;
var mouseXOnMouseDown = 0;
var directionalLight, pointLight;
var mouseX = 0, mouseY = 0;
var loader = new THREE.BinaryLoader();
var light;
var mlib = {};
var jeansActiveData = {};
var jeansActiveDataThumb = {};
var mediaPath = jQuery("#media_path").val();

var OBJECT = {
    "obj_one": {
        url: mediaPath + "style_images/1525514060_Skinny.js",
        init_material: 2, //define initial material parts count
        materials: null

    }
};
OBJECT["obj_one"].materials = {};
function init() {

    container = document.getElementById('canvasHolder');
    jQuery(".tool-work-area").css({width: canvasWt + "px", height: canvasHt + "px"});
   
    // CAMERAS

    // camera = new THREE.PerspectiveCamera(fov = 15, window.innerWidth / window.innerHeight, 10, 2000);
    camera = new THREE.PerspectiveCamera(5, canvasWt / canvasHt, 0.1, 1000);
    camera.position.z = 450;
    // camera.position.x = 30;
    camera.position.y = 8;
    scene = new THREE.Scene();
    // LIGHTS

    var ambient = new THREE.AmbientLight(0x050505);
    //0x050505
    scene.add(ambient);
    var ambient = new THREE.AmbientLight(0x050505);
    scene.add(ambient);
    directionalLight = new THREE.DirectionalLight(0xffffff, 0.99);
    directionalLight.position.set(2, 1.2, 10).normalize();
    scene.add(directionalLight);
    directionalLight = new THREE.DirectionalLight(0xffffff, 0.7);
    directionalLight.position.set(-2, 1.2, -10).normalize();
    scene.add(directionalLight);
    //point-light
    pointLight = new THREE.PointLight(0xffffff, 0.09);
    pointLight.position.set(500, 500, 500);
    scene.add(pointLight);
    renderer = new THREE.WebGLRenderer({alpha: true, antialias: true, preserveDrawingBuffer: true});
    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setViewport(0, 0, canvasWt, canvasHt);
    renderer.setSize(canvasWt, canvasHt);
    renderer.shadowMapType = THREE.PCFSoftShadowMap;
    //change background color
    renderer.setClearColor(0xF8F6F7, 0);
    container.appendChild(renderer.domElement);
    container.addEventListener('mousedown', onDocumentMouseDown, false);
    container.addEventListener('touchstart', onDocumentTouchStart, false);
    container.addEventListener('touchmove', onDocumentTouchMove, false);
    window.addEventListener('resize', onWindowResize, false);
    if (STATS_ENABLED) {

        stats = new Stats();
        container.appendChild(stats.dom);
    }



    m = OBJECT[ "obj_one" ].materials;
    mi = OBJECT[ "obj_one" ].init_material;
    //set default texture index from the texture array
    OBJECT[ "obj_one" ].mmap = {
        0: m.body[ 3 ][ 1 ], // jeans
    };
    loader.load(OBJECT[ "obj_one" ].url, function(geometry) {
        createScene(geometry, "obj_one");
    });
}

jQuery("#canvasHolder").on("mousedown", function() {

    jQuery(".drag-rotate").fadeOut(3000);
    jQuery("body").css({"cursor": "move"});
});
jQuery("#canvasHolder").on("mouseup", function() {
    jQuery("body").css({"cursor": "pointer"});
});
function $(id) {
    return document.getElementById(id);
}
function button_name(bottle, index) {
    return "m_" + bottle + "_" + index;
}



function createButtons(materials, bottle) {

    var buttons;
   
    if (!reloadFlg) {

        jQuery(".texture-container").html("");
        //create category
        for (var i = 0; i < textureJSON['texture'].length; i++) {


            if (textureJSON['texture'][i]) {

                if (jQuery("#socks_" + textureJSON['texture'][i]['type']).length == 0) {

                    jQuery(".texture-container").append('<div class="socks-style-ul" id="socks_' + textureJSON['texture'][i]['type'] + '">  <label><strong><u> ' + textureJSON['texture'][i]['parent_name'] + '</u></strong></label><ul class="clearfix" id="socks_' + textureJSON['texture'][i]['type'] + '-ul">   </ul>  </div>')

                }
            }
        }

        //load respective cat data
        for (var i = 0; i < textureJSON['texture'].length; i++) {

            if (textureJSON['texture'][i]) {


                jQuery(".texture-container #socks_" + textureJSON['texture'][i]['type'] + "-ul").append('<li title="' + textureJSON['texture'][i]['name'] + '" type="' + textureJSON['texture'][i]['type'] + '" texture-price="' + textureJSON['texture'][i]['price'] + '" id="' + button_name(bottle, i) + '" onclick="changeColor(this)"> <div class="fabricView"><div class="fabric-view"><img src="' + textureJSON['texture'][i]['thumb_image'] + '" texture_image="' + textureJSON['texture'][i]['image'] + '" data-zoom-image="' + textureJSON['texture'][i]['hover_image'] + '"/></div><div class="fabric-detail">  <div></div><div></div> <div></div> </div></div></li> ')
            }

        }

        //load respective fade_design_effects category
        jQuery(".fade_riped_pants").html("");

        for (var i = 0; i < textureJSON['FadeDesignEffects'].length; i++) {

            if (textureJSON['FadeDesignEffects'][i]) {

                if (jQuery("#socks_effect" + textureJSON['FadeDesignEffects'][i]['type']).length == 0) {

                    jQuery(".fade_riped_pants").append('<div class="socks-style-ul" id="socks_effect' + textureJSON['FadeDesignEffects'][i]['type'] + '">  <label><strong><u> ' + textureJSON['FadeDesignEffects'][i]['parent_name'] + '</u></strong></label><ul class="clearfix" id="socks_effect' + textureJSON['FadeDesignEffects'][i]['type'] + '-ul">   </ul>  </div>')
                }
            }
        }


        //load respective fade_design_effects data
        for (var i = 0; i < textureJSON['FadeDesignEffects'].length; i++) {

            if (textureJSON['FadeDesignEffects'][i]) {

                jQuery(".fade_riped_pants #socks_effect" + textureJSON['FadeDesignEffects'][i]['type'] + "-ul").append('<li title="' + textureJSON['FadeDesignEffects'][i]['name'] + '" type="' + textureJSON['FadeDesignEffects'][i]['type'] + '" effect-price="' + textureJSON['FadeDesignEffects'][i]['price'] + '" id="' + button_name(bottle, i) + '" onclick="changeGlowEffects(this)"> <div class="fabricView"><div class="fabric-view"><img src="' + textureJSON['FadeDesignEffects'][i]['thumb_image'] + '" data-zoom-image="' + textureJSON['FadeDesignEffects'][i]['hover_image'] + '" glow-image="' + textureJSON['FadeDesignEffects'][i]['glow_image'] + '" parentCat="' + textureJSON['FadeDesignEffects'][i]['parent_cat'] + '"/></div><div class="fabric-detail">  <div></div><div></div> <div></div> </div></div></li> ')
            }

        }



        jQuery(".texture-container ul li[title='skin_texture']").hide();
    }

    return buttons;


}


function attachButtonMaterials(materials, faceMaterial, material_indices, bottle) {

    
    for (var i = 0; i < textureJSON['texture']; i++) {

        document.getElementById(button_name(bottle, i)).counter = i;
        document.getElementById(button_name(bottle, i)).addEventListener('click', function() {

            var materialImg = materials[ this.counter ][ 0 ].split("_")[0];
           
            materials[ this.counter ][ 0 ] = materialImg;
            if (bottle == 'obj_one')
            {
                var selPart = document.querySelector('input[name="optColor"]:checked') ? document.querySelector('input[name="optColor"]:checked').value : "FrontBack";
                console.log(selPart);
                if (selPart == 'FrontBack') {
                    faceMaterial.materials[1] = materials[ this.counter ][ 1 ];
                }

                return;
            }

            for (var j = 0; j < material_indices.length; j++) {

                faceMaterial.materials[ material_indices [ j ] ] = materials[ this.counter ][ 1 ];
            }

        }, false);
    }

    setTimeout(function() {
        
        jQuery(".suit-preloader").hide();
        jQuery("#theInitialProgress").hide();
        jQuery("#options").hide();
        jQuery("#leftPanel").hide();
        reloadFlg = true;
        
        var activeImg = jQuery(".texture-container .socks-style-ul li").hasClass("activeClass");
        if (activeImg == true) {
             jQuery(".texture-container .socks-style-ul li").find(".activeClass").trigger("click");
        }
        else {

            jQuery("#m_obj_one_3").trigger("click");
        }

       var ripedImg = jQuery("#socks_effect2-ul li").hasClass("activeClass");
       if(ripedImg == true){
           jQuery("#socks_effect2-ul li").find(".activeClass").trigger("click");
       }
       else{
           jQuery("#socks_effect2-ul li[title='No-effect']").trigger("click");
       }
         
    }, 1000);
}

var theCurrentObjM;
function createScene(geometry, bottle) {

    geometry.sortFacesByMaterialIndex();
    theCurrentObjM = new THREE.MultiMaterial(),
            s = OBJECT[ bottle ].scale * 2,
            r = OBJECT[ bottle ].init_rotation,
            materials = OBJECT[ bottle ].materials,
            mi = OBJECT[ bottle ].init_material,
            bm = OBJECT[ bottle ].body_materials;
    for (var i in OBJECT[ bottle ].mmap) {

        theCurrentObjM.materials[ i ] = OBJECT[ bottle ].mmap[ i ];
    }

    obj = new THREE.Mesh(geometry, theCurrentObjM);
    scene.add(obj);
    OBJECT[ bottle ].buttons = createButtons(materials.body, bottle);
    attachButtonMaterials(materials.body, theCurrentObjM, bm, bottle);
}



function onWindowResize() {
    renderer.setViewport(0, 0, canvasWt, canvasHt);
    camera.aspect = canvasWt / canvasHt;
    camera.updateProjectionMatrix();
}


function onDocumentMouseDown(event) {

    event.preventDefault();
    document.addEventListener('mousemove', onDocumentMouseMove, false);
    document.addEventListener('mouseup', onDocumentMouseUp, false);
    document.addEventListener('mouseout', onDocumentMouseOut, false);
    mouseXOnMouseDown = event.clientX - canvasWt;
    targetRotationOnMouseDown = targetRotation;
}


function onDocumentMouseMove(event) {

    mouseX = event.clientX - canvasWt;
    targetRotation = targetRotationOnMouseDown + (mouseX - mouseXOnMouseDown) * 0.02;
}


function onDocumentMouseUp(event) {

    document.removeEventListener('mousemove', onDocumentMouseMove, false);
    document.removeEventListener('mouseup', onDocumentMouseUp, false);
    document.removeEventListener('mouseout', onDocumentMouseOut, false);
}


function onDocumentMouseOut(event) {

    document.removeEventListener('mousemove', onDocumentMouseMove, false);
    document.removeEventListener('mouseup', onDocumentMouseUp, false);
    document.removeEventListener('mouseout', onDocumentMouseOut, false);
}


function onDocumentTouchStart(event) {

    if (event.touches.length === 1) {

        event.preventDefault();
        mouseXOnMouseDown = event.touches[ 0 ].pageX - canvasWt;
        targetRotationOnMouseDown = targetRotation;
    }

}


function onDocumentTouchMove(event) {

    if (event.touches.length === 1) {

        event.preventDefault();
        mouseX = event.touches[ 0 ].pageX - canvasWt;
        targetRotation = targetRotationOnMouseDown + (mouseX - mouseXOnMouseDown) * 0.05;
    }

}


function animate() {

    requestAnimationFrame(animate);
    render();

}

function render() {

    if (obj) {
        obj.rotation.y += (targetRotation - obj.rotation.y) * 0.19;
        renderer.render(scene, camera);
    }
}

function updateProperties()
{


    addText();
    addImage();
    textTexture.needsUpdate = true;
   
}



function changeColor(actObj)
{

    jQuery(".suit-preloader").show();
    jQuery(".responsive-buttonse").removeClass("right-pannels-this");//for responsive view
    jQuery("body").removeClass("landscape right-pannels-open");//for responsive view
    jQuery(".texture-container .socks-style-ul").find(".activeClass").removeClass("activeClass");
    jQuery(actObj).addClass("activeClass");
   

    var activeImg = jQuery(actObj).find("img").attr("texture_image");
    var thumbImg = jQuery(actObj).find("img").attr("src");
    
    jeansActiveData["texture"] = activeImg;
    jeansActiveDataThumb["texture"] = thumbImg;
    applyTexture();
  
}



//for changes of glow images .
function changeGlowEffects(actObj)
{
    jQuery(".responsive-buttonse").removeClass("right-pannels-this");//for responsive view
    jQuery("body").removeClass("landscape right-pannels-open");//for responsive view

    jQuery(actObj).parent().find(".activeClass").removeClass("activeClass");
    jQuery(actObj).addClass("activeClass");
    jQuery(".suit-preloader").show();


    var parentCat = jQuery(actObj).find("img").attr("parentCat");

    var glowImg = jQuery(actObj).find("img").attr("glow-image");
    var thumbImg = jQuery(actObj).find("img").attr("src");
    
    jeansActiveData[parentCat] = glowImg;


    jeansActiveDataThumb[parentCat] = thumbImg;
    applyTexture();

}



function changePart(actObj)
{

    jQuery("#socks-textures").css("display", "block");
}


function loadStyle() {
    var styles = textureJSON['styles'];
    jQuery("#socks-style ul").html('');
    for (var style in styles)
    {
        if (styles.hasOwnProperty(style)) {
            jQuery("#socks-style ul").append('<li catid="' + styles[style]["id"] + '" isReady = "' + styles[style]["ready"] + '" title="' + styles[style]["name"] + '" price="' + styles[style]["price"] + '"cat_js="' + styles[style]["js"] + '" onClick="loadTemplate(this);"><a href="javascript:void(0);"><img src="' + styles[style]["image"] + '"></a></li>');
        }
    }

    jQuery("#socks-style ul li:first").addClass("activeClass");
}

var textTexture;
function loadTextures()
{
    //jQuery(".suit-preloader").show();
    var texture = textureJSON['texture'];
    var textBoxList = textureJSON['texts'];
    var imageLogoPList = textureJSON['imageBox'];
    jQuery("#socks-textures ul").html('');
    textTexture = new THREE.Texture(canvas);
    OBJECT["obj_one"].materials["body"] = [];
    for (var textr in texture)
    {

        var image = texture[textr]["image"]; //+ "_" + texture[textr]["type"];


        mlib[texture[textr]["name"]] = new THREE.MeshLambertMaterial({map: THREE.ImageUtils.loadTexture(image)});
        /*mlib[texture[textr]["name"]] = new THREE.MeshBasicMaterial({map: THREE.ImageUtils.loadTexture(image)});

         mlib[texture[textr]["image"]] = new THREE.MeshLambertMaterial({map: THREE.ImageUtils.loadTexture(texture[textr]["image"])});
         */
        OBJECT["obj_one"].materials["body"].push([image, mlib[texture[textr]["name"]]]);
    }


    //load text box list
    var textsOptions = '', imagesOptions = '';
    var fontFamily = jQuery("#font-list").val();
    var fontSize = jQuery("#font_size").slider("value");
    var fontColor = "#cc0000";
    var fontBorderColor = jQuery(".fontBorderColorChange").find(".sp-input").val();
    var fontBorder = jQuery("#textBorderSizeList").val();
    for (var textB in textBoxList)
    {
        textsOptions += '<option fontFamily="' + fontFamily + '" fontSize ="' + fontSize + '" fontColor ="' + fontColor + '" value="' + textBoxList[textB]['name'] + '" fontBorderColor="' + fontBorderColor + '" fontBorder="' + fontBorder + '" org-x="' + textBoxList[textB]['x'] + '" org-y="' + textBoxList[textB]['y'] + '" x="' + textBoxList[textB]['x'] + '" y="' + textBoxList[textB]['y'] + '" targetRotation="' + textBoxList[textB]['targetRotation'] + '">' + textBoxList[textB]['name'] + '</option>';
    }
    jQuery("#textBoxList").html(textsOptions);
    //image
    for (var imageB in imageLogoPList)
    {
        imagesOptions += '<option org-x="' + imageLogoPList[imageB]['x'] + '" org-y="' + imageLogoPList[imageB]['y'] + '" x="' + imageLogoPList[imageB]['x'] + '" y="' + imageLogoPList[imageB]['y'] + '" org-width="' + imageLogoPList[imageB]['width'] + '" org-height="' + imageLogoPList[imageB]['height'] + '" width="' + imageLogoPList[imageB]['width'] + '" height="' + imageLogoPList[imageB]['height'] + '" targetRotation="' + imageLogoPList[imageB]['targetRotation'] + '">' + imageLogoPList[imageB]['name'] + '</option>';
    }
    jQuery("#imageBoxList").html(imagesOptions);
    //end

}

// init all the data
function loadTemplate(obj) {
    jQuery(".responsive-buttonse").removeClass("right-pannels-this");//for responsive view
    jQuery("body").removeClass("landscape right-pannels-open");//for responsive view

    jQuery(obj).parent().find(".activeClass").removeClass("activeClass");
    jQuery(obj).addClass("activeClass");


    var catTitle = jQuery(obj).attr("title");
    jQuery('#fitName').text(catTitle);
    

    jQuery(".suit-preloader").show();
    jQuery(".tool-customization-area").css("opacity", 0);
    OBJECT.obj_one.url = jQuery(obj).attr("cat_js");
    targetRotation = 0;
    canvas = document.getElementById('canvas');
    ctx = canvas.getContext('2d');
    /*deleteImage();
     deleteImageGraphics();*/
    jQuery(".text-input-holder input").val("");
    jQuery("#canvasHolder").html('');
    mlib = {};
    OBJECT["obj_one"].materials = {};
    loadTextures();
    changeGlowEffects();
    init();
}

//apply texture(common)
function applyTexture() {


    jQuery.ajax({
        url: ajaxPath + "applyTexture.php",
        type: "POST",
        data: {
            jeansActiveData: jeansActiveData
        },
        success: function(msg) {
            
            theCurrentObjM.materials[0].map.image.src = msg;
            
            setTimeout(function() {
                jQuery(".suit-preloader").hide();
                jQuery(".tool-customization-area").css("opacity", 1);
            }, 500);
        }


    });

   
}


function slideToNext(actObj)
{
    jQuery(".right-sub-nav .active").parent().next().find("a").trigger("click")
}

function slideToPrev(actObj)
{
    jQuery(".right-sub-nav .active").parent().prev().find("a").trigger("click")
}