/**
 * ScrollIt
 * ScrollIt.js(scroll•it•dot•js) makes it easy to make long, vertically scrolling pages.
 *
 * Latest version: https://github.com/cmpolis/scrollIt.js
 *
 * License <https://github.com/cmpolis/scrollIt.js/blob/master/LICENSE.txt>
 */
var scrollClkFlg = false;

(function ($) {
    'use strict';

    var pluginName = 'ScrollIt',
            pluginVersion = '1.0.3';

    /*
     * OPTIONS
     */
    var defaults = {
        upKey: 38,
        downKey: 40,
        easing: 'linear',
        scrollTime: 600,
        activeClass: 'active',
        onPageChange: null,
        topOffset: 0
    };



    $.scrollIt = function (options) {

        /*
         * DECLARATIONS
         */
        var settings = $.extend(defaults, options),
                active = 0,
                lastIndex = $('[data-scroll-index]:last').attr('data-scroll-index');

        /*
         * METHODS
         */

        /**
         * navigate
         *
         * sets up navigation animation
         */
        var navigate = function (ndx) {

            if (ndx < 0 || ndx > lastIndex)
                return;

            var targetTop = $('[data-scroll-index=' + ndx + ']').offset().top + settings.topOffset + 1;
            $('html,body').animate({
                scrollTop: targetTop,
                easing: settings.easing
            }, settings.scrollTime);








        };

        /**
         * doScroll
         *
         * runs navigation() when criteria are met
         */
        var doScroll = function (e) {
            var target = $(e.target).closest("[data-scroll-nav]").attr('data-scroll-nav') ||
                    $(e.target).closest("[data-scroll-goto]").attr('data-scroll-goto');
            navigate(parseInt(target));




        };

        /**
         * keyNavigation
         *
         * sets up keyboard navigation behavior
         */
        var keyNavigation = function (e) {
            var key = e.which;
            if ($('html,body').is(':animated') && (key == settings.upKey || key == settings.downKey)) {
                return false;
            }
            if (key == settings.upKey && active > 0) {
                navigate(parseInt(active) - 1);
                return false;
            } else if (key == settings.downKey && active < lastIndex) {
                navigate(parseInt(active) + 1);
                return false;
            }
            return true;
        };

        /**
         * updateActive
         *
         * sets the currently active item
         */
        var updateActive = function (ndx) {
            var prevInd;
            var currentInd;

            var prevView;
            var currentView;






            if (settings.onPageChange && ndx && (active != ndx))
                settings.onPageChange(ndx);

            active = ndx;



            prevInd = jQuery(".right-sub-nav").find(".active").attr("data-scroll-nav");
            prevView = jQuery(".right-sub-nav").find(".active").attr("view");
            currentInd = ndx;


            $('[data-scroll-nav]').removeClass(settings.activeClass);
            $('[data-scroll-nav=' + ndx + ']').addClass(settings.activeClass);


            /*currentView = jQuery('[data-scroll-nav=' + ndx + ']').attr("view")
             
             if (!scrollClkFlg)
             {
             if (prevInd != currentInd) {
             
             changeImageToLeftOnScroll(prevView, currentView);
             }
             }*/
        }

        /**
         * watchActive
         *
         * watches currently active item and updates accordingly
         */
        var watchActive = function () {

            var winTop = $(window).scrollTop();

            var visible = $('[data-scroll-index]').filter(function (ndx, div) {
                return winTop >= $(div).offset().top + settings.topOffset &&
                        winTop < $(div).offset().top + (settings.topOffset) + $(div).outerHeight()
            });


            var newActive = visible.first().attr('data-scroll-index');

            var actScrolIndx = jQuery(".right-sub-nav").find(".active").attr("data-scroll-nav");

            //console.log("newActive = "+newActive+" actScrolIndx =  "+actScrolIndx)


            if (newActive != actScrolIndx) {
                /*if (newActive == "5")
                 {
                 newActive = "6";
                 }*/

                updateActive(newActive);
            }


        };

        /*
         * runs methods
         */
        jQuery(window).on('scroll', watchActive).scroll();


        jQuery(window).on('keydown', keyNavigation);

        jQuery('body').on('click', '[data-scroll-nav], [data-scroll-goto]', function (e) {

            var prevIndx;
            var curIndx;

            prevIndx = jQuery(".right-sub-nav").find(".active").attr("view");

            scrollClkFlg = true;
            e.preventDefault();
            doScroll(e);

            /*setTimeout(function () {
             curIndx = jQuery(".right-sub-nav").find(".active").attr("view");
             changeImageToLeftOnScroll(prevIndx, curIndx);
             scrollClkFlg = false;
             }, 1000)*/
        });

    };
}(jQuery));