var config = {
    map: {
        '*': {
             three          :'Suit_Tool/js/library/three',
             scroll         : 'Suit_Tool/js/library/jquery.mCustomScrollbar.concat.min',
            
             
                 
             BinaryLoader   : 'Suit_Tool/js/library/BinaryLoader',
             Detector       : 'Suit_Tool/js/library/Detector',
             dynamic_texture: 'Suit_Tool/js/library/dynamic_texture',
             pipl_shirt     : 'Suit_Tool/js/pipl-js/pipl_shirt',
             pipl_text      : 'Suit_Tool/js/pipl-js/pipl_text',
             pipl_share     : 'Suit_Tool/js/pipl-js/pipl_share',
            
             elevatezoom    : 'Suit_Tool/js/library/jquery.elevatezoom',
             jquery_ui      : 'Suit_Tool/js/library/jquery-ui',
             bootstrap_min  : 'Suit_Tool/js/library/bootstrap.min',
             sweetalert_min : 'Suit_Tool/js/library/sweetalert.min',
             fabric         : 'Suit_Tool/js/library/fabric-1.7',
             wow            : 'Suit_Tool/js/library/wow',
             spectrum       : 'Suit_Tool/js/library/spectrum',
             device_min     : 'Suit_Tool/js/library/device.min',
             
             custom         : 'Suit_Tool/js/library/custom',
////             angular        : 'Suit_Tool/js/library/angular',
//             angularmin        : 'Suit_Tool/js/library/angular.min',
//             angular_route  : 'Suit_Tool/js/library/angular-route',
//             angular_animate: 'Suit_Tool/js/library/angular-animate',
             pipl_app       : 'Suit_Tool/js/pipl-js/pipl_app',
             pipl_directive : 'Suit_Tool/js/pipl-js/pipl_directive',
             pipl_controller: 'Suit_Tool/js/pipl-js/pipl_controller',
             pipl_factory   : 'Suit_Tool/js/pipl-js/pipl_factory',
             pipl_measurement: 'Suit_Tool/js/pipl-js/pipl_measurement'
        }
    }
};
