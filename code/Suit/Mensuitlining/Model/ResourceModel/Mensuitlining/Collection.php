<?php

namespace Suit\Mensuitlining\Model\ResourceModel\Mensuitlining;

use \Suit\Mensuitlining\Model\ResourceModel\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'id';

    /**
     * Load data for preview flag
     *
     * @var bool
     */
    protected $_previewFlag;

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Suit\Mensuitlining\Model\Mensuitlining', 'Suit\Mensuitlining\Model\ResourceModel\Mensuitlining');
        $this->_map['fields']['id'] = 'main_table.id';
    }
}
