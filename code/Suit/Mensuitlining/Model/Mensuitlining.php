<?php

namespace Suit\Mensuitlining\Model;

class Mensuitlining extends \Magento\Framework\Model\AbstractModel {

    protected function _construct() {
        $this->_init('Suit\Mensuitlining\Model\ResourceModel\Mensuitlining');
    }

    public function getAvailableStatuses() {


        $availableOptions = ['1' => 'Enable',
            '0' => 'Disable'];

        return $availableOptions;
    }

}
