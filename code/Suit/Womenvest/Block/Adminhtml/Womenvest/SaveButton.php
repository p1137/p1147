<?php
/**
* @author Dhirajkumar Deore
* Copyright © 2018 Magento. All rights reserved.
* See COPYING.txt for license details.
*/
namespace Suit\Womenvest\Block\Adminhtml\Womenvest;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

/**
 * Class SaveButton
 * @package Magento\Womenvester\Block\Adminhtml\Edit
 */
class SaveButton extends GenericButton implements ButtonProviderInterface
{
    /**
     * @return array
     */
    public function getButtonData()
    {
        return [
            'label' => __('Save'),
            'class' => 'save primary',
            'data_attribute' => [
                'mage-init' => ['button' => ['ewomenvest' => 'save']],
                'form-role' => 'save',
            ],
            'sort_order' => 90,
        ];
    }
}
