<?php
/**
* @author Dhirajkumar Deore
* Copyright © 2018 Magento. All rights reserved.
* See COPYING.txt for license details.
*/
namespace Suit\Womenvest\Model\Womenvest\Source;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class Status
 */
class Size implements OptionSourceInterface
{

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {

        return [
            ['value' => '1', 'label' => __('Slim')],
            ['value' => '2', 'label' => __('Regular')],
            ['value' => '3', 'label' => __('Wide')]
        ];
    }
}
