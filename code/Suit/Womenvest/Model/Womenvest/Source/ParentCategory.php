<?php
/**
* @author Dhirajkumar Deore
* Copyright © 2018 Magento. All rights reserved.
* See COPYING.txt for license details.
*/
namespace Suit\womenvest\Model\womenvest\Source;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class Status
 */
class ParentCategory implements OptionSourceInterface
{

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {

        return [
            ['value' => '1', 'label' => __('fade_img')],
            ['value' => '2', 'label' => __('riped_img')],
            ['value' => '2', 'label' => __('art_img')]
        ];
    }
}
