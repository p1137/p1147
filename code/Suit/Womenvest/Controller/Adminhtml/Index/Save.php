<?php

/**
 * @author Dhirajkumar Deore
 * Copyright © 2018 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Suit\Womenvest\Controller\Adminhtml\Index;

use Magento\Backend\App\Action\Context;
use Magento\Backend\App\Action;
use Suit\Womenvest\Model\Womenvest;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\TestFramework\Inspection\Exception;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\RequestInterface;

//use Magento\Framework\Stdlib\DateTime\DateTime;
//use Magento\Ui\Component\MassAction\Filter;
//use FME\News\Model\ResourceModel\Test\CollectionFactory;

class Save extends \Magento\Backend\App\Action {

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;
    protected $scopeConfig;
    protected $_escaper;
    protected $inlineTranslation;
    protected $_dateFactory;
    protected $_womenvestpocketFactory;
    protected $_womenveststyleedgeFactory;
    protected $_womenvestlapelsFactory;
    protected $objectManager;
    protected $connection;
    protected $directory;
    protected $rootPath1;
    protected $rootPath;

    /**
     * @param Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param DataPersistorInterface $dataPersistor
     */
    public function __construct(
    Context $context, DataPersistorInterface $dataPersistor, \Magento\Framework\Escaper $escaper, \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Magento\Framework\Stdlib\DateTime\DateTimeFactory $dateFactory, \Suit\Womenvest\Model\WomenvestpocketFactory $womenvestpocketFactory, \Suit\Womenvest\Model\WomenveststyleedgeFactory $womenveststyleedgeFactory, \Suit\Womenvest\Model\WomenvestlapelsFactory $womenvestlapelsFactory
    ) {

        $this->_womenvestpocketFactory = $womenvestpocketFactory;
        $this->_womenveststyleedgeFactory = $womenveststyleedgeFactory;
        $this->_womenvestlapelsFactory = $womenvestlapelsFactory;
        $this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->connection = $this->objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
        $this->directory = $this->objectManager->get('\Magento\Framework\Filesystem\DirectoryList');
        $this->rootPath1 = $this->directory->getRoot();
        $fileSystem = $this->objectManager->create('\Magento\Framework\Filesystem');
        $this->mediaPath = $fileSystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA)->getAbsolutePath();
        $this->rootPath = $this->mediaPath . "suit-tool/";
        $this->dataPersistor = $dataPersistor;
        $this->scopeConfig = $scopeConfig;
        $this->_escaper = $escaper;
        $this->_dateFactory = $dateFactory;
        $this->inlineTranslation = $inlineTranslation;
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute() {

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();


        if ($data) {

            $id = $this->getRequest()->getParam('id');

            if (isset($data['status']) && $data['status'] === 'true') {
                $data['status'] = Block::STATUS_ENABLED;
            }
            if (empty($data['id'])) {
                $data['id'] = null;
            }


            /** @var \Magento\Cms\Model\Block $model */
            $model = $this->_objectManager->create('Suit\Womenvest\Model\Womenvest')->load($id);
            if (!$model->getId() && $id) {
                $this->messageManager->addError(__('This suit_womenvest no longer exists.'));
                return $resultRedirect->setPath('*/*/');
            }


            if (isset($data['fabric_thumb'][0]['name']) && isset($data['fabric_thumb'][0]['tmp_name'])) {
                $data['fabric_thumb'] = 'suit-vest/women_fab_suit_images/thumbs/' . $data['fabric_thumb'][0]['name'];
            } elseif (isset($data['fabric_thumb'][0]['name']) && !isset($data['image'][0]['tmp_name'])) {
                $data['fabric_thumb'] = $data['fabric_thumb'][0]['name'];
            } else {
                $data['fabric_thumb'] = null;
            }

            if (isset($data['display_fabric_thumb'][0]['name']) && isset($data['display_fabric_thumb'][0]['tmp_name'])) {
                $data['display_fabric_thumb'] = 'suit-vest/women_fab_suit_images/thumbs/' . $data['display_fabric_thumb'][0]['name'];
            } elseif (isset($data['display_fabric_thumb'][0]['name']) && !isset($data['image'][0]['tmp_name'])) {
                $data['display_fabric_thumb'] = $data['display_fabric_thumb'][0]['name'];
            } else {
                $data['display_fabric_thumb'] = null;
            }

            if (isset($data['fabric_large_image'][0]['name']) && isset($data['fabric_large_image'][0]['tmp_name'])) {
                $data['fabric_large_image'] = 'suit-vest/women_fab_suit_images/thumbs/' . $data['fabric_large_image'][0]['name'];
            } elseif (isset($data['fabric_large_image'][0]['name']) && !isset($data['image'][0]['tmp_name'])) {
                $data['fabric_large_image'] = $data['fabric_large_image'][0]['name'];
            } else {
                $data['fabric_large_image'] = null;
            }
            $data['status'] = 0;
            $model->setData($data);

            $this->inlineTranslation->suspend();
            try {
                $model->save();
                $this->messageManager->addSuccess(__('suit_womenvest Saved successfully'));
                $this->dataPersistor->clear('suit_womenvest');


                /* Suit Image Generation Start */
                if ($data['fabric_thumb'] != '') {
                    $tableName = $this->connection->getTableName('generation_cmd');
                    /* color image creation start */
                    $fabric_id = $model->getId();

                    $homepath = $this->rootPath;

                    $fabricThumbImgpath = $this->mediaPath . $data['fabric_thumb'];
                    $fabricName = $fabric_id . "_womenvest_fabric_main.png";
                    $fabric_0 = $homepath . 'women_fab_suit_images/' . $fabricName;

//back

                    $fabricName_90 = $fabric_id . "_womenvest_fabric_main_90.png";
                    $fabric_90 = $homepath . 'women_fab_suit_images/' . $fabricName_90;

//for zoom view
                    $fabric_zoom = $homepath . "women_fab_suit_images/" . $fabric_id . "_womenvest_fabric_zoom.png";
                    $shirtFabImgPath = $homepath . 'women_fab_suit_images/';

                    exec('convert ' . $fabricThumbImgpath . ' -rotate 90 ' . $fabric_90);

                    exec('convert -size 1080x1320 tile:' . $fabric_90 . ' ' . $shirtFabImgPath . $fabric_id . '_womenvest_fabric_main_back.png');
                    exec('convert -size 1080x1320 tile:' . $fabric_90 . ' ' . $fabric_90);

                    $fabricName_60 = $fabric_id . "_womenvest_fabric_main_60.png";
                    $fabric_60 = $homepath . 'women_fab_suit_images/' . $fabricName_60;



                    exec('convert -size 1080x1320 tile:' . $fabricThumbImgpath . ' ' . $fabric_0);
//created for collar

                    exec('convert ' . $fabric_0 . ' -background "rgba(0,0,0,0.5)" -distort SRT -67 ' . $fabric_60);

                    exec('convert ' . $fabric_0 . ' -background "rgba(0,0,0,0.5)" -distort SRT 80 ' . $shirtFabImgPath . $fabric_id . '_womenvest_fabric_cuff_side.png');

//zoom view fabirc
                    exec('convert -size 1200x900 tile:' . $fabricThumbImgpath . ' ' . $fabric_zoom);
//created for collar
                    exec('convert ' . $fabric_zoom . ' -background "rgba(0,0,0,0.5)" -distort SRT 90 ' . $shirtFabImgPath . $fabric_id . '_womenvest_fabric_main_back_zoom.png');

                    /* END create fabric image */

                    /* Create plain fabric image */
                    $lapel_fabric_left = $homepath . 'women_fab_suit_images/' . $fabric_id . '_womenvest_lapel_fabric_left.png';
                    $lapel_fabric_right = $homepath . 'women_fab_suit_images/' . $fabric_id . '_womenvest_lapel_fabric_right.png';
                    exec('convert ' . $fabric_0 . ' -background "rgba(0,0,0,0.5)" -distort SRT 15 ' . $lapel_fabric_left);
                    exec('convert ' . $fabric_0 . ' -background "rgba(0,0,0,0.5)" -distort SRT -15 ' . $lapel_fabric_right);


                    $lapel_fabric_upper_left = $homepath . 'women_fab_suit_images/' . $fabric_id . '_womenvest_lapel_fabric_upper_left.png';
                    $lapel_fabric_upper_right = $homepath . 'women_fab_suit_images/' . $fabric_id . '_womenvest_lapel_fabric_upper_right.png';
                    exec('convert ' . $fabric_0 . ' -background "rgba(0,0,0,0.5)" -distort SRT -45 ' . $lapel_fabric_upper_left);
                    exec('convert ' . $fabric_0 . ' -background "rgba(0,0,0,0.5)" -distort SRT 45 ' . $lapel_fabric_upper_right);


                    $fabricName_wave_left = $fabric_id . "_womenvest_fabric_wave_left.png";
                    $fabricName_wave_right = $fabric_id . "_womenvest_fabric_wave_right.png";

                    $wave_left = $homepath . 'women_fab_suit_images/' . $fabricName_wave_left;
                    $wave_right = $homepath . 'women_fab_suit_images/' . $fabricName_wave_right;

                    exec('convert ' . $fabric_0 . ' -background "rgba(0,0,0,0.5)" -distort SRT -5 ' . $wave_left);
                    exec('convert ' . $fabric_0 . ' -background "rgba(0,0,0,0.5)" -distort SRT 5 ' . $wave_right);


// MEN //
// Womenvest pockets 
                    $womenvestpocketFactory = $this->_womenvestpocketFactory->create()->getCollection();
                    $womenvestPocket_collection = $womenvestpocketFactory->getData();

                    foreach ($womenvestPocket_collection as $womenvestPocket) {
                        $womenvestPocketId = $womenvestPocket['vestpocket_id'];

//view 1
                        $glow_view_1 = $homepath . '' . $womenvestPocket['glow_image'];
                        $mask_view_1 = $homepath . '' . $womenvestPocket['mask_image'];
                        $highlight_view_1 = $homepath . '' . $womenvestPocket['hi_image'];



                        if ($womenvestPocket['glow_image'] && $womenvestPocket['mask_image'] && $womenvestPocket['hi_image']) {
//mask
                            $cmd = 'composite -compose Dst_In -gravity center ' . $mask_view_1 . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'women_suit_images/vest_images/vestpocket/' . $fabric_id . '_vestpocket_' . $womenvestPocketId . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//convert
                            $cmd = 'convert ' . $homepath . 'women_suit_images/vest_images/vestpocket/' . $fabric_id . '_vestpocket_' . $womenvestPocketId . '_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_suit_images/vest_images/vestpocket/' . $fabric_id . '_vestpocket_' . $womenvestPocketId . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//glow
                            $cmd = 'convert ' . $homepath . 'women_suit_images/vest_images/vestpocket/' . $fabric_id . '_vestpocket_' . $womenvestPocketId . '_view_1.png ' . $glow_view_1 . ' -geometry +0+0 -composite ' . $homepath . 'women_suit_images/vest_images/vestpocket/' . $fabric_id . '_vestpocket_' . $womenvestPocketId . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//highlight
                            $cmd = 'composite ' . $highlight_view_1 . ' -compose Overlay  ' . $homepath . 'women_suit_images/vest_images/vestpocket/' . $fabric_id . '_vestpocket_' . $womenvestPocketId . '_view_1.png ' . $homepath . 'women_suit_images/vest_images/vestpocket/' . $fabric_id . '_vestpocket_' . $womenvestPocketId . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }
                    }

//breast pocket
                    $glow_view_1 = $homepath . 'woman_glow_mask/vestpocket/Breast-Pocket_shad.png';
                    $mask_view_1 = $homepath . 'woman_glow_mask/vestpocket/Breast-Pocket_mask.png';
                    $highlight_view_1 = $homepath . 'woman_glow_mask/vestpocket/Breast-Pocket_hi.png';

                    if ($glow_view_1 && $mask_view_1 && $highlight_view_1) {
//mask
                        $cmd = 'composite -compose Dst_In -gravity center ' . $mask_view_1 . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'women_suit_images/vest_images/vestpocket/' . $fabric_id . '_breastpocket_view_1.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);
//convert
                        $cmd = 'convert ' . $homepath . 'women_suit_images/vest_images/vestpocket/' . $fabric_id . '_breastpocket_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_suit_images/vest_images/vestpocket/' . $fabric_id . '_breastpocket_view_1.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);
//glow
                        $cmd = 'convert ' . $homepath . 'women_suit_images/vest_images/vestpocket/' . $fabric_id . '_breastpocket_view_1.png ' . $glow_view_1 . ' -geometry +0+0 -composite ' . $homepath . 'women_suit_images/vest_images/vestpocket/' . $fabric_id . '_breastpocket_view_1.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);
//highlight
                        $cmd = 'composite ' . $highlight_view_1 . ' -compose Overlay  ' . $homepath . 'women_suit_images/vest_images/vestpocket/' . $fabric_id . '_breastpocket_view_1.png ' . $homepath . 'women_suit_images/vest_images/vestpocket/' . $fabric_id . '_breastpocket_view_1.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);
                    }


// Womenvest pockets  
// Womenvest Styles 
                    $womenveststyleedgeFactory = $this->_womenveststyleedgeFactory->create()->getCollection();
                    $womenvestStyle_collection = $womenveststyleedgeFactory->getData();

                    $back_glow_view_1 = $homepath . 'woman_glow_mask/vestedge/Back_shad.png';
                    $back_mask_view_1 = $homepath . 'woman_glow_mask/vestedge/Back_mask.png';
                    $back_highlight_view_1 = $homepath . 'woman_glow_mask/vestedge/Back_hi.png';

                    foreach ($womenvestStyle_collection as $womenvestStyle) {
                        $womenvestStyleId = $womenvestStyle['veststyleedge_id'];
                        $styleId = $womenvestStyle['style'];
                        $edgeId = $womenvestStyle['edge'];

//view 1
                        $glow_view_1 = $homepath . '' . $womenvestStyle['glow_image'];
                        $mask_view_1 = $homepath . '' . $womenvestStyle['mask_image'];
                        $highlight_view_1 = $homepath . '' . $womenvestStyle['hi_image'];

                        if ($womenvestStyle['glow_image'] && $womenvestStyle['mask_image'] && $womenvestStyle['hi_image']) {
//mask
                            $cmd = 'composite -compose Dst_In -gravity center ' . $mask_view_1 . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'women_suit_images/vest_images/veststyle/' . $fabric_id . '_style_' . $styleId . '_bottom_' . $edgeId . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//convert
                            $cmd = 'convert ' . $homepath . 'women_suit_images/vest_images/veststyle/' . $fabric_id . '_style_' . $styleId . '_bottom_' . $edgeId . '_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_suit_images/vest_images/veststyle/' . $fabric_id . '_style_' . $styleId . '_bottom_' . $edgeId . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//glow
                            $cmd = 'convert ' . $homepath . 'women_suit_images/vest_images/veststyle/' . $fabric_id . '_style_' . $styleId . '_bottom_' . $edgeId . '_view_1.png ' . $glow_view_1 . ' -geometry +0+0 -composite ' . $homepath . 'women_suit_images/vest_images/veststyle/' . $fabric_id . '_style_' . $styleId . '_bottom_' . $edgeId . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//highlight
                            $cmd = 'composite ' . $highlight_view_1 . ' -compose Overlay  ' . $homepath . 'women_suit_images/vest_images/veststyle/' . $fabric_id . '_style_' . $styleId . '_bottom_' . $edgeId . '_view_1.png ' . $homepath . 'women_suit_images/vest_images/veststyle/' . $fabric_id . '_style_' . $styleId . '_bottom_' . $edgeId . '_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);


//mask
                            $cmd = 'composite -compose Dst_In -gravity center ' . $back_mask_view_1 . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'women_suit_images/vest_images/veststyle/' . $fabric_id . '_style_' . $styleId . '_bottom_' . $edgeId . '_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//convert
                            $cmd = 'convert ' . $homepath . 'women_suit_images/vest_images/veststyle/' . $fabric_id . '_style_' . $styleId . '_bottom_' . $edgeId . '_view_2.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_suit_images/vest_images/veststyle/' . $fabric_id . '_style_' . $styleId . '_bottom_' . $edgeId . '_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//glow
                            $cmd = 'convert ' . $homepath . 'women_suit_images/vest_images/veststyle/' . $fabric_id . '_style_' . $styleId . '_bottom_' . $edgeId . '_view_2.png ' . $back_glow_view_1 . ' -geometry +0+0 -composite ' . $homepath . 'women_suit_images/vest_images/veststyle/' . $fabric_id . '_style_' . $styleId . '_bottom_' . $edgeId . '_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//highlight
                            $cmd = 'composite ' . $back_highlight_view_1 . ' -compose Overlay  ' . $homepath . 'women_suit_images/vest_images/veststyle/' . $fabric_id . '_style_' . $styleId . '_bottom_' . $edgeId . '_view_2.png ' . $homepath . 'women_suit_images/vest_images/veststyle/' . $fabric_id . '_style_' . $styleId . '_bottom_' . $edgeId . '_view_2.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);


// Inner Lining

                            $glow_innerlining_view_2 = $homepath . 'woman_glow_mask/lining/VestlLining_shad.png';
                            $mask_innerlining_view_2 = $homepath . 'woman_glow_mask/lining/VestLining_mask.png';
                            $highlight_innerlining_view_2 = $homepath . 'woman_glow_mask/lining/VestLining_hi.png';

                            if ($glow_innerlining_view_2 != '' && $mask_innerlining_view_2 != '' && $highlight_innerlining_view_2 != '') {
//mask
                                $cmd = 'composite -compose Dst_In -gravity center ' . $mask_innerlining_view_2 . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'women_suit_images/vest_images/vestlining/' . $fabric_id . '_vestlining_view_3.png';
                                $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                                $this->connection->query($writeQuery);
//convert
                                $cmd = 'convert ' . $homepath . 'women_suit_images/vest_images/vestlining/' . $fabric_id . '_vestlining_view_3.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_suit_images/vest_images/vestlining/' . $fabric_id . '_vestlining_view_3.png';
                                $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                                $this->connection->query($writeQuery);
//glow
                                $cmd = 'convert ' . $homepath . 'women_suit_images/vest_images/vestlining/' . $fabric_id . '_vestlining_view_3.png ' . $glow_innerlining_view_2 . ' -geometry +0+0 -composite ' . $homepath . 'women_suit_images/vest_images/vestlining/' . $fabric_id . '_vestlining_view_3.png';
                                $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                                $this->connection->query($writeQuery);
//highlight
                                $cmd = 'composite ' . $highlight_innerlining_view_2 . ' -compose Overlay  ' . $homepath . 'women_suit_images/vest_images/vestlining/' . $fabric_id . '_vestlining_view_3.png ' . $homepath . 'women_suit_images/vest_images/vestlining/' . $fabric_id . '_vestlining_view_3.png';
                                $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                                $this->connection->query($writeQuery);
                            }


// Inner Collar

                            $glow_innerlining_view_2 = $homepath . 'woman_glow_mask/lining/VestCollar_shad.png';
                            $mask_innerlining_view_2 = $homepath . 'woman_glow_mask/lining/VestCollar_mask.png';
                            $highlight_innerlining_view_2 = $homepath . 'woman_glow_mask/lining/VestCollar_hi.png';

                            if ($glow_innerlining_view_2 != '' && $mask_innerlining_view_2 != '' && $highlight_innerlining_view_2 != '') {
//mask
                                $cmd = 'composite -compose Dst_In -gravity center ' . $mask_innerlining_view_2 . ' ' . $fabric_0 . ' -alpha Set ' . $homepath . 'women_suit_images/vest_images/vestlining/' . $fabric_id . '_vestcollar_view_3.png';
                                $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                                $this->connection->query($writeQuery);
//convert
                                $cmd = 'convert ' . $homepath . 'women_suit_images/vest_images/vestlining/' . $fabric_id . '_vestcollar_view_3.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_suit_images/vest_images/vestlining/' . $fabric_id . '_vestcollar_view_3.png';
                                $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                                $this->connection->query($writeQuery);
//glow
                                $cmd = 'convert ' . $homepath . 'women_suit_images/vest_images/vestlining/' . $fabric_id . '_vestcollar_view_3.png ' . $glow_innerlining_view_2 . ' -geometry +0+0 -composite ' . $homepath . 'women_suit_images/vest_images/vestlining/' . $fabric_id . '_vestcollar_view_3.png';
                                $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                                $this->connection->query($writeQuery);
//highlight
                                $cmd = 'composite ' . $highlight_innerlining_view_2 . ' -compose Overlay  ' . $homepath . 'women_suit_images/vest_images/vestlining/' . $fabric_id . '_vestcollar_view_3.png ' . $homepath . 'women_suit_images/vest_images/vestlining/' . $fabric_id . '_vestcollar_view_3.png';
                                $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                                $this->connection->query($writeQuery);
                            }
                        }
                    }

// Womenvest Styles 
// Womenvest lapels 
                    $womenvestlapelsFactory = $this->_womenvestlapelsFactory->create()->getCollection();
                    $womenvestLapel_collection = $womenvestlapelsFactory->getData();

                    foreach ($womenvestLapel_collection as $womenvestLapel) {
                        $womenvestLapelId = $womenvestLapel['vestlapels_id'];
                        $styleId = $womenvestLapel['style'];
                        $lapelId = $womenvestLapel['lapel'];

//view UL
                        $glow_view_UL = $homepath . '' . $womenvestLapel['upper_left_glow_image'];
                        $mask_view_UL = $homepath . '' . $womenvestLapel['upper_left_mask_image'];
                        $highlight_view_UL = $homepath . '' . $womenvestLapel['upper_left_hi_image'];

                        if ($womenvestLapel['upper_left_glow_image'] && $womenvestLapel['upper_left_mask_image'] && $womenvestLapel['upper_left_hi_image']) {
//mask
                            $cmd = 'composite -compose Dst_In -gravity center ' . $mask_view_UL . ' ' . $lapel_fabric_upper_left . ' -alpha Set ' . $homepath . 'women_suit_images/vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_upper_left_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//convert
                            $cmd = 'convert ' . $homepath . 'women_suit_images/vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_upper_left_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_suit_images/vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_upper_left_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//glow
                            $cmd = 'convert ' . $homepath . 'women_suit_images/vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_upper_left_view_1.png ' . $glow_view_UL . ' -geometry +0+0 -composite ' . $homepath . 'women_suit_images/vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_upper_left_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//highlight
                            $cmd = 'composite ' . $highlight_view_UL . ' -compose Overlay  ' . $homepath . 'women_suit_images/vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_upper_left_view_1.png ' . $homepath . 'women_suit_images/vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_upper_left_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }

//view UR
                        $glow_view_UR = $homepath . '' . $womenvestLapel['upper_right_glow_image'];
                        $mask_view_UR = $homepath . '' . $womenvestLapel['upper_right_mask_image'];
                        $highlight_view_UR = $homepath . '' . $womenvestLapel['upper_right_hi_image'];

                        if ($womenvestLapel['upper_right_glow_image'] && $womenvestLapel['upper_right_mask_image'] && $womenvestLapel['upper_right_hi_image']) {
//mask
                            $cmd = 'composite -compose Dst_In -gravity center ' . $mask_view_UR . ' ' . $lapel_fabric_upper_right . ' -alpha Set ' . $homepath . 'women_suit_images/vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_upper_right_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//convert
                            $cmd = 'convert ' . $homepath . 'women_suit_images/vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_upper_right_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_suit_images/vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_upper_right_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//glow
                            $cmd = 'convert ' . $homepath . 'women_suit_images/vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_upper_right_view_1.png ' . $glow_view_UR . ' -geometry +0+0 -composite ' . $homepath . 'women_suit_images/vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_upper_right_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//highlight
                            $cmd = 'composite ' . $highlight_view_UR . ' -compose Overlay  ' . $homepath . 'women_suit_images/vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_upper_right_view_1.png ' . $homepath . 'women_suit_images/vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_upper_right_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }

//view LL
                        $glow_view_LL = $homepath . '' . $womenvestLapel['lower_left_glow_image'];
                        $mask_view_LL = $homepath . '' . $womenvestLapel['lower_left_mask_image'];
                        $highlight_view_LL = $homepath . '' . $womenvestLapel['lower_left_hi_image'];

                        if ($womenvestLapel['lower_left_glow_image'] && $womenvestLapel['lower_left_mask_image'] && $womenvestLapel['lower_left_hi_image']) {
//mask
                            $cmd = 'composite -compose Dst_In -gravity center ' . $mask_view_LL . ' ' . $lapel_fabric_left . ' -alpha Set ' . $homepath . 'women_suit_images/vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_lower_left_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//convert
                            $cmd = 'convert ' . $homepath . 'women_suit_images/vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_lower_left_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_suit_images/vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_lower_left_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//glow
                            $cmd = 'convert ' . $homepath . 'women_suit_images/vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_lower_left_view_1.png ' . $glow_view_LL . ' -geometry +0+0 -composite ' . $homepath . 'women_suit_images/vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_lower_left_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//highlight
                            $cmd = 'composite ' . $highlight_view_LL . ' -compose Overlay  ' . $homepath . 'women_suit_images/vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_lower_left_view_1.png ' . $homepath . 'women_suit_images/vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_lower_left_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }

//view LR
                        $glow_view_LR = $homepath . '' . $womenvestLapel['lower_right_glow_image'];
                        $mask_view_LR = $homepath . '' . $womenvestLapel['lower_right_mask_image'];
                        $highlight_view_LR = $homepath . '' . $womenvestLapel['lower_right_hi_image'];

                        if ($womenvestLapel['lower_right_glow_image'] && $womenvestLapel['lower_right_mask_image'] && $womenvestLapel['lower_right_hi_image']) {
//mask
                            $cmd = 'composite -compose Dst_In -gravity center ' . $mask_view_LR . ' ' . $lapel_fabric_right . ' -alpha Set ' . $homepath . 'women_suit_images/vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_lower_right_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//convert
                            $cmd = 'convert ' . $homepath . 'women_suit_images/vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_lower_right_view_1.png -crop 500x1320+290+0  +repage ' . $homepath . 'women_suit_images/vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_lower_right_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//glow
                            $cmd = 'convert ' . $homepath . 'women_suit_images/vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_lower_right_view_1.png ' . $glow_view_LR . ' -geometry +0+0 -composite ' . $homepath . 'women_suit_images/vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_lower_right_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
//highlight
                            $cmd = 'composite ' . $highlight_view_LR . ' -compose Overlay  ' . $homepath . 'women_suit_images/vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_lower_right_view_1.png ' . $homepath . 'women_suit_images/vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_lower_right_view_1.png';
                            $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                            $this->connection->query($writeQuery);
                        }


                        $cmd = 'convert ' . $homepath . 'women_suit_images/vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_upper_left_view_1.png ' . $homepath . 'women_suit_images/vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_upper_right_view_1.png -geometry +0+0 -composite ' . $homepath . 'women_suit_images/vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_view_1.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);

//                        if (file_exists($homepath . 'women_suit_images/vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_lower_left_view_1.png')) {
                        $cmd = 'convert ' . $homepath . 'women_suit_images/vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_lower_left_view_1.png ' . $homepath . 'women_suit_images/vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_view_1.png -geometry +0+0 -composite ' . $homepath . 'women_suit_images/vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_view_1.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);
//                        }
//                        if (file_exists($homepath . 'women_suit_images/vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_lower_right_view_1.png')) {
                        $cmd = 'convert ' . $homepath . 'women_suit_images/vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_lower_right_view_1.png ' . $homepath . 'women_suit_images/vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_view_1.png -geometry +0+0 -composite ' . $homepath . 'women_suit_images/vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_view_1.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);
//                        }

                        $cmd = "rm " . $homepath . 'women_suit_images/vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_upper_left_view_1.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);

                        $cmd = "rm " . $homepath . 'women_suit_images/vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_upper_right_view_1.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);

                        $cmd = "rm " . $homepath . 'women_suit_images/vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_lower_left_view_1.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);

                        $cmd = "rm " . $homepath . 'women_suit_images/vest_images/vestlapel/' . $fabric_id . '_style_' . $styleId . '_lapel_' . $lapelId . '_lower_right_view_1.png';
                        $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                        $this->connection->query($writeQuery);
                    }
//                    Womenvest lapels

                    $cmd = "rm " . $homepath . 'women_fab_suit_images/' . $fabric_id . "_womenvest_fabric_cuff_side.png";
                    $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($writeQuery);

                    $cmd = "rm " . $homepath . 'women_fab_suit_images/' . $fabric_id . "_womenvest_fabric_main.png";
                    $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($writeQuery);

                    $cmd = "rm " . $homepath . 'women_fab_suit_images/' . $fabric_id . "_womenvest_fabric_main_60.png";
                    $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($writeQuery);

                    $cmd = "rm " . $homepath . 'women_fab_suit_images/' . $fabric_id . "_womenvest_fabric_main_90.png";
                    $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($writeQuery);

                    $cmd = "rm " . $homepath . 'women_fab_suit_images/' . $fabric_id . "_womenvest_fabric_main_back.png";
                    $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($writeQuery);

                    $cmd = "rm " . $homepath . 'women_fab_suit_images/' . $fabric_id . "_womenvest_fabric_main_back_zoom.png";
                    $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($writeQuery);

                    $cmd = "rm " . $homepath . 'women_fab_suit_images/' . $fabric_id . "_womenvest_fabric_wave_left.png";
                    $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($writeQuery);

                    $cmd = "rm " . $homepath . 'women_fab_suit_images/' . $fabric_id . "_womenvest_fabric_wave_right.png";
                    $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($writeQuery);

                    $cmd = "rm " . $homepath . 'women_fab_suit_images/' . $fabric_id . "_womenvest_fabric_zoom.png";
                    $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($writeQuery);

                    $cmd = "rm " . $homepath . 'women_fab_suit_images/' . $fabric_id . "_womenvest_lapel_fabric_left.png";
                    $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($writeQuery);

                    $cmd = "rm " . $homepath . 'women_fab_suit_images/' . $fabric_id . "_womenvest_lapel_fabric_right.png";
                    $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($writeQuery);

                    $cmd = "rm " . $homepath . 'women_fab_suit_images/' . $fabric_id . "_womenvest_lapel_fabric_upper_left.png";
                    $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($writeQuery);

                    $cmd = "rm " . $homepath . 'women_fab_suit_images/' . $fabric_id . "_womenvest_lapel_fabric_upper_right.png";
                    $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$cmd}')";
                    $this->connection->query($writeQuery);

                    $updateQuery = "UPDATE `suit_womenvest_fabrics` SET `status`=1 WHERE `id`=" . $fabric_id;
                    $writeQuery = "INSERT INTO {$tableName}(`command`) VALUES ('{$updateQuery}')";
                    $this->connection->query($writeQuery);
                }
                /* Suit Image Generation End */

                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['id' => $model->getId()]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the suit_womenvest.'));
            }
            $this->dataPersistor->set('suit_womenvest', $data);
            return $resultRedirect->setPath('*/*/edit', ['id' => $this->getRequest()->getParam('id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }

}
