<?php

/**
 * @Author: Ngo Quang Cuong
 * @Date:   2017-10-29 22:02:41
 * @Last Modified by:   https://www.facebook.com/giaphugroupcom
 * @Last Modified time: 2017-10-29 23:39:50
 */

namespace PHPCuong\Newsletter\Controller\Customer;

use Magento\Customer\Api\AccountManagementInterface as CustomerAccountManagement;
use Magento\Customer\Model\Url as CustomerUrl;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Action\Context;
use Magento\Newsletter\Model\SubscriberFactory;
use Magento\Customer\Model\Session;
use Magento\Framework\Controller\Result\JsonFactory;

class Index extends \Magento\Framework\App\Action\Action {

    const WHOLESALERGROUPID = 2;

    /**
     * @var CustomerAccountManagement
     */
    protected $customerAccountManagement;

    /**
     * Customer session
     *
     * @var Session
     */
    protected $customerSession;

    /**
     * Subscriber factory
     *
     * @var SubscriberFactory
     */
    protected $subscriberFactory;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var CustomerUrl
     */
    protected $customerUrl;

    /**
     * @var JsonFactory
     */
    protected $resultJsonFactory;
    protected $_subscriber;

    /**
     * Initialize dependencies.
     *
     * @param Context $context
     * @param SubscriberFactory $subscriberFactory
     * @param Session $customerSession
     * @param StoreManagerInterface $storeManager
     * @param CustomerUrl $customerUrl
     * @param CustomerAccountManagement $customerAccountManagement
     * @param JsonFactory $resultJsonFactory
     */
    public function __construct(
    \Magento\Newsletter\Model\Subscriber $subscriber, Context $context, SubscriberFactory $subscriberFactory, Session $customerSession, StoreManagerInterface $storeManager, CustomerUrl $customerUrl, CustomerAccountManagement $customerAccountManagement, JsonFactory $resultJsonFactory
    ) {
        $this->_subscriber = $subscriber;
        $this->customerAccountManagement = $customerAccountManagement;
        $this->storeManager = $storeManager;
        $this->subscriberFactory = $subscriberFactory;
        $this->customerSession = $customerSession;
        $this->customerUrl = $customerUrl;
        $this->resultJsonFactory = $resultJsonFactory;
        parent::__construct($context);
    }

    public function execute() {
        $data['isSubscribe'] = false;
        $result = $this->resultJsonFactory->create();
        if ($this->customerSession->isLoggedIn()) {
            $customerEmail = $this->customerSession->getCustomer()->getEmail();
            $checkSubscriber = $this->_subscriber->loadByEmail($customerEmail);            
            if ($this->customerSession->getCustomerGroupId() != self::WHOLESALERGROUPID) {
                if ($checkSubscriber->isSubscribed()) {
                    $data['isSubscribe'] = true;
                }
            } else {
                $data['isSubscribe'] = true;
            }
        }
        return $result->setData($data);
    }

}
