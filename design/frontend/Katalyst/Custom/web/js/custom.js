// JavaScript Document
jQuery(document).ready(function () {

// -----------------  Cart discount coupon manage --------------------------------

    setTimeout(function(){ 
        var adjusttop =  jQuery('.form-cart').height();
        jQuery('.cart-discount').css('top', adjusttop + 'px');

    }, 100);

// --------------------------------------  Header --------------------------------

    if(jQuery('.customer-welcome').length<1){
        jQuery('.header.links').children('.authorization-link').remove();   
    }else{
        jQuery('.header.links').children('.authorization-link').hide();
        var li = jQuery('.header.links').children('.authorization-link').html();
        jQuery(".all_links .header.links").append('<li class="authorization-link">'+li+'</li>'); 
        jQuery('.all_links .header.links').children('.authorization-link').show();

    }


    

    fullSize(); //fullSize() Function initialize

    /*--------------owlCarousel---------------*/
    jQuery('.banner-slider').owlCarousel({
        loop: true,
        margin: 0,
        nav: true,
        dots: true,
        autoplay: false,
        items: 1,
        mouseDrag: true,
        navText: [
            "<i class='fa fa-angle-left'></i>",
            "<i class='fa fa-angle-right'></i>"
        ],
    })
    jQuery('.testimonials-slider').owlCarousel({
        loop: true,
        margin: 0,
        nav: false,
        dots: true,
        autoplay: false,
        items: 1,
        mouseDrag: true,
        navText: [
            "<i class='fa fa-angle-left'></i>",
            "<i class='fa fa-angle-right'></i>"
        ],
    })
    jQuery('.product-slider').owlCarousel({
        loop: true,
        margin: 30,
        nav: true,
        dots: false,
        autoplay: false,
        items: 3,
        autoPlayTimeout: 5000,
        navText: [
            "<i class='fa fa-angle-left'></i>",
            "<i class='fa fa-angle-right'></i>"
        ],
        responsive: {
            1000: {
                items: 4
            },
            767: {
                items: 4
            },
            320: {
                items: 1
            }
        }
    })
    /*--------------fullHeight---------*/
    function fullSize() {
        var heights = window.innerHeight;
        jQuery(".fullHt").css('min-height', (heights + 0) + "px");
    }

    /*-----------static-loader------------*/
    function handlePreloader() {
        if (jQuery('#loader-wrapper').length) {
            jQuery('#loader-wrapper').delay(200).fadeOut(500);
        }
    }
    handlePreloader();
    /*------------toggle-menu----------*/
    jQuery(".toggle-menu").click(function () {
        jQuery("body").toggleClass("open-menu");
    });
    /*-----------sticky header---------*/
    var banner_Ht = window.innerHeight - jQuery('header').innerHeight();
    jQuery(window).scroll(function () {
        var sticky = jQuery('body'),
                scroll = jQuery(window).scrollTop();

        if (scroll >= 300)
            sticky.addClass('sticky-header');
        else
            sticky.removeClass('sticky-header');
    });
    /*-------------------Scroll Top Script------------*/

    jQuery(window).scroll(function () {
        if (jQuery(this).scrollTop() != 0) {
            jQuery(".page-wrapper .page-header .panel.wrapper").fadeOut();
            jQuery('#toTop').fadeIn();
        } else {
            jQuery(".page-wrapper .page-header .panel.wrapper").fadeIn();
            jQuery('#toTop').fadeOut();
        }
    });


    jQuery('#toTop').click(function () {
        jQuery("html, body").animate({scrollTop: 0}, 1500);
        return false;
    });

    jQuery('.page-layout-cmscolumn .page.messages').fadeOut(7000);


});
